<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>

<script>
	$(document).ready(function(){
		var pages = ['greetings.do','history.do','organization.do','function.do','operate.do','watch.do','location.do'];
		var pageSub3 = ['organization.do','PageView.do'];
		var pageSub4 = ['function.do','authority.do','convoke.do','process.do','executive.do','estimate.do','petition.do'];
		var pageSub6 = ['watch.do','visitRequ.do','visitList.do'];

		var path = window.location.pathname;

		if(path.includes('/coun/intro')) {
			pages.forEach(function (item, idx) {
				if (path.includes(item)) {
					// console.log('pages', item);
					$(".depth2 > li:nth-child(" + (idx + 1) + ")").addClass("current_on");
				}
			});
			pageSub3.forEach(function (item, idx) {
				if (path.includes(item)) {
					// console.log('pageSub3', item);
					$(".depth2 > li:nth-child(3)").addClass("current_on");
					$(".depth2 > li:nth-child(3) > ul > li:nth-child(" + (idx + 1) + ")").addClass("current_on");
				}
			});
			pageSub4.forEach(function (item, idx) {
				if (path.includes(item)) {
					// console.log('pageSub4', item);
					$(".depth2 > li:nth-child(4)").addClass("current_on");
					$(".depth2 > li:nth-child(4) > ul > li:nth-child(" + (idx + 1) + ")").addClass("current_on");
				}
			});
			pageSub6.forEach(function (item, idx) {
				if (path.includes(item)) {
					// console.log('pageSub6', item);
					$(".depth2 > li:nth-child(6)").addClass("current_on");
					$(".depth2 > li:nth-child(6) > ul > li:nth-child(" + (idx + 1) + ")").addClass("current_on");
				}
			});
		}
	});
</script>

		<!-- sub menu -->
	  <div id="submenuArea" class="w sm_intro">
		<div class="sm_tit ">
			<h2>
			의회소개
			</h2>
		</div>
		<ul class="depth2">
		<!-- 메뉴 활성화시 class="current" 가 class="current_on"으로 바뀌도록 -->
					<li class="current"> <a href="/coun/intro/greetings.do" title="의장인사말">의장인사말</a> </li>
					<li class="current"> <a href="/coun/intro/history.do" title="의회연혁">의회연혁</a> </li>
					<li class="current"> <a href="/coun/intro/organization.do" title="조직 및 구성">조직 및 구성</a>
						<ul>
							<li class="current"> <a href="/coun/intro/organization.do" title="조직 및 구성">조직 및 구성</a> </li>
							<li class="current"> <a href="/coun/intro/phone/PageView.do" title="전화번호">전화번호</a> </li>
						</ul>
					</li>
					<li class="current"> <a href="/coun/intro/function.do" title="의회기능">의회기능</a>
						<ul>
							<li class="current"> <a href="/coun/intro/function.do" title="의회기능">의회기능</a> </li>
							<li class="current"> <a href="/coun/intro/authority.do" title="의회권한">의회권한</a> </li>
							<li class="current"> <a href="/coun/intro/convoke.do" title="회기소집 및 운영">회기소집 및 운영</a> </li>
							<li class="current"> <a href="/coun/intro/process.do" title="의안 처리절차">의안처리절차</a> </li>
							<li class="current"> <a href="/coun/intro/executive.do" title="행정사무감사/조사">행정사무감사/조사</a> </li>
							<li class="current"> <a href="/coun/intro/estimate.do" title="예산 및 결산">예산 및 결산</a> </li>
							<li class="current"> <a href="/coun/intro/petition.do" title="청원/진정 처리절차">청원/진정 처리절차</a> </li>
						</ul>
					</li>
					<li class="current"> <a href="/coun/intro/operate.do" title="의회운영">의회운영</a> </li>
					<li class="current"> <a href="/coun/intro/watch.do" title="방청안내">방청안내</a>
						<ul>
							<li class="current"> <a href="/coun/intro/watch.do" title="방청안내">방청안내</a> </li>
							<li class="current"> <a href="/coun/intro/visitRequ.do" title="온라인신청">온라인신청</a> </li>
							<li class="current"> <a href="/coun/intro/visitList.do" title="신청확인">신청확인</a> </li>
						</ul>
					</li>
					<li class="current"> <a href="/coun/intro/location.do" title="오시는길">오시는길</a> </li>
				</ul>
		
		
			<!-- sub menu end -->
			</div>
			
			
			