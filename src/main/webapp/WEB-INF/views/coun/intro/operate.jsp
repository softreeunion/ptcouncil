<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Pragma" content="no-cache">
<meta property="og:image" content=""/>
<meta name="keywords" content="평택시의회" />
<meta name="description" content="평택시의회 홈페이지입니다." />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><%= webTitle %></title>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="canonical" href="http://www.ptcouncil.go.kr">
<!-- css -->

<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
<!-- sub contentcss -->
<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
<script type="text/javascript" src="/js/slick.min.js"></script>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
    <!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->

	
	<!-- 메인메뉴 end -->
	<div id="subContent">
	<!-- submenu -->
	<jsp:include page="/WEB-INF/views/coun/intro/intro_left.jsp" flush="true"/>
	<!-- sbumenu end -->
<h3 class="skip">본문내용 he</h3>
	   <div id="content">
		<div id="subTitle">
			<h3>의회운영&nbsp;</h3>
		</div>
		   <div id="sub_default">
		   	   
		     <p class="round_txt">의회의 소집과 회기 정례회는 매년 6월 중과 11월 중에 45일 이내로 집회하며, 임시회는 시장 또는 재적의원 1/3 이상의 요구로 매 회기 15일 이내로 소집되고 연간 회의총일수는 정례회와 임시회를 합쳐 100일 이내로 한다. 다만, 부득이한 사유로 인하여 연간 회의초일수를 초과하여 집회할 필요가 있을 때에는 본회의 의결로 10일 이내의 범위에서 이를 연장할 수 있다.</p>
			 <h4>본회의 </h4>  
			 
			  <p class='round_txt'>본 회의는 의회 의원 전원으로 구성되는 회의체입니다. 의회의 최종의사 결정단계이며 재적의원 1/3 이상의 출석으로 개의하고, <b>특별한 경우를 제외하고는 재적의원 과반수 출석과 출석의원 과반수의 찬성으로 의결한다.</b></p>
			 <div class='list_double'><ul>
					<li>의회에 제출되는 안건은 각 상임위원회에 회부되며, 상임위원회에서 심사한 의안을 다시 본회의에서 최종 심의한다. </li>
					<li>의원이 발언하고자 할때에 미리 의장의 허가를 받은 다음 질의, 보충 발언, 의사진행발언 및 신상 발언을 할 수 있다. </li>
					<li>의장은 의결에 있어서 표결권을 가지며, 가부동수일 때에는 부결된 것으로 본다. </li>
					<li>의회에 제출된 의안은 회기 중에 의결되지 못한 이유로 폐기되지 아니하며(다만, 의원의 임기가 만료된 경우에는 그러하지 아니하다) 의회에서 부결된 의안은 같은 회기 중에 다시 발의 또는 제출할 수 없다. </li>
					<li><span class='span_txt'>※ 지방의회의원의 발언은 국회의원과 달리 면책특권이 없다.</span></li>
				</ul></div>
			   
			   <h4>정례회 </h4> 
			   <div class='list_double'>
				   <ul>
                    <li>[집회] 매년 6월 중과 11월 중에 집회한다.</li>
                    <li>[주요 활동] 결산서의 승인(제1차 정례회), 행정사무감사(제1차 정례회), 예산안의 의결(제2차 정례회), 조례안 등 기타 안건을 처리함</li>
                     </ul></div>
			   <h4>상임위원회</h4>
			   <p class='round_txt'>상임위원회는 <b>각종 의안을 전문적이고 능률적으로 심사하기 위하여 의회내에 설치되는 기관</b>으로서 의안을 심사하여 본회의에 보고한다. 회기중에는 위원장이 필요하다고 인정 하거나 재적의원 1/3 이상의 요구가 있을 때 개회한다.</p>
				<div class='list_double'><ul>
					<li>다만 폐회 중에는 본회의의 의결이 있거나 의장이 필요하다고 인정할 때 시장의 요구가 있는 때에 한하여 개의하여 안건을 심사한다.</li>
					<li>현재 우리 시 의회는 <b>의회운영위원회, 기획행정위원회, 복지환경위원회, 산업건설위원회 등 4개 상임위원회로 운영</b>되고 있다.</li>
				</ul></div>
			   
			   <h4>특별위원회</h4>
			   
			  <p class='round_txt'>특별위원회는 <b>수개의 상임위원회의 소관과 관련되거나 특정사안에 관하여 조사</b> 등이 필요한 경우에 본회의 의결로 설치할 수 있다. <b>예산안 심사 및 결산 승인·특정사안의 조사 때로는 행정사무감사나 조사 등을 위해서 구성</b>하기도 한다.</p>
				<div class='list_double'><ul>
					<li>상임위원회는 상설화되어 그 소관에 속한 사항을 심사·처리하지만 특별위원회는 특정사안의 조사를 위하여 구성되는 것이므로 활동 기간이 정하여지며 활동 결과보고서를 작성하여 본 회의에 보고하고 특정사안에 대하여 시정·개선을 요구한다.</li>
				</ul></div>
			   
			  
			   
			   
			   
			   
			</div>
			
         </div>
	 </div>
	</div>
	<!-- footer -->
	<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
	<!-- //footer -->
		
		
	</div>
	
	

</body>
</html>
