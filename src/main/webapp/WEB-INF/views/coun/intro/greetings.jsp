<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Pragma" content="no-cache">
<meta property="og:image" content=""/>
<meta name="keywords" content="평택시의회" />
<meta name="description" content="평택시의회 홈페이지입니다." />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><%= webTitle %></title>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="canonical" href="http://www.ptcouncil.go.kr">
<!-- css -->
<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
<!-- sub contentcss -->
<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />

<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
<script type="text/javascript" src="/js/slick.min.js"></script>

<script>
	// $(document).ready(function(){
	// 	$(".depth2 > li:nth-child(1)").addClass("current_on");
	// });
</script>

	
</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
    <!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->

	
	<!-- 메인메뉴 end -->
	<div id="subContent">
	<!-- submenu -->
	<jsp:include page="/WEB-INF/views/coun/intro/intro_left.jsp" flush="true"/>
	<!-- sbumenu end -->
	<h3 class="skip">본문내용 he</h3>
	   <div id="content">
		<div id="subTitle">
			<h3>의장인사말</h3>
		</div>
		   <div id="sub_default">
		   <div class="sub_greeting01">
		     <p> 존경하고 사랑하는 평택시민 여러분!&nbsp;</p>
		     <p>시민을 위한 열린 의회, 평택시의회 의장 <span>강정구</span>입니다. </p>
		     <div class="pic"><img src="/images/intro/chairman_img.png" title="의장사진"></div>
           </div>
		   <div class="sub_greeting02">
		     <p> 시정에 대한 불편한 점이나 제안 사항이 있으시면
			     자유롭게 의견을 개진하여 주시기 바랍니다.
			     평택시의회는 시민 여러분의 진솔하고 건설적인 의견을 환영합니다.&nbsp; </p>
		     <p>시민 여러분 곁에서 시민의 눈과 귀가 되어 지역 실정에 맞는
			     자치법규를 제정하고 정책과 예산을 효율적으로 조정하며,
			     집행부에 대한 감시와 견제의 역할을 충실히 이행하겠습니다.
  &nbsp;</p>
			   <p>시민 여러분께서 주신 관심과 사랑을 효과적인 정책으로,
			     평택시의 발전과 번영으로 돌려드리기 위해
			     오늘도 최선을 다하겠습니다. </p>
			   <div class="name">평택시의회 의장 <span>강정구</span></div>
           </div>
			
	</div>
	 </div>
	</div>
	<!-- footer -->
	<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
	<!-- //footer -->
		
		
	</div>
	
	

</body>
</html>
