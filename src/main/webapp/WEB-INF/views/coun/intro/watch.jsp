<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Pragma" content="no-cache">
<meta property="og:image" content=""/>
<meta name="keywords" content="평택시의회" />
<meta name="description" content="평택시의회 홈페이지입니다." />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><%= webTitle %></title>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<script>
		var doActProgram    = function() {
			window.open("<%= webBase %>/cmmn/program.do","program","status=no,toolbar=no,resizable=no,menubar=no,location=yes,left=200,top=100,width=1024,height=800,scrollbars=yes");
		};
	</script>
</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
    <!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->

	
	<!-- 메인메뉴 end -->
	<div id="subContent">
	<!-- submenu -->
	<jsp:include page="/WEB-INF/views/coun/intro/intro_left.jsp" flush="true"/>
	<!-- sbumenu end -->
<h3 class="skip">본문내용 he</h3>
	   <div id="content">
		<div id="subTitle">
			<h3>방청안내</h3>
		</div>
		   <div id="sub_default">
		   	   
		     <p class="round_txt">본회의가 개의 중일 때 의사 진행 과정 방청을 원하시는 분들을 위한 안내입니다.</p>
			 <h4>2024년도 의회 운영 기본일정</h4> 
			   <div class='btn_group left'><a href='javascript:void(0);' class='watch_btn' onclick='doActProgram()'>연간 회기 일정 보기</a></div>
			 
			  	   
			   <h4>방청시기 </h4> 
			   <p class='round_txt'>본회의가 개의 중일때 의사 진행 과정 방청을 원하시는 분들을 위한 안내</p>
			   
			   <h4>방청 방법</h4>
			   <p class='round_txt'>방청을 하고자 하는 분은 회의 시작 10분 전까지 신분증을 지참하여 방문하시면 사무국에서 1인 1매 방청권을 교부받아 당일에 한하여 방청하실 수 있습니다.</p>
				<div class='list_double'><ul>
					<li>정례회(2회)</li>
					<li>임시회(수시) : 임시회 개회 시 <br/>(단, 방청권은 방청석의 수용 능력 및 특별한 사정에 의하여 제한될 수 있음)</li>
					<li><span class='span_txt'>문의) 031-8024-7560~8</span></li>
				</ul></div>
			   
			   <h4>방청인의 준수사항</h4>
			   
			  <p class='round_txt'>방청인은 다음과 같은 행위를 할 수 없다.</p>
				<div class='list_double'><ul>
					<li>회의장 안으로 진입하는 행위</li>
					<li>모자·외투를 착용하는 행위</li>
					<li>회의와 관계없는 물품을 휴대하거나 반입하는 행위</li>
					<li>음식물의 섭취나 흡연행위</li>
					<li>신문 기타 서적류의 열독 행위</li>
					<li>의장의 허가 없는 녹음·녹화·촬영행위</li>
					<li>회의 장내 발언에 대하여 공공연하게 가부를 표명하거나 박수를 치는 행위</li>
					<li>기타 소란 등 회의의 진행을 방해하는 행위</li>
				</ul></div>
			   
			   <h4>방청의 제한 및 퇴장</h4>
			   <div class='list_double'><ul>
					<li>흉기 또는 위험한 물품을 휴대한 자</li>
					<li>주기가 있는 자</li>
					<li>정신에 이상이 있는 자</li>
					<li>기타 행동이 수상하다고 인정되거나 질서유지에 방해가 될 우려가 있는 자</li>
				</ul></div>
			
			   
			   
			   
			</div>
			
         </div>
	 </div>
	</div>
	<!-- footer -->
	<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
	<!-- //footer -->
		
		
	</div>
	
	

</body>
</html>
