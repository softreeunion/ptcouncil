<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Pragma" content="no-cache">
<meta property="og:image" content=""/>
<meta name="keywords" content="평택시의회" />
<meta name="description" content="평택시의회 홈페이지입니다." />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><%= webTitle %></title>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="canonical" href="http://www.ptcouncil.go.kr">
<!-- css -->

<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
<!-- sub contentcss -->
<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
<script type="text/javascript" src="/js/slick.min.js"></script>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
    <!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->

	
	<!-- 메인메뉴 end -->
	<div id="subContent">
	<!-- submenu -->
	<jsp:include page="/WEB-INF/views/coun/intro/intro_left.jsp" flush="true"/>
	<!-- sbumenu end -->
<h3 class="skip">본문내용 he</h3>
	   <div id="content">
		<div id="subTitle">
			<h3>청원/진정처리절차</h3>
		</div>
		   <div id="sub_default">
		   	   
		   <h4>청원의 의의</h4>
			   <p class="round_txt">청원권은 연혁적으로 시민적 법치국가에서 보장하고 있는 국민의 구제권리 가운데에서 가장 오래된 전통적인 권리로서 우리나라 헌법에도 제26조에 명시하여 규정하고 있으며, 국민이 국가기관에 문서로 일정한 의견 또는 희망을 표시하는 것은 물론 권리 또는 이익이 침해되었다고 할 때 구제를 호소하는 국민의 권리임.</p>
			 <h4>청원절차 </h4>  
			 <ul class="peti">
				<li class="pet1"><span>청원인 : 소개의원</span> 
				 <ul >
					<li class="tit1">접수</li>
					 <li class="tit2">접수하지 않음 통보</li>
					</ul>
				 
				 </li>
				<li class="pet2"><span>의장</span>
				<ul>
	          <li class="pet2_1"><span>자치단체장</span>
					<ul>
					<li class="tit1">처리결과보고:의회</li>
					 <li class="tit2">이송</li>
					</ul>
					
					</li>
	          </ul>
				 </li>
             <li class="pet3"><span>소관위(특위)</span></li>
				
            <li class="pet4"><span>본회의</span></li>
				
           <li class="pet5"><span>의장</span>
				 <ul>
					<li class="tit1">부의하지 않을시 보고</li>
					 </ul>
				 </li>
           </ul>

			    <h4>청원서 제출 </h4> 
			  <div class='list_double'><ul>
					<li>청원은 반드시 문서로 하여야 하며 평택시의회 의원의 소개의견서를 첨부 </li>
					<li>청원서에는 청원인의 성명(법인은 그 명칭과 대표자 성명) 주소 및 연락처 기재 서명 날인</li>
					<li>소개의원은 청원서와 소개의견서에 서명 날인</li>
					<li>청원서에는 청원의 취지와 이유 및 요구의 주된 내용을 구체적으로 명시 </li>
					<li>청원인의 현주소 또는 거소를 증명하는 주민등록서류 첨부 </li>
					<li>다수인 공동청원일 경우 대표자를 선임 표시하고 연명부는 원본을 첨부</li>
					<li>기타 필요한 참고자료 첨부 가능(관련 서류, 도면, 사진 등)</li>
				</ul></div>
			   
			   <h4>청원의 접수 </h4> 
			   <p class='round_txt'><b>접수는 제출한 청원을 일단 사실적으로 받아들이는 사실행위임.</b></p>
			   <div class='list_double'><ul>
					<li>형식적 요건 검토 후 접수</li>
					<li>청원처리부에 등재</li>
				</ul></div>
				<p class='round_txt'><b>보완요구</b></p>
				<div class='list_double'><ul>
					<li>제출된 청원의 형식적 요건 미비시 보완 요구</li>
					<li>형식요건 : 의원의 소개의견서 첨부 청원의 취지와 이유 및 요구의 주된 내용 명시, 주소, 성명 기재 서명 날인 등</li>
					<li>보완 기간 : 15일 이내로 지정</li>
				</ul></div>
			   <h4>수리 여부 결정</h4>
			   <div class='list_double'><ul>
					<li>수리는 접수된 청원이 형식적, 내용적 요건을 갖추어서 유효한 청원임을 인정하는 준법률행위적 행정행위임</li>
					<li>형식적 요건을 충족하고 그 내용 또한 불수리 사항이 아니면 수리</li>
					<li>불수리 사항
						<ul>
							<li>재판에 간섭하는 내용</li>
							<li>법령에 위배되는 경우</li>
							<li>국가원수 국가기관을 모독하는 것</li>
							<li>이중 청원 : 
								<ul>
									<li>동일 기관(이유가 다르더라도 요구의 주된 내용이 동일)의 청원서를 동일 기관 2개 이상에 제출한 것 중 나중에 접수한 것은 수리 불가</li>
								</ul>
							</li>
							<li>※ 벌칙 사항 : 타인을 모해하는 허위사실의 청원 (불수리 통지)</li>
						</ul>
					</li>
					<li>불수리 사항일 경우 사유를 명시하여 소개의원과 청원인에게 통지 (이의신청)</li>
					<li>이중 청원의 사유로 불 수리된 경우에 한해 통지를 받은 날로부터 15일 이내 소개의원 경유 이의신청 가능</li>
					<li>의장은 이의신청이 이유 있다고 인정한 때 접수 처리 (청원의 철회)</li>
					<li>청원인이 접수된 청원을 철회하고자 할때에 철회 요구서에 철회 이유를 명기하고 청원인과 소개의원이 서명날인하여 의장에게 제출</li>
					<li>위원회 또는 본회의의 의제가 된(상정 후 계류된) 청원의 철회 시에는 위원회 또는 본회의의 동의 필요</li>
				</ul></div>
			   
			   <h4>의장 및 본회의 보고</h4>
			   
			   <div class='list_double'><ul>
					<li>접수 및 의장 보고</li>
					<li>위원회 회부사항 본회의 보고</li>
					<li>청원요지서 인쇄 및 각 의원에게 배부</li>
				</ul></div>
			   
			   <h4>위원회 회부심사</h4>
			   <p class='round_txt'><b>보완요구</b></p>
			   <div class='list_double'><ul>
					<li>회부사항 보고</li>
					<li>의사 일정 상정(유사 청원 일괄상정 가능)</li>
					<li>소개의원의 청원 취지 설명 요구 가능 </li>
					<li>전문위원의 검토보고</li>
					<li>질의 답변 <ul><li>(위원회의 의결로 청원인, 이해관계인 및 학식과 경험이 있는 자로부터의 진술 의견 청취 가능)</li></ul></li>
					<li>토론<ul><li>(필요시 소위원회 구성심사 가능, 예산이 수반되는 청원은 시장 또는 관계 공무원의 의견 청취)</li></ul></li>
					<li>의결
						<ul>
							<li>※ 청원은 가결시키는 것이 아니고 청원에 대한 의견을 채택하는 행위임.</li>
							<li>※ 제척사항 : 청원과 직접 이해관계 또는 공정하게 할 수 없는 사유가 있는 의원은 심사 의결 참여 불가 위반 시 징계 사유 (의장보고)</li>
						</ul>
					</li>
					<li>위원회에서 채택 의결한 청원은 처리하여야 할 기관(시장 또는 의회)을 명시한 의견서를 첨부하여 심사보고 <br/>※ 의회에서 처리해야 할 청원인 경우 의견서에 조치 방법 등 포함작성 (조례 제ㆍ개정 필요시 조례안 제안 등)</li>
					<li>위원회에서 본회의에 부의할 필요가 없다고 결정한 청원은 심사 결과에 불채택 사유를 명기하여 의장에게 보고하면 본회의에 보고 
						<ul>
							<li>※ 불채택 사유 : 청원 목적 달성, 예산 사정 등 현실적 실현 불가능, 시책에 어긋나는 등 타당성의 결여 등 본회의에 부의하지 않기로 보고한 날로부터 7일 이내 (휴 폐회 기간 제외)</li>
							<li>의장 또는 재적의원 1/3 이상 요구 시 본회의 부의</li>
						</ul>
					</li>
					<li>처리 기간 내 심사하지 못했을 경우 중간보고와 함께 심사 기간 연장 요구 (통지)</li>
					<li>청원인 및 소개의원에 통지 
						<ul>
							<li>위원회 회부</li>
							<li>위원회에서 본회의에 부의할 필요가 없다고 결정하여 심사보고 시</li>
						</ul>
					</li>
				</ul></div>
			   
			   <h4>본회의 부의</h4>
			   
			   <div class='list_double'><ul>
					<li>회부사항 보고
						<ul>
							<li>의사 일정 상정</li>
							<li>심사보고</li>
							<li>질의 답변 및 토론</li>
							<li>의결 (위원회 의견서 채택)</li>
						</ul>
					</li>
					<li>의결결과 청원인 및 소개의원에 통지 (이송)</li>
					<li>시장이 처리함이 타당한 것은 청원서에 의견서 첨부 이송 (청원 처리)</li>
					<li>시장의 처리 (의회보고)</li>
					<li>시장은 처리 결과를 지체없이 의장에게 보고</li>
					<li>처리 결과 본회의에 최종 보고</li>
				</ul></div>
			   
			   <h4>진정 민원안내</h4>
			   <p class='round_txt'><b>평택시의회에서는 시정 및 의정활동 등에 관한 진정을 접수 처리하고 있습니다.</b></p>
			   <div class='list_double'><ul>
					<li>대상 : 진정, 건의, 탄원, 호소문</li>
					<li>접수 방법 : 우편 또는 방문</li>
					<li>접수처 : 평택시 의회사무국 (접수 후 집행기관 및 의원회부)</li>
				</ul></div>
				<p class='round_txt'><b>진정서 수리가 되지 않는 사항</b></p>
				<div class='list_double'><ul>
					<li>재판에 간섭하는 사항 </li>
					<li>의장 또는 의원을 모독하는 사항 </li>
					<li>보완기간 : 15일 이내로 지정</li>
					<li>동일인이 동일한 내용의 진정을 2건 이상 제출하였을 때 나중에 제출한 진정</li>
					<li>진정인( 다수인 경우에는 대표자 )의 주소, 성명 및 진정의 내용이 분명치 않은 것</li>
				</ul></div>
			   <p class='round_txt'><b>진정서는 다음과 같이 구분처리 됩니다.</b></p>
				<div class='list_double'><ul>
					<li>집행기관 이송, 민원 처리 결과 -> 민원인, 의회회신(의원회부)</li>
				</ul></div>
			   
			   
			   
			   
			</div>
			
         </div>
	 </div>
	</div>
	<!-- footer -->
	<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
	<!-- //footer -->
		
		
	</div>
	
	

</body>
</html>
