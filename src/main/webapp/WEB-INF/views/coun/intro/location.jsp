<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Pragma" content="no-cache">
<meta property="og:image" content=""/>
<meta name="keywords" content="평택시의회" />
<meta name="description" content="평택시의회 홈페이지입니다." />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><%= webTitle %></title>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="canonical" href="http://www.ptcouncil.go.kr">
<!-- css -->

<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
<!-- sub contentcss -->
<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />
	
<script type="text/javascript" src="/js/jquery.js"></script> 
<script type="text/javascript" src="/js/jquery-ui.min.js"></script> 
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>	
<script type="text/javascript" src="/js/slick.min.js"></script>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
    <!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->

	
	<!-- 메인메뉴 end -->
	<div id="subContent">
	<!-- submenu -->
	<jsp:include page="/WEB-INF/views/coun/intro/intro_left.jsp" flush="true"/>
	<!-- sbumenu end -->

	<h3 class="skip">본문내용 he</h3>
	   <div id="content">
		<div id="subTitle">
			<h3>오시는길</h3>
		</div>
		   <div id="sub_default">
		     <h4>찾아오시는길</h4> 
			    <div id="map">
				<div id='map_view' style='width:100%;height:500px;'>
					<script src="//dapi.kakao.com/v2/maps/sdk.js?appkey=7bab18077def29e4be9bea03286dac44"></script>
					<!--<script src="//dapi.kakao.com/v2/maps/sdk.js?appkey=dbb6be5e4cfba4392b73bcc89ecdeeea"></script>-->
					<script>
						var objs = document.getElementById("map_view"); // 지도를 표시할 div
						var opts = {
							center: new kakao.maps.LatLng(37.067319,127.065020), // 지도의 중심좌표
							level: 3, // 지도의 확대 레벨
							mapTypeId : kakao.maps.MapTypeId.ROADMAP // 지도종류
						};// 지도를 생성한다
						var map = new kakao.maps.Map(objs, opts);
						// 지도 타입 변경 컨트롤을 생성한다
						var mapTypeControl = new kakao.maps.MapTypeControl();
						// 지도의 상단 우측에 지도 타입 변경 컨트롤을 추가한다
						map.addControl(mapTypeControl, kakao.maps.ControlPosition.TOPRIGHT);
						// 지도에 확대 축소 컨트롤을 생성한다
						var zoomControl = new kakao.maps.ZoomControl();
						// 지도의 우측에 확대 축소 컨트롤을 추가한다
						map.addControl(zoomControl, kakao.maps.ControlPosition.RIGHT);
						// 지도에 마커를 생성하고 표시한다
						var marker = new kakao.maps.Marker({
							position: new kakao.maps.LatLng(37.067419,127.065020), // 마커의 좌표
							map: map // 마커를 표시할 지도 객체
						});
					</script>
			   
			   <div class="addr">
<ul>
<li>경기도 평택시 경기대로 1366(서정동)</li>
</ul>
</div>
			   </div>
			   </div>
				
			   
			  	   
			   <h4>교통편 </h4> 
			  <div class='table_wrap'>
					<table class="board_n_list">
					<caption class='hide'>상위원회</caption>
					<colgroup>
						<col style='width:8%;'/>
						<col style='width:10%;'/>
						<col style='width:22%;'/>
						<col style='width:45%;'/>
						<col style='width:15%;'/>
					</colgroup>
					<thead>
					<tr>
						<th scope='col'>구분</th>
						<th scope='col'>이용노선</th>
						<th scope='col' colspan='2'>이용방법</th>
						<th scope='col'>소요시간</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td rowspan='4'>자가용</td>
						<td rowspan='2'>고속도로</td>
						<td class='left'>오산IC → 송탄출장소</td>
						<td class='left'>오산IC → 고가도로를 지나 첫번째 사거리에서 평택방향(좌회전) → 직진(7분 정도) → 송탄출장소</td>
						<td>오산IC에서 20분</td>
					</tr>
					<tr>
						<td class='left'>송탄IC → 송탄출장소</td>
						<td class='left'>송탄IC → 첫번째 사거리에서 직진 → 송탄출장소</td>
						<td>송탄IC에서 10분</td>
					</tr>
					<tr>
						<td rowspan='2'>국도</td>
						<td class='left'>수원/서울방향 → 송탄출장소</td>
						<td class='left'>1번 국도 → 비상활주로 → 병점 → 송탄 → 송탄출장소</td>
						<td>수원터미널에서 60분</td>
					</tr>
					<tr>
						<td class='left'>평택/천안방향 → 송탄출장소</td>
						<td class='left'>1번 국도 → 평택이마트 → 송탄 → 송탄출장소</td>
						<td>평택이마트에서 15분</td>
					</tr>
					<tr>
						<td rowspan='2'>버스</td>
						<td>평택방면</td>
						<td class='left'>1-1, 2-2</td>
						<td class='left'>송탄출장소 앞 하차</td>
						<td></td>
					</tr>
					<tr>
						<td>오산방면</td>
						<td class='left'>1-1, 2-2</td>
						<td class='left'>송탄출장소 앞 하차</td>
						<td></td>
					</tr>
					</tbody>
				</table>
				</div>
			   <div class='list_double'>
					<ul>
						<li>교통문의 : 대중교통과 버스정책팀 031-8024-4873</li>
					</ul>
				</div>
			   
			   <h4>평택시의회 송탄출장소 부설주차장 이용안내</h4>
			   <div class='list_double'>
					<ul>
						<li>운영시간 : 월 ~ 금, 오전 8시 ~ 오후 7시(공휴일 제외)</li>
					</ul>
				</div>
			   <p class='round_txt'>주차요금(현금 및 카드결제 가능)</p>
				<div class='table_wrap'>
					<table class="board_n_list">
					<caption class='hide'>상위원회</caption>
					<colgroup>
						<col style='width:14%;'/>
						<col style='width:13%;'/>
						<col style='width:73%;'/>
					</colgroup>
					<thead>
					<tr>
						<th scope='col'>구분</th>
						<th scope='col'>요금</th>
						<th scope='col'>비고</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>최초 30분까지</td>
						<td>무료</td>
						<td class='left' rowspan='4'>
							<p class='p_list'>주차요금 면제 및 감면</p>
							<div class='list_style01'>
								<ul>
									<li>1시간 무료: 해당 부서장이 민원확인증을 발급한 차량</li>
									<li>주차요금 50% 감면 ☞ 인증서 부착 및 증명서 제시 차량에 한함
										<ul>
											<li>경형차량, 저공해자동차</li>
											<li>장애인, 국가유공자, 장기/인체조직 기증, 병역명문가, 평택시 우수자원봉사자, 다자녀카드, 우수납세자 등 증명서 소지차량</li>
										</ul>
									</li>
								</ul>
							</div>
						</td>
					</tr>
					<tr>
						<td>30분 초과부터<br/>2시간 까지</td>
						<td style='border-right:1px solid #d2d2d2;'>30분당 500원</td>
					</tr>
					<tr>
						<td>2시간 초과부터</td>
						<td style='border-right:1px solid #d2d2d2;'>30분당 1,000원</td>
					</tr>
					<tr>
						<td>1일 주차요금</td>
						<td style='border-right:1px solid #d2d2d2;'>5,000원</td>
					</tr>
					</tbody>
				</table>
					<div class='list_double'>
					<ul>
						<li>주차요금문의: 주차요금 정산소 031-8024-6076</li>
					</ul>
				</div>
				</div>
			    
			   
			   <h4>근무시간</h4> 
			   <div class='list_double'>
					<ul>
						<li>평일 : 09:00 ~ 18:00 (점심시간 12:00 ~ 13:00)</li>
						<li>토요일,일요일,공휴일 : 휴무</li>
					</ul>
				</div>
			   
			</div>
			
         </div>
	 </div>
	</div>
	<!-- footer -->
	<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
	<!-- //footer -->
		
		
	</div>
	
	

</body>
</html>
