﻿<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Pragma" content="no-cache">
<meta property="og:image" content=""/>
<meta name="keywords" content="평택시의회" />
<meta name="description" content="평택시의회 홈페이지입니다." />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><%= webTitle %></title>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="canonical" href="http://www.ptcouncil.go.kr">
<!-- css -->

<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
<!-- sub contentcss -->
<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
<script type="text/javascript" src="/js/slick.min.js"></script>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
    <!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->

	
	<!-- 메인메뉴 end -->
	<div id="subContent">
	<!-- submenu -->
	<jsp:include page="/WEB-INF/views/coun/intro/intro_left.jsp" flush="true"/>
	<!-- sbumenu end -->
		<h3 class="skip">본문내용 he</h3>
	   <div id="content">
		<div id="subTitle">
			<h3>전화번호&nbsp;</h3>
		</div>
		   <div id="sub_default">
		   	   
		   <h4>담당업무 및 연락처</h4>
		     <p class="round_txt"><span class="phone"></span><em>평택시의회의 담당 업무및 연락처를 확인 할 수 있습니다.</em>  </p>
		   <div class="table_wrap">

			   <c:if test="${! empty dataView}">${content1}</c:if>

<%--
             <table  class="board_n_list"><caption>담당업무 및 연락처를 확인할 수 있습니다</caption>					
					<colgroup>
						<col style='width:18%;'/>
						<col style='width:15%;'/>
						<col style='width:15%;'/>
						<col style='width:32%;'/>
						<col style='width:20%;'/>
					</colgroup>
					<thead>
						<tr>
							<th scope='col' class='first'>소속</th>
							<th scope='col'>성명</th>
							<th scope='col'>직위</th>
							<th scope='col'>담당업무</th>
							<th scope='col'>전화번호</th>
						</tr>
					</thead>
					<tbody>

						<tr>
							<td class='first bold'>의회사무국</td>
							<td>장OO</td>
							<td>국장</td>
							<td class='left'>의회사무국 업무 총괄</td>
							<td>031-8024-7550</td>
						</tr>

						<tr>
							<td class='first bold' rowspan='2'>의회운영위원회</td>
							<td>김OO</td>
							<td>전문위원</td>
							<td class='left'>의회운영위원회 소관업무 총괄</td>
							<td>031-8024-7523</td>
						</tr>
						<tr>
							<td>전OO</td>
							<td>주무관</td>
							<td class='left'>운영위, 예결위</td>
							<td>031-8024-7524</td>
						</tr>

						<tr>
							<td class='first bold' rowspan='2'>기획행정위원회</td>
							<td>전OO</td>
							<td>전문위원</td>
							<td class='left'>기획행정위원회 소관업무 총괄</td>
							<td>031-8024-7533</td>
						</tr>
						<tr>
							<td>김OO</td>
							<td>주무관</td>
							<td class='left'>기획행정위원회 소관업무</td>
							<td>031-8024-7535</td>
						</tr>
						
						<tr>
							<td class='first bold' rowspan='2'>복지환경위원회</td>
							<td>공OO</td>
							<td>전문위원</td>
							<td class='left'>복지환경위원회 소관업무 총괄</td>
							<td>031-8024-7517</td>
						</tr>
						<tr>
							<td>안OO</td>
							<td>주무관</td>
							<td class='left'>복지환경위원회 소관업무</td>
							<td>031-8024-7519</td>
						</tr>

						<tr>
							<td class='first bold' rowspan='2'>산업건설위원회</td>
							<td>김OO</td>
							<td>전문위원</td>
							<td class='left'>산업건설위원회 소관업무 총괄</td>
							<td>031-8024-7543</td>
						</tr>
						<tr>
							<td>주OO</td>
							<td>주무관</td>
							<td class='left'>산업건설위원회 소관업무</td>
							<td>031-8024-7545</td>
						</tr>

						<tr>
							<td class='first bold' rowspan='13'>의정팀</td>
							<td>이OO</td>
							<td>의정팀장</td>
							<td class='left'>의정업무 총괄</td>
							<td>031-8024-7560</td>
						</tr>
						<tr>
                            <td>정OO</td>
                            <td>주무관</td>
                            <td class='left'>청사, 전기</td>
                            <td>031-8024-7562</td>
                        </tr>
						<tr>
							<td>조OO</td>
							<td>주무관</td>
							<td class='left'>서무, 의정연수, 업무보고</td>
							<td>031-8024-7561</td>
						</tr>
						<tr>
							<td>우O</td>
							<td>주무관</td>
							<td class='left'>인사운영</td>
							<td>031-8024-7564</td>
						</tr>
						<tr>
							<td>한OO</td>
							<td>주무관</td>
							<td class='left'>의장 수행</td>
							<td>031-8024-7563</td>
						</tr>
						<tr>
							<td>박OO</td>
							<td>주무관</td>
							<td class='left'>회계, 예산</td>
							<td>031-8024-7568</td>
						</tr>
						<tr>
							<td>이OO</td>
							<td>주무관</td>
							<td class='left'>의장 수행</td>
							<td>031-8024-7507</td>
						</tr>
						<tr>
							<td>오OO</td>
							<td>주무관</td>
							<td class='left'>과서무, 민원</td>
							<td>031-8024-7565</td>
						</tr>
						<tr>
							<td>김OO</td>
							<td>주무관</td>
							<td class='left'>업무차량</td>
							<td>031-8024-7567</td>
						</tr>
						<tr>
							<td>최OO</td>
							<td>주무관</td>
							<td class='left'>의전차량</td>
							<td>031-8024-7566</td>
						</tr>
						<tr>
							<td>지OO</td>
							<td>청원경찰</td>
							<td class='left'>청사방호</td>
							<td>031-8024-7505</td>
						</tr>
						<tr>
							<td>박OO</td>
							<td>공무직</td>
							<td class='left'>의장비서</td>
							<td>031-8024-7500</td>
						</tr>
						<tr>
							<td>류OO</td>
							<td>공무직</td>
							<td class='left'>국장비서</td>
							<td>031-8024-7550</td>
						</tr>

						<tr>
							<td class='first bold' rowspan='7'>의사팀</td>
							<td>최OO</td>
							<td>의사팀장</td>
							<td class='left'>의사업무 총괄</td>
							<td>031-8024-7570</td>
						</tr>
						<tr>
							<td>김OO</td>
							<td>주무관</td>
							<td class='left'>본회의, 의안접수 및 관리</td>
							<td>031-8024-7571</td>
						</tr>
						<tr>
							<td>장OO</td>
							<td>주무관</td>
							<td class='left'>청소년의회, 의원연구회</td>
							<td>031-8024-7572</td>
						</tr>
						<tr>
							<td>오OO</td>
							<td>주무관</td>
							<td class='left'>본회의, 의회운영위원회 속기 및 회의록 관리</td>
							<td>031-8024-7574</td>
						</tr>
						<tr>
							<td>이OO</td>
							<td>주무관</td>
							<td class='left'>복지환경위원회 속기 및 회의록 관리</td>
							<td>031-8024-7575</td>
						</tr>
						<tr>
							<td>여OO</td>
							<td>주무관</td>
							<td class='left'>산업건설위원회 속기 및 회의록 관리</td>
							<td>031-8024-7576</td>
						</tr
						<tr>
							<td>장OO</td>
							<td>주무관</td>
							<td class='left'>기획행정위원회 속기 및 회의록 관리</td>
							<td>031-8024-7577</td>
						</tr>

						<tr>
							<td class='first bold' rowspan='5'>홍보팀</td>
							<td>김OO</td>
							<td>홍보팀장</td>
							<td class='left'>홍보팀 총괄</td>
							<td>031-8024-7580</td>
						</tr>
						<tr>
							<td>김OO</td>
							<td>주무관</td>
							<td class='left'>방송, 통신, 홈페이지</td>
							<td>031-8024-7581</td>
						</tr>
						<tr>
							<td>조OO</td>
							<td>주무관</td>
							<td class='left'>언론광고, SNS 관리</td>
							<td>031-8024-7582</td>
						</tr>
						<tr>
							<td>최OO</td>
							<td>주무관</td>
							<td class='left'>특집기사, 축사</td>
							<td>031-8024-7583</td>
						</tr>
						<tr>
							<td>정OO</td>
							<td>주무관</td>
							<td class='left'>사진 촬영 및 관리</td>
							<td>031-8024-7585</td>
						</tr>

						<tr>
							<td class='first bold' rowspan='9'>정책지원팀</td>
							<td>최OO</td>
							<td>정책지원팀장</td>
							<td class='left'>정책지원업무 총괄</td>
							<td>031-8024-7610</td>
						</tr>
						<tr>
							<td>박OO</td>
							<td>주무관</td>
							<td class='left'>기획행정위원회 정책지원</td>
							<td>031-8024-7611</td>
						</tr>
						<tr>
							<td>황OO</td>
							<td>주무관</td>
							<td class='left'>기획행정위원회 정책지원</td>
							<td>031-8024-7612</td>
						</tr>
						<tr>
							<td>김OO</td>
							<td>주무관</td>
							<td class='left'>복지환경위원회 정책지원</td>
							<td>031-8024-7614</td>
						</tr>
						<tr>
							<td>노OO</td>
							<td>주무관</td>
							<td class='left'>복지환경위원회 정책지원</td>
							<td>031-8024-7615</td>
						</tr>
						<tr>
							<td>손OO</td>
							<td>주무관</td>
							<td class='left'>복지환경위원회 정책지원</td>
							<td>031-8024-7616</td>
						</tr>
						<tr>
							<td>홍OO</td>
							<td>주무관</td>
							<td class='left'>산업건설위원회 정책지원</td>
							<td>031-8024-7617</td>
						</tr>
						<tr>
							<td>김OO</td>
							<td>주무관</td>
							<td class='left'>산업건설위원회 정책지원</td>
							<td>031-8024-7618</td>
						</tr>
						<tr>
							<td>박OO</td>
							<td>주무관</td>
							<td class='left'>산업건설위원회 정책지원</td>
							<td>031-8024-7619</td>
						</tr>

					</tbody>
				</table>
--%>
</div>
		  
			   </div>
			   
			
         </div>
	 </div>
	</div>
	<!-- footer -->
	<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
	<!-- //footer -->
		
		
	</div>
	
	

</body>
</html>
