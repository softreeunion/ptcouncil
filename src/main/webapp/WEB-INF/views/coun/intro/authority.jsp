<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Pragma" content="no-cache">
<meta property="og:image" content=""/>
<meta name="keywords" content="평택시의회" />
<meta name="description" content="평택시의회 홈페이지입니다." />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><%= webTitle %></title>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="canonical" href="http://www.ptcouncil.go.kr">
<!-- css -->

<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
<!-- sub contentcss -->
<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
<script type="text/javascript" src="/js/slick.min.js"></script>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
    <!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->

	
	<!-- 메인메뉴 end -->
	<div id="subContent">
	<!-- submenu -->
	<jsp:include page="/WEB-INF/views/coun/intro/intro_left.jsp" flush="true"/>
	<!-- sbumenu end -->
	<h3 class="skip">본문내용 he</h3>
	   <div id="content">
		<div id="subTitle">
			<h3>의회권한</h3>
		</div>
		   <div id="sub_default">
		   	   
		   <h4>의결권</h4>
			   <p class="round_txt">조례의 제정 및 개폐, 예산의 심의 확정, 결산의 승인</p>
			   <ul class="list_bul2">
					<li>법령에 규정된 것을 제외한 사용료, 수수료, 분담금, 지방세 또는 가입금의 부과와 징수</li>
					<li>기금의 설치 운용, 중요재산의 취득 · 처분</li>
					<li>공공시설의 설치, 관리 및 처분, 청원의 수리와 처리</li>
					<li>법령과 조례에 규정된 것을 제외한 예산의 업무분담이나 권리의 포기</li>
				</ul>
			 <h4>행정사무감사 · 조사권</h4>  
			   
			<ul class="list_bul2">
					<li>지방자치단체 사무에 대하여 실시 </li>
					<li>필요하면  현장 확인, 서류 제출, 시장 또는 관계 공무원 출석 증언, 의견  진술을 요구</li>
					<li>행정사무감사는 정례회 기간 중 선택적으로 7일 이내 실시</li>
					<li>본회의 의결로 본회의 또는 위원회가 조사</li>
				</ul>
				<h4>자율권</h4> 
			   <ul class="list_bul2">
					<li>헌법 및 법률에 저촉되지 아니하는 범위내에서 외부기관 등으로부터 간섭을 받지 아니하고 스스로를 규율하는 권한임</li>
					<li>내부조직권으로 의장단 구성, 위원회 설치</li>
					<li>의사 자율권으로 회의 규칙의 제정, 회의 소집, 회기 결정 등</li>
				</ul>
			  <h4>청원 처리권</h4>  
			  <ul class="list_bul2">
					<li>주민으로부터 제출되는 청원이나 진정의 수리</li>
					<li>의견서를 채택하여 집행기관에 송부 및 처리 결과 회신</li>
					<li>문서로 제출하여야 하며 1인 이상의 소개의원이 있어야 함</li>
				</ul>
		     </div>
		   
		   
			</div>
			
         </div>
	 </div>
	</div>
	<!-- footer -->
	<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
	<!-- //footer -->
		
		
	</div>
	
	

</body>
</html>
