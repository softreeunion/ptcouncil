<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<div id="subContent">
		<!-- header -->
		<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
		<!-- //header -->


		<!-- 메인메뉴 end -->
		<!-- submenu -->
		<jsp:include page="/WEB-INF/views/coun/intro/intro_left.jsp" flush="true"/>
		<!-- sbumenu end -->
		<h3 class="skip">본문내용 he</h3>
		<div id="content">
			<div id="subTitle">
				<h3>의회연혁</h3>
			</div>
			<div id="sub_default">
				<div class="history01">
					<h2><span>HISTORY</span> 평택시 의회의 주요 연혁을 보실 수 있습니다</h2>
				</div>
				<ul class="history_d">
					<li><strong class="histrory_tit">제 1대</strong>
						<ul class="h_list">
							<li ><p>1991.04.15 ~ 1995.06.30</p>
								<ul class="dot3">
									<li><strong>1952.04.25</strong> 초대 읍 · 면의원 선거</li>
									<li><strong>1956.08.08</strong> 제 2대 읍 · 면의원 선거</li>
									<li><strong>1960.12.26</strong>제 3대 읍 · 면의원 선거</li>
									<li><strong>1961.05.16</strong> 읍 · 면의회 해산 </li>
									<li><strong>1991.03.26</strong> 초대의원 선거(평택시 7 · 송탄시 8 · 평택군 10) </li>
									<li><strong>1991.04.15</strong>3개시 · 군 의회 개원</li>
								</ul>

							</li>
						</ul>

					</li>
				</ul>

				<ul class="history_d">
					<li><strong class="histrory_tit">제 2대</strong>
						<ul class="h_list">
							<li ><p>1995.07.01 ~ 1998.06.30</p>
								<ul class="dot3">
									<li><strong>1995.05.10</strong>
										3개 시군의회 통합(23개 선거구 29명)
										<span>초대 의장단 · 상임위원장 선출</span>

									</li>
									<li><strong>1956.08.08</strong> 제 2대 읍 · 면의원 선거</li>
									<li><strong>1995.06.27</strong>제 2대 평택시의회 개원(23개 선거구 29명)</li>
									<li><strong>1995.07.01</strong> 제 2대 평택시의회 개원 </li>
									<li><strong>1995.07.07</strong> 제 2대 전반기 · 상임위원장 선출 </li>
									<li><strong>1996.12.31</strong>제 2대 후반기 의장단, 상임위원장 선출</li>
								</ul>

							</li>
						</ul>

					</li>
				</ul>

				<ul class="history_d">
					<li><strong class="histrory_tit">제 3대</strong>
						<ul class="h_list">
							<li ><p>1998.07.01 ~ 2002.06.30</p>
								<ul class="dot3">
									<li><strong>1998.06.04</strong>제 3대 평택시의회 의원 선거 (21개 선거구 21명) </li>
									<li><strong>1998.07.01</strong> 제 3대 평택시의회 개원</li>
									<li><strong>1998.07.07</strong>제 3대 전반기 의장단 선출 · 상임위원장 선출</li>
									<li><strong>1998.11.24</strong>제 3대 전반기 의장 · 상임건설위원장 보궐선거 </li>
									<li><strong>2000.06.29</strong>제 3대 후반기 의장단, 상임위원장 선출  </li>
									<li><strong>2001.05.23</strong>제 3대 후반기 산업건설위원장 보궐선거</li>
									<li><strong>2001.11.01</strong>제 3대 후반기 의장 보궐선거</li>
								</ul>

							</li>
						</ul>

					</li>
				</ul>

				<ul class="history_d">
					<li><strong class="histrory_tit">제 4대</strong>
						<ul class="h_list">
							<li ><p>2002.07.01 ~ 2006.06.30</p>
								<ul class="dot3">
									<li><strong>2002.06.13</strong> 제 4대 평택시의회 의원 선거(21개 선거구 21명) </li>
									<li><strong>2002.07.01</strong> 제 4대 평택시의회 개원</li>
									<li><strong>2002.07.08</strong>제 4대 전반기 의장단 선출</li>
									<li><strong>2002.07.09</strong>제 4대 전반기 상임위원장 선출 </li>
									<li><strong>2004.04.15</strong>서정동 선거구 보궐선거 1명 당선(21개 선거구 21명) </li>
									<li><strong>2004.07.05</strong>제 4대 후반기 의장단, 상임위원장 선출</li>
								</ul>

							</li>
						</ul>

					</li>
				</ul>


				<ul class="history_d">
					<li><strong class="histrory_tit">제 5대</strong>
						<ul class="h_list">
							<li ><p>2006.07.01 ~ 2010.06.30</p>
								<ul class="dot3">
									<li><strong>2006.05.31</strong> 제 5대 평택시의회 의원선거(6개 선거구 16명)</li>
									<li><strong>2006.07.01</strong> 제 5대 평택시의회 개원</li>
									<li><strong>2006.07.12</strong>제 5대 전반기 의장단 선출</li>
									<li><strong>2006.07.13</strong> 제 5대 전반기 상임위원장 선출 </li>
									<li><strong>2008.06.30</strong> 제 5대 후반기 의장단, 상임위원장 선출 </li>
								</ul>

							</li>
						</ul>

					</li>
				</ul>


				<ul class="history_d">
					<li><strong class="histrory_tit">제 6대</strong>
						<ul class="h_list">
							<li ><p>2010.07.01 ~ 2014.06.30</p>
								<ul class="dot3">
									<li><strong>1995.05.10</strong>
										제 6대 평택시의회 의원선거
										<span>(6개 선거구 지역구 14명, 비례대표 2명)</span>

									</li>
									<li><strong>1956.08.08</strong>제 6대 평택시의회 개원</li>
									<li><strong>2010.07.01</strong>제 6대 전반기 의장선출</li>
									<li><strong>2010.07.07</strong> 제 2대 평택시의회 개원 </li>
									<li><strong>2010.09.03</strong>제 6대 전반기 상임위원장 선출</li>
									<li><strong>2012.06.28</strong>제 6대 후반기 의장단, 상임위원장 선출</li>
								</ul>

							</li>
						</ul>

					</li>
				</ul>


				<ul class="history_d">
					<li><strong class="histrory_tit">제 7대</strong>
						<ul class="h_list">
							<li ><p>2014.07.01 ~ 2018.06.30</p>
								<ul class="dot3">
									<li><strong>2014.06.04</strong>
										제 7대 평택시의회 의원선거
										<span>(6개 선거구 지역구 14명, 비례대표 2명)</span>

									</li>
									<li><strong>2014.07.01</strong> 제 7대 평택시의회 개원</li>
									<li><strong>2014.07.08</strong> 제 7대 전반기 의장선출 </li>
									<li><strong>2014.07.15</strong>제 7대 전반기 부의장 선출, 제 7대 전반기 상임위원장 선출 </li>
									<li><strong>2015.02.12</strong>「공직선거법」 제265조에 따른 당선무효1명</li>
									<li><strong>2015.04.29</strong>다 선거구 보궐선거 1명 당선
										<span>(6개 선거구 지역구 14명, 비례대표 2명)</span></li>
									<li><strong>2016.07.08</strong>제7대 후반기 의장단, 상임위원장 선출 </li>
								</ul>


							</li>
						</ul>

					</li>
				</ul>


				<ul class="history_d">
					<li><strong class="histrory_tit">제 8대</strong>
						<ul class="h_list">
							<li ><p>2018.07.01 ~ 2022.06.30</p>
								<ul class="dot3">
									<li><strong>2018.06.13</strong>
										제 8대 평택시의회 의원선거
										<span>(6개 선거구 지역구 14명, 비례대표 2명)</span>

									</li>
									<li><strong>2018.07.02</strong> 제 8대 전반기 의장, 부의장 선출</li>
									<li><strong>2018.07.02</strong>제 8대 평택시의회 개원</li>
									<li><strong>2018.07.03</strong> 제 8대 전반기 상임위원장 선출 </li>
									<li><strong>2020.02.13</strong> 「공직선거법」 제264조에 따른 당선무효1명 </li>
									<li><strong>2020.04.15</strong>나 선거구 재선거 1명 당선<span>(6개 선거구 지역구 14명, 비례대표 2명)</span></li>
									<li><strong>2020.06.26</strong> 제8대 후반기 의장단, 상임위원장 선출 </li>
								</ul>

							</li>
						</ul>

					</li>
				</ul>

			</div>
		</div>
	</div>
	<!-- footer -->
	<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
	<!-- //footer -->


</div>



</body>
</html>
