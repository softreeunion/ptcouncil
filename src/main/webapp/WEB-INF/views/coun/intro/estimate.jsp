<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Pragma" content="no-cache">
<meta property="og:image" content=""/>
<meta name="keywords" content="평택시의회" />
<meta name="description" content="평택시의회 홈페이지입니다." />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><%= webTitle %></title>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="canonical" href="http://www.ptcouncil.go.kr">
<!-- css -->

<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
<!-- sub contentcss -->
<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
<script type="text/javascript" src="/js/slick.min.js"></script>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
    <!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->

	
	<!-- 메인메뉴 end -->
	<div id="subContent">
	<!-- submenu -->
	<jsp:include page="/WEB-INF/views/coun/intro/intro_left.jsp" flush="true"/>
	<!-- sbumenu end -->
<h3 class="skip">본문내용 he</h3>
	   <div id="content">
		<div id="subTitle">
			<h3>예산 및 결산</h3>
		</div>
		   <div id="sub_default">
		   	   
		   <h4>예산이란?</h4>
			   <p class="round_txt">예산이란 평택시의 한 해 동안 살림 규모라 할 수 있습니다. 예산안은 시장이 편성하고, 의회에서 심의 확정하며, 결산은 시장이 예산을 집행한 후 의회의 승인을 받도록 하고 있습니다. 시민들이 내는 세금이 한 푼도 낭비되지 않도록 하기 위하여 엄격한 심의ㆍ의결 절차를 거치고 있습니다.</p>
			 <h4>예산안 처리 절차</h4>  
			 <div class='dl_style01'>
					<dl>
						<dt><dl>
							<dt>01</dt>
							<dd>예산안 편성제출<br/>(시장)</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>회계연도 개시 40일 전까지 의회에 제출</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>02</dt>
							<dd>제안설명<br/>(본회의:시장)</dd>
						</dl></dt>
					</dl><dl>
						<dt><dl>
							<dt>03</dt>
							<dd>예비심사<br/>(소관 상임위원회)</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>의사 담당보고 / 제출, 발의 건 제안설명</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>04</dt>
							<dd>종합심사<br/>(예산결산특별위원회)</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>의회는 시장의 동의 없이 지출예산 각 항의 금액을 증액하거나 새 비목을 설치할 수 없다. 시장은 예산안을 제출한 후 부득이한 사유로 그 내용의 일부를 수정하고자 할 때는 수정예산안을 작성하여 제출한다.</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>05</dt>
							<dd>심의의결<br/>(본회의)</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>회계연도 개시 10일 전까지 의결 의장은 의결된 예산안을 3일 이내에 시장에게 이송</li>
						</ul></dd>
					</dl>
				</div>
			   <h4>결산 승인과정</h4>
			   <div class='dl_style03'>
					<dl>
						<dt><dl>
							<dt>01</dt>
							<dd>승인요구(시장)</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>단체장은 출납 폐쇄 후 80일 이내에 결산서 및 증빙서류 작성</li>
							<li>의회가 선임한 결산 검사 위원의 검사의견서 첨부 후 다음 회계연도 개시 6월 말까지 시의회에 결산서 제시</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>02</dt>
							<dd>예비심사<br/>(상임위원회)</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>예산이 의회 승인대로 적법, 타당하게 집행되었는지 확인</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>03</dt>
							<dd>종합심사<br/>(예산결산특별위원회)</dd>
						</dl></dt>
					</dl><dl>
						<dt><dl>
							<dt>04</dt>
							<dd>심의의결승인<br/>(본회의)</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>결산 승인은 한 회계연도의 수입과 지출에 대한 실적을 사후통제 받는 것으로 의회의 승인서를 거쳐야 한다. 이미 집행된 예산은 무효 또는 취소시킬 수는 없으나 장래에 있어서 시 재정계획의 효율적 운영을 위한 참고자료로 활용한다.</li>
						</ul></dd>
					</dl>
				</div>
			</div>
			
         </div>
	 </div>
	</div>
	<!-- footer -->
	<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
	<!-- //footer -->
		
		
	</div>
	
	

</body>
</html>
