<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Pragma" content="no-cache">
<meta property="og:image" content=""/>
<meta name="keywords" content="평택시의회" />
<meta name="description" content="평택시의회 홈페이지입니다." />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><%= webTitle %></title>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="canonical" href="http://www.ptcouncil.go.kr">
<!-- css -->

<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
<!-- sub contentcss -->
<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
<script type="text/javascript" src="/js/slick.min.js"></script>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
    <!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->

	
	<!-- 메인메뉴 end -->
	<div id="subContent">
	<!-- submenu -->
	<jsp:include page="/WEB-INF/views/coun/intro/intro_left.jsp" flush="true"/>
	<!-- sbumenu end -->
<h3 class="skip">본문내용 he</h3>
	   <div id="content">
		<div id="subTitle">
			<h3>의회소집 및 운영</h3>
		</div>
		   <div id="sub_default">
		   	   
		   <h4>의회의 소집과 회기</h4>
			   <p class="round_txt"><strong>의회의 소집과 회기정례회</strong>는 매년 6월과 11월 중에 45일 이내로 집회하며, 임시회는 시장 또는 재적의원 1/3 이상의 요구로 매 회기 15일 이내로 소집되고 연간 회의일 수는 정례회와 임시회를 합쳐 100일을 초과할 수 없다.</p>
			   
			 <h4>본회의 운영</h4>
			   <p class="round_txt"><strong>지방의회</strong>는 연중 계속 활동하는 것이 아니라, 일정한 기간을 정하여 활동하게 되며 의회 고유의 기능을 행사하기 위하여 일정한 장소에 모이는 행위를 집회라 한다.<br/>
					회기는 의회의 의결로서 결정하고, 정례회는 매년 6월 1일과 11월 19일에 개의하여 45일 이내의 회기로 운영되며 임시회는 55일 이내로 열리게 된다.<br/>
			   따라서 연간 총 회의일수는 임시회와 정례회를 합하여 100일을 초과하지 않는 범위 내에서 회의를 할 수 있다.</p>
			   <p class="round_txt">
				   <strong>임시회의 경우</strong> 지방자치 단체장이나 재적의원 1/3 이상의 요구가 있을 경우 의회 의장은 요구일로부터 15일 이내에 소집하여야 하며, 이와 같이 임시회의 집회 요구가 있을 경우 의장은 집회일 3일 전에 집회공고를 하고, 이를 전 의원에게 통지함으로써 집회가 이루어지게 된다.</p>
			   <p class="round_txt">
				   <strong>정례회</strong>는 별도의 집회요구 없이 공고 절차만 거쳐 집회하게 된다. 집회와 동시에 의회의 의정활동이 시작되는데 이러한 의정활동을 할 수 있는 기간을 『회기』라 한다.</p>
			   <p class="round_txt">
				   <strong> 회의 시작 전에</strong> 개의일시 및 부의안건과 그 순서를 기재한 당일의 의사 일정과 관계 의안 자료 등을 미리 의석에 배부하고 의사정족수인 재적의원 1/3 이상이 입장하면 회의를 시작한다. 본회의 개의에 앞서 개회식을 하는데 개회식에는 통상 시장과 집행부 간부공무원이 참석한다.</p>
			   <p class="round_txt">
				   <strong>개회식이 끝나면</strong> 개의 선포 후 의안의 발의 등 의원이 회의하는데 필요한 사항을 먼저 보고한 후 의사 일정에 기재된 순서대로 안건을 상정하여 처리한다.
		       집회 첫날에는 개회식과 회기 결정 및 회의록 서명 의원 선출 등 비교적 간단한 사항을 관례로 처리하고 있다.</p>
			   <p class="round_txt">
				   <strong>본회의에서의 의안 처리 절차를 살펴보면 </strong>안건을 상정하고 제안자의 제안설명이나 예비 심사한 위원회의 심사보고를 들은 후 질의 답변과 토론을 거쳐 표결을 의결한다.</p>
			   <p class="round_txt">
				   <strong>질의가 있을 경우</strong>에는 그 안건을 심사한 위원회의 위원장이나 심사 보고한 의원이 보충 설명을 하며 제안설명일 경우에는 제안의원이나 제안 설명한 의원이 답변하고 토론은 반대, 찬성순으로 진행하며 토론이 끝나면 토론 종결과 함께 표결할 것을 선포한다.</p>
			   <p class="round_txt">
				   <strong>표결은</strong> 안건에 대한 찬반 의견을 파악하여 가부를 결정하는 안건 심의의 최종 단계로서 그 방법은 만장일치, 기립, 무기명, 기명투표 등이 있는데 반대토론이 있을 경우 대부분의 일반 안건은 기립표결로 처리하는 경우가 많으며, 질의나 찬반 토론이 없을 경우에는 이의 유무 형식인 만장일 치식 방법을 취하고 있다.</p>
			   <p class="round_txt">
				   <strong>표결 방법에 대하여</strong> 의장 제의 또는 의원의 동의로 본회의의 의결이 있을 때는 그 의결된 방법으로 진행할 수도 있으며, 의회에서 실시하는 각종 선거는 특별한 규정이 없는 한 무기명 투표로 한다. 특정한 사항을 제외하고는 일반 안건의 표결은 재적의원 과반수 출석과 출석의원 과반수 찬성으로 의결하는데 의장도 표결권을 가지며 표결 결과가 가부 동수일 경우에는 부결된 것으로 본다. 당일의 의사 일정 일을 모두 마쳤을 때는 다음 회의일시 등을 공지하고 산회를 선포한다. 
		       </p>
			   <h4>정례회</h4>
			   <div class="table_wrap"><table class="board_n_list">
					<caption class='hide'>정례회에 대한 정보</caption>
					<colgroup>
						<col style='width:15%;'/>
						<col style='width:85%;'/>
					</colgroup>
					<tbody><tr>
						<th>집회</th>
						<td class='left'>매년 6월과 11월 중에 집회한다. 다만, 총선거가 실시되는 연도의 제 1차 정례회는 의회의 의결로 9월, 10월 중에 따로 정할 수 있다.</td>
					</tr><tr>
						<th>회기</th>
						<td class='left'>45일 이내</td>
					</tr><tr>
						<th>주요활동</th>
						<td class='left'>결산서의 승인 (제1차 정례회), 행정사무감사 (제2차 정례회)<br/>예산안의 의결, 조례안 등 기타 안건을 처리함</td>
					</tr></tbody>
				</table></div>
			   
			   <h4>임시회</h4>
			   <div class="'"table_wrap"'"><table class="board_n_list">
					<caption class='hide'>임시회에 대한 정보</caption>
					<colgroup>
						<col style='width:15%;'/>
						<col style='width:85%;'/>
					</colgroup>
					<tbody><tr>
						<th>집회</th>
						<td class='left'>시장 또는 재적의원 1/3 이상의 요구 시 집회함(지방자치법 제39조)</td>
					</tr><tr>
						<th>회기</th>
						<td class='left'>55일 이내</td>
					</tr><tr>
						<th>주요활동</th>
						<td class='left'>주요 현안에 대한 집행부 측의 설명을 듣고 질의 답변을 함<br/>조례안, 추경안 등 계류된 안건을 심사함.</td>
					</tr></tbody>
				</table></div>
			   <h4> 특별위원회 운영</h4>
			   <p class="round_txt"><strong>특별위원회는</strong> 특정한 안건을 심사 처리하기 위하여 필요한 경우에 본회의의 의결로 설치 운영하고 있으며, 특별위원회는 그 위원회에서 심사한 안건이 본회의에서 의결될 때까지 존속하며, 임기는 위원회의 존속기간으로 한다.</p>
			   <h4>회의원칙</h4>
			   <div class="list_double">
			 		<ul >
						<li><b>회의 공개 원칙</b>
							<ul >
								<li>의회의 회의는 공개가 원칙임. 의안 심의 과정을 외부에 공개하는 것으로서 방청의 자유 회의의 기록, 공포, 보도의 자유 등이다.</li>
							</ul>
						</li>
						<li><b>회의 계속 원칙</b>
							<ul>
								<li>의회에 제출된 조례안 기타의 의안은 회기 중에 의결되지 못한 이유로 폐기되지 아니함. 의회가 회기 중에 한하여 활동할 수 있지만, 매회기 마다 독립된 별개의 의회로서 가 아니라 임기 중에 일체성을 갖는 의회로써 존재한다는 뜻임</li>
							</ul>
						</li>
						<li><b>일사부재의의 원칙</b>
							<ul>
								<li>부결된 안건은 같은 회기 중에 다시 발의 또는 제출하지 못함. 한 안건이 한번 의회에서 부결되면 그 회기에는 다시 동일 안건에 대하여 심의를 하지 않는 것을 뜻함.</li>
								<li>이미 결정된 의안에 관하여 동일회기 중에 거듭 발의 또는 심의하게 되면 회의의 원활한 운영을 방해한다는 것을 그 근거로 하며, 특히 소수파에 의한 의사 방해 배제가 주된 목적임.</li>
							</ul>
						</li>
					</ul>
			   </div>
			   <h4>정족수</h4>
			   <div class='list_double'>
					<ul>
						<li><b>의사정족수</b>
							<ul>
								<li>회의를 여는데 필요한 최소한의 출석의원 수를 말함. 본회의는 재적의원 1/3 이상의 출석으로 개의함 </li>
							</ul>
						</li>
						<li><b>의결정족수</b>
							<ul>
								<li>의결을 하는데 필요한 최소한의 출석의원 수를 말함.</li>
								<li><b>ㄱ) 일반정족수</b>
									<ul>
										<li style='padding-left:15px;'>특별한 규정이 없는 한 재적의원 과반수의 출석과 출석의원 과반수의 찬성으로 의결함, 가부 동수일 때에는 부결된 것으로 봄.</li>
									</ul>
								</li>
								<li><b>ㄴ) 특별정족수</b>
									<ul>
										<li style='padding-left:15px;'>일반정족수와 다른 규정이 있는 경우로서 지방자치법이 의결에 관하여 특별정족수를 규정한 예로써 다음의 경우가 있음.
											<ul>
												<li>- 재적의원 2/3 이상의 찬성 : 의원의 제명, 의원 자격 상실 결정</li>
												<li>- 재적의원 과반수 찬성 : 의장, 부의장 불신임 의결</li>
												<li>- 재적의원 과반수의 출석과 출석의원 2/3 이상의 찬성 : 조례안의 재의결</li>
											</ul>
										</li>
									</ul>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			    <h4>발언제도</h4>
			   <div class='list_double'>
					<ul>
						<li><b>시정 질문</b>
							<ul>
								<li>본회의에서 회기 중 기간을 정하여 시정 전반 또는 시정의 특정 분야를 대상으로 집행부에 대하여 질문을 하는 것으로 질문과 답변이 포함된 일문일답 방식(40분)과, 질문자의 본 질문 20분, 보충 질문 10분의 일괄질문 일괄답변 방식으로 운영되며 질문요지서는 시정 질문 시작 4일 전, 답변은 1일 전까지 제출하여야 함.</li>
							</ul>
						</li>
						<li><b>의사진행발언</b>
							<ul>
								<li>회의 진행 과정에서 회의 진행 방법 등에 대하여 제기하거나 자기 의견을 개진하기 위한 발언임. 의사 진행에 관한 발언은 그 범위가 넓고 고의로 의사 진행을 방해하는 경우가 있음으로 발언 요지를 의장에게 미리 통지하고 허락을 받아야 함.</li>
							</ul>
						</li>
						<li><b>신상 발언</b>
							<ul>
								<li>의원 일상사에 관한 문제가 생긴 경우에 본인이 해명하는 발언임. 자격심사의 피 심리원 윤리 검사 대상자 징계대상 의원으로서 변명할 때에는 그 안건이 의제가 되었을 때 발언할 수 있음.</li>
							</ul>
						</li>
					</ul>
				</div>
			   <h4>표걸제도</h4>
		     <div class='list_double'>
					<ul>
						<li><b>표결의 의의</b>
							<ul>
								<li>의장의 요구에 의하여 의원이 안건에 대한 찬성과 반대의 의사를 표명하고 그 수를 집계하는 것임.</li>
							</ul>
						</li>
						<li><b>표결의 기본원칙</b>
							<ul>
								<li><b>ㄱ) 부재 의원의 표결 불참가</b>
									<ul>
										<li style='padding-left:15px;'>회의장에 있지 아니한 의원은 표결에 참가할 수 없음.</li>
									</ul>
								</li>
								<li><b>ㄴ) 의사변경의 금지</b>
									<ul>
										<li style='padding-left:15px;'>표결에 있어서 표시한 의사를 변경할 수 없음.</li>
									</ul>
								</li>
								<li><b>ㄷ) 조건부표결의 금지</b>
									<ul>
										<li style='padding-left:15px;'>표결은 토론과 달라서 찬반의 이유를 필요로 하지 아니하고 가부 어느 한쪽에 의사를 표시하는 것이므로 이에 조건을 붙여서는 안 됨.</li>
									</ul>
								</li>
							</ul>
						</li>
						<li><b>중요안건의 표결 방법</b>
							<ul>
								<li>중요한 안건으로 의장의 제의 또는 의원의 동의로 본회의의 의결이 있을 때에는 기명 또는 무기명 투표로 표결함.</li>
								<li><b>ㄱ) 전자 투표</b>
									<ul>
										<li style='padding-left:15px;'>전자 투표에 의한 기록 표결로 안건에 대한 가부를 결정</li>
									</ul>
								</li>
								<li><b>ㄴ) 기명투표</b>
									<ul>
										<li style='padding-left:15px;'>투표용지에 안건에 대한 가부와 의원의 성명을 기재함.</li>
									</ul>
								</li>
								<li><b>ㄷ) 무기명투표</b>
									<ul>
										<li style='padding-left:15px;'>투표용지에 안건에 가부만 기재하고 의원의 성명을 쓰지 아니함.</li>
									</ul>
								</li>
							</ul>
						</li>
						<li><b>이의 유무 확인</b>
							<ul>
								<li>출석의원 전원이 찬성하는 경우 의장이 안건에 대한 이의의 유무를 물어서 이의가 없다고 인정한 때에는 가결되었음을 선포할 수 있음.</li>
								<li>표결에 부치는 문제가 간단하고 경미한 경우와 의사 진행에 관한 동의 및 안건에 대해서 반대자가 없다고 인정되는 경우 등 의사 운영의 신속성과 표결의 명확성을 기하기 위하여 활용됨.</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
			
         </div>
	 </div>
	</div>
	<!-- footer -->
	<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
	<!-- //footer -->
		
		
	</div>
	
	

</body>
</html>
