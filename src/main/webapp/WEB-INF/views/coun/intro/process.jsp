<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Pragma" content="no-cache">
<meta property="og:image" content=""/>
<meta name="keywords" content="평택시의회" />
<meta name="description" content="평택시의회 홈페이지입니다." />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><%= webTitle %></title>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="canonical" href="http://www.ptcouncil.go.kr">
<!-- css -->

<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
<!-- sub contentcss -->
<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
<script type="text/javascript" src="/js/slick.min.js"></script>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
<div id="subContent">
    <!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->

	
	<!-- 메인메뉴 end -->
	<!-- submenu -->
	<jsp:include page="/WEB-INF/views/coun/intro/intro_left.jsp" flush="true"/>
	<!-- sbumenu end -->
<h3 class="skip">본문내용 he</h3>
	   <div id="content">
		<div id="subTitle">
			<h3>의안처리절차</h3>
		</div>
		   <div id="sub_default">
		   	   
		   <h4>의안처리 절차</h4>
			   <p class="round_txt">지방의회의 의결을 필요로 하는 많은 사항 즉, 안건 중에서 특별한 형식적 요건을 구비하여 <strong>의원이나 지방자치단체의 장이 제출하는 것을「의안(議案)」이라 한다.</strong> 의안은 안을 갖추어 서면으로 제출되어야 하며, 안에 대해서 수정이 가능하고 의원, 지방자치단체의 장, 위원회에서 제출하여야 하며, 상임위원회가 구성되어 있는 경우에는 상임위원회의 심사를 거쳐야 하는 것을 말하나 모든 의안이 위의 요건을 갖추고 있는 것은 아니다. 예를 들면, 조례안, 회의 규칙안, 예산안 및 예산 각종 건의안 및 결의안 동의안(승인안) 등을 &ldquo;의안(議案)&rdquo;이라고 한다.</p>
			 <h4>의회의 소집과 회기</h4>   
			 <div class='dl_style01'>
					<dl>
						<dt><dl>
							<dt>01</dt>
							<dd>제출 발의</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>자치단체장 제출, 의원 1/5 이상 발의</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>02</dt>
							<dd>접수</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>의장</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>03</dt>
							<dd>본회의상정(회부)</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>의사 담당보고 / 제출, 발의 건 제안설명</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>04</dt>
							<dd>위원회심사<br/>(결과 보고)</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>제안설명, 검토보고(전문위원) / 질의, 토론 의결</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>05</dt>
							<dd>본회의의결(이송)</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>심사보고 질의, 토론 의결</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>06</dt>
							<dd>집행기관</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>조례안 5일 이내 / 예산안 3일 이내</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>07</dt>
							<dd>공표 또는 재의요구</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>20일 이내</li>
						</ul></dd>
					</dl>
				</div>
			</div>
			
         </div>
	 </div>
	</div>
	<!-- footer -->
	<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
	<!-- //footer -->
		
		
	</div>
	
	

</body>
</html>
