<%
////////////////////////////////////////////////////////////
// Program ID  : historys
// Description : 의회소개 > 열린의장실 - 의장약력
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "의장약력";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb01_01").addClass("on").removeClass("noChild");
		$("#snb01_01_02").addClass("on");
	});
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/coun/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/coun/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="1"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 의회소개 &gt; 열린의장실 &gt;  <%= subTitle %></div>
			<div class='greetings'>
				<div class='img'><img src='<%= webBase %>/images/coun/img_greetings.png' alt='의장 이미지'/></div>
				<div class='txt'>
					<h2>평택시의회 의장<span>권영화</span></h2>
					<div class='historys_style'>
						<ul>
							<li class='first'>경성종합 고등학교 졸업</li>
							<li>(전)평택농협이사</li>
							<li>(현)민주당 농업발전 특별위원장</li>
							<li>(전)평택초등학교 제2대 총동문회장</li>
							<li>(전)평택초등학교 운영위원장</li>
							<li>(전)군문초등학교 운영위원장</li>
							<li>(전)제6대 평택시의회 의원</li>
							<li>(전)제6대 평택시의회 후반기 자치행정위원장</li>
							<li>(전)제7대 평택시의회 의원</li>
							<li>(전)제7대 평택시의회 의원</li>
							<li>(전)제7대 평택시의회 후반기 운영위원장</li>
							<li>(현)제8대 평택시의회 의원</li>
							<li>(현)제8대 평택시의회 전반기 의장</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer.jsp" flush="true"><jsp:param name="qk" value="F"/></jsp:include>
	<!-- //footer -->
</div>
</form>
</body>
</html>