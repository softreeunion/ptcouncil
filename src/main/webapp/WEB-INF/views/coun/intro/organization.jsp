<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Pragma" content="no-cache">
<meta property="og:image" content=""/>
<meta name="keywords" content="평택시의회" />
<meta name="description" content="평택시의회 홈페이지입니다." />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><%= webTitle %></title>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="canonical" href="http://www.ptcouncil.go.kr">
<!-- css -->

<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
<!-- sub contentcss -->
<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
<script type="text/javascript" src="/js/slick.min.js"></script>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
    <!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->

	
	<!-- 메인메뉴 end -->
	<div id="subContent">
	<!-- submenu -->
	<jsp:include page="/WEB-INF/views/coun/intro/intro_left.jsp" flush="true"/>
	<!-- sbumenu end -->
		<h3 class="skip">본문내용 he</h3>
	   <div id="content">
		<div id="subTitle">
			<h3>조직 및 구성&nbsp;</h3>
		</div>
		   <div id="sub_default">
		   <div class="orgin01">
		     <h4>의원 정수 - 18명</h4>
		     <ul class="bul1">
			   <li>의장, 부의장</li>
				<li>상임위원회 구성 : 의회운영위원회, 기획행정위원회, 복지환경위원회, 산업건설위원회,예산결산특별위원회, 윤리특별위원회</li>
			   </ul>
		     <h4>의회조직</h4>
<ul class="org">
<li class="org1"><span>의 장</span>
<ul>
<li class="org2"><span>부의장</span>
<ul>
<li class="org3 org3_1"><span>상임위원회</span>
<ul>
<li class="org4 org4_1"><span>의회운영<br>위원회</span></li>
<li class="org4 org4_2"><span>기획경제<br>위원회</span></li>
<li class="org4 org4_3"><span>복지환경<br>위원회</span></li>
<li class="org4 org4_4"><span>산업건설<br>위원회</span></li>
</ul>
</li>
<li class="org3 org3_2"><span>특별위원회</span>
<ul>
<li class="org5 org5_1"><span>예산결산<br>특별위원회</span></li>
<li class="org5 org5_2"><span>윤리<br>특별위원회</span></li>
</ul>
</li>
</ul>
</li>
</ul>
</li>
</ul>
			   
			   <h4>의회사무국</h4>
			   <ul class="org org-2">
<li class="org1"><span>사무국장</span>
<ul>
<ul>
<li class="org3 org3_1"><span>의정팀</span></li>
<li class="org3 org3_2"><span>의사팀</span></li>
<li class="org3 org3_3"><span>홍보팀</span></li>
<li class="org3 org3_4"><span>정책지원팀</span></li>
<li class="org3 org3_5"><span>관리팀</span></li>
</ul>
<li class="org2 org2_1"><span>전문위원</span></li>
</ul>
</li>
</ul>
		   
		   <h4>사무국 정·현원</h4>
		   <div class="table_wrap">
<table class="board_n_list"><caption>사무국 정·현원을 확인할 수 있습니다</caption><colgroup> <col width="10%"> <col width="10%"> <col width="10%"> <col width="10%"> <col width="10%"> </colgroup>
<thead>
<tr>
  <th scope="col">구분</th>
<th scope="col">계</th>
<th scope="col">4급</th>
<th scope="col">5급</th>
<th scope="col">6급</th>
<th scope="col">7급</th>
<th scope="col">8급</th>
<th scope="col">9급</th>
<th scope="col">비고</th>
</tr>
</thead>
<tbody>
<tr>
  <td>정원</td>
<td>40</td>
<td>1</td>
<td>2</td>
<td>9</td>
<td>16</td>
<td>9</td>
<td>3</td>
<td class="bdL" rowspan="2">공무직 2<br>
  청원경찰 1<br>
  휴직 3<br>
  시간선택제<br>
  임기제 1</td>
</tr>
<tr>
  <td>현원</td>
<td>38</td>
<td>1</td>
<td>2</td>
<td>9</td>
<td>14</td>
<td>6<br>
  (임기제2)</td>
<td>6</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
</div>
		  
			   </div>
			   
			
         </div>
	 </div>
	</div>
	<!-- footer -->
	<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
	<!-- //footer -->
		
		
	</div>
	
	

</body>
</html>
