<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Pragma" content="no-cache">
<meta property="og:image" content=""/>
<meta name="keywords" content="평택시의회" />
<meta name="description" content="평택시의회 홈페이지입니다." />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><%= webTitle %></title>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="canonical" href="http://www.ptcouncil.go.kr">
<!-- css -->

<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
<!-- sub contentcss -->
<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
<script type="text/javascript" src="/js/slick.min.js"></script>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
    <!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->

	
	<!-- 메인메뉴 end -->
	<div id="subContent">
	<!-- submenu -->
	<jsp:include page="/WEB-INF/views/coun/intro/intro_left.jsp" flush="true"/>
	<!-- sbumenu end -->
<h3 class="skip">본문내용 he</h3>
	   <div id="content">
		<div id="subTitle">
			<h3>행정사무감사/조사</h3>
		</div>
		   <div id="sub_default">
		   	   
		   <h4>행정사무감사 / 조사의 절차 및 의안처리 절차</h4>
			   
			 <div class='dl_styleL'>
					<dl>
						<dt><dl>
							<dt>01</dt>
							<dd>시정 및 처리 결과 보고<br/>(행정감사시기 및 기간결정)</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>감사시기, 대상기관 / 감사자료요구사항</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>02</dt>
							<dd>행정사무감사계획서 본회의 승인</dd>
						</dl></dt>
					</dl><dl>
						<dt><dl>
							<dt>03</dt>
							<dd>감사계획서 이송</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>집행부</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>04</dt>
							<dd>행정사무감사 실시</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>감사 선언 및 인사</li>
							<li>증인선서</li>
							<li>보고 및 질의답변</li>
							<li>감사 결과 강평 및 감사 종료선언</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>05</dt>
							<dd>행정사무감사 결과보고서 채택<br/>(행정사무감사 특별위원회)</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>감사목적, 감사기관, 감사실시내용, 감사 결과 및 처리내용, 기타 감사의견 및 특기 사항 주요 근거 서류</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>06</dt>
							<dd>행정사무감사결과 본회의 채택<br/>(시정 및 처리요구사항 채택)</dd>
						</dl></dt>
					</dl><dl>
						<dt><dl>
							<dt>07</dt>
							<dd>집행부 이송</dd>
						</dl></dt>
					</dl><dl>
						<dt><dl>
							<dt>08</dt>
							<dd>시정 및 처리 결과 보고<br/>(시 및 해당 기관으로부터 의회에<br/> 보고)</dd>
						</dl></dt>
					</dl><dl>
						<dt><dl>
							<dt>09</dt>
							<dd>시정 및 처리 결과 위원회 통보</dd>
						</dl></dt>
					</dl>
				</div>
			</div>
			
         </div>
	 </div>
	</div>
	<!-- footer -->
	<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
	<!-- //footer -->
		
		
	</div>
	
	

</body>
</html>
