<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Pragma" content="no-cache">
<meta property="og:image" content=""/>
<meta name="keywords" content="평택시의회" />
<meta name="description" content="평택시의회 홈페이지입니다." />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><%= webTitle %></title>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="canonical" href="http://www.ptcouncil.go.kr">
<!-- css -->

<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
<!-- sub contentcss -->
<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
<script type="text/javascript" src="/js/slick.min.js"></script>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
    <!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->

	
	<!-- 메인메뉴 end -->
	<div id="subContent">
	<!-- submenu -->
	<jsp:include page="/WEB-INF/views/coun/intro/intro_left.jsp" flush="true"/>
	<!-- sbumenu end -->
	<h3 class="skip">본문내용 he</h3>
	   <div id="content">
		<div id="subTitle">
			<h3>의회기능</h3>
		</div>
		   <div id="sub_default">
		   	   <h4>의회 기능은 이렇습니다.</h4>
			   <p class="round_txt">의회는 <strong>주민을 대표하는 의결기관</strong>으로서 예산, 결산안승인, 청원과 진정을 처리하고, 법령의 범위내에서 조례를 제정·개정·폐지하며 집행기관에 대하여 행정사무감사 및 조사, 동의, 승인 보고 등의 통제권을 부여하고 있습니다. 또한 조직과 운영에 있어서 회의 규칙 제정권, 회의의 개폐 및 회기결정권 등 의사 운영의 자율권과 의장·부의장 불신임권·내부조사권 등의 내부운영에 관한 자율권을 가집니다.</p>
		    <h4>조례의 제정 및 개폐</h4>
			   <p class="round_txt"><strong>조례라 함은 우리 시의 법률이라 할 수 있는 것이다.</strong> 내용에 따라서는 시민들의 권리를 제한하거나 의무를 부과하는 조례가 있는 반면 시민들의 복지향상과 부담을 덜어주는 조례도 있다.<br/>
다만, 시민의 권리 제한이나 의무 부과에 관한 사항, 또는 벌칙을 정할 때에 법률의 위임이 있어야 한다.
				   <strong>조례는 의회에 의결을 거치고 공포됨으로써 그 효력을 가지게 된다</strong></p>
			 
			 <div class='dl_style01'>
					<dl>
						<dt><dl>
							<dt>01</dt>
							<dd>발의</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>시장 - 재적의원 1/5 이상 또는 의원10인 이상의 연서</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>02</dt>
							<dd>심사</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>소관 상임위원회</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>03</dt>
							<dd>의결</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>본회의 상정 - 재적의원 과반수 출석과 출석의원 과반수 찬성</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>04</dt>
							<dd>이송</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>의결된 날로부터 5일 이내에 시장에게 이송</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>05</dt>
							<dd>공포</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>20일 이내에 시장이 공포 <br/>효력 발생은 특별한 규정이 없는 한 공포일로부터 20일 경과 후</li>
						</ul></dd>
					</dl>
				</div>
			   
			   <h4> 예산안 처리 절차</h4>
			    <p class="round_txt">예산안이란 한 해 동안 우리 시의 살림 규모라 할 수 있습니다. 예산안은 시장이 편성하고 의회에서 심의·확정 합니다.</p>
			   <div class='dl_style02'>
					<dl>
						<dt><dl>
							<dt>01</dt>
							<dd>예산안편성(시장)</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>회계년도개시 40일 전까지 의회에 제출</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>02</dt>
							<dd>예비심사<br/>(상임위원회)</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>소관 실국 소장이 제안설명</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>03</dt>
							<dd>종합심사<br/>(예산결산특별위원회)</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>의회는 시장의 동의 없이 지출예산 각항의 금액을 증액하거나 새 비목을 설치할 수 없다.</li>
							<li>시장은 예산안을 제출한 후 부득이한 사유로 그 내용의 일부를 수정하고자 할 때는 수정예산안을 작성하여 제출한다.</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>04</dt>
							<dd>심의의결(본회의)</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>회계연도 개시 10일 전까지 의결<br/>의장은 의결된 예산안을 3일 이내에 시장에게 이송</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>05</dt>
							<dd>확정</dd>
						</dl></dt>
					</dl>
				</div>
			   
			   <h4>결산안 처리 절차</h4>
			   <p class="round_txt">결산은 시장이 예산을 집행한 후 의회의 승인을 받도록 하고 있으며, <b>시민들이 내는 세금이 한 푼도 낭비되지 않도록 하기 위하여 엄격한 심의, 의결 절차를 거치고 있다. </p>
			   <div class='dl_style03'>
					<dl>
						<dt><dl>
							<dt>01</dt>
							<dd>승인요구(시장)</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>단체장은 출납 폐쇄 후 80일 이내에 결산서 및 증빙서류 작성</li>
							<li>의회가 선임한 결산 검사 위원의 검사의견서 첨부 후 시의회에 결산 승인 요구</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>02</dt>
							<dd>예비심사<br/>(상임위원회)</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>예산이 의회승인대로 적법, 타당하게 집행되었는지 확인</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>03</dt>
							<dd>종합심사<br/>(예산결산특별위원회)</dd>
						</dl></dt>
					</dl><dl>
						<dt><dl>
							<dt>04</dt>
							<dd>심의의결(본회의)</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>결산 승인은 한 회계연도의 수입과 지출에 대한 실적을 사후통제 받는 것으로 의회의 승인 절차를 거쳐야 완전한 것이 될 수 있다.</li>
							<li>이미 집행된 예산은 무효 또는 취소 시킬 수 없으나, 장래에 있어서 시 재정계획의 효율적 운영을 위한 참고 자료로 활용된다.</li>
						</ul></dd>
					</dl>
				</div>
			   
			   <h4 >청원의 심사처리</h4>
				<p class='round_txt'>청원은 <b>시민이 시정에 관한 희망이나 개선 사항을 시의회 의원의 소개를 받아 서면으로 제출하는 제도</b>입니다.</p>
				<div class='dl_style04'>
					<dl>
						<dt><dl>
							<dt>01</dt>
							<dd>청원서 제출</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>청원서에는 청원인의 성명 및 주소를 기재한 후 서명날인</li>
							<li>소개의원 서명 날인</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>02</dt>
							<dd>소관위원회 회부, 심사</dd>
						</dl></dt>
						<dd><ul class="bul1">
							<li>회부일로부터 10일 이내 심사</li>
							<li>본회의에 부의할 필요가 없다고 결정한 때, 그 처리 결과를 의장에게 보고하고 의장은 청원인에게 통지</li>
							<li>청원 소개의원은 소관위원회 또는 본회의의 요구가 있을 때 청원의 취지설명</li>
						</ul></dd>
					</dl><dl>
						<dt><dl>
							<dt>03</dt>
							<dd>본 회의 심사 의결을 거쳐 채택</dd>
						</dl></dt>
					</dl><dl>
						<dt><dl>
							<dt>04</dt>
							<dd>시장이 처리하여야 할 사항은 청원의견서를 첨부, 시장에게 이송 </dd>
						</dl></dt>
					</dl><dl>
						<dt><dl>
							<dt>05</dt>
							<dd>시장은 청원을 처리하고 그 결과를 지체없이 의회에서 보고</dd>
						</dl></dt>
					</dl><dl>
						<dt><dl>
							<dt>06</dt>
							<dd>의회는 처리 결과를 청원인에게 통보</dd>
						</dl></dt>
					</dl>
				</div>
		   
			   
			   
			</div>
				
		     </div>
		   
		 
			
         </div>
	 </div>
	</div>
	<!-- footer -->
	<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
	<!-- //footer -->
		
		
	</div>
	
	

</body>
</html>
