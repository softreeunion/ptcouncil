<%
////////////////////////////////////////////////////////////
// Program ID  : index
// Description : 시의회 INDEX
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	boolean viewPoster			= true;
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle %></title>
<%-- 20191219 --%>
<meta property="og:url" content="http://www.ptcouncil.go.kr" />
<link rel="canonical" href="http://www.ptcouncil.go.kr">
<%-- 20191219 --%>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg.jspf"/>
	<script>
	var fnActDetail     = function(view){
		doActLoadShow();
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"noticeView.do","method":"post","target":"_blank"}).submit();
	};
	$(document).ready(function(){
		$(".main_visual>ul").bxSlider({auto:true});
		//$("#main_popup1 .mlay_clbt").click(function(){if($("input[name='chkday1']").is(":checked"))setCookieMobile("toCook1","done",1);$("#main_popup1").hide();});
		//$("#main_popup2 .mlay_clbt").click(function(){if($("input[name='chkday2']").is(":checked"))setCookieMobile("toCook2","done",1);$("#main_popup2").hide();});
		//$("#main_popup3 .mlay_clbt").click(function(){if($("input[name='chkday3']").is(":checked"))setCookieMobile("toCook3","done",1);$("#main_popup3").hide();});
		//$("#main_popup4 .mlay_clbt").click(function(){if($("input[name='chkday4']").is(":checked"))setCookieMobile("toCook4","done",1);$("#main_popup4").hide();});

		//$(".mlay_clbt").click(function(){if(!$("#main_popup1").is(":visible"))$("#mask").hide();});
		//$(".mlay_clbt").click(function(){if(!$("#main_popup1").is(":visible")&&!$("#main_popup2").is(":visible"))$("#mask").hide();});
		//$(".mlay_clbt").click(function(){if(!$("#main_popup1").is(":visible")&&!$("#main_popup2").is(":visible")&&!$("#main_popup3").is(":visible"))$("#mask").hide();});
		//$(".mlay_clbt").click(function(){if(!$("#main_popup1").is(":visible")&&!$("#main_popup2").is(":visible")&&!$("#main_popup3").is(":visible")&&!$("#main_popup4").is(":visible"))$("#mask").hide();});
		//$(".mlay_clbt").click(function(){if(!$("#main_popup1").is(":visible")&&!$("#main_popup2").is(":visible"))$("#mask").hide();});

		//if(getCookieMobile("toCook1")==""){$("#main_popup1").show();$("#mask").show();};
		//if(getCookieMobile("toCook2")==""){$("#main_popup2").show();$("#mask").show();};
		//if(getCookieMobile("toCook3")==""){$("#main_popup3").show();$("#mask").show();};
		//if(getCookieMobile("toCook4")==""){$("#main_popup4").show();$("#mask").show();};
	});
	var fnMembFinder    = function(){
		doActLoadShow();
		var $form=$("#frmDetail");
		var  view=$("#searchMembText").val();
		if($form.length<=0){$form=$("<form/>").attr({id:"frmDetail",name:"frmDetail",method:"post"});$(document.body).append($form);}
		if($form.length<=1){$form.empty();}
		$("<input/>").attr({type:"hidden",id:"pageCurNo",name:"pageCurNo",value:"1"}).appendTo($form);
		$("<input/>").attr({type:"hidden",id:"reEraCode",name:"reEraCode",value:"<%= BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsCate" ) %>"}).appendTo($form);
		$("<input/>").attr({type:"hidden",id:"condType",name:"condType",value:"N"}).appendTo($form);
		$("<input/>").attr({type:"hidden",id:"condValue",name:"condValue",value:view}).appendTo($form);
		if($form.length<=1){$form.attr({"action":"<%= webBase %>/coun/memb/activeFinder.do","method":"post","target":"_self"}).submit();}
	};
	var fnActDetail     = function(view,acts){
		doActLoadShow();
		var $form=$("#frmDetail");
		if($form.length<=0){$form=$("<form/>").attr({id:"frmDetail",name:"frmDetail",method:"post"});$(document.body).append($form);}
		if($form.length<=1){$form.empty();}
		if(!isEmpty(view)){$("<input/>").attr({type:"hidden",id:"viewNo",name:"viewNo",value:view}).appendTo($form);}
		if($form.length<=1){$form.attr({"action":"<%= webBase %>/coun/"+acts+".do","method":"post","target":"_self"}).submit();}
	};<% if( movieLayer ) { %>
	var fnActMedia      = function(view,acts){
		$.ajax({
			type          : "post", 
			url           : "<%= webBase %>/coun/mediaView.do", 
			dataType      : "html", 
			data          : {viewNo:view}, 
			success       : function(data,status,xhr  ){var $LV=$("#layVideo");$LV.empty();$LV.html(data);media_layer();}, 
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);}
		}); 
	};<% } %>
	</script>
	<style type="text/css">
   .video {
      width: 100%; 
      height: 365px; 
      overflow: hidden; 
      display: flex; 
      justify-content: center; 
      align-items: center;
     }
   video[poster]{ 
      height:100%;
      width:100%;
    }
</style>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/coun/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<div class='main_visual' id='contents'><ul><c:if test="${!empty mainImage}"><c:forEach var="rult" varStatus="status" items="${mainImage}"><c:choose><c:when test="${'F' eq rult.bbsSiteType}">
			<li><% if( imgMode ) { %><img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/><% } else { %><img src='<%= webBase + uploadPath %>${rult.fileAbs}' alt='${rult.bbsTitle}'/><% } %></li></c:when><c:otherwise>
			<li><a href='${rult.bbsSiteUri}'<c:if test="${'N' eq rult.bbsSiteType}"> target='_blank'</c:if>><% if( imgMode ) { %><img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/><% } else { %><img src='<%= webBase + uploadPath %>${rult.fileAbs}' alt='${rult.bbsTitle}'/><% } %></a></li></c:otherwise></c:choose></c:forEach></c:if><%--
			<li><img src='<%= webBase %>/images/20191125_mm/1580294423412.jpg' alt='메인이미지'/></li>
			<% --<li><img src='<%= webBase %>/images/20191125_mm/1573818760991.jpg' alt='메인이미지'/></li>-- %> 
			<li><img src='<%= webBase %>/images/20191125_mm/1573455205713.jpg' alt='메인이미지2'/></li>
			<li><img src='<%= webBase %>/images/20191125_mm/1573455251523.jpg' alt='메인이미지3'/></li>
			<li><img src='<%= webBase %>/images/20191125_mm/1573741478947.jpg' alt='메인이미지4'/></li>
			<% --<li><img src='<%= webBase %>/images/20191125_mm/1580224246368.jpg' alt='메인이미지5'/></li>--%>
		</ul></div>
		<div class='main_section01'>
			<div class='inner'><ul>
				<li><a href='<%= webBase + counEList[ 5][4] %>'><img src='<%= webBase %>/images/main/coun_icon01.png' alt='회의록 검색'/></a></li>
				<li><a href='<%= webBase + counBList[ 0][4] %>'><img src='<%= webBase %>/images/main/coun_icon02.png' alt='의원정보'/></a></li>
				<li><a href='/coun/press/reportList.do'><img src='<%= webBase %>/images/main/coun_icon03.png' alt='보도자료'/></a></li>
				<li><a href='<%= webBase + counGList[ 2][4] %>'><img src='<%= webBase %>/images/main/coun_icon04.png' alt='의회소식지'/></a></li>
				<li><a href='<%= webBase + counFList[ 1][4] %>'><img src='<%= webBase %>/images/main/coun_icon05.png' alt='의회에 바란다'/></a></li>
				<li><a href='<%= webBase + counDList[ 1][4] %>'><img src='<%= webBase %>/images/main/coun_icon06.png' alt='영상회의록'/></a></li>
				<li><a href='<%= webBase + counAList[7][4] %>'><img src='<%= webBase %>/images/main/coun_icon07.png' alt='전화번호안내'/></a></li>
			</ul></div>
		</div>
		<div class='main_section02'>
			<div class='notice'>
				<div class='tab_area'><ul>
					<li><a href='javascript:void(0);' class='on'>공지사항/입법예고</a></li><%--
					<li><a href='javascript:void(0);'>의사일정</a></li>--%>
					<li><a href='javascript:void(0);'>보도자료</a></li>
				</ul></div>
				<div class='tab_cont'>
					<!-- 공지사항/입법예고 -->
					<div class='item' style='display:block;'>
						<div class='list'>
							<ul><c:choose><c:when test="${ empty mainNoti}">
								<li class='center'><%= viewData %></li></c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${mainNoti}">
								<li><a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")'>${rult.bbsTitle}</a><span class='date'>${rult.creDate}</span></li></c:forEach></c:otherwise></c:choose>
							</ul>
							<a href='<%= webBase + counGList[ 0][4] %>' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='공지사항/입법예고 더보기'></a>
						</div>
					</div>
					<!-- //공지사항/입법예고 --><%--
					<!-- 의사일정 -->
					<div class='item'>
						<div class='list'>
							<ul><c:choose><c:when test="${ empty mainSchd}">
								<li class='center'><%= viewData %></li></c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${mainSchd}">
								<li><a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")'>${rult.bbsTitle}</a><span class='date'>${rult.creDate}</span></li></c:forEach></c:otherwise></c:choose>
							</ul>
							<a href='<%= webBase + counCList[ 6][4] %>' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='의사일정 더보기'></a>
						</div>
					</div>
					<!-- //의사일정 -->--%>
					<!-- 보도자료 -->
					<div class='item'>
						<div class='list'>
							<ul><c:choose><c:when test="${ empty mainPress}">
								<li class='center'><%= viewData %></li></c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${mainPress}">
								<li><a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")'>${rult.bbsTitle}</a><span class='date'>${rult.creDate}</span></li></c:forEach></c:otherwise></c:choose>
							</ul>
							<a href='/coun/press/reportList.do' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='보도자료 더보기'></a>
						</div>
					</div>
					<!-- //보도자료 -->
				</div>
			</div>
			<div class='movie'>
				<h2>동영상갤러리</h2>
				<a href='<%= webBase + counCList[ 2][4] %>' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='동영상갤러리 더보기'/></a>
				<div class='movie_area'><c:choose><c:when test="${ empty mainMovie}">
					<div class='img center'><%= viewData %></div></c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${mainMovie}">
					<%-- a href='javascript:alert("홍보영상 준비 중입니다.");' --%>
<%-- onclick='fnAct<%= movieLayer ? "Media" : "Detail" %>("${rult.viewNo}","${rult.actMode}")' --%>
<%-- 20200313 --%>
<a href='javascript:void(0);' onclick='fnAct<%= movieLayer ? "Media" : "Detail" %>("${rult.viewNo}","${rult.actMode}")'> 
						<div class='img'>
							<span class='ico_play'><img src='<%= webBase %>/images/icon/ico_play.png' alt='재생'/></span><c:choose><c:when test="${!empty rult.fileUUID}">
							<img src='<% if( imgMode ) { %><%= webBase %>/GetImage.do?key=${rult.fileUUID}<% } else { %><%= webBase + uploadPath %>${rult.fileAbs}<% } %>' alt='${rult.bbsTitle}'/></c:when><c:otherwise>
							<video oncontextmenu='return false;' width='260' height='147'<% if(viewPoster){ %><c:if test="${!empty rult.fileUUID}"> poster='<% if( imgMode ) { %><%= webBase %>/GetImage.do?key=${rult.fileUUID}<% } else { %><%= webBase + uploadPath %>${rult.fileAbs}<% } %>'</c:if><% }else{ %> preload='metadata'<% } %>><% if( vodMode ) { %>
								<source src='<%= webBase %>/GetMovie.do?key=${rult.downUUID}' type='video/${rult.downExt}'/><% } else { %>
								<source src='<%= viewPort + webBase %>${rult.downAbs}' type='video/${rult.downExt}'/><% } %>
							</video></c:otherwise></c:choose>
						</div>
						<div class='txt'>${rult.bbsTitle}</div>
					</a></c:forEach></c:otherwise></c:choose>
				</div>
			</div>
			<div class='agenda'>
				<div class='tab_area'><ul>
					<li><a href='javascript:void(0);' class='on'>접수의안</a></li>
					<li><a href='javascript:void(0);'>처리의안</a></li>
				</ul></div>
				<div class='tab_cont'>
					<!-- 접수의안 -->
					<div class='item' style='display:block;'>
						<div class='list'>
							<ul><c:choose><c:when test="${ empty mainRecv}">
								<li class='center'><%= viewData %></li></c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${mainRecv}">
								<li><a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")'>${rult.billTitle}</a></li></c:forEach></c:otherwise></c:choose>
							</ul>
							<a href='<%= webBase + counEList[13][4] %>' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='접수의안 더보기'></a>
						</div>
					</div>
					<!-- //접수의안 -->
					<!-- 처리의안 -->
					<div class='item'>
						<div class='list'>
							<ul><c:choose><c:when test="${ empty mainProc}">
								<li class='center'><%= viewData %></li></c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${mainProc}">
								<li><a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")'>${rult.billTitle}</a></li></c:forEach></c:otherwise></c:choose>
							</ul>
							<a href='<%= webBase + counEList[11][4] %>' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='처리의안 더보기'></a>
						</div>
					</div>
					<!-- //처리의안 -->
				</div>
			</div>
		</div>
		<div class='main_section03'>
			<div class='hotissue'>
				<h2>평택시의회 핫이슈</h2>
				<div class='hotissue_list'><ul><c:if test="${!empty mainPopup}"><c:forEach var="rult" varStatus="status" items="${mainPopup}"><c:choose><c:when test="${'F' eq rult.bbsSiteType}">
					<li><% if( imgMode ) { %><img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/><% } else { %><img src='<%= webBase + uploadPath %>${rult.fileAbs}' alt='${rult.bbsTitle}'/><% } %></li></c:when><c:otherwise>
					<li><a href='${rult.bbsSiteUri}'<c:if test="${'N' eq rult.bbsSiteType}"> target='_blank'</c:if>><% if( imgMode ) { %><img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/><% } else { %><img src='<%= webBase + uploadPath %>${rult.fileAbs}' alt='${rult.bbsTitle}'/><% } %></a></li></c:otherwise></c:choose></c:forEach></c:if>
				</ul></div>
			</div>
			<div class='chairman'>
				<h2>의장인사말</h2>
				<a href='<%= webBase + counAList[ 0][4] %>' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='의장인사말 더보기'/></a>
				<div class='chairman_img'><a href='<%= webBase + counAList[ 0][4] %>'>
					<img src='<%= webBase %>/images/main/chairman.png' alt='생생한 현장의 목소리를 경청하며, 현장중심의 의정을 펼치겠습니다. 강정구 의장' width="400px"/>
				</a></div>
			</div>
			<div class='gallery'>
				<h2>의정활동 갤러리</h2>
				<a href='<%= webBase + counCList[ 1][4] %>' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='의정활동 갤러리 더보기'/></a>
				<div class='gallery_list'><ul><c:choose><c:when test="${ empty mainPhoto}">
					<li class='center'><%= viewData %></li></c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${mainPhoto}">
					<li><a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")'><% if( isGallery ) { %>
							<div class='img'><img src='<%= imgDomain %>/mimg/${rult.fileUri}/${rult.fileName}' alt='${rult.bbsTitle}'/></div><% } else { %>
							<div class='img'><img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/></div><% } %>
							<div class='txt'><p>${fn:substring(rult.bbsTitle,0,fn:length(rult.bbsTitle)-3)}</p><span class='date'><% if( isGallery ) { %>${rult.takeDate}<% } else { %>${rult.creDate}<% } %></span></div>
					</a></li></c:forEach></c:otherwise></c:choose>
				</ul></div>
			</div>
		</div>
		<div class='main_section04'>
			<h2>현역의원</h2>
			<div class='search_member'>
				<div>
					<label for='searchMembText' class='hide'>의원명</label>
					<input type='text' id='searchMembText' placeholder='의원명을 입력하세요' /><%--
					<input type='image' src='<%= webBase %>/images/icon/ico_search01.png' onclick='fnMembFinder("view")' alt='검색'/>--%>
					<img src='<%= webBase %>/images/icon/ico_search01.png' class='center vam' onclick='fnMembFinder("view")' alt='검색' style='cursor:pointer;'/>
				</div>
			</div>
			<div class='list'><ul><c:if test="${!empty mainMemb}"><c:forEach var="rult" varStatus="status" items="${mainMemb}">
				<li><a href='javascript:void(0);' onclick='fnActActive("${rult.memID}")'>
					<div class='img'><img src='<%= webBase %>/images/former/${rult.memPic}' alt='${rult.memName} ${rult.comName}'/></div>
					<div class='txt'><strong>${rult.memName}<span> ${rult.comName}</span></strong><p>${rult.regName}</p></div>
				</a></li>
			</c:forEach></c:if></ul></div>
		</div>
		<div class='main_section05'><ul><c:if test="${!empty mainBanner}"><c:forEach var="rult" varStatus="status" items="${mainBanner}"><c:choose><c:when test="${'F' eq rult.bbsSiteType}">
			<li><% if( imgMode ) { %><img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/><% } else { %><img src='<%= webBase + uploadPath %>${rult.fileAbs}' alt='${rult.bbsTitle}'/><% } %></li></c:when><c:otherwise>
			<li><a href='${rult.bbsSiteUri}'<c:if test="${'N' eq rult.bbsSiteType}"> target='_blank'</c:if>><% if( imgMode ) { %><img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/><% } else { %><img src='<%= webBase + uploadPath %>${rult.fileAbs}' alt='${rult.bbsTitle}'/><% } %></a></li></c:otherwise></c:choose></c:forEach></c:if>
		</ul></div>
	</div><%= movieLayer ? "<div class='layer'><div class='bg'></div><div class='layer_pop' id='layVideo'></div></div>" : "" %>
	<!--layer popup START-->

	<div id='mask' style='display:none;'></div>


<!-- Popup Start -->
<%--
<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><img src='<%= webBase %>/popup/20241108.jpg' alt='쓰레기의 과학' /></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span>
        <span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
	</div>
--%>

<%--
<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><img src='<%= webBase %>/popup/20241008.jpg' alt='제252회 평택시의회 임시회 의사일정(안) (수정)' onclick='fnActDetail("6478851AC1EBB8C70A6C52D5006521D7C1A2AA00E9BA22291D5994738D13E9A91","base/noticeView")' style='cursor:pointer' /></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span>
        <span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
	</div>
--%>

<%--
<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><img src='<%= webBase %>/popup/20240829.jpg' alt='2024년 추석 명절 청탁금지법 카드뉴스' onclick='fnActDetail("D5716EDBB29E226DECFC3EC5620B23BA0AC2C1FDD5866B5F679F8B489C3321EF2","base/noticeView")' style='cursor:pointer' /></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday3'> 오늘 하루동안 보지 않기</label></span>
        <span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
	</div>
--%>
<%--
<div class='mlay_ppbg' id='main_popup2'>
		<div class='mlay_cont'><img src='<%= webBase %>/popup/20240910_1.jpg' alt='명절선물 주지도 받지도 않겠습니다' /></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday2' name='chkday2'/><label for='chkday2'> 오늘 하루동안 보지 않기</label></span>
        <span class='mlay_clbt' id='mlay_clbt2'>닫기</span></div>
	</div>
--%>

<%--
<div class='mlay_ppbg' id='main_popup4'>
   <div class='mlay_cont'><a href='https://www.ptcouncil.go.kr/movie/coun/2024/09/20240911.mp4' target="_blank"><img src='<%= webBase %>/popup/20240911.jpg' alt='의장님 명절 인사'/></a></div>
   <div class='mlay_clbg'><span><input type='checkbox' id='chkday4' name='chkday4'/><label for='chkday4'> 오늘 하루동안 보지 않기</label></span><span class='mlay_clbt' id='mlay_clbt4'>닫기</span></div>
</div>
--%>
<%--
<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><img src='<%= webBase %>/popup/20240513.jpg' alt='평택시의 조례를 함께 만들어주세요' onclick='fnActDetail("9AD863311E226C34FD2D4AE6E6DBD71638A03AEF5F32148399538759C65C89741","base/noticeView")' style='cursor:pointer' /></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span>
        <span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
	</div>
--%>
<%--
<div class='mlay_ppbg' id='main_popup2'>
		<div class='mlay_cont'><img src='<%= webBase %>/popup/20240621_2.jpg' alt='2024 평택시의회 초등학생 공모전' onclick='fnActDetail("28EA6B45EDF4E8274239B7559599042EE5BDA589F513D0C0F17CF37693892DF43","base/noticeView")' style='cursor:pointer' /></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday2' name='chkday2'/><label for='chkday2'> 오늘 하루동안 보지 않기</label></span>
        <span class='mlay_clbt' id='mlay_clbt2'>닫기</span></div>
	</div>
--%>
<%--
<div class='mlay_ppbg' id='main_popup3'>
		<div class='mlay_cont'><img src='<%= webBase %>/popup/20240621_1.jpg' alt='제247회 임시회 의사일정' onclick='fnActDetail("BAB5AC0BCC34AD091EA4EB3B40B0FCEC574713C2C11A17BD1FCEAA697BBBD3032","base/noticeView")' style='cursor:pointer' /></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday3' name='chkday3'/><label for='chkday3'> 오늘 하루동안 보지 않기</label></span>
        <span class='mlay_clbt' id='mlay_clbt3'>닫기</span></div>
	</div>
--%>
<%--
<div class='mlay_ppbg' id='main_popup1'>
   <div class='mlay_cont'><a href='/coun/tip/noticeList.do' target="_blank"><img src='<%= webBase %>/popup/20240419.png' alt='행정사무감사 시민제보'/></a></div>
   <div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span><span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
</div>
--%>

<%--
<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><img src='<%= webBase %>/popup/20240320.jpg' alt='청소년의회 참가학교 및 단체 모집' onclick='fnActDetail("0A820F3621D1BC41FEED1E2D32C5A1D1ECD1CF24759FBFB052F409FFC131EA6A1","base/noticeView")' style='cursor:pointer' /></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span>
        <span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
	</div>
--%>

<%--
<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><img src='<%= webBase %>/popup/20240228.jpg' alt='245회 평택시의회 임시회 의사일정' usemap="#20240228.jpg" onfocus="this.blur()" /></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span>
			<span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
		<map name="20240228.jpg">
			<area shape="rect" coords="270,510,470,640" href="/download.do?fileName=245회 평택시의회 임시회 의사일정.hwp" alt="" />
		</map>
</div>
<div class='mlay_ppbg' id='main_popup2'>
		<div class='mlay_cont'><a href='https://www.debates.go.kr/2016_parti/parti01_2020.php' target="_blank"><img src='<%= webBase %>/popup/20240306.jpg' alt='제22대 국회의원선거 후보자토론회' /></a></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday2' name='chkday2'/><label for='chkday2'> 오늘 하루동안 보지 않기</label></span>
        <span class='mlay_clbt' id='mlay_clbt2'>닫기</span></div>
	</div>
--%>

<%--
<div class='mlay_ppbg' id='main_popup2'>
   <div class='mlay_cont'><img src='<%= webBase %>/popup/20240208.jpg' alt='명절선물 주지도 받지도 않겠습니다.'/></div>
   <div class='mlay_clbg'><span><input type='checkbox' id='chkday2' name='chkday2'/><label for='chkday2'> 오늘 하루동안 보지 않기</label></span><span class='mlay_clbt' id='mlay_clbt2'>닫기</span></div>
</div>
--%>
<%--
<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><img src='<%= webBase %>/popup/20240122.png' alt='제244회 평택시의회 임시회 의사일정' usemap="#20240122.png" onfocus="this.blur()" /></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span>
			<span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
		<map name="20240122.png">
			<area shape="rect" coords="79,457,270,556" href="/download.do?fileName=제244회 평택시의회 임시회 의사일정.hwp" alt="" />
		</map>
</div>
--%>

<%--
<div class='mlay_ppbg' id='main_popup1'>
   <div class='mlay_cont'><a href='http://ptcouncil.go.kr:5608/movie/coun/2023/12/20231228.mp4' target="_blank"><img src='<%= webBase %>/popup/20231228.jpg' alt='의장님 새해인사'/></a></div>
   <div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span><span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
</div>
--%>

<%--
	<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><img src='<%= webBase %>/popup/20231121.png' alt='제243회 평택시의회 임시회 의사일정' usemap="#20231121.png" onfocus="this.blur()" /></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span>
			<span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
		<map name="20231121.png">
			<area shape="rect" coords="179,457,470,556" href="/download.do?fileName=제243회 평택시의회 제2차 정례회 의사일정.hwp" alt="" />
		</map>
	</div>
--%>

<%--
	<div class='mlay_ppbg' id='main_popup1'>
	   <div class='mlay_cont'><img src='<%= webBase %>/popup/20231205.png' alt='의장님 추석인사말'/></div>
	   <div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span><span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
	</div>
--%>

<%--
<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><img src='<%= webBase %>/popup/20230922.jpg' alt='2023 초등학생 그림그리기 공모전' onclick='fnActDetail("5332C403007AD4E13FE5D4C40E235B91CE5479EBE57BF0F0577B6E3FCCB9E8731","base/noticeView")' style='cursor:pointer' /></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span>
        <span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
	</div>
--%>

<%--
<div class='mlay_ppbg' id='main_popup2'>
   <div class='mlay_cont'><a href='http://ptcouncil.go.kr:5608/movie/coun/2023/09/1695637332539.mp4' target="_blank"><img src='<%//= webBase %>/popup/20230926.jpg' alt='의장님 추석인사말'/></a></div>
   <div class='mlay_clbg'><span><input type='checkbox' id='chkday2' name='chkday2'/><label for='chkday2'> 오늘 하루동안 보지 않기</label></span><span class='mlay_clbt' id='mlay_clbt2'>닫기</span></div>
</div>
--%>

<%--
	<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><img src='<%//= webBase %>/popup/20230420.jpg' alt='제 238회 평택시의회 임시회' usemap="#20230420.jpg" onfocus="this.blur()" /></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span>
			<span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
		<map name="20230420.jpg">
			<area shape="rect" coords="211,258,455,320" href="/download.do?fileName=제238회 임시회 의사일정 (안)-최종.hwp" alt="" />
			<area shape="rect" coords="108,452,578,507" href="http://www.ptcouncil.go.kr/coun/cast/live.do" target="_blank" alt="" onfocus="this.blur()" />
			<area shape="rect" coords="108,524,339,554" href="https://www.youtube.com/channel/UCgVmwwXQeon_wcdjmxruj0w" target="_blank" alt="" onfocus="this.blur()" />
		</map>
	</div>
	
	<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><img src='<%//= webBase %>/popup/20230501.jpg' alt='2023 평택시의회 입법정책 아이디어 공모' onclick='fnActDetail("5DACBCB6325D2DC1140D932102589F712B6D9E2C30DC2BAF46307BA84D4BAEB71","base/noticeView")' style='cursor:pointer' /></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span>
        <span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
	</div>

	<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><a href="http://ptcouncil.go.kr/coun/base/counTour.do"><img src='<%//= webBase %>/popup/20230612.png' alt='평택시의회 청사 견학 안내'/></a></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span>
        <span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
	</div>

	<div class='mlay_ppbg' id='main_popup3'>
		<div class='mlay_cont'><img src='<%//= webBase %>/popup/20230621.png' alt='2023년 평택시의회 1주년 축하 이벤트'/></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday3' name='chkday3'/><label for='chkday3'> 오늘 하루동안 보지 않기</label></span>
        <span class='mlay_clbt' id='mlay_clbt3'>닫기</span></div>
	</div>

	<div class='mlay_ppbg' id='main_popup2'>
		<div class='mlay_cont'><img src='<%//= webBase %>/popup/20230630.jpg' alt='2023년 초등학생 그림그리기 공모전' onclick='fnActDetail("619965AAA59F13162E0A4778D83134FDFC0A1B0614429BA31341FD2FDBFFBCF41","base/noticeView")' style='cursor:pointer' /></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday2' name='chkday2'/><label for='chkday2'> 오늘 하루동안 보지 않기</label></span>
        <span class='mlay_clbt' id='mlay_clbt2'>닫기</span></div>
	</div>

	<div class='mlay_ppbg' id='main_popup3'>
		<div class='mlay_cont'><img src='<%//= webBase %>/popup/20230711.png' alt='제240회 평택시의회 임시회 의사일정' usemap="#20230711.png" onfocus="this.blur()" /></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday3' name='chkday3'/><label for='chkday3'> 오늘 하루동안 보지 않기</label></span>
			<span class='mlay_clbt' id='mlay_clbt3'>닫기</span></div>
		<map name="20230711.png">
			<area shape="rect" coords="179,507,470,626" href="/download.do?fileName=제240회 평택시의회 임시회 의사일정.hwp" alt="" />
		</map>
	</div>

	<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><a href="http://www.give.go.kr" target="_blank"><img src='<%//= webBase %>/popup/20221208.jpg' alt='정치후원금'/></a></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span><span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
	</div>

	<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><img src='<%//= webBase %>/popup/20221031.jpg' alt='서울 이태원 참사 관련 입장문'/></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span>
        <span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
	</div>

	<div class='mlay_ppbg' id='main_popup2'>
		<div class='mlay_cont'><img src='<%//= webBase %>/images/20220915.png' alt='제233회 평택시의회 정례회 안내'/></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday2' name='chkday2'/><label for='chkday2'> 오늘 하루동안 보지 않기</label></span>
        <span class='mlay_clbt' id='mlay_clbt2'>닫기</span></div>
	</div>

	<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><img src='<%//= webBase %>/popup/20220914.png' alt='2022년도 행정사무감사 대비 시민제보' usemap="#20220914.png"/></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span>
        <span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
		<map name="20220914.png">
			<area shape="rect" coords="30,510,610,550" href="/download.do?fileName=2022 행정사무감사 시민제보 신청서.hwp" alt="" />
		</map>
	</div>
--%>
<!-- Popup End -->


<%--
	 <div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><img src='<%= webBase %>/images/2021.jpg' alt='2021년 새해인사'/></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span><span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
	</div>
--%> 

<%--	
	<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><img src='<%= webBase %>/images/20210204.png' alt='판결문'/></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span><span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
--%>

<%--
	<div class='mlay_ppbg' id='main_popup2'>
		<div class='mlay_cont'><img src='<%= webBase %>/images/20201217.png' alt='청렴도 측정 결과'/></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday2' name='chkday2'/><label for='chkday2'> 오늘 하루동안 보지 않기</label></span><span class='mlay_clbt' id='mlay_clbt2'>닫기</span></div>
	</div>
--%>


<%--
	<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><img src='<%//= webBase %>/images/20230206.png' alt='코로나 방청제한' usemap="#20230206.png" onfocus="this.blur()" /></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span>
        <span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
		<map name="20230206.png">
			<area shape="rect" coords="145,636,580,693" href="http://www.ptcouncil.go.kr/coun/cast/live.do" target="_blank" alt="" onfocus="this.blur()" />
			<area shape="rect" coords="145,695,348,724" href="https://www.youtube.com/channel/UCgVmwwXQeon_wcdjmxruj0w" target="_blank" alt="" onfocus="this.blur()" />
		</map>
	</div>
--%>
	
<%--   
	<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><img src='<%= webBase %>/popup/20220601.png' alt='평택(의회) 제9대 평택시의회 의원 등록서류' usemap="#20220601.png"/></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span>
        <span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
		<map name="20220601.png">
			<area shape="rect" coords="16,383,818,412" href="/download.do?fileName=평택(의회) 제9대 평택시의회 의원 등록서류.hwp" alt="" />
		</map>
	</div>
--%>

<%--
	<div class='mlay_ppbg' id='main_popup2'>
		<div class='mlay_cont'><a href="<%= webBase %>/images/judgment.jpg" target="_blank"><img src='<%= webBase %>/images/20210204.png' alt='판결문'/></a></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday2' name='chkday2'/><label for='chkday2'> 오늘 하루동안 보지 않기</label></span>
		<span class='mlay_clbt' id='mlay_clbt2'>닫기</span></div>
	</div>
--%>

<%--
<div class='mlay_ppbg' id='main_popup1'>
      <div class='video'>
      <video src='http://ptcouncil.go.kr:5608/movie/coun/2022/12/1672376258681.mp4' poster="<%= webBase %>/popup/20221230.png" controls>
            해당 브라우저는 video 태그를 지원하지 않습니다.
      </video>
      </div>
      <div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span><span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
</div>
--%>



	<!-- 이미지맵 반응형 처리를 위해 스크립트 추가 -->
	<script>
		function isMobile() {
			return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
		}
		(function($) {
			$.fn.rwdImageMaps = function() {
				var $img = this;
				var rwdImageMap = function() {
					$img.each(function() {
						if (typeof($(this).attr('usemap')) == 'undefined')
							return;
						var that = this,
								$that = $(that);
						$('<img />').on('load', function() {
							var attrW = 'width',
									attrH = 'height',
									w = $that.attr(attrW),
									h = $that.attr(attrH);
							if (!w || !h) {
								var temp = new Image();
								temp.src = $that.attr('src');
								if (!w)
									w = temp.width;
								if (!h)
									h = temp.height-10; // -200
							}
							var wPercent = $that.width()/100,
									hPercent = $that.height()/100,
									map = $that.attr('usemap').replace('#', ''),
									c = 'coords';
							$('map[name="' + map + '"]').find('area').each(function() {
								var $this = $(this);
								if (!$this.data(c))
									$this.data(c, $this.attr(c));
								var coords = $this.data(c).split(','),
										coordsPercent = new Array(coords.length);
								for (var i = 0; i < coordsPercent.length; ++i) {
									if (i % 2 === 0)
										coordsPercent[i] = parseInt(((coords[i]/w)*100)*wPercent);
									else
										coordsPercent[i] = parseInt(((coords[i]/h)*100)*hPercent);
								}
								$this.attr(c, coordsPercent.toString());
							});
						}).attr('src', $that.attr('src'));
					});
				};
				$(window).resize(rwdImageMap).trigger('resize');
				return this;
			};
		})(jQuery);

		$(document).ready(function(e) {
			if (isMobile()) {
				$('img[usemap]').rwdImageMaps();
			}
		});
	</script>

	<!--layer popup END-->
	<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>
