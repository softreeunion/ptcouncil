<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Pragma" content="no-cache">
<meta property="og:image" content=""/>
<meta name="keywords" content="평택시의회" />
<meta name="description" content="평택시의회 홈페이지입니다." />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><%= webTitle %></title>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="canonical" href="http://www.ptcouncil.go.kr">
<!-- css -->

<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
<!-- sub contentcss -->
<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />
	
	
<script type="text/javascript" src="/js/jquery.js"></script> 
<script type="text/javascript" src="/js/jquery-ui.min.js"></script> 
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>	
<script type="text/javascript" src="/js/slick.min.js"></script>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

<script>
	$(document).ready(function(){
		$(".depth2 > li:nth-child(5)").addClass("current_on");
	});
</script>
	
</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
    <!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->

	
	<!-- 메인메뉴 end -->
		<div id="subContent">
	<!-- submenu -->
	<jsp:include page="/WEB-INF/views/coun/memb/memb_left.jsp" flush="true"/>
	<!-- sbumenu end -->
<h3 class="skip">본문내용 he</h3>
	   <div id="content">
		<div id="subTitle">
			<h3>의원윤리강령</h3>
		</div>
		   <div id="sub_default">
	   	     <h4>평택시의회 의원 윤리강령 및 윤리실천규범</h4>
		  		 
			   <div class='square_txt'>
				    <p class="round_txt">
			   <span class="pt_ethics" ><img src="/images/ptcouncil_logo.png" title="의회마크"><br> 
			   <br>
			   평택시의회 의원윤리강령</span>
			   
			   <span class="pt_ethics_tit"><br>
			   우리 평택시의회 의원은 시민의 대표자로서 그 직무를 수행함에 있어서 공공의 이익을 우선하고 양심에 따라 성실하게 행동함으로써 시민들로부터 신뢰와 존중을 받을 수 있도록 노력하며, 시민의 복리 증진과 지역 사회 발전에 헌신·봉사함은 물론 이 시대가 요구하는 바람직한 의원상 정립과 효율적인 의정 활동을 수행하기 위하여 본 윤리강령을 제정하고 다음 사항들을 윤리 실천규범으로 삼을 것을 천명한다.</span></p>
				   
				   <ol class="numbertxt" style="list-style:decimal;">
					<li>1.우리는 법령을 준수하고 공공의 이익을 우선하며 양심에 따라 직무를 성실히 수행한다.</li>
					<li>2.우리는 직무를 수행함에 있어서 지위를 남용하지 아니하며 의원으로서의 품위를 충실히 유지한다.</li>
					<li>3.우리는 직무와 관련된 재산상의 권리․이익 또는 직위를 취득하거나 다른 사람을 위하여 그 취득을 알선하지 아니한다.</li>
					<li>4.우리는 공·사 생활에 있어서 청렴한 생활을 실천하여 시민에게 모범을 보이며 공정성을 의심받는 행동을 하지 아니한다.</li>
					<li>5.우리는 시민의 복리증진과 지역 발전을 위하여 헌신·봉사함은 물론, 평택시 및 시민의 명예를 고양시키기 위하여 항상 최선의 노력을 기울인다.</li>
					<li>6.우리는 의정 활동에 있어서 전문성을 부단히 함양하고 의원 상호 간의 예의와 인격을 존중하며 충분한 토론을 통해 양보와 합의를 도출하는 선진 의회상 구현에 앞장선다.</li>
			 </ol></div>
	     </div>
		   
		   
			</div>
			
         </div>
	 </div>
	</div>
	<!-- footer -->
	<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
	<!-- //footer -->
		
		
	</div>
	
	

</body>
</html>
