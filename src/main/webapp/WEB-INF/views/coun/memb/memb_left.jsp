<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>


		
		<!-- sub menu -->
	  <div id="submenuArea" class="w sm_intro">
		<div class="sm_tit ">
			<h2>
			의원소개
			</h2>
		</div>
		<ul class="depth2">
		<!-- 메뉴 활성화시 class="current" 가 class="current_on"으로 바뀌도록 -->
				<li class="current"> <a href="/coun/memb/activeName.do" title="현역의원">현역의원</a> </li>
					<li class="current"> <a href="/coun/memb/activeChair.do" title="의장단·상임위원장">의장단·상임위원장</a> </li>
					<li class="current"> <a href="/coun/memb/activeFormer.do" title="역대의원">역대의원</a> </li>
					<li class="current"> <a href="/coun/memb/activeFinder.do" title="의원검색">의원검색</a> </li>
					<li class="current"> <a href="/coun/memb/ethics.do" title="의원윤리강령">의원윤리강령</a> </li>
				</ul>
		
		
			<!-- sub menu end -->
			</div>
			
			
			