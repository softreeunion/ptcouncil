<%
////////////////////////////////////////////////////////////
// Program ID  : activeParty_list
// Description : 의원소개 > 현역의원 - 정당별 [목록조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "현역의원";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb02_01").addClass("on").removeClass("noChild");
	});
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/coun/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/coun/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="2"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 의원소개 &gt; <%= subTitle %></div>
			<div class='con_tab_area four'><ul>
				<li><a href='<%= webBase %>/coun/memb/activeName.do'>인명별</a></li>
				<li><a href='<%= webBase %>/coun/memb/activeArea.do'>지역구별</a></li>
				<li><a href='<%= webBase %>/coun/memb/activeStand.do'>위원회별</a></li>
				<li><a href='<%= webBase %>/coun/memb/activeParty.do' class='on'>정당별</a></li>
			</ul></div>
			<c:choose><c:when test="${ empty dataList}">
			<h4 class='h4_tit'>무소속</h4>
			<div class='section'>
				<div class='member_list'><ul>
					<li><dl><%= noneData %></dl></li>
			</div>
			</c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${dataList}">
				<c:if test="${status.first}">
			<h4 class='h4_tit'>${rult.memParty}</h4>
			<div class='section'>
				<div class='member_list'><ul>
					<li><dl>
						<dt><p><img src='<%= webBase %>/images/former/${rult.memPic}' alt='${rult.memName} ${rult.comName}'/></p><span><a href='javascript:void(0);' onclick='fnActActive("${rult.memID}")'>자세히</a></span></dt>
						<dd><p>${rult.memName}</p><table>
							<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
							<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>${rult.comName}</td></tr>
								<tr><th>선거구</th><td>${rult.regName}</td></tr>
								<tr><th>정당명</th><td>${rult.memParty}</td></tr>
								<tr><th>휴대폰</th><td>${rult.memMobile}</td></tr>
								<tr><th>이메일</th><td>${rult.memEmail}</td></tr>
							</tbody>
						</table></dd>
					</dl></li>
				</c:if>
				<c:choose><c:when test="${varTemp eq rult.memParty}">
					<li><dl>
						<dt><p><img src='<%= webBase %>/images/former/${rult.memPic}' alt='${rult.memName} ${rult.comName}'/></p><span><a href='javascript:void(0);' onclick='fnActActive("${rult.memID}")'>자세히</a></span></dt>
						<dd><p>${rult.memName}</p><table>
							<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
							<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>${rult.comName}</td></tr>
								<tr><th>선거구</th><td>${rult.regName}</td></tr>
								<tr><th>정당명</th><td>${rult.memParty}</td></tr>
								<tr><th>휴대폰</th><td>${rult.memMobile}</td></tr>
								<tr><th>이메일</th><td>${rult.memEmail}</td></tr>
							</tbody>
						</table></dd>
					</dl></li>
				</c:when><c:otherwise><c:if test="${!status.first}">
				</ul></div>
			</div>
			<h4 class='h4_tit'>${rult.memParty}</h4>
			<div class='section'>
				<div class='member_list'><ul>
					<li><dl>
						<dt><p><img src='<%= webBase %>/images/former/${rult.memPic}' alt='${rult.memName} ${rult.comName}'/></p><span><a href='javascript:void(0);' onclick='fnActActive("${rult.memID}")'>자세히</a></span></dt>
						<dd><p>${rult.memName}</p><table>
							<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
							<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>${rult.comName}</td></tr>
								<tr><th>선거구</th><td>${rult.regName}</td></tr>
								<tr><th>정당명</th><td>${rult.memParty}</td></tr>
								<tr><th>휴대폰</th><td>${rult.memMobile}</td></tr>
								<tr><th>이메일</th><td>${rult.memEmail}</td></tr>
							</tbody>
						</table></dd>
					</dl></li>
				</c:if></c:otherwise></c:choose>
				<c:if test="${status.last}">
				</ul></div>
			</div>
				</c:if><c:set var="varTemp" value="${rult.memParty}"/>
			</c:forEach></c:otherwise></c:choose>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer.jsp" flush="true"><jsp:param name="qk" value="F"/></jsp:include>
	<!-- //footer -->
</div>
</form>
</body>
</html>