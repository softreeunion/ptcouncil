<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<script>
		$(document).ready(function(){
			$(".depth2 > li:nth-child(1)").addClass("current_on");
		});
	</script>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<!-- 메인메뉴 end -->
	<div id="subContent">
		<!-- submenu -->
		<jsp:include page="/WEB-INF/views/coun/memb/memb_left.jsp" flush="true"/>
		<!-- sbumenu end -->

		<h3 class="skip">본문내용 he</h3>
		<div id="content">
			<div id="subTitle">
				<h3>현역의원</h3>
			</div>
			<div id="sub_default">
				<!-- sub_default  -->
				<div class="sub_tab_Area three">
					<ul>
						<li><a href="/coun/memb/activeName.do">인명별</a></li>
						<li><a href="/coun/memb/activeArea.do">지역구별</a></li>
						<li><a href="/coun/memb/activeStand.do" class="on">위원회별</a></li>
					</ul>
				</div>

				<h4 class='h4_tit'>의회운영위원회</h4>
				<div class='section'>
					<div class='member_list'><ul>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09002.jpg' alt='이종원 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09002","A")'>자세히</a></span></dt>
							<dd><p>이종원</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원장</td></tr>
								<tr><th>선거구</th><td>가(진위,서탄,지산,송북,신장1·2)</td></tr>
								<tr><th>정당명</th><td>더불어민주당</td></tr>
								<tr><th>연락처</th><td>010-4121-4846</td></tr>
								<tr><th>이메일</th><td>hot88heart@hanmail.net</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09003.jpg' alt='김영주 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09003","B")'>자세히</a></span></dt>
							<dd><p>김영주</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>부위원장</td></tr>
								<tr><th>선거구</th><td>나(중앙,서정)</td></tr>
								<tr><th>정당명</th><td>국민의힘</td></tr>
								<tr><th>연락처</th><td>010-8634-0467</td></tr>
								<tr><th>이메일</th><td>rla0wn@nate.com</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09007.jpg' alt='김명숙 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09007","C")'>자세히</a></span></dt>
							<dd><p>김명숙</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
								<tr><th>선거구</th><td>라(비전1,동삭)</td></tr>
								<tr><th>정당명</th><td>더불어민주당</td></tr>
								<tr><th>연락처</th><td>010-4040-8690</td></tr>
								<tr><th>이메일</th><td>kimms8690@gmail.com</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09009.jpg' alt='김산수 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09009","C")'>자세히</a></span></dt>
							<dd><p>김산수</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
								<tr><th>선거구</th><td>마(비전2,용이)</td></tr>
								<tr><th>정당명</th><td>더불어민주당</td></tr>
								<tr><th>연락처</th><td>031-8024-7515</td></tr>
								<tr><th>이메일</th><td>sandsoo@hanmail.net</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09012.jpg' alt='류정화 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09012","C")'>자세히</a></span></dt>
							<dd><p>류정화</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
								<tr><th>선거구</th><td>바(안중,포승,청북,오성,현덕)</td></tr>
								<tr><th>정당명</th><td>더불어민주당</td></tr>
								<tr><th>연락처</th><td>010-7372-3883</td></tr>
								<tr><th>이메일</th><td>rjhtee@naver.com</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09010.jpg' alt='김혜영 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09010","C")'>자세히</a></span></dt>
							<dd><p>김혜영</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
								<tr><th>선거구</th><td>마(비전2,용이)</td></tr>
								<tr><th>정당명</th><td>국민의힘</td></tr>
								<tr><th>연락처</th><td>010-2173-5558</td></tr>
								<tr><th>이메일</th><td>hyeyoung5558@naver.com</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09015.jpg' alt='이기형 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09015","C")'>자세히</a></span></dt>
							<dd><p>이기형</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
								<tr><th>선거구</th><td>사(팽성,고덕,신평,원평,고덕)</td></tr>
								<tr><th>정당명</th><td>더불어민주당</td></tr>
								<tr><th>연락처</th><td>010-5390-3482</td></tr>
								<tr><th>이메일</th><td>isowkenmay@hanmail.net</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09001.jpg' alt='이관우 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09001","C")'>자세히</a></span></dt>
							<dd><p>이관우</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
								<tr><th>선거구</th><td>가(진위,서탄,지산,송북,신장1·2)</td></tr>
								<tr><th>정당명</th><td>국민의힘</td></tr>
								<tr><th>연락처</th><td>010-9054-3872</td></tr>
								<tr><th>이메일</th><td>dd3872@hanmail.net</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
					</ul></div>
				</div>
				<h4 class='h4_tit'>기획행정위원회</h4>
				<div class='section'>
					<div class='member_list'><ul>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09003.jpg' alt='김영주 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09003","A")'>자세히</a></span></dt>
							<dd><p>김영주</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원장</td></tr>
								<tr><th>선거구</th><td>나(중앙,서정)</td></tr>
								<tr><th>정당명</th><td>국민의힘</td></tr>
								<tr><th>연락처</th><td>010-8634-0467</td></tr>
								<tr><th>이메일</th><td>rla0wn@nate.com</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09007.jpg' alt='김명숙 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09007","B")'>자세히</a></span></dt>
							<dd><p>김명숙</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>부위원장</td></tr>
								<tr><th>선거구</th><td>라(비전1,동삭)</td></tr>
								<tr><th>정당명</th><td>더불어민주당</td></tr>
								<tr><th>연락처</th><td>010-4040-8690</td></tr>
								<tr><th>이메일</th><td>kimms8690@gmail.com</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09004.jpg' alt='최재영 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09004","C")'>자세히</a></span></dt>
							<dd><p>최재영</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
								<tr><th>선거구</th><td>나(중앙,서정)</td></tr>
								<tr><th>정당명</th><td>더불어민주당</td></tr>
								<tr><th>연락처</th><td>031-8024-7595</td></tr>
								<tr><th>이메일</th><td>jy710729@hanmail.net</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09010.jpg' alt='김혜영 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09010","C")'>자세히</a></span></dt>
							<dd><p>김혜영</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
								<tr><th>선거구</th><td>마(비전2,용이)</td></tr>
								<tr><th>정당명</th><td>국민의힘</td></tr>
								<tr><th>연락처</th><td>010-2173-5558</td></tr>
								<tr><th>이메일</th><td>hyeyoung5558@naver.com</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09015.jpg' alt='이기형 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09015","C")'>자세히</a></span></dt>
							<dd><p>이기형</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
								<tr><th>선거구</th><td>사(팽성,고덕,신평,원평,고덕)</td></tr>
								<tr><th>정당명</th><td>더불어민주당</td></tr>
								<tr><th>연락처</th><td>010-5390-3482</td></tr>
								<tr><th>이메일</th><td>isowkenmay@hanmail.net</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
					</ul></div>
				</div>
				<h4 class='h4_tit'>복지환경위원회</h4>
				<div class='section'>
					<div class='member_list'><ul>

						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09009.jpg' alt='김산수 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09009","A")'>자세히</a></span></dt>
							<dd><p>김산수</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원장</td></tr>
								<tr><th>선거구</th><td>마(비전2,용이)</td></tr>
								<tr><th>정당명</th><td>더불어민주당</td></tr>
								<tr><th>연락처</th><td>031-8024-7599</td></tr>
								<tr><th>이메일</th><td>sandsoo@hanmail.net</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09008.jpg' alt='최준구 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09008","B")'>자세히</a></span></dt>
							<dd><p>최준구</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>부위원장</td></tr>
								<tr><th>선거구</th><td>라(비전1,동삭)</td></tr>
								<tr><th>정당명</th><td>국민의힘</td></tr>
								<tr><th>연락처</th><td>010-2950-6767</td></tr>
								<tr><th>이메일</th><td>jkchoi19@naver.com</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09002.jpg' alt='이종원 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09002","C")'>자세히</a></span></dt>
							<dd><p>이종원</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
								<tr><th>선거구</th><td>가(진위,서탄,지산,송북,신장1·2)</td></tr>
								<tr><th>정당명</th><td>더불어민주당</td></tr>
								<tr><th>연락처</th><td>010-4121-4846</td></tr>
								<tr><th>이메일</th><td>hot88heart@hanmail.net</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09006.jpg' alt='이윤하 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09006","C")'>자세히</a></span></dt>
							<dd><p>이윤하</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
								<tr><th>선거구</th><td>다(송탄,통복,세교)</td></tr>
								<tr><th>정당명</th><td>더불어민주당</td></tr>
								<tr><th>연락처</th><td>010-2390-7771</td></tr>
								<tr><th>이메일</th><td>daeha101@hanmail.net</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09016.jpg' alt='정일구 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09016","C")'>자세히</a></span></dt>
							<dd><p>정일구</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
								<tr><th>선거구</th><td>사(팽성,고덕,신평,원평,고덕)</td></tr>
								<tr><th>정당명</th><td>국민의힘</td></tr>
								<tr><th>연락처</th><td>031-8024-7530</td></tr>
								<tr><th>이메일</th><td>ilgoo21@naver.com</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09018.jpg' alt='최선자 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09018","C")'>자세히</a></span></dt>
							<dd><p>최선자</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
								<tr><th>선거구</th><td>비례대표</td></tr>
								<tr><th>정당명</th><td>더불어민주당</td></tr>
								<tr><th>연락처</th><td>010-5611-8400</td></tr>
								<tr><th>이메일</th><td>sunja8400@hanmail.net</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
					</ul></div>
				</div>
				<h4 class='h4_tit'>산업건설위원회</h4>
				<div class='section'>
					<div class='member_list'><ul>

						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09012.jpg' alt='류정화 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09012","A")'>자세히</a></span></dt>
							<dd><p>류정화</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원장</td></tr>
								<tr><th>선거구</th><td>바(안중,포승,청북,오성,현덕)</td></tr>
								<tr><th>정당명</th><td>더불어민주당</td></tr>
								<tr><th>연락처</th><td>010-7372-3883</td></tr>
								<tr><th>이메일</th><td>rjhtee@naver.com</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09005.jpg' alt='소남영 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09005","B")'>자세히</a></span></dt>
							<dd><p>소남영</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>부위원장</td></tr>
								<tr><th>선거구</th><td>다(송탄,통복,세교)</td></tr>
								<tr><th>정당명</th><td>국민의힘</td></tr>
								<tr><th>연락처</th><td>010-3757-4475</td></tr>
								<tr><th>이메일</th><td>youngjinso@naver.com</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09001.jpg' alt='이관우 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09001","C")'>자세히</a></span></dt>
							<dd><p>이관우</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
								<tr><th>선거구</th><td>가(진위,서탄,지산,송북,신장1·2)</td></tr>
								<tr><th>정당명</th><td>국민의힘</td></tr>
								<tr><th>연락처</th><td>010-9054-3872</td></tr>
								<tr><th>이메일</th><td>dd3872@hanmail.net</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09013.jpg' alt='유승영 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09013","C")'>자세히</a></span></dt>
							<dd><p>유승영</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>의원</td></tr>
								<tr><th>선거구</th><td>바(안중, 포승, 청북, 오성, 현덕)</td></tr>
								<tr><th>정당명</th><td>더불어민주당</td></tr>
								<tr><th>연락처</th><td>010-3348-2345</td></tr>
								<tr><th>이메일</th><td>ptysy@hanmail.net</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09014.jpg' alt='김승겸 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09014","C")'>자세히</a></span></dt>
							<dd><p>김승겸</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
								<tr><th>선거구</th><td>사(팽성,고덕,신평,원평,고덕)</td></tr>
								<tr><th>정당명</th><td>더불어민주당</td></tr>
								<tr><th>연락처</th><td>010-5413-8213</td></tr>
								<tr><th>이메일</th><td>seunggyum123@hanmail.net</td></tr>
								</tbody>
							</table></dd>
						</dl></li>
						<li><dl>
							<dt><p><img src='<%= webBase %>/images/former/09017.jpg' alt='김순이 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09017","C")'>자세히</a></span></dt>
							<dd><p>김순이</p><table>
								<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
								<tbody>
								<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
								<tr><th>선거구</th><td>비례대표</td></tr>
								<tr><th>정당명</th><td>국민의힘</td></tr>
								<tr><th>연락처</th><td>010-8556-5026</td></tr>
								<tr><th>이메일</th><td>nike708@nate.com</td></tr>
								</tbody>
							</table></dd>
						</dl></li>

					</ul></div>
				</div>



			</div><!-- sub_default end -->
		</div>


	</div>


	<!-- footer -->
	<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
	<!-- //footer -->


</div>



</body>
</html>
