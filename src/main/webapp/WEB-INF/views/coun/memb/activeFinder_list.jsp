<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<script>
		$(document).ready(function(){
			$(".depth2 > li:nth-child(4)").addClass("current_on");
		});
		var fnActChanger    = function(){
			$.ajax({
				type          : "post",
				url           : "<%= webBase %>/cmmn/seleParty.do",
				dataType      : "html",
				data          : {era:$("#reEraCode").val(),reg:"${reRegCode}"},
				success       : function(data,status,xhr  ){$("#reRegCode").empty();$("#reRegCode").html(data);},
				error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
			});
		};
		var fnActRetrieve   = function(page){
			doActLoadShow();
			$("#pageCurNo").val(isEmpty(page)?"1":page);
			$("#frmDefault").attr({"action":"activeFinder.do","method":"post","target":"_self"}).submit();
		};
	</script>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<form id='frmDefault' name='frmDefault'>
		<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
		<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
		<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>

	<div id="subContent">
		<!-- submenu -->
		<jsp:include page="/WEB-INF/views/coun/memb/memb_left.jsp" flush="true"/>
		<!-- sbumenu end -->

		<h3 class="skip">본문내용 he</h3>
		<div id="content">
			<div id="subTitle">
				<h3>의원검색</h3>
			</div>
			<div id="sub_default">
				<!-- sub_default  -->

				<div class="board_search">
					<dl>
						<dt>대수</dt>
						<dd>
							<label for="condType" class="hide">검색영역</label>
							<select id='reEraCode' name='reEraCode' class='wp100' onchange='fnActChanger()'>
								<option value=''>전체</option>
								<c:if test="${!empty eraList}">
									<c:forEach var="cate" varStatus="status" items="${eraList}">
										<option value='${cate.eraCode}'<c:if test="${reEraCode eq cate.eraCode}"> selected='selected'</c:if>>${cate.eraName}</option>
									</c:forEach>
								</c:if>
							</select>
						</dd>
						<dt>선거구</dt>
						<dd>
							<label for="condType" class="hide">검색영역</label>
							<select id='reRegCode' name='reRegCode' class='wp250'>
								<option value=''>전체</option>
								<c:if test="${!empty regList}">
									<c:forEach var="cate" varStatus="status" items="${regList}">
										<option value='${cate.regCode}'<c:if test="${reRegCode eq cate.regCode}"> selected='selected'</c:if>>${cate.regName}</option>
									</c:forEach>
								</c:if>
							</select>
						</dd>
						<dt>의원명</dt>
						<dd>
							<label for="condValue" class="hide">검색어</label>
							<input type='text' id='condValue' name='condValue' class='w25' maxlength='30' value='${condValue}' placeholder='의원명을 입력하세요.'/>
							<a href='javascript:void(0);' class='form_btn' onclick='fnActRetrieve()'>검색</a>
						</dd>
					</dl>
				</div>

				<div class='board_total'>의원 검색결과: 총 <strong>1,921</strong>명 &nbsp; 1 / 193</div>

				<table class="board_list">
					<caption class='hide'>Board List</caption>
					<colgroup>
						<col style="width:15%;" />
						<col style='width:15%;'/>
						<col style='width:35%;' />
						<col style='width:35%;' />
					</colgroup>
					<thead><tr>
						<th scope='col'>대수</th>
						<th scope='col'>의원명</th>
						<th scope='col'>선거구</th>
						<th scope='col'>비고</th>
					</tr></thead>
					<tbody>
					<c:choose>
						<c:when test="${ empty dataList}">
							<tr class='h200'><td colspan='4'><%= noneData %></td></tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="rult" varStatus="status" items="${dataList}">
								<tr class='pnc' onclick='fnActActive("${rult.memID}")'>
									<td>${rult.eraName}</td>
									<td>${rult.memName}</td>
									<td>${rult.regName}</td>
									<td>${rult.comName}</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
					</tbody>
				</table>
				<c:if test="${!empty pageInfo}">
					<div class='paging'><ui:pagination paginationInfo="${pageInfo}" type="baseText" jsFunction="fnActRetrieve" /></div>
				</c:if>

			</div><!-- sub_default end -->
		</div>


	</div>
	</form>


	<!-- footer -->
	<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
	<!-- //footer -->

</div>

</body>
</html>
