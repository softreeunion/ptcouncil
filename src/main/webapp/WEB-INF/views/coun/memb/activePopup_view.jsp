<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>평택시의회</title>
	<link rel="shortcut icon" href="images/favicon.ico" />
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>

	<script>
		$(document).ready(function(){
		});
		function popClose(){
			var ua = navigator.userAgent.toLowerCase();
			if(ua.indexOf("android") > -1){
				window.open("about:blank","_self").self.close();
			}else if(ua.indexOf("iphone") > -1 || ua.indexOf("ipad") > -1 || ua.indexOf("ipod") > -1){
				window.open('','_self','');
				window.close();
			}else{
				self.close();
			}
		}
	</script>

</head>

<body style="overflow: hidden">
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<h3 class="skip">본문내용</h3>
<div id='pro_popupArea'>
	<div class='pro_popup'>
		<h2>의원 프로필</h2>

		<div class='pop_memb'>
			<dl>
				<dt><img src='<%= webBase %>/images/former/${dataView.memPic}' alt='${dataView.memName} ${dataView.comName}'/></dt>
				<dd>
					<div class="name">
						<strong>
							${dataView.memName}
						</strong>
					</div>
					<ul class="dot">
						<li><em>직    위</em><span class="colon">:</span><span> ${dataView.comName}</span></li>
						<li><em>선 거 구</em><span class="colon">:</span><span> ${dataView.regName}</span></li>
						<li><em>정 당 명</em><span class="colon">:</span><span> ${dataView.memParty}</span></li>
						<li><em>연 락 처</em><span class="colon">:</span><span> ${dataView.memMobile}</span></li>
						<li><em>이 메 일</em><span class="colon">:</span><span> ${dataView.memEmail}</span></li>
					</ul>
				</dd>
			</dl>
		</div>
		<div class='pro_bottom'>
			<h4>약력</h4>
			<div class='mem_history'>${dataView.memProfileBR}</div>
		</div>

		<div class='btn_group center'>
			<a href='javascript:void(0);' class='s_btn_style04' onclick='popClose()'>창닫기 X</a>
		</div>
	</div>
</div>


</body>
</html>
