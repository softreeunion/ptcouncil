<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<script>
		$(document).ready(function(){
			$(".depth2 > li:nth-child(3)").addClass("current_on");
		});
		var fnActRetrieve   = function(page) {
			var $form=$("#frmDetail");
			if($form.length<=0){$form=$("<form/>").attr({id:"frmDetail",name:"frmDetail",method:"post"});$(document.body).append($form);}
			if($form.length<=1){$form.empty();}
			if(!isEmpty(page )){$("<input/>").attr({type:"hidden",id:"reEraCode",name:"reEraCode",value:page}).appendTo($form);}
			if($form.length<=1){$form.attr({"action":"activeFormer.do","method":"post","target":"_self"}).submit();}
		};
	</script>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>

	<div id="content">
		<!-- header -->
		<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
		<!-- //header -->

		<form id='frmDefault' name='frmDefault'>
			<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
			<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
			<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>

		<div id="subContent">
			<!-- submenu -->
			<jsp:include page="/WEB-INF/views/coun/memb/memb_left.jsp" flush="true"/>
			<!-- sbumenu end -->

			<h3 class="skip">본문내용 he</h3>
			<div id="content">
				<div id="subTitle">
					<h3>역대의원</h3>
				</div>
				<div id="sub_default"> <!-- sub_default  -->

					<div class="sub_tab_Area ${numbText}"><ul>
						<ul>
						<c:choose>
							<c:when test="${ empty eraList}">
								<li><a href='javascript:void(0);' onclick='fnActRetrieve("08")' class='on'>제8대 의원</a></li>
								<li><a href='javascript:void(0);' onclick='fnActRetrieve("07")'>제7대 의원</a></li>
								<li><a href='javascript:void(0);' onclick='fnActRetrieve("06")'>제6대 의원</a></li>
								<li><a href='javascript:void(0);' onclick='fnActRetrieve("05")'>제5대 의원</a></li>
								<li><a href='javascript:void(0);' onclick='fnActRetrieve("04")'>제4대 의원</a></li>
								<li><a href='javascript:void(0);' onclick='fnActRetrieve("03")'>제3대 의원</a></li>
								<li><a href='javascript:void(0);' onclick='fnActRetrieve("02")'>제2대 의원</a></li>
								<li><a href='javascript:void(0);' onclick='fnActRetrieve("01")'>제1대 의원</a></li>
							</c:when>
							<c:otherwise>
								<c:forEach var="cate" varStatus="status" items="${eraList}">
									<c:if test="${!status.first}">
										<li><a href='javascript:void(0);' onclick='fnActRetrieve("${cate.eraCode}")'<c:if test="${reEraCode eq cate.eraCode}"> class='on'</c:if>>${cate.eraName} 의원</a></li>
									</c:if>
								</c:forEach>
							</c:otherwise>
						</c:choose>
						</ul>
					</ul></div>

					<%-- 전반기/ 후반기 의장단 하드코딩 --%>
					<c:choose>
						<c:when test="${reEraCode eq '08'}">
							<h4 class='h4_tit'>전반기 의장단</h4>
							<table class="board_list_memb">
								<colgroup>
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 25%;">
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 45%;">
								</colgroup>
								<thead>
								<tr>
									<th scope="cols">직위</th>
									<th scope="cols">사진</th>
									<th scope="cols">성명</th>
									<th scope="cols">임기</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>의장</td>
									<td><img src="<%= webBase %>/images/former/08009.jpg" onclick='fnActActive("08009")' /></td>
									<td>권영화</td>
									<td>-</td>
								</tr>
								<tr>
									<td>부의장</td>
									<td><img src="<%= webBase %>/images/former/08005.jpg" onclick='fnActActive("08005")' /></td>
									<td>이병배</td>
									<td>-</td>
								</tr>
								</tbody>
							</table>
							<div class="h_30"></div>
							<h4 class='h4_tit'>후반기 의장단</h4>
							<table class="board_list_memb">
								<colgroup>
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 25%;">
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 45%;">
								</colgroup>
								<thead>
								<tr>
									<th scope="cols">직위</th>
									<th scope="cols">사진</th>
									<th scope="cols">성명</th>
									<th scope="cols">임기</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>의장</td>
									<td><img src="<%= webBase %>/images/former/08002.jpg" onclick='fnActActive("08002")' /></td>
									<td>홍선의</td>
									<td>-</td>
								</tr>
								<tr>
									<td>부의장</td>
									<td><img src="<%= webBase %>/images/former/08007.jpg" onclick='fnActActive("08007")' /></td>
									<td>강정구</td>
									<td>-</td>
								</tr>
								</tbody>
							</table>
							<div class="h_30"></div>
							<h4 class='h4_tit'>평택시의회 8대 의원</h4>
						</c:when>
						<c:when test="${reEraCode eq '07'}">
							<h4 class='h4_tit'>전반기 의장단</h4>
							<table class="board_list_memb">
								<colgroup>
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 25%;">
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 45%;">
								</colgroup>
								<thead>
								<tr>
									<th scope="cols">직위</th>
									<th scope="cols">사진</th>
									<th scope="cols">성명</th>
									<th scope="cols">임기</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>의장</td>
									<td><img src="<%= webBase %>/images/former/07013.gif" onclick='fnActActive("07013")' /></td>
									<td>김인식</td>
									<td>-</td>
								</tr>
								<tr>
									<td>부의장</td>
									<td><img src="<%= webBase %>/images/former/07002.gif" onclick='fnActActive("07002")' /></td>
									<td>양경석</td>
									<td>-</td>
								</tr>
								</tbody>
							</table>
							<div class="h_30"></div>
							<h4 class='h4_tit'>후반기 의장단</h4>
							<table class="board_list_memb">
								<colgroup>
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 25%;">
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 45%;">
								</colgroup>
								<thead>
								<tr>
									<th scope="cols">직위</th>
									<th scope="cols">사진</th>
									<th scope="cols">성명</th>
									<th scope="cols">임기</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>의장</td>
									<td><img src="<%= webBase %>/images/former/07001.gif" onclick='fnActActive("07001")' /></td>
									<td>김윤태</td>
									<td>-</td>
								</tr>
								<tr>
									<td>부의장</td>
									<td><img src="<%= webBase %>/images/former/07009.gif" onclick='fnActActive("07009")' /></td>
									<td>김기성</td>
									<td>-</td>
								</tr>
								</tbody>
							</table>
							<div class="h_30"></div>
							<h4 class='h4_tit'>평택시의회 7대 의원</h4>
						</c:when>
						<c:when test="${reEraCode eq '06'}">
							<h4 class='h4_tit'>전반기 의장단</h4>
							<table class="board_list_memb">
								<colgroup>
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 25%;">
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 45%;">
								</colgroup>
								<thead>
								<tr>
									<th scope="cols">직위</th>
									<th scope="cols">사진</th>
									<th scope="cols">성명</th>
									<th scope="cols">임기</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>의장</td>
									<td><img src="<%= webBase %>/images/former/06009.jpg" onclick='fnActActive("06009")' /></td>
									<td>송종수</td>
									<td>-</td>
								</tr>
								<tr>
									<td>부의장</td>
									<td><img src="<%= webBase %>/images/former/06007.jpg" onclick='fnActActive("06007")' /></td>
									<td>김재균</td>
									<td>-</td>
								</tr>
								</tbody>
							</table>
							<div class="h_30"></div>
							<h4 class='h4_tit'>후반기 의장단</h4>
							<table class="board_list_memb">
								<colgroup>
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 25%;">
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 45%;">
								</colgroup>
								<thead>
								<tr>
									<th scope="cols">직위</th>
									<th scope="cols">사진</th>
									<th scope="cols">성명</th>
									<th scope="cols">임기</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>의장</td>
									<td><img src="<%= webBase %>/images/former/06016.jpg" onclick='fnActActive("06016")' /></td>
									<td>이희태</td>
									<td>-</td>
								</tr>
								<tr>
									<td>부의장</td>
									<td><img src="<%= webBase %>/images/former/06014.gif" onclick='fnActActive("06014")' /></td>
									<td>임승근</td>
									<td>-</td>
								</tr>
								</tbody>
							</table>
							<div class="h_30"></div>
							<h4 class='h4_tit'>평택시의회 6대 의원</h4>
						</c:when>
						<c:when test="${reEraCode eq '05'}">
							<h4 class='h4_tit'>전반기 의장단</h4>
							<table class="board_list_memb">
								<colgroup>
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 25%;">
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 45%;">
								</colgroup>
								<thead>
								<tr>
									<th scope="cols">직위</th>
									<th scope="cols">사진</th>
									<th scope="cols">성명</th>
									<th scope="cols">임기</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>의장</td>
									<td><img src="<%= webBase %>/images/former/05001.jpg" onclick='fnActActive("05011")' /></td>
									<td>배연서</td>
									<td>-</td>
								</tr>
								<tr>
									<td>부의장</td>
									<td><img src="<%= webBase %>/images/former/05002.jpg" onclick='fnActActive("05006")' /></td>
									<td>송기철</td>
									<td>-</td>
								</tr>
								</tbody>
							</table>
							<div class="h_30"></div>
							<h4 class='h4_tit'>후반기 의장단</h4>
							<table class="board_list_memb">
								<colgroup>
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 25%;">
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 45%;">
								</colgroup>
								<thead>
								<tr>
									<th scope="cols">직위</th>
									<th scope="cols">사진</th>
									<th scope="cols">성명</th>
									<th scope="cols">임기</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>의장</td>
									<td><img src="<%= webBase %>/images/former/05010.jpg" onclick='fnActActive("05001")' /></td>
									<td>유해준</td>
									<td>-</td>
								</tr>
								<tr>
									<td>부의장</td>
									<td><img src="<%= webBase %>/images/former/05003.jpg" onclick='fnActActive("05002")' /></td>
									<td>최종석</td>
									<td>-</td>
								</tr>
								</tbody>
							</table>
							<div class="h_30"></div>
							<h4 class='h4_tit'>평택시의회 5대 의원</h4>
						</c:when>
						<c:when test="${reEraCode eq '04'}">
							<h4 class='h4_tit'>전반기 의장단</h4>
							<table class="board_list_memb">
								<colgroup>
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 25%;">
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 45%;">
								</colgroup>
								<thead>
								<tr>
									<th scope="cols">직위</th>
									<th scope="cols">사진</th>
									<th scope="cols">성명</th>
									<th scope="cols">임기</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>의장</td>
									<td><img src="<%= webBase %>/images/former/04014.gif" onclick='fnActActive("04014")' /></td>
									<td>이익재</td>
									<td>-</td>
								</tr>
								<tr>
									<td>부의장</td>
									<td><img src="<%= webBase %>/images/former/04010.gif" onclick='fnActActive("04010")' /></td>
									<td>유해준</td>
									<td>-</td>
								</tr>
								</tbody>
							</table>
							<div class="h_30"></div>
							<h4 class='h4_tit'>후반기 의장단</h4>
							<table class="board_list_memb">
								<colgroup>
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 25%;">
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 45%;">
								</colgroup>
								<thead>
								<tr>
									<th scope="cols">직위</th>
									<th scope="cols">사진</th>
									<th scope="cols">성명</th>
									<th scope="cols">임기</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>의장</td>
									<td><img src="<%= webBase %>/images/former/04015.gif" onclick='fnActActive("04015")' /></td>
									<td>이정우</td>
									<td>-</td>
								</tr>
								<tr>
									<td>부의장</td>
									<td><img src="<%= webBase %>/images/former/04006.gif" onclick='fnActActive("04006")' /></td>
									<td>배연서</td>
									<td>-</td>
								</tr>
								</tbody>
							</table>
							<div class="h_30"></div>
							<h4 class='h4_tit'>평택시의회 4대 의원</h4>
						</c:when>
						<c:when test="${reEraCode eq '03'}">
							<h4 class='h4_tit'>전반기 의장단</h4>
							<table class="board_list_memb">
								<colgroup>
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 25%;">
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 45%;">
								</colgroup>
								<thead>
								<tr>
									<th scope="cols">직위</th>
									<th scope="cols">사진</th>
									<th scope="cols">성명</th>
									<th scope="cols">임기</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>의장</td>
									<td><img src="<%= webBase %>/images/former/02007.gif" onclick='fnActActive("02007")' /></td>
									<td>노승관</td>
									<td>-</td>
								</tr>
								<tr>
									<td>부의장</td>
									<td><img src="<%= webBase %>/images/former/03017.gif" onclick='fnActActive("03017")' /></td>
									<td>최학수</td>
									<td>-</td>
								</tr>
								</tbody>
							</table>
							<div class="h_30"></div>
							<h4 class='h4_tit'>후반기 의장단</h4>
							<table class="board_list_memb">
								<colgroup>
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 25%;">
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 45%;">
								</colgroup>
								<thead>
								<tr>
									<th scope="cols">직위</th>
									<th scope="cols">사진</th>
									<th scope="cols">성명</th>
									<th scope="cols">임기</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>의장</td>
									<td><img src="<%= webBase %>/images/former/03001.gif" onclick='fnActActive("03001")' /></td>
									<td>공명구</td>
									<td>-</td>
								</tr>
								<tr>
									<td>부의장</td>
									<td><img src="<%= webBase %>/images/former/03019.gif" onclick='fnActActive("03019")' /></td>
									<td>한일우</td>
									<td>-</td>
								</tr>
								</tbody>
							</table>
							<div class="h_30"></div>
							<h4 class='h4_tit'>평택시의회 3대 의원</h4>
						</c:when>
						<c:when test="${reEraCode eq '02'}">
							<h4 class='h4_tit'>전반기 의장단</h4>
							<table class="board_list_memb">
								<colgroup>
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 25%;">
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 45%;">
								</colgroup>
								<thead>
								<tr>
									<th scope="cols">직위</th>
									<th scope="cols">사진</th>
									<th scope="cols">성명</th>
									<th scope="cols">임기</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>의장</td>
									<td><img src="<%= webBase %>/images/former/02019.gif" onclick='fnActActive("02019")' /></td>
									<td>이영기</td>
									<td>-</td>
								</tr>
								<tr>
									<td>부의장</td>
									<td><img src="<%= webBase %>/images/former/02028.gif" onclick='fnActActive("02028")' /></td>
									<td>한일우</td>
									<td>-</td>
								</tr>
								</tbody>
							</table>
							<div class="h_30"></div>
							<h4 class='h4_tit'>후반기 의장단</h4>
							<table class="board_list_memb">
								<colgroup>
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 25%;">
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 45%;">
								</colgroup>
								<thead>
								<tr>
									<th scope="cols">직위</th>
									<th scope="cols">사진</th>
									<th scope="cols">성명</th>
									<th scope="cols">임기</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>의장</td>
									<td><img src="<%= webBase %>/images/former/02029.gif" onclick='fnActActive("02029")' /></td>
									<td>황종만</td>
									<td>-</td>
								</tr>
								<tr>
									<td>부의장</td>
									<td><img src="<%= webBase %>/images/former/02024.gif" onclick='fnActActive("02024")' /></td>
									<td>최재성</td>
									<td>-</td>
								</tr>
								</tbody>
							</table>
							<div class="h_30"></div>
							<h4 class='h4_tit'>평택시의회 2대 의원</h4>
						</c:when>
						<c:when test="${reEraCode eq '01'}">
							<h4 class='h4_tit'>의장단</h4>
							<table class="board_list_memb">
								<colgroup>
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 25%;">
									<col span="1" style="width: 15%;">
									<col span="1" style="width: 45%;">
								</colgroup>
								<thead>
								<tr>
									<th scope="cols">직위</th>
									<th scope="cols">사진</th>
									<th scope="cols">성명</th>
									<th scope="cols">임기</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>의장</td>
									<td><img src="<%= webBase %>/images/former/01010.gif" onclick='fnActActive("01010")' /></td>
									<td>방효원</td>
									<td>-</td>
								</tr>
								<tr>
									<td>부의장</td>
									<td><img src="<%= webBase %>/images/former/01023.gif" onclick='fnActActive("01023")' /></td>
									<td>한일우</td>
									<td>-</td>
								</tr>
								</tbody>
							</table>
							<div class="h_30"></div>
							<h4 class='h4_tit'>평택시의회 1대 의원</h4>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
					<%-- 전반기/ 후반기 의장단 하드코딩 end --%>

					<%-- 역대의원 하드코딩 --%>
					<c:choose>
						<c:when test="${reEraCode eq '01'}"> <!-- 1대 의원 선거구별 정렬 하드코딩 -->
							<div class='member_list_s'><ul>
								<!-- 1대 의원 정보 입력 (시작)-->
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01010.gif' alt='방효원 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01010")'>자세히</a></span></dt>
									<dd><p>방효원</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>팽성읍</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01022.gif' alt='조재득 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01022")'>자세히</a></span></dt>
									<dd><p>조재득</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>팽성읍</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01025.gif' alt='황종만 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01025")'>자세히</a></span></dt>
									<dd><p>황종만</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>안중면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01014.gif' alt='유원목 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01014")'>자세히</a></span></dt>
									<dd><p>유원목</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>포승면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01019.gif' alt='이춘재 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01019")'>자세히</a></span></dt>
									<dd><p>이춘재</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>청북면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01002.gif' alt='권혁동 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01002")'>자세히</a></span></dt>
									<dd><p>권혁동</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>진위면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01004.gif' alt='김영호 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01004")'>자세히</a></span></dt>
									<dd><p>김영호</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>서탄면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01006.gif' alt='김현승 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01006")'>자세히</a></span></dt>
									<dd><p>김현승</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>고덕면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01021.gif' alt='정의화 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01021")'>자세히</a></span></dt>
									<dd><p>정의화</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>오성면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01001.gif' alt='공명구 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01001")'>자세히</a></span></dt>
									<dd><p>공명구</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>현덕면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01008.gif' alt='박영웅 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01008")'>자세히</a></span></dt>
									<dd><p>박영웅</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>중앙동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01024.gif' alt='한창수 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01024")'>자세히</a></span></dt>
									<dd><p>한창수</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>서정동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01012.gif' alt='송영철 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01012")'>자세히</a></span></dt>
									<dd><p>송영철</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>송탄동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01016.gif' alt='이병주 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01016")'>자세히</a></span></dt>
									<dd><p>이병주</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>지산동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01018.gif' alt='이의상 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01018")'>자세히</a></span></dt>
									<dd><p>이의상</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>송북동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01013.gif' alt='오승수 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01013")'>자세히</a></span></dt>
									<dd><p>오승수</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>신장 1동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01023.gif' alt='한일우 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01023")'>자세히</a></span></dt>
									<dd><p>한일우</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>신장 2동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01007.gif' alt='김흥수 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01007")'>자세히</a></span></dt>
									<dd><p>김흥수</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>신평동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01011.gif' alt='방효익 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01011")'>자세히</a></span></dt>
									<dd><p>방효익</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>신평동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01009.gif' alt='박종수 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01009")'>자세히</a></span></dt>
									<dd><p>박종수</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>원평동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01020.gif' alt='장필선 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01020")'>자세히</a></span></dt>
									<dd><p>장필선</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>통복동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01005.gif' alt='김학연 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01005")'>자세히</a></span></dt>
									<dd><p>김학연</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>비전 1동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01015.gif' alt='이남수 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01015")'>자세히</a></span></dt>
									<dd><p>이남수</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>비전 2동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01017.gif' alt='이영기 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01017")'>자세히</a></span></dt>
									<dd><p>이영기</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>세교동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/01003.gif' alt='김기호 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("01003")'>자세히</a></span></dt>
									<dd><p>김기호</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>도원동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<!-- 1대 의원 정보 입력 (종료)-->
							</ul></div>
						</c:when>
						<c:when test="${reEraCode eq '02'}"> <!-- 2대 의원 선거구별 정렬 하드코딩 -->
							<div class='member_list_s'><ul>
								<!-- 2대 의원 정보 입력 (시작)-->
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02002.gif' alt='곽노선 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02002")'>자세히</a></span></dt>
									<dd><p>곽노선</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>팽성읍</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02009.gif' alt='배연서 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02009")'>자세히</a></span></dt>
									<dd><p>배연서</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>팽성읍</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02029.gif' alt='황종만 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02029")'>자세히</a></span></dt>
									<dd><p>황종만</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>안중면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02012.gif' alt='오태수 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02012")'>자세히</a></span></dt>
									<dd><p>오태수</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>포승면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02021.gif' alt='이익재 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02021")'>자세히</a></span></dt>
									<dd><p>이익재</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>청북면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02024.gif' alt='최재성 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02024")'>자세히</a></span></dt>
									<dd><p>최재성</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>진위면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02010.gif' alt='송준섭 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02010")'>자세히</a></span></dt>
									<dd><p>송준섭</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>서탄면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02006.gif' alt='김현승 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02006")'>자세히</a></span></dt>
									<dd><p>김현승</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>고덕면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02026.gif' alt='표태정 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02026")'>자세히</a></span></dt>
									<dd><p>표태정</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>오성면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02001.gif' alt='공명구 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02001")'>자세히</a></span></dt>
									<dd><p>공명구</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>현덕면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02004.gif' alt='김성남 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02004")'>자세히</a></span></dt>
									<dd><p>김성남</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>중앙동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02016.gif' alt='이기택 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02016")'>자세히</a></span></dt>
									<dd><p>이기택</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>중앙동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02013.gif' alt='윤한수 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02013")'>자세히</a></span></dt>
									<dd><p>윤한수</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>서정동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02014.gif' alt='이규천 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02014")'>자세히</a></span></dt>
									<dd><p>이규천</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>서정동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02027.gif' alt='한덕수 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02027")'>자세히</a></span></dt>
									<dd><p>한덕수</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>송탄동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02017.gif' alt='이병주 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02017")'>자세히</a></span></dt>
									<dd><p>이병주</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>지산동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02005.gif' alt='김창수 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02005")'>자세히</a></span></dt>
									<dd><p>김창수</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>송북동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02020.gif' alt='이의상 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02020")'>자세히</a></span></dt>
									<dd><p>이의상</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>송북동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02011.gif' alt='오승수 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02011")'>자세히</a></span></dt>
									<dd><p>오승수</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>신장 1동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02028.gif' alt='한일우 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02028")'>자세히</a></span></dt>
									<dd><p>한일우</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>신장 2동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02003.gif' alt='김남옥 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02003")'>자세히</a></span></dt>
									<dd><p>김남옥</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>신평동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02025.gif' alt='최학수 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02025")'>자세히</a></span></dt>
									<dd><p>최학수</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>신평동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02023.gif' alt='전용식 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02023")'>자세히</a></span></dt>
									<dd><p>전용식</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>원평동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02007.gif' alt='노승관 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02007")'>자세히</a></span></dt>
									<dd><p>노승관</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>통복동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02018.gif' alt='이상만 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02018")'>자세히</a></span></dt>
									<dd><p>이상만</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>비전 1동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02015.gif' alt='이근표 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02015")'>자세히</a></span></dt>
									<dd><p>이근표</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>비전 2동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02022.gif' alt='장석근 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02022")'>자세히</a></span></dt>
									<dd><p>장석근</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>비전 2동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02019.gif' alt='이영기 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02019")'>자세히</a></span></dt>
									<dd><p>이영기</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>세교동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/02008.gif' alt='박학순 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("02008")'>자세히</a></span></dt>
									<dd><p>박학순</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>도원동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<!-- 2대 의원 정보 입력 (종료)-->
							</ul></div>
						</c:when>
						<c:when test="${reEraCode eq '03'}"> <!-- 3대 의원 선거구별 정렬 하드코딩 -->
							<div class='member_list_s'><ul>
								<!-- 3대 의원 정보 입력 (시작)-->
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03003.gif' alt='배연서 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03003")'>자세히</a></span></dt>
									<dd><p>배연서</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>팽성읍</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03023.gif' alt='황종만 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03023")'>자세히</a></span></dt>
									<dd><p>황종만</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>안중면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03018.gif' alt='한만수 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03018")'>자세히</a></span></dt>
									<dd><p>한만수</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>포승면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03006.gif' alt='용갑중 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03006")'>자세히</a></span></dt>
									<dd><p>용갑중</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>청북면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03005.gif' alt='안광두 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03005")'>자세히</a></span></dt>
									<dd><p>안광두</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>진위면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03021.gif' alt='홍선의 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03021")'>자세히</a></span></dt>
									<dd><p>홍선의</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>진위면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03004.gif' alt='송준섭 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03004")'>자세히</a></span></dt>
									<dd><p>송준섭</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>서탄면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03009.gif' alt='이무전 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03009")'>자세히</a></span></dt>
									<dd><p>이무전</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>서탄면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03002.gif' alt='김현승 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03002")'>자세히</a></span></dt>
									<dd><p>김현승</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>고덕면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03015.gif' alt='정의화 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03015")'>자세히</a></span></dt>
									<dd><p>정의화</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>오성면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03001.gif' alt='공명구 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03001")'>자세히</a></span></dt>
									<dd><p>공명구</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>현덕면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03022.gif' alt='황순오 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03022")'>자세히</a></span></dt>
									<dd><p>황순오</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>중앙동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03008.gif' alt='이규천 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03008")'>자세히</a></span></dt>
									<dd><p>이규천</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>서정동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03010.gif' alt='이병주 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03010")'>자세히</a></span></dt>
									<dd><p>이병주</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>지산동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03012.gif' alt='이의상 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03012")'>자세히</a></span></dt>
									<dd><p>이의상</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>송북동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03013.gif' alt='이정우 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03013")'>자세히</a></span></dt>
									<dd><p>이정우</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>신장1동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03019.gif' alt='한일우 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03019")'>자세히</a></span></dt>
									<dd><p>한일우</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>신장2동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03017.gif' alt='최학수 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03017")'>자세히</a></span></dt>
									<dd><p>최학수</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>신평동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03016.gif' alt='최종석 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03016")'>자세히</a></span></dt>
									<dd><p>최종석</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>원평동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03020.gif' alt='한장희 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03020")'>자세히</a></span></dt>
									<dd><p>한장희</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>통복동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03011.gif' alt='이영창 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03011")'>자세히</a></span></dt>
									<dd><p>이영창</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>비전1동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03014.gif' alt='장석근 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03014")'>자세히</a></span></dt>
									<dd><p>장석근</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>비전2동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/03007.gif' alt='유해준 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("03007")'>자세히</a></span></dt>
									<dd><p>유해준</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>세교동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<!-- 3대 의원 정보 입력 (종료)-->
							</ul></div>
						</c:when>
						<c:when test="${reEraCode eq '04'}"> <!-- 4대 의원 선거구별 정렬 하드코딩 -->
							<div class='member_list_s'><ul>
								<!-- 4대 의원 정보 입력 (시작)-->
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04006.gif' alt='배연서 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04006")'>자세히</a></span></dt>
									<dd><p>배연서</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>팽성읍</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04007.gif' alt='서정희 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04007")'>자세히</a></span></dt>
									<dd><p>서정희</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>안중읍</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04012.gif' alt='이민관 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04012")'>자세히</a></span></dt>
									<dd><p>이민관</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>포승면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04014.gif' alt='이익재 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04014")'>자세히</a></span></dt>
									<dd><p>이익재</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>청북면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04009.gif' alt='안광두 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04009")'>자세히</a></span></dt>
									<dd><p>안광두</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>진위면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04008.gif' alt='송기철 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04008")'>자세히</a></span></dt>
									<dd><p>송기철</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>서탄면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04003.gif' alt='김준배 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04003")'>자세히</a></span></dt>
									<dd><p>김준배</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>고덕면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04021.gif' alt='황인호 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04021'>자세히</a></span></dt>
									<dd><p>황인호</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>오성면<td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04001.gif' alt='공창석 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04001")'>자세히</a></span></dt>
									<dd><p>공창석</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>현덕면</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04020.gif' alt='황순오 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04020")'>자세히</a></span></dt>
									<dd><p>황순오</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>중앙.송탄동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04022.gif' alt='김재균 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04022")'>자세히</a></span></dt>
									<dd><p>김재균</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>서정동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04005.gif' alt='박옥란 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04005")'>자세히</a></span></dt>
									<dd><p>박옥란</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>지산동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04004.gif' alt='류영청 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04004")'>자세히</a></span></dt>
									<dd><p>류영청</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>송북동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04015.gif' alt='이정우 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04015")'>자세히</a></span></dt>
									<dd><p>이정우</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>신장1동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04016.gif' alt='전명신 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04016")'>자세히</a></span></dt>
									<dd><p>전명신</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>신장2동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04002.gif' alt='김성환 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04002")'>자세히</a></span></dt>
									<dd><p>김성환</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>신평동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04018.gif' alt='최종석 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04018")'>자세히</a></span></dt>
									<dd><p>최종석</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>원평동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04019.gif' alt='한장희 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04019")'>자세히</a></span></dt>
									<dd><p>한장희</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>통복동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04013.gif' alt='이영창 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04013")'>자세히</a></span></dt>
									<dd><p>이영창</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>비전1동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04017.gif' alt='전진규 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04017")'>자세히</a></span></dt>
									<dd><p>전진규</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>비전2동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<li><dl>
									<dt><p><img src='<%= webBase %>/images/former/04010.gif' alt='유해준 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("04010")'>자세히</a></span></dt>
									<dd><p>유해준</p><table>
										<colgroup><col style='width:30%;'/><col style='width:70%;'/></colgroup>
										<tbody>
										<tr><th class='b_none'>선거구</th><td class='b_none'>세교동</td></tr>
										<tr><th>정당명</th><td></td></tr>
										<tr><th>연락처</th><td></td></tr>
										</tbody>
									</table></dd>
								</dl></li>
								<!-- 4대 의원 정보 입력 (종료)-->
							</ul></div>
						</c:when>
						<c:otherwise> <!-- 1-4대 의원을 제외한 나머지는 DB에서 불러와 정렬 -->

							<c:choose>
								<c:when test="${ empty dataList}">
									<div class='member_list_s'><%= noneData %></div>
								</c:when>
								<c:otherwise>
								<c:forEach var="rult" varStatus="status" items="${dataList}">
									<div class="profile <c:if test="${status.count mod 2 == 0}">profile_right</c:if>">
										<dl>
											<dt><img src='<%= webBase %>/images/former/${rult.memPic}' alt='${rult.memName} ${rult.comName}'/></dt>
											<dd>
												<div class="name"><strong>${rult.memName}</strong></div>
												<ul class="dot">
													<li><em>직    위</em><span class="colon">:</span><span> ${rult.comName}</span></li>
													<li><em>선 거 구</em><span class="colon">:</span><span> ${rult.regName}</span></li>
													<li><em>정 당 명</em><span class="colon">:</span><span> ${rult.memParty}</span></li>
													<li><em>연 락 처</em><span class="colon">:</span><span> ${rult.memMobile}</span></li>
													<li><em>이 메 일</em><span class="colon">:</span><span> ${rult.memEmail}</span></li>
												</ul>
											</dd>
										</dl>
										<div class="btns">
											<a href="javascript:void(0);"  class="btn_profile" onclick='fnActActive("${rult.memID}")' title="${rult.memName} ${rult.comName} 프로필(새창열림)">프로필자세히보기</a>
										</div>
									</div>
								</c:forEach>
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>
					<%-- 역대의원 하드코딩 end --%>


				</div><!-- sub_default end -->
			</div>
		</div>
		</form>

		<!-- footer -->
		<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
		<!-- //footer -->

	</div>

</body>
</html>
