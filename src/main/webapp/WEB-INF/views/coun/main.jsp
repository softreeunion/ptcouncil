<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
    boolean viewPoster			= true;
    out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Pragma" content="no-cache">
    <meta property="og:image" content=""/>
    <meta name="keywords" content="평택시의회" />
    <meta name="description" content="평택시의회 홈페이지입니다." />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title><%= webTitle %></title>
    <link rel="shortcut icon" href="/images/favicon.ico" />
    <link rel="canonical" href="http://www.ptcouncil.go.kr">
    <!-- css -->

    <link media="all" type="text/css" rel="stylesheet" href="/common/css/main.css" />
    <link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
    <link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
    <link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
    <link rel="stylesheet" type="text/css" href="/common/css/slick.css" />

    <jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript" src="/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/js/common.js"></script>
    <script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
    <script type="text/javascript" src="/js/slick.min.js"></script>

    <script>
        <%--$(function() {--%>
        <%--// 날짜 비교 위한 날짜 가져오기--%>
        <%--<c:set var="now" value="<%=new java.util.Date() %>" />--%>
        <%--<fmt:formatDate var="today" value="${now}" pattern="yyyy.MM.dd" />--%>
        <%--var todayDate = "<c:out value='${today}'/>";--%>
        <%--});--%>
        var fnActDetail     = function(view){
            // doActLoadShow();
            $("#viewNo").val(view);
            $("#frmDefault").attr({"action":"noticeView.do","method":"post","target":"_blank"}).submit();
        };
        $(document).ready(function(){
            // $(".main_visual>ul").bxSlider({auto:true});
            //$("#main_popup1 .mlay_clbt").click(function(){if($("input[name='chkday1']").is(":checked"))setCookieMobile("toCook1","done",1);$("#main_popup1").hide();});
            //$("#main_popup2 .mlay_clbt").click(function(){if($("input[name='chkday2']").is(":checked"))setCookieMobile("toCook2","done",1);$("#main_popup2").hide();});
            //$("#main_popup3 .mlay_clbt").click(function(){if($("input[name='chkday3']").is(":checked"))setCookieMobile("toCook3","done",1);$("#main_popup3").hide();});
            //$("#main_popup4 .mlay_clbt").click(function(){if($("input[name='chkday4']").is(":checked"))setCookieMobile("toCook4","done",1);$("#main_popup4").hide();});

            //$(".mlay_clbt").click(function(){if(!$("#main_popup1").is(":visible"))$("#mask").hide();});
            //$(".mlay_clbt").click(function(){if(!$("#main_popup1").is(":visible")&&!$("#main_popup2").is(":visible"))$("#mask").hide();});
            //$(".mlay_clbt").click(function(){if(!$("#main_popup1").is(":visible")&&!$("#main_popup2").is(":visible")&&!$("#main_popup3").is(":visible"))$("#mask").hide();});
            //$(".mlay_clbt").click(function(){if(!$("#main_popup1").is(":visible")&&!$("#main_popup2").is(":visible")&&!$("#main_popup3").is(":visible")&&!$("#main_popup4").is(":visible"))$("#mask").hide();});
            //$(".mlay_clbt").click(function(){if(!$("#main_popup1").is(":visible")&&!$("#main_popup2").is(":visible"))$("#mask").hide();});

            //if(getCookieMobile("toCook1")==""){$("#main_popup1").show();$("#mask").show();};
            //if(getCookieMobile("toCook2")==""){$("#main_popup2").show();$("#mask").show();};
            //if(getCookieMobile("toCook3")==""){$("#main_popup3").show();$("#mask").show();};
            //if(getCookieMobile("toCook4")==""){$("#main_popup4").show();$("#mask").show();};
        });
        var fnKeyDownMembFinder = function () {
            // 엔터키가 눌렸을 때 실행할 내용
            if (window.event.keyCode == 13) {
                fnMembFinder();
            }
        }
        var fnMembFinder    = function(){
            // doActLoadShow();
            var $form=$("#frmDetail");
            var  view=$("#searchMembText").val();
            if($form.length<=0){$form=$("<form/>").attr({id:"frmDetail",name:"frmDetail",method:"post"});$(document.body).append($form);}
            if($form.length<=1){$form.empty();}
            $("<input/>").attr({type:"hidden",id:"pageCurNo",name:"pageCurNo",value:"1"}).appendTo($form);
            $("<input/>").attr({type:"hidden",id:"reEraCode",name:"reEraCode",value:"<%= BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsCate" ) %>"}).appendTo($form);
            $("<input/>").attr({type:"hidden",id:"condType",name:"condType",value:"N"}).appendTo($form);
            $("<input/>").attr({type:"hidden",id:"condValue",name:"condValue",value:view}).appendTo($form);
            if($form.length<=1){$form.attr({"action":"<%= webBase %>/coun/memb/activeFinder.do","method":"post","target":"_self"}).submit();}
        };
        var fnActDetail     = function(view,acts){
            // doActLoadShow();
            var $form=$("#frmDetail");
            if($form.length<=0){$form=$("<form/>").attr({id:"frmDetail",name:"frmDetail",method:"post"});$(document.body).append($form);}
            if($form.length<=1){$form.empty();}
            if(!isEmpty(view)){$("<input/>").attr({type:"hidden",id:"viewNo",name:"viewNo",value:view}).appendTo($form);}
            if($form.length<=1){$form.attr({"action":"<%= webBase %>/coun/"+acts+".do","method":"post","target":"_self"}).submit();}
        };<% if( movieLayer ) { %>
        var fnActMedia      = function(view,acts){
            $.ajax({
                type          : "post",
                url           : "<%= webBase %>/coun/mediaView.do",
                dataType      : "html",
                data          : {viewNo:view},
                success       : function(data,status,xhr  ){
                    var $LV=$("#layVideo");
                    $LV.empty();
                    $LV.html(data);
                    media_layer();
                },
                error         : function(xhr ,status,error){
                    console.log(xhr.responseText);
                    alert("Code: "+xhr.status+"\nMessage: "+error);
                }
            });
        };<% } %>
    </script>
</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">

    <!-- header -->
    <jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
    <!-- //header -->

    <!-- Popup Start -->
    <div id='mask' style='display:none;'></div>
    <div class='mlay_ppbg' id='main_popup2'>
        <div class='mlay_cont'><a href='https://www.ptcouncil.go.kr/popup/20241231.mp4' target="_blank"><img src='<%= webBase %>/popup/20241231_1.jpg' alt='의장님 새해 인사'/></a></div>
        <div class='mlay_clbg'><span><input type='checkbox' id='chkday2' name='chkday2'/><label for='chkday2'> 오늘 하루동안 보지 않기</label></span><span class='mlay_clbt' id='mlay_clbt2'>닫기</span></div>
    </div>
    <div class='mlay_ppbg' id='main_popup1'>
        <div class='mlay_cont'><a href='https://www.ptcouncil.go.kr/movie/coun/2025/01/1737454972088.mp4' target="_blank"><img src='https://www.ptcouncil.go.kr/files/movie_coun/2025/01/1737454972079.jpg' alt='의장님 설날 인사'/></a></div>
        <div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span><span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
    </div>
    <!-- //Popup End -->

    <section class="parent-element">
    </section>
</div>

<form id='frmDefault' name='frmDefault'>

    <c:set var="now" value="<%=new java.util.Date() %>" />
    <fmt:parseNumber value="${now.time / (1000*60*60*24)}" integerOnly="true" var="today"></fmt:parseNumber>

    <div class="main_content">
        <h3 class="skip">본문내용</h3>

        <div class="main_wrap">
            <div class="inner">

                <!-- main slide -->
                <div id="ptcuncil_img">
                    <p> </p>
                    <h3>
                        <strong>공감하는 <em>의정</em> </strong>
                        <span>신뢰받는 <em>의회</em></span>
                    </h3>
                    <div class="swiper-wrapper">
                        <c:if test="${!empty mainImage}">
                            <c:forEach var="rult" varStatus="status" items="${mainImage}">
                                <c:choose>
                                    <c:when test="${'F' eq rult.bbsSiteType}">
                                        <div class="swiper-slide">
                                            <% if( imgMode ) { %>
                                            <img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/>
                                            <% } else { %>
                                            <img src='<%= webBase + uploadPath %>${rult.fileAbs}' alt='${rult.bbsTitle}'/>
                                            <% } %>
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="swiper-slide">
                                            <a href='${rult.bbsSiteUri}'<c:if test="${'N' eq rult.bbsSiteType}"> target='_blank'</c:if>>
                                                <% if( imgMode ) { %>
                                                <img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/>
                                                <% } else { %>
                                                <img src='<%= webBase + uploadPath %>${rult.fileAbs}' alt='${rult.bbsTitle}'/>
                                                <% } %>
                                            </a>
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </c:if>
                        <%--<div class="swiper-slide bg-1"></div>--%>
                        <%--<div class="swiper-slide bg-2"></div>--%>
                        <%--<div class="swiper-slide bg-3"></div>--%>
                    </div>

                    <!-- 페이징 추가 -->
                    <div class="swiper-pagination"></div>

                    <!-- 네비게이션 버튼 -->
                    <div class="controls">
                        <button type="button" id="prevButton">이전</button>
                        <button type="button" id="stopButton">정지</button>
                        <button type="button" id="resumeButton" style="display: none;">재생</button>
                        <button type="button" id="nextButton">다음</button>
                    </div>
                </div>
                <!-- //main slide end -->

                <div class="con">
                    <div id="chairperson">
                        <h4>의장 <em>강정구</em></h4>
                        <div class="txt">
                            <p>생생한 현장 목소리를 경청하며 </p>
                            현장 중심의 목소리를 펼치겠습니다

                            <a href="/coun/intro/greetings.do" class="greeting">의장 인사말<span></span></a>
                        </div>
                        <div class="pic">
                            <img src="images/main/chairperson_img.png" alt="강정구 의장 사진">
                        </div>
                    </div>

                    <!-- boardArea -->
                    <div id="boardArea">

                        <ul class="">

                            <!-- 공지사항 -->
                            <li id="board1" class="on">
                                <h5><a href="#board1"><span>공지사항</span></a></h5>
                                <div class="con">
                                    <c:choose>
                                    <c:when test="${ empty mainNoti}">
                            <li class='center'><%= viewData %></li>
                            </c:when>
                            <c:otherwise>
                            <c:forEach var="rult" varStatus="status" items="${mainNoti}">
                            <c:if test="${status.first}">
                            <div class="first">
                                <a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")' title="${rult.bbsTitle}">
                                    <span class="date"><div>${fn:substring(rult.creDate,0,4)}<strong>${fn:substring(rult.creDate,5,10)}</strong> </div></span>
                                    <strong class="title"><span>공지사항</span> ${rult.bbsTitle}</strong>

                                    <fmt:parseDate value="${rult.creDate}" var="chg_dttm" pattern="yyyy.MM.dd"/>
                                    <fmt:parseNumber value="${chg_dttm.time / (1000*60*60*24)}" integerOnly="true" var="chgDttm"></fmt:parseNumber>
                                    <c:if test="${today - chgDttm le 3}"><span class="new"><span class="skip">새 글</span>N</span></c:if> 

                                    <span class="detail">${rult.contDesc}</span>
                                </a>
                            </div>
                            </c:if>
                            </c:forEach>
                            </c:otherwise>
                            </c:choose>
                            <ul>
                                <c:choose>
                                    <c:when test="${ empty mainNoti}">
                                        <li class='center'><%= viewData %></li>
                                    </c:when>
                                    <c:otherwise>
                                        <c:forEach var="rult" varStatus="status" items="${mainNoti}" begin="1" end="2">
                                            <li>
                                                <a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")' title="${rult.bbsTitle}">
                                                    <strong class="tit">${rult.bbsTitle}</strong>
                                                    <span class='date'>${rult.creDate}</span>
                                                </a>
                                            </li>
                                        </c:forEach>
                                    </c:otherwise>
                                </c:choose>
                            </ul>
                    </div>
                    <a href="/coun/base/noticeList.do" class="more" title="공지사항">더보기 +</a>
                    </li>
                    <!-- //공지사항 end -->

                    <!-- 보도자료 -->
                    <li id="board2" class="">
                        <h5><a href="#board2"><span>보도자료</span></a></h5>
                        <div class="con">
                            <c:choose>
                            <c:when test="${ empty mainPress}">
                    <li class='center'><%= viewData %></li>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="rult" varStatus="status" items="${mainPress}">
                            <c:if test="${status.first}">
                                <div class="first">
                                    <a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")' title="${rult.bbsTitle}">
                                        <span class="date"><div>${fn:substring(rult.creDate,0,4)}<strong>${fn:substring(rult.creDate,5,10)}</strong> </div></span>
                                        <strong class="title"><span>보도자료</span> ${rult.bbsTitle}</strong>

                                        <fmt:parseDate value="${rult.creDate}" var="chg_dttm" pattern="yyyy.MM.dd"/>
                                        <fmt:parseNumber value="${chg_dttm.time / (1000*60*60*24)}" integerOnly="true" var="chgDttm"></fmt:parseNumber>
                                        <c:if test="${today - chgDttm le 3}"><span class="new"><span class="skip">새 글</span>N</span></c:if> 

                                        <span class="detail">${rult.contDesc}</span>
                                    </a>
                                </div>
                            </c:if>
                        </c:forEach>
                    </c:otherwise>
                    </c:choose>
                    <ul>
                        <c:choose>
                            <c:when test="${ empty mainPress}">
                                <li class='center'><%= viewData %></li>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="rult" varStatus="status" items="${mainPress}" begin="1" end="2">
                                    <li>
                                        <a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")' title="${rult.bbsTitle}">
                                            <strong class="tit">${rult.bbsTitle}</strong>
                                            <span class='date'>${rult.creDate}</span>
                                        </a>
                                    </li>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </ul>
                </div>
                <a href="/coun/press/reportList.do" class="more" title="보도자료">더보기 +</a>
                </li>
                <!-- //보도자료 end -->

                <!-- 입법예고 -->
                <li id="board3" class="">
                    <h5><a href="#board3"><span>입법예고</span></a></h5>
                    <div class="con">
                        <c:choose>
                        <c:when test="${ empty mainRaws}">
                <li class='center'><%= viewData %></li>
                </c:when>
                <c:otherwise>
                    <c:forEach var="rult" varStatus="status" items="${mainRaws}">
                        <c:if test="${status.first}">
                            <div class="first">
                                <a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")' title="${rult.bbsTitle}">
                                    <span class="date"><div>${fn:substring(rult.creDate,0,4)}<strong>${fn:substring(rult.creDate,5,10)}</strong> </div></span>
                                    <strong class="title"><span>입법예고</span> ${rult.bbsTitle}</strong>

                                    <fmt:parseDate value="${rult.creDate}" var="chg_dttm" pattern="yyyy.MM.dd"/>
                                    <fmt:parseNumber value="${chg_dttm.time / (1000*60*60*24)}" integerOnly="true" var="chgDttm"></fmt:parseNumber>
                                    <c:if test="${today - chgDttm le 3}"><span class="new"><span class="skip">새 글</span>N</span></c:if> 

                                    <span class="detail">${rult.contDesc}</span>
                                </a>
                            </div>
                        </c:if>
                    </c:forEach>
                </c:otherwise>
                </c:choose>
                <ul>
                    <c:choose>
                        <c:when test="${ empty mainRaws}">
                            <li class='center'><%= viewData %></li>
                        </c:when>
                        <c:otherwise>
                            <c:forEach var="rult" varStatus="status" items="${mainRaws}" begin="1" end="2">
                                <li>
                                    <a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")' title="${rult.bbsTitle}">
                                        <strong class="tit">${rult.bbsTitle}</strong>
                                        <span class='date'>${rult.creDate}</span>
                                    </a>
                                </li>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </ul>
            </div>
            <a href="/coun/laws/reportList.do" class="more" title="입법예고">더보기 +</a>
            </li>
            <!-- //입법예고 end -->

            </ul>
        </div>
        <!-- //boardArea end -->


    </div>
    </div>
    </div>

    <div id="favoredmenu">
        <div class="inner">
            <div class="tit">
                <h2>
                    자주찾는<span> MENU</span>
                    <p>아이콘을 클릭하시면 해당 페이지로<br />이동합니다</p>
                </h2>
            </div>
            <ul>
                <li class="fa1">
                    <a href="/coun/past/minutesList.do"> 회의록검색</a>
                </li>
                <li class="fa2">
                    <a href="/coun/memb/activeName.do"> 의원정보</a>
                </li>
                <li class="fa3">
                    <a href="/coun/press/reportList.do"> 보도자료</a>
                </li>
                <li class="fa4">
                    <a href="/coun/news/noticeList.do"> 소식지신청</a>
                </li>
                <li class="fa5">
                    <a href="/coun/hope/noticeList.do"> 의회에바란다</a>
                </li>
                <li class="fa6">
                    <a href="/coun/stand/movieList.do?ct=1"> 인터넷방송</a>
                </li>
                <li class="fa7">
                    <a href="/coun/intro/phone/PageView.do"> 전화번호안내</a>
                </li>
            </ul>
        </div>
    </div>
    <!--자주찾는 메뉴 end -->

    <!-- 현역의원 -->
    <div id="member">
        <div class="inner">
            <h4>
                평택시의회 <span>현역의원</span>
            </h4>
            <div id="member_search">
                <input type="hidden" name="mode" value="process">
                <fieldset>
                    <legend>의원검색</legend>
                    <div class="m_sch">
                        <div class="m_txt">
                            <%--<input type="text" name="msb" id="membersc" placeholder="의원을 검색하세요" title="의원을 검색하세요" onkeyup='fnKeyDownMembFinder("view")' />--%>
                            <input type="text" id='searchMembText' name='searchMembText' placeholder="의원을 검색하세요" title="의원을 검색하세요" onkeyup='fnKeyDownMembFinder("view")' />
                            <button type="button" onclick='fnMembFinder("view")'>검색</button>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="member_wrap after">
                <div class="big_wrap">
                    <div class="big_list">
                        <c:if test="${!empty mainMemb}">
                            <c:forEach var="rult" varStatus="status" items="${mainMemb}">
                                <div class="list after">
                                    <div class="pic">
                                        <img src="<%= webBase %>/images/former/${rult.memPic}" alt="${rult.memName}" />
                                    </div>
                                    <div class="txt">
                                        <div class="name after">
                                            <strong>${rult.memName}</strong>
                                            <a href='javascript:void(0);' class="btn_profile" onclick='fnActActive("${rult.memID}")' title="${rult.memName} ${rult.comName}">프로필보기</a>
                                        </div>
                                        <ul class="dash">
                                            <li>
                                                <c:choose>
                                                    <c:when test="${rult.memID=='09001'}">
                                                        대표의원
                                                    </c:when>
                                                    <c:when test="${rult.memID=='09015'}">
                                                        대표의원
                                                    </c:when>
                                                    <c:otherwise>
                                                        ${rult.comName}
                                                    </c:otherwise>
                                                </c:choose>
                                            </li>
                                            <li>${rult.regName}</li>
                                            <li>${rult.memParty}</li>
                                            <li>${rult.memMobile}</li>
                                            <li>${rult.memEmail}</li>
                                        </ul>
                                    </div>
                                </div>
                            </c:forEach>
                        </c:if>
                    </div>
                    <div class="control">
                        <button type="button" class="btn_prev">이전 현역의원 보기</button>
                        <button type="button" class="btn_stop">현역의원 보기 일시정지</button>
                        <button type="button" class="btn_next">다음 현역의원 보기</button>
                    </div>
                </div>
                <div class="small_wrap">
                    <div class="small_list">
                        <c:if test="${!empty mainMemb}">
                            <c:forEach var="rult" varStatus="status" items="${mainMemb}">
                                <a href="#member" class="mem_s"><img src="<%= webBase %>/images/former/${rult.memPic}" alt="${rult.memName} ${rult.comName}" /></a>
                            </c:forEach>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //현역의원 end -->

    <div class="content_03">

        <div class="inner">

            <!-- 의정활동 gallery -->
            <div id="gallery">
                <h4>의정활동 <span>GALLERY</span>
                    <span class="more"><a href="/coun/base/photoList.do" class="more" title="더보기">더보기 +</a></span>
                </h4>

                <c:choose>
                    <c:when test="${ empty mainPhoto}">
                        <div class='center'><%= viewData %></div>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="rult" varStatus="status" items="${mainPhoto}">
                            <div class="gallery_list">
                                <div class="list">
                                    <a hhref='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")' title="${rult.bbsTitle}">
                                            <span class="img">
                                                <% if( isGallery ) { %>
                                                    <img src='<%= imgDomain %>/mimg/${rult.fileUri}/${rult.fileName}' alt='${rult.bbsTitle}'/>
                                                <% } else { %>
                                                    <img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/>
                                                <% } %>
                                            </span>
                                        <span class="subject">
                                                <strong class="tit">
                                                    ${fn:substring(rult.bbsTitle,0,fn:length(rult.bbsTitle)-3)}

                                                    <fmt:parseDate value="${rult.creDate}" var="chg_dttm" pattern="yyyy.MM.dd"/>
                                                    <fmt:parseNumber value="${chg_dttm.time / (1000*60*60*24)}" integerOnly="true" var="chgDttm"></fmt:parseNumber>
                                                    <c:if test="${today - chgDttm le 3}"><span class="new"><span class="skip">새 글</span>N</span></c:if> 
                                                </strong>
                                                <span class="date"><% if( isGallery ) { %>${rult.takeDate}<% } else { %>${rult.creDate}<% } %></span>
                                            </span>
                                    </a>
                                </div>
                            </div>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </div>
            <!-- //의정활동 gallery end -->

            <!--홍보영상 -->
            <div id="prvod">
                <h4>
                    의회 <span>홍보영상</span>
                    <span class="more"><a href="/coun/base/movieList.do" title="홍보영상 더보기"><span class="skip">더보기</span></a></span>
                </h4>
                <div class="pr_list">
                    <c:choose>
                        <c:when test="${ empty mainMovie}">
                            <div class='img center'><%= viewData %></div>
                        </c:when>
                        <c:otherwise>
                            <c:forEach var="rult" varStatus="status" items="${mainMovie}">
                                <a href='javascript:void(0);' onclick='fnAct<%= movieLayer ? "Media" : "Detail" %>("${rult.viewNo}","${rult.actMode}")'>
                                    <span class="play"></span>
                                    <c:choose>
                                        <c:when test="${!empty rult.fileUUID}">
                                            <span class="img"><img src='<% if( imgMode ) { %><%= webBase %>/GetImage.do?key=${rult.fileUUID}<% } else { %><%= webBase + uploadPath %>${rult.fileAbs}<% } %>' alt='${rult.bbsTitle}'/></span>
                                        </c:when>
                                        <c:otherwise>
                                            <video oncontextmenu='return false;' width='260' height='147' <% if(viewPoster){ %><c:if test="${!empty rult.fileUUID}"> poster='<% if( imgMode ) { %><%= webBase %>/GetImage.do?key=${rult.fileUUID}<% } else { %><%= webBase + uploadPath %>${rult.fileAbs}<% } %>'</c:if><% }else{ %> preload='metadata'<% } %>>
                                                <% if( vodMode ) { %>
                                                <source src='<%= webBase %>/GetMovie.do?key=${rult.downUUID}' type='video/${rult.downExt}'/>
                                                <% } else { %>
                                                <source src='<%= viewPort + webBase %>${rult.downAbs}' type='video/${rult.downExt}'/>
                                                <% } %>
                                            </video>
                                        </c:otherwise>
                                    </c:choose>
                                    <span class="subject">${rult.bbsTitle}</span>
                                </a>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <!-- //홍보영상 end -->

            <%= movieLayer ? "<div class='layer'><div class='bg'></div><div class='layer_pop' id='layVideo'></div></div>" : "" %>

        </div>
    </div>

    </div>
</form>

<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->

</div>


<script type="text/javascript" src="/js/main.js"></script>
</body>
</html>
