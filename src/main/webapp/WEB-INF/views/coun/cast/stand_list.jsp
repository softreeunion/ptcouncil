<%
////////////////////////////////////////////////////////////
// Program ID  : stand
// Description : 인터넷방송 > 상임위원회 [목록조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
//  v1.1    HexaMedia new0man      2020.05.08  대수별 기간 최소최대 선택일지정
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "상임위원회";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incCalendar.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb04_03").addClass("on").removeClass("noChild");
		fnActConfDate("${reEraCode}");
	});
	$(document).on("click",".ordr",function(e){e.preventDefault();$(".ordrList").hide();$(this).parent().find(".ordrList").show();});
	$(document).on("click",".cast",function(e){
		e.preventDefault();
		$("#wrap").waitMe({effect:"roundBounce",text:"영상회의록을 조회중입니다.",color:"#ddd",bg:"rgba(0,45,45,0.5)"});
		$.ajax({
			type          : "post", 
			url           : "<%= webBase %>/cmmn/loadCastVod.do", 
			dataType      : "html", 
			async         : false, 
			data          : {viewNo:$(this).data("key"),eraCode:$("#reEraCode").val()}, 
			success       : function(data,status,xhr  ){setTimeout(function(){$("#itemList").empty();$("#itemList").html(data);$("#wrap").waitMe("hide");},2000);}, 
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);$("#wrap").waitMe("hide");}
		});
	});
	var fnActConfDate   = function(era){
		$.ajax({
			type          : "post", 
			url           : "<%= webBase %>/cmmn/seleDate.do", 
			dataType      : "json", 
			data          : {era:era}, 
			success       : function(data,status,xhr  ){$("#reBgnDate,#reEndDate").datepicker("option","beforeShow",null).datepicker("option","minDate",data.min).datepicker("option","maxDate",data.max);$("#reBgnDate").val(data.min);$("#reEndDate").val(data.max);}, 
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};
	var fnActChanger    = function(){
		$.ajax({
			type          : "post", 
			url           : "<%= webBase %>/cmmn/seleRound.do", 
			dataType      : "html", 
			data          : {era:$("#reEraCode").val(),rnd:"${reRndCode}"}, 
			success       : function(data,status,xhr  ){$("#reRndCode").empty();$("#reRndCode").html(data);}, 
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
		fnActConfDate($("#reEraCode").val());
	};
	var fnActRetrieve   = function(page){
		doActLoadShow();
		$("#pageCurNo").val(isEmpty(page)?"1":page);
		$("#frmDefault").attr({"action":"stand.do","method":"post","target":"_self"}).submit();
	};
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/coun/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/coun/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="4"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 인터넷방송 &gt; 본회의 &gt; <%= subTitle %></div>
			<div class='section'><div class='program_search'>
				<p class='p_tit'>회의 동영상을 임의로 저장·편집·배포할 경우 저작권법 제 124조 및 제 136조에 의거하여 처벌 받을 수 있음을 유념하시기 바랍니다.</p>
				<table>
					<caption class='hide'>Search Detail</caption>
					<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
					<tbody><tr>
						<th scope='col' class='center'>대수</th>
						<td><label for='reEraCode' class='hide'>대수</label>
							<select id='reEraCode' name='reEraCode' class='w100' onchange='fnActChanger()'><c:choose><c:when test="${ empty eraList}">
								<option value='08'<c:if test="${'08' eq reEraCode}"> selected='selected'</c:if>>제8대</option></c:when><c:otherwise><c:forEach var="cate" varStatus="status" items="${eraList}">
								<option value='${cate.eraCode}'<c:if test="${cate.eraCode eq reEraCode}"> selected='selected'</c:if>>${cate.eraName}</option></c:forEach></c:otherwise></c:choose>
							</select>
						</td>
					</tr><tr>
						<th class='center'>위원회</th>
						<td><label for='reKndCode' class='hide'>위원회</label>
							<select id='reKndCode' name='reKndCode' class='w100'><option value=''>전체</option>
								<option value='201'<c:if test="${'201' eq reKndCode}"> selected='selected'</c:if>>의회운영위원회</option>
								<option value='202'<c:if test="${'202' eq reKndCode}"> selected='selected'</c:if>>자치행정위원회</option>
								<option value='203'<c:if test="${'203' eq reKndCode}"> selected='selected'</c:if>>산업건설위원회</option>
							</select>
						</td>
					</tr><tr>
						<th class='center'>회기</th>
						<td><label for='reRndCode' class='hide'>회기</label>
							<select id='reRndCode' name='reRndCode' class='w100'><option value=''>전체</option><c:if test="${!empty rndList}"><c:forEach var="cate" varStatus="status" items="${rndList}">
								<option value='${cate.roundCode}'<c:if test="${cate.roundCode eq reRndCode}"> selected='selected'</c:if>>${cate.roundName}</option></c:forEach></c:if>
							</select>
						</td>
					</tr><tr>
						<th class='center'>기간</th>
						<td><label for='reBgnDate' class='hide'>기간</label><input type='text' id='reBgnDate' name='reBgnDate' class='w90 center' maxlength='8' value='${reBgnDate}' placeholder='<%= dateHolder %>'/> ~ <label for='reEndDate' class='hide'>기간</label><input type='text' id='reEndDate' name='reEndDate' class='w90 center' maxlength='8' value='${reEndDate}' placeholder='<%= dateHolder %>'/></td>
					</tr><tr>
						<td colspan='2' class='center'><a href='javascript:void(0);' class='form_btn' onclick='fnActRetrieve()'>검색</a></td>
					</tr></tbody>
				</table>
			</div></div>
			<div class='section'><div class='list_half'>
				<div class='list_left'>
					<h2>회수/차수 선택</h2>
					<ul><c:choose><c:when test="${ empty dataList}">
						<li><%= noneData %></li>
						</c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${dataList}">
						<li><a href='javascript:void(0);' class='ordr'>${rult.rndTitle}</a>
							<c:if test="${!empty rult.childConf}">
							<ul class='ordrList dpn'><c:forEach var="chld" varStatus="status" items="${rult.childConf}">
								<li><a href='javascript:void(0);' class='cast' data-key='${chld.nextUUID}'>${chld.pntTitle}</a></li>
							</c:forEach></ul>
							</c:if>
						</li>
						</c:forEach></c:otherwise></c:choose>
					</ul>
					<c:if test="${!empty pageInfo}"><div class='list_paging'><ui:pagination paginationInfo="${pageInfo}" type="siteText" jsFunction="fnActRetrieve" /></div></c:if>
				</div>
				<div class='list_right'>
					<h2>안건명 선택</h2>
					<ul><li id='itemList'><p>영상회의록을 선택하여 주십시오.</p><ul></ul></ul>
				</div>
			</div></div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer.jsp" flush="true"><jsp:param name="qk" value="F"/></jsp:include>
	<!-- //footer -->
</div>
</form>
</body>
</html>