<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>


		<!-- sub menu -->
	  <div id="submenuArea" class="w sm_intro">
		<div class="sm_tit ">
			<h2>
			의회소개
			</h2>
		</div>
		<ul class="depth2">
		<!-- 메뉴 활성화시 class="current" 가 class="current_on"으로 바뀌도록 -->
					<li class="current"> <a href="/coun/cast/live.do" title="생방송">생방송</a> </li>
					<li class="current"> <a href="/coun/stand/movieList.do?ct=1" title="다시보기">다시보기</a>
						<ul>
							<li class="current"> <a href="/coun/stand/movieList.do?ct=1" title="본회의">본회의</a> </li>
							<li class="current"> <a href="/coun/stand/movieList.do?ct=2" title="의회운영위원회">의회운영위원회</a> </li>
							<li class="current"> <a href="/coun/stand/movieList.do?ct=3" title="기획행정위원회">기획행정위원회</a> </li>
							<li class="current"> <a href="/coun/stand/movieList.do?ct=4" title="복지환경위원회">복지환경위원회</a> </li>
							<li class="current"> <a href="/coun/stand/movieList.do?ct=5" title="산업건설위원회">산업건설위원회</a> </li>
							<li class="current"> <a href="/coun/stand/movieList.do?ct=6" title="특별위원회">특별위원회</a> </li>
						</ul>
					</li>
				</ul>
		
		
			<!-- sub menu end -->
			</div>
			
			
			