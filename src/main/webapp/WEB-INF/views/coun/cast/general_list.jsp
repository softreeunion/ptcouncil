<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />

	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>
	<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incCalendar.jspf"/>

	<script>
		var cate;
		function searchParam(name){
			var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
			if (results == null){
				return null;
			}
			else {
				return decodeURI(results[1]) || 0;
			}
		}
		$(document).ready(function(){
			cate = searchParam("ct");
			$(".menu5").removeClass("current");
			$(".menu5").addClass("current_on");
			$(".depth2 > li:nth-child(8)").addClass("current_on");
			$(".depth2 > li:nth-child(8) > ul > li:nth-child(" + cate + ")").addClass("current_on");
			fnActConfDate("${reEraCode}");
		});
		$(document).on("click",".ordr",function(e){e.preventDefault();$(".ordrList").hide();$(this).parent().find(".ordrList").show();});
		$(document).on("click",".cast",function(e){
			e.preventDefault();
			$("#wrap").waitMe({effect:"roundBounce",text:"영상회의록을 조회중입니다.",color:"#ddd",bg:"rgba(0,45,45,0.5)"});
			$.ajax({
				type          : "post",
				url           : "<%= webBase %>/cmmn/loadCastVod.do",
				dataType      : "html",
				async         : false,
				data          : {viewNo:$(this).data("key"),eraCode:$("#reEraCode").val()},
				success       : function(data,status,xhr  ){setTimeout(function(){$("#itemList").empty();$("#itemList").html(data);$("#wrap").waitMe("hide");},2000);},
				error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);$("#wrap").waitMe("hide");}
			});
		});
		var fnActConfDate   = function(era){
			$.ajax({
				type          : "post",
				url           : "<%= webBase %>/cmmn/seleDate.do",
				dataType      : "json",
				data          : {era:era},
				success       : function(data,status,xhr  ){$("#reBgnDate,#reEndDate").datepicker("option","beforeShow",null).datepicker("option","minDate",data.min).datepicker("option","maxDate",data.max);$("#reBgnDate").val(data.min);$("#reEndDate").val(data.max);},
				error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
			});
		};
		var fnActChanger    = function(){
			$.ajax({
				type          : "post",
				url           : "<%= webBase %>/cmmn/seleRound.do",
				dataType      : "html",
				data          : {era:$("#reEraCode").val(),rnd:"${reRndCode}"},
				success       : function(data,status,xhr  ){$("#reRndCode").empty();$("#reRndCode").html(data);},
				error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
			});
			fnActConfDate($("#reEraCode").val());
		};
		var fnActRetrieve   = function(page){
			doActLoadShow();
			$("#pageCurNo").val(isEmpty(page)?"1":page);
			$("#frmDefault").attr({"action":"general.do?ct="+cate,"method":"post","target":"_self"}).submit();
		};
	</script>


</head>

<body>
<%
	String ct = request.getParameter("ct");
	String subTitle = "";
	if("1".equals(ct)) subTitle = "본회의";
	else if("2".equals(ct)) subTitle = "의회운영위원회";
	else if("3".equals(ct)) subTitle = "기획행정위원회";
	else if("4".equals(ct)) subTitle = "복지환경위원회";
	else if("5".equals(ct)) subTitle = "산업건설위원회";
	else if("6".equals(ct)) subTitle = "예산결산특별위원회";
%>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<!-- 메인메뉴 end -->
	<div id="subContent">
		<!-- submenu -->
		<!-- 왼쪽메뉴 -->
		<jsp:include page="/WEB-INF/views/coun/conf/conf_left.jsp" flush="true"/>
		<!-- sbumenu end -->


		<h3 class="skip">본문내용 he</h3>
		<div id="content">

			<div id="subTitle">
				<!-- sub 제목부분 -->
				<h3><%=subTitle%>&nbsp;</h3>
			</div>
			<div id="sub_default">

				<!-- program 들어갈부분 -->
				<form id='frmDefault' name='frmDefault'>
					<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
					<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
					<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>

					<div class='board_search'>
						<p>회의 동영상을 임의로 저장·편집·배포할 경우 저작권법 제 124조 및 제 136조에 의거하여 처벌 받을 수 있음을 유념하시기 바랍니다.</p><br/>
						<dl>
							<dt>대수</dt>
							<dd><label for='reEraCode' class='hide'>대수</label>
								<select id='reEraCode' name='reEraCode' class='wp100' onchange='fnActChanger()'><c:choose><c:when test="${ empty eraList}">
									<option value='08'<c:if test="${'08' eq reEraCode}"> selected='selected'</c:if>>제8대</option></c:when><c:otherwise><c:forEach var="cate" varStatus="status" items="${eraList}">
									<option value='${cate.eraCode}'<c:if test="${cate.eraCode eq reEraCode}"> selected='selected'</c:if>>${cate.eraName}</option></c:forEach></c:otherwise></c:choose>
								</select>
							</dd>
							<dt>회기</dt>
							<dd><label for='reRndCode' class='hide'>회기</label>
								<select id='reRndCode' name='reRndCode' class='wp100'><option value=''>전체</option><c:if test="${!empty rndList}"><c:forEach var="cate" varStatus="status" items="${rndList}">
									<option value='${cate.roundCode}'<c:if test="${cate.roundCode eq reRndCode}"> selected='selected'</c:if>>${cate.roundName}</option></c:forEach></c:if>
								</select>
							</dd>
							<dt>기간</dt>
							<dd><label for='reBgnDate' class='hide'>기간</label>
								<input type='text' id='reBgnDate' name='reBgnDate' class='wp100 center' maxlength='8' value='${reBgnDate}' placeholder='<%= dateHolder %>'/>
								~
								<label for='reEndDate' class='hide'>기간</label>
								<input type='text' id='reEndDate' name='reEndDate' class='wp100 center' maxlength='8' value='${reEndDate}' placeholder='<%= dateHolder %>'/>
								<a href='javascript:void(0);' class='form_btn' onclick='fnActRetrieve()'>검색</a>
							</dd>
						</dl>
					</div>

					<div class='generalArea'>
						<div class='list_left'>
							<h4>회수/차수 선택</h4>
							<ul class="l_list">
							<c:choose>
								<c:when test="${ empty dataList}">
									<li><%= noneData %></li>
								</c:when>
								<c:otherwise>
									<c:forEach var="rult" varStatus="status" items="${dataList}">
										<li><a href='javascript:void(0);' class='ordr'>${rult.rndTitle}</a>
											<c:if test="${!empty rult.childConf}">
												<ul class='ordrList dpn'>
													<c:forEach var="chld" varStatus="status" items="${rult.childConf}">
														<li><a href='javascript:void(0);' class='cast' data-key='${chld.nextUUID}'>${chld.pntTitle}</a></li>
													</c:forEach>
												</ul>
											</c:if>
										</li>
									</c:forEach>
								</c:otherwise>
							</c:choose>
							</ul>
							<c:if test="${!empty pageInfo}"><div class='paging' style="padding-top:10px"><ui:pagination paginationInfo="${pageInfo}" type="siteText" jsFunction="fnActRetrieve" /></div></c:if>
						</div>
						<div class='list_right'>
							<h4>안건명 선택</h4>
							<ul class="l_list"><li id='itemList'><p>영상회의록을 선택하여 주십시오.</p><ul></ul></ul>
						</div>
					</div>


				</form>
				<!--  program 들어갈부분 end -->

			</div>



		</div>
	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
