<%
////////////////////////////////////////////////////////////
// Program ID  : speech_list
// Description : 인터넷방송 > 7분자유발언 [목록조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "7분자유발언";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb04_07").addClass("on").removeClass("noChild");
	});
	$(document).on("click",".cast",function(e){
		e.preventDefault();
		$("#wrap").waitMe({effect:"roundBounce",text:"영상회의록을 조회중입니다.",color:"#ddd",bg:"rgba(0,45,45,0.5)"});
		$.ajax({
			type          : "post", 
			url           : "<%= webBase %>/cmmn/loadCastMem.do", 
			dataType      : "html", 
			async         : false, 
			data          : {viewNo:$(this).data("key"),eraCode:$("#reEraCode").val(),lstType:"S"}, 
			success       : function(data,status,xhr  ){setTimeout(function(){$("#itemList").empty();$("#itemList").html(data);$("#wrap").waitMe("hide");},2000);}, 
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);$("#wrap").waitMe("hide");}
		});
	});
	var fnActRetrieve   = function(page){
		doActLoadShow();
		$("#pageCurNo").val(isEmpty(page)?"1":page);
		$("#frmDefault").attr({"action":"speech.do","method":"post","target":"_self"}).submit();
	};
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/coun/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/coun/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="4"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 인터넷방송 &gt; <%= subTitle %></div>
			<div class='section'><div class='program_search'>
				<p class='p_tit'>회의 동영상을 임의로 저장·편집·배포할 경우 저작권법 제 124조 및 제 136조에 의거하여 처벌 받을 수 있음을 유념하시기 바랍니다.</p>
				<table>
					<caption class='hide'>Search Detail</caption>
					<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
					<tbody><tr>
						<th scope='col' class='center'>대수</th>
						<td><label for='reEraCode' class='hide'>대수</label>
							<select id='reEraCode' name='reEraCode' class='w100' onchange='fnActChanger()'><c:choose><c:when test="${ empty eraList}">
								<option value='08'<c:if test="${'08' eq reEraCode}"> selected='selected'</c:if>>제8대</option></c:when><c:otherwise><c:forEach var="cate" varStatus="status" items="${eraList}">
								<option value='${cate.eraCode}'<c:if test="${cate.eraCode eq reEraCode}"> selected='selected'</c:if>>${cate.eraName}</option></c:forEach></c:otherwise></c:choose>
							</select>
						</td>
					</tr><tr>
						<td colspan='2' class='center'><a href='javascript:void(0);' class='form_btn' onclick='fnActRetrieve()'>검색</a></td>
					</tr></tbody>
				</table>
			</div></div>
			<div class='section'><div class='list_half'>
				<div class='list_left02'>
					<h2>의원 선택</h2>
					<ul><c:choose><c:when test="${ empty dataList}">
						<li><%= noneData %></li>
						</c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${dataList}">
						<li><a href='javascript:void(0);' class='cast' data-key='${rult.memName}'>${rult.memName}</a></li>
						</c:forEach></c:otherwise></c:choose>
					</ul>
				</div>
				<div class='list_right'>
					<h2>안건명 선택</h2>
					<ul><li id='itemList'><p>의원을 선택하여 주십시오.</p><ul></ul></ul>
				</div>
			</div></div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer.jsp" flush="true"><jsp:param name="qk" value="F"/></jsp:include>
	<!-- //footer -->
</div>
</form>
</body>
</html>