<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />

	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>


	<script>
		$(document).ready(function(){
			$(".depth2 > li:nth-child(2)").addClass("current_on");
			<% if( isGallery ) { %>
			$(".fotorama").fotorama().data("fotorama").show(${chldShow});
			$(".fotorama").on("fotorama:show",function(e,fotorama){$("#fotTitle").text(fotorama.activeFrame.caption);}).fotorama();
			$(".photo_view_title").removeHighlight().highlight($("#condValue").val());
			<% } else { %>
			$("#table_view").removeHighlight().highlight($("#condValue").val());
			<% } %>
		});
		$(window).load(function(){<% if( isGallery ) { %><% } %>});
		var fnActReturn     = function(seq){
			doActLoadShow();
			$("#frmDefault").attr({"action":"photo"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self"}).submit();
		};
		var fnActDetail     = function(view){
			doActLoadShow();
			$("#viewNo").val(view);
			$("#frmDefault").attr({"action":"photoView.do","method":"post","target":"_self"}).submit();
		};
	</script>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<!-- 메인메뉴 end -->
	<div id="subContent">
		<!-- submenu -->
		<jsp:include page="/WEB-INF/views/coun/acts/acts_left.jsp" flush="true"/>
		<!-- sbumenu end -->
		<h3 class="skip">본문내용 he</h3>
		<div id="content">
			<div id="subTitle">
				<h3>포토 의정활동</h3>
			</div>
			<div id="sub_default">

				<form id='frmDefault' name='frmDefault'>
					<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
					<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
					<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
					<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
					<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
					<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
					<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
					<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
					<input type='hidden' id='condDele'      name='condDele'/>


					<% if( isGallery ) { %>
					<table class='board_view mT20'>
						<caption class='hide'>Previous/Next</caption>
						<colgroup><col style='width:100%;'/></colgroup>
						<tbody>
						<tr>
							<th class="left">${dataView.bbsTitle} <span class="date">${dataView.takeDate}</span>
								<div class="photo_head">
									<ul >
										<li><a href='javascript:void(0);' onclick='shareFacebook("<%= webPath %>${viewLink}")'><img src='/images/sns_facebook.png'  alt='facebook' title='페이스북'/></a></a></li>
										<li><a href='javascript:void(0);' onclick='shareTwitter("<%= webPath %>${viewLink}",encodeURIComponent($("#fotTitle").text()))'><img src='/images/sns_twitter.png'  alt='트위터' title='twitter'/></a></li>
										<li><a href='javascript:void(0);' onclick='shareBand("<%= webPath %>${viewLink}")'><img src='/images/sns_naverband.png'  alt='naverBand' title='네이버밴드'/></a></li>
										<li><a href='javascript:void(0);' onclick='shareKakaoStory("<%= webPath %>${viewLink}")'><img src='/images/sns_kakaostory.png'  alt='kakaoStory' title='카카오스토리'/></a></li>
										<li><a href='javascript:void(0);' onclick='shareTrackback("<%= webPath %>${viewLink}")'><img src='/images/sns_trackback.png'  alt='trackBack' title='주소 공유'/></span></li>
										<li><a href='javascript:void(0);' onclick='fnImgPrint($("#fotTitle").text()+" (${dataView.creDate})")'><img src='/images/sns_printer.png'  alt='print' title='문서 출력'/></a></li>
										<li><a href='javascript:void(0);' onclick='fnImgExpand($("#fotTitle").text()+" (${dataView.creDate})")'><img src='/images/sns_expand.png'  alt='expand' title='크게 보기'/></a></li>
										<li><a href='javascript:void(0);' onclick='fnImgDown($("#fotTitle").text()+" (${dataView.creDate})")'><img src='/images/sns_down.png'  alt='down' title='파일 저장'/></a></li>
									</ul>
								</div>

							</th>
						</tr>
						<tr>
							<td class="line_out">
								<div class='fotorama' data-nav='thumbs' data-allowfullscreen='true' data-autoplay='false' data-captions='false' data-width='100%' data-maxwidth='100%'>
									<c:if test="${!empty chldFile}">
										<c:forEach var="rult" varStatus="status" items="${chldFile}">
											<a href='<%= imgDomain %>/vimg/${rult.fileUri}/${rult.fileName}' data-caption='${rult.bbsTitle}'>
											<div data-thumb='<%= imgDomain %>/mimg/${rult.fileUri}/${rult.fileName}'></div></a>
										</c:forEach>
									</c:if>
								</div>

							</td>
						</tr>
						<tr>
							<td class="line_out">${dataView.contDesc}</td>
						</tr>
						</tbody>
					</table>
					<% } else { %>
						<div class='table_view'><table>
							<caption class='hide'>Board View</caption>
							<colgroup><col style='width:10%;'/><col style='width:30%;'/><col style='width:10%;'/><col style='width:20%;'/><col style='width:10%;'/><col style='width:20%;'/></colgroup>
							<tbody><tr>
								<th>제목</th>
								<td colspan='5'>${dataView.bbsTitle}</td>
							</tr><tr>
								<th scope='col'>작성자</th>
								<td>${dataView.creName}</td>
								<th scope='col'>작성일</th>
								<td>${dataView.creDate}</td>
								<th scope='col'>조회수</th>
								<td>${dataView.hitCount}</td>
							</tr><tr class='h250'>
								<th>썸네일</th>
								<td colspan='5'><img src='<%= webBase %>/GetImage.do?key=${dataView.fileUUID}' alt='${dataView.bbsTitle}' style='width:100%;height:auto;'/></td>
							</tr><tr class='h100'>
								<th>내용</th>
								<td colspan='5' class='cont'><c:choose><c:when test="${'P' eq dataView.contPattern}">${dataView.contDescBR}</c:when><c:otherwise>${dataView.contDesc}</c:otherwise></c:choose></td>
							</tr></tbody>
						</table></div>
					<% } %>
					<div class="right">
						<a href='javascript:void(0);' class='btn btn-primary' onclick='fnActReturn("seq")'>목록</a>
					</div>
					<c:if test="${'T' eq moveItem}">
					<div class='board_view mT20'>
						<table>
							<caption class='hide'>Previous/Next</caption>
							<colgroup><col style='width:10%;'/><col style='width:90%;'/></colgroup>
							<tbody><tr>
								<th scope='col'>이전글</th>
								<td>&nbsp;&nbsp;<c:choose><c:when test="${ empty prevView}">이전 글이 존재 하지 않습니다.</c:when><c:otherwise>${prevView.gabSpace}<a href='javascript:void(0);' onclick='fnActDetail("${prevView.viewNo}")'><c:if test="${'Y' eq prevView.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${prevView.bbsTitle}</a></c:otherwise></c:choose></td>
							</tr><tr>
								<th scope='col'>다음글</th>
								<td>&nbsp;&nbsp;<c:choose><c:when test="${ empty nextView}">다음 글이 존재 하지 않습니다.</c:when><c:otherwise>${nextView.gabSpace}<a href='javascript:void(0);' onclick='fnActDetail("${nextView.viewNo}")'><c:if test="${'Y' eq nextView.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${nextView.bbsTitle}</a></c:otherwise></c:choose></td>
							</tr></tbody>
						</table>
					</div>
					</c:if>
				</form>


			</div>

		</div>
	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
