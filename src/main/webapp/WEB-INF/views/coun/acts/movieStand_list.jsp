<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Calendar" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	String subTitle				= "홍보 영상";
	boolean viewPoster			= true;
	out.clearBuffer();

	Date nowTime = new Date();
	SimpleDateFormat sf = new SimpleDateFormat("yyyy.MM.dd");

	//1년전
	Calendar mon = Calendar.getInstance();
	mon.add(Calendar.MONTH , -12);
	String bfDate = new SimpleDateFormat("yyyy.MM.dd").format(mon.getTime());
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />

	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>
	<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incCalendar.jspf"/>
	<style>
		.vod_mb10 {margin-bottom:10px !important;}
		.vod_comment{width:100%;max-height:200px;margin:0 0 2em;overflow-y:auto;padding:1em;border:1px solid #ddd;background:#fafafa;box-sizing:border-box;}
	</style>

	<script>
		function searchParam(name){
			var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
			if (results == null){
				return null;
			}
			else {
				return decodeURI(results[1]) || 0;
			}
		};
		var cate;
		$(document).ready(function(){
			cate = searchParam("ct");
			$(".depth2 > li:nth-child(2)").addClass("current_on");
			if( cate === "1") {
				$(".depth2 > li:nth-child(2) > ul > li:nth-child(1)").addClass("current_on");
			} else if(cate === "2") {
				$(".depth2 > li:nth-child(2) > ul > li:nth-child(2)").addClass("current_on");
			} else if(cate === "3") {
				$(".depth2 > li:nth-child(2) > ul > li:nth-child(3)").addClass("current_on");
			} else if(cate === "4") {
				$(".depth2 > li:nth-child(2) > ul > li:nth-child(4)").addClass("current_on");
			} else if(cate === "5") {
				$(".depth2 > li:nth-child(2) > ul > li:nth-child(5)").addClass("current_on");
			} else if(cate === "6") {
				$(".depth2 > li:nth-child(2) > ul > li:nth-child(6)").addClass("current_on");
			}
			$("#divVideo").css("border-bottom","2px solid #333");
		});
		var fnActInitial    = function(){
			$("#condType").val("A");
			$("#condValue").val("");
			$("#bgnScDate").val("<%=bfDate%>");
			$("#endScDate").val("<%=sf.format(nowTime)%>");
			fnActRetrieve();
		};
		var fnActRetrieve   = function(page){
			doActLoadShow();
			$("#pageCurNo").val(isEmpty(page)?"1":page);
			$("#frmDefault").attr({"action":"movieList.do?ct="+cate,"method":"post","target":"_self"}).submit();
		};
		var fnActDetail     = function(view){<% if( movieLayer ) { %>
			$.ajax({
				type          : "post",
				url           : "<%= webBase %>/coun/mediaView.do",
				dataType      : "html",
				data          : {viewNo:view,actMode:"false"},
				success       : function(data,status,xhr  ){var $LV=$("#layVideo");$LV.empty();$LV.html(data);media_layer();},
				error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);}
			});<% } else {
		if( movieView ) { %>
			$("#viewNo").val(view);
			fnActRetrieve(${pageCurNo});<% } else { %>
			doActLoadShow();
			$("#viewNo").val(view);
			$("#frmDefault").attr({"action":"formerView.do","method":"post","target":"_self"}).submit();<% }} %>
		};
	</script>

</head>

<body>
<%
	String ct = request.getParameter("ct");
	String title = "";
	if("1".equals(ct)) title = "본회의";
	else if("2".equals(ct)) title = "의회운영위원회";
	else if("3".equals(ct)) title = "기획행정위원회";
	else if("4".equals(ct)) title = "복지환경위원회";
	else if("5".equals(ct)) title = "산업건설위원회";
	else if("6".equals(ct)) title = "특별위원회";
%>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<form id='frmDefault' name='frmDefault'>
		<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
		<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
		<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>

		<div id="subContent">
			<!-- submenu -->
			<jsp:include page="/WEB-INF/views/coun/cast/cast_left.jsp" flush="true"/>
			<!-- sbumenu end -->
			<h3 class="skip">본문내용 he</h3>
			<div id="content">
				<div id="subTitle">
					<h3><%=title%></h3>
				</div>
				<div id="sub_default">

					<% if( !movieLayer && movieView ) { %>
					<c:if test="${!empty dataView}">
						<div class='table_view' id='divVideo'><table>
							<caption class='hide'>Board View</caption>
							<colgroup><col style='width:100%;'/></colgroup>
							<tbody><tr class='h250'>
								<td class='nonebd_l'><video id='vod' oncontextmenu='return false;' width='100%' controls<% if( viewPoster ) { %> preload='metadata'<c:if test="${!empty dataView.fileUUID}"> poster='<% if( imgMode ) { %><%= webBase %>/GetImage.do?key=${dataView.fileUUID}<% } else { %><%= webBase + uploadPath %>${dataView.fileAbs}<% } %>'</c:if><% } else { %> autoplay preload='auto'<% } %>><% if( vodMode ) { %>
									<source src='<%= webBase %>/GetMovie.do?key=${dataView.downUUID}' type='video/${dataView.downExt}'/><% } else { %>
									<source src='<%= viewPort + webBase %>${dataView.downAbs}' type='video/${dataView.downExt}'/><% } %>
								</video></td>
							</tr><tr>
								<th class='nonebd_l'>${dataView.bbsTitle}</th>
							</tr></tbody>
						</table></div><br/>
					</c:if>
					<% } %>


					<div class='board_search'>
						<dl>
							<dt>회차</dt>
							<dd><label for='condType' class='hide'>검색영역</label>
								<select id='reRndCode' name='reRndCode' class='wp100'>
									<option value=''>전체</option>
									<option value='252' <c:if test="${'252' eq reRndCode}"> selected='selected'</c:if>>252회</option>
									<option value='251' <c:if test="${'251' eq reRndCode}"> selected='selected'</c:if>>251회</option>
									<option value='250' <c:if test="${'250' eq reRndCode}"> selected='selected'</c:if>>250회</option>
									<option value='249' <c:if test="${'249' eq reRndCode}"> selected='selected'</c:if>>249회</option>
									<option value='248' <c:if test="${'248' eq reRndCode}"> selected='selected'</c:if>>248회</option>
									<option value='247' <c:if test="${'247' eq reRndCode}"> selected='selected'</c:if>>247회</option>
									<option value='246' <c:if test="${'246' eq reRndCode}"> selected='selected'</c:if>>246회</option>
									<option value='245' <c:if test="${'245' eq reRndCode}"> selected='selected'</c:if>>245회</option>
									<option value='244' <c:if test="${'244' eq reRndCode}"> selected='selected'</c:if>>244회</option>
									<option value='243' <c:if test="${'243' eq reRndCode}"> selected='selected'</c:if>>243회</option>
									<option value='242' <c:if test="${'242' eq reRndCode}"> selected='selected'</c:if>>242회</option>
									<option value='241' <c:if test="${'241' eq reRndCode}"> selected='selected'</c:if>>241회</option>
									<option value='240' <c:if test="${'240' eq reRndCode}"> selected='selected'</c:if>>240회</option>
									<option value='239' <c:if test="${'239' eq reRndCode}"> selected='selected'</c:if>>239회</option>
									<option value='238' <c:if test="${'238' eq reRndCode}"> selected='selected'</c:if>>238회</option>
									<option value='237' <c:if test="${'237' eq reRndCode}"> selected='selected'</c:if>>237회</option>
									<option value='236' <c:if test="${'236' eq reRndCode}"> selected='selected'</c:if>>236회</option>
									<option value='235' <c:if test="${'235' eq reRndCode}"> selected='selected'</c:if>>235회</option>
									<option value='234' <c:if test="${'234' eq reRndCode}"> selected='selected'</c:if>>234회</option>
									<option value='233' <c:if test="${'233' eq reRndCode}"> selected='selected'</c:if>>233회</option>
									<option value='232' <c:if test="${'232' eq reRndCode}"> selected='selected'</c:if>>232회</option>
									<option value='231' <c:if test="${'231' eq reRndCode}"> selected='selected'</c:if>>231회</option>
									<option value='230' <c:if test="${'230' eq reRndCode}"> selected='selected'</c:if>>230회</option>
									<option value='229' <c:if test="${'229' eq reRndCode}"> selected='selected'</c:if>>229회</option>
									<option value='228' <c:if test="${'228' eq reRndCode}"> selected='selected'</c:if>>228회</option>
									<option value='227' <c:if test="${'227' eq reRndCode}"> selected='selected'</c:if>>227회</option>
									<option value='226' <c:if test="${'226' eq reRndCode}"> selected='selected'</c:if>>226회</option>
									<option value='225' <c:if test="${'225' eq reRndCode}"> selected='selected'</c:if>>225회</option>
									<option value='224' <c:if test="${'224' eq reRndCode}"> selected='selected'</c:if>>224회</option>
									<option value='223' <c:if test="${'223' eq reRndCode}"> selected='selected'</c:if>>223회</option>
									<option value='222' <c:if test="${'222' eq reRndCode}"> selected='selected'</c:if>>222회</option>
									<option value='221' <c:if test="${'221' eq reRndCode}"> selected='selected'</c:if>>221회</option>
									<option value='220' <c:if test="${'220' eq reRndCode}"> selected='selected'</c:if>>220회</option>
									<option value='219' <c:if test="${'219' eq reRndCode}"> selected='selected'</c:if>>219회</option>
									<option value='218' <c:if test="${'218' eq reRndCode}"> selected='selected'</c:if>>218회</option>
									<option value='217' <c:if test="${'217' eq reRndCode}"> selected='selected'</c:if>>217회</option>
									<option value='216' <c:if test="${'216' eq reRndCode}"> selected='selected'</c:if>>216회</option>
									<option value='215' <c:if test="${'215' eq reRndCode}"> selected='selected'</c:if>>215회</option>
									<option value='214' <c:if test="${'214' eq reRndCode}"> selected='selected'</c:if>>214회</option>
									<option value='213' <c:if test="${'213' eq reRndCode}"> selected='selected'</c:if>>213회</option>
									<option value='212' <c:if test="${'212' eq reRndCode}"> selected='selected'</c:if>>212회</option>
									<option value='211' <c:if test="${'211' eq reRndCode}"> selected='selected'</c:if>>211회</option>
									<option value='210' <c:if test="${'210' eq reRndCode}"> selected='selected'</c:if>>210회</option>
									<option value='209' <c:if test="${'209' eq reRndCode}"> selected='selected'</c:if>>209회</option>
									<option value='208' <c:if test="${'208' eq reRndCode}"> selected='selected'</c:if>>208회</option>
									<option value='207' <c:if test="${'207' eq reRndCode}"> selected='selected'</c:if>>207회</option>
									<option value='206' <c:if test="${'206' eq reRndCode}"> selected='selected'</c:if>>206회</option>
									<option value='205' <c:if test="${'205' eq reRndCode}"> selected='selected'</c:if>>205회</option>
									<option value='204' <c:if test="${'204' eq reRndCode}"> selected='selected'</c:if>>204회</option>
									<option value='203' <c:if test="${'203' eq reRndCode}"> selected='selected'</c:if>>203회</option>
									<option value='202' <c:if test="${'202' eq reRndCode}"> selected='selected'</c:if>>202회</option>
									<option value='201' <c:if test="${'201' eq reRndCode}"> selected='selected'</c:if>>201회</option>
									<option value='200' <c:if test="${'200' eq reRndCode}"> selected='selected'</c:if>>200회</option>
								</select></dd>
							<dt>검색어</dt>
							<dd>
								<label for='condValue' class='hide'>검색어</label>
								<input type='text' id='condValue' name='condValue' class='w25' maxlength='30' value='${condValue}' placeholder='검색어를 입력하세요.'/>
								<a href='javascript:void(0);' class='form_btn' onclick='fnActRetrieve()'>검색</a>
							</dd>
						</dl>
					</div>
					<div class='board_total'>전체 <strong>${listCnt}</strong>건 &nbsp; ${pageCurNo} / ${pageInfo.totalPageCount}</div>
					<table class="board_list">
						<caption class='hide'>Board List</caption>
						<colgroup><col style='width:10%;' class='w_no'/><col style='width:50%;'/><col style='width:15%;' class='w_wn'/><col style='width:15%;' class='w_wd'/><col style='width:10%;' class='w_wr'/></colgroup>
						<thead><tr>
							<th scope='col'>번호</th>
							<th scope='col'>제목</th>
							<th scope='col'>작성자</th>
							<th scope='col'>등록일</th>
							<th scope='col'>조회</th>
						</tr></thead>
						<tbody>
						<c:choose><c:when test="${ empty dataList}">
							<tr class='h200'><td colspan='5'><%= noneData %></td></tr>
						</c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${dataList}">
							<tr class='pnc' onclick='fnActDetail("${rult.viewNo}")'>
								<td>${rult.rowIdx}</td>
								<td class='left'>${rult.gabSpace}<c:if test="${'Y' eq rult.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${rult.bbsTitle}</td>
								<td>${rult.creName}</td>
								<td>${rult.creDate}</td>
								<td>${rult.hitCount}</td>
							</tr>
						</c:forEach></c:otherwise></c:choose>
						</tbody>
					</table>
					<c:if test="${!empty pageInfo}"><div class='paging'><ui:pagination paginationInfo="${pageInfo}" type="baseText" jsFunction="fnActRetrieve" /></div></c:if>

				</div><%= movieLayer ? "<div class='layer'><div class='bg'></div><div class='layer_pop' id='layVideo'></div></div>" : "" %>

			</div>
		</div>
	</form>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
