<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	out.clearBuffer(); %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta property="og:image" content="" />
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>평택시의회 > 연간회기일정</title>
	<link rel="shortcut icon" href="images/favicon.ico" />
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />

	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>
</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
	<div id="subContent">
		<!-- sub menu -->

		<!-- sub menu end-->
		<h3 class="skip">본문내용</h3>
		<div id="content">
			<div id="subTitle">
				<h3>연간회기일정&nbsp;</h3>
			</div>
			<div id="sub_default">
				<!-- sub_sub_default 사이에 프로그램 들어감 -->

				<h4>2024년도 의회운영 기본일정(안)</h4>
				<p class="round_txt">
					【회기 운영 계획 : 7회 83일 (정례회 2회 38일, 임시회 5회 45일)】
				</p>
				<table class="board_list">
					<caption class="hide">
						2024년도 의회운영 기본일정(안)
					</caption>
					<colgroup>
						<col style="width: 10%" />
						<col style="width: 12%" />
						<col style="width: 18%" />
						<col style="width: 10%" />
						<col style="width: 23%" />
						<col style="width: 27%" />
					</colgroup>
					<thead>
					<tr>
						<th scope="col">월</th>
						<th scope="col">회기</th>
						<th scope="col">기간</th>
						<th scope="col">일수</th>
						<th scope="col">주요안건</th>
						<th scope="col">비고</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td class="line_out">계</td>
						<td class="line_on">7회</td>
						<td>-</td>
						<td>83일</td>
						<td class="left">-</td>
						<td class="left">-</td>
					</tr>
					<tr>
						<td class="line_out">1~2</td>
						<td class="line_on">제244회<br />임시회</td>
						<td>1.29(월)~2.5(월)</td>
						<td>8일</td>
						<td class="left">
							<ul class="dot">
								<li>조례 및 기타 안건</li>
								<li>업무보고 청취</li>
								<li>결산검사위원 선임</li>
								<li>시정질문</li>
							</ul>
						</td>
						<td class="left">
							<ul class="dot">
								<li>간담회 1.16(화)</li>
								<li>설연휴 2.9~2.12</li>
								<li>간담회 2.20(화)</li>
								<li>상임위 비교견학 2.22(목)~2.27(화)</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td rowspan="1" class="line_out">3</td>
						<td class="line_on">제245회<br />임시회</td>
						<td>3.4(월)~3.15(금)</td>
						<td>12일</td>
						<td class="left">
							<ul class="dot">
								<li>조례 및 기타 안건</li>
								<li>행정사무감사 계획 승인</li>
								<li>추경 예산안</li>
								<li>현장활동</li>
								<li>시정질문</li>
							</ul>
						</td>
						<td rowspan="1" class="left">
							<ul class="dot">
								<li>삼일절 3.1</li>
							</ul>
						</td>
					</tr>

					<tr>
						<td class="line_out">4~5</td>
						<td class="line_on">-</td>
						<td>-</td>
						<td>-</td>
						<td class="left">-</td>
						<td class="left">
							<ul class="dot">
								<li>결산검사(20일) [4.1~4.20]</li>
								<li>국회의원선거 4.10</li>
								<li>국외연수 4.22(월)~5.3(금)</li>
								<li>어린이날 대체휴무 5.6</li>
								<li>간담회 5.14(화)</li>
								<li>석가탄신일 5.15</li>
								<li>의정연수 5.23(목)~5.24(금)</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td rowspan="2" class="line_out">6</td>
						<td class="line_on">제246회<br />정례회</td>
						<td>6.4(화)~6.21(금)</td>
						<td>18일</td>
						<td class="left">
							<ul class="dot">
								<li>조례 및 기타 안건</li>
								<li>2024년 행정사무감사</li>
								<li>2023회계연도 예비비 지출승인</li>
								<li>2023회계연도 결산승인</li>
								<li>시정질문</li>
							</ul>
						</td>
						<td rowspan="2" class="left">
							<ul class="dot">
								<li>현충일 6.6</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td class="line_on">제247회<br />임시회</td>
						<td>6.27목)~6.28(금)</td>
						<td>2일</td>
						<td class="left" style="border-right: solid 1px #d2d2d2">
							<ul class="dot">
								<li>제9대 후반기</li>
								<li>- 의장, 부의장 선출</li>
								<li>- 상임 의원 선임</li>
								<li>- 상임 위원장 선출</li>
								<li>- 운영위원 선임</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td class="line_out">7</td>
						<td class="line_on">제248회<br />임시회</td>
						<td>7.15(월)~7.22(월)</td>
						<td>8일</td>
						<td class="left">
							<ul class="dot">
								<li>조례 및 기타 안건</li>
								<li>업무보고 청취</li>
								<li>시정질문</li>
							</ul>
						</td>
						<td class="left">-</td>
					</tr>
					<tr>
						<td class="line_out">8~9</td>
						<td class="line_on">-</td>
						<td>-</td>
						<td>-</td>
						<td class="left">-</td>
						<td class="left">
							<ul class="dot">
								<li>간담회 9.10(화)</li>
								<li>추석연휴 9.15~9.18</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td class="line_out">10</td>
						<td class="line_on">제249회<br />임시회</td>
						<td>10.7(월)~10.21(월)</td>
						<td>15일</td>
						<td class="left">
							<ul class="dot">
								<li>조례 및 기타 안건</li>
								<li>추경 예산안</li>
								<li>현장활동</li>
								<li>시정질문</li>
							</ul>
						</td>
						<td class="left">
							<ul class="dot">
								<li>개천절 10.3 / 한글날 10.9</li>
								<li>의정연수 10.24(목)~10.25(금)</li>
								<li>상임위 비교견학 10.28(월)~11.1(금)</li>
							</ul>
						</td>
					</tr>

					<tr>
						<td class="line_out">11 ~ 12</td>
						<td class="line_on">제250회<br />제2차<br />정례회</td>
						<td>11.29(금)~12.18(수)</td>
						<td>20일</td>
						<td class="left">
							<ul class="dot">
								<li>조례 및 기타 안건</li>
								<li>시정질문</li>
								<li>추경 예산안</li>
								<li>2025 예산안 등</li>
							</ul>
						</td>
						<td class="left">
							<ul class="dot">
								<li>간담회 11.19(화)</li>
								<li>성탄절 12.25</li>
							</ul>
						</td>
					</tr>
					</tbody>
				</table>

				<div class="round_txt">
				위 계획은 집행부 여건과 운영위원회 협의 과정에서 변동될 수 있음. ※ 비회기 4, 5, 8, 9월<br/>
				예비일: 17일(정례회 7일, 임시회 10일)<br/>
				『평택시의회 정례회 및 임시회 회기 운영 조례』제2조(회의총일수): 100일 이내(정례회: 45일 이내 / 임시회: 55일 이내)
				</div>

				<!-- sub_sub_default end -->
			</div>
		</div>
</div>
</body>
</html>
