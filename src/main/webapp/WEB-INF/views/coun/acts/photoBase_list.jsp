<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />

	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>


	<script>
		$(document).ready(function(){
			$(".depth2 > li:nth-child(2)").addClass("current_on");
			$(".photo_gall_list").removeHighlight().highlight($("#condValue").val());
		});
		var fnActInitial    = function(){
			$("#condType").val("A");
			$("#condValue").val("");
			fnActRetrieve();
		};
		var fnActRetrieve   = function(page){
			doActLoadShow();
			$("#pageCurNo").val(isEmpty(page)?"1":page);
			$("#frmDefault").attr({"action":"photoList.do","method":"post","target":"_self"}).submit();
		};
		var fnActDetail     = function(view){
			doActLoadShow();
			$("#viewNo").val(view);
			$("#frmDefault").attr({"action":"photoView.do","method":"post","target":"_self"}).submit();
		};
	</script>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<form id='frmDefault' name='frmDefault'>
		<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
		<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
		<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>

	<div id="subContent">
		<!-- submenu -->
		<jsp:include page="/WEB-INF/views/coun/acts/acts_left.jsp" flush="true"/>
		<!-- sbumenu end -->
		<h3 class="skip">본문내용 he</h3>
		<div id="content">
			<div id="subTitle">
				<h3>포토 의정활동</h3>
			</div>
			<div id="sub_default">


				<div class='board_search'>
					<dl>
						<dt>검색</dt>
						<dd><label for='condType' class='hide'>검색영역</label>
							<select id='condType' name='condType' class='wp100'>
								<option value='A'  <c:if test="${'A'  eq condType}"> selected='selected'</c:if>>전체</option>
								<option value='T'  <c:if test="${'T'  eq condType}"> selected='selected'</c:if>>제목</option>
								<option value='D'  <c:if test="${'D'  eq condType}"> selected='selected'</c:if>>내용</option>
							</select></dd>
						<dt>검색어</dt>
						<dd>
							<label for='condValue' class='hide'>검색어</label>
							<input type='text' id='condValue' name='condValue' class='w25' maxlength='30' value='${condValue}' placeholder='검색어를 입력하세요.'/>
							<a href='javascript:void(0);' class='form_btn' onclick='fnActRetrieve()'>검색</a>
						</dd>
					</dl>
				</div>
				<div class='board_total'>전체 <strong>${listCnt}</strong>건 &nbsp; ${pageCurNo} / ${pageInfo.totalPageCount}</div>
				<ul class="photo_list">

				<c:choose>
					<c:when test="${ empty dataList}">
						<li><%= noneData %></li>
					</c:when>
					<c:otherwise>
					<c:forEach var="rult" varStatus="status" items="${dataList}">
						<li>
							<a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}")'>
							<% if( isGallery ) { %>
							<span class='img'><img src='<%= imgDomain %>/mimg/${rult.fileUri}/${rult.fileName}' alt='${rult.bbsTitle}'/></span>
							<% } else { %>
							<span class='img'><img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/></span>
							<% } %>
							<em class='title'>${rult.bbsTitle}</em>
							<span class='date'>
							<% if( isGallery ) { %>${rult.takeDate}<% } else { %>${rult.creDate}<% } %>
							&nbsp;/&nbsp;조회수 ${rult.hitCount}</span></a>
						</li>
					</c:forEach>
					</c:otherwise>
				</c:choose>

				</ul>
				<c:if test="${!empty pageInfo}"><div class='paging'><ui:pagination paginationInfo="${pageInfo}" type="baseText" jsFunction="fnActRetrieve" /></div></c:if>

			</div>

		</div>
	</div>
	</form>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
