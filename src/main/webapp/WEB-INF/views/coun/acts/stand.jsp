<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Pragma" content="no-cache">
<meta property="og:image" content=""/>
<meta name="keywords" content="평택시의회" />
<meta name="description" content="평택시의회 홈페이지입니다." />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><%= webTitle %></title>
<link rel="shortcut icon" href="/images/favicon.ico" />
<link rel="canonical" href="http://www.ptcouncil.go.kr">
<!-- css -->

<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
<!-- sub contentcss -->
<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />

<script type="text/javascript" src="/js/jquery.js"></script> 
<script type="text/javascript" src="/js/jquery-ui.min.js"></script> 
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>	
<script type="text/javascript" src="/js/slick.min.js"></script>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>


<script>
	$(document).ready(function(){
		$(".depth2 > li:nth-child(1)").addClass("current_on");
	});
	var doActProgram    = function() { 
		window.open("<%= webBase %>/cmmn/program.do","program","status=no,toolbar=no,resizable=no,menubar=no,location=yes,left=200,top=100,width=1024,height=800,scrollbars=yes");
	};
	</script>
	
</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
    <!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->

	
	<!-- 메인메뉴 end -->
	<div id="subContent">
	<!-- submenu -->
	<jsp:include page="/WEB-INF/views/coun/acts/acts_left.jsp" flush="true"/>
	<!-- sbumenu end -->
<h3 class="skip">본문내용 he</h3>
	   <div id="content">
		<div id="subTitle">
			<h3>상임위원회&nbsp;</h3>
		</div>
		   <div id="sub_default">
		   	   
		     <p class="round_txt"><span class="acts_icon"></span><em>의회의 소집과 회기 정례회는 매년 6월 중과 11월 중에 45일 이내로 집회하며, 임시회는 시장 또는 재적의원 1/3 이상의 요구로 매 회기 15일 이내로 소집되고 연간 회의총일수는 정례회와 임시회를 합쳐 100일 이내로 한다. 다만, 부득이한 사유로 인하여 연간 회의초일수를 초과하여 집회할 필요가 있을 때에는 본회의 의결로 10일 이내의 범위에서 이를 연장할 수 있다.</em></p>
			
			   <div id="acts_list">
			   <ul>
				    <li class="acts_01"><a href="/oper/index.do" target="_blank"><h4>운영위원회</h4> <span>평택시의회 운영위원회에
대한 내용을 보실 수 있습니다.</span></a></li>
				   <li class="acts_02"><a href="/gvadm/index.do" target="_blank"><h4>자치행정위원회</h4><span>평택시의회 기획행정위원회에
대한 내용을 보실 수 있습니다.</span></a></li>
				   <li class="acts_03"><a href="/welfare/index.do" target="_blank"><h4>복지환경위원회</h4><span>평택시의회 복지환경위원회에
대한 내용을 보실 수 있습니다.</span></a></li>
				   <li class="acts_04"><a href="/build/index.do" target="_blank"><h4>산업건설위원회</h4><span>평택시의회 산업건설위원회에
대한 내용을 보실 수 있습니다.</span></a></li>
             </ul>
			   </div>
			  
			   
			   
			   
			   
			</div>
			
         </div>
	 </div>
	</div>
	<!-- footer -->
	<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
	<!-- //footer -->
		
		
	</div>
	
	

</body>
</html>
