<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>


		
		<!-- sub menu -->
	  <div id="submenuArea" class="w sm_intro">
		<div class="sm_tit ">
			<h2>
			의정활동
			</h2>
		</div>
		<ul class="depth2">
		<!-- 메뉴 활성화시 class="current" 가 class="current_on"으로 바뀌도록 -->
					<li class="current"> <a href="/coun/acts/stand.do" title="상임위원회">상임위원회</a> </li>
					<li class="current"> <a href="/coun/base/photoList.do" title="포토 의정활동">포토 의정활동</a> </li>
					<li class="current"> <a href="/coun/base/movieList.do" title="홍보영상">홍보영상</a> </li>
					<li class="current"> <a href="/coun/cost/reportList.do" title="업무추진비 공개">업무추진비 공개</a> </li>
					<li class="current"> <a href="/coun/brief/reportList.do" title="국내외 연수보고서">국내외 연수보고서</a> </li>
					<li class="current"> <a href="/coun/resr/reportList.do" title="의원 연구단체">의원 연구단체</a> </li>
					<li class="current"> <a href="/coun/schd/monthList.do" title="의사일정">의사일정</a>
						<ul>
							<li class="current"> <a href="/coun/schd/monthList.do" title="월간일정">월간일정</a> </li>
							<li class="current"> <a href="/coun/acts/program.do" title="연간회기일정">연간회기일정</a> </li>
						</ul>
					</li>
					<li class="current"> <a href="/coun/hold/reportList.do" title="의원겸직현황">의원겸직현황</a> </li>
				</ul>
		
		
			<!-- sub menu end -->
			</div>
			
			
			