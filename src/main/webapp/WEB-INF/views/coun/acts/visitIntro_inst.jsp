<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />

	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<script>
		$(document).ready(function(){
			$(".menu1").removeClass("current");
			$(".menu1").addClass("current_on");
			$(".depth2 > li:nth-child(6)").addClass("current_on");
			$(".depth2 > li:nth-child(6) > ul > li:nth-child(3)").addClass("current_on");
		});
		var fnActReturn     = function(seq){
			doActLoadShow();
			$("#frmDefault").attr({"action":"visit"+(seq=="seq"?"List":"Requ")+".do","method":"post","target":"_self"}).submit();
		};
		var fnActUpdate     = function(){
			if($("[id^='vstType']:checked").length<=0){alert("방문구분은 필수선택입니다.");$("[id^='vstType']:eq(0)").focus();return false;}
			if($.trim($("#vstDate").val())==""){alert("방문일자는 필수선택입니다.");$("#vstDate").focus();return false;}
			if($.trim($("#vstTimeH").val())==""){alert("방문시간은 필수선택입니다.");$("#vstTimeH").focus();return false;}
			if($.trim($("#vstTimeM").val())==""){alert("방문시간은 필수선택입니다.");$("#vstTimeM").focus();return false;}
			if($.trim($("#vstDesc").val())==""){alert("방목목적은 필수입력입니다.");$("#vstDesc").focus();return false;}
			if($.trim($("#vstPerson").val())==""){alert("방문인원은 필수입력입니다.");$("#vstPerson").focus();return false;}
			$("#frmDefault").attr({"action":"visitCreate.do","method":"post","target":"_self"}).ajaxSubmit({
				dataType      : "json",
				cache         : false,
				beforeSubmit  : function(data,form,options){doActLoadShow();},
				success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn("seq");}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
				error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
			});
		};
	</script>


</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<!-- 메인메뉴 end -->
	<div id="subContent">
		<!-- submenu -->
		<!-- 왼쪽메뉴 -->
		<jsp:include page="/WEB-INF/views/coun/conf/conf_left.jsp" flush="true"/>
		<!-- sbumenu end -->


		<h3 class="skip">본문내용 he</h3>
		<div id="content">

			<div id="subTitle">
				<!-- sub 제목부분 -->
				<h3>온라인신청&nbsp;</h3>
			</div>
			<div id="sub_default">

				<!-- program 들어갈부분 -->
				<form id='frmDefault' name='frmDefault'>
					<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
					<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
					<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
					<input type='hidden' id='reStdStat'     name='reStdStat'     value='${reStdStat}'/>
					<input type='hidden' id='reStdDate'     name='reStdDate'     value='${reStdDate}'/>

					<h4>방문/방청 신청확인</h4>
					<div class="round_txt">
						<span>평택시의회 방문/방청을 하기 위해서는 신청 및 승인과정을 거쳐야 방문/방청을 하실 수 있으며, 방청석이 많이 준비하지 못한 관계로 단체 방청 시에는 사전에 협의하셔야 합니다.</span>
						<ul class="dot">
							<li>단체신청 : 대표자1인이 사전에 의회사무국에 방청 협의 </li>
							<li>회기일정은 홈페이지 의회 일정을 확인하시기 바랍니다.</li>
							<li>연락처 : 031-8024-7560~8</li>
						</ul>
					</div>
					<h4>신청서 입력</h4>
					<p class="mB10">* 신청내용은 모두 입력하셔야 합니다.</p>
					<table class="board_write">
						<caption class='hide'>Board View</caption>
						<colgroup><col style='width:12%;'/><col style='width:88%;'/></colgroup>
						<tbody><tr>
							<th scope='col'>방문구분</th>
							<td>
								<input type='radio' id='vstType1' name='vstType' value='P'/> <label for='vstType1'>개인</label>
								<input type='radio' id='vstType2' name='vstType' value='G'/> <label for='vstType2'>단체</label>
							</td>
						</tr><tr>
							<th>신청자</th>
							<td><label for='read1' class='hide'>신청자</label><input type='text' id='read1' class='w100 center' maxlength='30' value='${authInfo.certName}' readonly='readonly'/></td>
						</tr><tr>
							<th>방문일시</th>
							<td>
								<label for='vstDate' class='hide'>방문일시</label>
								<input type='text' id='vstDate' name='vstDate' class='w100 center' maxlength='10' value='${reqDate}' readonly='readonly'/> &nbsp; &nbsp;
								<label for='vstTimeH' class='hide'>방문일시</label>
								<select id='vstTimeH' name='vstTimeH' class='w80 center'><option value=''>선택(시)</option><c:forEach var="time" varStatus="status" begin="9" end="18"><fmt:formatNumber value="${status.current   }" pattern="00" var="toTime"/><option value='${toTime}'>${toTime}시</option></c:forEach></select> &nbsp;
								<label for='vstTimeM' class='hide'>방문일시</label>
								<select id='vstTimeM' name='vstTimeM' class='w80 center'><option value=''>선택(분)</option><c:forEach var="time" varStatus="status" begin="0" end="5" ><fmt:formatNumber value="${status.current*10}" pattern="00" var="toTime"/><option value='${toTime}'>${toTime}분</option></c:forEach></select> &nbsp;
							</td>
						</tr><tr>
							<th>방문목적</th>
							<td><label for='vstDesc' class='hide'>방문목적</label><textarea id='vstDesc' name='vstDesc' rows='6' class='w90_p' maxlength='400'></textarea></td>
						</tr><tr>
							<th>방문인원</th>
							<td><label for='vstPerson' class='hide'>방문인원</label><input type='number' id='vstPerson' name='vstPerson' min='0' max='99' maxlength='2' class='w100 center'/> 명</td>
						</tr></tbody>
					</table>
					<div class='center pT20'>
						<a href='javascript:void(0);' class='btn orange' onclick='fnActUpdate()'>신청하기</a>
						<a href='javascript:void(0);' class='btn' onclick='fnActReturn()'>취소</a>
					</div>

				</form>
				<!--  program 들어갈부분 end -->

			</div>



		</div>
	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
