<%
////////////////////////////////////////////////////////////
// Program ID  : photoIntro_list
// Description : 의회소개 > 열린의장실 - 포토 의정활동 [목록조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "포토 의정활동";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb01_01").addClass("on").removeClass("noChild");
		$("#snb01_01_03").addClass("on");
	});
	var fnActInitial    = function(){
		$("#condType").val("A");
		$("#condValue").val("");
		fnActRetrieve();
	};
	var fnActRetrieve   = function(page){
		doActLoadShow();
		$("#pageCurNo").val(isEmpty(page)?"1":page);
		$("#frmDefault").attr({"action":"photoList.do","method":"post","target":"_self"}).submit();
	};
	var fnActDetail     = function(view){
		doActLoadShow();
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"photoView.do","method":"post","target":"_self"}).submit();
	};
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/coun/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/coun/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="1"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 의회소개 &gt; 열린의장실 &gt; <%= subTitle %></div>
			<div class='program_search'><table>
				<caption class='hide'>Search Detail</caption>
				<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
				<tbody><tr>
					<th scope='col' class='center'>검색</th>
					<td><label for='condType' class='hide'>검색영역</label>
						<select id='condType' name='condType' class='w100'>
							<option value='A'  <c:if test="${'A'  eq condType}"> selected='selected'</c:if>>전체</option>
							<option value='T'  <c:if test="${'T'  eq condType}"> selected='selected'</c:if>>제목</option>
							<option value='D'  <c:if test="${'D'  eq condType}"> selected='selected'</c:if>>내용</option>
						</select>
					</td>
				</tr><tr>
					<th class='center'>검색어</th>
					<td><label for='condValue' class='hide'>검색어</label><input type='text' id='condValue' name='condValue' class='w60_p' maxlength='30' value='${condValue}' placeholder='검색어를 입력하세요.'/></td>
				</tr><tr>
					<td colspan='2' class='center'><a href='javascript:void(0);' class='form_btn' onclick='fnActRetrieve()'>검색</a></td>
				</tr></tbody>
			</table></div>
			<div class='board_total'>전체 <strong>${listCnt}</strong>건 &nbsp; ${pageCurNo} / ${pageInfo.totalPageCount}</div>
			<c:choose><c:when test="${ empty dataList}">
			<div class='table_list'><table>
				<caption class='hide'>Board List</caption>
				<colgroup><col style='width: 6%;'/><col style='width:64%;'/><col style='width:12%;'/><col style='width:10%;'/><col style='width: 8%;'/></colgroup>
				<tbody><tr class='h200'><td colspan='5'><%= noneData %></td></tr></tbody>
			</table></div>
			</c:when><c:otherwise>
			<div class='photo_gall_list'><ul>
			<c:forEach var="rult" varStatus="status" items="${dataList}">
				<li><% if( isGallery ) { %>
					<div class='img'><a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}")'><img src='<%= imgDomain %>/mimg/${rult.fileUri}/${rult.fileName}' alt='${rult.bbsTitle}'/></a></div><% } else { %>
					<div class='img'><a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}")'><img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/></a></div><% } %>
					<div class='tit'>${rult.gabSpace}<a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}")'><p>${rult.bbsTitle}</p></a></div>
					<div class='period'><ul><li><% if( isGallery ) { %>${rult.takeDate}<% } else { %>${rult.creDate}<% } %>&nbsp;/&nbsp;조회수 ${rult.hitCount}</li></ul></div>
				</li>
			</c:forEach>
			</ul></div>
			</c:otherwise></c:choose>
			<c:if test="${!empty pageInfo}"><div class='paging'><ui:pagination paginationInfo="${pageInfo}" type="baseText" jsFunction="fnActRetrieve" /></div></c:if>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer.jsp" flush="true"><jsp:param name="qk" value="F"/></jsp:include>
	<!-- //footer -->
</div>
</form>
</body>
</html>