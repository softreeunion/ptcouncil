<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_certi.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>

	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>
	<script>
		$(document).ready(function(){
			$(".depth2 > li:nth-child(6)").addClass("current_on");
			$(".depth2 > li:nth-child(6) > ul > li:nth-child(2)").addClass("current_on");
		});
		var fnActInitial    = function(){
			$("#condValue").val("");
			fnActRetrieve();
		};
		var fnActReturn     = function(seq){
			doActLoadShow();
			fncClearTimer();
			$("#frmDefault").attr({"action":"visit"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self"}).submit();
		};
		var fnActPopuper    = function(){<c:choose><c:when test="${'Delete' eq nextMode}">
			$("#frmDefault").attr({"action":"visitDelete.do","method":"post","target":"_self"}).ajaxSubmit({
				dataType      : "json",
				cache         : false,
				beforeSubmit  : function(data,form,options){doActLoadShow();},
				success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){fnActReturn("seq");}else{if(data.isError=="C"){if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}}else{fnActReturn("seq");}}},
				error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
			});</c:when><c:otherwise>
			doActLoadShow();<%--
		alert("<%= alertMsg[12] %>");--%>
			$("#frmDefault").attr({"action":"visit<c:choose><c:when test="${ empty nextMode}">Requ</c:when><c:otherwise>${nextMode}</c:otherwise></c:choose>.do","method":"post","target":"_self"}).submit();</c:otherwise></c:choose>
		};
		var fnActPopup      = function(){<c:if test="${'T' ne certTest}">
			if(!$("#privacy_agree").prop("checked")) {alert("개인정보 수집·이용에 동의하셔야 진행하실 수 있습니다.");$("#privacy_agree").focus();return false;}
			if(!$("#terms_agree").prop("checked")) {alert("이용약관에 동의하셔야 진행하실 수 있습니다.");$("#terms_agree").focus();return false;}
			fnActOpenPlus();</c:if><c:if test="${'T' eq certTest}">
			fnActPopuper();</c:if>
		};
	</script>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->

	<div id="subContent">
		<!-- submenu -->
		<jsp:include page="/WEB-INF/views/coun/intro/intro_left.jsp" flush="true"/>
		<!-- sbumenu end -->

		<form id='frmDefault' name='frmDefault'>
			<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
			<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
			<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
			<input type='hidden' id='reStdStat'     name='reStdStat'     value='${reStdStat}'/>
			<input type='hidden' id='reStdDate'     name='reStdDate'     value='${reStdDate}'/>
			<input type='hidden' id='reqDate'       name='reqDate'       value='${reqDate}'/>
			<input type='hidden' id='m'             name='m'             value='checkplusSerivce'/><%-- 필수 데이타로, 누락하시면 안됩니다. --%>
			<input type='hidden' id='EncodeData'    name='EncodeData'    value='<%= sEncData %>'/><%-- 위에서 업체정보를 암호화 한 데이타입니다. --%>


		<h3 class="skip">본문내용</h3>
		<div id="content">
			<div id="subTitle">
				<h3>온라인신청</h3>
			</div>
			<div id="sub_default">

				<h4>방문/방청 신청확인</h4>
				<div class="round_txt">
					<span>평택시의회 방문/방청을 하기 위해서는 신청 및 승인과정을 거쳐야 방문/방청을 하실 수 있으며, 방청석이 많이 준비하지 못한 관계로 단체 방청 시에는 사전에 협의하셔야 합니다.</span>
					<ul class="dot">
						<li>개인 : 개별 실명인증 확인 후 신청</li>
						<li>단체 : 대표자 1인이 실명인증 확인 후 신청, 사전에 의회사무국에 방청 협의</li>
						<li>회기일정은 홈페이지 의회 일정을 확인하시기 바랍니다.</li>
						<li>연락처 : 031-8024-7560~8</li>
					</ul>
				</div>

				<h4>개인정보 수집·이용 동의 안내</h4>
				<div class="round_txt">
					<ul class="dot">
						<li>개인정보를 제공받는 자 : NICE평가정보㈜</li>
						<li>개인정보의 이용목적 : 연락처 등을 통한 본인확인 제공</li>
						<li>제공항목 : 고객의 성명</li>
						<li>보유기간 : 개인정보의 이용목적 달성시 즉시 폐기(단, 회사 내부 방침 및 관계법령에 보유사유가 있을 경우 예외)</li>
						<li>동의를 거부하실 수 있으나, 거부 시 정상적인 방문 접수 처리가 불가함</li>
					</ul>
				</div>
				<div class='p_tit_np right'><input type='checkbox' id='privacy_agree' name='privacy_agree'/> <label for='privacy_agree'>개인정보 수집·이용에 동의합니다.</label></div>


				<h4>이용약관 동의 안내</h4>
				<div class="round_txt">
					평택시의회는 의회가 제공하는 인터넷 서비스(이하 서비스 라 합니다)의 내용을 보호하고, 저작권 등 타인의 권리를 침해하지 않도록 하기 위하여 다음과 같은 저작권 정책을 운영하고 있습니다. 다음에 명시되지 아니한 저작권 관련 사항은 평택시의회 홈페이지 이용약관과 저작권법을 준용합니다. 평택시의회의 저작권 보호 정책은 관련 법률 및 정부지침의 변경 등에 따라 개정될 수 있으며, 개정된 사항은 시행 전 평택시의회의 홈페이지에 공지하도록 합니다.
				</div>
				<div class='p_tit_np right'><input type='checkbox' id='terms_agree' name='terms_agree'/> <label for='terms_agree'>이용약관에 동의합니다.</label></div>

				<!-- 게시판 하단버튼 -->
				<div class="center  pT30">
					<a href='javascript:void(0);' class='btn orange' onclick='fnActPopup()'>연락처본인인증</a>
					<a href='javascript:void(0);' class='btn' onclick='fnActReturn("seq")'>취소</a>
				</div>

				<!-- sub_sub_default  end -->
			</div>
		</div>
		</form>


	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>

</body>
</html>
