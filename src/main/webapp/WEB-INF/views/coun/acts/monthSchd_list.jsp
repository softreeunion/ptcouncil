<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />

	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<script>
		$(document).ready(function(){
			$(".depth2 > li:nth-child(7)").addClass("current_on");
			$(".depth2 > li:nth-child(7) > ul > li:nth-child(1)").addClass("current_on");
		});
		var fnActRetrieve   = function(date,stat){
			doActLoadShow();
			$("#reStdDate").val(isEmpty(date)?"${reCurrDate}":date);
			$("#reStdStat").val(isEmpty(stat)?"C":stat);
			$("#frmDefault").attr({"action":"monthList.do","method":"post","target":"_self"}).submit();
		};
		var fnActMonth      = function(view,acts){
			$.ajax({
				type          : "post",
				url           : "<%= webBase %>/coun/monthView.do",
				dataType      : "html",
				data          : {viewNo:view,stdDate:acts,actMode:"D"},
				success       : function(data,status,xhr  ){$("#layMonth").empty();$("#layMonth").html(data);month_layer();},
				error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);}
			});
		};
	</script>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<div id="subContent">
		<!-- submenu -->
		<jsp:include page="/WEB-INF/views/coun/acts/acts_left.jsp" flush="true"/>
		<!-- sbumenu end -->

		<form id='frmDefault' name='frmDefault'>
			<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
			<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
			<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
			<input type='hidden' id='reStdStat'     name='reStdStat'     value='${reStdStat}'/>
			<input type='hidden' id='reStdDate'     name='reStdDate'     value='${reStdDate}'/>

		<h3 class="skip">본문내용 he</h3>
		<div id="content">
			<div id="subTitle">
				<h3>월간일정</h3>
			</div>
			<div id="sub_default">

				<div class="calender_btn mT30">
					<div class='calenda_header'>
						<span class='title'>${dispDate}</span>
						<div class='icon_btn'>
							<a href='javascript:void(0);' onclick='fnActRetrieve("${reCurrDate}","C" )' class='cal_btn01'>오늘</a>
							<a href='javascript:void(0);' onclick='fnActRetrieve("${reStdDate }","PY")' class='cal_btn02'>이전 해</a>
							<a href='javascript:void(0);' onclick='fnActRetrieve("${reStdDate }","PM")' class='cal_btn03'>이전 달</a>
							<a href='javascript:void(0);' onclick='fnActRetrieve("${reStdDate }","NM")' class='cal_btn03'>다음 달</a>
							<a href='javascript:void(0);' onclick='fnActRetrieve("${reStdDate }","NY")' class='cal_btn02'>다음 해</a>
						</div>
					</div>
				</div>
				<div id="calendarArea">
					<table class='calendar'>
						<caption class='hide'>의사일정</caption>
						<colgroup><col style='width:12%;'/><col style='width:15%;'/><col style='width:15%;'/><col style='width:15%;'/><col style='width:15%;'/><col style='width:15%;'/><col style='width:13%;'/></colgroup>
						<thead><tr class='weekdays'>
							<th class='sun'>일</th>
							<th>월</th>
							<th>화</th>
							<th>수</th>
							<th>목</th>
							<th>금</th>
							<th class='sat'>토</th>
						</tr></thead>
						<tbody><c:choose><c:when test="${ empty dispDocu}"><tr class='off h200'><td colspan='7'><%= noneData %></td></tr></c:when><c:otherwise>${dispDocu}</c:otherwise></c:choose></tbody>
					</table>
				</div>
			</div>
		</div>


		</form>

	</div>

</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</body>
</html>