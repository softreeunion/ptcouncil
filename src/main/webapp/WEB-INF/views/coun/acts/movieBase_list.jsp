<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	String subTitle				= "홍보 영상";
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />

	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>
	<style>
		.vod_mb10 {margin-bottom:10px !important;}
		.vod_comment{width:100%;max-height:200px;margin:0 0 2em;overflow-y:auto;padding:1em;border:1px solid #ddd;background:#fafafa;box-sizing:border-box;}
	</style>

	<script>
		$(document).ready(function(){
			$(".depth2 > li:nth-child(3)").addClass("current_on");
			$("#divVideo").css("border-bottom","2px solid #333");
		});
		var fnActInitial    = function(){
			$("#condType").val("A");
			$("#condValue").val("");
			fnActRetrieve();
		};
		var fnActRetrieve   = function(page){
			doActLoadShow();
			$("#pageCurNo").val(isEmpty(page)?"1":page);
			$("#frmDefault").attr({"action":"movieList.do","method":"post","target":"_self"}).submit();
		};
		var fnActDetail     = function(view){<% if( movieLayer ) { %>
			//alert("홍보영상 준비 중입니다.");
			//return;
			$.ajax({
				type          : "post",
				url           : "<%= webBase %>/coun/mediaView.do",
				dataType      : "html",
				data          : {viewNo:view},
				success       : function(data,status,xhr  ){var $LV=$("#layVideo");$LV.empty();$LV.html(data);media_layer();},
				error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);}
			});<% } else {
		if( movieView ) { %>
			$("#viewNo").val(view);
			fnActRetrieve(${pageCurNo});<% } else { %>
			doActLoadShow();
			$("#viewNo").val(view);
			$("#frmDefault").attr({"action":"movieView.do","method":"post","target":"_self"}).submit();<% }} %>
		};
	</script>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<form id='frmDefault' name='frmDefault'>
		<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
		<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
		<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>

		<div id="subContent">
			<!-- submenu -->
			<jsp:include page="/WEB-INF/views/coun/acts/acts_left.jsp" flush="true"/>
			<!-- sbumenu end -->
			<h3 class="skip">본문내용 he</h3>
			<div id="content">
				<div id="subTitle">
					<h3>홍보 영상</h3>
				</div>
				<div id="sub_default">

					<% if( !movieLayer && movieView ) { %>
					<c:if test="${!empty dataView}">
					<div class='table_view' id='divVideo'><table>
						<caption class='hide'>Board View</caption>
						<colgroup><col style='width:100%;'/></colgroup>
						<tbody><tr class='h250'>
							<td class='nonebd_l'><video id='vod' oncontextmenu='return false;' width='100%' controls<% if( viewPoster ) { %> preload='metadata'<c:if test="${!empty dataView.fileUUID}"> poster='<% if( imgMode ) { %><%= webBase %>/GetImage.do?key=${dataView.fileUUID}<% } else { %><%= webBase + uploadPath %>${dataView.fileAbs}<% } %>'</c:if><% } else { %> autoplay preload='auto'<% } %>><% if( vodMode ) { %>
								<source src='<%= webBase %>/GetMovie.do?key=${dataView.downUUID}' type='video/${dataView.downExt}'/><% } else { %>
								<source src='<%= viewPort + webBase %>${dataView.downAbs}' type='video/${dataView.downExt}'/><% } %>
							</video></td>
						</tr><tr>
							<th class='nonebd_l'>${dataView.bbsTitle}</th>
						</tr></tbody>
					</table></div><br/>
					</c:if>
					<% } %>


					<div class='board_search'>
						<dl>
							<dt>검색</dt>
							<dd><label for='condType' class='hide'>검색영역</label>
								<select id='condType' name='condType' class='wp100'>
									<option value='A'  <c:if test="${'A'  eq condType}"> selected='selected'</c:if>>전체</option>
									<option value='T'  <c:if test="${'T'  eq condType}"> selected='selected'</c:if>>제목</option>
									<option value='D'  <c:if test="${'D'  eq condType}"> selected='selected'</c:if>>내용</option>
								</select></dd>
							<dt>검색어</dt>
							<dd>
								<label for='condValue' class='hide'>검색어</label>
								<input type='text' id='condValue' name='condValue' class='w25' maxlength='30' value='${condValue}' placeholder='검색어를 입력하세요.'/>
								<a href='javascript:void(0);' class='form_btn' onclick='fnActRetrieve()'>검색</a>
							</dd>
						</dl>
					</div>
					<div class='board_total'>전체 <strong>${listCnt}</strong>건 &nbsp; ${pageCurNo} / ${pageInfo.totalPageCount}</div>
					<table class="board_list">
						<caption class='hide'>Board List</caption>
						<colgroup><col style='width:10%;' class='w_no'/><col style='width:50%;'/><col style='width:15%;' class='w_wn'/><col style='width:15%;' class='w_wd'/><col style='width:10%;' class='w_wr'/></colgroup>
						<thead><tr>
							<th scope='col'>번호</th>
							<th scope='col'>제목</th>
							<th scope='col'>작성자</th>
							<th scope='col'>등록일</th>
							<th scope='col'>조회</th>
						</tr></thead>
						<tbody>
						<c:choose><c:when test="${ empty dataList}">
							<tr class='h200'><td colspan='5'><%= noneData %></td></tr>
						</c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${dataList}">
							<tr class='pnc' onclick='fnActDetail("${rult.viewNo}")'>
								<td>${rult.rowIdx}</td>
								<td class='left'>${rult.gabSpace}<c:if test="${'Y' eq rult.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${rult.bbsTitle}</td>
								<td>${rult.creName}</td>
								<td>${rult.creDate}</td>
								<td>${rult.hitCount}</td>
							</tr>
						</c:forEach></c:otherwise></c:choose>
						</tbody>
					</table>
					<c:if test="${!empty pageInfo}"><div class='paging'><ui:pagination paginationInfo="${pageInfo}" type="baseText" jsFunction="fnActRetrieve" /></div></c:if>

				</div><%= movieLayer ? "<div class='layer'><div class='bg'></div><div class='layer_pop' id='layVideo'></div></div>" : "" %>

			</div>
		</div>
	</form>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
