<%
////////////////////////////////////////////////////////////
// Program ID  : photoIntro_view
// Description : 의회소개 > 열린의장실 - 포토 의정활동 [상세조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "포토 의정활동";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb01_01").addClass("on").removeClass("noChild");
		$("#snb01_01_03").addClass("on");<% if( isGallery ) { %>
		$(".fotorama").fotorama().data("fotorama").show(${chldShow});
		$(".fotorama").on("fotorama:show",function(e,fotorama){$("#fotTitle").text(fotorama.activeFrame.caption);}).fotorama();<% } %>
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"photo"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self"}).submit();
	};
	var fnActDetail     = function(view){
		doActLoadShow();
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"photoView.do","method":"post","target":"_self"}).submit();
	};
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/coun/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/coun/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="1"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 의회소개 &gt; 열린의장실 &gt; <%= subTitle %></div><% if( isGallery ) { %>
			<div class='snsbtn' style='text-align:right;'>
				<span><a href='javascript:void(0);' onclick='shareFacebook("<%= webPath %>${viewLink}")'><img src='<%= webBase %>/images/sns_facebook.png' style='width:42px;height:42px;' alt='facebook' title='페이스북'/></a></span>
				<span><a href='javascript:void(0);' onclick='shareTwitter("<%= webPath %>${viewLink}",encodeURIComponent($("#fotTitle").text()))'><img src='<%= webBase %>/images/sns_twitter.png' style='width:42px;height:42px;' alt='트위터' title='twitter'/></a></span>
				<span><a href='javascript:void(0);' onclick='shareBand("<%= webPath %>${viewLink}")'><img src='<%= webBase %>/images/sns_naverband.png' style='width:42px;height:42px;' alt='naverBand' title='네이버밴드'/></a></span>
				<span><a href='javascript:void(0);' onclick='shareKakaoStory("<%= webPath %>${viewLink}")'><img src='<%= webBase %>/images/sns_kakaostory.png' style='width:42px;height:42px;' alt='kakaoStory' title='카카오스토리'/></a></span>
				<span><a href='javascript:void(0);' onclick='shareTrackback("<%= webPath %>${viewLink}")'><img src='<%= webBase %>/images/sns_trackback.png' style='width:42px;height:42px;' alt='trackBack' title='주소 공유'/></a></span>
				<span><a href='javascript:void(0);' onclick='fnImgPrint($("#fotTitle").text()+" (${dataView.creDate})")'><img src='<%= webBase %>/images/sns_printer.png' style='width:42px;height:42px;' alt='print' title='문서 출력'/></a></span>
				<span><a href='javascript:void(0);' onclick='fnImgExpand($("#fotTitle").text()+" (${dataView.creDate})")'><img src='<%= webBase %>/images/sns_expand.png' style='width:42px;height:42px;' alt='expand' title='크게 보기'/></a></span>
				<span><a href='javascript:void(0);' onclick='fnImgDown($("#fotTitle").text()+" (${dataView.creDate})")'><img src='<%= webBase %>/images/sns_down.png' style='width:42px;height:42px;' alt='down' title='파일 저장'/></a></span>
			</div>
			<div class='photo_view_title'><h2 id='fotTitle'>${dataView.bbsTitle}</h2><span>${dataView.takeDate}</span></div><%--
			<div class='photo_area'>
			<c:if test="${!empty chldFile}">
				<div class='photo_viewer'><ul>
				<c:forEach var="rult" varStatus="status" items="${chldFile}">
					<li><img src='<%= imgDomain %>/img/${rult.fileUri}/${rult.fileName}' alt='${rult.bbsTitle}'/></li>
				</c:forEach>
				</ul></div>
				<div class='photo_viewer_pager'><ul>
				<c:forEach var="rult" varStatus="status" items="${chldFile}">
					<li><a href='javascript:void(0);' data-slide-index='${status.index}'><img src='<%= imgDomain %>/mimg/${rult.fileUri}/${rult.fileName}' alt='${rult.bbsTitle}'/></a></li>
				</c:forEach>
				</ul></div>
			</c:if>
			</div>--%>
			<div class='fotorama' data-nav='thumbs' data-allowfullscreen='true' data-autoplay='false' data-captions='false' data-width='100%' data-maxwidth='100%'>
			<c:if test="${!empty chldFile}"><c:forEach var="rult" varStatus="status" items="${chldFile}">
				<a href='<%= imgDomain %>/vimg/${rult.fileUri}/${rult.fileName}' data-caption='${rult.bbsTitle}'><div data-thumb='<%= imgDomain %>/mimg/${rult.fileUri}/${rult.fileName}'></div></a>
			</c:forEach></c:if>
			</div>
			<div class='photo_view_title'><h2>${dataView.contDesc}</h2></div><% } else { %>
			<div class='table_view'><table>
				<caption class='hide'>Board View</caption>
				<colgroup><col style='width:10%;'/><col style='width:30%;'/><col style='width:10%;'/><col style='width:20%;'/><col style='width:10%;'/><col style='width:20%;'/></colgroup>
				<tbody><tr>
					<th>제목</th>
					<td colspan='5'>${dataView.bbsTitle}</td>
				</tr><tr>
					<th scope='col'>작성자</th>
					<td>${dataView.creName}</td>
					<th scope='col'>작성일</th>
					<td>${dataView.creDate}</td>
					<th scope='col'>조회수</th>
					<td>${dataView.hitCount}</td>
				</tr><tr class='h250'>
					<th>썸네일</th>
					<td colspan='5'><img src='<%= webBase %>/GetImage.do?key=${dataView.fileUUID}' alt='${dataView.bbsTitle}' style='width:100%;height:auto;'/></td>
				</tr><tr class='h100'>
					<th>내용</th>
					<td colspan='5' class='cont'><c:choose><c:when test="${'P' eq dataView.contPattern}">${dataView.contDescBR}</c:when><c:otherwise>${dataView.contDesc}</c:otherwise></c:choose></td>
				</tr></tbody>
			</table></div><% } %>
			<div class='btn_group right'>
				<a href='javascript:void(0);' class='btn_style03' onclick='fnActReturn("seq")'>목록</a>
			</div><c:if test="${'T' eq moveItem}">
			<div class='table_view table_nextprev'><table>
				<caption class='hide'>Previous/Next</caption>
				<colgroup><col style='width:10%;'/><col style='width:90%;'/></colgroup>
				<tbody><tr>
					<th scope='col'>이전글</th>
					<td><c:choose><c:when test="${ empty prevView}">이전 글이 존재 하지 않습니다.</c:when><c:otherwise>${prevView.gabSpace}<a href='javascript:void(0);' onclick='fnActDetail("${prevView.viewNo}")'><c:if test="${'Y' eq prevView.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${prevView.bbsTitle}</a></c:otherwise></c:choose></td>
				</tr><tr>
					<th scope='col'>다음글</th>
					<td><c:choose><c:when test="${ empty nextView}">다음 글이 존재 하지 않습니다.</c:when><c:otherwise>${nextView.gabSpace}<a href='javascript:void(0);' onclick='fnActDetail("${nextView.viewNo}")'><c:if test="${'Y' eq nextView.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${nextView.bbsTitle}</a></c:otherwise></c:choose></td>
				</tr></tbody>
			</table></div></c:if>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer.jsp" flush="true"><jsp:param name="qk" value="F"/></jsp:include>
	<!-- //footer -->
</div>
</form>
</body>
</html>