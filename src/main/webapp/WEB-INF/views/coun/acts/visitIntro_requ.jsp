<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>

	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>
	<script>
		$(document).ready(function(){
			$(".depth2 > li:nth-child(6)").addClass("current_on");
			$(".depth2 > li:nth-child(6) > ul > li:nth-child(2)").addClass("current_on");
		});
		var fnActRetrieve   = function(date,stat){
			doActLoadShow();
			$("#reStdDate").val(isEmpty(date)?"${reStdDate}":date);
			$("#reStdStat").val(isEmpty(stat)?"${reStdStat}":stat);
			$("#frmDefault").attr({"action":"visitRequ.do","method":"post","target":"_self"}).submit();
		};
		var fnActCreate     = function(date,stat){
			doActLoadShow();
			$("#reqDate").val(isEmpty(date)?"${reStdDate}":date);
			$("#reStdStat").val(isEmpty(stat)?"${reStdStat}":stat);
			$("#frmDefault").attr({"action":"visitInst.do","method":"post","target":"_self"}).submit();
		};
	</script>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->

	<div id="subContent">
		<!-- submenu -->
		<jsp:include page="/WEB-INF/views/coun/intro/intro_left.jsp" flush="true"/>
		<!-- sbumenu end -->

		<form id='frmDefault' name='frmDefault'>
			<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
			<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
			<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
			<input type='hidden' id='reStdDate'     name='reStdDate'     value='${reStdDate}'/>
			<input type='hidden' id='reStdStat'     name='reStdStat'     value='${reStdStat}'/>
			<input type='hidden' id='reqDate'       name='reqDate'       value='${reqDate}'/>

		<h3 class="skip">본문내용</h3>
		<div id="content">
			<div id="subTitle">
				<h3>온라인신청</h3>
			</div>
			<div id="sub_default">
				<!-- sub_default  program 들어갈부분 -->
				<h4>방문/방청 일정</h4>
				<p class="round_txt">
              	<span class="visit_icon"></span>
					<em>평택시의회 방문/방청을 하기 위해서는 신청 및 승인과정을 거쳐야
					방문/방청을 하실 수 있으며, 방청석이 많이 준비하지 못한 관계로
					단체 방청 시에는 사전에 협의하셔야 합니다.</em>
				</p>
				<div class="calender_btn">
					<span class="title">${dispDate}</span>
					<div class="icon_btn">
						<a href='javascript:void(0);' class='cal_btn01' onclick='fnActRetrieve("${reCurrDate}","C" )'>오늘</a>
						<a href='javascript:void(0);' class='cal_btn02' onclick='fnActRetrieve("${reStdDate }","PY")'>이전 해</a>
						<a href='javascript:void(0);' class='cal_btn03' onclick='fnActRetrieve("${reStdDate }","PM")'>이전 달</a>
						<a href='javascript:void(0);' class='cal_btn03' onclick='fnActRetrieve("${reStdDate }","NM")'>다음 달</a>
						<a href='javascript:void(0);' class='cal_btn02' onclick='fnActRetrieve("${reStdDate }","NY")'>다음 해</a>
					</div>
				</div>

				<div id="calendarArea">
					<table class="calendar">
						<caption class="hide">
							방문/방청일정
						</caption>
						<colgroup>
							<col style="width: 12%" />
							<col style="width: 15%" />
							<col style="width: 15%" />
							<col style="width: 15%" />
							<col style="width: 15%" />
							<col style="width: 15%" />
							<col style="width: 13%" />
						</colgroup>
						<thead>
						<tr class="weekdays">
							<th class="sun">일</th>
							<th>월</th>
							<th>화</th>
							<th>수</th>
							<th>목</th>
							<th>금</th>
							<th class="sat">토</th>
						</tr>
						</thead>
						<tbody>
						<c:choose>
							<c:when test="${ empty dispDocu}">
								<tr class='day'><td colspan='7'><%= noneData %></td></tr>
							</c:when>
							<c:otherwise>${dispDocu}</c:otherwise>
						</c:choose>
						</tbody>
					</table>
				</div>
				<h4>신청안내</h4>
				<div class="round_txt">
              <span>평택시의회 방문/방청을 하기 위해서는 신청 및 승인과정을 거쳐야
                방문/방청을 하실 수 있으며, 방청석이 많이 준비하지 못한 관계로
                단체 방청 시에는 사전에 협의하셔야 합니다.</span>
					<ul class="dot">
						<li>개인 : 개별 실명인증 확인 후 신청</li>
						<li>
							단체 : 대표자 1인이 실명인증 확인 후 신청, 사전에 의회사무국에
							방청 협의
						</li>
						<li>회기일정은 홈페이지 의회 일정을 확인하시기 바랍니다.</li>
						<li>연락처 : 031-8024-7560~8</li>
					</ul>
				</div>
				<!-- sub_default end -->
			</div>
		</div>
		</form>


	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
