<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />

	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<script>
		$(document).ready(function(){
			$(".menu1").removeClass("current");
			$(".menu1").addClass("current_on");
			$(".depth2 > li:nth-child(6)").addClass("current_on");
			$(".depth2 > li:nth-child(6) > ul > li:nth-child(3)").addClass("current_on");
		});
		var fnActInitial    = function(){
			$("#condValue").val("");
			fnActRetrieve();
		};
		var fnActRetrieve   = function(page){
			doActLoadShow();
			$("#pageCurNo").val(isEmpty(page)?"1":page);
			$("#frmDefault").attr({"action":"visitList.do","method":"post","target":"_self"}).submit();
		};
		var fnActDetail     = function(view){
			doActLoadShow();
			$("#viewNo").val(view);
			$("#frmDefault").attr({"action":"visitView.do","method":"post","target":"_self"}).submit();
		};
	</script>


</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<!-- 메인메뉴 end -->
	<div id="subContent">
		<!-- submenu -->
		<!-- 왼쪽메뉴 -->
		<jsp:include page="/WEB-INF/views/coun/conf/conf_left.jsp" flush="true"/>
		<!-- sbumenu end -->


		<h3 class="skip">본문내용 he</h3>
		<div id="content">

			<div id="subTitle">
				<!-- sub 제목부분 -->
				<h3>신청확인&nbsp;</h3>
			</div>
			<div id="sub_default">

				<!-- program 들어갈부분 -->
				<form id='frmDefault' name='frmDefault'>
					<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
					<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
					<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>

					<h4>방문/방청 신청확인</h4>
					<div class="round_txt">
						<span>평택시의회 방문/방청을 하기위해서는 신청 및 승인과정을 거쳐야 방문/방청을 하실 수 있으며, 방청석이 많이 준비하지 못한 관계로 단체방청시에는 사전에 협의하셔야 합니다.</span>
						<ul class="dot">
							<li>개인 : 개별 실명인증 확인 후 신청</li>
							<li>단체 : 대표자 1인이 실명인증 확인 후 신청, 사전에 의회사무국에 방청 협의</li>
							<li>회기일정은 홈페이지 의회일정을 확인하시기 바랍니다.</li>
							<li>연락처 : 031-8024-7560~8</li>
						</ul>
					</div>

					<h4>신청 승인확인</h4>
					<div class='board_search'>
						<dl>
							<dt>검색어</dt>
							<dd>
								<label for='condValue' class='hide'>검색어</label>
								<input type='text' id='condValue' name='condValue' class='w25' maxlength='30' value='${condValue}' placeholder='신청자명을 입력하세요.'/>
								<a href='javascript:void(0);' class='form_btn' onclick='fnActRetrieve()'>검색</a>
							</dd>
						</dl>
					</div>

					<div class='board_total'>전체 <strong>${listCnt}</strong>건 &nbsp; ${pageCurNo} / ${pageInfo.totalPageCount}</div>
					<table class="board_list">
						<caption class='hide'>Board List</caption>
						<colgroup><col style='width:10%;'/><col style='width:10%;'/><col style='width:15%;'/><col style='width:20%;'/><col style='width:15%;'/><col style='width:10%;'/><col style='width:10%;'/></colgroup>
						<thead><tr>
							<th scope='col'>번호</th>
							<th scope='col'>구분</th>
							<th scope='col'>신청자</th>
							<th scope='col'>방문/방청일시</th>
							<th scope='col'>신청일자</th>
							<th scope='col'>신청인원</th>
							<th scope='col'>상태</th>
						</tr></thead>
						<c:choose><c:when test="${ empty dataList}">
							<tr class='pnc'><td colspan='7'><%= noneData %></td></tr>
						</c:when><c:otherwise>
							<c:forEach var="rult" varStatus="status" items="${dataList}">
							<tr class='pnc'>
								<td>${rult.rowIdx}</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							</c:forEach>
						</c:otherwise></c:choose>
						</tbody>
					</table>
					<c:if test="${!empty pageInfo}"><div class='paging'><ui:pagination paginationInfo="${pageInfo}" type="baseText" jsFunction="fnActRetrieve" /></div></c:if>

				</form>
				<!--  program 들어갈부분 end -->

			</div>



		</div>
	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
