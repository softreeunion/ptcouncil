<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />

	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<script>
		$(document).ready(function(){
			$(".menu5").removeClass("current");
			$(".menu5").addClass("current_on");
			$(".depth2 > li:nth-child(5)").addClass("current_on");
		});
		var fnActReturn     = function(seq){
			doActLoadShow();
			$("#frmDefault").attr({"action":"report"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self"}).submit();
		};
		var fnActDetail     = function(view){
			doActLoadShow();
			$("#viewNo").val(view);
			$("#frmDefault").attr({"action":"reportView.do","method":"post","target":"_self"}).submit();
		};
	</script>


</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<!-- 메인메뉴 end -->
	<div id="subContent">
		<!-- submenu -->
		<!-- 왼쪽메뉴 -->
		<jsp:include page="/WEB-INF/views/coun/conf/conf_left.jsp" flush="true"/>
		<!-- sbumenu end -->


		<h3 class="skip">본문내용 he</h3>
		<div id="content">

			<div id="subTitle">
				<!-- sub 제목부분 -->
				<h3>7분자유발언&nbsp;</h3>
			</div>
			<div id="sub_default">

				<!-- program 들어갈부분 -->
				<form id='frmDefault' name='frmDefault'>
					<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
					<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
					<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
					<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
					<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
					<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
					<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
					<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
					<input type='hidden' id='condDele'      name='condDele'/>

					<table class="board_view">
						<caption class='hide'>Board View</caption>
						<colgroup><col style='width:10%;'/><col style='width:30%;'/><col style='width:10%;'/><col style='width:20%;'/><col style='width:10%;'/><col style='width:20%;'/></colgroup>
						<tbody><tr>
							<th>제목</th>
							<td colspan='5'>${dataView.bbsTitle}</td>
						</tr><tr>
							<th scope='col'>발언자</th>
							<td>${dataView.creName}</td>
							<th scope='col'>발언일자</th>
							<td>${dataView.regiDate}</td>
							<th scope='col'>조회수</th>
							<td>${dataView.hitCount}</td>
						</tr><tr>
							<th>발언회의록</th><%--
					<td colspan='5'>${dataView.confName1}<c:if test="${!empty dataView.confCode1 and !empty dataView.confName1}">&nbsp; &nbsp;<input type='button' class='btn_small bt_white' style='width:100px;cursor:pointer;' value='▶ 회의록보기' onclick='open_viewer("<%= webBase %>/viewer/viewer_frame.jsp?conf_code=${dataView.confCode1}&conf_name=${dataView.confName1}")'/></c:if></td>--%>
							<td colspan='5'>${dataView.confName1}
								<c:if test="${!empty dataView.confCode1 and !empty dataView.confName1}">&nbsp; &nbsp;
									<%--<input type='button' class='btn_small bt_white' style='width:100px;cursor:pointer;' value='▶ 회의록보기' onclick='fnDocuViewer("${dataView.confUUID1}")'/>--%>
									<a href="javascript:void(0);" onclick='fnDocuViewer("${dataView.confUUID1}")' class="btn_list_view" title="바로보기">회의록보기</a>
								</c:if>
							</td>
						</tr><tr>
							<th>첨부파일</th>
							<td colspan='5'>${dataView.fileLinkS}</td>
						</tr><tr class='h200'>
							<td colspan='6' class='nonebd_l cont'><c:choose><c:when test="${'P' eq dataView.contPattern}">${dataView.contDescBR}</c:when><c:otherwise>${dataView.contDesc}</c:otherwise></c:choose></td>
						</tr></tbody>
					</table>
					<div class='right'>
						<a href='javascript:void(0);' class='btn' onclick='fnActReturn("seq")'>목록</a>
					</div>
					<c:if test="${'T' eq moveItem}">
					<div class='board_view mT20'><table>
						<caption class='hide'>Previous/Next</caption>
						<colgroup><col style='width:10%;'/><col style='width:90%;'/></colgroup>
						<tbody><tr>
							<th scope='col'>이전글</th>
							<td>&nbsp;&nbsp;<c:choose><c:when test="${ empty prevView}">이전 글이 존재 하지 않습니다.</c:when><c:otherwise>${prevView.gabSpace}<a href='javascript:void(0);' onclick='fnActDetail("${prevView.viewNo}")'><c:if test="${'Y' eq prevView.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${prevView.bbsTitle}</a></c:otherwise></c:choose></td>
						</tr><tr>
							<th scope='col'>다음글</th>
							<td>&nbsp;&nbsp;<c:choose><c:when test="${ empty nextView}">다음 글이 존재 하지 않습니다.</c:when><c:otherwise>${nextView.gabSpace}<a href='javascript:void(0);' onclick='fnActDetail("${nextView.viewNo}")'><c:if test="${'Y' eq nextView.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${nextView.bbsTitle}</a></c:otherwise></c:choose></td>
						</tr></tbody>
					</table></div>
					</c:if>
					<%--<div class='board_view mT20'><table>--%>
						<%--<caption class='hide'>Previous/Next</caption>--%>
						<%--<colgroup><col style='width:10%;'/><col style='width:90%;'/></colgroup>--%>
						<%--<tbody><tr>--%>
							<%--<th scope='col'>이전글</th>--%>
							<%--<td>&nbsp;&nbsp;<a href='javascript:void(0);' onclick=''>답변없는 글내용 </a></td>--%>
						<%--</tr><tr>--%>
							<%--<th scope='col'>다음글</th>--%>
							<%--<td>&nbsp;&nbsp;<a href='javascript:void(0);' onclick=''>답변없는 글내용</a></td>--%>
						<%--</tr></tbody>--%>
					<%--</table>--%>
					<%--</div>--%>

				</form>
				<!--  program 들어갈부분 end -->

			</div>



		</div>
	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
