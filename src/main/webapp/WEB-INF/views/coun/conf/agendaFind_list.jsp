<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />

	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<script>
		$(document).ready(function(){
			$(".menu5").removeClass("current");
			$(".menu5").addClass("current_on");
			$(".depth2 > li:nth-child(6)").addClass("current_on");
			$(".depth2 > li:nth-child(6) > ul > li:nth-child(1)").addClass("current_on");
		});
		var fnActChanger    = function(){
			$.ajax({
				type          : "post",
				url           : "<%= webBase %>/cmmn/seleRound.do",
				dataType      : "html",
				data          : {era:$("#reEraCode").val(),rnd:"${reRndCode}"},
				success       : function(data,status,xhr  ){$("#reRndCode").empty();$("#reRndCode").html(data);},
				error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
			});
		};
		var fnActRetrieve   = function(page){
			doActLoadShow();
			$("#pageCurNo").val(isEmpty(page)?"1":page);
			$("#frmDefault").attr({"action":"agendaList.do","method":"post","target":"_self"}).submit();
		};
		var fnActDetail     = function(view){
			doActLoadShow();
			$("#viewNo").val(view);
			$("#frmDefault").attr({"action":"agendaView.do","method":"post","target":"_self"}).submit();
		};
	</script>


</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<!-- 메인메뉴 end -->
	<div id="subContent">
		<!-- submenu -->
		<!-- 왼쪽메뉴 -->
		<jsp:include page="/WEB-INF/views/coun/conf/conf_left.jsp" flush="true"/>
		<!-- sbumenu end -->


		<h3 class="skip">본문내용 he</h3>
		<div id="content">

			<div id="subTitle">
				<!-- sub 제목부분 -->
				<h3>의안검색&nbsp;</h3>
			</div>
			<div id="sub_default">

				<!-- program 들어갈부분 -->
				<form id='frmDefault' name='frmDefault'>
					<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
					<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
					<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>

					<div class='board_search'>
						<dl>
							<dt>대수</dt>
							<dd><label for='reEraCode' class='hide'>대수</label>
								<select id='reEraCode' name='reEraCode' class='wp100' onchange='fnActChanger()'><c:choose><c:when test="${ empty eraList}">
									<option value='08'<c:if test="${'08' eq reEraCode}"> selected='selected'</c:if>>제8대</option></c:when><c:otherwise><c:forEach var="cate" varStatus="status" items="${eraList}">
									<option value='${cate.eraCode}'<c:if test="${reEraCode eq cate.eraCode}"> selected='selected'</c:if>>${cate.eraName}</option></c:forEach></c:otherwise></c:choose>
								</select>
							</dd>
							<dt>회기</dt>
							<dd><label for='reRndCode' class='hide'>회기</label>
								<select id='reRndCode' name='reRndCode' class='wp100'><option value=''>전체</option><c:if test="${!empty rndList}"><c:forEach var="cate" varStatus="status" items="${rndList}">
									<option value='${cate.roundCode}'<c:if test="${reRndCode eq cate.roundCode}"> selected='selected'</c:if>>${cate.roundName}</option></c:forEach></c:if>
								</select>
							</dd>
							<dt>의안종류</dt>
							<dd><label for='reSugCode' class='hide'>의안종류</label>
								<select id='reSugCode' name='reSugCode' class='wp100'><option value=''>전체</option><c:if test="${!empty sugList}"><c:forEach var="cate" varStatus="status" items="${sugList}">
									<option value='${cate.sugCode}'<c:if test="${reSugCode eq cate.sugCode}"> selected='selected'</c:if>>${cate.sugName}</option></c:forEach></c:if>
								</select>
							</dd>
							<dt>의안결과</dt>
							<dd><label for='reResCode' class='hide'>의안결과</label>
								<select id='reResCode' name='reResCode' class='wp100'><option value=''>전체</option><c:if test="${!empty resList}"><c:forEach var="cate" varStatus="status" items="${resList}">
									<option value='${cate.resCode}'<c:if test="${reResCode eq cate.resCode}"> selected='selected'</c:if>>${cate.resName}</option></c:forEach></c:if>
								</select>
							</dd>
							<dt>소관위원회</dt>
							<dd><label for='reComCode' class='hide'>소관위원회</label>
								<select id='reComCode' name='reComCode' class='w250'><option value=''>전체</option><c:if test="${!empty comList}"><c:forEach var="cate" varStatus="status" items="${comList}">
									<option value='${cate.comCode}'<c:if test="${reComCode eq cate.comCode}"> selected='selected'</c:if>>${cate.comName}</option></c:forEach></c:if>
								</select>
							</dd>
							<dt>발의자</dt>
							<dd><label for='searchRegi' class='hide'>발의자</label><input type='text' id='searchRegi' name='searchRegi' class='w30' maxlength='30' value='${searchRegi}' placeholder='발의자를 입력하세요.'/>
							</dd>
							<dt>의안명</dt>
							<dd>
								<label for='searchText' class='hide'>의안명</label>
								<input type='text' id='searchText' name='searchText' class='w30' maxlength='30' value='${searchText}' placeholder='의안명을 입력하세요.'/>
								<a href='javascript:void(0);' class='form_btn' onclick='fnActRetrieve()'>검색</a>
							</dd>
						</dl>
					</div>

					<div class='board_total'>전체 <strong>${listCnt}</strong>건 &nbsp; ${pageCurNo} / ${pageInfo.totalPageCount}</div>

					<table class="board_list">
						<caption class='hide'>Board List</caption>
						<colgroup><col style='width:5%;'/><col style='width:10%;'/><col style='width:30%;'/><col style='width:15%;'/><col style='width:15%;'/><col style='width:10%;'/><col style='width:10%;'/><col style='width:10%;'/></colgroup>
						<thead><tr>
							<th scope='col'>번호</th>
							<th scope='col'>의안번호</th>
							<th scope='col'>의안명</th>
							<th scope='col'>발의자</th>
							<th scope='col'>소관위원회</th>
							<th scope='col'>발의일</th>
							<th scope='col'>처리일</th>
							<th scope='col'>처리결과</th>
						</tr></thead>
						<tbody>
						<c:choose><c:when test="${ empty dataList}">
							<tr class='pnc'><td colspan='8'><%= noneData %></td></tr>
						</c:when><c:otherwise>
							<c:forEach var="rult" varStatus="status" items="${dataList}">
								<tr class='pnc' onclick='fnActDetail("${rult.viewNo}")'>
									<td>${rult.rowIdx}</td>
									<td>${rult.billNum}</td>
									<td class='left' title='${rult.billTitle}'>${rult.billTitle}</td>
									<td>${rult.sugMan}</td>
									<td>${rult.comName}</td>
									<td>${rult.sugDate}</td>
									<td>${rult.genVote}</td>
									<td>${rult.genResName}</td>
								</tr>
							</c:forEach></c:otherwise></c:choose>
						</tbody>
					</table>
					<c:if test="${!empty pageInfo}"><div class='paging'><ui:pagination paginationInfo="${pageInfo}" type="baseText" jsFunction="fnActRetrieve" /></div></c:if>
				</form>
				<!--  program 들어갈부분 end -->

			</div>



		</div>
	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
