<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />

	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<link rel='stylesheet' media='all' href='<%= webBase %>/plugins/fancytree/skin-lion/ui.fancytree.css'>
	<script src='<%= webBase %>/plugins/fancytree/jquery.fancytree-all.min.js'></script>
	<script src='<%= webBase %>/plugins/fancytree/jquery.fancytree.persist.js'></script>

	<script>
		$(document).ready(function(){
			$(".menu5").removeClass("current");
			$(".menu5").addClass("current_on");
			$(".depth2 > li:nth-child(2)").addClass("current_on");
			$(".depth2 > li:nth-child(2) > ul > li:nth-child(3)").addClass("current_on");
			$("#tree").fancytree({
				clickFolderMode:2,
				extensions:["persist"],
				persist:{expandLazy:true,overrideSource:false,store:"auto"},
				source:<c:if test="${ empty cateTree}">[]</c:if>${cateTree},
				lazyLoad:function(event,data){
					if(data.node.data.mode=="session"){<%--연도별 - 회의일정--%>
						data.result = {data:{act:data.node.data.mode,rid:data.node.data.root,yos:data.node.data.yos},url:"<%= webBase %>/year/loadClass.do"}
					}else if(data.node.data.mode=="class"){<%--연도별 - 회의구분(상위)--%>
						data.result = {data:{act:data.node.data.mode,rid:data.node.data.root,era:data.node.data.era,rnd:data.node.data.rnd},url:"<%= webBase %>/sess/loadSession.do"}
					}else if(data.node.data.mode=="kind"){<%--연도별 - 회의구분(하위)--%>
						data.result = {data:{act:data.node.data.mode,rid:data.node.data.root,era:data.node.data.era,rnd:data.node.data.rnd,knd:data.node.data.knd},url:"<%= webBase %>/sess/loadKind.do"}
					}else if(data.node.data.mode=="order"){<%--회의차수--%>
						data.result = {data:{act:data.node.data.mode,rid:data.node.data.root,era:data.node.data.era,rnd:data.node.data.rnd,knd:data.node.data.knd},url:"<%= webBase %>/cmmn/loadOrder.do"}
					}else{
						alert("입력값이 잘못되었습니다.");return false;
					}
				}
			});
		});
	</script>
	<style>.damo {padding:16px;border:1px solid #d6d6d6;box-sizing:border-box;min-height:160px;}</style>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<!-- 메인메뉴 end -->
	<div id="subContent">
		<!-- submenu -->
		<!-- 왼쪽메뉴 -->
		<jsp:include page="/WEB-INF/views/coun/conf/conf_left.jsp" flush="true"/>
		<!-- sbumenu end -->


		<h3 class="skip">본문내용 he</h3>
		<div id="content">

			<div id="subTitle">
				<!-- sub 제목부분 -->
				<h3>연도별 검색&nbsp;</h3>
			</div>
			<div id="sub_default">

				<!-- program 들어갈부분 -->
				<div id='tree' class='damo'></div>
				<!--  program 들어갈부분 end -->

			</div>

		</div>
	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
