<%
////////////////////////////////////////////////////////////
// Program ID  : minutesTemp_list
// Description : 회의록검색 > 임시회의록 [목록조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "임시회의록";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb05_09").addClass("on").removeClass("noChild");
	});
	var fnActRetrieve   = function(page) { 
		doActLoadShow();
		$("#pageCurNo").val(isEmpty(page)?"1":page);
		$("#frmDefault").attr({"action":"minutesList.do","method":"post","target":"_self"}).submit();
	};
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/coun/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/coun/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="5"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 회의록검색 &gt; <%= subTitle %></div>
			<div class='board_total'>전체 <strong>${listCnt}</strong>건 &nbsp; ${pageCurNo} / ${pageInfo.totalPageCount}</div>
			<div class='table_list'><table>
				<caption class='hide'>Board List</caption>
				<colgroup><col style='width:10%;'/><col style='width:10%;'/><col style='width:10%;'/><col style='width:14%;'/><col style='width:42%;'/><col style='width:14%;'/></colgroup>
				<thead><tr>
					<th scope='col'>번호</th>
					<th scope='col'>대수</th>
					<th scope='col'>회수</th>
					<th scope='col'>차수</th>
					<th scope='col'>회의명</th>
					<th scope='col'>회의일자</th>
				</tr></thead>
				<tbody>
				<c:choose><c:when test="${ empty dataList}">
					<tr class='h200'><td colspan='6'><%= noneData %></td></tr>
				</c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${dataList}">
					<tr class='pnc' onclick='fnDocuViewer("${rult.confUUID}")'>
						<td>${rult.rowIdx}</td>
						<td>${rult.eraName}</td>
						<td>${rult.roundName}</td>
						<td>${rult.pointName}</td><%--
						<td><a href='javascript:void(0);' onclick='open_viewer("<%= webBase %>/viewer/viewer_frame.jsp?conf_code=${rult.confCode}&conf_name=${rult.confName}")'>${rult.kindName}</a></td>--%>
						<td><c:if test="${'T' eq rult.tempFlag}"><span class='td_blu'>[임시회의록]</span> </c:if><c:if test = "${fn:contains(rult.kindCode,'4')}">행정사무감사 - </c:if>${rult.kindName}</td>
						<td>${rult.confDate}(${rult.confWeek})</td>
					</tr>
				</c:forEach></c:otherwise></c:choose>
				</tbody>
			</table></div>
			<c:if test="${!empty pageInfo}"><div class='paging'><ui:pagination paginationInfo="${pageInfo}" type="baseText" jsFunction="fnActRetrieve" /></div></c:if>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer.jsp" flush="true"><jsp:param name="qk" value="F"/></jsp:include>
	<!-- //footer -->
</div>
</form>
</body>
</html>