<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>

<!-- sub menu -->
<div id="submenuArea" class="w sm_intro">
	<div class="sm_tit ">
		<h2>
			회의록검색
		</h2>
	</div>
	<ul class="depth2">
		<!-- 메뉴 활성화시 class="current" 가 class="current_on"으로 바뀌도록 -->
		<li class="current"> <a href="/coun/past/minutesList.do" title="최근회의록">최근회의록</a> </li>
		<li class="current"> <a href="/coun/sess/minutesList.do" title="단순검색">단순검색</a>
			<ul>
				<li class="current"> <a href="/coun/sess/minutesList.do" title="회기별 검색">회기별 검색</a> </li>
				<li class="current"> <a href="/coun/meet/minutesList.do" title="회의별 검색">회의별 검색</a> </li>
				<li class="current"> <a href="/coun/year/minutesList.do" title="연도별 검색">연도별 검색</a> </li>
			</ul>
		</li>
		<li class="current"> <a href="/coun/tail/minutesList.do" title="상세검색">상세검색</a></li>
		<li class="current"> <a href="/coun/gover/reportList.do" title="시정질문답변">시정질문답변</a></li>
		<li class="current"> <a href="/coun/spch/reportList.do" title="7분자유발언">7분자유발언</a></li>
		<li class="current"> <a href="/coun/find/agendaList.do" title="의안정보">의안정보</a>
			<ul>
				<li class="current"> <a href="/coun/find/agendaList.do" title="의안검색">의안검색</a> </li>
				<li class="current"> <a href="/coun/info/agendaList.do" title="의안정보">의안정보</a> </li>
				<li class="current"> <a href="/coun/item/agendaList.do" title="의안항목">의안항목</a> </li>
				<li class="current"> <a href="/coun/kind/agendaStat.do" title="의안통계">의안통계</a> </li>
			</ul>
		</li>
		<li class="current"> <a href="/coun/days/minutesStat.do" title="회의통계">회의통계</a> </li>
		<li class="current"> <a href="/coun/cast/general.do?ct=1" title="영상회의록">영상회의록</a>
			<ul>
				<li class="current"> <a href="/coun/cast/general.do?ct=1" title="본회의">본회의</a> </li>
				<li class="current"> <a href="/coun/cast/general.do?ct=2" title="의회운영위원회">의회운영위원회</a> </li>
				<li class="current"> <a href="/coun/cast/general.do?ct=3" title="기획행정위원회">기획행정위원회</a> </li>
				<li class="current"> <a href="/coun/cast/general.do?ct=4" title="복지환경위원회">복지환경위원회</a> </li>
				<li class="current"> <a href="/coun/cast/general.do?ct=5" title="산업건설위원회">산업건설위원회</a> </li>
				<li class="current"> <a href="/coun/cast/general.do?ct=6" title="예산결산특별위원회">예산결산특별위원회</a> </li>
			</ul>
		</li>
	</ul>

	<!-- sub menu end -->
</div>
			
			
			