<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />

	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<script>
		$(document).ready(function(){
			$(".menu5").removeClass("current");
			$(".menu5").addClass("current_on");
			$(".depth2 > li:nth-child(7)").addClass("current_on");
		});
		var fnActRetrieve   = function(page) {
			doActLoadShow();
			$("#pageCurNo").val(isEmpty(page)?"1":page);
			$("#frmDefault").attr({"action":"minutesStat.do","method":"post","target":"_self"}).submit();
		};
	</script>


</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<!-- 메인메뉴 end -->
	<div id="subContent">
		<!-- submenu -->
		<!-- 왼쪽메뉴 -->
		<jsp:include page="/WEB-INF/views/coun/conf/conf_left.jsp" flush="true"/>
		<!-- sbumenu end -->


		<h3 class="skip">본문내용 he</h3>
		<div id="content">

			<div id="subTitle">
				<!-- sub 제목부분 -->
				<h3>회의통계&nbsp;</h3>
			</div>
			<div id="sub_default">

				<!-- program 들어갈부분 -->
				<form id='frmDefault' name='frmDefault'>
					<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
					<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
					<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>

					<div class='sub_tab_Area two'><ul>
						<li><a href='/coun/days/minutesStat.do' class='on' >회의일자</a></li>
						<li><a href='/coun/time/minutesStat.do'  >회의시간</a></li>
					</ul></div>

					<div class='board_search'>
						<dl>
							<dt>대수</dt>
							<dd><label for='reEraCode' class='hide'>대수</label>
								<select id='reEraCode' name='reEraCode' class='wp100' onchange='fnActChanger()'><c:choose><c:when test="${ empty eraList}">
									<option value='08'<c:if test="${'08' eq reEraCode}"> selected='selected'</c:if>>제8대</option></c:when><c:otherwise><c:forEach var="cate" varStatus="status" items="${eraList}">
									<option value='${cate.eraCode}'<c:if test="${reEraCode eq cate.eraCode}"> selected='selected'</c:if>>${cate.eraName}</option></c:forEach></c:otherwise></c:choose>
								</select>
								<a href='javascript:void(0);' class='form_btn' onclick='fnActRetrieve()'>검색</a>

							</dd>
						</dl>
					</div>
					<div class='board_total'>전체 <strong>${listCnt}</strong>건</div>
					<table class="board_list">
						<caption class='hide'>Board List</caption>
						<colgroup><col style='width:10%;'/><col style='width:10%;'/><col style='width:20%;'/><col style='width:8%;'/><col style='width:8%;'/><col style='width:8%;'/><col style='width:8%;'/><col style='width:10%;'/><col style='width:10%;'/></colgroup>
						<thead><tr>
							<th scope='col'>대수</th>
							<th scope='col'>회수</th>
							<th scope='col'>회기</th>
							<th scope='col'>회의일수</th>
							<th scope='col'>본회의</th>
							<th scope='col'>상임위원회</th>
							<th scope='col'>특별위원회</th>
							<th scope='col'>행정사무감사</th>
							<th scope='col'>행정사무조사</th>
						</tr></thead>
						<tbody>
						<c:choose><c:when test="${ empty dataList}">
							<tr class='pnc'><td colspan='9'><%= noneData %></td></tr>
						</c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${dataList}"><c:choose><c:when test="${'xsum' eq rult.eraCode}">
							<tr class='pnc'>
								<td${rult.fontColor} colspan='3'>${rult.eraName}</td>
								<td${rult.fontColor}>${rult.rndDiffDate}</td>
								<td${rult.fontColor}>${rult.knd100}</td>
								<td${rult.fontColor}>${rult.knd200}</td>
								<td${rult.fontColor}>${rult.knd300}</td>
								<td${rult.fontColor}>${rult.knd400}</td>
								<td${rult.fontColor}>${rult.knd500}</td>
							</tr></c:when><c:otherwise>
							<tr>
								<td>${rult.eraName}</td>
								<td>${rult.roundName}</td>
								<td>${rult.roundStartDate} ~ ${rult.roundCloseDate}</td>
								<td>${rult.rndDiffDate}</td>
								<td>${rult.knd100}</td>
								<td>${rult.knd200}</td>
								<td>${rult.knd300}</td>
								<td>${rult.knd400}</td>
								<td>${rult.knd500}</td>
							</tr></c:otherwise></c:choose>
						</c:forEach></c:otherwise></c:choose>
						</tbody>
					</table>


				</form>
				<!--  program 들어갈부분 end -->

			</div>



		</div>
	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
