<%
////////////////////////////////////////////////////////////
// Program ID  : noticeArch_huin
// Description : 열린마당 > 의회자료실 [로그인화면]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "의회자료실";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg.jspf"/>
	<script src='<%= webBase %>/scripts/rsa/jsbn.js'></script>
	<script src='<%= webBase %>/scripts/rsa/rsa.js'></script>
	<script src='<%= webBase %>/scripts/rsa/prng4.js'></script>
	<script src='<%= webBase %>/scripts/rsa/rng.js'></script>
	<script>
	$(document).ready(function(){
		$("#snb06_03").addClass("on").removeClass("noChild");
		$("#k_username").focus();
		$("#frmDefault input").keypress(function(e){
			if(e.which == 13) { 
				if($.trim($("#k_username").val())=="") { 
					alert("아이디를 입력 해주세요.");
					$("#k_username").focus();
					return false;
				} else if($.trim($("#k_userpass").val())=="") { 
					alert("비밀번호를 입력 해주세요.");
					$("#k_userpass").focus();
					return false;
				} else { 
					fnActLogin();
				}
			}
		});
	});
	var fnActLogin      = function(){
		var rsa = new RSAKey();
		if( $.trim($("#k_username").val())=="" ) { 
			alert("아이디를 입력 해주세요.");
			$("#k_username").focus();
			return false;
		} else if( $.trim($("#k_userpass").val())=="" ) { 
			alert("비밀번호를 입력 해주세요.");
			$("#k_userpass").focus();
			return false;
		}
		rsa.setPublic($("#rsaKeyModulus").val(),$("#rsaKeyExponent").val());
		$("#j_username").val(rsa.encrypt($("#k_username").val()));
		$("#j_userpass").val(rsa.encrypt($("#k_userpass").val()));
		$("#k_username").val("");
		$("#k_userpass").val("");
		$("#frmDefault").attr({"action":"noticeHuin_proc.do","method":"post"}).submit();
	};
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='rsaKeyModulus' name='rsaKeyModulus' value='${RSAModulus}'/>
<input type='hidden' id='rsaKeyExponent' name='rsaKeyExponent' value='${RSAExponent}'/>
<input type='hidden' id='j_username' name='j_username'>
<input type='hidden' id='j_userpass' name='j_userpass'>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/coun/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/coun/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="6"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 열린마당 &gt; <%= subTitle %></div>
			<div class='board_login'>
				<ul>
					<li>로그인이 필요한 서비스입니다.</li>
					<li>아이디와 비밀번호를 입력해주세요.</li>
				</ul>
				<div class='login_input'>
					<label for='k_username' class='hide'>아이디</label>
					<input type='text' id='k_username' name='k_username' maxlength='20' placeholder='아이디'/>
					<label for='k_userpass' class='hide'>비밀번호</label>
					<input type='password' id='k_userpass' name='k_userpass' maxlength='20' placeholder='비밀번호'/>
					<span><a class='log_btn' href='javascript:void(0);' onclick='fnActLogin()'>로그인</a></span>
				</div>
			</div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer.jsp" flush="true"><jsp:param name="qk" value="F"/></jsp:include>
	<!-- //footer -->
</div>
</form>
</body>
</html>