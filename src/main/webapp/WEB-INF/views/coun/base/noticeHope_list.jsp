<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>


	<script>
		$(document).ready(function(){
			$(".menu6").removeClass("current");
			$(".menu6").addClass("current_on");
			$(".depth2 > li:nth-child(2)").addClass("current_on");
		});
		var fnActInitial    = function(){
			$("#condType").val("A");
			$("#condValue").val("");
			fnActRetrieve();
		};
		var fnActRetrieve   = function(page){
			doActLoadShow();
			$("#pageCurNo").val(isEmpty(page)?"1":page);
			$("#frmDefault").attr({"action":"noticeList.do","method":"post","target":"_self"}).submit();
		};
		var fnActDetail     = function(view,opts){
			var show=false;
			if(opts=="Y"){if(confirm("비공개 게시물입니다.")){show=true;}else{show=false;}}else{show=true;}
			if(show){
				doActLoadShow();
				$("#viewNo").val(view);
				$("#frmDefault").attr({"action":"noticeView.do","method":"post","target":"_self"}).submit();
			}else{
				return;
			}
		};
		var fnActCreate     = function(){
			doActLoadShow();
			$("#frmDefault").attr({"action":"noticeInst.do","method":"post","target":"_self"}).submit();
		};
	</script>
</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<!-- 메인메뉴 end -->
	<div id="subContent">
		<!-- submenu -->
		<!-- 왼쪽메뉴 -->
		<jsp:include page="/WEB-INF/views/coun/base/base_left_01.jsp" flush="true"/>
		<!-- sbumenu end -->


		<h3 class="skip">본문내용 he</h3>
		<div id="content">

			<div id="subTitle">
				<!-- sub 제목부분 -->
				<h3>의회에 바란다</h3>
			</div>
			<div id="sub_default">

				<!-- program 들어갈부분 -->
				<form id='frmDefault' name='frmDefault'>
					<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
					<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
					<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>

					<div class="round_txt">
						근거 없는 비판이나 자신의 이익에 국한된 사항은 자제하여 주시기 바라며,
						게시판의 성격에 맞지 않는 <span class="red">저속한 표현, 타인의 명예훼손, 불건전한 내용</span>의 글은 본인 동의 없이 삭제될 수 있습니다.
						<br>
						※ 작성 시 개인 정보(휴대전화, 주민등록번호 등)가 노출되지 않도록 주의 바랍니다.
					</div>

					<script>
						// 삭제사유 보이기/숨기기 함수
						function toggle_txt() {
							var reason = document.getElementById('slide_law');
							var button = document.getElementById('toggleButton');

							// 텍스트 보이기/숨기기
							if (reason.style.display === 'none') {
								reason.style.display = 'block';
								button.textContent = '게시글 삭제사유 닫기';
							} else {
								reason.style.display = 'none';
								button.textContent = '게시글 삭제사유 보기';
							}
						}
					</script>
					<div class="center pT30">
						<a href='javascript:void(0);' class='btn orange' id="toggleButton" onclick='toggle_txt()'>게시글 삭제사유 보기</a>
					</div>

					<div id='slide_law' class='round_txt' style='display:none;'>
						<ul class="dot">
							<li>1. 국가안전이나 보안에 위배되는 경우</li>
							<li>2. 정치적 목적이나 성향이 있는 경우</li>
							<li>3. 특정기관, 단체, 부서를 근거없이 비난하는 경우</li>
							<li>4. 특정인을 비방하거나 명예훼손의 우려가 있는 경우</li>
							<li>5. 영리목적의 상업성 광고, 저작권을 침해할 수 있는 내용</li>
							<li>6. 욕설, 음란물 등 불건전한 내용</li>
							<li>7. 실명을 원칙으로 하는 경우에 실명을 사용하지 않았거나 실명이 아닌 것으로 판단되었을 경우</li>
							<li>8. 동일인 또는 동일인이라고 인정되는 자가 똑같은 내용을 주 2 회이상 게시하거나 비슷한 내용을 1일 2회이상 게시하는 경우</li>
							<li>9. 게시글에 대한 인위적인 조회 건수 조작</li>
							<li>10. 기타 연습성, 오류, 장난성의 내용 등</li>
						</ul></div>


					<div class='board_search'>
						<dl>
							<dt>검색</dt>
							<dd><label for='condType' class='hide'>검색영역</label>
								<select id='condType' name='condType' class='wp100'>
									<option value='A'  <c:if test="${'A'  eq condType}"> selected='selected'</c:if>>전체</option>
									<option value='T'  <c:if test="${'T'  eq condType}"> selected='selected'</c:if>>제목</option>
									<option value='D'  <c:if test="${'D'  eq condType}"> selected='selected'</c:if>>내용</option>
								</select></dd>
							<dt>검색어</dt>
							<dd>
								<label for='condValue' class='hide'>검색어</label>
								<input type='text' id='condValue' name='condValue' class='w25' maxlength='30' value='${condValue}' placeholder='검색어를 입력하세요.'/>
								<a href='javascript:void(0);' class='form_btn' onclick='fnActRetrieve()'>검색</a>
							</dd>
						</dl>
					</div>

					<div class='board_total'>전체 <strong>${listCnt}</strong>건 &nbsp; ${pageCurNo} / ${pageInfo.totalPageCount}</div>
					<table class="board_list">
						<caption class='hide'>Board List</caption>
						<colgroup><col style='width:10%;' class='w_no'/><col style='width:50%;'/><col style='width:15%;' class='w_wn'/><col style='width:15%;' class='w_wd'/><col style='width:10%;' class='w_wr'/></colgroup>
						<thead><tr>
							<th scope='col'>번호</th>
							<th scope='col'>제목</th>
							<th scope='col'>작성자</th>
							<th scope='col'>작성일</th>
							<th scope='col'>조회</th>
						</tr></thead>
						<tbody>
						<c:if test="${!empty notiList}"><c:forEach var="noti" varStatus="status" items="${notiList}">
							<tr class='pnc' onclick='fnActDetail("${noti.viewNo}")'>
								<td class='t_red'>공지</td>
								<td class='left'>${noti.bbsTitle}</td>
								<td>${noti.creName}</td>
								<td>${noti.creDate}</td>
								<td>${noti.hitCount}</td>
							</tr>
						</c:forEach></c:if>
						<c:choose>
							<c:when test="${ empty dataList}">
								<tr class='pnc'><td colspan='5'><%= noneData %></td></tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="rult" varStatus="status" items="${dataList}">

									<tr class='pnc' onclick='fnActDetail("${rult.viewNo}","${rult.creHide}")'>
										<td>${rult.rowIdx}</td>
										<td class='left'>
											<c:if test="${'Y' eq rult.creHide && false}">
												<img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/>
											</c:if>
												${rult.bbsTitle}
										</td>
										<td>${rult.creName}</td>
										<td>${rult.creDate}</td>
										<td>${rult.hitCount}</td>
									</tr>

								</c:forEach>
							</c:otherwise>
						</c:choose>
						</tbody>
					</table>

					<c:if test="${!empty pageInfo}"><div class='paging'><ui:pagination paginationInfo="${pageInfo}" type="baseText" jsFunction="fnActRetrieve" /></div></c:if>

					<!-- 게시판 하단버튼 -->
					<div class="right">
						<a href='javascript:void(0);' class='btn btn-primary' onclick='fnActCreate()'>등록하기</a>

					</div>

				</form>
				<!--  program 들어갈부분 end -->
			</div>



		</div>
	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
