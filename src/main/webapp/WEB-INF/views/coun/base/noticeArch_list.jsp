<%
////////////////////////////////////////////////////////////
// Program ID  : noticeArch_list
// Description : 열린마당 > 의회자료실 [목록조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "의회자료실";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb06_03").addClass("on").removeClass("noChild");
	});
	var fnActInitial    = function(){
		$("#condType").val("A");
		$("#condValue").val("");
		fnActRetrieve();
	};
	var fnActRetrieve   = function(page){
		doActLoadShow();
		$("#pageCurNo").val(isEmpty(page)?"1":page);
		$("#frmDefault").attr({"action":"noticeList.do","method":"post","target":"_self"}).submit();
	};
	var fnActDetail     = function(view){
		doActLoadShow();
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"noticeView.do","method":"post","target":"_self"}).submit();
	};<c:if test="${!empty MemUInfo}">
	var fnActCreate     = function(){
		doActLoadShow();
		$("#frmDefault").attr({"action":"noticeInst.do","method":"post","target":"_self"}).submit();
	};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/coun/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/coun/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="6"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 열린마당 &gt; <%= subTitle %></div><c:if test="${!empty MemUInfo}">
			<div class='board_log_btn'><a href='<%= webBase %>/coun/hcout_proc.do' class='td_none'>로그아웃</a></div></c:if>
			<div class='program_search'><table>
				<caption class='hide'>Search Detail</caption>
				<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
				<tbody><tr>
					<th scope='col' class='center'>검색</th>
					<td><label for='condType' class='hide'>검색영역</label>
						<select id='condType' name='condType' class='w100'>
							<option value='A'  <c:if test="${'A'  eq condType}"> selected='selected'</c:if>>전체</option>
							<option value='T'  <c:if test="${'T'  eq condType}"> selected='selected'</c:if>>제목</option>
							<option value='D'  <c:if test="${'D'  eq condType}"> selected='selected'</c:if>>내용</option>
						</select>
					</td>
				</tr><tr>
					<th class='center'>검색어</th>
					<td><label for='condValue' class='hide'>검색어</label><input type='text' id='condValue' name='condValue' class='w60_p' maxlength='30' value='${condValue}' placeholder='검색어를 입력하세요.'/></td>
				</tr><tr>
					<td colspan='2' class='center'><a href='javascript:void(0);' class='form_btn' onclick='fnActRetrieve()'>검색</a></td>
				</tr></tbody>
			</table></div>
			<div class='board_total'>전체 <strong>${listCnt}</strong>건 &nbsp; ${pageCurNo} / ${pageInfo.totalPageCount}</div>
			<div class='table_list'><table>
				<caption class='hide'>Board List</caption>
				<colgroup><col style='width:10%;' class='w_no'/><col style='width:50%;'/><col style='width:15%;' class='w_wn'/><col style='width:15%;' class='w_wd'/><col style='width:10%;' class='w_wr'/></colgroup>
				<thead><tr>
					<th scope='col'>번호</th>
					<th scope='col'>제목</th>
					<th scope='col'>작성자</th>
					<th scope='col'>작성일</th>
					<th scope='col'>조회</th>
				</tr></thead>
				<tbody>
				<c:choose><c:when test="${ empty dataList}">
					<tr class='h200'><td colspan='5'><%= noneData %></td></tr>
				</c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${dataList}">
					<tr class='pnc' onclick='fnActDetail("${rult.viewNo}")'>
						<td>${rult.rowIdx}</td>
						<td class='left'>${rult.gabSpace}<c:if test="${'Y' eq rult.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${rult.bbsTitle}</td>
						<td>${rult.creName}</td>
						<td>${rult.creDate}</td>
						<td>${rult.hitCount}</td>
					</tr>
				</c:forEach></c:otherwise></c:choose>
				</tbody>
			</table></div>
			<c:if test="${!empty pageInfo}"><div class='paging'><ui:pagination paginationInfo="${pageInfo}" type="baseText" jsFunction="fnActRetrieve" /></div></c:if>
			<div class='btn_group right'><c:if test="${!empty MemUInfo}">
				<a href='javascript:void(0);' class='btn_style03' onclick='fnActCreate()'>등록</a></c:if>
			</div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer.jsp" flush="true"><jsp:param name="qk" value="F"/></jsp:include>
	<!-- //footer -->
</div>
</form>
</body>
</html>