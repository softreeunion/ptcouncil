<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<script>
		$(document).ready(function(){
			$(".menu6").removeClass("current");
			$(".menu6").addClass("current_on");
			$(".depth2 > li:nth-child(4)").addClass("current_on");
		});
	</script>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<!-- 메인메뉴 end -->
	<div id="subContent">
		<!-- submenu -->
		<jsp:include page="/WEB-INF/views/coun/base/base_left_01.jsp" flush="true"/>
		<!-- sbumenu end -->

		<h3 class="skip">본문내용 he</h3>
		<div id="content">
			<div id="subTitle">
				<h3>주민조례청구</h3>
			</div>
			<div id="sub_default">

				<h4>주민조례청구 제도란? </h4>
				<ul class="list_bul2">
					<li>주민이 직접 조례 제정·개정·폐지를 청구할 수 있는 제도</li>
				</ul>




				<h4>법적근거 </h4>

				<ul class="list_bul2">
					<li>「지방자치법」 제19조</li>
					<li>「주민조례발안에 관한 법률」 및 「평택시의회 주민조례발안에 관한 조례」</li>
				</ul>

				<h4>주민조례청구 요건 </h4>
				<p class="dot1">청구권자</p>
				<div class="round_txt">18세 이상의 주민으로서 다음 각 호에 해당하는 사람
					(「공직선거법」 제18조에 따른 선거권이 없는 사람은 제외한다.)
					<ul class="dot">
						<li>1. 평택시에 주민등록이 되어 있는 사람</li>
						<li>2. 「출입국관리법」 제10조에 따른 영주(永住)할 수 있는 체류자격 취득일 후 3년이 지난 외국인으로서 같은 법 제34조에 따라 평택시의 외국인등록대장에 올라 있는 사람</li>
					</ul>
				</div>
				<p class="dot1">청구권자 총수</p>
				<div class="round_txt">평택시장은 매년 1월 10일까지 청구권자 총수를 공표<br>

					<strong>※ 2024년: 498,561명(2024. 1. 10. 공표)</strong>
				</div>
				<p class="dot1">주민조례청구를 위한 평택시 최소 연서 주민 수</p>
				<div class="round_txt">매년 1월 10일까지 공표된 청구권자 총수의 100분의 1 이상<br>

					<strong>※ 2024년: 4,986명</strong>
				</div>
				<p class="dot1">주민조례청구 대상</p>
				<div class="round_txt">지방자치법 제28조에 따른 조례로 규정할 수 있는 사항
					(조례 제정 외에 기존 조례의 개정 및 폐지 청구도 가능) <br>
					<br>
					※ 청구 제외 대상(주민조례발안에 관한 법률 제4조)
					<ul class="dot">
						<li> 법령을 위반하는 사항</li>
						<li> 지방세ㆍ사용료ㆍ수수료ㆍ부담금을 부과ㆍ징수 또는 감면하는 사항</li>
						<li> 행정기구를 설치하거나 변경하는 사항</li>
						<li> 공공시설의 설치를 반대하는 사항</li>
					</ul>
				</div>
				<h4>주민조례청구 절차 흐름도 </h4>
				<div id="propOrdinance">
					<ul class="step">
						<li><span class="cont">청구서&middot;조례안 제출<br>(대표자증명서 발급신청)<em>대표자 -&gt;평택시의회의장</em></span></li>
						<li><span class="cont">대표자증명서발급 및 공표<br>(평택시의회의장)<em>[별지 제 3호서식]</em></span></li>
						<li><span class="cont">서명요청<br>(대표자 등)<em>기간:3개월이내</em></span></li>

						<li><span class="cont">청구인명부제출<br>서명요청기간 경과후 5일이내<em>[별지 제 7호서식],[별지 제 8호서식]&nbsp; </em></span></li>
						<li><span class="cont">청구인명부 공표<br>제출일터부터 5일이내<em></em></span></li>
						<li><span class="cont">청구인명부열람 및 이의신청<br>공표일로부터 10일간<em>[별지 제 9호 서식]</em></span></li>

						<li><span class="cont">서명 우&middot;무확인 및<br>
  이의신청 심사샆&middot;결정<em>14일 이내</em></span></li>
						<li><span class="cont">(필요시)<br>보정기간<em>10일/1회</em></span></li>
						<li><span class="cont">수리&middot;각하<br>결정 및 통지<em>평택시의회장 -&gt; 대표자 (3개월이내)</em></span></li>

						<li><span class="cont">의안발의<br>수리 후 30일이내 <em>평택시 의회의장 명의로 발의</em></span></li>
						<li><span class="cont">심의&middot;의결<br>발의 후 1년 이내<em>(본회의 의결로 1년 연장 가능)</em></span></li>
						<li><span class="cont">이송 및 공포<br>본회의 의결후 <em>평택시장에게 이송</em></span></li>


					</ul>

					<div class="tit">
						※ 온라인으로 청구 가능(주민e 직접 (<a href="https://www.juminegov.go.kr" target="_blank">www.juminegov.go.kr</a>)<br>
						※ 대표자가 2인 이상인 경우 <span class="pink">[별지 제 2호 서식]</span> 추가로 제출<br>
						※ 서명요청권 위임할 경우 즉시 신고 <span class="pink">[별지 제 5호서식]</span> -> 위임신고증 발급 <span class="pink">[별지 제 6호서식]</span>
					</div>
					<div class="tit">
						※ 청구인명부 서명 또는 날인: 자필료 성명을 적거나 손도장 또는 도장을 찍음
					</div>
					<div class="tit">
						※ 서명 유&middot;무효 확인 및 이의신청 심사&middot;결정에 따른 유효서명수가 최소 연서 수 미달시 보정 요청<br>
						※ 철회: 수리&middot;각하 결정 전 까지 대표자 전원이 <span class="pink">[별지 제 10호 서식]</span> 제출
					</div>
					<div class="tit">
						※ 입법예고 미실시
					</div>

				</div>






				<h4>주민조례청구 방법</h4>
				<ul class="dot">
					<li>온라인 또는 방문 접수(문의: 평택시의회 의사팀 ☎031-8024-7571)</li>
					<li>온라인: 주민e직접 플랫폼(<a href="https://www.juminegov.go.kr" target="_blank">https://www.juminegov.go.kr</a>) 신청</li>
					<li>방문접수: 경기도 평택시 경기대로 1366 평택시의회 의사팀(우17730)</li>
				</ul>

				<h4>평택시의회 주민조례발안에 관한 조례」</h4>
				<ul class="dot">
					<li><a href="/download.do?fileName=[별지 제1호서식] 주민조례청구서.hwp">[별지 제1호서식] 주민조례청구서</a></li>
					<li><a href="/download.do?fileName=[별지 제2호서식] 공동대표자용 청구·철회·선정대표자 지정 서식.hwp">[별지 제2호서식] 공동대표자용 청구·철회·선정대표자 지정 서식</a></li>
					<li><a href="/download.do?fileName=[별지 제3호서식] 주민조례청구 대표자증명서.hwp">[별지 제3호서식] 주민조례청구 대표자증명서</a></li>
					<li><a href="/download.do?fileName=[별지 제4호서식] 주민조례청구 대표자 변경 신청서.hwp">[별지 제4호서식] 주민조례청구 대표자 변경 신청서</a></li>
					<li><a href="/download.do?fileName=[별지 제5호서식] 주민조례청구 서명요청권 위임신고서.hwp">[별지 제5호서식] 주민조례청구 서명요청권 위임신고서</a></li>
					<li><a href="/download.do?fileName=[별지 제6호서식] 주민조례청구 서명요청권 위임신고증.hwp">[별지 제6호서식] 주민조례청구 서명요청권 위임신고증</a></li>
					<li><a href="/download.do?fileName=[별지 제7호서식] 청구인명부.hwp">[별지 제7호서식] 청구인명부</a></li>
					<li><a href="/download.do?fileName=[별지 제8호서식] 청구인명부의 표지.hwp">[별지 제8호서식] 청구인명부의 표지</a></li>
					<li><a href="/download.do?fileName=[별지 제9호서식] 이의신청서.hwp">[별지 제9호서식] 이의신청서</a></li>
					<li><a href="/download.do?fileName=[별지 제10호서식] 주민조례청구 철회 신청서.hwp">[별지 제10호서식] 주민조례청구 철회 신청서</a></li>
				</ul>
			</div>

		</div>
	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
