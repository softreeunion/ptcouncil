<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>


	<script>
		$(document).ready(function(){
			$(".menu6").removeClass("current");
			$(".menu6").addClass("current_on");
			$(".depth2 > li:nth-child(3)").addClass("current_on");
			$("[id^='creOpen']").change(function(){$("#creHide").val($("[id^='creOpen']").is(":checked")?"Y":"");});
			$(".table_view").removeHighlight().highlight($("#condValue").val());
		});
		var fnActReturn     = function(seq){
			doActLoadShow();
			$("#frmDefault").attr({"action":"notice"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self"}).submit();
		};
		var fnActDetail     = function(view){
			doActLoadShow();
			$("#viewNo").val(view);
			$("#frmDefault").attr({"action":"noticeView.do","method":"post","target":"_self"}).submit();
		};<c:choose><c:when test="${ empty authInfo}">
		var fnActAuthto     = function(){
			$("#reUseStat").val("T");
			$("#frmDefault").attr({"action":"noticeView.do","method":"post","target":"_self"}).submit();
		};</c:when><c:otherwise>
		var fnActUpdate     = function(){
			$("#frmDefault").attr({"action":"noticeModify.do","method":"post","target":"_self"}).ajaxSubmit({
				dataType      : "json",
				cache         : false ,
				beforeSubmit  : function(data,form,options){doActLoadShow();},
				success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){fnActCloseIN();doActLoadHide();}else{if(data.isError=="C"){if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}}else{fnActReturn("seq");}}},
				error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
			});
		};</c:otherwise></c:choose><c:if test="${'Q' eq dataView.ansMode and '0' eq dataView.ansCount}">
		var fnActShower     = function(view){
			$("#pass_area").hide();
			$("#pass_area").append("<div class='inner'/>");
			$("#pass_area").children().append("\n<p>비밀번호: <input type='password' id='rePass' name='rePass'/></p>");
			$("#pass_area").children().append("\n<a href='javascript:void(0);' onclick='fnAct"+view+"N();'>확인</a>");
			$("#pass_area").children().append("\n<a href='javascript:void(0);' onclick='fnActCloseIN();'>닫기</a>");
			$("#pass_area").show();
			$("#rePass").focus();
		};
		var fnActModifyY    = function(){
			fnActShower("Modify");
		};
		var fnActModifyN    = function(){
			if($("#pass_area").is(":visible")){if(typeof($("#rePass")) != "undefined" && $.trim($("#rePass").val())==""){
				alert("비밀번호를 입력해주세요.");
				$("#rePass").focus();
				return false;
			}};
			fnActModify();
		};
		var fnActModify     = function(){
			alert("행정사무감사 시민제보 기간이 아닙니다.");
			return;
			//doActLoadShow();
			//$("#frmDefault").attr({"action":"noticeEdit.do","method":"post","target":"_self"}).submit();
		};
		var fnActDeleteY    = function(){
			fnActShower("Delete");
		};
		var fnActDeleteN    = function(){
			if($("#pass_area").is(":visible")){if(typeof($("#rePass")) != "undefined" && $.trim($("#rePass").val())==""){
				alert("비밀번호를 입력해주세요.");
				$("#rePass").focus();
				return false;
			}};
			fnActDelete();
		};
		var fnActDelete     = function(){
			if(confirm("<%= alertMsg[4].replaceAll("[@]", "현재") %>")==0) return false;
			$("#frmDefault").attr({"action":"noticeDelete.do","method":"post","target":"_self"}).ajaxSubmit({
				dataType      : "json",
				cache         : false,
				beforeSubmit  : function(data,form,options){doActLoadShow();},
				success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){fnActCloseIN();doActLoadHide();}else{if(data.isError=="C"){if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}}else{fnActReturn("seq");}}},
				error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
			});
		};</c:if>
		var fnActCloseIN    = function(){$("#pass_area").hide();$("#pass_area").empty();};
	</script>
</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<!-- 메인메뉴 end -->
	<div id="subContent">
		<!-- submenu -->
		<!-- 왼쪽메뉴 -->
		<jsp:include page="/WEB-INF/views/coun/base/base_left_01.jsp" flush="true"/>
		<!-- sbumenu end -->


		<h3 class="skip">본문내용 he</h3>
		<div id="content">

			<div id="subTitle">
				<!-- sub 제목부분 -->
				<h3>행정사무감사 시민제보</h3>
			</div>
			<div id="sub_default">

				<!-- program 들어갈부분 -->
				<form id='frmDefault' name='frmDefault'>
					<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
					<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
					<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
					<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
					<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
					<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
					<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
					<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
					<input type='hidden' id='condDele'      name='condDele'/>
					<input type='hidden' id='reUseStat'     name='reUseStat'/>

				<table class="board_view">
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:10%;'/><col style='width:30%;'/><col style='width:10%;'/><col style='width:20%;'/><col style='width:10%;'/><col style='width:20%;'/></colgroup>
					<tbody><tr>
						<th>제목</th>
						<td colspan='5'>${dataView.bbsTitle}</td>
					</tr><tr>
						<th scope='col'>작성자</th>
						<td>${dataView.creName}</td>
						<th scope='col'>작성일</th>
						<td>${dataView.creDate}</td>
						<th scope='col'>조회수</th>
						<td>${dataView.hitCount}</td>
					</tr><tr>
						<th>첨부파일</th>
						<td colspan='5'>
							<span onclick=''>${dataView.fileLinkS}</span>
						</td>
					</tr><c:if test="${'Q' eq dataView.ansMode}"><c:if test="${!empty authInfo}"><tr>
						<th>공개/비공개<br/>여부</th>
						<td colspan='5'>
							<input type='hidden' id='creHide' name='creHide' value='${dataView.creHide}'/>
							<input type='checkbox' id='creOpen' name='creOpen'<c:if test="${'Y' eq dataView.creHide}"> checked='checked'</c:if>/> <label for='creOpen'>다른 사람이 볼 수 없는 비공개글입니다.</label>
						</td>
					</tr></c:if></c:if><tr class='h200'>
						<th>내용</th>
						<td colspan='5' class='cont'>
							<c:choose><c:when test="${'P' eq dataView.contPattern}">
								${dataView.contDescBR}
							</c:when><c:otherwise>
								${dataView.contDesc}
							</c:otherwise></c:choose>
						</td>
					</tr></tbody>
				</table>
				<div class='right'>
					<c:if test="${'Q' eq dataView.ansMode}"><c:choose>
						<c:when test="${ empty authInfo}">
							<a href='javascript:void(0);' class='btn orange' onclick='fnActAuthto()'>
							<c:if test="${'Y' ne dataView.creHide}">비</c:if>공개변경</a>
						</c:when><c:otherwise>
							<a href='javascript:void(0);' class='btn orange' onclick='fnActUpdate()'>
							<c:if test="${'Y' ne dataView.creHide}">비</c:if>공개변경</a>
					</c:otherwise></c:choose></c:if>
					<c:if test="${'Q' eq dataView.ansMode and '0' eq dataView.ansCount}">
						<a href='javascript:void(0);' class='btn orange' onclick='fnActModify${dataView.creLock}()'>수정</a>
						<a href='javascript:void(0);' class='btn orange' onclick='fnActDelete${dataView.creLock}()'>삭제</a>
					</c:if>
					<a href='javascript:void(0);' class='btn' onclick='fnActReturn("seq")'>목록</a>
				</div>
				<div class='board_view mT20'><table>
					<caption class='hide'>Previous/Next</caption>
					<colgroup><col style='width:10%;'/><col style='width:90%;'/></colgroup>
					<tbody><tr>
						<th scope='col'>이전글</th>
						<td>&nbsp;&nbsp;<c:choose><c:when test="${ empty prevView}">이전 글이 존재 하지 않습니다.</c:when><c:otherwise>${prevView.gabSpace}<a href='javascript:void(0);' onclick='fnActDetail("${prevView.viewNo}")'><c:if test="${'Y' eq prevView.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${prevView.bbsTitle}</a></c:otherwise></c:choose></td>
					</tr><tr>
						<th scope='col'>다음글</th>
						<td>&nbsp;&nbsp;<c:choose><c:when test="${ empty nextView}">다음 글이 존재 하지 않습니다.</c:when><c:otherwise>${nextView.gabSpace}<a href='javascript:void(0);' onclick='fnActDetail("${nextView.viewNo}")'><c:if test="${'Y' eq nextView.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${nextView.bbsTitle}</a></c:otherwise></c:choose></td>
					</tr></tbody>
				</table>

				</div>
				</form>
				<!--  program 들어갈부분 end -->
			</div>



		</div>
	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
