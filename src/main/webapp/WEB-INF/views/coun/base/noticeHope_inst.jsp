<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />

	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<script>
		$(document).ready(function(){
			$(".menu6").removeClass("current");
			$(".menu6").addClass("current_on");
			$(".depth2 > li:nth-child(2)").addClass("current_on");
			$("[id^='creOpen']").change(function(){$("#creHide").val($("[id^='creOpen']").is(":checked")?"Y":"");});
		});
		var fnActReturn     = function(seq){
			doActLoadShow();
			$("#frmDefault").attr({"action":"notice"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self","enctype":""}).submit();
		};<c:if test="${!empty authInfo}">
		var fnActUpdate     = function(){
			if($.trim($("#bbsTitle").val())==""){alert("제목은 필수입력입니다.");$("#bbsTitle").focus();return false;}
			if($.trim($("#contDesc").val())==""){alert("작성된 내용이 없습니다. 내용을 입력하세요.");$("#contDesc").focus();return false;}
			$("#frmDefault").attr({"action":"noticeCreate.do","method":"post","target":"_self","enctype":"multipart/form-data"}).ajaxSubmit({
				dataType      : "json",
				cache         : false ,
				contentType   : false ,
				processData   : false ,
				beforeSubmit  : function(data,form,options){doActLoadShow();},
				beforeSend    : function(){$(".progress").show();$(".progress-bar").width("0%");},
				uploadProgress: function(event,position,total,percentComplete){$(".progress-bar").width(percentComplete+"%");if(percentComplete==100){$(".progress").hide();};},
				success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn("seq");}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
				error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
			});
		};</c:if>
	</script>


</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<!-- 메인메뉴 end -->
	<div id="subContent">
		<!-- submenu -->
		<!-- 왼쪽메뉴 -->
		<jsp:include page="/WEB-INF/views/coun/conf/conf_left.jsp" flush="true"/>
		<!-- sbumenu end -->


		<h3 class="skip">본문내용 he</h3>
		<div id="content">

			<div id="subTitle">
				<!-- sub 제목부분 -->
				<h3>의회에 바란다&nbsp;</h3>
			</div>
			<div id="sub_default">

				<!-- program 들어갈부분 -->
				<form id='frmDefault' name='frmDefault'>
					<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
					<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
					<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
					<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
					<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
					<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
					<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
					<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
					<input type='hidden' id='condDele'      name='condDele'/>

					<table class="board_write">
						<caption class='hide'>Board View</caption>
						<colgroup><col style='width:12%;'/><col style='width:88%;'/></colgroup>
						<tbody><tr>
							<th scope='col'><label for='bbsTitle'>제목</label> *</th>
							<td><input type='text' id='bbsTitle' name='bbsTitle' class='w90_p' maxlength='400'/></td>
						</tr><tr>
							<th><label for='creHide'>공개/비공개<br/>여부</label></th>
							<td>
								<input type='hidden' id='creHide' name='creHide' value='${dataView.creHide}'/>
								<input type='checkbox' id='creOpen' name='creOpen'/> <label for='creOpen'>다른 사람이 볼 수 없는 비공개글입니다.</label><%--
						<input type='radio' id='creOpen1' name='creOpen' value='Y'/> <label for='creOpen1'>다른 사람이 <font color='red'>볼 수 없는 비공개글</font>입니다.</label><br/>
						<input type='radio' id='creOpen2' name='creOpen' value='N'/> <label for='creOpen2'>다른 사람이 <font color='blue'>볼 수 있는 공개글</font>입니다.</label>--%>
							</td>
						</tr><tr>
							<th><label for='read1'>작성자</label> *</th>
							<td><input type='text' id='read1' class='w200' maxlength='30' value='${authInfo.certName}' readonly='readonly'/></td>
						</tr><tr>
							<th><label for='contDesc'>내용</label> *</th>
							<td><textarea id='contDesc' name='contDesc' rows='16' class='w90_p'></textarea></td>
						</tr><c:forEach var="file" varStatus="status" begin="0" end="2"><tr>
							<th><label for='fileBase${status.index+1}'>첨부파일</label></th>
							<td id='file_base${status.index+1}'><label for='fileBase${status.index+1}' class='hide'>첨부파일</label><input type='file' id='fileBase${status.index+1}' name='fileBase${status.index+1}' class='w90_p'/></td>
						</tr></c:forEach></tbody>
					</table>
					<div class='file'><span class='td_blu'>※ 첨부파일 용량 제한 <font color='red'>10MB 이하</font></span></div>
					<div class='file'><span class='td_blu'><%= allowFiles[3].replaceAll("info01 light_grey'>", "sention_txt td_blu'>※") %></span></div>
					<div class='center pT20'>
						<c:if test="${!empty authInfo}">
							<a href='javascript:void(0);' class='btn orange' onclick='fnActUpdate()'>저장</a>
						</c:if>
						<a href='javascript:void(0);' class='btn' onclick='fnActReturn("seq")'>목록</a>
					</div>

				</form>
				<!--  program 들어갈부분 end -->

			</div>



		</div>
	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
