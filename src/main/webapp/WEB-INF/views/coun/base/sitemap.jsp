<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
	<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<script>
		$(document).ready(function(){
			$(".menu7").removeClass("current");
			$(".menu7").addClass("current_on");
		});
	</script>
</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
    <!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->

	
	<!-- 메인메뉴 end -->
	<div id="subContent">
	<!-- submenu -->
	<jsp:include page="/WEB-INF/views/coun/base/base_left_02.jsp" flush="true"/>
	<!-- sbumenu end -->

	<h3 class="skip">본문내용 he</h3>
	   <div id="content">
		<div id="subTitle">
			<h3>사이트맵</h3>
		</div>
		   <div id="sub_default">
			   <div id="sitemapArea">
				   
				 <div class="site_list">
					 <h4> <a href="/coun/intro/greetings.do" title="의회안내">의회안내</a></h4>
	   	     <ul  >
					<li> <a href="/coun/intro/greetings.do" title="의장인사말">의장인사말</a> </li>
					<li> <a href="/coun/intro/history.do" title="의회연혁">의회연혁</a> </li>
					<li> <a href="/coun/intro/organization.do" title="조직 및 구성">조직 및 구성</a>
						<ul class="dot">
							<li> <a href="/coun/intro/organization.do" title="조직 및 구성">조직 및 구성</a> </li>
							<li> <a href="/coun/intro/phone/PageView.do" title="전화번호">전화번호</a> </li>
						</ul>
					</li>
					<li> <a href="/coun/intro/function.do" title="의회기능">의회기능</a>
						<ul class="dot">
							<li> <a href="/coun/intro/function.do" title="의회기능">의회기능</a> </li>
							<li> <a href="/coun/intro/authority.do" title="의회권한">의회권한</a> </li>
							<li> <a href="/coun/intro/convoke.do" title="회기소집 및 운영">회기소집 및 운영</a> </li>
							<li> <a href="/coun/intro/process.do" title="의안 처리절차">의안처리절차</a> </li>
							<li> <a href="/coun/intro/executive.do" title="행정사무감사/조사">행정사무감사/조사</a> </li>
							<li> <a href="/coun/intro/estimate.do" title="예산 및 결산">예산 및 결산</a> </li>
							<li> <a href="/coun/intro/petition.do" title="청원/진정 처리절차">청원/진정 처리절차</a> </li>
						</ul>
					</li>
					<li> <a href="/coun/intro/operate.do" title="의회운영">회운영</a> </li>
					<li> <a href="/coun/intro/watch.do" title="방청안내">방청안내</a>
						<ul class="dot">
							<li> <a href="/coun/intro/watch.do" title="방청안내">방청안내</a> </li>
							<li> <a href="/coun/intro/visitRequ.do" title="온라인신청">온라인신청</a> </li>
							<li> <a href="/coun/intro/visitList.do" title="신청확인">신청확인</a> </li>
						</ul>
					</li>
					<li> <a href="/coun/intro/location.do" title="오시는길">오시는길</a> </li>
				</ul>
			</div>
		
				
		 
			<div class="site_list">
				<h4><a href="/coun/memb/activeName.do" title="의원소개">의원소개</a></h4>
				
				<ul >
					<li> <a href="/coun/memb/activeName.do" title="현역의원">현역의원</a> </li>
					<li> <a href="/coun/memb/activeChair.do" title="의장단·상임위원장">의장단·상임위원장</a> </li>
					<li> <a href="/coun/memb/activeFormer.do" title="역대의원">역대의원</a> </li>
					<li> <a href="/coun/memb/activeFinder.do" title="의원검색">의원검색</a> </li>
					<li> <a href="/coun/memb/ethics.do" title="의원윤리강령">의원윤리강령</a> </li>
				</ul>
			</div>
		
		
			<div class="site_list">
				<h4><a href="/coun/acts/stand.do" title="의정활동">의정활동</a></h4>
				<ul >
					<li> <a href="/coun/acts/stand.do" title="상임위원회">상임위원회</a> </li>
					<li> <a href="/coun/base/photoList.do" title="포토 의정활동">포토 의정활동</a> </li>
					<li> <a href="/coun/base/movieList.do" title="홍보영상">홍보영상</a> </li>
					<li> <a href="/coun/cost/reportList.do" title="업무추진비 공개">업무추진비 공개</a> </li>
					<li> <a href="/coun/brief/reportList.do" title="국내외 연수보고서">국내외 연수보고서</a> </li>
					<li> <a href="/coun/resr/reportList.do" title="의원 연구단체">의원 연구단체</a> </li>
					<li> <a href="/coun/schd/monthList.do" title="의사일정">의사일정</a>
						<ul  class="dot">
							<li> <a href="/coun/schd/monthList.do" title="월간일정">월간일정</a> </li>
							<li> <a href="/coun/acts/program.do" title="연간회기일정">연간회기일정</a> </li>
						</ul>
					</li>
					<li> <a href="/coun/hold/reportList.do" title="의원겸직현황">의원겸직현황</a> </li>
				</ul>
			</div>
		
		
			<div class="site_list">
				<h4><a href="/coun/cast/live.do" title="인터넷방송" target='_self'>인터넷방송</a></h4>
				<ul  >
					<li> <a href="/coun/cast/live.do" title="생방송">생방송</a> </li>
					<li> <a href="/coun/stand/movieList.do?ct=1" title="다시보기">다시보기</a>
						<ul class="dot">
							<li> <a href="/coun/stand/movieList.do?ct=1" title="본회의">본회의</a> </li>
							<li> <a href="/coun/stand/movieList.do?ct=2" title="의회운영위원회">의회운영위원회</a> </li>
							<li> <a href="/coun/stand/movieList.do?ct=3" title="기획행정위원회">기획행정위원회</a> </li>
							<li> <a href="/coun/stand/movieList.do?ct=4" title="복지환경위원회">복지환경위원회</a> </li>
							<li> <a href="/coun/stand/movieList.do?ct=5" title="산업건설위원회">산업건설위원회</a> </li>
							<li> <a href="/coun/stand/movieList.do?ct=6" title="특별위원회">특별위원회</a> </li>
						</ul>
					</li>
				</ul>
			</div>
		
		 
			<div class="site_list">
				<h4><a href="/coun/past/minutesList.do" title="회의록검색" target='_self'>회의록검색</a></h4>
				<ul >
					<li> <a href="/coun/past/minutesList.do" title="최근 회의록" target='_self'>최근 회의록</a> </li>
					<li> <a href="/coun/sess/minutesList.do" title="단순검색" target='_self'>단순검색</a>
						<ul  class="dot">
							<li> <a href="/coun/sess/minutesList.do" title="회기별검색">회기별검색</a> </li>
							<li> <a href="/coun/meet/minutesList.do" title="회의별검색">회의별검색</a> </li>
							<li> <a href="/coun/year/minutesList.do" title="연도별검색">연도별검색</a> </li>
						</ul>
					</li>
					<li> <a href="/coun/tail/minutesList.do" title="상세검색" target='_self'>상세검색</a> </li>
					<li> <a href="/coun/gover/reportList.do" title="시정질문답변" target='_self'>시정질문답변</a> </li>
					<li> <a href="/coun/spch/reportList.do" title="7분자유발언" target='_self'>7분자유발언</a> </li>
					<li> <a href="/coun/find/agendaList.do" title="의안정보" target='_self'>의안정보</a>
						<ul  class="dot">
							<li> <a href="/coun/find/agendaList.do'" title="의안검색">의안검색</a> </li>
							<li> <a href="/coun/info/agendaList.do" title="의안정보">의안정보</a> </li>
							<li> <a href="/coun/item/agendaList.do" title="의안항목">의안항목</a> </li>
							<li> <a href="/coun/kind/agendaStat.do" title="의안통계">의안통계</a> </li>
						</ul>
					</li>
					<li> <a href="/coun/days/minutesStat.do" title="회의통계" target='_self'>회의통계</a> </li>
					<li> <a href="/coun/cast/general.do?ct=1" title="영상회의록" target='_self'>영상회의록</a>
						<ul  class="dot">
							<li> <a href="/coun/cast/general.do?ct=1" title="본회의">본회의</a> </li>
							<li> <a href="/coun/cast/general.do?ct=2" title="의회운영위원회">의회운영위원회</a> </li>
							<li> <a href="/coun/cast/general.do?ct=3" title="기획행정위원회">기획행정위원회</a> </li>
							<li> <a href="/coun/cast/general.do?ct=4" title="복지환경위원회">복지환경위원회</a> </li>
							<li> <a href="/coun/cast/general.do?ct=5" title="산업건설위원회">산업건설위원회</a> </li>
							<li> <a href="/coun/cast/general.do?ct=6" title="예산결산특별위원회">예산결산특별위원회</a> </li>
						</ul>
					</li>
				</ul>
			</div>
		
		
			<div class="site_list">
				<h4> <a href="/coun/base/claim.do" title="의안">열린마당</a></h4>
				<ul  >
					<li> <a href="/coun/base/claim.do" title="민원안내">민원안내</a> </li>
					<li> <a href="/coun/hope/noticeList.do" title="의회에 바란다">의회에 바란다</a> </li>
					<li> <a href="/coun/tip/noticeList.do" title="행정사무감사 시민제보">행정사무감사 시민제보</a> </li>
					<li> <a href="/coun/base/propOrdinance.do" title="주민조례청구">주민조례청구</a> </li>
					<li> <a href="/coun/base/search.do" title="통합검색">통합검색</a> </li>
				</ul>
			</div>
		
		
			<div class="site_list">
				<h4><a href="/coun/base/noticeList.do" title="알림마당">알림마당</a></h4>
				<ul >
					<li> <a href="/coun/base/noticeList.do" title="공지사항">공지사항</a> </li>
					<li> <a href="/coun/laws/reportList.do" title="입법예고">입법예고</a> </li>
					<li> <a href="/coun/news/noticeList.do" title="의회소식지">의회소식지</a>
						<ul class="dot">
							<li> <a href="/coun/news/noticeList.do" title="의회소식지">의회소식지</a> </li>
							<li> <a href="/coun/news/noticeInst.do" title="의회소식지 신청">의회소식지 신청</a> </li>

						</ul>
					</li>
					<li> <a href="/coun/press/reportList.do" title="보도자료">보도자료</a> </li>
					<li> <a href="/coun/interview/reportList.do" title="의원인터뷰">의원인터뷰</a> </li>
					<li> <a href="//www.elis.go.kr/newlaib/laibLaws/h1126/laws.jsp?regionId=41220" target="_blank" title="자치법규">자치법규</a> </li>
					<li> <a href="/webdata/index.do" target="_blank" title="자료실">자료실</a> </li>
					<li> <a href="/coun/base/counTour.do" title="청사 견학">청사 견학</a> </li>
					</ul>
			</div>

			   </div>
	     </div>
		   
		   
			</div>
			
         </div>
	 </div>
	</div>
	<!-- footer -->
	<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
	<!-- //footer -->
		
		
	</div>
	
	

</body>
</html>
