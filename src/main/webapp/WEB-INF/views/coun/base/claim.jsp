<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<script>
		$(document).ready(function(){
			$(".menu6").removeClass("current");
			$(".menu6").addClass("current_on");
			$(".depth2 > li:nth-child(1)").addClass("current_on");
		});
	</script>


</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<!-- 메인메뉴 end -->
	<div id="subContent">
		<!-- submenu -->
		<jsp:include page="/WEB-INF/views/coun/base/base_left_01.jsp" flush="true"/>
		<!-- sbumenu end -->

		<h3 class="skip">본문내용 he</h3>
		<div id="content">
			<div id="subTitle">
				<h3>민원안내</h3>
			</div>
			<div id="sub_default">

				<h4>청원 </h4>
				<div class='list_double'><ul>
					<li>청원대상
						<ul>
							<li>불법·부당한 권한 행사로 주민의 권리나 이익이 침해된 경우</li>
							<li>공무원의 비위 시정 요구</li>
							<li>조례·규칙의 제정·개정 또는 폐지에 대한 의견</li>
							<li>기타 지방자치단체의 권한에 속하는 사항</li>
						</ul>
					</li>
				</ul>
				</div>

				<div class='list_double'><ul>
					<li>청원 불수리 사항
						<ul>
							<li>재판에 간섭하는 내용</li>
							<li>국가기관을 모독하는 내용</li>
							<li>동일 기관에 2개 이상 또는 2개 이상의 기관에 제출된 것</li>
							<li>법령에 위배되는 것</li>
						</ul>
					</li>
				</ul>
				</div>

				<div class='list_double'><ul>
					<li>제출 방법
						<ul>
							<li>청원인의 성명, 주소를 기재하고 서명날인한 문서로 제출(우편 또는 방문)</li>
							<li>1인 이상의 소개 의원의 소개 의견서를 첨부</li>
							<li>다수인 공동 청원의 경우 대표자를 선임하여 제출</li>
						</ul>
					</li>
				</ul>
				</div>
				<div class='list_double'><ul>
					<li>처리 과정
						<ul>
							<li>접수 ⇒ 소관 위원회(또는 본회의) 회부 ⇒ 심사 ⇒ 처리 결과를 의장에게 보고 ⇒ 청원인에게 통지</li>
							<li>조례안 등과 같이 일반의안에 준하여 처리되며 시장이 처리함이 타당하다고 인정될 경우 의견서를 첨부하여 시장에게 이송</li>
						</ul>
					</li>
				</ul>
				</div>

				<h4>진정 </h4>

				<div class='list_double'><ul>
					<li>진정 대상
						<ul>
							<li>진정·건의·탄원·호소·문의 등</li>
							<li>불법·부당한 권한 행사로 주민의 권리나 이익이 침해된 경우</li>
							<li>공무원의 비위 시정 요구</li>
							<li>조례·규칙의 제정·개정 또는 폐지에 대한 의견</li>
							<li>기타 지방자치단체의 권한에 속하는 사항</li>

						</ul>
					</li>
				</ul>
				</div>

				<div class='list_double'><ul>
					<li>진정 불수리 사항
						<ul>
							<li>재판에 간섭하는 내용</li>
							<li>국가 원수를 모독하는 내용</li>
							<li>동일 기관에 2개 이상 또는 2개 이상의 기관에 제출된 것</li>
							<li>진정인의 주소, 성명 및 진정서의 내용이 분명하지 아니한 것</li>

						</ul>
					</li>
				</ul>
				</div>

				<div class='list_double'><ul>
					<li>제출 방법
						<ul>
							<li>전달하고자 하는 내용이 정확하게 파악될 수 있도록 작성하여 제출(우편 또는 방문)</li>
							<li>진정인의 성명, 주소를 기재하고 서명날인한 문서로 제출(우편 또는 방문)</li>
							<li>다수인의 공동 진정의 경우 대표자를 선임하여 제출</li>
						</ul>
					</li>
				</ul>
				</div>

				<div class='list_double'><ul>
					<li>처리 과정
						<ul>
							<li>의회 자체 처리</li>
							<li>시청 이송 처리</li>

						</ul>
					</li>
				</ul>
				</div>

				<div class="round_txt">
					<ul class="dot">
						<li>접  수  처 : 평택시의회 (우 17730) 경기도 평택시 경기대로 1366(서정동)
						</li>
						<li>문의 전화 : 의사팀(031-8024-7570)</li>
					</ul>
				</div>

			</div>

		</div>
	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
