<%
////////////////////////////////////////////////////////////
// Program ID  : noticeArch_inst
// Description : 열린마당 > 의회자료실 [등록화면]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "의회자료실";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb06_03").addClass("on").removeClass("noChild");
		$("#bbsTitle").focus();
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"notice"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self","enctype":""}).submit();
	};<c:if test="${!empty MemUInfo}">
	var fnActUpdate     = function(){
		if($.trim($("#bbsTitle").val())==""){alert("제목은 필수입력입니다.");$("#bbsTitle").focus();return false;}
		if($.trim($("#contDesc").val())==""){alert("작성된 내용이 없습니다. 내용을 입력하세요.");$("#contDesc").focus();return false;}
		$("#frmDefault").attr({"action":"noticeCreate.do","method":"post","target":"_self","enctype":"multipart/form-data"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			contentType   : false , 
			processData   : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			beforeSend    : function(){$(".progress").show();$(".progress-bar").width("0%");},
			uploadProgress: function(event,position,total,percentComplete){$(".progress-bar").width(percentComplete+"%");if(percentComplete==100){$(".progress").hide();};},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn("seq");}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/coun/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/coun/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="6"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 열린마당 &gt; <%= subTitle %></div><c:if test="${!empty MemUInfo}">
			<div class='board_log_btn'><a href='<%= webBase %>/coun/hcout_proc.do' class='td_none'>로그아웃</a></div></c:if>
			<div class='table_write'><table>
				<caption class='hide'>Board View</caption>
				<colgroup><col style='width:12%;'/><col style='width:88%;'/></colgroup>
				<tbody><tr>
					<th scope='col'>제목 *</th>
					<td><label for='bbsTitle' class='hide'>제목</label><input type='text' id='bbsTitle' name='bbsTitle' class='w90_p' maxlength='400'/></td>
				</tr><tr>
					<th>작성자 *</th>
					<td><label for='read1' class='hide'>작성자</label><input type='text' id='read1' class='w200' maxlength='30' value='${MemUInfo.userName}' readonly='readonly'/></td>
				</tr><tr>
					<th>내용 *</th>
					<td><label for='contDesc' class='hide'>내용</label><textarea id='contDesc' name='contDesc' rows='16' class='w90_p'></textarea></td>
				</tr><tr>
					<th>첨부파일</th>
					<td id='file_base1'><label for='fileBase1' class='hide'>첨부파일</label><input type='file' id='fileBase1' name='fileBase1' class='w90_p'/></td>
				</tr></tbody>
			</table></div>
			<div class='search_area'><input type='button' id='fileAdd01' class='w70 pnt' value='파일추가'/> &nbsp; <%= allowFiles[3].replaceAll("info01 light_grey'>", "sention_txt td_blu'>※") %></div>
			<div class='btn_group center'><c:if test="${!empty MemUInfo}">
				<a href='javascript:void(0);' class='btn_style03' onclick='fnActUpdate()'>저장</a></c:if>
				<a href='javascript:void(0);' class='btn_style03' onclick='fnActReturn("seq")'>목록</a>
			</div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer.jsp" flush="true"><jsp:param name="qk" value="F"/></jsp:include>
	<!-- //footer -->
</div>
</form>
</body>
</html>