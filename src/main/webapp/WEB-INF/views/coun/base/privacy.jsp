<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<script>
		$(document).ready(function(){
			$(".menu7").removeClass("current");
			$(".menu7").addClass("current_on");
		});
	</script>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<!-- 메인메뉴 end -->
	<div id="subContent">
		<!-- submenu -->
		<jsp:include page="/WEB-INF/views/coun/base/base_left_02.jsp" flush="true"/>
		<!-- sbumenu end -->

		<h3 class="skip">본문내용 he</h3>
		<div id="content">
			<div id="subTitle">
				<h3>개인정보 처리방침</h3>
			</div>
			<div id="sub_default">
				<h4>제1조(개인정보의 처리목적)</h4>


				<p class="round_txt">평택시의회는 개인정보 보호법 제30조에 따라 정보주체의 개인정보를 보호하고 이와 관련한 고충을 신속하고 원활하게 처리할 수 있도록 하기 위하여 다음과 같이 개인정보 처리지침을 수립·공개합니다.</p>

				<ul class="list_double">
					<li>① 평택시의회는 다음의 목적을 위하여 개인정보를 처리합니다. 처리하고 있는 개인정보는 다음의 목적 이외의 용도로는 이용되지 않으며, 이용 목적이 변경되는 경우에는 개인정보 보호법 제18조에 따라 별도의 동의를 받는 등 필요한 조치를 이행할 예정입니다.
						<ul>
							<li>1. 민원사무 처리</li>
							<li>민원인의 신원 확인, 민원사항 확인 등의 목적으로 개인정보를 처리합니다.</li>
						</ul>
					</li>
					<li>② 평택시의회가 개인정보 보호법 제32조에 따라 등록·공개하는 개인정보파일의 처리목적은 다음과 같습니다.<br/><br/></li>
					<li>
						<div class="table_wrap">
							<table class="board_n_list">
								<caption class="hide">개인정보파일의 처리목적</caption>
								<colgroup>
									<col width="5%">
									<col width="15%">
									<col width="25%">
									<col width="25%">
									<col width="20%">
								</colgroup>
								<thead>
								<tr>
									<th>순번</th>
									<th>개인정보파일의 명칭</th>
									<th>운영근거 / 처리목적</th>
									<th>개인정보파일에 기록되는<br/>개인정보의 항목</th>
									<th>보유기간</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>1</td>
									<td>의회에바란다</td>
									<td style="text-align: left; padding-left: 10px">공공기록물 관리에 관한 법령 및 민원처리에 관한 법령</td>
									<td>필수항목 : 성명, 연락처<br/>선택항목 : 이메일</td>
									<td>3년(게시글 10년)</td>
								</tr>
								</tbody>
							</table>
						</div>
					</li>
				</ul>

				<h4 >제2조(개인정보의 처리 및 보유 기간)</h4>
				<ul class="list_double">
					<li>① 평택시의회는 법령에 따른 개인정보 보유·이용기간 또는 정보주체로부터 개인정보를 수집시에 동의받은 개인정보 보유·이용기간 내에서 개인정보를 처리·보유합니다.
					</li>
					<li>② 각각의 개인정보 처리 및 보유 기간은 다음과 같습니다.
						<ul>
							<li>1. 민원사무 처리 : 민원처리 종료 후 3년(게시된 글은 10년)</li>
							<li>2. 「정보통신망 이용촉진 및 정보보호 등에 관한 법률」시행령 제29조에 따른 본인확인정보 보관 : 게시판에 정보 게시가 종료된 후 1년</li>
						</ul>
					</li>
				</ul>

				<h4 >제3조(개인정보의 제3자 제공)</h4>
				<ul class="list_double">
					<li>① 평택시의회는 정보주체의 동의, 법률의 특별한 규정 등 개인정보 보호법 제17조 및 제18조에 해당하는 경우에만 개인정보를 제3자에게 제공합니다.</li>
					<li>② 평택시의회는 다음과 같이 개인정보를 제3자에게 제공하고 있습니다.
						<ul>
							<li>개인정보를 제공받는 자 : NICE 평가정보(주)</li>
							<li>제공받는 자의 개인정보 이용목적 : 실명인증</li>
							<li>제공하는 개인정보 항목 : 성명, 성별, 생년월일, 휴대폰번호, 이동통신사</li>
							<li>제공받는 자의 보유·이용기간 : 이용자의 정보 폐기 요청시까지, 정보 폐기 후 폐기에 관한 기록을 1년간 보관</li>
						</ul>
					</li>
				</ul>

				<h4 >제4조(정보주체의 권리·의무 및 행사방법)</h4>
				<ul class="list_double">
					<li>① 정보주체는 평택시의회에 대해 언제든지 다음 각 호의 개인정보 보호 관련 권리를 행사할 수 있습니다.
						<ul>
							<li>1. 개인정보 열람요구</li>
							<li>2. 오류 등이 있을 경우 정정 요구</li>
							<li>3. 삭제요구</li>
							<li>4. 처리정지 요구</li>
						</ul>
					</li>
					<li>② 제1항에 따른 권리 행사는 평택시의회에 대해 서면, 전화, 전자우편, 모사전송(FAX) 등을 통하여 하실 수 있으며 평택시의회는 이에 대해 지체없이 조치하겠습니다.</li>
					<li>③ 정보주체가 개인정보의 오류 등에 대한 정정 또는 삭제를 요구한 경우에는 평택시의회는 정정 또는 삭제를 완료할 때까지 당해 개인정보를 이용하거나 제공하지 않습니다.</li>
					<li>④ 제1항에 따른 권리 행사는 정보주체의 법정대리인이나 위임을 받은 자 등 대리인을 통하여 하실 수 있습니다. 이 경우 개인정보 보호법 시행규칙 별지 제11호 서식에 따른 위임장을 제출하셔야 합니다.</li>
					<li>⑤ 정보주체는 개인정보 보호법 등 관계법령을 위반하여 평택시의회가 처리하고 있는 정보주체 본인이나 타인의 개인정보 및 사생활을 침해하여서는 아니됩니다.</li>
				</ul>

				<h4 >제5조(처리하는 개인정보 항목)</h4>
				<ul class="list_double">
					<li>평택시의회는 다음의 개인정보 항목을 처리하고 있습니다.
						<ul>
							<li>1. 민원사무 처리
								<ul>
									<li>필수항목 : 성명, 연락처</li>
									<li>선택항목 : 이메일</li>
								</ul>
							</li>
						</ul>
					</li>
				</ul>

				<h4>제6조(개인정보의 파기)</h4>
				<ul class="list_double">
					<li>① 평택시의회는 개인정보 보유기간의 경과, 처리목적 달성 등 개인정보가 불필요하게 되었을 때에는 지체없이 해당 개인정보를 파기합니다.</li>
					<li>② 정보주체로부터 동의받은 개인정보 보유기간이 경과하거나 처리목적이 달성되었음에도 불구하고 다른 법령에 따라 개인정보를 계속 보존하여야 하는 경우에는, 해당 개인정보(또는 개인정보파일)을 별도의 데이터베이스(DB)로 옮기거나 보관장소를 달리하여 보존합니다.</li>
					<li>③ 개인정보 파기의 절차 및 방법은 다음과 같습니다.
						<ul>
							<li>1. 파기절차
								<ul>
									<li>평택시의회는 파기하여야 하는 개인정보(또는 개인정보파일)에 대해 개인정보 파기계획을 수립하여 파기합니다. 평택시의회는 파기 사유가 발생한 개인정보(또는 개인정보파일)을 선정하고, 평택시의회는 개인정보 보호책임자의 승인을 받아 개인정보(또는 개인정보파일)를 파기합니다.</li>
								</ul>
							</li>
							<li>2. 파기방법
								<ul>
									<li>평택시의회는 전자적 파일 형태로 기록, 저장된 개인정보는 기록을 재생할 수 없도록 로우레밸포멧(Low Level Format) 등의 방법을 이용하여 파기하며, 종이 문서에 기록,저장된 개인정보는 분쇄기로 분쇄하거나 소각하여 파기합니다.</li>
								</ul>
							</li>
						</ul>
					</li>
				</ul>

				<h4>제7조(개인정보의 안전성 확보조치)</h4>
				<ul class="list_double">
					<li>① 평택시의회는 개인정보의 안전성 확보를 위해 다음과 같은 조치를 취하고 있습니다.
						<ul>
							<li>1. 관리적 조치 : 내부관리계획 수립·시행, 정기적 직원 교육 등</li>
							<li>2. 기술적 조치 : 개인정보처리시스템 등의 접근권한 관리, 접근통제시스템 설치, 고유식별정보 등의 암호화, 보안프로그램 설치</li>
							<li>3. 물리적 조치 : 전산실, 자료보관실 등의 접근통제</li>
						</ul>
					</li>
				</ul>

				<h4>제8조(개인정보 보호책임자)</h4>
				<ul class="list_double">
					<li>① 평택시의회는 개인정보 처리에 관한 업무를 총괄해서 책임지고, 개인정보 처리와 관련한 정보주체의 불만처리 및 피해구제 등을 위하여 아래와 같이 개인정보 보호책임자를 지정하고 있습니다.
						<ul>
							<li>▶ 개인정보 보호 담당부서
								<ul>
									<li>부서명 : 평택시의회 의회사무국 의정팀</li>
									<li>담당자 : </li>
									<li>전화 : 031-8024-</li>
									<li>팩스 : 031-8024-</li>
								</ul>
							</li>
						</ul>
					</li>
					<li>② 정보주체께서는 평택시의회의 서비스(또는 사업)을 이용하시면서 발생한 모든 개인정보 보호 관련 문의, 불만처리, 피해구제 등에 관한 사항을 개인정보 보호책임자 및 담당부서로 문의하실 수 있습니다. 평택시의회는 정보주체의 문의에 대해 지체없이 답변 및 처리해드릴 것입니다.</li>
				</ul>
				<h4 >제9조(권익침해 구제방법)</h4>
				<ul class="list_double">
					<li>정보주체는 아래의 기관에 대해 개인정보 침해에 대한 피해구제, 상담 등을 문의하실 수 있습니다.</li>
					<li>&#8249;아래의 기관은 평택시의회와는 별개의 기관으로서, 평택시의회의 자체적인 개인정보 불만처리, 피해구제 결과에 만족하지 못하시거나 보다 자세한 도움이 필요하시면 문의하여 주시기 바랍니다&#8250;</li>
					<li>▶ 개인정보 침해신고센터 (한국인터넷진흥원 운영)
						<ul>
							<li>홈페이지 : privacy.kisa.or.kr</li>
							<li>전화 : (국번없이) 118</li>
							<li>주소 : (138-950) 서울시 송파구 중대로 135 한국인터넷진흥원 개인정보침해신고센터</li>
						</ul>
					</li>
					<li>▶ 개인정보 분쟁조정위원회 (한국인터넷진흥원 운영)
						<ul>
							<li>소관업무 : 개인정보 분쟁조정신청, 집단분쟁조정 (민사적 해결)</li>
							<li>홈페이지 : privacy.kisa.or.kr</li>
							<li>전화 : (국번없이) 118</li>
							<li>주소 : (138-950) 서울시 송파구 중대로 135 한국인터넷진흥원 개인정보침해신고센터</li>
						</ul>
					</li>
					<li>▶ 대검찰청 사이버범죄수사단 : 02-3480-3573 (www.spo.go.kr)</li>
					<li>▶ 경찰청 사이버테러대응센터 : 1566-0112 (www.netan.go.kr)</li>
				</ul>
			</div>


		</div>

	</div>
</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
