<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<script>
		$(document).ready(function(){
			$(".menu7").removeClass("current");
			$(".menu7").addClass("current_on");
			$(".depth2 > li:nth-child(3)").addClass("current_on");
			$(".depth2 > li:nth-child(3) > ul > li:nth-child(1)").addClass("current_on");
		});
	</script>

	<style>
		/*body{margin:0;background-color:#fff;}*/
		/*td, p, div, select{font-size:12px;!*font-family:굴림;*!text-decoration:none;}*/
		/*form{display:inline;margin:0;padding:0;}*/
		/*img{vertical-align:middle;border:0;}*/

		.bookcase{display:block;position:relative;width:835px;height:auto !important;margin:0 auto;padding:0;}
		.catabox{display:block;position:relative;width:auto;height:28px;padding:0;text-align:right;}
		#thumbcont{z-index:2;display:block;position:relative;height:auto !important;top:0px !important;left:0px !important;width: 100% !important;margin-bottom:40px}
		#thumbcont p{display:inline-block;position:relative;margin:10px 1px;padding:0;top:0px !important;left:0px !important;border:1px solid #ddd}
		#thumbcont p img{display:block;width:100%;height:100%;}

		#bottomcnt{display:block;position:relative;bottom:30px;}
		#pagingsect{display:block;width:96% !important;height:34px;margin:0px;text-align:center;background-image:url('/ebook/include/black40.png');}
		#pagingsect p{display:inline-block;width:15px;height:14px;margin:0;padding:0px;line-height:14px;font-family:Tahoma;}
		#pagingsect p a{text-decoration:none;color:#fff;}

		.currpage{background-color:#666;color:#fff;}
		.nextpage{background-color:#444;color:#fff;}
		.nextpage:hover{background-color:#333;color:#fff;}

		#searchsect{display:block;float:right;margin:5px 35px 0 0;}
		.qtxt{font-size:12px;width:120px;height:20px;margin-left:2px;border:2px solid #444;-webkit-border-radius:0px;-moz-border-radius:0px;}
		.qsubmit{height:25px;font-size:14px;background-color:#555;color:#fff;border:1px solid #555;padding:0px 5px}
		.qsubmit:hover{background-color:#444;}

		.nolist{color:#000;height:30px;}
		.clearx{clear:both;height:0;}
		#divmessage{position:absolute;left:0;top:0;visibility:hidden;z-index:100;border:1px solid #999;background-color:#fff;padding:4px;}
		.thtitle{display:block;height:20px;margin:2px 0 0 0;overflow:hidden;text-align:center;color:#000;}
		.balloon{position:absolute;left:0;top:0;z-index:100;border:1px solid #999;background-color:#fff;padding:4px;display:none;}
	</style>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<!-- 메인메뉴 end -->
	<div id="subContent">
		<!-- submenu -->
		<jsp:include page="/WEB-INF/views/coun/base/base_left_02.jsp" flush="true"/>
		<!-- sbumenu end -->

		<h3 class="skip">본문내용 he</h3>
		<div id="content">
			<div id="subTitle">
				<h3>의회 소식지</h3>
			</div>
			<div id="sub_default">

				<jsp:include page="/ebook/include/catalist5.jsp" flush="true"/>

			</div>

		</div>
	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
