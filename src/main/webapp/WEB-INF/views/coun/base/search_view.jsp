<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<script>
		$(document).ready(function(){
			$(".menu6").removeClass("current");
			$(".menu6").addClass("current_on");
			$(".depth2 > li:nth-child(5)").addClass("current_on");
		});
		var fnActRetrieve   = function(page) {
			doActLoadShow();
			$("#pageCurNo").val(isEmpty(page)?"1":page);
			var $form=$("#frmDefault");
			var  view=$("#condValue").val();
			$("<input/>").attr({type:"hidden",id:"searchText",name:"searchText",value:view}).appendTo($form);
			$("#frmDefault").attr({"action":"/coun/base/search.do","method":"post","target":"_self"}).submit();
		};
		var fnActDetail     = function(page,acts) {
			var $form=$("#frmDetail");
			var  view=$("#searchText").val();
			if($form.length<=0){$form=$("<form/>").attr({id:"frmDetail",name:"frmDetail",method:"post"});$(document.body).append($form);}
			if($form.length<=1){$form.empty();}
			if($form.length<=1){$("<input/>").attr({type:"hidden",id:"pageCurNo",name:"pageCurNo",value:"1"}).appendTo($form);}
			if($form.length<=1){$("<input/>").attr({type:"hidden",id:"condType",name:"condType",value:"A"}).appendTo($form);}
			if($form.length<=1){$("<input/>").attr({type:"hidden",id:"condValue",name:"condValue",value:view}).appendTo($form);}
			if($form.length<=1){$("<input/>").attr({type:"hidden",id:"reEraCode",name:"reEraCode",value:"${reEraCode}"}).appendTo($form);}
			if($form.length<=1){$("<input/>").attr({type:"hidden",id:"searchText",name:"searchText",value:view}).appendTo($form);}
			if($form.length<=1){$form.attr({"action":"<%= webBase %>/coun/"+acts+"List.do","method":"post"}).submit();}
		};
	</script>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<!-- 메인메뉴 end -->
	<div id="subContent">
		<!-- submenu -->
		<!-- 왼쪽메뉴 -->
		<jsp:include page="/WEB-INF/views/coun/base/base_left_01.jsp" flush="true"/>
		<!-- sbumenu end -->


		<h3 class="skip">본문내용 he</h3>
		<div id="content">

			<div id="subTitle">
				<!-- sub 제목부분 -->
				<h3>통합검색&nbsp;</h3>
			</div>
			<div id="sub_default">

				<!-- program 들어갈부분 -->
				<form id='frmDefault' name='frmDefault'>
					<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
					<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
					<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>

				<div class="round_txt">
					<span class="search_icon"></span>
					<em>평택시의회 통합검색페이지 입니다.<p>검색어를 입력하셔서 검색버튼을 누르세요</p>
						<label for='condValue' class='hide'>검색어</label>
						<input type='text' id='condValue' name='condValue' class='w50' maxlength='30' value='${searchText}' placeholder='검색어를 입력하세요.'/>
						<a href='javascript:void(0);' class='form_btn' onclick='fnActRetrieve()'>검색</a>
						<%--<input type="text" id='searchTopsText' name='searchTopsText' value='${searchText}' placeholder="검색어를 입력해주세요" class="searchbar" onkeyup='fnKeyDownFinder("view")'/>--%>
						<%--<a href='javascript:void(0);' class='form_btn' onclick='fnTopsFinder("view")'>검색</a>--%>
					</em>
				</div>

				<c:if test="${!empty searchText}">
					<div class='board_total'>검색어 "<strong>${searchText}</strong>"에 대한 전체 "<strong>${totsCnt}</strong>"개의 결과를 찾았습니다</div>
					<div class='section'>
						<h4 class='h4_tit'>게시판 검색결과 : ${boardCnt}건</h4>
						<div class='list_style01'><ul><%--
					<li><c:if test="${'0' ne schdCnt}"><a href='javascript:void(0);' onclick='fnActDetail(1,"schd/report")' title='의사일정 게시글 검색결과: ${schdCnt}건(새창열림)'></c:if>의사일정(${schdCnt}건)<c:if test="${'0' ne schdCnt}"></a></c:if></li>--%>
							<li><c:if test="${'0' ne photoCnt}"><a href='javascript:void(0);' onclick='fnActDetail(1,"base/photo")' title='포토의정활동 게시글 검색결과: ${photoCnt}건(새창열림)'></c:if>포토의정활동(${photoCnt}건)<c:if test="${'0' ne photoCnt}"></a></c:if></li>
							<li><c:if test="${'0' ne lawsCnt}"><a href='javascript:void(0);' onclick='fnActDetail(1,"laws/report")' title='입법예고 게시글 검색결과: ${lawsCnt}건(새창열림)'></c:if>입법예고(${lawsCnt}건)<c:if test="${'0' ne lawsCnt}"></a></c:if></li>
							<li><c:if test="${'0' ne notiCnt}"><a href='javascript:void(0);' onclick='fnActDetail(1,"base/notice")' title='공지사항 게시글 검색결과: ${notiCnt}건(새창열림)'></c:if>공지사항(${notiCnt}건)<c:if test="${'0' ne notiCnt}"></a></c:if></li>
							<li><c:if test="${'0' ne pressCnt}"><a href='javascript:void(0);' onclick='fnActDetail(1,"press/report")' title='보도자료 게시글 검색결과: ${pressCnt}건(새창열림)'></c:if>보도자료(${pressCnt}건)<c:if test="${'0' ne pressCnt}"></a></c:if></li>
							<li><c:if test="${'0' ne hopeCnt}"><a href='javascript:void(0);' onclick='fnActDetail(1,"hope/notice")' title='의회에 바란다 게시글 검색결과: ${hopeCnt}건(새창열림)'></c:if>의회에바란다(${hopeCnt}건)<c:if test="${'0' ne hopeCnt}"></a></c:if></li>
						</ul></div>
					</div>
					<div class='section'>
						<h4 class='h4_tit'><c:if test="${'0' ne minutesCnt}"><a href='javascript:void(0);' onclick='fnActDetail(1,"tail/minutes")' title='회의록 검색결과: ${minutesCnt}건(새창열림)'></c:if>회의록 검색결과 : ${minutesCnt}건<c:if test="${'0' ne minutesCnt}"></a></c:if></h4>
					</div>
					<div class='section'>
						<h4 class='h4_tit'><c:if test="${'0' ne agendaCnt}"><a href='javascript:void(0);' onclick='fnActDetail(1,"find/agenda")' title='의안 검색결과: ${agendaCnt}건(새창열림)'></c:if>의안 검색결과 : ${agendaCnt}건<c:if test="${'0' ne agendaCnt}"></a></c:if></h4>
					</div>
				</c:if>

				</form>
				<!--  program 들어갈부분 end -->

			</div>



		</div>
	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
