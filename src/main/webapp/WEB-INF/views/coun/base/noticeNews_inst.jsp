<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<script>
		$(document).ready(function(){
			$(".menu7").removeClass("current");
			$(".menu7").addClass("current_on");
			$(".depth2 > li:nth-child(3)").addClass("current_on");
			$(".depth2 > li:nth-child(3) > ul > li:nth-child(2)").addClass("current_on");
		});
	</script>
</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<!-- 메인메뉴 end -->
	<div id="subContent">
		<!-- submenu -->
		<jsp:include page="/WEB-INF/views/coun/base/base_left_02.jsp" flush="true"/>
		<!-- sbumenu end -->

		<h3 class="skip">본문내용 he</h3>
		<div id="content">
			<div id="subTitle">
				<h3>의회소식지 신청&nbsp;</h3>
			</div>
			<div id="sub_default">

				<p class="round_txt"><span class="news_icon"></span><em>평택시의회에서는 시민의 알권리 충족과 열린 의회 구현을 위해 다양한 의정활동 소식을 담은 ｢의정산책(평택시의회 소식지)｣을 발행해 시민 여러분께 무료로 제공하고 있습니다.</em>
					<span class="right"><a href="/cmmn/down/app.do" class='btn blue'> 신청서다운로드</a></span>

				</p>

				<h4>소식지 무료 구독 신청안내</h4>
				<p class="round_txt">평택시의회에서는 시민의 알권리 충족과 열린 의회 구현을 위해 다양한 의정활동 소식을 담은 ｢의정산책(평택시의회 소식지)｣을 발행해 시민 여러분께 무료로 제공하고 있습니다. 구독을 원하시는 시민 여러분의 많은 신청을 바랍니다.</p>
				<ul class="dot"><li>수록내용 : 회기 운영현황, 의원발의 조례, 7분 자유발언, 상임위 주요 활동 등</li></ul>

				<h4>신청 방법 : 무료 구독 신청서 작성 후 아래 방법으로 제출</h4>
				<ul class="dot">
					<li>메 일 : uihoe@korea.kr</li>
					<li>팩 스 : 031) 8024-7569</li>
					<li>우 편 : (17730) 경기도 평택시 경기대로 1366, 평택시의회 의회사무국</li>
					<li>문 의 : 평택시의회 홍보팀 ☎ 031)8024-7582</li>
				</ul>

			</div>

		</div>
	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
