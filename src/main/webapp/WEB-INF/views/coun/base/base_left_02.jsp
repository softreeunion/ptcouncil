<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>

		
		<!-- sub menu -->
	  <div id="submenuArea" class="w sm_intro">
		<div class="sm_tit ">
			<h2>
			알림마당
			</h2>
		</div>
		<ul class="depth2">
		<!-- 메뉴 활성화시 class="current" 가 class="current_on"으로 바뀌도록 -->
					<li class="current"> <a href="/coun/base/noticeList.do" class="nav-link" title="공지사항">공지사항</a> </li>
					<li class="current"> <a href="/coun/laws/reportList.do" class="nav-link" title="입법예고">입법예고</a> </li>
					<li class="current"> <a href="/coun/news/noticeList.do" class="nav-link" title="의회소식지">의회소식지</a>
						<ul>
							<li class="current"> <a href="/coun/news/noticeList.do" class="nav-link" title="의회소식지">의회소식지</a> </li>
							<li class="current"> <a href="/coun/news/noticeInst.do" class="nav-link" title="의회소식지 신청">의회소식지 신청</a> </li>

						</ul>
					</li>
					<li class="current "> <a href="/coun/press/reportList.do" class="nav-link" title="보도자료">보도자료</a> </li>
					<li class="current"> <a href="/coun/interview/reportList.do" class="nav-link" title="의원인터뷰">의원인터뷰</a> </li>
					<li class="current"> <a href="//www.elis.go.kr/newlaib/laibLaws/h1126/laws.jsp?regionId=41220" class="nav-link" target="_blank" title="자치법규">자치법규</a> </li>
					<li class="current"> <a href="/webdata/index.do" class="nav-link" target="_blank" title="자료실">자료실</a> </li>
					<li class="current"> <a href="/coun/base/counTour.do" class="nav-link" title="청사 견학">청사 견학</a> </li>
				
				</ul>
		
		
			<!-- sub menu end -->
			</div>
			
			
			