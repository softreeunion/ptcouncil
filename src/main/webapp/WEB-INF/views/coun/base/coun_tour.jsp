<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	boolean viewPoster			= true;
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>

	<script>
		$(document).ready(function(){
			$(".menu7").removeClass("current");
			$(".menu7").addClass("current_on");
			$(".depth2 > li:nth-child(8)").addClass("current_on");
		});
	</script>


</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->


	<!-- 메인메뉴 end -->
	<div id="subContent">
		<!-- submenu -->
		<jsp:include page="/WEB-INF/views/coun/base/base_left_02.jsp" flush="true"/>
		<!-- sbumenu end -->

		<h3 class="skip">본문내용 he</h3>
		<div id="content">
			<div id="subTitle">
				<h3>청사 견학&nbsp;</h3>
			</div>
			<div id="sub_default">

				<p class="round_txt"><span class="tour_icon"></span><em>평택시의회에서는 의정 활동을 홍보하여 공감하는 의정, 신뢰받는 의회 구현을 위해 시민을 대상으로 의회 견학을 진행하오니 많은 신청 바랍니다.<br>
					※ 견학 희망일 10일전까지 신청 바랍니다.</em>
					<span class="right"><a href="/download.do?fileName=견학 신청서(개인정보 활용 동의서 포함).hwp" class='btn blue'> 신청서다운로드</a></span>

				</p>

				<h4>운영 일정</h4>
				<ul class="dot">
					<li>운영 기간 : 2023년 6월부터</li>
					<li>운영 시간 : 평일 10시 ~ 17시</li>
					<li>※ 토·일·공휴일 및 회기 중 제외되며, 회기 일정은 홈페이지 공지사항
						연간 회기일정을 참고하시기 바랍니다.</li>
					<li>※ 시의회 홍보 영상 시청, 청사 견학 등 약 1시간 소요</li>
				</ul>
				<h4>참여대상</h4>
				<ul class="dot">
					<li>견학대상 : 평택 시민, 초·중·고등학교 재학생, 지역아동센터, 학교밖청소년지원센터 등 청소년</li>
					<li>참여인원 : 10명 ~ 30명(인솔자 포함)</li>
				</ul>
				<h4>신청 방법 : 신청서 작성 후 아래 방법으로 제출</h4>
				<ul class="dot">
					<li>메 일 : uihoe@korea.kr</li>
					<li>팩 스 : 031) 8024-7569</li>
					<li>문 의 : 평택시의회 홍보팀 ☎ 031)8024-7582</li>
				</ul>

			</div>

		</div>
	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>



</body>
</html>
