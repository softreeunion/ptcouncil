<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_certi.jspf"/>

<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta property="og:image" content=""/>
	<meta name="keywords" content="평택시의회" />
	<meta name="description" content="평택시의회 홈페이지입니다." />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><%= webTitle %></title>
	<link rel="shortcut icon" href="/images/favicon.ico" />
	<link rel="canonical" href="http://www.ptcouncil.go.kr">
	<!-- css -->

	<link media="all" type="text/css" rel="stylesheet" href="/common/css/lyout.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/bootstrap.css" />
	<link media="all" type="text/css" rel="stylesheet" href="/common/css/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/common/css/slick.css" />
	<!-- sub contentcss -->
	<link rel="stylesheet" type="text/css" href="/common/css/sub_content.css" />


	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script type="text/javascript" src="/js/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="/js/slick.min.js"></script>

	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg-new.jspf"/>
	<script>
		$(document).ready(function(){
			$(".menu6").removeClass("current");
			$(".menu6").addClass("current_on");
			$(".depth2 > li:nth-child(3)").addClass("current_on");
		});
		var fnActReturn     = function(seq){
			doActLoadShow();
			fncClearTimer();
			$("#frmDefault").attr({"action":"notice"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self"}).submit();
		};
		var fnActPopuper    = function(){<c:choose><c:when test="${'Delete' eq nextMode}">
			$("#frmDefault").attr({"action":"noticeDelete.do","method":"post","target":"_self"}).ajaxSubmit({
				dataType      : "json",
				cache         : false ,
				beforeSubmit  : function(data,form,options){doActLoadShow();},
				success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){fnActReturn("seq");}else{if(data.isError=="C"){if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}}else{fnActReturn("seq");}}},
				error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
			});</c:when><c:otherwise>
			doActLoadShow();<%--
		alert("<%= alertMsg[12] %>");--%>
			$("#frmDefault").attr({"action":"notice<c:choose><c:when test="${ empty nextMode}">List</c:when><c:otherwise>${nextMode}</c:otherwise></c:choose>.do","method":"post","target":"_self"}).submit();</c:otherwise></c:choose>
		};
		var fnActPopup      = function(){<c:if test="${'T' ne certTest}">
			if(!$("#privacy_agree").prop("checked")) {alert("개인정보 수집·이용에 동의하셔야 진행하실 수 있습니다.");$("#privacy_agree").focus();return false;}
			if(!$("#terms_agree").prop("checked")) {alert("이용약관에 동의하셔야 진행하실 수 있습니다.");$("#terms_agree").focus();return false;}
			fnActOpenPlus();</c:if><c:if test="${'T' eq certTest}">
			fnActPopuper();</c:if>
		};
	</script>

</head>

<body>
<p id="skip-navigation"><a href="#main_contents">본문바로가기</a></p>
<div id="content">
	<!-- header -->
	<jsp:include page="/WEB-INF/views/coun/inc/header-new.jsp" flush="true"/>
	<!-- //header -->

	<div id="subContent">
		<!-- submenu -->
		<jsp:include page="/WEB-INF/views/coun/base/base_left_01.jsp" flush="true"/>
		<!-- sbumenu end -->

		<form id='frmDefault' name='frmDefault'>
			<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
			<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
			<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
			<input type='hidden' id='reUseStat'     name='reUseStat'     value='${reUseStat}'/>
			<input type='hidden' id='m'             name='m'             value='checkplusSerivce'/><%-- 필수 데이타로, 누락하시면 안됩니다. --%>
			<input type='hidden' id='EncodeData'    name='EncodeData'    value='<%= sEncData %>'/><%-- 위에서 업체정보를 암호화 한 데이타입니다. --%>


			<h3 class="skip">본문내용</h3>
			<div id="content">
				<div id="subTitle">
					<h3>행정사무감사 시민제보</h3>
				</div>
				<div id="sub_default">

					<h4>개인정보 수집·이용 동의 안내</h4>
					<div class="round_txt">
						<ul class="dot">
							<li>개인정보 수집·이용 목적 : 행정사무감사 시민제보</li>
							<li>수집하는 개인정보 항목 : 공공기록물 관리에 관한 법령 및 민원처리에 관한 법령</li>
							<li>개인정보의 보유 및 이용 기간 : <span style='color:red;'>3년(게시글:10년)</span>(「표준 개인정보 보호지침(행정자치부 고시 제2014-1호)」‘[별표 1]개인정보 파일 보유기간 책정 기준표’에 준함)</li>
							<li>동의를 거부하실 수 있으나, 거부 시 정상적인 민원 접수 처리가 불가함</li>
						</ul>
					</div>
					<div class='p_tit_np right'><input type='checkbox' id='privacy_agree' name='privacy_agree'/> <label for='privacy_agree'>개인정보 수집·이용에 동의합니다.</label></div>


					<h4>이용약관 동의 안내</h4>
					<div class="round_txt">
						평택시의회는 의회가 제공하는 인터넷 서비스(이하 서비스 라 합니다)의 내용을 보호하고, 저작권 등 타인의 권리를 침해하지 않도록 하기 위하여 다음과 같은 저작권 정책을 운영하고 있습니다. 다음에 명시되지 아니한 저작권 관련 사항은 평택시의회 홈페이지 이용약관과 저작권법을 준용합니다. 평택시의회의 저작권 보호 정책은 관련 법률 및 정부지침의 변경 등에 따라 개정될 수 있으며, 개정된 사항은 시행 전 평택시의회의 홈페이지에 공지하도록 합니다.
					</div>
					<div class='p_tit_np right'><input type='checkbox' id='terms_agree' name='terms_agree'/> <label for='terms_agree'>이용약관에 동의합니다.</label></div>

					<!-- 게시판 하단버튼 -->
					<div class="center  pT30">
						<a href='javascript:void(0);' class='btn orange' onclick='fnActPopup()'>휴대폰 본인인증</a>
						<a href='javascript:void(0);' class='btn' onclick='fnActReturn("seq")'>취소</a>
					</div>

					<!-- sub_sub_default  end -->
				</div>
			</div>
		</form>


	</div>
</div>
<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer-new.jsp" flush="true"/>
<!-- //footer -->


</div>

</body>
</html>
