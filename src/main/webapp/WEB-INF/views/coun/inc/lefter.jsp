<%
////////////////////////////////////////////////////////////
// Program ID  : lefter
// Description : 시의회 LEFTER
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String tbParam			= BaseUtility.null2Blank( request.getParameter("tb") );
	StringBuffer dispMenu	= new StringBuffer();
	int[] snbIdx			= {0,0};

	if( "1".equals(tbParam) && !(counAList == null || counAList.length <= 0) ) { 
		dispMenu.append(baseTab[2] +"<div id='snb'>\n");
		dispMenu.append(baseTab[3] +"<h2 class='h2_tit'>의회소개</h2>\n");
		dispMenu.append(baseTab[3] +"<ul>\n");
		for( int m = 0; m < counAList.length; m++ ) {
			if( "1".equals(counAList[m][2]) ) snbIdx[0]++;
			if( "T".equals(counAList[m][0]) && "1".equals(counAList[m][2]) ) { 
				dispMenu.append(baseTab[4] +"<li><a href='"+ ("L".equals(counAList[m][1])?counAList[m][4]+"' target='_blank":webBase+counAList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2));
				dispMenu.append("'"+ ("N".equals(counAList[m][1])?" class='noChild'":"") +">"+ counAList[m][3] +"</a>"+ ("N".equals(counAList[m][1])?"</li>":"") +"\n");
				if( "C".equals(counAList[m][1]) ) dispMenu.append(baseTab[5] +"<ul>\n");
			}
			if( "T".equals(counAList[m][0]) && "2".equals(counAList[m][2]) ) { 
				snbIdx[1]++;
				dispMenu.append(baseTab[6] +"<li><a href='"+ ("L".equals(counAList[m][1])?counAList[m][4]+"' target='_blank":webBase+counAList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2) +"_"+ BaseUtility.zeroFill(snbIdx[1],2));
				dispMenu.append("'>"+ counAList[m][3] +"</a></li>\n");
			}
			if( "T".equals(counAList[m][0]) && "E".equals(counAList[m][1]) ) { 
				snbIdx[1] = 0;
				dispMenu.append(baseTab[5] +"</ul>\n").append(baseTab[4] +"</li>\n");
			}
		}
		dispMenu.append(baseTab[3] +"</ul>\n").append(baseTab[2] +"</div>\n");
	}
	if( "2".equals(tbParam) && !(counBList == null || counBList.length <= 0) ) { 
		dispMenu.append(baseTab[2] +"<div id='snb'>\n");
		dispMenu.append(baseTab[3] +"<h2 class='h2_tit'>의원소개</h2>\n");
		dispMenu.append(baseTab[3] +"<ul>\n");
		for( int m = 0; m < counBList.length; m++ ) {
			if( "1".equals(counBList[m][2]) ) snbIdx[0]++;
			if( "T".equals(counBList[m][0]) && "1".equals(counBList[m][2]) ) { 
				dispMenu.append(baseTab[4] +"<li><a href='"+ ("L".equals(counBList[m][1])?counBList[m][4]+"' target='_blank":webBase+counBList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2));
				dispMenu.append("'"+ ("N".equals(counBList[m][1])?" class='noChild'":"") +">"+ counBList[m][3] +"</a>"+ ("N".equals(counBList[m][1])?"</li>":"") +"\n");
				if( "C".equals(counBList[m][1]) ) dispMenu.append(baseTab[5] +"<ul>\n");
			}
			if( "T".equals(counBList[m][0]) && "2".equals(counBList[m][2]) ) { 
				snbIdx[1]++;
				dispMenu.append(baseTab[6] +"<li><a href='"+ ("L".equals(counBList[m][1])?counBList[m][4]+"' target='_blank":webBase+counBList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2) +"_"+ BaseUtility.zeroFill(snbIdx[1],2));
				dispMenu.append("'>"+ counBList[m][3] +"</a></li>\n");
			}
			if( "T".equals(counBList[m][0]) && "E".equals(counBList[m][1]) ) { 
				snbIdx[1] = 0;
				dispMenu.append(baseTab[5] +"</ul>\n").append(baseTab[4] +"</li>\n");
			}
		}
		dispMenu.append(baseTab[3] +"</ul>\n").append(baseTab[2] +"</div>\n");
	}
	if( "3".equals(tbParam) && !(counCList == null || counCList.length <= 0) ) { 
		dispMenu.append(baseTab[2] +"<div id='snb'>\n");
		dispMenu.append(baseTab[3] +"<h2 class='h2_tit'>의정활동</h2>\n");
		dispMenu.append(baseTab[3] +"<ul>\n");
		for( int m = 0; m < counCList.length; m++ ) {
			if( "1".equals(counCList[m][2]) ) snbIdx[0]++;
			if( "T".equals(counCList[m][0]) && "1".equals(counCList[m][2]) ) { 
				dispMenu.append(baseTab[4] +"<li><a href='"+ ("L".equals(counCList[m][1])?counCList[m][4]+"' target='_blank":webBase+counCList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2));
				dispMenu.append("'"+ ("N".equals(counCList[m][1])?" class='noChild'":"") +">"+ counCList[m][3] +"</a>"+ ("N".equals(counCList[m][1])?"</li>":"") +"\n");
				if( "C".equals(counCList[m][1]) ) dispMenu.append(baseTab[5] +"<ul>\n");
			}
			if( "T".equals(counCList[m][0]) && "2".equals(counCList[m][2]) ) { 
				snbIdx[1]++;
				dispMenu.append(baseTab[6] +"<li><a href='"+ ("L".equals(counCList[m][1])?counCList[m][4]+"' target='_blank":webBase+counCList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2) +"_"+ BaseUtility.zeroFill(snbIdx[1],2));
				dispMenu.append("'>"+ counCList[m][3] +"</a></li>\n");
			}
			if( "T".equals(counCList[m][0]) && "E".equals(counCList[m][1]) ) { 
				snbIdx[1] = 0;
				dispMenu.append(baseTab[5] +"</ul>\n").append(baseTab[4] +"</li>\n");
			}
		}
		dispMenu.append(baseTab[3] +"</ul>\n").append(baseTab[2] +"</div>\n");
	}
	if( "4".equals(tbParam) && !(counDList == null || counDList.length <= 0) ) {
//		String url = request.getRequestURI();
//		String uri = url.replaceAll("/WEB-INF/views/","");
//		String ct = request.getParameter("ct");
//		dispMenu.append("uri=> "+uri+"<br>");
//		dispMenu.append("ct=> "+ct+"<br>");
//
//		String menu1 = "",menu2 = "",menu3="";
//		String menu3_1 = "",menu3_2 = "",menu3_3 = "",menu3_4 = "";
//
//		if("/coun/cast/live_list.jsp".equals(uri)) menu1 = "on";
//		else if("/coun/cast/general_list.jsp".equals(uri)) menu2 = "on";
//		else if("/coun/acts/movieStand_list.jsp".equals(uri)) menu3 = "on";
//
//		if("1".equals(ct)) menu3_1 = "on";
//		else if("2".equals(ct)) menu3_2 = "on";
//		else if("3".equals(ct)) menu3_3 = "on";
//		else if("4".equals(ct)) menu3_4 = "on";
//
//		dispMenu.append(" <div id='snb'> ");
//		dispMenu.append(" <h2 class='h2_tit'>인터넷방송</h2> ");
//		dispMenu.append(" <ul> ");
//		dispMenu.append(" <li><a href='/coun/cast/live.do' id='snb04_01' class='noChild "+menu1+"'>생방송</a></li> ");
//		dispMenu.append(" <li><a href='/coun/cast/general.do' id='snb04_02' class='"+menu2+"'>본회의</a> ");
//		dispMenu.append(" <ul> ");
//		dispMenu.append(" <li><a href='/coun/cast/general.do' id='snb04_02_01'>본회의</a></li> ");
//		dispMenu.append(" <li><a href='/coun/base/formerList.do' id='snb04_02_02'>이전회의록</a></li> ");
//		dispMenu.append(" </ul> ");
//		dispMenu.append(" </li> ");
//		dispMenu.append(" <li><a href='/coun/stand/movieList.do?ct=1' id='snb04_03' class='"+menu3+"'>상임위원회</a> ");
//		dispMenu.append(" <ul> ");
//		dispMenu.append(" <li><a href='/coun/stand/movieList.do?ct=1' id='snb04_03_01' class='"+menu3_1+"'>의회운영위원회</a></li> ");
//		dispMenu.append(" <li><a href='/coun/stand/movieList.do?ct=2' id='snb04_03_02' class='"+menu3_2+"'>기획행정위원회</a></li> ");
//		dispMenu.append(" <li><a href='/coun/stand/movieList.do?ct=3' id='snb04_03_03' class='"+menu3_3+"'>복지환영위원회</a></li> ");
//		dispMenu.append(" <li><a href='/coun/stand/movieList.do?ct=4' id='snb04_03_04' class='"+menu3_4+"'>산업건설위원회</a></li> ");
//		dispMenu.append(" </ul> ");
//		dispMenu.append(" </li> ");
//		dispMenu.append(" </ul> ");
//		dispMenu.append(" </div> ");




		dispMenu.append(baseTab[2] +"<div id='snb'>\n");
		dispMenu.append(baseTab[3] +"<h2 class='h2_tit'>인터넷방송</h2>\n");
		dispMenu.append(baseTab[3] +"<ul>\n");
		for( int m = 0; m < counDList.length; m++ ) {
//			if(m==1) {
//				dispMenu.append("<div style='background:#a9a9a9;color:#ffffff;padding: 20px 0 20px 10px;font-size:18px;'>다시보기</div>");
//			}
			if( "1".equals(counDList[m][2]) ) snbIdx[0]++;
			if( "T".equals(counDList[m][0]) && "1".equals(counDList[m][2]) ) {
				dispMenu.append(baseTab[4] +"<li><a href='"+ ("L".equals(counDList[m][1])?counDList[m][4]+"' target='_blank":webBase+counDList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2));
				dispMenu.append("'"+ ("N".equals(counDList[m][1])?" class='noChild'":"") +">"+ counDList[m][3] +"</a>"+ ("N".equals(counDList[m][1])?"</li>":"") +"\n");
				if( "C".equals(counDList[m][1]) ) dispMenu.append(baseTab[5] +"<ul>\n");
			}
			if( "T".equals(counDList[m][0]) && "2".equals(counDList[m][2]) ) {
				snbIdx[1]++;
				dispMenu.append(baseTab[6] +"<li><a href='"+ ("L".equals(counDList[m][1])?counDList[m][4]+"' target='_blank":webBase+counDList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2) +"_"+ BaseUtility.zeroFill(snbIdx[1],2));
				dispMenu.append("'>"+ counDList[m][3] +"</a></li>\n");
			}
			if( "T".equals(counDList[m][0]) && "E".equals(counDList[m][1]) ) {
				snbIdx[1] = 0;
				dispMenu.append(baseTab[5] +"</ul>\n").append(baseTab[4] +"</li>\n");
			}
		}
		dispMenu.append(baseTab[3] +"</ul>\n").append(baseTab[2] +"</div>\n");
	}
	if( "5".equals(tbParam) && !(counEList == null || counEList.length <= 0) ) { 
		dispMenu.append(baseTab[2] +"<div id='snb'>\n");
		dispMenu.append(baseTab[3] +"<h2 class='h2_tit'>회의록검색</h2>\n");
		dispMenu.append(baseTab[3] +"<ul>\n");

		for( int m = 0; m < counEList.length; m++ ) {
//			dispMenu.append("counEList[m][0]=> "+counEList[m][0]+"<br>");
//			dispMenu.append("counEList[m][1]=> "+counEList[m][1]+"<br>");
//			dispMenu.append("counEList[m][2]=> "+counEList[m][2]+"<br>");
//			dispMenu.append("counEList[m][3]=> "+counEList[m][3]+"<br>");
//			dispMenu.append("counEList[m][4]=> "+counEList[m][4]+"<br>");
//			dispMenu.append("request.getRequestURI()=> "+request.getRequestURI()+"<br>");


			if( "1".equals(counEList[m][2]) ) snbIdx[0]++;
			if( "T".equals(counEList[m][0]) && "1".equals(counEList[m][2]) ) { 
				dispMenu.append(baseTab[4] +"<li><a href='"+ ("L".equals(counEList[m][1])?counEList[m][4]+"' target='_blank":webBase+counEList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2));
				dispMenu.append("'"+ ("N".equals(counEList[m][1])?" class='noChild'":"") +">"+ counEList[m][3] +"</a>"+ ("N".equals(counEList[m][1])?"</li>":"") +"\n");
				if( "C".equals(counEList[m][1]) ) dispMenu.append(baseTab[5] +"<ul>\n");
			}
			if( "T".equals(counEList[m][0]) && "2".equals(counEList[m][2]) ) { 
				snbIdx[1]++;
				dispMenu.append(baseTab[6] +"<li><a href='"+ ("L".equals(counEList[m][1])?counEList[m][4]+"' target='_blank":webBase+counEList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2) +"_"+ BaseUtility.zeroFill(snbIdx[1],2));
				dispMenu.append("'>"+ counEList[m][3] +"</a></li>\n");
			}
			if( "T".equals(counEList[m][0]) && "E".equals(counEList[m][1]) ) { 
				snbIdx[1] = 0;
				dispMenu.append(baseTab[5] +"</ul>\n").append(baseTab[4] +"</li>\n");
			}
		}
		dispMenu.append(baseTab[3] +"</ul>\n").append(baseTab[2] +"</div>\n");
	}
	if( "6".equals(tbParam) && !(counFList == null || counFList.length <= 0) ) { 
		dispMenu.append(baseTab[2] +"<div id='snb'>\n");
		dispMenu.append(baseTab[3] +"<h2 class='h2_tit'>열린마당</h2>\n");
		dispMenu.append(baseTab[3] +"<ul>\n");
		for( int m = 0; m < counFList.length; m++ ) {
			if( "1".equals(counFList[m][2]) ) snbIdx[0]++;
			if( "T".equals(counFList[m][0]) && "1".equals(counFList[m][2]) ) { 
				dispMenu.append(baseTab[4] +"<li><a href='"+ ("L".equals(counFList[m][1])?counFList[m][4]+"' target='_blank":webBase+counFList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2));
				dispMenu.append("'"+ ("N".equals(counFList[m][1])?" class='noChild'":"") +">"+ counFList[m][3] +"</a>"+ ("N".equals(counFList[m][1])?"</li>":"") +"\n");
				if( "C".equals(counFList[m][1]) ) dispMenu.append(baseTab[5] +"<ul>\n");
			}
			if( "T".equals(counFList[m][0]) && "2".equals(counFList[m][2]) ) { 
				snbIdx[1]++;
				dispMenu.append(baseTab[6] +"<li><a href='"+ ("L".equals(counFList[m][1])?counFList[m][4]+"' target='_blank":webBase+counFList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2) +"_"+ BaseUtility.zeroFill(snbIdx[1],2));
				dispMenu.append("'>"+ counFList[m][3] +"</a></li>\n");
			}
			if( "T".equals(counFList[m][0]) && "E".equals(counFList[m][1]) ) { 
				snbIdx[1] = 0;
				dispMenu.append(baseTab[5] +"</ul>\n").append(baseTab[4] +"</li>\n");
			}
		}
		dispMenu.append(baseTab[3] +"</ul>\n").append(baseTab[2] +"</div>\n");
	}
	if( "7".equals(tbParam) && !(counGList == null || counGList.length <= 0) ) { 
		dispMenu.append(baseTab[2] +"<div id='snb'>\n");
		dispMenu.append(baseTab[3] +"<h2 class='h2_tit'>알림마당</h2>\n");
		dispMenu.append(baseTab[3] +"<ul>\n");
		for( int m = 0; m < counGList.length; m++ ) {
			if( "1".equals(counGList[m][2]) ) snbIdx[0]++;
			if( "T".equals(counGList[m][0]) && "1".equals(counGList[m][2]) ) { 
				dispMenu.append(baseTab[4] +"<li><a href='"+ ("L".equals(counGList[m][1])?counGList[m][4]+"' target='_blank":webBase+counGList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2));
				dispMenu.append("'"+ ("N".equals(counGList[m][1])?" class='noChild'":"") +">"+ counGList[m][3] +"</a>"+ ("N".equals(counGList[m][1])?"</li>":"") +"\n");
				if( "C".equals(counGList[m][1]) ) dispMenu.append(baseTab[5] +"<ul>\n");
			}
			if( "T".equals(counGList[m][0]) && "2".equals(counGList[m][2]) ) { 
				snbIdx[1]++;
				dispMenu.append(baseTab[6] +"<li><a href='"+ ("L".equals(counGList[m][1])?counGList[m][4]+"' target='_blank":webBase+counGList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2) +"_"+ BaseUtility.zeroFill(snbIdx[1],2));
				dispMenu.append("'>"+ counGList[m][3] +"</a></li>\n");
			}
			if( "T".equals(counGList[m][0]) && "E".equals(counGList[m][1]) ) { 
				snbIdx[1] = 0;
				dispMenu.append(baseTab[5] +"</ul>\n").append(baseTab[4] +"</li>\n");
			}
		}
		dispMenu.append(baseTab[3] +"</ul>\n").append(baseTab[2] +"</div>\n");
	}

	out.clearBuffer();
	out.print(dispMenu.toString()); %>