<%
////////////////////////////////////////////////////////////
// Program ID  : header
// Description : 시의회 / HEADER
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	int gnbSub				= 0;
	StringBuffer dispMenu	= new StringBuffer();
	StringBuffer dispFull	= new StringBuffer();
	StringBuffer dispMobi	= new StringBuffer();
	if( !(counAList == null || counAList.length <= 0) ) { 
		gnbSub++;
		dispMenu.append("<li>");
		dispMenu.append("\n"+ baseTab[6] +"<a href='"+ webBase + counAList[ 0][4] +"'>의회소개</a>");
		dispMenu.append("\n"+ baseTab[6] +"<div id='gnb_sub"+ BaseUtility.zeroFill( gnbSub, 2 ) +"' class='gnb_sub'><ul>");
		dispFull.append("<li>");
		dispFull.append("\n"+ baseTab[6] +"<a href='"+ webBase + counAList[ 0][4] +"'>의회소개</a>");
		dispFull.append("\n"+ baseTab[6] +"<div class='fullmenu_list_sub'><ul>");
		dispMobi.append("<li>");
		dispMobi.append("\n"+ baseTab[5] +"<a href='"+ webBase + counAList[ 0][4] +"'>의회소개</a>");
		dispMobi.append("\n"+ baseTab[5] +"<div class='mobile_menu_list_sub'><ul>");
		for( int m = 0; m < counAList.length; m++ ) { 
			if( "T".equals(counAList[m][0]) && "1".equals(counAList[m][2]) ) { 
				dispMenu.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(counAList[m][1])?counAList[m][4]+"' target='_blank":webBase+counAList[m][4]) +"'>"+ counAList[m][3] +"</a></li>");
				dispFull.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(counAList[m][1])?counAList[m][4]+"' target='_blank":webBase+counAList[m][4]) +"'>"+ counAList[m][3] +"</a>").append("N".equals(counAList[m][1])?"</li>":"");
				if( "C".equals(counAList[m][1]) ) dispFull.append("\n"+ baseTab[8] +"<ul>");
				dispMobi.append("\n"+ baseTab[6] +"<li><a href='"+ ("L".equals(counAList[m][1])?counAList[m][4]+"' target='_blank":webBase+counAList[m][4]) +"'>"+ counAList[m][3] +"</a>").append("N".equals(counAList[m][1])?"</li>":"");
				if( "C".equals(counAList[m][1]) ) dispMobi.append("\n"+ baseTab[7] +"<ul>");
			}
			if( "T".equals(counAList[m][0]) && "2".equals(counAList[m][2]) ) { 
				dispFull.append("\n"+ baseTab[9] +"<li><a href='"+ ("L".equals(counAList[m][1])?counAList[m][4]+"' target='_blank":webBase+counAList[m][4]) +"'>"+ counAList[m][3] +"</a></li>");
				dispMobi.append("\n"+ baseTab[8] +"<li><a href='"+ ("L".equals(counAList[m][1])?counAList[m][4]+"' target='_blank":webBase+counAList[m][4]) +"'>"+ counAList[m][3] +"</a></li>");
			}
//			if( m != counAList.length-1 && "2".equals(counAList[m][2]) ) { 
//				if( "1".equals(counAList[m+1][2]) ) dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
//				if( "1".equals(counAList[m+1][2]) ) dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
//			}
			if( "T".equals(counAList[m][0]) && "E".equals(counAList[m][1]) ) { 
				dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
				dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
			}
		}
		dispMenu.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispFull.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispMobi.append("\n"+ baseTab[5] +"</ul></div>").append("\n"+ baseTab[4] +"</li>");
	}
	if( !(counBList == null || counBList.length <= 0) ) { 
		gnbSub++;
		dispMenu.append("<li>");
		dispMenu.append("\n"+ baseTab[6] +"<a href='"+ webBase + counBList[ 0][4] +"'>위원소개2</a>");
		dispMenu.append("\n"+ baseTab[6] +"<div id='gnb_sub"+ BaseUtility.zeroFill( gnbSub, 2 ) +"' class='gnb_sub'><ul>");
		dispFull.append("<li>");
		dispFull.append("\n"+ baseTab[6] +"<a href='"+ webBase + counBList[ 0][4] +"'>위원소개2</a>");
		dispFull.append("\n"+ baseTab[6] +"<div class='fullmenu_list_sub'><ul>");
		dispMobi.append("<li>");
		dispMobi.append("\n"+ baseTab[5] +"<a href='"+ webBase + counBList[ 0][4] +"'>위원소개2</a>");
		dispMobi.append("\n"+ baseTab[5] +"<div class='mobile_menu_list_sub'><ul>");
		for( int m = 0; m < counBList.length; m++ ) { 
			if( "T".equals(counBList[m][0]) && "1".equals(counBList[m][2]) ) { 
				dispMenu.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(counBList[m][1])?counBList[m][4]+"' target='_blank":webBase+counBList[m][4]) +"'>"+ counBList[m][3] +"</a></li>");
				dispFull.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(counBList[m][1])?counBList[m][4]+"' target='_blank":webBase+counBList[m][4]) +"'>"+ counBList[m][3] +"</a>").append("N".equals(counBList[m][1])?"</li>":"");
				if( "C".equals(counBList[m][1]) ) dispFull.append("\n"+ baseTab[8] +"<ul>");
				dispMobi.append("\n"+ baseTab[6] +"<li><a href='"+ ("L".equals(counBList[m][1])?counBList[m][4]+"' target='_blank":webBase+counBList[m][4]) +"'>"+ counBList[m][3] +"</a>").append("N".equals(counBList[m][1])?"</li>":"");
				if( "C".equals(counBList[m][1]) ) dispMobi.append("\n"+ baseTab[7] +"<ul>");
			}
			if( "T".equals(counBList[m][0]) && "2".equals(counBList[m][2]) ) { 
				dispFull.append("\n"+ baseTab[9] +"<li><a href='"+ ("L".equals(counBList[m][1])?counBList[m][4]+"' target='_blank":webBase+counBList[m][4]) +"'>"+ counBList[m][3] +"</a></li>");
				dispMobi.append("\n"+ baseTab[8] +"<li><a href='"+ ("L".equals(counBList[m][1])?counBList[m][4]+"' target='_blank":webBase+counBList[m][4]) +"'>"+ counBList[m][3] +"</a></li>");
			}
//			if( m != counBList.length-1 && "2".equals(counBList[m][2]) ) { 
//				if( "1".equals(counBList[m+1][2]) ) dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
//				if( "1".equals(counBList[m+1][2]) ) dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
//			}
			if( "T".equals(counBList[m][0]) && "E".equals(counBList[m][1]) ) { 
				dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
				dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
			}
		}
		dispMenu.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispFull.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispMobi.append("\n"+ baseTab[5] +"</ul></div>").append("\n"+ baseTab[4] +"</li>");
	}
	if( !(counCList == null || counCList.length <= 0) ) { 
		gnbSub++;
		dispMenu.append("<li>");
		dispMenu.append("\n"+ baseTab[6] +"<a href='"+ webBase + counCList[ 0][4] +"'>의정활동</a>");
		dispMenu.append("\n"+ baseTab[6] +"<div id='gnb_sub"+ BaseUtility.zeroFill( gnbSub, 2 ) +"' class='gnb_sub'><ul>");
		dispFull.append("<li>");
		dispFull.append("\n"+ baseTab[6] +"<a href='"+ webBase + counCList[ 0][4] +"'>의정활동</a>");
		dispFull.append("\n"+ baseTab[6] +"<div class='fullmenu_list_sub'><ul>");
		dispMobi.append("<li>");
		dispMobi.append("\n"+ baseTab[5] +"<a href='"+ webBase + counCList[ 0][4] +"'>의정활동</a>");
		dispMobi.append("\n"+ baseTab[5] +"<div class='mobile_menu_list_sub'><ul>");
		for( int m = 0; m < counCList.length; m++ ) { 
			if( "T".equals(counCList[m][0]) && "1".equals(counCList[m][2]) ) { 
				dispMenu.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(counCList[m][1])?counCList[m][4]+"' target='_blank":webBase+counCList[m][4]) +"'>"+ counCList[m][3] +"</a></li>");
				dispFull.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(counCList[m][1])?counCList[m][4]+"' target='_blank":webBase+counCList[m][4]) +"'>"+ counCList[m][3] +"</a>").append("N".equals(counCList[m][1])?"</li>":"");
				if( "C".equals(counCList[m][1]) ) dispFull.append("\n"+ baseTab[8] +"<ul>");
				dispMobi.append("\n"+ baseTab[6] +"<li><a href='"+ ("L".equals(counCList[m][1])?counCList[m][4]+"' target='_blank":webBase+counCList[m][4]) +"'>"+ counCList[m][3] +"</a>").append("N".equals(counCList[m][1])?"</li>":"");
				if( "C".equals(counCList[m][1]) ) dispMobi.append("\n"+ baseTab[7] +"<ul>");
			}
			if( "T".equals(counCList[m][0]) && "2".equals(counCList[m][2]) ) { 
				dispFull.append("\n"+ baseTab[9] +"<li><a href='"+ ("L".equals(counCList[m][1])?counCList[m][4]+"' target='_blank":webBase+counCList[m][4]) +"'>"+ counCList[m][3] +"</a></li>");
				dispMobi.append("\n"+ baseTab[8] +"<li><a href='"+ ("L".equals(counCList[m][1])?counCList[m][4]+"' target='_blank":webBase+counCList[m][4]) +"'>"+ counCList[m][3] +"</a></li>");
			}
//			if( m != counCList.length-1 && "2".equals(counCList[m][2]) ) { 
//				if( "1".equals(counCList[m+1][2]) ) dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
//				if( "1".equals(counCList[m+1][2]) ) dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
//			}
			if( "T".equals(counCList[m][0]) && "E".equals(counCList[m][1]) ) { 
				dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
				dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
			}
		}
		dispMenu.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispFull.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispMobi.append("\n"+ baseTab[5] +"</ul></div>").append("\n"+ baseTab[4] +"</li>");
	}
	if( !(counDList == null || counDList.length <= 0) ) { 
		gnbSub++;
		dispMenu.append("<li>");
		dispMenu.append("\n"+ baseTab[6] +"<a href='"+ webBase + counDList[ 0][4] +"'>인터넷방송</a>");
		dispMenu.append("\n"+ baseTab[6] +"<div id='gnb_sub"+ BaseUtility.zeroFill( gnbSub, 2 ) +"' class='gnb_sub'><ul>");
		dispFull.append("<li>");
		dispFull.append("\n"+ baseTab[6] +"<a href='"+ webBase + counDList[ 0][4] +"'>인터넷방송</a>");
		dispFull.append("\n"+ baseTab[6] +"<div class='fullmenu_list_sub'><ul>");
		dispMobi.append("<li>");
		dispMobi.append("\n"+ baseTab[5] +"<a href='"+ webBase + counDList[ 0][4] +"'>인터넷방송</a>");
		dispMobi.append("\n"+ baseTab[5] +"<div class='mobile_menu_list_sub'><ul>");
		for( int m = 0; m < counDList.length; m++ ) { 
			if( "T".equals(counDList[m][0]) && "1".equals(counDList[m][2]) ) { 
				dispMenu.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(counDList[m][1])?counDList[m][4]+"' target='_blank":webBase+counDList[m][4]) +"'>"+ counDList[m][3] +"</a></li>");
				dispFull.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(counDList[m][1])?counDList[m][4]+"' target='_blank":webBase+counDList[m][4]) +"'>"+ counDList[m][3] +"</a>").append("N".equals(counDList[m][1])?"</li>":"");
				if( "C".equals(counDList[m][1]) ) dispFull.append("\n"+ baseTab[8] +"<ul>");
				dispMobi.append("\n"+ baseTab[6] +"<li><a href='"+ ("L".equals(counDList[m][1])?counDList[m][4]+"' target='_blank":webBase+counDList[m][4]) +"'>"+ counDList[m][3] +"</a>").append("N".equals(counDList[m][1])?"</li>":"");
				if( "C".equals(counDList[m][1]) ) dispMobi.append("\n"+ baseTab[7] +"<ul>");
			}
			if( "T".equals(counDList[m][0]) && "2".equals(counDList[m][2]) ) { 
				dispFull.append("\n"+ baseTab[9] +"<li><a href='"+ ("L".equals(counDList[m][1])?counDList[m][4]+"' target='_blank":webBase+counDList[m][4]) +"'>"+ counDList[m][3] +"</a></li>");
				dispMobi.append("\n"+ baseTab[8] +"<li><a href='"+ ("L".equals(counDList[m][1])?counDList[m][4]+"' target='_blank":webBase+counDList[m][4]) +"'>"+ counDList[m][3] +"</a></li>");
			}
			if( m != counDList.length-1 && "2".equals(counDList[m][2]) ) { 
				if( "1".equals(counDList[m+1][2]) ) dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
				if( "1".equals(counDList[m+1][2]) ) dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
			}
		}
		dispMenu.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispFull.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispMobi.append("\n"+ baseTab[5] +"</ul></div>").append("\n"+ baseTab[4] +"</li>");
	}
	if( !(counEList == null || counEList.length <= 0) ) { 
		gnbSub++;
		dispMenu.append("<li>");
		dispMenu.append("\n"+ baseTab[6] +"<a href='"+ webBase + counEList[ 0][4] +"'>회의록검색</a>");
		dispMenu.append("\n"+ baseTab[6] +"<div id='gnb_sub"+ BaseUtility.zeroFill( gnbSub, 2 ) +"' class='gnb_sub'><ul>");
		dispFull.append("<li>");
		dispFull.append("\n"+ baseTab[6] +"<a href='"+ webBase + counEList[ 0][4] +"'>회의록검색</a>");
		dispFull.append("\n"+ baseTab[6] +"<div class='fullmenu_list_sub'><ul>");
		dispMobi.append("<li>");
		dispMobi.append("\n"+ baseTab[5] +"<a href='"+ webBase + counEList[ 0][4] +"'>회의록검색</a>");
		dispMobi.append("\n"+ baseTab[5] +"<div class='mobile_menu_list_sub'><ul>");
		for( int m = 0; m < counEList.length; m++ ) { 
			if( "T".equals(counEList[m][0]) && "1".equals(counEList[m][2]) ) { 
				dispMenu.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(counEList[m][1])?counEList[m][4]+"' target='_blank":webBase+counEList[m][4]) +"'>"+ counEList[m][3] +"</a></li>");
				dispFull.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(counEList[m][1])?counEList[m][4]+"' target='_blank":webBase+counEList[m][4]) +"'>"+ counEList[m][3] +"</a>").append("N".equals(counEList[m][1])?"</li>":"");
				if( "C".equals(counEList[m][1]) ) dispFull.append("\n"+ baseTab[8] +"<ul>");
				dispMobi.append("\n"+ baseTab[6] +"<li><a href='"+ ("L".equals(counEList[m][1])?counEList[m][4]+"' target='_blank":webBase+counEList[m][4]) +"'>"+ counEList[m][3] +"</a>").append("N".equals(counEList[m][1])?"</li>":"");
				if( "C".equals(counEList[m][1]) ) dispMobi.append("\n"+ baseTab[7] +"<ul>");
			}
			if( "T".equals(counEList[m][0]) && "2".equals(counEList[m][2]) ) { 
				dispFull.append("\n"+ baseTab[9] +"<li><a href='"+ ("L".equals(counEList[m][1])?counEList[m][4]+"' target='_blank":webBase+counEList[m][4]) +"'>"+ counEList[m][3] +"</a></li>");
				dispMobi.append("\n"+ baseTab[8] +"<li><a href='"+ ("L".equals(counEList[m][1])?counEList[m][4]+"' target='_blank":webBase+counEList[m][4]) +"'>"+ counEList[m][3] +"</a></li>");
			}
			if( m != counEList.length-1 && "2".equals(counEList[m][2]) ) { 
				if( "1".equals(counEList[m+1][2]) ) dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
				if( "1".equals(counEList[m+1][2]) ) dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
			}
		}
		dispMenu.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispFull.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispMobi.append("\n"+ baseTab[5] +"</ul></div>").append("\n"+ baseTab[4] +"</li>");
	}
	if( !(counFList == null || counFList.length <= 0) ) { 
		gnbSub++;
		dispMenu.append("<li>");
		dispMenu.append("\n"+ baseTab[6] +"<a href='"+ webBase + counFList[ 0][4] +"'>열린마당</a>");
		dispMenu.append("\n"+ baseTab[6] +"<div id='gnb_sub"+ BaseUtility.zeroFill( gnbSub, 2 ) +"' class='gnb_sub'><ul>");
		dispFull.append("<li>");
		dispFull.append("\n"+ baseTab[6] +"<a href='"+ webBase + counFList[ 0][4] +"'>열린마당</a>");
		dispFull.append("\n"+ baseTab[6] +"<div class='fullmenu_list_sub'><ul>");
		dispMobi.append("<li>");
		dispMobi.append("\n"+ baseTab[5] +"<a href='"+ webBase + counFList[ 0][4] +"'>열린마당</a>");
		dispMobi.append("\n"+ baseTab[5] +"<div class='mobile_menu_list_sub'><ul>");
		for( int m = 0; m < counFList.length; m++ ) { 
			if( "T".equals(counFList[m][0]) && "1".equals(counFList[m][2]) ) { 
				dispMenu.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(counFList[m][1])?counFList[m][4]+"' target='_blank":webBase+counFList[m][4]) +"'>"+ counFList[m][3] +"</a></li>");
				dispFull.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(counFList[m][1])?counFList[m][4]+"' target='_blank":webBase+counFList[m][4]) +"'>"+ counFList[m][3] +"</a>").append("N".equals(counFList[m][1])?"</li>":"");
				if( "C".equals(counFList[m][1]) ) dispFull.append("\n"+ baseTab[8] +"<ul>");
				dispMobi.append("\n"+ baseTab[6] +"<li><a href='"+ ("L".equals(counFList[m][1])?counFList[m][4]+"' target='_blank":webBase+counFList[m][4]) +"'>"+ counFList[m][3] +"</a>").append("N".equals(counFList[m][1])?"</li>":"");
				if( "C".equals(counFList[m][1]) ) dispMobi.append("\n"+ baseTab[7] +"<ul>");
			}
			if( "T".equals(counFList[m][0]) && "2".equals(counFList[m][2]) ) { 
				dispFull.append("\n"+ baseTab[9] +"<li><a href='"+ ("L".equals(counFList[m][1])?counFList[m][4]+"' target='_blank":webBase+counFList[m][4]) +"'>"+ counFList[m][3] +"</a></li>");
				dispMobi.append("\n"+ baseTab[8] +"<li><a href='"+ ("L".equals(counFList[m][1])?counFList[m][4]+"' target='_blank":webBase+counFList[m][4]) +"'>"+ counFList[m][3] +"</a></li>");
			}
			if( m != counFList.length-1 && "2".equals(counFList[m][2]) ) { 
				if( "1".equals(counFList[m+1][2]) ) dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
				if( "1".equals(counFList[m+1][2]) ) dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
			}
		}
		dispMenu.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispFull.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispMobi.append("\n"+ baseTab[5] +"</ul></div>").append("\n"+ baseTab[4] +"</li>");
	}
	if( !(counGList == null || counGList.length <= 0) ) { 
		gnbSub++;
		dispMenu.append("<li>");
		dispMenu.append("\n"+ baseTab[6] +"<a href='"+ webBase + counGList[ 0][4] +"'>알림마당</a>");
		dispMenu.append("\n"+ baseTab[6] +"<div id='gnb_sub"+ BaseUtility.zeroFill( gnbSub, 2 ) +"' class='gnb_sub'><ul>");
		dispFull.append("<li>");
		dispFull.append("\n"+ baseTab[6] +"<a href='"+ webBase + counGList[ 0][4] +"'>알림마당</a>");
		dispFull.append("\n"+ baseTab[6] +"<div class='fullmenu_list_sub'><ul>");
		dispMobi.append("<li>");
		dispMobi.append("\n"+ baseTab[5] +"<a href='"+ webBase + counGList[ 0][4] +"'>알림마당</a>");
		dispMobi.append("\n"+ baseTab[5] +"<div class='mobile_menu_list_sub'><ul>");
		for( int m = 0; m < counGList.length; m++ ) { 
			if( "T".equals(counGList[m][0]) && "1".equals(counGList[m][2]) ) { 
				dispMenu.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(counGList[m][1])?counGList[m][4]+"' target='_blank":webBase+counGList[m][4]) +"'>"+ counGList[m][3] +"</a></li>");
				dispFull.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(counGList[m][1])?counGList[m][4]+"' target='_blank":webBase+counGList[m][4]) +"'>"+ counGList[m][3] +"</a>").append("N".equals(counGList[m][1])?"</li>":"");
				if( "C".equals(counGList[m][1]) ) dispFull.append("\n"+ baseTab[8] +"<ul>");
				dispMobi.append("\n"+ baseTab[6] +"<li><a href='"+ ("L".equals(counGList[m][1])?counGList[m][4]+"' target='_blank":webBase+counGList[m][4]) +"'>"+ counGList[m][3] +"</a>").append("N".equals(counGList[m][1])?"</li>":"");
				if( "C".equals(counGList[m][1]) ) dispMobi.append("\n"+ baseTab[7] +"<ul>");
			}
			if( "T".equals(counGList[m][0]) && "2".equals(counGList[m][2]) ) { 
				dispFull.append("\n"+ baseTab[9] +"<li><a href='"+ ("L".equals(counGList[m][1])?counGList[m][4]+"' target='_blank":webBase+counGList[m][4]) +"'>"+ counGList[m][3] +"</a></li>");
				dispMobi.append("\n"+ baseTab[8] +"<li><a href='"+ ("L".equals(counGList[m][1])?counGList[m][4]+"' target='_blank":webBase+counGList[m][4]) +"'>"+ counGList[m][3] +"</a></li>");
			}
			if( m != counGList.length-1 && "2".equals(counGList[m][2]) ) { 
				if( "1".equals(counGList[m+1][2]) ) dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
				if( "1".equals(counGList[m+1][2]) ) dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
			}
		}
		dispMenu.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispFull.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispMobi.append("\n"+ baseTab[5] +"</ul></div>").append("\n"+ baseTab[4] +"</li>");
	}

	out.clearBuffer(); %>	<div id='skipNavi'><a href='#contents'>본문 바로가기</a></div>
	<div class='top_link'>
		<div class='inner'><ul>
			<li><a href='<%= webBase %>/index.do' class='on'>평택시의회</a></li>
			<li><a href='javascript:void(0);'>상임위원회</a>
				<div class='top_link_sub'><ul>
					<li><a href='<%= webBase %>/oper/index.do'>의회운영위원회</a></li>
					<li><a href='<%= webBase %>/gvadm/index.do'>기획행정위원회</a></li>
					<li><a href='<%= webBase %>/welfare/index.do'>복지환경위원회</a></li>
					<li><a href='<%= webBase %>/build/index.do'>산업건설위원회</a></li>
				</ul></div>
			</li>
			<li><a href='<%= webBase %>/youth/index.do'>청소년의회</a></li>
		</ul></div>
	</div>
	<!-- Base Header -->
	<div id='header'>
		<div class='search_top'><div class='inner'>
			<h1><a href='<%= webBase %>/index.do'><img src='<%= webBase %>/images/common/logo_base.png' alt='<%= webTitle %>'/></a></h1>
			<div class='zoom'>
                <div class="youtube-img"></div>
				<a href="https://www.youtube.com/%ED%8F%89%ED%83%9D%EC%8B%9C%EC%9D%98%ED%9A%8C" target="_blank"><img src='<%= webBase %>/images/icon/youtube.png' width='40px' alt='유튜브'/></a>&nbsp;
                <a href="https://www.facebook.com/pyeongtaekcouncil" target="_blank"><img src='<%= webBase %>/images/icon/facebook.png' width='40px' alt='페이스북'/></a>&nbsp;
				<a href="https://www.instagram.com/pt_council/?igshid=YmMyMTA2M2Y%3D" target="_blank"><img src='<%= webBase %>/images/icon/instagram.png' width='40px' alt='인스타그램'/></a>&nbsp;&nbsp;
			</div>
			<div class='search_form'>
				<label for='searchTopsText' class='hide'>검색어 입력</label>
				<div><input type='text' id='searchTopsText' name='searchTopsText' placeholder='검색어를 입력하세요.'/></div><%--
				<input type='image' src='<%= webBase %>/images/icon/ico_search01.png' onclick='fnTopsFinder("view")' alt='검색'/>--%>
				<img src='<%= webBase %>/images/icon/ico_search01.png' class='center vam' onclick='fnTopsFinder("view")' alt='검색' style='cursor:pointer;'/>
			</div>
			<div class='zoom'>
				<a href='javascript:void(0);' class='btn_plus'  onclick='zoomcontrol.zoomin();' ><img src='<%= webBase %>/images/icon/ico_plus.png'  alt='확대'/></a>
				<a href='javascript:void(0);' class='btn_minus' onclick='zoomcontrol.zoomout();'><img src='<%= webBase %>/images/icon/ico_minus.png' alt='축소'/></a>
			</div>
		</div></div>
		<div class='gnb_wrap'>
			<div class='gnb'>
				<div class='inner'>
					<ul><%= dispMenu.toString() %></ul>
				</div>
				<div class='bg_gnb_sub'></div>
			</div>
			<div class='fullmenu'>
				<a href='javascript:void(0);' class='open_fullmenu'><img src='<%= webBase %>/images/btns/btn_fullmenu.png' alt='전체메뉴'/></a>
				<div class='fullmenu_list'>
					<ul><%= dispFull.toString() %></ul>
				</div>
			</div>
		</div>
	</div>
	<!-- //Base Header -->
	<!-- //mobile Header -->
	<div id='mobile_header'>
		<a href='javascript:void(0);' class='open_mobile_menu'><img src='<%= webBase %>/images/btns/btn_mobile_menu.png' alt='전체메뉴'/></a>
		<h1><a href='<%= webBase %>/index.do'><img src='<%= webBase %>/images/common/logo_base.png' alt='<%= webTitle %>'/></a></h1>
		<a href='javascript:void(0);' class='open_mobile_search'><img src='<%= webBase %>/images/btns/btn_mobile_search.png' alt='검색'/></a>
		<div style="width:100%;text-align:right;padding-bottom:5px;">
		<a href="https://www.youtube.com/%ED%8F%89%ED%83%9D%EC%8B%9C%EC%9D%98%ED%9A%8C" target="_blank"><img src='<%= webBase %>/images/icon/youtube.png' width='40px' alt='유튜브'/></a>&nbsp;&nbsp;
		<a href="https://www.facebook.com/pyeongtaekcouncil" target="_blank"><img src='<%= webBase %>/images/icon/facebook.png' width='40px' alt='페이스북'/></a>&nbsp;&nbsp;
		<a href="https://www.instagram.com/pt_council/?igshid=YmMyMTA2M2Y%3D" target="_blank"><img src='<%= webBase %>/images/icon/instagram.png' width='40px' alt='인스타그램'/></a>&nbsp;&nbsp;&nbsp;
		</div>
		<div class='mobile_menu'>
			<div class='logo'><img src='<%= webBase %>/images/common/logo_base.png' alt='<%= webTitle %>'/></div>
			<a href='javascript:void(0);' class='close_mobile_menu'><img src='<%= webBase %>/images/btns/btn_close_mobile_gnb.png' alt='전체메뉴 닫기'/></a>
			<div class='mobile_menu_list'>
				<ul><%= dispMobi.toString() %></ul>
			</div>
		</div>
	</div>
	<!-- //mobile Header -->