<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<script>
	$(document).ready(function(){
		var menu3_url = ['/coun/acts/stand.do','/base/photoList.do','/coun/base/photoView.do','/coun/base/movieList.do','/coun/cost/reportList.do','/coun/cost/reportView.do','/coun/brief/reportList.do','/coun/brief/reportView.do','/coun/resr/reportList.do','/coun/resr/reportView.do','/coun/schd/monthList.do','/coun/acts/program.do','/coun/hold/reportList.do','/coun/hold/reportView.do']
		var menu4_url = ['/coun/cast/live.do','/coun/stand/movieList.do']
        var path = window.location.pathname;
        if(path.includes('/coun/intro')) {
			$(".menu1").removeClass("current");
            $(".menu1").addClass("current_on");
        }
		if(path.includes('/coun/memb')) {
			$(".menu2").removeClass("current");
			$(".menu2").addClass("current_on");
		}
		menu3_url.forEach(function(item) {
			if(path.includes(item)) {
				$(".menu3").removeClass("current");
				$(".menu3").addClass("current_on");
			}
		});
		menu4_url.forEach(function(item) {
			if(path.includes(item)) {
				$(".menu4").removeClass("current");
				$(".menu4").addClass("current_on");
			}
		});
	});
</script>

<div id="top">
	<div class="inner">
		<ul class="top_link">
			<li><a href='/' class='on'>평택시의회</a></li>
			<li><a href='#' class="sangim">상임위원회</a>
				<ul class='top_link_sub'>
					<li><a href="/oper/index.do">의회운영위원회</a></li>
					<li><a href="/gvadm/index.do">기획행정위원회</a></li>
					<li><a href="/welfare/index.do">복지환경위원회</a></li>
					<li><a href="/build/index.do">산업건설위원회</a></li>
				</ul>
			</li>
            <li><a href='#' class="special">특별의원회</a>
				<ul class='top_link_sub'>
					<li><a href="/oper/index.do">윤리특별위원회</a></li>
					<li><a href="/gvadm/index.do">예산결산특별위원회</a></li>
				</ul>
			</li>
            <li><a href="/youth/index.do" class="teen">청소년의회</a></li>
			<li><a href="https://www.pyeongtaek.go.kr" class="ptcity" target="_blank"><img src="/images/ptcity_logo.png" title="평택시청마크"/> 평택시청</a></li>
		</ul>
	</div>
</div>
<div id="header">
	<div class="inner">
		<h1 class="logo"> <a href="/"> <img src="/images/ptcouncil_logo.png" alt="의회마크" /> <span> <p> 평택시의회 </p> <span> PYEONGTAEK CITY COUNCIL </span> </span> </a> </h1>
		<ul id="sns">
			<li> <a href="https://www.facebook.com/pyeongtaekcouncil" class="fb" target="_blank" title="평택시의회 페이스북(새창열림)">평택시의회 페이스북</a> </li>
			<li> <a href="https://www.youtube.com/@pyeongtaekcouncil" class="yt" target="_blank" title="평택시의회 유튜브(새창열림)">평택시의회 유튜브</a> </li>
			<li> <a href="https://www.instagram.com/pt_council/?igshid=YmMyMTA2M2Y%3D" class="inst" target="_blank" title="평택시의회 인스타그램(새창열림)">평택시의회 인스타그램</a> </li>
			<li> <a href="https://blog.naver.com/pt_council" class="blog" target="_blank" title="평택시의회 블로그(새창열림)">평택시의회 블로그</a> </li>
		</ul>
		<div id="search"> <a href="#search" class="btn_search">검색 열기</a>
			<form name="searchform" method="post">
				<input type="hidden" name="mode" value="process">
				<fieldset>
					<legend>통합검색</legend>
					<div class="in_sch">
						<h2 class="m">SEARCH</h2>
						<div class="in_txt">
							<input type="text" id='searchTopsText' name='searchTopsText' placeholder="검색어를 입력해주세요" class="searchbar" onkeyup='fnKeyDownFinder("view")'/>
							<button type="button" class="btn_submit" onclick='fnTopsFinder("view")'>검색</button>
						</div>
					</div>
				</fieldset>
			</form>
		</div>

		<!-- 화면확대부분 -->
		<div id="zoom_control">
			<h2 class="skip">화면크기조절</h2>
			<ul>
				<li class="zoomin"> <a href="#zoomIn" id="zoomIn" onclick="javascript:zoomIn();">화면크기를 한단계 크게</a> </li>
				<li class="current"> <a href="#resetZoom" id="resetZoom" onclick="javascript:zoomReset();" title="화면크기 100%로 변경">가</a> </li>
				<li class="zoomout"> <a href="#zoomOut" id="zoomOut" onclick="javascript:zoomOut();">화면크기를 한단계 작게</a> </li>
			</ul>
		</div>
	</div>
</div>

<!-- mobile menu -->
<div class="m_menu m">
	<div class="m_top">
		<button type="button" class="btn_menu_close">
			<span class="skip">전체메뉴 닫기</span> <span class="bar bar1"></span> <span class="bar bar2"></span>
		</button>
		<h1 class="logo">
			<a href="/">
				<img src="/images/ptcouncil_logo.png" alt="의회마크" />
				<span>
					<strong>
						평택시의회
					</strong>
					<span>
						PYEONGTAEK CITY COUNCIL
					</span>
				</span>
			</a>
		</h1>
	</div>
	<ul >
		<li class="menu1 current"> <a href="/coun/intro/greetings.do" title="의회안내">의회안내</a>
			<div class="menu">
				<ul>
					<li class="current"> <a href="/coun/intro/greetings.do" title="의장인사말">의장인사말</a> </li>
					<li class="current"> <a href="/coun/intro/history.do" title="의회연혁">의회연혁</a> </li>
					<li class="current"> <a href="/coun/intro/organization.do" title="조직 및 구성">조직 및 구성</a>
						<ul>
							<li class="current"> <a href="/coun/intro/organization.do" title="조직 및 구성">조직 및 구성</a> </li>
							<li class="current"> <a href="/coun/intro/phone/PageView.do" title="전화번호">전화번호</a> </li>
						</ul>
					</li>
					<li class="current"> <a href="/coun/intro/function.do" title="의회기능">의회기능</a>
						<ul>
							<li class="current"> <a href="/coun/intro/function.do" title="의회기능">의회기능</a> </li>
							<li class="current"> <a href="/coun/intro/authority.do" title="의회권한">의회권한</a> </li>
							<li class="current"> <a href="/coun/intro/convoke.do" title="회기소집 및 운영">회기소집 및 운영</a> </li>
							<li class="current"> <a href="/coun/intro/process.do" title="의안 처리절차">의안처리절차</a> </li>
							<li class="current"> <a href="/coun/intro/executive.do" title="행정사무감사/조사">행정사무감사/조사</a> </li>
							<li class="current"> <a href="/coun/intro/estimate.do" title="예산 및 결산">예산 및 결산</a> </li>
							<li class="current"> <a href="/coun/intro/petition.do" title="청원/진정 처리절차">청원/진정 처리절차</a> </li>
						</ul>
					</li>
					<li class="current"> <a href="/coun/intro/operate.do" title="의회운영">의회운영</a> </li>
					<li class="current"> <a href="/coun/intro/watch.do" title="방청안내">방청안내</a>
						<ul>
							<li class="current"> <a href="/coun/intro/watch.do" title="방청안내">방청안내</a> </li>
							<li class="current"> <a href="/coun/intro/visitRequ.do" title="온라인신청">온라인신청</a> </li>
							<li class="current"> <a href="/coun/intro/visitList.do" title="신청확인">신청확인</a> </li>
						</ul>
					</li>
					<li class="current"> <a href="/coun/intro/location.do" title="오시는길">오시는길</a> </li>
				</ul>
			</div>
		</li>
		<li class="menu2 current"> <a href="/coun/memb/activeName.do" title="의원소개">의원소개</a>
			<div class="menu">
				<ul>
					<li class="current"> <a href="/coun/memb/activeName.do" title="현역의원">현역의원</a> </li>
					<li class="current"> <a href="/coun/memb/activeChair.do" title="의장단·상임위원장">의장단·상임위원장</a> </li>
					<li class="current"> <a href="/coun/memb/activeFormer.do" title="역대의원">역대의원</a> </li>
					<li class="current"> <a href="/coun/memb/activeFinder.do" title="의원검색">의원검색</a> </li>
					<li class="current"> <a href="/coun/memb/ethics.do" title="의원윤리강령">의원윤리강령</a> </li>
				</ul>
			</div>
		</li>
		<li class="menu3 current"> <a href="/coun/acts/stand.do" title="의정활동">의정활동</a>
			<div class="menu">
				<ul>
					<li class="current"> <a href="/coun/acts/stand.do" title="상임위원회">상임위원회</a> </li>
					<li class="current"> <a href="/coun/base/photoList.do" title="포토 의정활동">포토 의정활동</a> </li>
					<li class="current"> <a href="/coun/base/movieList.do" title="홍보영상">홍보영상</a> </li>
					<li class="current"> <a href="/coun/cost/reportList.do" title="업무추진비 공개">업무추진비 공개</a> </li>
					<li class="current"> <a href="/coun/brief/reportList.do" title="국내외 연수보고서">국내외 연수보고서</a> </li>
					<li class="current"> <a href="/coun/resr/reportList.do" title="의원 연구단체">의원 연구단체</a> </li>
					<li class="current"> <a href="/coun/schd/monthList.do" title="의사일정">의사일정</a>
						<ul>
							<li class="current"> <a href="/coun/schd/monthList.do" title="월간일정">월간일정</a> </li>
							<li class="current"> <a href="/coun/acts/program.do" title="연간회기일정">연간회기일정</a> </li>
						</ul>
					</li>
					<li class="current"> <a href="/coun/hold/reportList.do" title="의원겸직현황">의원겸직현황</a> </li>
				</ul>
			</div>
		</li>
		<li class="menu4 current"> <a href="/coun/cast/live.do" title="인터넷방송" target='_self'>인터넷방송</a>
			<div class="menu">
				<ul>
					<li class="current"> <a href="/coun/cast/live.do" title="생방송">생방송</a> </li>
					<li class="current"> <a href="/coun/stand/movieList.do?ct=1" title="다시보기">다시보기</a>
						<ul>
							<li class="current"> <a href="/coun/stand/movieList.do?ct=1" title="본회의">본회의</a> </li>
							<li class="current"> <a href="/coun/stand/movieList.do?ct=2" title="의회운영위원회">의회운영위원회</a> </li>
							<li class="current"> <a href="/coun/stand/movieList.do?ct=3" title="기획행정위원회">기획행정위원회</a> </li>
							<li class="current"> <a href="/coun/stand/movieList.do?ct=4" title="복지환경위원회">복지환경위원회</a> </li>
							<li class="current"> <a href="/coun/stand/movieList.do?ct=5" title="산업건설위원회">산업건설위원회</a> </li>
							<li class="current"> <a href="/coun/stand/movieList.do?ct=6" title="특별위원회">특별위원회</a> </li>
						</ul>
					</li>
				</ul>
			</div>
		</li>
		<li class="menu5 current"> <a href="/coun/past/minutesList.do" title="회의록검색" target='_self'>회의록검색</a>
			<div class="menu">
				<ul>
					<li class="current"> <a href="/coun/past/minutesList.do" title="최근 회의록" target='_self'>최근 회의록</a> </li>
					<li class="current"> <a href="/coun/sess/minutesList.do" title="단순검색" target='_self'>단순검색</a>
						<ul>
							<li class="current"> <a href="/coun/sess/minutesList.do" title="회기별검색">회기별검색</a> </li>
							<li class="current"> <a href="/coun/meet/minutesList.do" title="회의별검색">회의별검색</a> </li>
							<li class="current"> <a href="/coun/year/minutesList.do" title="연도별검색">연도별검색</a> </li>
						</ul>
					</li>
					<li class="current"> <a href="/coun/tail/minutesList.do" title="상세검색" target='_self'>상세검색</a> </li>
					<li class="current"> <a href="/coun/gover/reportList.do" title="시정질문답변" target='_self'>시정질문답변</a> </li>
					<li class="current"> <a href="/coun/spch/reportList.do" title="7분자유발언" target='_self'>7분자유발언</a> </li>
					<li class="current"> <a href="/coun/find/agendaList.do" title="의안정보" target='_self'>의안정보</a>
						<ul>
							<li class="current"> <a href="/coun/find/agendaList.do" title="의안검색">의안검색</a> </li>
							<li class="current"> <a href="/coun/info/agendaList.do" title="의안정보">의안정보</a> </li>
							<li class="current"> <a href="/coun/item/agendaList.do" title="의안항목">의안항목</a> </li>
							<li class="current"> <a href="/coun/kind/agendaStat.do" title="의안통계">의안통계</a> </li>
						</ul>
					</li>
					<li class="current"> <a href="/coun/days/minutesStat.do" title="회의통계" target='_self'>회의통계</a> </li>
					<li class="current"> <a href="/coun/cast/general.do?ct=1" title="영상회의록" target='_self'>영상회의록</a>
						<ul>
							<li class="current"> <a href="/coun/cast/general.do?ct=1" title="본회의">본회의</a> </li>
							<li class="current"> <a href="/coun/cast/general.do?ct=2" title="의회운영위원회">의회운영위원회</a> </li>
							<li class="current"> <a href="/coun/cast/general.do?ct=3" title="기획행정위원회">기획행정위원회</a> </li>
							<li class="current"> <a href="/coun/cast/general.do?ct=4" title="복지환경위원회">복지환경위원회</a> </li>
							<li class="current"> <a href="/coun/cast/general.do?ct=5" title="산업건설위원회">산업건설위원회</a> </li>
							<li class="current"> <a href="/coun/cast/general.do?ct=6" title="예산결산특별위원회">예산결산특별위원회</a> </li>
						</ul>
					</li>
				</ul>
			</div>
		</li>
		<li class="menu6 current"> <a href="/coun/base/claim.do" title="의안">열린마당</a>
			<div class="menu">
				<ul>
					<li class="current"> <a href="/coun/base/claim.do" title="민원안내">민원안내</a> </li>
					<li class="current"> <a href="/coun/hope/noticeList.do" title="의회에 바란다">의회에 바란다</a> </li>
					<li class="current"> <a href="/coun/tip/noticeList.do" title="행정사무감사 시민제보">행정사무감사 시민제보</a> </li>
					<li class="current"> <a href="/coun/base/propOrdinance.do" title="주민조례청구">주민조례청구</a> </li>
					<li class="current"> <a href="/coun/base/search.do" title="통합검색">통합검색</a> </li>
				</ul>
			</div>
		</li>
		<li class="menu7 current"> <a href="/coun/base/noticeList.do" title="알림마당">알림마당</a>
			<div class="menu">
				<ul>
					<li class="current"> <a href="/coun/base/noticeList.do" title="공지사항">공지사항</a> </li>
					<li class="current"> <a href="/coun/laws/reportList.do" title="입법예고">입법예고</a> </li>
					<li class="current"> <a href="/coun/news/noticeList.do" title="의회소식지">의회소식지</a>
						<ul>
							<li class="current"> <a href="/coun/news/noticeList.do" title="의회소식지">의회소식지</a> </li>
							<li class="current"> <a href="/coun/news/noticeInst.do" title="의회소식지 신청">의회소식지 신청</a> </li>

						</ul>
					</li>
					<li class="current"> <a href="/coun/press/reportList.do" title="보도자료">보도자료</a> </li>
					<li class="current"> <a href="/coun/interview/reportList.do" title="의원인터뷰">의원인터뷰</a> </li>
					<li class="current"> <a href="//www.elis.go.kr/newlaib/laibLaws/h1126/laws.jsp?regionId=41220" target="_blank" title="자치법규">자치법규</a> </li>
					<li class="current"> <a href="/webdata/index.do" target="_blank" title="자료실">자료실</a> </li>
					<li class="current"> <a href="/coun/base/counTour.do" title="청사 견학">청사 견학</a> </li>
				</ul>
			</div>
		</li>
	</ul>
</div>
<!-- //mobile menu end -->


<!-- 메인메뉴 -->
<div id="menuArea" class="w">
	<div class="inner">
		<h2 class="skip">메인메뉴</h2>
		<ul id="topmenu">
			<li class="menu1 current"> <a href="/coun/intro/greetings.do" title="의회안내">의회안내</a>
				<div class="menu">
					<ul>
						<li class="current"> <a href="/coun/intro/greetings.do" title="의장인사말">의장인사말</a> </li>
						<li class="current"> <a href="/coun/intro/history.do" title="의회연혁">의회연혁</a> </li>
						<li class="current"> <a href="/coun/intro/organization.do" title="조직 및 구성">조직 및 구성</a>
							<ul>
								<li class="current"> <a href="/coun/intro/organization.do" title="조직 및 구성">조직 및 구성</a> </li>
								<li class="current"> <a href="/coun/intro/phone/PageView.do" title="전화번호">전화번호</a> </li>
							</ul>
						</li>
						<li class="current"> <a href="/coun/intro/function.do" title="의회기능">의회기능</a>
							<ul>
								<li class="current"> <a href="/coun/intro/function.do" title="의회기능">의회기능</a> </li>
								<li class="current"> <a href="/coun/intro/authority.do" title="의회기능">의회기능</a> </li>
								<li class="current"> <a href="/coun/intro/convoke.do" title="회기소집 및 운영">회기소집 및 운영</a> </li>
								<li class="current"> <a href="/coun/intro/process.do" title="의안 처리절차">의안처리절차</a> </li>
								<li class="current"> <a href="/coun/intro/executive.do" title="행정사무감사/조사">행정사무감사/조사</a> </li>
								<li class="current"> <a href="/coun/intro/estimate.do" title="예산 및 결산">예산 및 결산</a> </li>
								<li class="current"> <a href="/coun/intro/petition.do" title="청원/진정 처리절차">청원/진정 처리절차</a> </li>
							</ul>
						</li>
						<li class="current"> <a href="/coun/intro/operate.do" title="의회운영">의회운영</a> </li>
						<li class="current"> <a href="/coun/intro/watch.do" title="방청안내">방청안내</a>
							<ul>
								<li class="current"> <a href="/coun/intro/watch.do" title="방청안내">방청안내</a> </li>
								<li class="current"> <a href="/coun/intro/visitRequ.do" title="온라인신청">온라인신청</a> </li>
								<li class="current"> <a href="/coun/intro/visitList.do" title="신청확인">신청확인</a> </li>
							</ul>
						</li>
						<li class="current"> <a href="/coun/intro/location.do" title="오시는길">오시는길</a> </li>
					</ul>
				</div>
			</li>
			<li class="menu2 current"> <a href="/coun/memb/activeName.do" title="의원소개">의원소개</a>
				<div class="menu">
					<ul>
						<li class="current"> <a href="/coun/memb/activeName.do" title="현역의원">현역의원</a> </li>
						<li class="current"> <a href="/coun/memb/activeChair.do" title="의장단·상임위원장">의장단·상임위원장</a> </li>
						<li class="current"> <a href="/coun/memb/activeFormer.do" title="역대의원">역대의원</a> </li>
						<li class="current"> <a href="/coun/memb/activeFinder.do" title="의원검색">의원검색</a> </li>
						<li class="current"> <a href="/coun/memb/ethics.do" title="의원윤리강령">의원윤리강령</a> </li>
					</ul>
				</div>
			</li>
			<li class="menu3 current"> <a href="/coun/acts/stand.do" title="의정활동">의정활동</a>
				<div class="menu">
					<ul>
						<li class="current"> <a href="/coun/acts/stand.do" title="상임위원회">상임위원회</a> </li>
						<li class="current"> <a href="/coun/base/photoList.do" title="포토 의정활동">포토 의정활동</a> </li>
						<li class="current"> <a href="/coun/base/movieList.do" title="홍보영상">홍보영상</a> </li>
						<li class="current"> <a href="/coun/cost/reportList.do" title="업무추진비 공개">업무추진비 공개</a> </li>
						<li class="current"> <a href="/coun/brief/reportList.do" title="국내외 연수보고서">국내외 연수보고서</a> </li>
						<li class="current"> <a href="/coun/resr/reportList.do" title="의원 연구단체">의원 연구단체</a> </li>
						<li class="current"> <a href="/coun/schd/monthList.do" title="의사일정">의사일정</a>
							<ul>
								<li class="current"> <a href="/coun/schd/monthList.do" title="월간일정">월간일정</a> </li>
								<li class="current"> <a href="/coun/acts/program.do" title="연간회기일정">연간회기일정</a> </li>
							</ul>
						</li>
						<li class="current"> <a href="/coun/hold/reportList.do" title="의원겸직현황">의원겸직현황</a> </li>
					</ul>
				</div>
			</li>
			<li class="menu4 current"> <a href="/coun/cast/live.do" title="인터넷방송" target='_self'>인터넷방송</a>
				<div class="menu">
					<ul>
						<li class="current"> <a href="/coun/cast/live.do" title="생방송">생방송</a> </li>
						<li class="current"> <a href="/coun/stand/movieList.do?ct=1" title="다시보기">다시보기</a>
							<ul>
								<li class="current"> <a href="/coun/stand/movieList.do?ct=1" title="본회의">본회의</a> </li>
								<li class="current"> <a href="/coun/stand/movieList.do?ct=2" title="의회운영위원회">의회운영위원회</a> </li>
								<li class="current"> <a href="/coun/stand/movieList.do?ct=3" title="기획행정위원회">기획행정위원회</a> </li>
								<li class="current"> <a href="/coun/stand/movieList.do?ct=4" title="복지환경위원회">복지환경위원회</a> </li>
								<li class="current"> <a href="/coun/stand/movieList.do?ct=5" title="산업건설위원회">산업건설위원회</a> </li>
								<li class="current"> <a href="/coun/stand/movieList.do?ct=6" title="특별위원회">특별위원회</a> </li>
							</ul>
						</li>
					</ul>
				</div>
			</li>
			<li class="menu5 current"> <a href="/coun/past/minutesList.do" title="회의록검색" target='_self'>회의록검색</a>
				<div class="menu">
					<ul>
						<li class="current"> <a href="/coun/past/minutesList.do" title="최근 회의록" target='_self'>최근 회의록</a> </li>
						<li class="current"> <a href="/coun/sess/minutesList.do" title="단순검색" target='_self'>단순검색</a>
							<ul>
								<li class="current"> <a href="/coun/sess/minutesList.do" title="회기별검색">회기별검색</a> </li>
								<li class="current"> <a href="/coun/meet/minutesList.do" title="회의별검색">회의별검색</a> </li>
								<li class="current"> <a href="/coun/year/minutesList.do" title="연도별검색">연도별검색</a> </li>
							</ul>
						</li>
						<li class="current"> <a href="/coun/tail/minutesList.do" title="상세검색" target='_self'>상세검색</a> </li>
						<li class="current"> <a href="/coun/gover/reportList.do" title="시정질문답변" target='_self'>시정질문답변</a> </li>
						<li class="current"> <a href="/coun/spch/reportList.do" title="7분자유발언" target='_self'>7분자유발언</a> </li>
						<li class="current"> <a href="/coun/find/agendaList.do" title="의안정보" target='_self'>의안정보</a>
							<ul>
								<li class="current"> <a href="/coun/find/agendaList.do" title="의안검색">의안검색</a> </li>
								<li class="current"> <a href="/coun/info/agendaList.do" title="의안정보">의안정보</a> </li>
								<li class="current"> <a href="/coun/item/agendaList.do" title="의안항목">의안항목</a> </li>
								<li class="current"> <a href="/coun/kind/agendaStat.do" title="의안통계">의안통계</a> </li>
							</ul>
						</li>
						<li class="current"> <a href="/coun/days/minutesStat.do" title="회의통계" target='_self'>회의통계</a> </li>
						<li class="current"> <a href="/coun/cast/general.do?ct=1" title="영상회의록" target='_self'>영상회의록</a>
							<ul>
								<li class="current"> <a href="/coun/cast/general.do?ct=1" title="본회의">본회의</a> </li>
								<li class="current"> <a href="/coun/cast/general.do?ct=2" title="의회운영위원회">의회운영위원회</a> </li>
								<li class="current"> <a href="/coun/cast/general.do?ct=3" title="기획행정위원회">기획행정위원회</a> </li>
								<li class="current"> <a href="/coun/cast/general.do?ct=4" title="복지환경위원회">복지환경위원회</a> </li>
								<li class="current"> <a href="/coun/cast/general.do?ct=5" title="산업건설위원회">산업건설위원회</a> </li>
								<li class="current"> <a href="/coun/cast/general.do?ct=6" title="예산결산특별위원회">예산결산특별위원회</a> </li>
							</ul>
						</li>
					</ul>
				</div>
			</li>
			<li class="menu6 current"> <a href="/coun/base/claim.do" title="의안">열린마당</a>
				<div class="menu">
					<ul>
						<li class="current"> <a href="/coun/base/claim.do" title="민원안내">민원안내</a> </li>
						<li class="current"> <a href="/coun/hope/noticeList.do" title="의회에 바란다">의회에 바란다</a> </li>
						<li class="current"> <a href="/coun/tip/noticeList.do" title="행정사무감사">행정사무감사</a> </li>
						<li class="current"> <a href="/coun/base/propOrdinance.do" title="주민조례청구">주민조례청구</a> </li>
						<li class="current"> <a href="/coun/base/search.do" title="통합검색">통합검색</a> </li>
					</ul>
				</div>
			</li>
			<li class="menu7 current"> <a href="/coun/base/noticeList.do" title="알림마당">알림마당</a>
				<div class="menu">
					<ul>
						<li class="current"> <a href="/coun/base/noticeList.do" title="공지사항">공지사항</a> </li>
						<li class="current"> <a href="/coun/laws/reportList.do" title="입법예고">입법예고</a> </li>
						<li class="current"> <a href="/coun/news/noticeList.do" title="의회소식지">의회소식지</a>
							<ul>
								<li class="current"> <a href="/coun/news/noticeList.do" title="의회소식지">의회소식지</a> </li>
								<li class="current"> <a href="/coun/news/noticeInst.do" title="의회소식지 신청">의회소식지 신청</a> </li>

							</ul>
						</li>
						<li class="current"> <a href="/coun/press/reportList.do" title="보도자료">보도자료</a> </li>
						<li class="current"> <a href="/coun/interview/reportList.do" title="의원인터뷰">의원인터뷰</a> </li>
						<li class="current"> <a href="//www.elis.go.kr/newlaib/laibLaws/h1126/laws.jsp?regionId=41220" target="_blank" title="자치법규">자치법규</a> </li>
						<li class="current"> <a href="/webdata/index.do" target="_blank" title="자료실">자료실</a> </li>
						<li class="current"> <a href="/coun/base/counTour.do" title="청사 견학">청사 견학</a> </li>
					</ul>
				</div>
			</li>
		</ul>
	</div>
	<hr />
</div>
<button type="button" class="btn_menu">
	<span class="bar bar1"></span>
	<span class="bar bar2"></span>
	<span class="bar bar3"></span> <span>전체메뉴</span>
</button>

<!-- //메인메뉴 end -->