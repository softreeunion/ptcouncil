<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>

<h2 class="skip">copyright 저작권정보</h2>
<div id="footer">

	<div class="linksite">
		<ul class="inner">

			<li><a href="#linksite01">전국 시·도 의회</a>
				<div id="linksite01">
					<ul>
						<li><a href="http://www.smc.seoul.kr" target="_blank" rel="noopener noreferrer" title="서울특별시의회(새창열림)">서울특별시의회</a></li>
						<li><a href="http://council.busan.go.kr" target="_blank" rel="noopener noreferrer" title="부산광역시의회(새창열림)">부산광역시의회</a></li>
						<li><a href="http://council.daegu.go.kr" target="_blank" rel="noopener noreferrer" title="대구광역시의회(새창열림)">대구광역시의회</a></li>
						<li><a href="http://www.icouncil.go.kr" target="_blank" rel="noopener noreferrer" title="인천광역시의회(새창열림)">인천광역시의회</a></li>
						<li><a href="http://council.daejeon.go.kr" target="_blank" rel="noopener noreferrer" title="대전광역시의회(새창열림)">대전광역시의회</a></li>
						<li><a href="http://www.council.ulsan.kr" target="_blank" rel="noopener noreferrer" title="울산광역시의회(새창열림)">울산광역시의회</a></li>
						<li><a href="http://council.gwangju.go.kr" target="_blank" rel="noopener noreferrer" title="광주광역시의회(새창열림)">광주광역시의회</a></li>
						<li><a href="http://www.ggc.go.kr" target="_blank" rel="noopener noreferrer" title="경기도의회(새창열림)">경기도의회</a></li>
						<li><a href="http://council.gb.go.kr" target="_blank" rel="noopener noreferrer" title="경상북도의회(새창열림)">경상북도의회</a></li>
						<li><a href="http://council.gyeongnam.go.kr" target="_blank" rel="noopener noreferrer" title="경상남도의회(새창열림)">경상남도의회</a></li>
						<li><a href="http://council.gangwon.kr" target="_blank" rel="noopener noreferrer" title="강원특별자치도의회(새창열림)">강원특별자치도의회</a></li>
						<li><a href="http://council.chungbuk.kr" target="_blank" rel="noopener noreferrer" title="충청북도의회(새창열림)">충청북도의회</a></li>
						<li><a href="http://council.chungnam.go.kr" target="_blank" rel="noopener noreferrer" title="충청남도의회(새창열림)">충청남도의회</a></li>
						<li><a href="http://www.jbstatecouncil.jeonbuk.kr/jbassem/main" target="_blank" rel="noopener noreferrer" title="전북특별자치도의회(새창열림)">전북특별자치도의회</a></li>
						<li><a href="http://www.jnassembly.go.kr" target="_blank" rel="noopener noreferrer" title="전라남도의회(새창열림)">전라남도의회</a></li>
						<li><a href="http://council.sejong.go.kr" target="_blank" rel="noopener noreferrer" title="세종특별자치시의회(새창열림)">세종특별자치시의회</a></li>
						<li><a href="http://www.council.jeju.kr" target="_blank" rel="noopener noreferrer" title="제주특별자치도의회(새창열림)">제주특별자치도의회</a></li>
					</ul>
				</div>
			</li>

			<li><a href="#linksite02">경기도 시·구 의회</a>
				<div id="linksite02">
					<ul>
						<li><a href='http://www.gpassem.go.kr/' target='_blank'>가평군의회</a></li>
						<li><a href='http://www.goyangcouncil.go.kr' target='_blank'>고양시의회</a></li>
						<li><a href='http://www.gccouncil.go.kr' target='_blank'>과천시의회</a></li>
						<li><a href='http://council.gm.go.kr' target='_blank'>광명시의회</a></li>
						<li><a href='http://gjcouncil.go.kr' target='_blank'>광주시의회</a></li>
						<li><a href='http://www.gcc.or.kr' target='_blank'>구리시의회</a></li>
						<li><a href='http://www.gunpocouncil.or.kr' target='_blank'>군포시의회</a></li>
						<li><a href='http://www.gimpo.go.kr/council/index.do' target='_blank'>김포시의회</a></li>
						<li><a href='http://www.nyjc.go.kr' target='_blank'>남양주시의회</a></li>
						<li><a href='http://council.ddc.go.kr' target='_blank'>동두천시의회</a></li>
						<li><a href='https://council.bucheon.go.kr' target='_blank'>부천시의회</a></li>
						<li><a href='https://www.sncouncil.go.kr' target='_blank'>성남시의회</a></li>
						<li><a href='http://council.suwon.go.kr' target='_blank'>수원시의회</a></li>
						<li><a href='https://council.siheung.go.kr' target='_blank'>시흥시의회</a></li>
						<li><a href='https://council.ansan.go.kr' target='_blank'>안산시의회</a></li>
						<li><a href='http://www.anseongcl.go.kr' target='_blank'>안성시의회</a></li>
						<li><a href='http://aycouncil.go.kr' target='_blank'>안양시의회</a></li>
						<li><a href='http://yjcc.yangju.go.kr' target='_blank'>양주시의회</a></li>
						<li><a href='https://www.ypccouncil.net' target='_blank'>양평군의회</a></li>
						<li><a href='http://www.yeojucouncil.go.kr' target='_blank'>여주시의회</a></li>
						<li><a href='https://www.yca21.go.kr' target='_blank'>연천군의회</a></li>
						<li><a href='https://www.osancouncil.go.kr' target='_blank'>오산시의회</a></li>
						<li><a href='http://www.iyongin.or.kr' target='_blank'>용인시의회</a></li>
						<li><a href='http://council.uiwang.go.kr' target='_blank'>의왕시의회</a></li>
						<li><a href='http://www.ujbcl.go.kr' target='_blank'>의정부시의회</a></li>
						<li><a href='http://council.icheon.go.kr' target='_blank'>이천시의회</a></li>
						<li><a href='http://www.pajucouncil.go.kr' target='_blank'>파주시의회</a></li>
						<li><a href='http://council.pocheon.go.kr' target='_blank'>포천시의회</a></li>
						<li><a href='http://council.hanam.go.kr' target='_blank'>하남시의회</a></li>
						<li><a href='http://council.hscity.go.kr' target='_blank'>화성시의회</a></li>
					</ul>
				</div>
			</li>

			<li><a href="#linksite03">유관기관</a>
				<div id="linksite03">
					<ul>
						<li><a href='http://www.gppc.or.kr' target='_blank'>경기평택항만공사</a></li>
						<li><a href='http://www.pt1318.kr' target='_blank'>평택시청소년상담복지센터</a></li>
						<li><a href='http://www.goept.kr' target='_blank'>경기도평택교육지원청</a></li>
						<li><a href='http://www.pyf.kr' target='_blank'>평택시청소년재단</a></li>
						<li><a href='http://www.ggpolice.go.kr/pt' target='_blank'>평택경찰서</a></li>
						<li><a href='http://www.ptsports.or.kr' target='_blank'>평택시체육회·평택시장애인체육회</a></li>
						<li><a href='http://www.puc.or.kr' target='_blank'>평택도시공사</a></li>
						<li><a href='http://www.peec.go.kr' target='_blank'>평택영어교육센터</a></li>
						<li><a href='http://www.ptmunhwa.or.kr' target='_blank'>평택문화원</a></li>
						<li><a href='http://www.pthappy2000.or.kr' target='_blank'>평택행복나눔본부</a></li>
						<li><a href='http://www.ptwf.or.kr' target='_blank'>평택복지재단</a></li>
						<li><a href='http://www.ptymca.com' target='_blank'>평택YMCA</a></li>
						<li><a href='http://119.gg.go.kr/pyeongtaek' target='_blank'>평택소방서</a></li>
						<li><a href='http://soriter.pyeongtaek.go.kr' target='_blank'>한국소리터</a></li>
						<li><a href='http://www.liveinkorea.kr/center/default.asp?pzt=ct&cc=pyeongtaeksi' target='_blank'>평택시건강가정·다문화가족지원센터</a></li>
						<li><a href='http://yesfez.gg.go.kr' target='_blank'>황해경제자유구역청</a></li>
						<li><a href='http://www.pief.or.kr' target='_blank'>평택시국제교류재단</a></li>
						<li><a href='http://www.moobong.kr' target='_blank'>평택시무봉산청소년수련원</a></li>
						<li><a href='http://www.ptsf.or.kr' target='_blank'>평택시장학재단</a></li>
						<li><a href='http://www.ptycc.kr' target='_blank'>평택시청소년문화센터</a></li>
					</ul>
				</div>
			</li>

		</ul>

	</div>
	<hr />


	<div class="inner info">
		<h2 class="logo">
			<img src="/images/ptcouncil_logo.png" alt="의회마크" />
			<span>
					<strong>평택시의회</strong>
					<span>PYEONGTAEK CITY COUNCIL</span>
				</span>
		</h2>
		<ul class="link">
			<li class="privacy"><a href="/coun/base/privacy.do" title="개인정보처리방침">개인정보처리방침</a></li>
			<li><a href="/coun/base/sitemap.do" title="사이트맵">사이트맵</a></li>
			<li><a href="/coun/intro/location.do" title="오시는길">오시는길</a></li>
			<li class="total" id='visitor'><span>오늘: 0명</span> 전체: 0</li>
		</ul>
		<address>(17730)경기도 평택시 경기대로 1366(서정동) <span>대표전화 : <a href="tel: 031-8024-7560">: 031-8024-7560</a>(의정팀) , <a href="tel: 031-8024-7570"> 031-8024-7570</a>(의사팀) ,  <a href="tel:  031-8024-7580"> 031-8024-7580</a>(홍보팀) , <a href="tel: 031-8024-7610">031-8024-7610</a>(정책지원팀) Fax : 031-8024-7569</address>
		<p class="copyright">Copyright PyeongTaek City Council. All Rights Reserved.</p>
	</div>

	<div style='display:none;'>
		<input style='visibility:hidden;width:0;'/><label for='' class='hide'>Input</label>
		<div id='baseDN-modal'  style='display:none;' title='기본 프로그레스'><div id='progressBarLD' style='width:100%;height:22px;margin-top:38px;'></div></div>
		<div id='fileDN-modal'  style='display:none;' title='파일 다운로드'><div id='progressBarDN' style='width:100%;height:22px;margin-top:38px;'></div></div>
		<div id='errorDN-modal' style='display:none;' title='오류'><p style='margin-top:20px;'>엑셀파일 생성이 실패하였습니다.<br><br>관리자에게 문의바랍니다.</p></div>
		<div id='fileUP-modal'  style='display:none;' title='파일 업로드'><div id='progressBarUP' style='width:100%;height:22px;margin-top:38px;'></div></div>
		<div id='errorUP-modal' style='display:none;' title='오류'><p style='margin-top:20px;'>엑셀파일 처리가 실패하였습니다.<br><br>관리자에게 문의바랍니다.</p></div>
		<div id='progress'      class='progress'      title='파일 업로드'><div class='progress-bar progress-bar-success'></div></div>
	</div>
</div>