<%
////////////////////////////////////////////////////////////
// Program ID  : index
// Description : 시의회 INDEX
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	boolean viewPoster			= true;
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg.jspf"/>
	<script>
	$(document).ready(function(){
		$(".main_visual>ul").bxSlider({auto:true});
		$("#main_popup1 .mlay_clbt").click(function(){if($("input[name='chkday1']").is(":checked"))setCookieMobile("toCook1","done",1);$("#main_popup1").hide();});
		// $("#main_popup2 .mlay_clbt").click(function(){if($("input[name='chkday2']").is(":checked"))setCookieMobile("toCook2","done",1);$("#main_popup2").hide();});
		// $("#main_popup3 .mlay_clbt").click(function(){if($("input[name='chkday3']").is(":checked"))setCookieMobile("toCook3","done",1);$("#main_popup3").hide();});
//		$("#main_popup4 .mlay_clbt").click(function(){if($("input[name='chkday4']").is(":checked"))setCookieMobile("toCook4","done",1);$("#main_popup4").hide();});


// 조건절도 같이 수정
// 		$(".mlay_clbt").click(function(){if(!$("#main_popup1").is(":visible")&&!$("#main_popup2").is(":visible")&&!$("#main_popup3").is(":visible"))$("#mask").hide();});
// 		$(".mlay_clbt").click(function(){if(!$("#main_popup1").is(":visible")&&!$("#main_popup2").is(":visible"))$("#mask").hide();});
		$(".mlay_clbt").click(function(){if(!$("#main_popup1").is(":visible"))$("#mask").hide();});
//
		if(getCookieMobile("toCook1")==""){$("#main_popup1").show();$("#mask").show();};
		// if(getCookieMobile("toCook2")==""){$("#main_popup2").show();$("#mask").show();};
// 		if(getCookieMobile("toCook3")==""){$("#main_popup3").show();$("#mask").show();};
// 		if(getCookieMobile("toCook4")==""){$("#main_popup4").show();$("#mask").show();};
	});
	var fnMembFinder    = function(){
		doActLoadShow();
		var $form=$("#frmDetail");
		var  view=$("#searchMembText").val();
		if($form.length<=0){$form=$("<form/>").attr({id:"frmDetail",name:"frmDetail",method:"post"});$(document.body).append($form);}
		if($form.length<=1){$form.empty();}
		$("<input/>").attr({type:"hidden",id:"pageCurNo",name:"pageCurNo",value:"1"}).appendTo($form);
		$("<input/>").attr({type:"hidden",id:"reEraCode",name:"reEraCode",value:"<%= BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsCate" ) %>"}).appendTo($form);
		$("<input/>").attr({type:"hidden",id:"condType",name:"condType",value:"N"}).appendTo($form);
		$("<input/>").attr({type:"hidden",id:"condValue",name:"condValue",value:view}).appendTo($form);
		if($form.length<=1){$form.attr({"action":"<%= webBase %>/coun/memb/activeFinder.do","method":"post","target":"_self"}).submit();}
	};
	var fnActDetail     = function(view,acts){
		doActLoadShow();
		var $form=$("#frmDetail");
		if($form.length<=0){$form=$("<form/>").attr({id:"frmDetail",name:"frmDetail",method:"post"});$(document.body).append($form);}
		if($form.length<=1){$form.empty();}
		if(!isEmpty(view)){$("<input/>").attr({type:"hidden",id:"viewNo",name:"viewNo",value:view}).appendTo($form);}
		if($form.length<=1){$form.attr({"action":"<%= webBase %>/coun/"+acts+".do","method":"post","target":"_self"}).submit();}
	};<% if( movieLayer ) { %>
	var fnActMedia      = function(view,acts){
		$.ajax({
			type          : "post", 
			url           : "<%= webBase %>/coun/mediaView.do", 
			dataType      : "html", 
			data          : {viewNo:view}, 
			success       : function(data,status,xhr  ){var $LV=$("#layVideo");$LV.empty();$LV.html(data);media_layer();}, 
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);}
		}); 
	};<% } %>
	</script>
	<style type="text/css">
	.video {
		width: 100%; 
		height: 365px;
		overflow: hidden; 
		display: flex; 
		justify-content: center; 
		align-items: center;
  	}
	video[poster]{ 
		height:100%;
		width:100%;
    }
	</style>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/coun/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<div class='main_visual' id='contents'><ul><c:if test="${!empty mainImage}"><c:forEach var="rult" varStatus="status" items="${mainImage}"><c:choose><c:when test="${'F' eq rult.bbsSiteType}">
			<li><% if( imgMode ) { %><img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/><% } else { %><img src='<%= webBase + uploadPath %>${rult.fileAbs}' alt='${rult.bbsTitle}'/><% } %></li></c:when><c:otherwise>
			<li><a href='${rult.bbsSiteUri}'<c:if test="${'N' eq rult.bbsSiteType}"> target='_blank'</c:if>><% if( imgMode ) { %><img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/><% } else { %><img src='<%= webBase + uploadPath %>${rult.fileAbs}' alt='${rult.bbsTitle}'/><% } %></a></li></c:otherwise></c:choose></c:forEach></c:if>
		</ul></div>
		<div class='main_section01'>
			<div class='inner'><ul>
				<li><a href='<%= webBase + counEList[ 5][4] %>'><img src='<%= webBase %>/images/main/coun_icon01.png' alt='회의록 검색'/></a></li>
				<li><a href='<%= webBase + counBList[ 0][4] %>'><img src='<%= webBase %>/images/main/coun_icon02.png' alt='의원정보'/></a></li>
				<li><a href='<%= webBase + counGList[ 3][4] %>'><img src='<%= webBase %>/images/main/coun_icon03.png' alt='언론보도'/></a></li>
				<li><a href='<%= webBase + counGList[ 2][4] %>'><img src='<%= webBase %>/images/main/coun_icon04.png' alt='의회소식지'/></a></li>
				<li><a href='<%= webBase + counFList[ 1][4] %>'><img src='<%= webBase %>/images/main/coun_icon05.png' alt='의회에 바란다'/></a></li>
				<li><a href='<%= webBase%>/coun/cast/general.do?ct=1'><img src='<%= webBase %>/images/main/coun_icon06.png' alt='영상회의록'/></a></li>
				<li><a href='<%= webBase + counDList[ 1][4] %>'><img src='<%= webBase %>/images/main/coun_icon06.png' alt='인터넷방송'/></a></li>
				<li><a href='<%= webBase + counAList[20][4] %>'><img src='<%= webBase %>/images/main/coun_icon07.png' alt='전화번호안내'/></a></li>
			</ul></div>
		</div>
		<div class='main_section02'>
			<div class='notice'>
				<div class='tab_area'><ul>
					<li><a href='javascript:void(0);' class='on'>공지사항/입법예고</a></li><%--
					<li><a href='javascript:void(0);'>의사일정</a></li>--%>
					<li><a href='javascript:void(0);'>언론보도</a></li>
				</ul></div>
				<div class='tab_cont'>
					<!-- 공지사항/입법예고 -->
					<div class='item' style='display:block;'>
						<div class='list'>
							<ul><c:choose><c:when test="${ empty mainNoti}">
								<li class='center'><%= viewData %></li></c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${mainNoti}">
								<li><a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")'>${rult.bbsTitle}</a><span class='date'>${rult.creDate}</span></li></c:forEach></c:otherwise></c:choose>
							</ul>
							<a href='<%= webBase + counGList[ 0][4] %>' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='공지사항/입법예고 더보기'></a>
						</div>
					</div>
					<!-- //공지사항/입법예고 --><%--
					<!-- 의사일정 -->
					<div class='item'>
						<div class='list'>
							<ul><c:choose><c:when test="${ empty mainSchd}">
								<li class='center'><%= viewData %></li></c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${mainSchd}">
								<li><a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")'>${rult.bbsTitle}</a><span class='date'>${rult.creDate}</span></li></c:forEach></c:otherwise></c:choose>
							</ul>
							<a href='<%= webBase + counCList[ 6][4] %>' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='의사일정 더보기'></a>
						</div>
					</div>
					<!-- //의사일정 -->--%>
					<!-- 언론보도 -->
					<div class='item'>
						<div class='list'>
							<ul><c:choose><c:when test="${ empty mainPress}">
								<li class='center'><%= viewData %></li></c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${mainPress}">
								<li><a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")'>${rult.bbsTitle}</a><span class='date'>${rult.creDate}</span></li></c:forEach></c:otherwise></c:choose>
							</ul>
							<a href='<%= webBase + counGList[ 3][4] %>' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='언론보도 더보기'></a>
						</div>
					</div>
					<!-- //언론보도 -->
				</div>
			</div>
			<div class='movie'>
				<h2>동영상갤러리</h2>
				<a href='<%= webBase + counCList[ 2][4] %>' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='동영상갤러리 더보기'/></a>
				<div class='movie_area'><c:choose><c:when test="${ empty mainMovie}">
					<div class='img center'><%= viewData %></div></c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${mainMovie}">
					<%-- <script type="text/javascript">
    						alert("alert를 사용한 경고 창 예시");
    					</script> --%>

					<%-- <a href='javascript:alert("동영상 준비 중입니다.");'> --%>
					<onclick='fnAct<%= movieLayer ? "Media" : "Detail" %>("${rult.viewNo}","${rult.actMode}")'>
					<a href='javascript:void(0);' onclick='fnAct<%= movieLayer ? "Media" : "Detail" %>("${rult.viewNo}","${rult.actMode}")'>
						<div class='img'>
							<span class='ico_play'><img src='<%= webBase %>/images/icon/ico_play.png' alt='재생'/></span><c:choose><c:when test="${!empty rult.fileUUID}">
							<img src='<% if( imgMode ) { %><%= webBase %>/GetImage.do?key=${rult.fileUUID}<% } else { %><%= webBase + uploadPath %>${rult.fileAbs}<% } %>' alt='${rult.bbsTitle}'/></c:when><c:otherwise>
							<video oncontextmenu='return false;' width='260' height='147'<% if(viewPoster){ %><c:if test="${!empty rult.fileUUID}"> poster='<% if( imgMode ) { %><%= webBase %>/GetImage.do?key=${rult.fileUUID}<% } else { %><%= webBase + uploadPath %>${rult.fileAbs}<% } %>'</c:if><% }else{ %> preload='metadata'<% } %>><% if( vodMode ) { %>
								<source src='<%= webBase %>/GetMovie.do?key=${rult.downUUID}' type='video/${rult.downExt}'/><% } else { %>
								<source src='<%= viewPort + webBase %>${rult.downAbs}' type='video/${rult.downExt}'/><% } %>
							</video></c:otherwise></c:choose>
						</div>
						<div class='txt'>${rult.bbsTitle}</div>
					</a></c:forEach></c:otherwise></c:choose>
				</div>
			</div>
			<div class='agenda'>
				<div class='tab_area'><ul>
					<li><a href='javascript:void(0);' class='on'>접수의안</a></li>
					<li><a href='javascript:void(0);'>처리의안</a></li>
				</ul></div>
				<div class='tab_cont'>
					<!-- 접수의안 -->
					<div class='item' style='display:block;'>
						<div class='list'>
							<ul><c:choose><c:when test="${ empty mainRecv}">
								<li class='center'><%= viewData %></li></c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${mainRecv}">
								<li><a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")'>${rult.billTitle}</a></li></c:forEach></c:otherwise></c:choose>
							</ul>
							<a href='<%= webBase + counEList[13][4] %>' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='접수의안 더보기'></a>
						</div>
					</div>
					<!-- //접수의안 -->
					<!-- 처리의안 -->
					<div class='item'>
						<div class='list'>
							<ul><c:choose><c:when test="${ empty mainProc}">
								<li class='center'><%= viewData %></li></c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${mainProc}">
								<li><a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")'>${rult.billTitle}</a></li></c:forEach></c:otherwise></c:choose>
							</ul>
							<a href='<%= webBase + counEList[11][4] %>' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='처리의안 더보기'></a>
						</div>
					</div>
					<!-- //처리의안 -->
				</div>
			</div>
		</div>
		<div class='main_section03'>
			<div class='hotissue'>
				<h2>평택시의회 핫이슈</h2>
				<div class='hotissue_list'><ul><c:if test="${!empty mainPopup}"><c:forEach var="rult" varStatus="status" items="${mainPopup}"><c:choose><c:when test="${'F' eq rult.bbsSiteType}">
					<li><% if( imgMode ) { %><img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/><% } else { %><img src='<%= webBase + uploadPath %>${rult.fileAbs}' alt='${rult.bbsTitle}'/><% } %></li></c:when><c:otherwise>
					<li><a href='${rult.bbsSiteUri}'<c:if test="${'N' eq rult.bbsSiteType}"> target='_blank'</c:if>><% if( imgMode ) { %><img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/><% } else { %><img src='<%= webBase + uploadPath %>${rult.fileAbs}' alt='${rult.bbsTitle}'/><% } %></a></li></c:otherwise></c:choose></c:forEach></c:if>
				</ul></div>
			</div>
			<div class='chairman'>
				<h2>의장인사말</h2>
				<a href='<%= webBase + counAList[ 0][4] %>' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='의장인사말 더보기'/></a>
				<div class='chairman_img'><a href='<%= webBase + counAList[ 0][4] %>'>
					<img src='<%= webBase %>/images/main/main_chairman.jpg' alt='생생한 현장의 목소리를 경청하며, 현장중심의 의정을 펼치겠습니다. 권영화 의장'/>
				</a></div>
			</div>
			<div class='gallery'>
				<h2>의정활동 갤러리</h2>
				<a href='<%= webBase + counCList[ 1][4] %>' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='의정활동 갤러리 더보기'/></a>

			</div>
		</div>
		<div class='main_section04'>
			<h2>현역의원</h2>
			<div class='search_member'>
				<div>
					<label for='searchMembText' class='hide'>의원명</label>
					<input type='text' id='searchMembText' placeholder='의원명을 입력하세요' /><%--
					<input type='image' src='<%= webBase %>/images/icon/ico_search01.png' onclick='fnMembFinder("view")' alt='검색'/>--%>
					<img src='<%= webBase %>/images/icon/ico_search01.png' class='center vam' onclick='fnMembFinder("view")' alt='검색' style='cursor:pointer;'/>
				</div>
			</div>
			<div class='list'><ul><c:if test="${!empty mainMemb}"><c:forEach var="rult" varStatus="status" items="${mainMemb}">
				<li><a href='javascript:void(0);' onclick='fnActActive("${rult.memID}")'>
					<div class='img'><img src='<%= webBase %>/images/former/${rult.memPic}' alt='${rult.memName} ${rult.comName}'/></div>
					<div class='txt'><strong>${rult.memName}<span> ${rult.comName}</span></strong><p>${rult.regName}</p></div>
				</a></li>
			</c:forEach></c:if></ul></div>
		</div>
		<div class='main_section05'><ul><c:if test="${!empty mainBanner}"><c:forEach var="rult" varStatus="status" items="${mainBanner}"><c:choose><c:when test="${'F' eq rult.bbsSiteType}">
			<li><% if( imgMode ) { %><img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/><% } else { %><img src='<%= webBase + uploadPath %>${rult.fileAbs}' alt='${rult.bbsTitle}'/><% } %></li></c:when><c:otherwise>
			<li><a href='${rult.bbsSiteUri}'<c:if test="${'N' eq rult.bbsSiteType}"> target='_blank'</c:if>><% if( imgMode ) { %><img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/><% } else { %><img src='<%= webBase + uploadPath %>${rult.fileAbs}' alt='${rult.bbsTitle}'/><% } %></a></li></c:otherwise></c:choose></c:forEach></c:if>
		</ul></div>
	</div><%= movieLayer ? "<div class='layer'><div class='bg'></div><div class='layer_pop' id='layVideo'></div></div>" : "" %>
	<!--layer popup START-->

	
	<div id='mask' style='display:none;'></div>

	<%-- // 이미지맵 방청제한 팝업
	<div class='mlay_ppbg' id='main_popup3'>
		<div class='mlay_cont'><img src='<%= webBase %>/popup/20220206.png' alt='평택시의회 대표홈페이지 리뉴얼 안내' usemap="#20220206.jpg"/></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday3' name='chkday3'/><label for='chkday3'> 오늘 하루동안 보지 않기</label></span><span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
		<map name="20220206.jpg">
			<area shape="rect" coords="56,194,618,233" href="http://www.ptcouncil.go.kr/coun/cast/live.do" target="_blank" alt="" />-
			<area shape="rect" coords="56,243,618,302" href="https://www.youtube.com/channel/UCgVmwwXQeon_wcdjmxruj0w" target="_blank" alt="" />
		</map>
	</div>
	--%>

	<!--
	<div class='mlay_ppbg' id='main_popup2'>
		<div class='mlay_cont'><img src='<%//= webBase %>/popup/20220128.png' alt='의원 등록에 따른 안내말씀'/></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday2' name='chkday2'/><label for='chkday2'> 오늘 하루동안 보지 않기</label></span><span class='mlay_clbt' id='mlay_clbt2'>닫기</span></div>
	</div>
	-->
<!--
	<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><img src='<%= webBase %>/popup/221031.jpg' alt='의원 등록에 따른 안내말씀'/></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span><span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
	</div>
-->

	<div class='mlay_ppbg' id='main_popup1'>
		<div class='mlay_cont'><img src='<%= webBase %>/popup/20230216.jpg' alt='제 237회 평택시의회 임시회' usemap="#20230216.jpg" onfocus="this.blur()" onclick='fnActDetail("1DFFDA9F7BBF8F8DCDA2A7B2347AD0C9A37C2238237BB782F5BA035C2E2360551","base/noticeView")' /></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday1' name='chkday1'/><label for='chkday1'> 오늘 하루동안 보지 않기</label></span>
			<span class='mlay_clbt' id='mlay_clbt1'>닫기</span></div>
		<map name="20230216.jpg">
			<area shape="rect" coords="231,228,475,290" href="/download?fileName=test.hwp" target="" alt="" />
			<area shape="rect" coords="108,402,578,437" href="http://www.ptcouncil.go.kr/coun/cast/live.do" target="_blank" alt="" onfocus="this.blur()" />
			<area shape="rect" coords="108,437,339,474" href="https://www.youtube.com/channel/UCgVmwwXQeon_wcdjmxruj0w" target="_blank" alt="" onfocus="this.blur()" />
		</map>
	</div>

	<%--
	<div class='mlay_ppbg' id='main_popup2'>
		<div class='mlay_cont'><img src='<%= webBase %>/images/2020.jpg' alt='2020년 새해 신년사'/></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday2' name='chkday2'/><label for='chkday2'> 오늘 하루동안 보지 않기</label></span><span class='mlay_clbt' id='mlay_clbt2'>닫기</span></div>
	</div>
	--%>

	<!--
	<div class='mlay_ppbg' id='main_popup3'>
		<div class='mlay_cont'><img src='<%= webBase %>/images/20200113.jpg' onclick='fnActDetail("08D5E9D6CF84A7D43BE95D50AF975FEB46BC9204793D6F5F3B697B0A842C328F6","mediaView");' style='cursor:pointer;' alt='2020년01월13일~14일까지 작업 알림'/></div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday3' name='chkday3'/><label for='chkday3'> 오늘 하루동안 보지 않기</label></span><span class='mlay_clbt' id='mlay_clbt3'>닫기</span></div>
	</div>
	-->

	<!--
	<div class='mlay_ppbg' id='main_popup3'>
		<div class='video'>
			<video src='<%= webBase %>/popup/20220128.mp4' poster="<%= webBase %>/popup/20220128.png" controls autoplay>
				해당 브라우저는 video 태그를 지원하지 않습니다.
			</video>
		</div>
		<div class='mlay_clbg'><span><input type='checkbox' id='chkday3' name='chkday3'/><label for='chkday3'> 오늘 하루동안 보지 않기</label></span><span class='mlay_clbt' id='mlay_clbt3'>닫기</span></div>
	</div>
	-->

	<script>
		(function($) {
			$.fn.rwdImageMaps = function() {
				var $img = this;
				var rwdImageMap = function() {
					$img.each(function() {
						if (typeof($(this).attr('usemap')) == 'undefined')
							return;
						var that = this,
								$that = $(that);
						$('<img />').on('load', function() {
							var attrW = 'width',
									attrH = 'height',
									w = $that.attr(attrW),
									h = $that.attr(attrH);
							if (!w || !h) {
								var temp = new Image();
								temp.src = $that.attr('src');
								if (!w)
									w = temp.width;
								if (!h)
									h = temp.height-200; // -200
							}
							var wPercent = $that.width()/100,
									hPercent = $that.height()/100,
									map = $that.attr('usemap').replace('#', ''),
									c = 'coords';
							$('map[name="' + map + '"]').find('area').each(function() {
								var $this = $(this);
								if (!$this.data(c))
									$this.data(c, $this.attr(c));
								var coords = $this.data(c).split(','),
										coordsPercent = new Array(coords.length);
								for (var i = 0; i < coordsPercent.length; ++i) {
									if (i % 2 === 0)
										coordsPercent[i] = parseInt(((coords[i]/w)*100)*wPercent);
									else
										coordsPercent[i] = parseInt(((coords[i]/h)*100)*hPercent);
								}
								$this.attr(c, coordsPercent.toString());
							});
						}).attr('src', $that.attr('src'));
					});
				};
				$(window).resize(rwdImageMap).trigger('resize');
				return this;
			};
		})(jQuery);

		$(document).ready(function(e) {
			$('img[usemap]').rwdImageMaps();
		});
	</script>

	<!--layer popup END-->
	<!-- footer -->
<jsp:include page="/WEB-INF/views/coun/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>
