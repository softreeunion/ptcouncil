<%
////////////////////////////////////////////////////////////
// Program ID  : imagesYouth_edit
// Description : 청소년의회 > 메인이미지 [수정화면]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "메인이미지";
	String subFirst				= "";
	if(mngrHList!=null) for(int m=0;m<mngrHList.length;m++) if("T".equals(mngrHList[m][0])){subFirst=mngrHList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incCalendar.jspf"/>
	<script>
	$(document).ready(function(){
		$("[id^='creOrder']").keyup(function(){$(this).val($(this).val().replace(/[^0-9]/gi,""));});
		$("#bbsTitle").focus();
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"Image"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self","enctype":""}).submit();
	};<c:if test="${!empty dataView and ('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
	var fnActUpdate     = function(){
		if($.trim($("#bbsTitle").val())==""){alert("제목은 필수입력입니다.");$("#bbsTitle").focus();return false;}
		if($.trim($("#bgnDate").val())==""){alert("게시기간은 필수입력입니다.");$("#bgnDate").focus();return false;}
		if($.trim($("#fileThmb").val())==""&&$("[id^='fileDele']").length==0){alert("썸네일은 필수입력입니다.");$("#fileThmb").focus();return false;}
		if(!fnValidImage("fileThmb")) return false;
		if($.trim($("#bbsSiteUri").val())==""&&$("#bbsSiteType").val()!="F"){alert("링크구분 사용시 링크주소는 필수입력입니다.");$("#bbsSiteUri").focus();return false;}
		if($.trim($("#bbsSiteUse").val())==""){alert("사용여부는 필수선택입니다.");$("#bbsSiteUse").focus();return false;}
		$("#frmDefault").attr({"action":"ImageUpdate.do","method":"post","target":"_self","enctype":"multipart/form-data"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			contentType   : false , 
			processData   : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			beforeSend    : function(){$(".progress").show();$(".progress-bar").width("0%");},
			uploadProgress: function(event,position,total,percentComplete){$(".progress-bar").width(percentComplete+"%");if(percentComplete==100){$(".progress").hide();};},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn();}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='reSiteUse'     name='reSiteUse'     value='${reSiteUse}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="8"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>청소년의회</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Youth/ImageList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:88%;'/></colgroup>
					<tbody><tr>
						<th scope='col'>제목 *</th>
						<td><input type='text' id='bbsTitle' name='bbsTitle' class='w90_p' maxlength='400' value='${dataView.bbsTitle}'/></td>
					</tr><tr>
						<th>게시기간 *</th>
						<td>
							<input type='text' id='bgnDate' name='bgnDate' class='w90 center' maxlength='8' value='${dataView.bgnDate}' placeholder='<%= dateHolder %>' readonly='readonly'/>&nbsp;~&nbsp;
							<input type='text' id='endDate' name='endDate' class='w90 center' maxlength='8' value='${dataView.endDate}' placeholder='<%= dateHolder %>' readonly='readonly'/>
						</td>
					</tr><c:choose><c:when test="${ empty thmbFile}"><tr>
						<th>썸네일 *</th>
						<td id='file_thmb'><input type='file' id='fileThmb' name='fileThmb' class='w90_p' accept='image/*'/><%= allowFiles[2] %></td>
					</tr></c:when><c:otherwise><c:forEach var="file" varStatus="status" items="${thmbFile}"><c:if test="${'T' eq file.fileImage}"><tr>
						<th>썸네일 *</th>
						<td id='file_thmb${status.count}'>
							<input type='button' id='fileDele${status.count}' class='w70 btn_small bt_grey' value='파일삭제' onclick='fnThmbRemove("${status.count}","${file.fileUUID}")'/>&nbsp;&nbsp;
							<a href='javascript:void(0);' onclick='fnActDownload("${file.fileUUID}")'>${file.fileRel}</a>
						</td>
					</tr></c:if></c:forEach></c:otherwise></c:choose><tr>
						<th>링크구분</th>
						<td>
							<select id='bbsSiteType' name='bbsSiteType' class='w12_p'>
								<option value='F' <c:if test="${'F' eq dataView.bbsSiteType}"> selected='selected'</c:if>>사용 안함</option>
								<option value='N' <c:if test="${'N' eq dataView.bbsSiteType}"> selected='selected'</c:if>>새창 열기</option>
								<option value='D' <c:if test="${'D' eq dataView.bbsSiteType}"> selected='selected'</c:if>>바로 가기</option>
							</select>
						</td>
					</tr><tr>
						<th>링크주소</th>
						<td><input type='text' id='bbsSiteUri' name='bbsSiteUri' class='w90_p' maxlength='400' value='${dataView.bbsSiteUri}'/></td>
					</tr><tr>
						<th>보기순번</th>
						<td><input type='text' id='creOrder' name='creOrder' class='w12_p center' maxlength='2' value='${dataView.creOrder}'/><input type='hidden' id='modOrder' name='modOrder' value='${dataView.creOrder}'/> <span class='info01 light_grey'> 미입력시 최종 보기순번으로 입력됩니다.</span></td>
					</tr><tr>
						<th>사용여부 *</th>
						<td>
							<select id='bbsSiteUse' name='bbsSiteUse' class='w12_p center'>
								<option value='Y' <c:if test="${'Y' eq dataView.bbsSiteUse}"> selected='selected'</c:if>>Y</option>
								<option value='N' <c:if test="${'Y' ne dataView.bbsSiteUse}"> selected='selected'</c:if>>N</option>
							</select>
						</td>
					</tr></tbody>
				</table>
				<div class='btn_area right'><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
					<input type='button' class='w70 btn_medium bt_red'    value='저장'       onclick='fnActUpdate()'/></c:if>
					<input type='button' class='w70 btn_medium bt_grey'   value='취소'       onclick='fnActReturn()'/>
					<input type='button' class='w70 btn_medium bt_grey'   value='목록'       onclick='fnActReturn("seq")'/>
				</div>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>