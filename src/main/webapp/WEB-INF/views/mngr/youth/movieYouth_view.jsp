<%
////////////////////////////////////////////////////////////
// Program ID  : movieYouth_view
// Description : 청소년의회 > 의회동영상 [상세조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "의회동영상";
	boolean viewReply			= false;
	boolean viewPoster			= true;
	String subFirst				= "";
	if(mngrHList!=null) for(int m=0;m<mngrHList.length;m++) if("T".equals(mngrHList[m][0])){subFirst=mngrHList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script>
	$(document).ready(function(){
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"Movie"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self"}).submit();
	};
	var fnActDetail     = function(view){
		doActLoadShow();
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"MovieView.do","method":"post","target":"_self"}).submit();
	};<c:if test="${!empty dataView and ('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
	var fnActModify     = function(){<c:choose><c:when test="${'0' eq dataView.ansCount}">
		doActLoadShow();
		$("#frmDefault").attr({"action":"MovieEdit.do","method":"post","target":"_self"}).submit();</c:when><c:otherwise>
		alert("답변글이 있어 수정이 제한됩니다.");return false;</c:otherwise></c:choose>
	};
	var fnActDelete     = function(){<c:choose><c:when test="${'0' eq dataView.ansCount}">
		if(confirm("<%= alertMsg[4].replaceAll("[@]", "현재") %>")==0) return false;
		$("#frmDefault").attr({"action":"MovieDelete.do","method":"post","target":"_self"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn("seq");}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});</c:when><c:otherwise>
		alert("답변글이 있어 삭제가 제한됩니다.");return false;</c:otherwise></c:choose>
	};</c:if><% if( viewReply ) { %><c:if test="${!empty dataView}">
	var fnActReply      = function(){
		doActLoadShow();
		$("#frmDefault").attr({"action":"MovieReEdit.do","method":"post","target":"_self"}).submit();
	};</c:if><% } %>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="8"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>청소년의회</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Youth/MovieList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:25%;'/><col style='width:12%;'/><col style='width:25%;'/><col style='width:12%;'/><col style='width:20%;'/></colgroup>
					<tbody><tr>
						<th>제목</th>
						<td colspan='5'>${dataView.bbsTitle}</td>
					</tr><tr>
						<th scope='col'>작성자</th>
						<td>${dataView.creName}</td>
						<th scope='col'>작성일</th>
						<td>${dataView.creDate}</td>
						<th scope='col'>조회수</th>
						<td>${dataView.hitCount}</td>
					</tr><tr class='h250'>
						<td colspan='6' class='nonebd_l'><c:if test="${!empty dataView.downUUID}"><video id='vod' oncontextmenu='return false;' width='100%' controls<% if( viewPoster ) { %> preload='metadata'<c:if test="${!empty dataView.fileUUID}"> poster='<% if( imgMode ) { %><%= webBase %>/GetImage.do?key=${dataView.fileUUID}<% } else { %><%= webBase + uploadPath %>${dataView.fileAbs}<% } %>'</c:if><% } else { %> autoplay preload='auto'<% } %>><% if( vodMode ) { %>
							<source src='<%= webBase %>/GetMovie.do?key=${dataView.downUUID}' type='video/${dataView.downExt}'/><% } else { %>
							<source src='<%= viewPort + webBase %>${dataView.downAbs}' type='video/${dataView.downExt}'/><% } %>
						</video></c:if></td>
					</tr></tbody>
				</table>
				<div class='btn_area right'><% if( viewReply ) { %><c:if test="${!empty dataView}">
					<input type='button' class='w70 btn_medium bt_blue'   value='답변'       onclick='fnActReply()'/><c:if test="${'0' eq dataView.ansCount and ('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
					<input type='button' class='w70 btn_medium bt_blue'   value='수정'       onclick='fnActModify()'/>
					<input type='button' class='w70 btn_medium bt_red'    value='삭제'       onclick='fnActDelete()'/></c:if></c:if><% } else { %><c:if test="${!empty dataView and '0' eq dataView.ansCount and ('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
					<input type='button' class='w70 btn_medium bt_blue'   value='수정'       onclick='fnActModify()'/>
					<input type='button' class='w70 btn_medium bt_red'    value='삭제'       onclick='fnActDelete()'/></c:if><% } %>
					<input type='button' class='w70 btn_medium bt_grey'   value='목록'       onclick='fnActReturn("seq")'/>
				</div><c:if test="${'T' eq moveItem}">
				<div class='board_list'><table class='table table_view'>
					<caption class='hide'>Previous/Next</caption>
					<colgroup><col style='width:12%;'/><col style='width:88%;'/></colgroup>
					<tbody><tr>
						<th scope='col'>이전글</th>
						<td><c:choose><c:when test="${ empty prevView}">이전 글이 존재 하지 않습니다.</c:when><c:otherwise>${prevView.gabSpace}<a href='javascript:void(0);' onclick='fnActDetail("${prevView.viewNo}")'><c:if test="${'Y' eq prevView.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${prevView.bbsTitle}</a></c:otherwise></c:choose></td>
					</tr><tr>
						<th scope='col'>다음글</th>
						<td><c:choose><c:when test="${ empty nextView}">다음 글이 존재 하지 않습니다.</c:when><c:otherwise>${nextView.gabSpace}<a href='javascript:void(0);' onclick='fnActDetail("${nextView.viewNo}")'><c:if test="${'Y' eq nextView.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${nextView.bbsTitle}</a></c:otherwise></c:choose></td>
					</tr></tbody>
				</table></div></c:if>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>