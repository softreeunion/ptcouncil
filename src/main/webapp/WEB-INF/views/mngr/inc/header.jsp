<%
////////////////////////////////////////////////////////////
// Program ID  : header
// Description : 관리자 HEADER
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<%@ page import="egovframework.ptcc.cmmn.vo.*" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	BaseCommonVO userInfo		= (BaseCommonVO)request.getSession().getAttribute("MemRInfo");		// 관리자 로그인 내역

	if( userInfo == null ) { 
		out.print("<script>alert(\"로그인 정보가 존재하지 않습니다.\\n\\n로그인 화면으로 이동합니다.\");location.href=\""+ webBase +"/ptnhexa/index.do\";</script>");
	} else { 
		out.clearBuffer();

		out.print(baseTab[1] +"<div id='header'>\n");
		out.print(baseTab[2] +"<div class='head_info'>\n");
		out.print(baseTab[3] +"<h1><a href='"+ webBase +"/ptnhexa/index.do'><img src='"+ webBase +"/images/common/logo_base.png' alt='+ webTitle +'/></a></h1>\n");
		out.print(baseTab[3] +"<div class='head_right'><ul>\n");
		out.print(baseTab[4] +"<li><strong>"+ userInfo.getUserName() +"</strong>님</li>\n");
		out.print(baseTab[4] +"<li><a href='"+ webBase +"/ptnhexa/huout_proc.do' class='btn_xsmall bt_white border_1px'>로그아웃</a></li>\n");
		out.print(baseTab[3] +"</ul></div>\n");
		out.print(baseTab[2] +"</div>\n");

		out.print(baseTab[2] +"<div class='gnb_warp'>\n");
		out.print(baseTab[3] +"<ul>\n");
		if(mngrAList!= null) for(int m=0;m<mngrAList.length;m++) if("T".equals(mngrAList[m][0])){
			out.print(baseTab[4] +"<li><a href='"+ webBase + mngrAList[m][4] +"'>계정 관리</a></li>\n");break;
		}
		if(mngrBList!= null) for(int m=0;m<mngrBList.length;m++) if("T".equals(mngrBList[m][0])){
			out.print(baseTab[4] +"<li><a href='"+ webBase + mngrBList[m][4] +"'>메인 관리</a></li>\n");break;
		}
		if(mngrCList!= null) for(int m=0;m<mngrCList.length;m++) if("T".equals(mngrCList[m][0])){
			out.print(baseTab[4] +"<li><a href='"+ webBase + mngrCList[m][4] +"'>의원 관리</a></li>\n");break;
		}
		if(mngrDList!= null) for(int m=0;m<mngrDList.length;m++) if("T".equals(mngrDList[m][0])){
			out.print(baseTab[4] +"<li><a href='"+ webBase + mngrDList[m][4] +"'>의정 활동</a></li>\n");break;
		}
		if(mngrEList!= null) for(int m=0;m<mngrEList.length;m++) if("T".equals(mngrEList[m][0])){
			out.print(baseTab[4] +"<li><a href='"+ webBase + mngrEList[m][4] +"'>회의록 관리</a></li>\n");break;
		}
		if(mngrFList!= null) for(int m=0;m<mngrFList.length;m++) if("T".equals(mngrFList[m][0])){
			out.print(baseTab[4] +"<li><a href='"+ webBase + mngrFList[m][4] +"'>게시판 관리</a></li>\n");break;
		}
		if(mngrGList!= null) for(int m=0;m<mngrGList.length;m++) if("T".equals(mngrGList[m][0])){
			out.print(baseTab[4] +"<li><a href='"+ webBase + mngrGList[m][4] +"'>상임위원회</a></li>\n");break;
		}
		if(mngrHList!= null) for(int m=0;m<mngrHList.length;m++) if("T".equals(mngrHList[m][0])){
			out.print(baseTab[4] +"<li><a href='"+ webBase + mngrHList[m][4] +"'>청소년의회</a></li>\n");break;
		}
		if(mngrIList!= null) for(int m=0;m<mngrIList.length;m++) if("T".equals(mngrIList[m][0])){
			out.print(baseTab[4] +"<li><a href='"+ webBase + mngrIList[m][4] +"'>접속통계</a></li>\n");break;
		}
		out.print(baseTab[3] +"</ul>\n");
		out.print(baseTab[3] +"<span class='full_menu'>\n");
		out.print(baseTab[3] +"</span>\n");
		out.print(baseTab[2] +"</div>\n");
		out.print(baseTab[1] +"</div>\n");
	} %>