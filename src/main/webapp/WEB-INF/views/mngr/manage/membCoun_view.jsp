<%
////////////////////////////////////////////////////////////
// Program ID  : membCoun_view
// Description : 의원관리 > 의원 관리 [상세조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "의원 관리";
	String subFirst				= "";
	if(mngrCList!=null) for(int m=0;m<mngrCList.length;m++) if("T".equals(mngrCList[m][0])){subFirst=mngrCList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script>
	$(document).ready(function(){
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"Member"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self"}).submit();
	};
	var fnActDetail     = function(view){
		doActLoadShow();
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"MemberView.do","method":"post","target":"_self"}).submit();
	};<c:if test="${!empty dataView and ('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
	var fnActModify     = function(){
		doActLoadShow();
		$("#frmDefault").attr({"action":"MemberEdit.do","method":"post","target":"_self"}).submit();
	};
	var fnActDelete     = function(){
		if(confirm("<%= alertMsg[4].replaceAll("[@]", "현재") %>") == 0) return false;
		$("#frmDefault").attr({"action":"MemberDelete.do","method":"post","target":"_self"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn("seq");}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='reEraCode'     name='reEraCode'     value='${reEraCode}'/>
<input type='hidden' id='reRegCode'     name='reRegCode'     value='${reRegCode}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="3"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>의원 관리</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Coun/MemberList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:38%;'/><col style='width:12%;'/><col style='width:14%;'/><col style='width:24%;'/></colgroup>
					<tbody><tr>
						<th>대수</th>
						<td colspan='3'>${dataView.eraName}</td>
						<td rowspan='5' class='nonebd_l center'><c:choose><c:when test="${ empty dataView.memPic}"><img src='<%= webBase %>/images/common/NoPhoto150_200.png' alt='NoPhoto'/></c:when><c:otherwise><img src='<%= webBase %>/images/former/${dataView.memPic}' alt='${dataView.memName}'/></c:otherwise></c:choose></td>
					</tr><tr>
						<th>성명(한글)</th>
						<td colspan='3'>${dataView.memName}</td>
					</tr><tr>
						<th>성명(영문)</th>
						<td colspan='3'>${dataView.memEng}</td>
					</tr><tr>
						<th>성명(한문)</th>
						<td colspan='3'>${dataView.memChi}</td>
					</tr><tr>
						<th>직위</th>
						<td colspan='3'>${dataView.comName}</td>
					</tr><tr>
						<th scope='col'>선거구</th>
						<td colspan='1'>${dataView.regName}</td>
						<th scope='col'>정당명</th>
						<td colspan='4'>${dataView.memParty}</td>
					</tr><tr>
						<th>휴대폰</th>
						<td colspan='1'>${dataView.memMobile}</td>
						<th>이메일</th>
						<td colspan='2'>${dataView.memEmail}</td>
					</tr><tr>
						<th>소개</th>
						<td colspan='4'>${dataView.memIntroBR}</td>
					</tr><tr>
						<th>약력</th>
						<td colspan='4'>${dataView.memProfileBR}</td>
					</tr></tbody>
				</table>
				<div class='btn_area right'><c:if test="${false and ('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
					<input type='button' class='w70 btn_medium bt_blue'   value='수정'       onclick='fnActModify()'/>
					<input type='button' class='w70 btn_medium bt_red'    value='삭제'       onclick='fnActDelete()'/></c:if>
					<input type='button' class='w70 btn_medium bt_grey'   value='목록'       onclick='fnActReturn("seq")'/>
				</div>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>