<%
////////////////////////////////////////////////////////////
// Program ID  : membStaff_list
// Description : 계정관리 > 담당자 관리 [목록조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<%@ page import="egovframework.ptcc.cmmn.vo.*" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "담당자 관리";
	BaseCommonVO userInfo		= (BaseCommonVO)request.getSession().getAttribute("MemRInfo");		// 관리자 로그인 내역
	String subFirst				= "";
	if(mngrAList!=null) for(int m=0;m<mngrAList.length;m++) if("T".equals(mngrAList[m][0])){subFirst=mngrAList[m][4];break;}

	if( !egovframework.ptcc.cmmn.util.BaseCategory.getCheckOfAdmin( userInfo.getAuthRole() ) ) { 
		out.println("<script>alert(\"접속권한이 없습니다.\\n\\n시스템 관리자에게 문의바랍니다.\");history.back();</script>");
	} else { 
		out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script>
	$(document).ready(function(){
	});
	var fnActInitial    = function(){
		doActLoadShow();
		$("#condType").val("A");
		$("#condValue").val("");
		fnActRetrieve();
	};
	var fnActRetrieve   = function(page){
		doActLoadShow();
		$("#pageCurNo").val(isEmpty(page)?"1":page);
		$("#frmDefault").attr({"action":"MemberList.do","method":"post","target":"_self"}).submit();
	};
	var fnActDetail     = function(view){
		doActLoadShow();
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"MemberView.do","method":"post","target":"_self"}).submit();
	};<c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole)}">
	var fnActCreate     = function(){
		doActLoadShow();
		$("#frmDefault").attr({"action":"MemberInst.do","method":"post","target":"_self"}).submit();
	};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="1"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>계정 관리</a></li>
				<li><a href='<%= webBase %>/ptnhexa/User/MemberList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='search_area'>
				<table class='table table_cols'>
					<colgroup><col style='width: 8%;'/><col style='width:16%;'/><col style='width: 8%;'/><col style='width:16%;'/><col style='width: 8%;'/><col style='width:44%;'/></colgroup>
					<tbody><tr>
						<th>권한구분</th>
						<td><select id='reAuthRole' name='reAuthRole' class='w120'>
								<option value=''   <c:if test="${''   eq reAuthRole}"> selected='selected'</c:if>>전체</option>
								<option value='CM' <c:if test="${'CM' eq reAuthRole}"> selected='selected'</c:if>>최고관리자</option>
								<option value='GM' <c:if test="${'GM' eq reAuthRole}"> selected='selected'</c:if>>업무담당자</option>
								<option value='GE' <c:if test="${'GE' eq reAuthRole}"> selected='selected'</c:if>>일반담당자</option>
						</select></td>
						<th>승인여부</th>
						<td><select id='reAuthFlag' name='reAuthFlag' class='w120'>
								<option value=''   <c:if test="${''   eq reAuthFlag}"> selected='selected'</c:if>>전체</option>
								<option value='T'  <c:if test="${'T'  eq reAuthFlag}"> selected='selected'</c:if>>승인</option>
								<option value='F'  <c:if test="${'F'  eq reAuthFlag}"> selected='selected'</c:if>>미승인</option>
						</select></td>
						<th>검색</th>
						<td><label for='condType' class='hide'>검색영역</label>
							<select id='condType' name='condType' class='w120'>
								<option value='A'  <c:if test="${'A'  eq condType}"> selected='selected'</c:if>>전체</option>
								<option value='P'  <c:if test="${'P'  eq condType}"> selected='selected'</c:if>>사용자ID</option>
								<option value='N'  <c:if test="${'N'  eq condType}"> selected='selected'</c:if>>사용자명</option>
							</select>&nbsp;&nbsp;
							<label for='condValue' class='hide'>검색어</label>
							<input type='text' id='condValue'     name='condValue'     class='w60_p' maxlength='30' value='${condValue}' placeholder='검색어를 입력하세요.'/>
						</td>
					</tr></tbody>
				</table>
				<div class='btn_area center'>
					<input type='button' class='w70 btn_large bt_grey' value='검색'       onclick='fnActRetrieve()'/>
				</div>
			</div>
			<div class='board_list'>
				<div class='board_total'>
					<span class='left'>전체 <strong class='red'>${listCnt}</strong> 건 &nbsp; ${pageCurNo} / ${pageInfo.totalPageCount}</span>
					<span class='right'><select id='pagePerNo'     name='pagePerNo' onchange='fnActRetrieve()'>
						<option value='10' <c:if test="${ '10' eq pagePerNo}"> selected='selected'</c:if>> 10개씩 보기</option>
						<option value='20' <c:if test="${ '20' eq pagePerNo}"> selected='selected'</c:if>> 20개씩 보기</option>
						<option value='30' <c:if test="${ '30' eq pagePerNo}"> selected='selected'</c:if>> 30개씩 보기</option>
						<option value='50' <c:if test="${ '50' eq pagePerNo}"> selected='selected'</c:if>> 50개씩 보기</option>
						<option value='100'<c:if test="${'100' eq pagePerNo}"> selected='selected'</c:if>>100개씩 보기</option>
						<option value='200'<c:if test="${'200' eq pagePerNo}"> selected='selected'</c:if>>200개씩 보기</option>
						<option value='all'<c:if test="${ '10' ne pagePerNo and  '20' ne pagePerNo and  '30' ne pagePerNo and  '50' ne pagePerNo and '100' ne pagePerNo and '200' ne pagePerNo}"> selected='selected'</c:if>>전체 보기</option>
					</select></span>
				</div>
				<div class='rows_table'><table class='table table_rows'>
					<caption class='hide'>Board List</caption>
					<colgroup>
						<col style='width: 6%;'/>
						<col style='width:14%;'/>
						<col style='width:14%;'/>
						<col style='width:14%;'/>
						<%--<col style='width:12%;'/>--%>
						<col style='width:10%;'/>
						<col style='width:16%;'/>
						<col style='width:14%;'/>
					</colgroup>
					<thead><tr>
						<th scope='col'>번호</th>
						<th scope='col'>사용자ID</th>
						<th scope='col'>사용자명</th>
						<th scope='col'>권한구분</th>
						<%--<th scope='col'>소속</th>--%>
						<th scope='col'>승인여부</th>
						<th scope='col'>최종접속일</th>
						<th scope='col'>작성일</th>
					</tr></thead>
					<tbody>
					<c:choose><c:when test="${ empty dataList}">
						<tr class='off h200'><td colspan='8'><%= noneData %></td></tr>
					</c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${dataList}">
						<tr class='on' onclick='fnActDetail("${rult.viewNo}")'>
							<td>${rult.rowIdx}</td>
							<td>${rult.userPID}</td>
							<td>${rult.userName}</td>
							<td>${rult.authRoleName}</td>
							<%--<td>--%>
								<%--<c:choose>--%>
									<%--<c:when test="${rult.userComm eq '00'}">의장</c:when>--%>
									<%--<c:when test="${rult.userComm eq '01'}">의회운영위원회</c:when>--%>
									<%--<c:when test="${rult.userComm eq '02'}">기획행정위원회</c:when>--%>
									<%--<c:when test="${rult.userComm eq '03'}">복지환경위원회</c:when>--%>
									<%--<c:when test="${rult.userComm eq '04'}">산업건설위원회</c:when>--%>
									<%--<c:when test="${rult.userComm eq '99'}">일반</c:when>--%>
									<%--<c:otherwise>No data</c:otherwise>--%>
								<%--</c:choose>--%>
							<%--</td>--%>
							<td<c:if test="${'F' eq rult.authFlag}"> style='color:#f00;'</c:if>>${rult.authFlagName}</td>
							<td>${rult.pwdDateLst}</td>
							<td>${rult.modDate}</td>
						</tr>
					</c:forEach></c:otherwise></c:choose>
					</tbody>
				</table></div>
				<c:if test="${!empty pageInfo}"><div class='paging usr_pagination1'><ui:pagination paginationInfo="${pageInfo}" type="baseImage" jsFunction="fnActRetrieve" /></div></c:if>
			</div>
			<div class='btn_area right'><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole)}">
				<input type='button' class='w70 btn_medium bt_blue'   value='등록'       onclick='fnActCreate()'/></c:if>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html><%	} %>