<%
////////////////////////////////////////////////////////////
// Program ID  : membStaff_pass
// Description : 계정관리 > 담당자 관리 - 비밀번호변경 [수정화면]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "비밀번호 변경";
	String subFirst				= "";
	if(mngrAList!=null) for(int m=0;m<mngrAList.length;m++) if("T".equals(mngrAList[m][0])){subFirst=mngrAList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script src='<%= webBase %>/scripts/rsa/jsbn.js'></script>
	<script src='<%= webBase %>/scripts/rsa/rsa.js'></script>
	<script src='<%= webBase %>/scripts/rsa/prng4.js'></script>
	<script src='<%= webBase %>/scripts/rsa/rng.js'></script>
	<script>
	$(document).ready(function(){
		$("#o_userpass").focus();
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"${nextMode}"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self"}).submit();
	};
	var fnActUpdate     = function(){
		if(!fnValidation()) return;
		var rsa = new RSAKey();
		rsa.setPublic($("#rsaKeyModulus").val(),$("#rsaKeyExponent").val());
		$("#j_userpass").val(rsa.encrypt($("#k_userpass").val()));
		$("#p_userpass").val(rsa.encrypt($("#o_userpass").val()));
		$("#c_userpass").val(rsa.encrypt($("#n_userpass").val()));
		$("#k_userpass").val("");
		$("#o_userpass").val("");
		$("#n_userpass").val("");
		$("#frmDefault").attr({"action":"MemberChange.do","method":"post","target":"_self"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn("seq");}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};
	var fnValidation    = function(){
		var oPass = $("#o_userpass").val();
		var nPass = $("#n_userpass").val();
		var kPass = $("#k_userpass").val();
		if(!oPass){fnDisplayMsg("o_userpass","기존 비밀번호를 입력하세요.",true);return false;}
		if(!nPass){fnDisplayMsg("n_userpass","신규 비밀번호를 입력하세요.",true);return false;}
		if(!kPass){fnDisplayMsg("k_userpass","신규 비밀번호 확인을 입력하세요.",true);return false;}
		if(nPass&&kPass){
			var chk_num=nPass.search(/[0-9]/g);
			var chk_eng=nPass.search(/[a-zA-Z]/ig);
			if(/(\w)\1\1\1/.test(nPass)){fnDisplayMsg("n_userpass","비밀번호에 같은 문자를 4번 이상 사용하실 수 없습니다.",true);return false;}
			if(nPass!=kPass){fnDisplayMsg("n_userpass","비밀번호가 다르게 입력되었습니다. 다시 입력하세요.",true);return false;}
			if(!/^(?=.*[a-zA-Z])(?=.*[!#$%&\'\(\)*+,\-./:;\\\<\=>?@\[\]^_`\{|\}~])(?=.*[0-9]).{8,20}$/.test(nPass)){fnDisplayMsg("n_userpass","비밀번호는 숫자와 영문자, 특수문자 조합으로 8~20자리를 사용해야 합니다.",true);return false;}
			if(chk_num<0||chk_eng<0){fnDisplayMsg("n_userpass","비밀번호는 숫자와 영문자를 혼용해야 합니다.",true);return false;}
			if(nPass.search($("#j_username").val())>-1){fnDisplayMsg("n_userpass","ID가 포함된 비밀번호는 사용하실 수 없습니다.",true);return false;}
		}<%--
		if(oPass==kPass){fnDisplayMsg("n_userpass","기존 비밀번호와 신규 비밀번호가 같을 수 없습니다.",true);return false;}
		if(pCheckKChar(nPass)){fnDisplayMsg("n_userpass","비밀번호는 8 ~ 20자리로 대/소문자, 숫자, 특수문자 중 세가지 이상을 조합하여 입력해주세요.",true);return false;}
		if(!pCheckLen08(nPass)||!pCheckLen20(nPass)) {fnDisplayMsg("n_userpass","비밀번호는 8 ~ 20자리로 대/소문자, 숫자, 특수문자 중 세가지 이상을 조합하여 입력해주세요.",true);return false;}
		if(!pCheckSChar(nPass)){fnDisplayMsg("n_userpass","특수문자는 ! # $ % & \' ( ) * + , - . / : ; < = > ? @ [ \\ ] ^ _ ` { | } ~ 만 사용 가능합니다.",true);return false;}
		if(!pCombination(nPass)){fnDisplayMsg("n_userpass","비밀번호는 8 ~ 20자리로 대/소문자, 숫자, 특수문자 중 세가지 이상을 조합하여 입력해주세요.",true);return false;}
		if(pCheckBlank(nPass)){fnDisplayMsg("n_userpass","공백은 입력할 수 없습니다.",true);return false;}
		if(pCheckENumb(nPass)){fnDisplayMsg("n_userpass","동일숫자는 입력할 수 없습니다.",true);return false;}
		if(pCheckCNumb(nPass)){fnDisplayMsg("n_userpass","연속숫자는 입력할 수 없습니다.",true);return false;}
		if(pCheckBlank(nPass)){fnDisplayMsg("n_userpass","공백은 입력할 수 없습니다.",true);return false;}--%>
		fnDisplayMsg("n_userpass",null,false);
		return true;
	};
	var fnDisplayMsg    = function(name,text,show){
		var $obj = $("#"+name);
		var $msg = $("#msgResult");
		if(show){
			$obj.addClass("error_Msg");
			$msg.text(text);
			$msg.show();
			$obj.focus();
		}else{
			$obj.removeClass("error_Msg");
			$msg.text("");
			$msg.hide();
		}
	};
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='reAuthRole'    name='reAuthRole'    value='${reAuthRole}'/>
<input type='hidden' id='reAuthFlag'    name='reAuthFlag'    value='${reAuthFlag}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<input type='hidden' id='rsaKeyModulus' name='rsaKeyModulus' value='${RSAModulus}'/>
<input type='hidden' id='rsaKeyExponent' name='rsaKeyExponent' value='${RSAExponent}'/>
<input type='hidden' id='j_username'    name='j_username'    value='<c:choose><c:when test="${ empty MemRInfo}">${userNo}</c:when><c:otherwise>${MemRInfo.userPID}</c:otherwise></c:choose>'/>
<input type='hidden' id='j_userpass'    name='j_userpass'>
<input type='hidden' id='p_userpass'    name='p_userpass'>
<input type='hidden' id='c_userpass'    name='c_userpass'>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="1"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>계정 관리</a></li>
				<li><a href='<%= webBase %>/ptnhexa/User/MemberList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='search_area'><table class='table table_cols'>
				<colgroup><col style='width:100%;'/></colgroup>
				<tbody><tr>
					<td>사용자의 개인 정보를 안전하게 보호하기 위해 평택시의회는 비밀번호를 암호화하여 저장, 관리하고 있습니다.</td>
				</tr></tbody>
			</table></div>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
					<tbody><tr>
						<th scope='col'>기존 비밀번호 *</th>
						<td colspan='3'><input type='password' id='o_userpass' name='o_userpass' class='w30_p' maxlength='20' placeholder='기존 비밀번호를 입력하세요.'/></td>
					</tr><tr>
						<th>신규 비밀번호 *</th>
						<td colspan='3'><input type='password' id='n_userpass' name='n_userpass' class='w30_p' maxlength='20' placeholder='영문+숫자+특수문자 8~20자리'/></td>
					</tr><tr>
						<th>신규 비밀번호 확인 *</th>
						<td colspan='3'><input type='password' id='k_userpass' name='k_userpass' class='w30_p' maxlength='20' placeholder='영문+숫자+특수문자 8~20자리'/> &nbsp;<span id='msgResult' style='width:100%;color:#c00;'></span></td>
				</table>
				<div class='btn_area center'>
					<input type='button' class='w100 btn_medium bt_red'   value='확인'         onclick='fnActUpdate()'/>
				</div>
			</div>
		</div></div>
		<div id='contents'>
			<div class='gray_box'>
				<h5>유의사항</h5>
				<ul class='list_type2'>
					<li>영문 대/소문자, 숫자, 특수문자 중 세 가지 이상 조합하여 8 ~ 20자리 이내 입력</li>
					<li>사용가능한 특수문자 : ! # $ % & ' ( ) * + , - . / : ; < = > ? @ [ ＼ ] ^ _ ` { | } ~</li>
					<li>한글, 공백, 동일숫자, 연속숫자 3자리 이상, 사용자 아이디 입력 불가</li>
					<li>생년월일, 이메일, 휴대전화 번호(마지막 4자리) 등 타인이 쉽게 유추 가능한 비밀번호는 사용을 권장하지 않습니다.</li>
					<li>비밀번호 변경은 3개월 이내에 주기적으로 변경하시기를 권해 드립니다.</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>