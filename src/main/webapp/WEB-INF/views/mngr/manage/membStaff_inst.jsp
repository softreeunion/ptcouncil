<%
////////////////////////////////////////////////////////////
// Program ID  : membStaff_inst
// Description : 계정관리 > 담당자 관리 [등록화면]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<%@ page import="egovframework.ptcc.cmmn.vo.*" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "담당자 관리";
	BaseCommonVO userInfo		= (BaseCommonVO)request.getSession().getAttribute("MemRInfo");		// 관리자 로그인 내역
	String subFirst				= "";
	if(mngrAList!=null) for(int m=0;m<mngrAList.length;m++) if("T".equals(mngrAList[m][0])){subFirst=mngrAList[m][4];break;}

	if( !egovframework.ptcc.cmmn.util.BaseCategory.getCheckOfAdmin( userInfo.getAuthRole() ) ) { 
		out.println("<script>alert(\"접속권한이 없습니다.\\n\\n시스템 관리자에게 문의바랍니다.\");history.back();</script>");
	} else { 
		out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script>
	$(document).ready(function(){
		$("select[name='mailSelect']").change(function(){$("#seleMailT").prop("readonly",$(this).val()!="direct");$("#seleMailT").val($(this).val()!="direct"?$(this).val():"");if($(this).val()=="direct")$("#seleMailT").focus();});
		$("#userPNO").focus();
		$("#authFlag").val("F");
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"Member"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self"}).submit();
	};<c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole)}">
	var fnActCreate     = function(){

		var checkBoxArr = [];
		$("input[name=userCommSel]:checked").each(function (i) {
			checkBoxArr.push($(this).val());
		});
		$("#userComm").val(checkBoxArr);
		// alert(checkBoxArr);
		// alert(checkBoxArr.length);
		// return;

		if($.trim($("#userPNO").val())==""){alert("사용자ID를 입력하세요.");$("#userPNO").focus();return false;}
		if(($("#userPNO").val().search(/[0-9]/g)<0)&&($("#userPNO").val().search(/[a-zA-Z]/ig)<0)) {alert("사용자ID는 숫자와 영문자만 가능합니다.");return false;}
		if($("#userPNO").val()!=$("#userPNC").val()) {alert("사용자ID 중복 체크를 하세요.");$("#userPNO").focus();return false;}
		if($.trim($("#userName").val())==""){alert("사용자명을 입력하세요.");$("#userName").focus();return false;}<%--
		if(($.trim($("#userPWI").val())==""||$.trim($("#userPWC").val())=="")) {alert("비밀번호를 입력하세요.");$("#userPWI").focus();return false;}
		if($("#userPWI").val()&&$("#userPWC").val()){
			var chk_num = $("#userPWI").val().search(/[0-9]/g);
			var chk_eng = $("#userPWI").val().search(/[a-zA-Z]/ig);
			if(/(\w)\1\1\1/.test($("#userPWI").val())) {alert("비밀번호에 같은 문자를 4번 이상 사용하실 수 없습니다.");$("#userPWI").focus();	return false;}
			if($("#userPWI").val()!=$("#userPWC").val()) {alert("비밀번호가 다르게 입력되었습니다. 다시 입력하세요.");$("#userPWI").focus();return false;}
			if(!/^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{8,20}$/.test($("#userPWI").val())) {alert("비밀번호는 숫자와 영문자, 특수문자 조합으로 8~20자리를 사용해야 합니다.");$("#userPWI").focus();return false;}
			if(chk_num<0||chk_eng<0) {alert("비밀번호는 숫자와 영문자를 혼용해야 합니다.");$("#userPWI").focus();return false;}
			if($("#userPWI").val().search($("#userPNO").val())>-1) {alert("사용자ID가 포함된 비밀번호는 사용하실 수 없습니다.");$("#userPWI").focus();return false;}
		}--%>
		if($("#seleMailA").val()&&$.trim($("#seleMailT").val())==""){alert("올바른 이메일 주소를 입력하세요.");$("#seleMailT").focus();return false;}
		if($.trim($("#seleMailA").val())==""&&$("#seleMailT").val()) {alert("올바른 이메일 주소를 입력하세요.");$("#seleMailA").focus();return false;}
		if($("#seleMailA").val()&&$("#seleMailT").val()&&!fnValidMail($("#seleMailA").val()+"@"+$("#seleMailT").val())) {alert("올바른 이메일 주소를 입력하세요,");$("#seleMailA").focus();return false;}
		if($.trim($("#authRole").val())==""){alert("권한구분을 선택하세요.");$("#authRole").focus();return false;}
		// if($.trim($("#userComm").val())==""){alert("소속을 선택하세요.");$("#userComm").focus();return false;}
		if(checkBoxArr.length==0){alert("소속을 선택하세요.");return false;}
		if(confirm("<%= alertMsg[3] %>")==0) return false;


		$("#frmDefault").attr({"action":"MemberCreate.do","method":"post","target":"_self"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn("seq");}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};
	var fnActChecker     = function(){
		var cVal = $("#userPNO").val();
		$("#msgResult").css({"color":"#000"}).html("");
		if($.trim($("#userPNO").val())==""){$("#msgResult").css({"color":"#00f"}).html("사용자ID를 입력하세요.");$("#userPNO").focus();return false;}
		if(($("#userPNO").val().search(/[0-9]/g)<0)&&($("#userPNO").val().search(/[a-zA-Z]/ig)<0)){$("#msgResult").css({"color":"#00f"}).html("사용자ID는 숫자와 영문자만 가능합니다.");$("#userPNO").focus();return false;}
		$("#frmDefault").attr({"action":"MemberFound.do","method":"post","target":"_self"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			success       : function(data,status,xhr  ){
				if(data.isExist=="Y"){$("#msgResult").html("");$("#userPNC").val(data.result);$("#userPNO").val(data.result);}else
				if(data.isExist=="T"){$("#msgResult").css({"color":"#f00"}).html("이미 등록된 ID 입니다.");$("#userPNC").val("");$("#userPNO").focus();
				}else{$("#msgResult").css({"color":"#c00"}).html("사용 가능한 ID 입니다.");$("#userPNC").val(cVal);$("#userPNO").val(cVal);}
			}, 
			error        : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='reAuthRole'    name='reAuthRole'    value='${reAuthRole}'/>
<input type='hidden' id='reAuthFlag'    name='reAuthFlag'    value='${reAuthFlag}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<input type='hidden' id='userComm'      name='userComm'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="1"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>계정 관리</a></li>
				<li><a href='<%= webBase %>/ptnhexa/User/MemberList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:38%;'/><col style='width:12%;'/><col style='width:38%;'/></colgroup>
					<tbody><tr>
						<th scope='col'>사용자ID *</th>
						<td colspan='3'><input type='hidden' id='userPNC' name='userPNC' maxlength='20'/><input type='text' id='userPNO' name='userPNO' class='w20_p' maxlength='20'/> &nbsp;<a class='btn_small bt_white border_1px' href='javascript:void(0);' onclick='fnActChecker()'>중복검색</a> &nbsp;<span id='msgResult' style='width:200px;color:#c00;'></span>&nbsp;<span class='info01 light_grey'> <font color='red'>* 비밀번호는 "<%= BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsPass" ) %>"로 초기화하여 저장합니다.</font></span></td>
					</tr><tr>
						<th>사용자명 *</th>
						<td colspan='3'><input type='text' id='userName' name='userName' class='w20_p' maxlength='30'/></td>
					</tr><tr><%--
						<th>비밀번호 *</th>
						<td colspan='3'><input type='password' id='userPWI' name='userPWI' class='w30_p' maxlength='20'<%=webUri.indexOf("localhost")<0||webUri.indexOf("127.0.0.1")<0?" value='qwe!@#123'":""%>/> <span class='info01 light_grey'> <font color='red'> * 숫자, 영문자, 특수문자 조합으로 8~20자리로 입력하세요.</font></span></td>
					</tr><tr>
						<th>비밀번호 확인 *</th>
						<td colspan='3'><input type='password' id='userPWC' name='userPWC' class='w30_p' maxlength='20'<%=webUri.indexOf("localhost")<0||webUri.indexOf("127.0.0.1")<0?" value='qwe!@#123'":""%>/></td>
					</tr><tr>--%><%--
						<th>사용자구분</th>
						<td colspan='3'><input type='hidden' id='userType' name='userType' readonly='readonly'/><input type='text' id='userTypeName' name='userTypeName' class='w30_p' readonly='readonly'/>&nbsp;<span class='info01 light_grey'> <font color='red'>* 해당 계정과 의원 내역이 연동됩니다.</font></span></td>--%>
					</tr><tr>
						<th>사용자메일</th>
						<td colspan='3'><input type='text' id='seleMailA' name='seleMailA' class='w20_p' maxlength='40'/>&nbsp;@&nbsp;
							<input type='text' id='seleMailT' name='seleMailT' class='w20_p' maxlength='40' readonly='readonly'/>
							<select id='mailSelect' name='mailSelect' class='w20_p'><option value=''            label='선택하세요'/>
								<c:if test="${!empty mailList}"><c:forEach var="mail" varStatus="status" items="${mailList}"><option value='${mail[1]}' label='${mail[0]}'/></c:forEach></c:if>
							</select>
						</td>
					</tr><tr>
						<th>비고</th>
						<td colspan='3'><input type='text' id='userDesc' Name='userDesc' class='w90_p' maxlength='200'/></td>
					</tr><tr>
						<th>권한구분 *</th>
						<td colspan='3'><select id='authRole' name='authRole' class='w20_p'><option value=''>선택</option>
							<c:choose><c:when test="${ empty authList}">
								<option value='CM'>최고관리자</option>
								<option value='GM'>업무담당자</option>
								<option value='GE'>일반담당자</option>
							</c:when><c:otherwise><c:forEach var="auth" varStatus="status" items="${authList}">
								<option value='${auth.autType}' >${auth.autName}</option>
							</c:forEach></c:otherwise></c:choose>
						</select></td>
					</tr><tr>
						<th>소속</th>
						<td colspan='3'>
							<%--<input type='checkbox' id='userComm00' name='userCommSel' value='00'/> <label for='userComm00'>의장</label>--%>
							<input type='checkbox' id='userComm01' name='userCommSel' value='01'/> <label for='userComm01'>의회운영위원회</label>
							<input type='checkbox' id='userComm02' name='userCommSel' value='02'/> <label for='userComm02'>기획행정위원회</label>
							<input type='checkbox' id='userComm03' name='userCommSel' value='03'/> <label for='userComm03'>복지환경위원회</label>
							<input type='checkbox' id='userComm04' name='userCommSel' value='04'/> <label for='userComm04'>산업건설위원회</label>
							<input type='checkbox' id='userComm06' name='userCommSel' value='06'/> <label for='userComm06'>의정팀</label>
							<input type='checkbox' id='userComm07' name='userCommSel' value='07'/> <label for='userComm07'>의사팀</label>
							<input type='checkbox' id='userComm08' name='userCommSel' value='08'/> <label for='userComm08'>홍보팀</label>
							<input type='checkbox' id='userComm09' name='userCommSel' value='09'/> <label for='userComm09'>정책지원팀</label>
							<input type='checkbox' id='userComm10' name='userCommSel' value='10'/> <label for='userComm10'>관리팀</label>

							<%--<select id='userComm' name='userComm' class='w20_p'>--%>
								<%--<option value=''>선택</option>--%>
								<%--<option value='00'>의장</option>--%>
								<%--<option value='01'>의회운영위원회</option>--%>
								<%--<option value='02'>기획행정위원회</option>--%>
								<%--<option value='03'>복지환경위원회</option>--%>
								<%--<option value='04'>산업건설위원회</option>--%>
								<%--<option value='99'>일반</option>--%>
							<%--</select>--%>
						</td>
					</tr><tr>
						<th>승인여부</th>
						<td colspan='3'>
							<select id='authFlag' name='authFlag' class='w20_p'>
								<option value='T'  >승인</option>
								<option value='F'  >미승인</option>
							</select>&nbsp;<span class='info01 light_grey'> <font color='red'>* 해당 계정의 승인여부를 결정합니다.</font></span>
						</td>
					</tr></tbody>
				</table>
				<div class='btn_area right'><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole)}">
					<input type='button' class='w70 btn_medium bt_red'    value='저장'       onclick='fnActCreate()'/></c:if>
					<input type='button' class='w70 btn_medium bt_grey'   value='목록'       onclick='fnActReturn("seq")'/>
				</div>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html><%	} %>