<%
////////////////////////////////////////////////////////////
// Program ID  : membStaff_view
// Description : 계정관리 > 담당자 관리 [상세조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<%@ page import="egovframework.ptcc.cmmn.vo.*" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "담당자 관리";
	BaseCommonVO userInfo		= (BaseCommonVO)request.getSession().getAttribute("MemRInfo");		// 관리자 로그인 내역
	boolean useNow				= false;
	String subFirst				= "";
	if(mngrAList!=null) for(int m=0;m<mngrAList.length;m++) if("T".equals(mngrAList[m][0])){subFirst=mngrAList[m][4];break;}

	if( !egovframework.ptcc.cmmn.util.BaseCategory.getCheckOfAdmin( userInfo.getAuthRole() ) ) { 
		out.println("<script>alert(\"접속권한이 없습니다.\\n\\n시스템 관리자에게 문의바랍니다.\");history.back();</script>");
	} else { 
		out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script>
	$(document).ready(function(){
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"Member"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self"}).submit();
	};
	var fnActDetail     = function(view){
		doActLoadShow();
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"MemberView.do","method":"post","target":"_self"}).submit();
	};<c:if test="${!empty dataView and ('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole)}">
	var fnActModify     = function(){
		doActLoadShow();
		$("#frmDefault").attr({"action":"MemberEdit.do","method":"post","target":"_self"}).submit();
	};<% if( useNow ) { %>
	var fnActChange     = function(){
		doActLoadShow();
		$("#frmDefault").attr({"action":"MemberPass.do","method":"post","target":"_self"}).submit();
	};<% } %>
	var fnActUpdate     = function(){
		var checkBoxArr = [];
		$("input[name=userCommSel]:checked").each(function (i) {
			checkBoxArr.push($(this).val());
		});
		$("#userComm").val(checkBoxArr);

		if($.trim($("#authRole").val())==""){alert("권한구분을 선택하세요.");$("#authRole").focus();return false;}
		// if($.trim($("#userComm").val())==""){alert("소속을 선택하세요.");$("#userComm").focus();return false;}
		if(checkBoxArr.length==0){alert("소속을 선택하세요.");return false;}
		if($.trim($("#authFlag").val())==""){alert("승인여부를 선택하세요.");$("#authFlag").focus();return false;}
		$("#frmDefault").attr({"action":"MemberModify.do","method":"post","target":"_self"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn("seq");}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};
	var fnActDelete     = function(){
		if(confirm("<%= alertMsg[4].replaceAll("[@]", "현재") %>") == 0) return false;
		$("#frmDefault").attr({"action":"MemberDelete.do","method":"post","target":"_self"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn("seq");}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};
	var fnActInitial    = function(){
		if(confirm("비밀번호를 \'<%= BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsPass" ) %>\'로 초기화합니다.\n\n계속 진행하시겠습니까?") == 0) return false;
		$("#frmDefault").attr({"action":"MemberInitial.do","method":"post","target":"_self"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn("seq");}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='reAuthRole'    name='reAuthRole'    value='${reAuthRole}'/>
<input type='hidden' id='reAuthFlag'    name='reAuthFlag'    value='${reAuthFlag}'/>
<input type='hidden' id='condDele'      name='condDele'      value='${dataView.userPID}'/>
<input type='hidden' id='userComm'      name='userComm'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="1"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>계정 관리</a></li>
				<li><a href='<%= webBase %>/ptnhexa/User/MemberList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:38%;'/><col style='width:12%;'/><col style='width:38%;'/></colgroup>
					<tbody><tr>
						<th scope='col'>사용자ID</th>
						<td colspan='3'><input type='text' class='w50_p' value='${dataView.userPID}' readonly='readonly'/></td>
					</tr><tr>
						<th>사용자명</th>
						<td colspan='3'><input type='text' class='w50_p' value='${dataView.userName}' readonly='readonly'/></td>
					</tr><tr><%--
						<th>사용자구분</th>
						<td colspan='3'><input type='text' class='w50_p' value='${dataView.userTypeName}' readonly='readonly'/> <span class='info01 light_grey'> <font color='red'>* 해당 계정과 의원 내역이 연동됩니다.</font></span></td>--%>
					</tr><tr>
						<th>사용자메일</th>
						<td colspan='3'><input type='text' class='w50_p' value='${dataView.userMail}' readonly='readonly'/></td>
					</tr><tr>
						<th>비고</th>
						<td colspan='3'><input type='text' class='w90_p' value='${dataView.userDesc}' readonly='readonly'/></td>
					</tr><tr>
						<th>권한구분</th>
						<td colspan='3'><select id='authRole' name='authRole' class='w20_p'><option value=''>선택</option>
							<c:choose><c:when test="${ empty authList}">
								<option value='CM'<c:if test="${'CM' eq dataView.authRole}"> selected='selected'</c:if>>최고관리자</option>
								<option value='GM'<c:if test="${'GM' eq dataView.authRole}"> selected='selected'</c:if>>업무담당자</option>
								<option value='GE'<c:if test="${'GE' eq dataView.authRole}"> selected='selected'</c:if>>일반담당자</option>
							</c:when><c:otherwise><c:forEach var="auth" varStatus="status" items="${authList}">
								<option value='${auth.autType}' <c:if test="${auth.autType eq dataView.authRole}"> selected='selected'</c:if>>${auth.autName}</option>
							</c:forEach></c:otherwise></c:choose>
						</select></td>
					</tr><tr>
						<th>소속</th>
						<td colspan='3'>
							<%--<input type='checkbox' id='userComm00' name='userCommSel' value='00' <c:if test = "${fn:contains(dataView.userComm, '00')}">checked</c:if> /> <label for='userComm00'>의장</label>--%>
							<input type='checkbox' id='userComm01' name='userCommSel' value='01' <c:if test = "${fn:contains(dataView.userComm, '01')}">checked</c:if> /> <label for='userComm01'>의회운영위원회</label>
							<input type='checkbox' id='userComm02' name='userCommSel' value='02' <c:if test = "${fn:contains(dataView.userComm, '02')}">checked</c:if> /> <label for='userComm02'>기획행정위원회</label>
							<input type='checkbox' id='userComm03' name='userCommSel' value='03' <c:if test = "${fn:contains(dataView.userComm, '03')}">checked</c:if> /> <label for='userComm03'>복지환경위원회</label>
							<input type='checkbox' id='userComm04' name='userCommSel' value='04' <c:if test = "${fn:contains(dataView.userComm, '04')}">checked</c:if> /> <label for='userComm04'>산업건설위원회</label>
							<input type='checkbox' id='userComm06' name='userCommSel' value='06' <c:if test = "${fn:contains(dataView.userComm, '06')}">checked</c:if> /> <label for='userComm06'>의정팀</label>
							<input type='checkbox' id='userComm07' name='userCommSel' value='07' <c:if test = "${fn:contains(dataView.userComm, '07')}">checked</c:if> /> <label for='userComm07'>의사팀</label>
							<input type='checkbox' id='userComm08' name='userCommSel' value='08' <c:if test = "${fn:contains(dataView.userComm, '08')}">checked</c:if> /> <label for='userComm08'>홍보팀</label>
							<input type='checkbox' id='userComm09' name='userCommSel' value='09' <c:if test = "${fn:contains(dataView.userComm, '09')}">checked</c:if> /> <label for='userComm09'>정책지원팀</label>
							<input type='checkbox' id='userComm10' name='userCommSel' value='10' <c:if test = "${fn:contains(dataView.userComm, '10')}">checked</c:if> /> <label for='userComm10'>관리팀</label>

							<%--<select id='userComm' name='userComm' class='w20_p'>--%>
								<%--<option value=''>선택</option>--%>
								<%--<option value='00' <c:if test="${'00' eq dataView.userComm}"> selected='selected'</c:if>>의장</option>--%>
								<%--<option value='01' <c:if test="${'01' eq dataView.userComm}"> selected='selected'</c:if>>의회운영위원회</option>--%>
								<%--<option value='02' <c:if test="${'02' eq dataView.userComm}"> selected='selected'</c:if>>기획행정위원회</option>--%>
								<%--<option value='03' <c:if test="${'03' eq dataView.userComm}"> selected='selected'</c:if>>복지환경위원회</option>--%>
								<%--<option value='04' <c:if test="${'04' eq dataView.userComm}"> selected='selected'</c:if>>산업건설위원회</option>--%>
								<%--<option value='99' <c:if test="${'99' eq dataView.userComm}"> selected='selected'</c:if>>일반</option>--%>
							<%--</select>--%>
						</td>
					</tr><tr>
						<th>승인여부</th>
						<td colspan='3'><c:choose><c:when test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole)}">
							<select id='authFlag' name='authFlag' class='w20_p'>
								<option value='T'  <c:if test="${'T' eq dataView.authFlag}"> selected='selected'</c:if>>승인</option>
								<option value='F'  <c:if test="${'T' ne dataView.authFlag}"> selected='selected'</c:if>>미승인</option>
							</select></c:when><c:otherwise>
							<select class='w20_p' readonly='readonly'><option value='${dataView.authFlag}'>${dataView.authFlagName}</option></select></c:otherwise></c:choose>
							<span class='info01 light_grey'> <font color='red'>* 해당 계정의 승인여부를 결정합니다.</font></span>
						</td>
					</tr></tbody>
				</table>
				<div class='btn_area right'><% if( useNow ) { %>
					<input type='button' class='w120 btn_medium bt_blue'  value='비밀번호 변경'   onclick='fnActChange()'/><% } %>
					<input type='button' class='w120 btn_medium bt_green' value='비밀번호 초기화' onclick='fnActInitial()'/><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole)}">
					<input type='button' class='w70 btn_medium bt_green'  value='적용'       onclick='fnActUpdate()'/>
					<input type='button' class='w70 btn_medium bt_blue'   value='수정'       onclick='fnActModify()'/>
					<input type='button' class='w70 btn_medium bt_red'    value='삭제'       onclick='fnActDelete()'/></c:if>
					<input type='button' class='w70 btn_medium bt_grey'   value='목록'       onclick='fnActReturn("seq")'/>
				</div>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html><%	} %>