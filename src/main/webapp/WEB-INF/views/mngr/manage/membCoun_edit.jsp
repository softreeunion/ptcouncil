<%
////////////////////////////////////////////////////////////
// Program ID  : membCoun_edit
// Description : 의원관리 > 의원 관리 [수정화면]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "의원 관리";
	String subFirst				= "";
	if(mngrCList!=null) for(int m=0;m<mngrCList.length;m++) if("T".equals(mngrCList[m][0])){subFirst=mngrCList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script>
	$(document).ready(function(){
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"Member"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self"}).submit();
	};<c:if test="${!empty dataView and ('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
	var fnActUpdate     = function(){
		if(confirm("<%= alertMsg[3] %>")==0) return false;
		$("#frmDefault").attr({"action":"MemberUpdate.do","method":"post","target":"_self"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn();}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='reEraCode'     name='reEraCode'     value='${reEraCode}'/>
<input type='hidden' id='reRegCode'     name='reRegCode'     value='${reRegCode}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="3"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>의원 관리</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Coun/MemberList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:38%;'/><col style='width:12%;'/><col style='width:38%;'/></colgroup>
					<tbody><tr>
						<th scope='col'>사용자ID *</th>
						<td colspan='3'><input type='text' id='userPNO' name='userPNO' class='w20_p' maxlength='20' value='${dataView.userPID}' value='${dataView.userPID}' readonly='readonly'/></td>
					</tr><tr>
						<th>사용자명 *</th>
						<td colspan='3'><input type='text' id='userName' name='userName' class='w20_p' maxlength='30' value='${dataView.userName}'/></td>
					</tr><tr>
						<th>사용자구분</th>
						<td colspan='3'><input type='hidden' id='userType' name='userType' value='${dataView.userType}' readonly='readonly'/><input type='text' id='userTypeName' name='userTypeName' class='w30_p' value='${dataView.userTypeName}' readonly='readonly'/>&nbsp;<span class='info01 light_grey'> <font color='red'>* 해당 의원과 의원 내역이 연동됩니다.</font></span></td>
					</tr><tr>
						<th>사용자메일</th>
						<td colspan='3'><input type='text' id='seleMailA' name='seleMailA' class='w20_p' maxlength='40' value='${dataView.seleMailA}'/>&nbsp;@&nbsp;
							<input type='text' id='seleMailT' name='seleMailT' class='w20_p' maxlength='40' value='${dataView.seleMailT}' readonly='readonly'/>
							<select id='mailSelect' name='mailSelect' class='w20_p'><option value=''            label='선택하세요'/>
								<c:if test="${!empty mailList}"><c:forEach var="mail" varStatus="status" items="${mailList}"><option value='${mail[1]}' label='${mail[0]}'<c:if test="${mail[1] eq dataView.seleMailT}"> selected</c:if>/></c:forEach></c:if>
							</select>
						</td>
					</tr><tr>
						<th>비고</th>
						<td colspan='3'><input type='text' id='userDesc' Name='userDesc' class='w90_p' maxlength='200' value='${dataView.userDesc}'/></td>
					</tr><tr>
						<th>권한구분 *</th>
						<td colspan='3'><select id='authRole' name='authRole' class='w20_p'><option value=''>선택</option>
							<c:choose><c:when test="${ empty authList}">
								<option value='CM'<c:if test="${'CM' eq dataView.authRole}"> selected='selected'</c:if>>최고관리자</option>
								<option value='GM'<c:if test="${'GM' eq dataView.authRole}"> selected='selected'</c:if>>업무의원</option>
								<option value='GE'<c:if test="${'GE' eq dataView.authRole}"> selected='selected'</c:if>>일반의원</option>
							</c:when><c:otherwise><c:forEach var="auth" varStatus="status" items="${authList}">
								<option value='${auth.autType}' <c:if test="${auth.autType eq dataView.authRole}"> selected='selected'</c:if>>${auth.autName}</option>
							</c:forEach></c:otherwise></c:choose>
							</select>
						</td>
					</tr><tr>
						<th>승인여부</th>
						<td colspan='3'><select id='authFlag' name='authFlag' class='w20_p'>
								<option value='T'  <c:if test="${'T' eq dataView.authFlag}"> selected='selected'</c:if>>승인</option>
								<option value='F'  <c:if test="${'T' ne dataView.authFlag}"> selected='selected'</c:if>>미승인</option>
							</select>&nbsp;<span class='info01 light_grey'> <font color='red'>* 해당 의원의 승인여부를 결정합니다.</font></span>
						</td>
					</tr></tbody>
				</table>
				<div class='btn_area right'><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
					<input type='button' class='w70 btn_medium bt_red'    value='저장'       onclick='fnActUpdate()'/></c:if>
					<input type='button' class='w70 btn_medium bt_grey'   value='취소'       onclick='fnActReturn()'/>
					<input type='button' class='w70 btn_medium bt_grey'   value='목록'       onclick='fnActReturn("seq")'/>
				</div>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>