<%
////////////////////////////////////////////////////////////
// Program ID  : reptSchd_list
// Description : 의정활동 > 의사일정 [목록조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "의사일정";
	String subFirst				= "";
	if(mngrDList!=null) for(int m=0;m<mngrDList.length;m++) if("T".equals(mngrDList[m][0])){subFirst=mngrDList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script>
	$(document).ready(function(){
	});
	var fnActInitial    = function(){
		$("#condType").val("A");
		$("#condValue").val("");
		fnActRetrieve();
	};
	var fnActRetrieve   = function(page){
		doActLoadShow();
		$("#pageCurNo").val(isEmpty(page)?"1":page);
		$("#frmDefault").attr({"action":"ReportList.do","method":"post","target":"_self"}).submit();
	};
	var fnActDetail     = function(view){
		doActLoadShow();
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"ReportView.do","method":"post","target":"_self"}).submit();
	};<c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
	var fnActCreate     = function(){
		doActLoadShow();
		$("#frmDefault").attr({"action":"ReportInst.do","method":"post","target":"_self"}).submit();
	};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="4"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>의정 활동</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Schd/ReportList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='search_area'>
				<table class='table table_cols'>
					<colgroup><col style='width:10%;'/><col style='width:23%;'/><col style='width:10%;'/><col style='width:23%;'/><col style='width:10%;'/><col style='width:24%;'/></colgroup>
					<tbody><tr>
						<th>검색</th>
						<td colspan='5'>
							<label for='condType' class='hide'>검색영역</label>
							<select id='condType' name='condType' class='w120'>
								<option value='A'  <c:if test="${'A'  eq condType}"> selected='selected'</c:if>>전체</option>
								<option value='T'  <c:if test="${'T'  eq condType}"> selected='selected'</c:if>>제목</option>
								<option value='D'  <c:if test="${'D'  eq condType}"> selected='selected'</c:if>>내용</option>
								<option value='N'  <c:if test="${'N'  eq condType}"> selected='selected'</c:if>>작성자</option>
							</select>&nbsp;&nbsp;
							<label for='condValue' class='hide'>검색어</label>
							<input type='text' id='condValue'     name='condValue'     class='w60_p' maxlength='30' value='${condValue}' placeholder='검색어를 입력하세요.'/>
						</td>
					</tr></tbody>
				</table>
				<div class='btn_area center'>
					<input type='button' class='w70 btn_large bt_grey' value='검색'       onclick='fnActRetrieve()'/>
				</div>
			</div>
			<div class='board_list'>
				<div class='board_total'>
					<span class='left'>전체 <strong class='red'>${listCnt}</strong> 건 &nbsp; ${pageCurNo} / ${pageInfo.totalPageCount}</span>
					<span class='right'><select id='pagePerNo'     name='pagePerNo' onchange='fnActRetrieve()'>
						<option value='10' <c:if test="${ '10' eq pagePerNo}"> selected='selected'</c:if>> 10개씩 보기</option>
						<option value='20' <c:if test="${ '20' eq pagePerNo}"> selected='selected'</c:if>> 20개씩 보기</option>
						<option value='30' <c:if test="${ '30' eq pagePerNo}"> selected='selected'</c:if>> 30개씩 보기</option>
						<option value='50' <c:if test="${ '50' eq pagePerNo}"> selected='selected'</c:if>> 50개씩 보기</option>
						<option value='100'<c:if test="${'100' eq pagePerNo}"> selected='selected'</c:if>>100개씩 보기</option>
						<option value='200'<c:if test="${'200' eq pagePerNo}"> selected='selected'</c:if>>200개씩 보기</option>
						<option value='all'<c:if test="${ '10' ne pagePerNo and  '20' ne pagePerNo and  '30' ne pagePerNo and  '50' ne pagePerNo and '100' ne pagePerNo and '200' ne pagePerNo}"> selected='selected'</c:if>>전체 보기</option>
					</select></span>
				</div>
				<div class='rows_table'><table class='table table_rows'>
					<caption class='hide'>Board List</caption>
					<colgroup><col style='width: 6%;'/><col style='width:54%;'/><col style='width:12%;'/><col style='width:10%;'/><col style='width:10%;'/><col style='width: 8%;'/></colgroup>
					<thead><tr>
						<th scope='col'>번호</th>
						<th scope='col'>제목</th>
						<th scope='col'>작성자</th>
						<th scope='col'>작성일</th>
						<th scope='col'>공개여부</th>
						<th scope='col'>조회수</th>
					</tr></thead>
					<tbody>
					<c:if test="${!empty notiList}"><c:forEach var="noti" varStatus="status" items="${notiList}">
						<tr class='on noti' onclick='fnActDetail("${noti.viewNo}")'>
							<td class='t_red'>공지</td>
							<td>${noti.bbsTitle}</td>
							<td>${noti.creName}</td>
							<td>${noti.creDate}</td>
							<td>${noti.creHideName}</td>
							<td>${noti.hitCount}</td>
						</tr>
					</c:forEach></c:if><c:choose><c:when test="${ empty dataList}">
						<tr class='off h200'><td colspan='6'><%= noneData %></td></tr>
					</c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${dataList}">
						<tr class='on' onclick='fnActDetail("${rult.viewNo}")'>
							<td>${rult.rowIdx}</td>
							<td class='left'>${rult.gabSpace}<c:if test="${'Y' eq rult.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${rult.bbsTitle}</td>
							<td>${rult.creName}</td>
							<td>${rult.creDate}</td>
							<td>${rult.creHideName}</td>
							<td>${rult.hitCount}</td>
						</tr>
					</c:forEach></c:otherwise></c:choose>
					</tbody>
				</table></div>
				<c:if test="${!empty pageInfo}"><div class='paging usr_pagination1'><ui:pagination paginationInfo="${pageInfo}" type="baseImage" jsFunction="fnActRetrieve" /></div></c:if>
			</div>
			<div class='btn_area right'><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
				<input type='button' class='w70 btn_medium bt_blue'   value='등록'       onclick='fnActCreate()'/></c:if>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>