<%
////////////////////////////////////////////////////////////
// Program ID  : reptGover_edit
// Description : 회의록관리 > 시정질문답변 [수정화면]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "시정질문답변";
	String subFirst				= "";
	if(mngrEList!=null) for(int m=0;m<mngrEList.length;m++) if("T".equals(mngrEList[m][0])){subFirst=mngrEList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incCalendar.jspf"/>
	<script>
	$(document).ready(function(){
		$("[id^='creOpen']").change(function(){$("#creHide").val($("[id^='creOpen']:checked").val());});
		$("#bbsTitle").focus();
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"Report"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self","enctype":""}).submit();
	};<c:if test="${!empty dataView and ('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
	var fnActUpdate     = function(){
		if($.trim($("#bbsTitle").val())==""){alert("제목은 필수입력입니다.");$("#bbsTitle").focus();return false;}
		if($("[id^='creOpen']:checked").length<=0){alert("공개여부는 필수선택입니다.");$("[id^='creOpen']:eq(0)").focus();return false;}
		if($.trim($("#regiName1").val())==""){alert("질문자는 필수입력입니다.");$("#regiName1").focus();return false;}
		if($.trim($("#confCode1").val())==""){alert("질문회의록은 필수입력입니다.");$("#confCode").focus();return false;}
		if($.trim($("#qstDate").val())==""){alert("질문일자는 필수입력입니다.");$("#qstDate").focus();return false;}
		if($.trim($("#contDesc").val())==""){alert("작성된 질문내용이 없습니다. 질문내용을 입력하세요.");$("#contDesc").focus();return false;}
		if($.trim($("#regiName2").val())==""){alert("답변자는 필수입력입니다.");$("#regiName2").focus();return false;}
		if($.trim($("#confCode2").val())==""){alert("답변회의록은 필수입력입니다.");$("#confCode2").focus();return false;}
		if($.trim($("#ansDate").val())==""){alert("답변일자는 필수입력입니다.");$("#ansDate").focus();return false;}
		if($.trim($("#contents2").val())==""){alert("작성된 답변내용이 없습니다. 답변내용을 입력하세요.");$("#contents2").focus();return false;}
		$("#frmDefault").attr({"action":"ReportUpdate.do","method":"post","target":"_self","enctype":"multipart/form-data"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			contentType   : false , 
			processData   : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			beforeSend    : function(){$(".progress").show();$(".progress-bar").width("0%");},
			uploadProgress: function(event,position,total,percentComplete){$(".progress-bar").width(percentComplete+"%");if(percentComplete==100){$(".progress").hide();};},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn();}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="5"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>회의록 관리</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Gover/ReportList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:38%;'/><col style='width:12%;'/><col style='width:38%;'/></colgroup>
					<tbody><tr>
						<th>제목 *</th>
						<td colspan='3'><input type='text' id='bbsTitle' name='bbsTitle' class='w90_p' maxlength='400' value='${dataView.bbsTitle}'/></td>
					</tr><tr>
						<th>공개여부 *</th>
						<td colspan='3'>
							<input type='hidden' id='creHide' name='creHide' value='${dataView.creHide}'/>
							<input type='radio' id='creOpen1' name='creOpen' value='Y'<c:if test="${'Y' eq dataView.creHide}"> checked='checked'</c:if>/> <label for='creOpen1'>비공개</label>
							<input type='radio' id='creOpen2' name='creOpen' value='N'<c:if test="${'Y' ne dataView.creHide}"> checked='checked'</c:if>/> <label for='creOpen2'>공개</label>
						</td>
					</tr><tr>
						<th>질문자 *</th>
						<td><input type='text' id='regiName1' name='regiName1' class='w200' maxlength='30' value='${dataView.creName}'/></td>
						<th>질문일자 *</th>
						<td><input type='text' id='qstDate' name='qstDate' class='w90 center' maxlength='8' value='${dataView.regiDate}' placeholder='<%= dateHolder %>'/></td>
					</tr><tr>
						<th>질문회의록 *</th>
						<td colspan='3'><input type='text' id='confCode1' name='confCode1' class='w200' maxlength='19' value='${dataView.confCode1}'/>&nbsp;<span class='info01 light_grey'> <font color='red'>* 회의록코드 19자리를 입력하세요.</font></span></td>
					</tr><tr>
						<th>질문내용 *</th>
						<td colspan='3'><textarea id='contDesc' name='contDesc' rows='16' class='w90_p'><c:choose><c:when test="${'P' eq dataView.contPattern}"><c:out value="${dataView.contDescCR}" escapeXml="true"/></c:when><c:otherwise><c:out value="${dataView.contDesc}" escapeXml="true"/></c:otherwise></c:choose></textarea></td>
					</tr></tbody>
				</table><c:choose><c:when test="${ empty dataSupp}">
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:38%;'/><col style='width:12%;'/><col style='width:38%;'/></colgroup>
					<tbody><tr>
						<th>답변자 *</th>
						<td><input type='text' id='regiName2' name='regiName2' class='w200' maxlength='200'/></td>
						<th>답변일자 *</th>
						<td><input type='text' id='ansDate' name='ansDate' class='w90 center' maxlength='8' placeholder='<%= dateHolder %>'/></td>
					</tr><tr>
						<th>답변회의록 *</th>
						<td colspan='3'><input type='text' id='confCode2' name='confCode2' class='w200' maxlength='19'/>&nbsp;<span class='info01 light_grey'> <font color='red'>* 회의록코드 19자리를 입력하세요.</font></span></td>
					</tr><tr>
						<th>답변내용 *</th>
						<td colspan='3'><textarea id='contents2' name='contents2' rows='16' class='w90_p'></textarea></td>
					</tr><tr>
						<th>첨부파일</th>
						<td colspan='3' id='file_base1'><input type='file' id='fileBase1' name='fileBase1' class='w90_p'/></td>
					</tr></tbody>
				</table></c:when><c:otherwise>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:38%;'/><col style='width:12%;'/><col style='width:38%;'/></colgroup>
					<tbody><c:forEach var="supp" varStatus="status" items="${dataSupp}"><c:if test="${status.first}">
					<tr>
						<th>답변자 *</th>
						<td><input type='text' id='regiName2' name='regiName2' class='w200' maxlength='200' value='${supp.regiName2}'/></td>
						<th>답변일자 *</th>
						<td><input type='text' id='ansDate' name='ansDate' class='w90 center' maxlength='8' value='${supp.regiDate2}' placeholder='<%= dateHolder %>'/></td>
					</tr><tr>
						<th>답변회의록 *</th>
						<td colspan='3'><input type='text' id='confCode2' name='confCode2' class='w200' maxlength='19' value='${supp.confCode2}'/>&nbsp;<span class='info01 light_grey'> <font color='red'>* 회의록코드 19자리를 입력하세요.</font></span></td>
					</tr><tr>
						<th>답변내용 *</th>
						<td colspan='3'><textarea id='contents2' name='contents2' rows='16' class='w90_p'><c:out value="${supp.contents2CR}" escapeXml="true"/></textarea></td>
					</tr><c:choose><c:when test="${ empty dataFile}"><tr>
						<th>첨부파일</th>
						<td colspan='3' id='file_base1'><input type='file' id='fileBase1' name='fileBase1' class='w90_p'/></td>
					</tr></c:when><c:otherwise><c:forEach var="file" varStatus="status" items="${dataFile}"><tr><th>첨부파일</th>
						<td colspan='3' id='file_base${status.count}'>
							<input type='button' id='fileDele${status.count}' class='w70 btn_small bt_grey' value='파일삭제' onclick='fcBaseRemove($(this),"${file.fileUUID}`")'/>&nbsp;&nbsp;
							<a href='javascript:void(0);' onclick='fnActDownload("${file.fileUUID}")'>${file.fileRel}</a>
						</td>
					</tr></c:forEach></c:otherwise></c:choose></c:if></c:forEach></tbody>
				</table></c:otherwise></c:choose>
				<div class='search_area'><input type='button' id='fileAdd03' class='w70 btn_small bt_blue' value='파일추가'/> &nbsp; <%= allowFiles[3] %></div>
				<div class='btn_area right'><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
					<input type='button' class='w70 btn_medium bt_red'    value='저장'       onclick='fnActUpdate()'/></c:if>
					<input type='button' class='w70 btn_medium bt_grey'   value='취소'       onclick='fnActReturn()'/>
					<input type='button' class='w70 btn_medium bt_grey'   value='목록'       onclick='fnActReturn("seq")'/>
				</div>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>