<%
////////////////////////////////////////////////////////////
// Program ID  : reptPress_edit
// Description : 게시판관리 > 언론보도 [수정화면]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "의원 인터뷰";
	String subFirst				= "";
	if(mngrFList!=null) for(int m=0;m<mngrFList.length;m++) if("T".equals(mngrFList[m][0])){subFirst=mngrFList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script>
	$(document).ready(function(){
		$("[id^='creOpen']").change(function(){$("#creHide").val($("[id^='creOpen']:checked").val());});
		$("#bbsTitle").focus();
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"Report"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self","enctype":""}).submit();
	};<c:if test="${!empty dataView and ('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
	var fnActUpdate     = function(){
		if($.trim($("#bbsTitle").val())==""){alert("제목은 필수입력입니다.");$("#bbsTitle").focus();return false;}
		if($("[id^='creOpen']:checked").length<=0){alert("공개여부는 필수선택입니다.");$("[id^='creOpen']:eq(0)").focus();return false;}
		if($.trim($("#contDesc").val())==""){alert("작성된 내용이 없습니다. 내용을 입력하세요.");$("#contDesc").focus();return false;}
		$("#frmDefault").attr({"action":"ReportUpdate.do","method":"post","target":"_self","enctype":"multipart/form-data"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			contentType   : false , 
			processData   : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			beforeSend    : function(){$(".progress").show();$(".progress-bar").width("0%");},
			uploadProgress: function(event,position,total,percentComplete){$(".progress-bar").width(percentComplete+"%");if(percentComplete==100){$(".progress").hide();};},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn();}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="6"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>게시판 관리</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Press/ReportList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:38%;'/><col style='width:12%;'/><col style='width:38%;'/></colgroup>
					<tbody><tr>
						<th>제목 *</th>
						<td colspan='3'><input type='text' id='bbsTitle' name='bbsTitle' class='w90_p' maxlength='400' value='${dataView.bbsTitle}'/></td>
					</tr><tr>
						<th>공개여부 *</th>
						<td colspan='3'>
							<input type='hidden' id='creHide' name='creHide' value='${dataView.creHide}'/>
							<input type='radio' id='creOpen1' name='creOpen' value='Y'<c:if test="${'Y' eq dataView.creHide}"> checked='checked'</c:if>/> <label for='creOpen1'>비공개</label>
							<input type='radio' id='creOpen2' name='creOpen' value='N'<c:if test="${'Y' ne dataView.creHide}"> checked='checked'</c:if>/> <label for='creOpen2'>공개</label>
						</td>
					</tr><tr>
						<th scope='col'>작성자</th>
						<td><input type='text' class='w70_p' value='${dataView.creName}' readonly='readonly'/></td>
						<th scope='col'>작성일</th>
						<td><input type='text' class='w70_p' value='${dataView.creDate}' readonly='readonly'/></td>
					</tr><tr>
						<th>Link 주소 *</th>
						<td colspan='3'><input type='text' id='contDesc' name='contDesc' class='w90_p' maxlength='400' value='${dataView.contDesc}'/></td>
					</tr><c:choose><c:when test="${ empty dataFile}"><tr>
						<th>첨부파일</th>
						<td colspan='3' id='file_base1'><input type='file' id='fileBase1' name='fileBase1' class='w90_p'/></td>
					</tr></c:when><c:otherwise><c:forEach var="file" varStatus="status" items="${dataFile}"><tr><th>첨부파일</th>
						<td colspan='3' id='file_base${status.count}'>
							<input type='button' id='fileDele${status.count}' class='w70 btn_small bt_grey' value='파일삭제' onclick='fcBaseRemove($(this),"${file.fileUUID}`")'/>&nbsp;&nbsp;
							<a href='javascript:void(0);' onclick='fnActDownload("${file.fileUUID}")'>${file.fileRel}</a>
						</td>
					</tr></c:forEach></c:otherwise></c:choose></tbody>
				</table>
				<div class='search_area'><input type='button' id='fileAdd03' class='w70 btn_small bt_blue' value='파일추가'/> &nbsp; <%= allowFiles[3] %></div>
				<div class='btn_area right'><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
					<input type='button' class='w70 btn_medium bt_red'    value='저장'       onclick='fnActUpdate()'/></c:if>
					<input type='button' class='w70 btn_medium bt_grey'   value='취소'       onclick='fnActReturn()'/>
					<input type='button' class='w70 btn_medium bt_grey'   value='목록'       onclick='fnActReturn("seq")'/>
				</div>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>