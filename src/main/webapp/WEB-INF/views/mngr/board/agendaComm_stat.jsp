<%
////////////////////////////////////////////////////////////
// Program ID  : agendaComm_stat
// Description : 회의록관리 > 의안통계(위원회별) [목록조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "의안통계-위원회별";
	String subFirst				= "";
	if(mngrEList!=null) for(int m=0;m<mngrEList.length;m++) if("T".equals(mngrEList[m][0])){subFirst=mngrEList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script>
	$(document).ready(function(){
	});
	var fnActRetrieve   = function(page) { 
		doActLoadShow();
		$("#pageCurNo").val(isEmpty(page)?"1":page);
		$("#frmDefault").attr({"action":"AgendaStat.do","method":"post","target":"_self"}).submit();
	};<c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
	var fnActExport     = function() { <c:choose><c:when test="${ empty dataList}">
	<%= alertMsg[6].replaceAll("[@]", "엑셀 출력") %></c:when><c:otherwise>
	var $fileDNmodal = $("#fileDN-modal");
	$fileDNmodal.attr({"title":"엑셀 다운로드 중..."});
	$fileDNmodal.dialog({modal:true});
	$("#progressBarDN").progressbar({value:false});
	$.fileDownload("AgendaExportStat.do", { 
		httpMethod        : "POST", 
		dataType          : "json", 
		data              : $("#frmDefault").serialize(), 
		abortCallBack     : function(uri) {$fileDNmodal.dialog("close");}, 
		successCallback   : function(uri) {$fileDNmodal.dialog("close");}, 
		failCallback      : function(resTag,uri) {$fileDNmodal.dialog("close"); $("#errorDN-modal").dialog({modal:true});}
	});
	return false;<%-- 버튼의 원래 클릭 이벤트를 중지 시키기 위해 필요합니다. --%></c:otherwise></c:choose>
};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="5"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>회의록 관리</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Kind/AgendaStat.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='search_area'>
				<table class='table table_cols'>
					<colgroup><col style='width:10%;'/><col style='width:23%;'/><col style='width:10%;'/><col style='width:23%;'/><col style='width:10%;'/><col style='width:24%;'/></colgroup>
					<tbody><tr>
						<th scope='col' class='center'>대수</th>
						<td colspan='5'><select id='reEraCode' name='reEraCode' class='w100' onchange='fnActRetrieve()'>
						<c:choose><c:when test="${ empty eraList}">
							<option value='08'<c:if test="${'08' eq reEraCode}"> selected='selected'</c:if>>제8대</option>
						</c:when><c:otherwise><c:forEach var="cate" varStatus="status" items="${eraList}">
							<option value='${cate.eraCode}'<c:if test="${cate.eraCode eq reEraCode}"> selected='selected'</c:if>>${cate.eraName}</option>
						</c:forEach></c:otherwise></c:choose>
						</select></td>
					</tr></tbody>
				</table>
				<div class='btn_area center'>
					<input type='button' class='w70 btn_large bt_grey' value='검색'       onclick='fnActRetrieve()'/>
				</div>
			</div>
			<div class='board_list'>
				<div class='rows_table'><table class='table table_rows'>
					<caption class='hide'>Board List</caption>
					<colgroup>
						<col style='width:16%;'/>
						<col style='width:6%;'/><col style='width:6%;'/><col style='width:6%;'/><col style='width:6%;'/>
						<col style='width:6%;'/><col style='width:6%;'/><col style='width:6%;'/><col style='width:6%;'/>
						<col style='width:6%;'/><col style='width:6%;'/><col style='width:6%;'/><col style='width:6%;'/>
						<col style='width:6%;'/><col style='width:6%;'/>
					</colgroup>
					<thead><tr>
						<th scope='col' rowspan='2'>구분</th>
						<th scope='col' rowspan='2'>본회의<br/>직상</th>
						<th colspan='2'>가결</th>
						<th colspan='2'>의결</th>
						<th scope='col' rowspan='2'>부분<br/>승인</th>
						<th scope='col' rowspan='2'>보류</th>
						<th scope='col' rowspan='2'>미료</th>
						<th scope='col' rowspan='2'>부결</th>
						<th scope='col' rowspan='2'>철회</th>
						<th scope='col' rowspan='2'>계류중</th>
						<th scope='col' rowspan='2'>코드<br/>없음</th>
						<th scope='col' rowspan='2'>빈값</th>
						<th scope='col' rowspan='2'>미지정</th>
					</tr><tr>
						<th scope='col'>원안</th>
						<th scope='col'>수정</th>
						<th scope='col'>원안</th>
						<th scope='col'>수정</th>
					</tr></thead>
					<tbody>
					<c:choose><c:when test="${ empty dataList}">
						<tr class='off h200'><td colspan='15'><%= noneData %></td></tr>
					</c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${dataList}">
						<tr class='off'>
							<td${rult.fontColor}>${rult.rultName}</td>
							<td${rult.fontColor}>${rult.cod0010}</td>
							<td${rult.fontColor}>${rult.cod0030}</td>
							<td${rult.fontColor}>${rult.cod0050}</td>
							<td${rult.fontColor}>${rult.cod0020}</td>
							<td${rult.fontColor}>${rult.cod0040}</td>
							<td${rult.fontColor}>${rult.cod0051}</td>
							<td${rult.fontColor}>${rult.cod0052}</td>
							<td${rult.fontColor}>${rult.cod0053}</td>
							<td${rult.fontColor}>${rult.cod0060}</td>
							<td${rult.fontColor}>${rult.cod0070}</td>
							<td${rult.fontColor}>${rult.cod0080}</td>
							<td${rult.fontColor}>${rult.cod0000}</td>
							<td${rult.fontColor}>${rult.codSpace}</td>
							<td${rult.fontColor}>${rult.codNull}</td>
						</tr>
					</c:forEach></c:otherwise></c:choose>
					</tbody>
				</table></div>
			</div>
			<div class='btn_area right'><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
				<input type='button' class='w70 btn_medium bt_grey'   value='엑셀다운'   onclick='fnActExport()'/>
			</c:if></div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>