<%
////////////////////////////////////////////////////////////
// Program ID  : attendSeat_inst
// Description : 의정활동 > 방청방문 관리 [등록화면]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "방청방문 관리";
	String subFirst				= "";
	boolean useNow				= true;
	if(mngrFList!=null) for(int m=0;m<mngrFList.length;m++) if("T".equals(mngrFList[m][0])){subFirst=mngrFList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incCalendar.jspf"/>
	<script>
	$(document).ready(function(){
		$("#reBgnDate").datepicker("destroy");
		$("#reEndDate").datepicker("destroy");
		$("#creName").focus();
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"Attend"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self","enctype":""}).submit();
	};<% if( useNow ) { %><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
	var fnActUpdate     = function(){
		if($("[id^='reqType']:checked").length<=0){alert("신청구분은 필수선택입니다.");$("[id^='reqType']:eq(0)").focus();return false;}
		if($("[id^='vstType']:checked").length<=0){alert("방문구분은 필수선택입니다.");$("[id^='vstType']:eq(0)").focus();return false;}
		if($.trim($("#creName").val())==""){alert("신청자은 필수입력입니다.");$("#creName").focus();return false;}
		if($.trim($("#vstDate").val())==""){alert("방문일자은 필수입력입니다.");$("#vstDate").focus();return false;}
		if($.trim($("#vstTimeH").val())==""){alert("방문시간은 필수선택입니다.");$("#vstTimeH").focus();return false;}
		if($.trim($("#vstTimeM").val())==""){alert("방문시간은 필수선택입니다.");$("#vstTimeM").focus();return false;}
		if($.trim($("#vstDesc").val())==""){alert("방목목적은 필수입력입니다.");$("#vstDesc").focus();return false;}
		if($.trim($("#vstPerson").val())==""){alert("방문인원은 필수입력입니다.");$("#vstPerson").focus();return false;}
		$("#frmDefault").attr({"action":"AttendCreate.do","method":"post","target":"_self","enctype":"multipart/form-data"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			contentType   : false , 
			processData   : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn("seq");}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};</c:if><% } %>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<input type='hidden' id='reBgnDate'     name='reBgnDate'     value='${reBgnDate}'/>/>
<input type='hidden' id='reEndDate'     name='reEndDate'     value='${reEndDate}'/>/>
<input type='hidden' id='reUseStat'     name='reUseStat'     value='${reUseStat}'/>/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="4"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>의정활동</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Base/NoticeList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:88%;'/></colgroup>
					<tbody><tr>
						<th scope='col'>신청구분 *</th>
						<td>
							<input type='radio' id='reqType1' name='reqType' value='CE'/> <label for='reqType1'>시의회 - 방청</label>
							<input type='radio' id='reqType2' name='reqType' value='CV'/> <label for='reqType2'>시의회 - 방문</label><%--
							<input type='radio' id='reqType3' name='reqType' value='YE'/> <label for='reqType3'>청소년 - 방청</label>
							<input type='radio' id='reqType4' name='reqType' value='YV'/> <label for='reqType4'>청소년 - 방문</label>--%>
						</td>
					</tr><tr>
						<th scope='col'>방문구분 *</th>
						<td>
							<input type='radio' id='vstType1' name='vstType' value='P'/> <label for='vstType1'>개인</label>
							<input type='radio' id='vstType2' name='vstType' value='G'/> <label for='vstType2'>단체</label>
						</td>
					</tr><tr>
						<th>신청자 *</th>
						<td><input type='text' id='creName' name='creName' class='w200' maxlength='30'/></td>
					</tr><tr>
						<th>방문일시 *</th>
						<td>
							<input type='text' id='vstDate' name='vstDate' class='w90 center' maxlength='8' placeholder='<%= dateHolder %>'/> &nbsp; &nbsp;
							<select id='vstTimeH' name='vstTimeH' class='w100 center'><option value=''>선택(시)</option><c:forEach var="time" varStatus="status" begin="9" end="18"><fmt:formatNumber value="${status.current   }" pattern="00" var="toTime"/><option value='${toTime}'>${toTime}</option></c:forEach></select> 시 &nbsp;
							<select id='vstTimeM' name='vstTimeM' class='w100 center'><option value=''>선택(분)</option><c:forEach var="time" varStatus="status" begin="0" end="5" ><fmt:formatNumber value="${status.current*10}" pattern="00" var="toTime"/><option value='${toTime}'>${toTime}</option></c:forEach></select> 분 &nbsp;
						</td>
					</tr><tr>
						<th>방문목적 *</th>
						<td><textarea id='vstDesc' name='vstDesc' rows='6' class='w90_p' maxlength='400'></textarea></td>
					</tr><tr>
						<th>방문인원 *</th>
						<td><input type='number' id='vstPerson' name='vstPerson' min='0' max='100' class='w60 center'/> 명</td>
					</tr></tbody>
				</table>
				<div class='btn_area right'><% if( useNow ) { %><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
					<input type='button' class='w70 btn_medium bt_red'    value='저장'       onclick='fnActUpdate()'/></c:if><% } %>
					<input type='button' class='w70 btn_medium bt_grey'   value='목록'       onclick='fnActReturn("seq")'/>
				</div>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>