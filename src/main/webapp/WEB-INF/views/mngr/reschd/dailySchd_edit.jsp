<%
////////////////////////////////////////////////////////////
// Program ID  : dailySchd_edit
// Description : 의정활동 > 통합 일정 [수정화면]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "통합 일정";
	String subFirst				= "";
	if(mngrDList!=null) for(int m=0;m<mngrDList.length;m++) if("T".equals(mngrDList[m][0])){subFirst=mngrDList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incCalendar.jspf"/>
	<script>
	$(document).ready(function(){
		$("[id^='schdTypW']").change(function(){$("[id^='schdTypW']").each(function(idx){if(idx==0){$("#schdType").val("");};$("#schdType").val($("#schdType").val()+($(this).prop("checked")?$(this).val():"")+";");});});
		$("#reStdDate").datepicker("destroy");
		$("#schdTitle").focus();
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		
		$("#frmDefault").attr({"action":"${nextMode}"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self","enctype":""}).submit();
	};<c:if test="${!empty dataView and ('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
	var fnActUpdate     = function(){
		if($.trim($("#schdTitle").val())==""){alert("일정명칭은 필수입력입니다.");$("#schdTitle").focus();return false;}
		if($.trim($("#schdSTitle").val())==""){alert("일정명칭(약식)은 필수입력입니다.");$("#schdSTitle").focus();return false;}
		if($.trim($("#schdBDate").val())==""){alert("기간은 필수입력입니다.");$("#schdBDate").focus();return false;}
		if($.trim($("#schdEDate").val())==""){alert("기간은 필수입력입니다.");$("#schdEDate").focus();return false;}
		if($.trim($("#schdBTimH").val())==""){alert("시간은 필수입력입니다.");$("#schdBTimH").focus();return false;}
		if($.trim($("#schdBTimM").val())==""){alert("시간은 필수입력입니다.");$("#schdBTimM").focus();return false;}
		if($.trim($("#schdETimH").val())==""){alert("시간은 필수입력입니다.");$("#schdETimH").focus();return false;}
		if($.trim($("#schdETimM").val())==""){alert("시간은 필수입력입니다.");$("#schdETimM").focus();return false;}
		if($.trim($("#schdBDate").val())==$.trim($("#schdEDate").val())&&$.trim($("#schdBTimH").val())+$.trim($("#schdBTimM").val())>$.trim($("#schdETimH").val())+$.trim($("#schdETimM").val())){alert("시작시간이 종료시간보다 큰 시간을 선택했습니다. 다시 선택해주시기 바랍니다.");return false;}
		if($("[id^='schdTypW']:checked").length<=0){alert("구분은 필수선택입니다.");$("[id^='schdTypW']:eq(0)").focus();return false;}
		$("#frmDefault").attr({"action":"${nextMode}Update.do","method":"post","target":"_self","enctype":"multipart/form-data"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn();}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<input type='hidden' id='reStdStat'     name='reStdStat'     value='${reStdStat}'/>
<input type='hidden' id='reStdDate'     name='reStdDate'     value='${reStdDate}'/>
<input type='hidden' id='reStdWeek'     name='reStdWeek'     value='${reStdWeek}'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="4"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>의정 활동</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Schd/${nextMode}List.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:88%;'/></colgroup>
					<tbody><tr>
						<th scope='col'>일정명칭 *</th>
						<td><input type='text' id='schdTitle' name='schdTitle' class='w90_p' maxlength='400' value='${dataView.schdTitle}'/></td>
					</tr><tr>
						<th>일정명칭(약식) *</th>
						<td><input type='text' id='schdSTitle' name='schdSTitle' class='w40_p' maxlength='100' value='${dataView.schdSTitle}'/>&nbsp;<span class='info01 light_grey'> <font color='red'>* 일정표에 표시할 내용을 입력합니다.</font></span></td>
					</tr><tr>
						<th>기간 *</th>
						<td>
							<input type='text' id='schdBDate' name='schdBDate' class='w90 center' maxlength='8' value='${dataView.schdBDate}' placeholder='<%= dateHolder %>'/> &nbsp;~ &nbsp;
							<input type='text' id='schdEDate' name='schdEDate' class='w90 center' maxlength='8' value='${dataView.schdEDate}' placeholder='<%= dateHolder %>'/> &nbsp; &nbsp;
						</td>
					</tr><tr>
						<th>시간 *</th>
						<td>
							<select id='schdBTimH' name='schdBTimH' class='w50'><c:forEach var="time" varStatus="status" begin="9" end="23"><fmt:formatNumber value="${status.current   }" pattern="00" var="toTime"/><option value='${toTime}'<c:if test="${toTime eq schdBTimH}"> selected='selected'</c:if>>${toTime}</option></c:forEach></select> 시 &nbsp;
							<select id='schdBTimM' name='schdBTimM' class='w50'><c:forEach var="time" varStatus="status" begin="0" end="5" ><fmt:formatNumber value="${status.current*10}" pattern="00" var="toTime"/><option value='${toTime}'<c:if test="${toTime eq schdBTimM}"> selected='selected'</c:if>>${toTime}</option></c:forEach></select> 분 &nbsp;~ &nbsp;
							<select id='schdETimH' name='schdETimH' class='w50'><c:forEach var="time" varStatus="status" begin="0" end="23"><fmt:formatNumber value="${status.current   }" pattern="00" var="toTime"/><option value='${toTime}'<c:if test="${toTime eq schdETimH}"> selected='selected'</c:if>>${toTime}</option></c:forEach></select> 시 &nbsp;
							<select id='schdETimM' name='schdETimM' class='w50'><c:forEach var="time" varStatus="status" begin="0" end="5" ><fmt:formatNumber value="${status.current*10}" pattern="00" var="toTime"/><option value='${toTime}'<c:if test="${toTime eq schdETimM}"> selected='selected'</c:if>>${toTime}</option></c:forEach></select> 분 &nbsp;
							<span class='info01 light_grey'> <font color='red'>* 시간은 하루일정 기준입니다.</font></span>
						</td>
					</tr><tr>
						<th>구분 *</th>
						<td>
							<input type='hidden' id='schdType' name='schdType' value='${dataView.schdType}'/>
							<table><tr>
								<td class='nonebd nonemg'><input type='checkbox' id='schdTypW1' name='schdTypW' value='CL'<c:if test="${'CL' eq schdTyCL}"> checked='checked'</c:if>/> <label for='schdTypW1'>생 방 송 (본회의)</label></td>
								<td class='nonebd nonemg'><input type='checkbox' id='schdTypW2' name='schdTypW' value='OL'<c:if test="${'OL' eq schdTyOL}"> checked='checked'</c:if>/> <label for='schdTypW2'>생 방 송 (의회운영위원회)</label></td>
								<td class='nonebd nonemg'><input type='checkbox' id='schdTypW3' name='schdTypW' value='GL'<c:if test="${'GL' eq schdTyGL}"> checked='checked'</c:if>/> <label for='schdTypW3'>생 방 송 (자치행정위원회)</label></td>
								<td class='nonebd nonemg'><input type='checkbox' id='schdTypW10' name='schdTypW' value='WL'<c:if test="${'WL' eq schdTyWL}"> checked='checked'</c:if>/> <label for='schdTypW10'>생 방 송 (복지환경위원회)</label></td>
								<td class='nonebd nonemg'><input type='checkbox' id='schdTypW4' name='schdTypW' value='BL'<c:if test="${'BL' eq schdTyBL}"> checked='checked'</c:if>/> <label for='schdTypW4'>생 방 송 (산업건설위원회)</label></td>
							</tr><tr>
								<td class='nonebd nonemg'><input type='checkbox' id='schdTypW5' name='schdTypW' value='CS'<c:if test="${'CS' eq schdTyCS}"> checked='checked'</c:if>/> <label for='schdTypW5'>의사일정 (시의회)</label></td>
								<td class='nonebd nonemg'><input type='checkbox' id='schdTypW6' name='schdTypW' value='OS'<c:if test="${'OS' eq schdTyOS}"> checked='checked'</c:if>/> <label for='schdTypW6'>의사일정 (의회운영위원회)</label></td>
								<td class='nonebd nonemg'><input type='checkbox' id='schdTypW7' name='schdTypW' value='GS'<c:if test="${'GS' eq schdTyGS}"> checked='checked'</c:if>/> <label for='schdTypW7'>의사일정 (자치행정위원회)</label></td>
								<td class='nonebd nonemg'><input type='checkbox' id='schdTypW11' name='schdTypW' value='WS'<c:if test="${'WS' eq schdTyWS}"> checked='checked'</c:if>/> <label for='schdTypW11'>의사일정 (복지환경위원회)</label></td>
								<td class='nonebd nonemg'><input type='checkbox' id='schdTypW8' name='schdTypW' value='BS'<c:if test="${'BS' eq schdTyBS}"> checked='checked'</c:if>/> <label for='schdTypW8'>의사일정 (산업건설위원회)</label></td>
							</tr><tr>
								<td class='nonebd nonemg'><input type='checkbox' id='schdTypW9' name='schdTypW' value='CW'<c:if test="${'CW' eq schdTyCW}"> checked='checked'</c:if>/> <label for='schdTypW9'>방청견학</label></td>
								<td class='nonebd nonemg'></td>
								<td class='nonebd nonemg'></td>
								<td class='nonebd nonemg'></td>
							</tr></table>
						</td>
					</tr><tr>
						<th>장소</th>
						<td><input type='text' id='schdPlace' name='schdPlace' class='w90_p' maxlength='200' value='${dataView.schdPlace}'/></td>
					</tr><tr>
						<th>내용</th>
						<td><textarea id='schdDesc' name='schdDesc' rows='8' class='w90_p' maxlength='4000'>${dataView.schdDescCR}</textarea></td>
					</tr></tbody>
				</table>
				<div class='btn_area right'><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
					<input type='button' class='w70 btn_medium bt_red'    value='저장'       onclick='fnActUpdate()'/></c:if>
					<input type='button' class='w70 btn_medium bt_grey'   value='취소'       onclick='fnActReturn()'/>
					<input type='button' class='w70 btn_medium bt_grey'   value='목록'       onclick='fnActReturn("seq")'/>
				</div>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>