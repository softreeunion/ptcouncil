<%
////////////////////////////////////////////////////////////
// Program ID  : monthSchd_list
// Description : 의정활동 > 통합 일정(월별) [목록조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "통합 일정 (월별)";
	String subFirst				= "";
	if(mngrDList!=null) for(int m=0;m<mngrDList.length;m++) if("T".equals(mngrDList[m][0])){subFirst=mngrDList[m][4];break;}

	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script>
	$(document).ready(function(){
		$(".pnt").mouseover(function(){$(this).addClass("td_udln");});
		$(".pnt").mouseout(function(){$(this).removeClass("td_udln");});
	});
	var fnActInitial    = function(){
		$("#condType").val("A");
		$("#condValue").val("");
		fnActRetrieve();
	};
	var fnActRetrieve   = function(date,stat){
		doActLoadShow();
		$("#reStdDate").val(isEmpty(date)?"${reCurrDate}":date);
		$("#reStdStat").val(isEmpty(stat)?"C":stat);
		$("#frmDefault").attr({"action":"MonthList.do","method":"post","target":"_self"}).submit();
	};
	var fnActDetail     = function(view){
		doActLoadShow();
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"MonthView.do","method":"post","target":"_self"}).submit();
	};<c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
	var fnActCreate     = function(){
		doActLoadShow();
		$("#frmDefault").attr({"action":"MonthInst.do","method":"post","target":"_self"}).submit();
	};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='reStdStat'     name='reStdStat'     value='${reStdStat}'/>
<input type='hidden' id='reStdDate'     name='reStdDate'     value='${reStdDate}'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="4"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>의정 활동</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Schd/MonthList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='search_area'>
				<table class='table table_cols'>
					<colgroup><col style='width:20%;'/><col style='width:20%;'/><col style='width:50%;'/><col style='width:10%;'/></colgroup>
					<tbody><tr>
						<th>${dispDate}</th>
						<td class='right' colspan='2'>
							<input type='button' class='w90 btn_large bt_grey' value='이번 달'    onclick='fnActRetrieve("${reCurrDate}","C" )' style='cursor:pointer;'/> &nbsp; &nbsp; &nbsp;
							<input type='button' class='w90 btn_large bt_grey' value='이전 해'    onclick='fnActRetrieve("${reStdDate }","PY")' style='cursor:pointer;'/>
							<input type='button' class='w90 btn_large bt_grey' value='이전 달'    onclick='fnActRetrieve("${reStdDate }","PM")' style='cursor:pointer;'/>
							<input type='button' class='w90 btn_large bt_grey' value='다음 달'    onclick='fnActRetrieve("${reStdDate }","NM")' style='cursor:pointer;'/>
							<input type='button' class='w90 btn_large bt_grey' value='다음 해'    onclick='fnActRetrieve("${reStdDate }","NY")' style='cursor:pointer;'/>
						</td>
						<td></td>
					</tr></tbody>
				</table>
			</div>
			<div class='board_list'>
				<div class='rows_table'><table class='table table_rows'>
					<caption class='hide'>Board List</caption>
					<colgroup><col style='width:10%;'/><col style='width:16%;'/><col style='width:16%;'/><col style='width:16%;'/><col style='width:16%;'/><col style='width:16%;'/><col style='width:16%;'/></colgroup>
					<thead><tr>
						<th scope='col' style='color:#f00;'>일</th>
						<th scope='col'>월</th>
						<th scope='col'>화</th>
						<th scope='col'>수</th>
						<th scope='col'>목</th>
						<th scope='col'>금</th>
						<th scope='col' style='color:#00f;'>토</th>
					</tr></thead>
					<tbody><c:choose><c:when test="${ empty dispDocu}"><tr class='off h200'><td colspan='7'><%= noneData %></td></tr></c:when><c:otherwise>${dispDocu}</c:otherwise></c:choose></tbody>
				</table></div>
			</div>
			<div class='btn_area right'><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
				<input type='button' class='w70 btn_medium bt_blue'   value='등록'       onclick='fnActCreate()'/></c:if>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>