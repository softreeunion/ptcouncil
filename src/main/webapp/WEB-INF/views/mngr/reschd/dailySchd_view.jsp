<%
////////////////////////////////////////////////////////////
// Program ID  : dailyScnd_view
// Description : 의정활동 > 통합 일정 [상세조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "통합 일정";
	boolean viewReply			= false;
	String subFirst				= "";
	if(mngrDList!=null) for(int m=0;m<mngrDList.length;m++) if("T".equals(mngrDList[m][0])){subFirst=mngrDList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script>
	$(document).ready(function(){
		$("#reStdDate").datepicker("destroy");
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"${nextMode}"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self"}).submit();
	};
	var fnActDetail     = function(view){
		doActLoadShow();
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"${nextMode}View.do","method":"post","target":"_self"}).submit();
	};<c:if test="${!empty dataView and ('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
	var fnActModify     = function(){
		doActLoadShow();
		$("#frmDefault").attr({"action":"${nextMode}Edit.do","method":"post","target":"_self"}).submit();
	};
	var fnActDelete     = function(){
		if(confirm("<%= alertMsg[4].replaceAll("[@]", "현재") %>")==0) return false;
		$("#frmDefault").attr({"action":"${nextMode}Delete.do","method":"post","target":"_self"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn("seq");}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<input type='hidden' id='reStdStat'     name='reStdStat'     value='${reStdStat}'/>
<input type='hidden' id='reStdDate'     name='reStdDate'     value='${reStdDate}'/>
<input type='hidden' id='reStdWeek'     name='reStdWeek'     value='${reStdWeek}'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="4"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>의정 활동</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Schd/${nextMode}List.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:38%;'/><col style='width:12%;'/><col style='width:38%;'/></colgroup>
					<tbody><tr>
						<th scope='col'>일정명칭</th>
						<td colspan='3'>${dataView.schdTitle}</td>
					</tr><tr>
						<th>일정명칭(약식)</th>
						<td colspan='3'>${dataView.schdSTitle}</td>
					</tr><tr>
						<th>기간</th>
						<td colspan='3'>${dataView.schdBDate}<c:if test="${!empty dataView.schdEDate}"> ~ ${dataView.schdEDate}</c:if></td>
					</tr><tr>
						<th>시간</th>
						<td colspan='3'>${dataView.schdBTime}<c:if test="${!empty dataView.schdETime}"> ~ ${dataView.schdETime}</c:if></td>
					</tr><tr>
						<th>구분</th>
						<td colspan='3'>${dataView.schdTypeName}</td><%--
						<td colspan='3'>${schdTyCLName}<c:if test="${!empty schdTyOL}">,${schdTyOLName}</c:if><c:if test="${!empty schdTyGL}">,${schdTyGLName}</c:if><c:if test="${!empty schdTyBL}">,${schdTyBLName}</c:if><c:if test="${!empty schdTyCS}">,${schdTyCSName}</c:if><c:if test="${!empty schdTyOS}">,${schdTyOSName}</c:if><c:if test="${!empty schdTyGS}">,${schdTyGSName}</c:if><c:if test="${!empty schdTyBS}">,${schdTyBSName}</c:if><c:if test="${!empty schdTyCW}">,${schdTyCWName}</c:if></td>--%>
					</tr><tr>
						<th>작성자</th>
						<td>${dataView.creName}</td>
						<th>작성일</th>
						<td>${dataView.creDate}</td>
					</tr><tr>
						<th>장소</th>
						<td colspan='3'>${dataView.schdPlace}</td>
					</tr><tr class='h200'>
						<th>내용</th>
						<td colspan='3'>${dataView.schdDescBR}</td></td>
					</tr></tbody>
				</table>
				<div class='btn_area right'><c:if test="${!empty dataView and ('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
					<input type='button' class='w70 btn_medium bt_blue'   value='수정'       onclick='fnActModify()'/>
					<input type='button' class='w70 btn_medium bt_red'    value='삭제'       onclick='fnActDelete()'/></c:if>
					<input type='button' class='w70 btn_medium bt_grey'   value='목록'       onclick='fnActReturn("seq")'/>
				</div><c:if test="${'T' eq moveItem}">
				<div class='board_list'><table class='table table_view'>
					<caption class='hide'>Previous/Next</caption>
					<colgroup><col style='width:12%;'/><col style='width:88%;'/></colgroup>
					<tbody><tr>
						<th scope='col'>이전글</th>
						<td><c:choose><c:when test="${ empty prevView}">이전 글이 존재 하지 않습니다.</c:when><c:otherwise>${prevView.gabSpace}<a href='javascript:void(0);' onclick='fnActDetail("${prevView.viewNo}")'><c:if test="${'Y' eq prevView.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${prevView.bbsTitle}</a></c:otherwise></c:choose></td>
					</tr><tr>
						<th scope='col'>다음글</th>
						<td><c:choose><c:when test="${ empty nextView}">다음 글이 존재 하지 않습니다.</c:when><c:otherwise>${nextView.gabSpace}<a href='javascript:void(0);' onclick='fnActDetail("${nextView.viewNo}")'><c:if test="${'Y' eq nextView.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${nextView.bbsTitle}</a></c:otherwise></c:choose></td>
					</tr></tbody>
				</table></div></c:if>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>