<%
////////////////////////////////////////////////////////////
// Program ID  : attendSeat_view
// Description : 의정활동 > 방청방문 관리 [상세조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "방청방문 관리";
	boolean useNow				= false;
	boolean viewReply			= false;
	String subFirst				= "";
	if(mngrFList!=null) for(int m=0;m<mngrFList.length;m++) if("T".equals(mngrFList[m][0])){subFirst=mngrFList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script>
	$(document).ready(function(){
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"Attend"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self"}).submit();
	};
	var fnActDetail     = function(view){
		doActLoadShow();
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"AttendView.do","method":"post","target":"_self"}).submit();
	};<c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}"><% if( useNow ) { %>
	var fnActModify     = function(){
		doActLoadShow();
		$("#frmDefault").attr({"action":"AttendEdit.do","method":"post","target":"_self"}).submit();
	};<% } %>
	var fnActUpdate     = function(){
		if($.trim($("#useAccept").val())==""){alert("신청상태를 선택하세요.");$("#useAccept").focus();return false;}
		$("#frmDefault").attr({"action":"AttendModify.do","method":"post","target":"_self"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn("seq");}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};<% if( useNow ) { %>
	var fnActDelete     = function(){
		if(confirm("<%= alertMsg[4].replaceAll("[@]", "현재") %>")==0) return false;
		$("#frmDefault").attr({"action":"AttendDelete.do","method":"post","target":"_self"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn("seq");}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};<% } %></c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<input type='hidden' id='reBgnDate'     name='reBgnDate'     value='${reBgnDate}'/>/>
<input type='hidden' id='reEndDate'     name='reEndDate'     value='${reEndDate}'/>/>
<input type='hidden' id='reUseStat'     name='reUseStat'     value='${reUseStat}'/>/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="4"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>의정활동</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Base/NoticeList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:38%;'/><col style='width:12%;'/><col style='width:38%;'/></colgroup>
					<tbody><tr>
						<th>신청구분</th>
						<td colspan='3'>${dataView.reqTypeName}</td>
					</tr><tr>
						<th scope='col'>신청자</th>
						<td>${dataView.creName}</td>
						<th scope='col'>신청일</th>
						<td>${dataView.reqDate}</td>
					</tr><tr>
						<th>방문일시</th>
						<td>${dataView.vstDate}</td>
						<th>방문인원</th>
						<td>${dataView.vstPerson}명</td>
					</tr><tr class='h200'>
						<th>방문목적</th>
						<td colspan='3'>${dataView.vstDescBR}</td>
					</tr><tr>
						<th>신청상태</th>
						<td colspan='3'><c:choose><c:when test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
							<select id='useAccept' name='useAccept' class='w20_p'><option value=''>신청상태</option>
								<option value='G'  <c:if test="${'G' eq dataView.useAccept}"> selected='selected'</c:if>>접수</option>
								<option value='Y'  <c:if test="${'Y' eq dataView.useAccept}"> selected='selected'</c:if>>접수대기</option>
								<option value='B'  <c:if test="${'B' eq dataView.useAccept}"> selected='selected'</c:if>>승인</option>
								<option value='R'  <c:if test="${'R' eq dataView.useAccept}"> selected='selected'</c:if>>승인불가</option><% if( useNow ) { %>
								<option value='C'  <c:if test="${'C' eq dataView.useAccept}"> selected='selected'</c:if>>취소</option><% } %>
							</select></c:when><c:otherwise>
							<select class='w20_p' readonly='readonly'><option value='${dataView.useAcceptName}'>${dataView.useAcceptName}</option></select></c:otherwise></c:choose>
							<span class='info01 light_grey'> <font color='red'>* 해당 신청내역의 승인여부를 결정합니다.</font></span>
						</td>
					</tr></tbody>
				</table>
				<div class='btn_area right'><c:if test="${!empty dataView and ('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
					<input type='button' class='w70 btn_medium bt_green'  value='적용'       onclick='fnActUpdate()'/><% if( useNow ) { %>
					<input type='button' class='w70 btn_medium bt_blue'   value='수정'       onclick='fnActModify()'/>
					<input type='button' class='w70 btn_medium bt_red'    value='삭제'       onclick='fnActDelete()'/><% } %></c:if>
					<input type='button' class='w70 btn_medium bt_grey'   value='목록'       onclick='fnActReturn("seq")'/>
				</div>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>