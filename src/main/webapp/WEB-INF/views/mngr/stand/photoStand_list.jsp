<%
////////////////////////////////////////////////////////////
// Program ID  : photoStand_list
// Description : 상임위원회 > 포토 의정활동 [목록조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "포토 의정활동";
	String subFirst				= "";
	if(mngrGList!=null) for(int m=0;m<mngrGList.length;m++) if("T".equals(mngrGList[m][0])){subFirst=mngrGList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script>
	$(document).ready(function(){
	});
	var fnActInitial    = function(){
		$("#condType").val("A");
		$("#condValue").val("");
		fnActRetrieve();
	};
	var fnActRetrieve   = function(page){
		doActLoadShow();
		$("#pageCurNo").val(isEmpty(page)?"1":page);
		$("#frmDefault").attr({"action":"PhotoList.do","method":"post","target":"_self"}).submit();
	};
	var fnActDetail     = function(view){
		doActLoadShow();
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"PhotoView.do","method":"post","target":"_self"}).submit();
	};<c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
	var fnActCreate     = function(){
		doActLoadShow();
		$("#frmDefault").attr({"action":"PhotoInst.do","method":"post","target":"_self"}).submit();
	};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="7"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>상임위원회</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Stand/PhotoList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='search_area'>
				<table class='table table_cols'>
					<colgroup><col style='width:10%;'/><col style='width:23%;'/><col style='width:10%;'/><col style='width:23%;'/><col style='width:10%;'/><col style='width:24%;'/></colgroup>
					<tbody><tr>
						<th>상임위원회</th>
						<td><select id='condCate' name='condCate' class='w120'>
								<option value=''      <c:if test="${''      eq condCate}"> selected='selected'</c:if>>전체</option>
								<option value='OPER'  <c:if test="${'OPER'  eq condCate}"> selected='selected'</c:if>>의회운영위원회</option>
								<option value='GVADM' <c:if test="${'GVADM' eq condCate}"> selected='selected'</c:if>>자치행정위원회</option>
								<option value='WELFARE' <c:if test="${'WELFARE' eq condCate}"> selected='selected'</c:if>>복지환경위원회</option>
								<option value='BUILD' <c:if test="${'BUILD' eq condCate}"> selected='selected'</c:if>>산업건설위원회</option>
						</select></td>
						<th>검색</th>
						<td colspan='3'>
							<select id='condType' name='condType' class='w120'>
								<option value='A'  <c:if test="${'A'  eq condType}"> selected='selected'</c:if>>전체</option>
								<option value='T'  <c:if test="${'T'  eq condType}"> selected='selected'</c:if>>제목</option>
							</select>&nbsp;&nbsp;
							<label for='condValue' class='hide'>검색어</label>
							<input type='text' id='condValue'     name='condValue'     class='w60_p' maxlength='30' value='${condValue}' placeholder='검색어를 입력하세요.'/>
						</td>
					</tr></tbody>
				</table>
				<div class='btn_area center'>
					<input type='button' class='w70 btn_large bt_grey' value='검색'       onclick='fnActRetrieve()'/>
				</div>
			</div>
			<div class='board_list'>
				<div class='board_total'>
					<span class='left'>전체 <strong class='red'>${listCnt}</strong> 건 &nbsp; ${pageCurNo} / ${pageInfo.totalPageCount}</span>
					<span class='right'><select id='pagePerNo'     name='pagePerNo' onchange='fnActRetrieve()'>
						<option value='8'  <c:if test="${  '8' eq pagePerNo}"> selected='selected'</c:if>>  8개씩 보기</option>
						<option value='16' <c:if test="${ '16' eq pagePerNo}"> selected='selected'</c:if>> 16개씩 보기</option>
						<option value='24' <c:if test="${ '24' eq pagePerNo}"> selected='selected'</c:if>> 24개씩 보기</option>
						<option value='40' <c:if test="${ '40' eq pagePerNo}"> selected='selected'</c:if>> 40개씩 보기</option>
						<option value='80' <c:if test="${ '80' eq pagePerNo}"> selected='selected'</c:if>> 80개씩 보기</option>
						<option value='160'<c:if test="${'160' eq pagePerNo}"> selected='selected'</c:if>>160개씩 보기</option>
						<option value='all'<c:if test="${  '8' ne pagePerNo and  '16' ne pagePerNo and  '24' ne pagePerNo and  '40' ne pagePerNo and  '80' ne pagePerNo and '160' ne pagePerNo}"> selected='selected'</c:if>>전체 보기</option>
					</select></span>
				</div>
				<c:choose><c:when test="${ empty dataList}">
				<div class='rows_table'><table class='table table_rows'>
					<caption class='hide'>Board List</caption>
					<colgroup><col style='width: 6%;'/><col style='width:64%;'/><col style='width:12%;'/><col style='width:10%;'/><col style='width: 8%;'/></colgroup>
					<tbody><tr class='off h200'><td colspan='5'><%= noneData %></td></tr></tbody>
				</table></div>
				</c:when><c:otherwise>
				<div class='gallery_list'>
					<ul><c:forEach var="rult" varStatus="status" items="${dataList}">
						<li class='on' onclick='fnActDetail("${rult.viewNo}")'>
							<div class='img'><img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/></div>
							<div class='tit'>${rult.gabSpace}<p>${rult.bbsTitle}</p></div>
							<div class='period'><ul><li>${rult.bbsCateName} | ${rult.creName}</li><li>${rult.creDate}&nbsp;/&nbsp;조회수 ${rult.hitCount}</li></ul></div>
						</li>
					</c:forEach></ul>
				</div>
				</c:otherwise></c:choose>
				<c:if test="${!empty pageInfo}"><div class='paging usr_pagination1'><ui:pagination paginationInfo="${pageInfo}" type="baseImage" jsFunction="fnActRetrieve" /></div></c:if>
			</div>
			<div class='btn_area right'><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
				<input type='button' class='w70 btn_medium bt_blue'   value='등록'       onclick='fnActCreate()'/></c:if>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>