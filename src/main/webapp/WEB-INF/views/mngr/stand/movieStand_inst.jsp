<%
////////////////////////////////////////////////////////////
// Program ID  : movieStand_inst
// Description : 상임위원회 > 영상 의정활동 [등록화면]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%
	String subTitle				= "인터넷방송";
	String subFirst				= "";
	if(mngrGList!=null) for(int m=0;m<mngrGList.length;m++) if("T".equals(mngrGList[m][0])){subFirst=mngrGList[m][4];break;}
	out.clearBuffer();

	Date nowTime = new Date();
	SimpleDateFormat sf = new SimpleDateFormat("yyyy.MM.dd");
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incCalendar.jspf"/>
	<script>
	$(document).ready(function(){
		$("#bbsCate").focus();
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"Movie"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self","enctype":""}).submit();
	};<c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
	var fnActUpdate     = function(){
		if($.trim($("#bbsCate").val())==""){alert("구분은 필수선택입니다.");$("#bbsCate").focus();return false;}
		if($.trim($("#bbsTitle").val())==""){alert("제목은 필수입력입니다.");$("#bbsTitle").focus();return false;}
		if($.trim($("#fileThmb").val())==""&&$("[id^='fileDele']").length==0){alert("영상썸네일은 필수입력입니다.");$("#fileThmb").focus();return false;}
		if(!fnValidImage("fileThmb")) return false;
		if($.trim($("#fileBase").val())==""&&$("[id^='fileDele']").length==0){alert("영상파일은 필수입력입니다.");$("#fileBase").focus();return false;}
		if(!fnValidVideo("fileBase")) return false;
		$("#frmDefault").attr({"action":"MovieCreate.do","method":"post","target":"_self","enctype":"multipart/form-data"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			contentType   : false , 
			processData   : false ,
			timeout		  : 1200000,
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			beforeSend    : function(){$(".progress").show();$(".progress-bar").width("0%");},
			uploadProgress: function(event,position,total,percentComplete){$(".progress-bar").width(percentComplete+"%");if(percentComplete==100){$(".progress").hide();};},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn("seq");}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log("code",xhr.status);console.log("message",xhr.responseText);console.log("error",xhr);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
		return false;
	};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="7"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>상임위원회</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Stand/MovieList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:88%;'/></colgroup>
					<tbody><tr>
						<th>구분 *</th>
						<td>
							<select id='bbsCate' name='bbsCate' class='w30_p'>
								<option value=''>해당없음</option>
								<option value='MAIN'>본회의</option>
								<option value='OPER'>의회운영위원회</option>
								<option value='GVADM'>자치행정위원회</option>
								<option value='WELFARE'>복지환경위원회</option>
								<option value='BUILD'>산업건설위원회</option>
								<option value='SPEC'>특별위원회</option>
								<option value='GOVER'>시정질문</option>
								<option value='SPCH'>7분자유발언</option>
							</select>
						</td>
					</tr>
					<tr>
						<th scope='col'>일자 *</th>
						<td><input type='text' id='scDate' name='scDate' class='w90 center' maxlength='8' value='<%=sf.format(nowTime)%>' placeholder='<%= dateHolder %>'/></td>
					</tr>
					<tr>
						<th scope='col'>제목 *</th>
						<td><input type='text' id='bbsTitle' name='bbsTitle' class='w90_p' maxlength='400'/></td>
					</tr><tr>
						<th>영상썸네일 *</th>
						<td id='file_thmb'><input type='file' id='fileThmb' name='fileThmb' class='w90_p' accept='image/*'/><%= allowFiles[2] %></td>
					</tr><tr>
						<th>영상파일 *</th>
						<td id='file_base'><input type='file' id='fileBase' name='fileBase' class='w90_p' accept='video/*'/><%= allowFiles[5] %></td>
					</tr><tr>
						<th>내용</th>
						<td><textarea id='contDesc' name='contDesc' rows='8' class='w90_p'></textarea></td>
					</tr></tbody>
				</table>
				<div class='btn_area right'><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
					<input type='button' class='w70 btn_medium bt_red'    value='저장'       onclick='fnActUpdate()'/></c:if>
					<input type='button' class='w70 btn_medium bt_grey'   value='목록'       onclick='fnActReturn("seq")'/>
				</div>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>