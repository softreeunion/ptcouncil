<%
////////////////////////////////////////////////////////////
// Program ID  : membStaff_edit
// Description : 계정관리 > 담당자 관리 [수정화면]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<%@ page import="egovframework.ptcc.cmmn.vo.*" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "생방송 관리";
	BaseCommonVO userInfo		= (BaseCommonVO)request.getSession().getAttribute("MemRInfo");		// 관리자 로그인 내역
	String subFirst				= "";
	if(mngrDList!=null) for(int m=0;m<mngrDList.length;m++) if("T".equals(mngrDList[m][0])){subFirst=mngrDList[m][4];break;}
	//if( !egovframework.ptcc.cmmn.util.BaseCategory.getCheckOfAdmin( userInfo.getAuthRole() ) ) {
	//	out.println("<script>alert(\"접속권한이 없습니다.\\n\\n시스템 관리자에게 문의바랍니다.\");history.back();</script>");
	//} else {
	//	out.clearBuffer();
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script>
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"LiveEdit.do","method":"post","target":"_self"}).submit();
	};
	var fnActUpdate     = function(){
		if($.trim($("#urlMain").val())==""){alert("본회의 URL을 입력해주세요.");$("#urlMain").focus();return false;}
		if($.trim($("#urlOper").val())==""){alert("의회운영위원회 URL을 입력해주세요.");$("#urlOper").focus();return false;}
		if($.trim($("#urlGvadm").val())==""){alert("기획행정위원회 URL을 입력해주세요.");$("#urlGvadm").focus();return false;}
		if($.trim($("#urlWelfare").val())==""){alert("복지환경위원회 URL을 입력해주세요.");$("#urlWelfare").focus();return false;}
		if($.trim($("#urlBuild").val())==""){alert("산업건설위원회 URL을 입력해주세요.");$("#urlBuild").focus();return false;}

		if(confirm("<%= alertMsg[3] %>")==0) return false;
		$("#frmDefault").attr({"action":"/ptnhexa/Stand/LiveUpdate.do","method":"post","target":"_self"}).ajaxSubmit({
			dataType      : "json",
			cache         : false ,
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn();}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<%--<input type='hidden' id='userComm'      name='userComm'/>--%>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="4"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>의정 활동</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Stand/LiveEdit.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='board_list'>
				<br/>
				<p style="color:#ef3a18">* 생방송 URL을 입력 후 저장하시면 실시간 반영됩니다.</p>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:38%;'/><col style='width:12%;'/><col style='width:38%;'/></colgroup>
					<tbody><tr>
						<th scope='col'>본회의</th>
						<td colspan='3'><input type='text' id='urlMain' name='urlMain' class='w40_p' maxlength='500' value='${dataView.urlMain}' /></td>
					</tr><tr>
						<th scope='col'>의회운영위원회</th>
						<td colspan='3'><input type='text' id='urlOper' name='urlOper' class='w40_p' maxlength='500' value='${dataView.urlOper}' /></td>
					</tr><tr>
						<th scope='col'>기획행정위원회</th>
						<td colspan='3'><input type='text' id='urlGvadm' name='urlGvadm' class='w40_p' maxlength='500' value='${dataView.urlGvadm}' /></td>
					</tr><tr>
						<th scope='col'>복지환경위원회</th>
						<td colspan='3'><input type='text' id='urlWelfare' name='urlWelfare' class='w40_p' maxlength='500' value='${dataView.urlWelfare}' /></td>
					</tr><tr>
						<th scope='col'>산업건설위원회</th>
						<td colspan='3'><input type='text' id='urlBuild' name='urlBuild' class='w40_p' maxlength='500' value='${dataView.urlBuild}' /></td>
					</tr></tbody>
				</table>
				<div style="height:500px !important;"></div>
				<div class='btn_area right'>
					<%--<c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">--%>
					<input type='button' class='w70 btn_medium bt_red'    value='저장'       onclick='fnActUpdate()'/>
					<%--</c:if>--%>
				</div>
			</div>
		</div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html><%	//} %>