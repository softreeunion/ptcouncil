<%
////////////////////////////////////////////////////////////
// Program ID  : minutesTail_list
// Description : 회의록 관리 > 회의록 관리 [목록조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
//  v1.1    HexaMedia new0man      2020.05.08  대수별 기간 최소최대 선택일지정
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "회의록 관리";
	String subFirst				= "";
	if(mngrEList!=null) for(int m=0;m<mngrEList.length;m++) if("T".equals(mngrEList[m][0])){subFirst=mngrEList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incCalendar.jspf"/>
	<script>
	$(document).ready(function(){
		$("#rowAll").click(function(){$("[id^='rows']").prop("checked",$(this).is(":checked"));});
		fnActConfDate("${reEraCode}");
	});
	var fnActConfDate   = function(era){
		$.ajax({
			type          : "post", 
			url           : "<%= webBase %>/cmmn/seleDate.do", 
			dataType      : "json", 
			data          : {era:era}, 
			success       : function(data,status,xhr  ){$("#reBgnDate,#reEndDate").datepicker("option","beforeShow",null).datepicker("option","minDate",data.min).datepicker("option","maxDate",data.max);$("#reBgnDate").val(data.min);$("#reEndDate").val(data.max);}, 
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};
	var fnActChanger    = function(){
		$.ajax({
			type          : "post", 
			url           : "<%= webBase %>/cmmn/seleRound.do", 
			dataType      : "html", 
			data          : {era:$("#reEraCode").val(),rnd:"${reRndCode}"}, 
			success       : function(data,status,xhr  ){$("#reRndCode").empty();$("#reRndCode").html(data);}, 
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
		$.ajax({
			type          : "post", 
			url           : "<%= webBase %>/cmmn/seleMemb.do", 
			dataType      : "html", 
			data          : {era:$("#reEraCode").val(),mid:"${reMemID}"}, 
			success       : function(data,status,xhr  ){$("#reMemID").empty();$("#reMemID").html(data);}, 
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
		$("#reKndCode").val("");
		$("#reBgnDate").val("");
		$("#reEndDate").val("");
		$("#searchText").val("");
		fnActConfDate($("#reEraCode").val());
	};
	var fnActRetrieve   = function(page){
		doActLoadShow();
		$("#pageCurNo").val(isEmpty(page)?"1":page);
		$("#frmDefault").attr({"action":"MinutesList.do","method":"post","target":"_self"}).submit();
	};
	var fnActDetail     = function(view){
		doActLoadShow();
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"MinutesView.do","method":"post","target":"_self"}).submit();
	};<c:if test="${!empty dataList and ('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
	var fnActUpdate     = function(rult){
		if(confirm("임시회의록 "+ (rult=="T"?"지정을":"해제를") +" 처리하시겠습니까?")==0) return false;
		$("#rultCode").val(rult);
		$("#frmDefault").attr({"action":"MinutesModify.do","method":"post","target":"_self"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActRetrieve(${pageCurNo});}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};
	var fnActModify     = function(){
		doActLoadShow();
		$("#frmDefault").attr({"action":"MinutesEdit.do","method":"post","target":"_self"}).submit();
	};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='rultCode'      name='rultCode'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="5"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>회의록 관리</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Tail/MinutesList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='search_area'>
				<table class='table table_cols'>
					<colgroup><col style='width:10%;'/><col style='width:23%;'/><col style='width:10%;'/><col style='width:23%;'/><col style='width:10%;'/><col style='width:24%;'/></colgroup>
					<tbody><tr>
						<th scope='col'>대수</th>
						<td><select id='reEraCode' name='reEraCode' class='w100' onchange='fnActChanger()'><option value=''>전체</option>
						<c:if test="${!empty eraList}"><c:forEach var="cate" varStatus="status" items="${eraList}">
							<option value='${cate.eraCode}'<c:if test="${cate.eraCode eq reEraCode}"> selected='selected'</c:if>>${cate.eraName}</option>
						</c:forEach></c:if>
						</select></td>
						<th scope='col'>회기</th>
						<td><select id='reRndCode' name='reRndCode' class='w100'><option value=''>전체</option>
						<c:if test="${!empty rndList}"><c:forEach var="cate" varStatus="status" items="${rndList}">
							<option value='${cate.roundCode}'<c:if test="${cate.roundCode eq reRndCode}"> selected='selected'</c:if>>${cate.roundName}</option>
						</c:forEach></c:if>
						</select></td>
						<th scope='col'>회의구분</th>
						<td><select id='reKndCode' name='reKndCode' class='w100'><option value=''>전체</option>
						<c:if test="${!empty kndList}"><c:forEach var="cate" varStatus="status" items="${kndList}">
							<option value='${cate.kindCode}'<c:if test="${cate.kindCode eq reKndCode}"> selected='selected'</c:if>>${cate.kindName}</option>
						</c:forEach></c:if>
						</select></td>
					</tr><tr>
						<th>기간</th>
						<td colspan='3'><input type='text' id='reBgnDate' name='reBgnDate' class='w90 center' maxlength='8' value='${reBgnDate}' placeholder='<%= dateHolder %>'/> ~ <input type='text' id='reEndDate' name='reEndDate' class='w90 center' maxlength='8' value='${reEndDate}' placeholder='<%= dateHolder %>'/></td>
						<th class='center'>위원명</th>
						<td><select id='reMemID' name='reMemID' class='w100'><option value=''>전체</option>
						<c:if test="${!empty memList}"><c:forEach var="cate" varStatus="status" items="${memList}">
							<option value='${cate.memName}'<c:if test="${cate.memName eq reMemID}"> selected='selected'</c:if>>${cate.memName}</option>
						</c:forEach></c:if>
						</select></td>
					</tr><tr>
						<th>검색어</th>
						<td colspan='5'><input type='text' id='searchText' name='searchText' class='w60_p' maxlength='30' value='${searchText}' placeholder='검색어를 입력하세요.'/></td>
					</tr></tbody>
				</table>
				<div class='btn_area center'>
					<input type='button' class='w70 btn_large bt_grey' value='검색'       onclick='fnActRetrieve()'/>
				</div>
			</div>
			<div class='board_list'>
				<div class='board_total'>
					<span class='left'>전체 <strong class='red'>${listCnt}</strong> 건 &nbsp; ${pageCurNo} / ${pageInfo.totalPageCount}</span>
					<span class='right'><select id='pagePerNo'     name='pagePerNo' onchange='fnActRetrieve()'>
						<option value='10' <c:if test="${ '10' eq pagePerNo}"> selected='selected'</c:if>> 10개씩 보기</option>
						<option value='20' <c:if test="${ '20' eq pagePerNo}"> selected='selected'</c:if>> 20개씩 보기</option>
						<option value='30' <c:if test="${ '30' eq pagePerNo}"> selected='selected'</c:if>> 30개씩 보기</option>
						<option value='50' <c:if test="${ '50' eq pagePerNo}"> selected='selected'</c:if>> 50개씩 보기</option>
						<option value='100'<c:if test="${'100' eq pagePerNo}"> selected='selected'</c:if>>100개씩 보기</option>
						<option value='200'<c:if test="${'200' eq pagePerNo}"> selected='selected'</c:if>>200개씩 보기</option>
						<option value='all'<c:if test="${ '10' ne pagePerNo and  '20' ne pagePerNo and  '30' ne pagePerNo and  '50' ne pagePerNo and '100' ne pagePerNo and '200' ne pagePerNo}"> selected='selected'</c:if>>전체 보기</option>
					</select></span>
				</div>
				<div class='rows_table'><table class='table table_rows'>
					<caption class='hide'>Board List</caption>
					<colgroup><col style='width:12%;'/><col style='width:10%;'/><col style='width:10%;'/><col style='width:14%;'/><col style='width:40%;'/><col style='width:14%;'/></colgroup>
					<thead><tr>
						<th scope='col'><label for='rowAll'>번호/선택 </label><input type='checkbox' id='rowAll' name='rowAll'/></th>
						<th scope='col'>대수</th>
						<th scope='col'>회수</th>
						<th scope='col'>차수</th>
						<th scope='col'>회의명</th>
						<th scope='col'>일자</th>
					</tr></thead>
					<tbody>
					<c:choose><c:when test="${ empty dataList}">
						<tr class='off h200'><td colspan='6'><%= noneData %></td></tr>
					</c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${dataList}">
						<tr class='on'>
							<td><label for='rows${status.index}'>${rult.rowIdx} &nbsp; </label><input type='checkbox' id='rows${status.index}' name='rows' value='${rult.confUUID}'/></td>
							<td onclick='fnActDetail("${rult.confUUID}")'>${rult.eraName}</td>
							<td onclick='fnActDetail("${rult.confUUID}")'>${rult.roundName}</td>
							<td onclick='fnActDetail("${rult.confUUID}")'>${rult.pointName}</td>
							<td onclick='fnActDetail("${rult.confUUID}")' class='left'><c:if test="${'T' eq rult.tempFlag}"><span class='td_blu'>[임시회의록]</span> </c:if><c:if test = "${fn:contains(rult.kindCode,'4')}">행정사무감사 - </c:if>${rult.kindName}</td>
							<td onclick='fnActDetail("${rult.confUUID}")'>${rult.confDate}(${rult.confWeek})</td>
						</tr>
					</c:forEach></c:otherwise></c:choose>
					</tbody>
				</table></div>
				<c:if test="${!empty pageInfo}"><div class='paging usr_pagination1'><ui:pagination paginationInfo="${pageInfo}" type="baseImage" jsFunction="fnActRetrieve" /></div></c:if>
			</div>
			<div class='btn_area right'><c:if test="${!empty dataList and ('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
				<input type='button' class='w120 btn_medium bt_green'  value='임시회의록 지정' onclick='fnActUpdate("T")'/>
				<input type='button' class='w120 btn_medium bt_green'  value='임시회의록 해제' onclick='fnActUpdate("F")'/></c:if>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>