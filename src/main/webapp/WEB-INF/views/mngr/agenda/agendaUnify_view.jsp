<%
////////////////////////////////////////////////////////////
// Program ID  : agendaUnify_view
// Description : 회의록 관리 > 통합의안 관리 [상세조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "통합의안 관리";
	boolean viewReply			= false;
	String subFirst				= "";
	if(mngrEList!=null) for(int m=0;m<mngrEList.length;m++) if("T".equals(mngrEList[m][0])){subFirst=mngrEList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script>
	$(document).ready(function(){
		$("[id^='sTimeStamp']").attr("autocomplete","off");
		$("[id^='nTimeStamp']").attr("autocomplete","off");
	});
	$(document).on("blur","[id^='sTimeStamp']",function(e){e.preventDefault();fnTimeColon($(this));});
	$(document).on("blur","[id^='nTimeStamp']",function(e){e.preventDefault();fnTimeColon($(this));});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"Agenda"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self","enctype":""}).submit();
	};
	var fnActDetail     = function(view){
		doActLoadShow();
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"AgendaView.do","method":"post","target":"_self","enctype":""}).submit();
	};<c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
	var fnActUpdate     = function(){
		$("#frmDefault").attr({"action":"AgendaModify.do","method":"post","target":"_self","enctype":""}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn();}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};
	var fnActSender     = function(obj){
		obj.parent().parent().children("td").removeClass("ln_btm");
	};
	var fnActInitial    = function(){
		if(confirm("<%= alertMsg[4].replaceAll("[@]", "현재").replaceAll("삭제", "초기화 진행") %>")==0) return false;
		$("#frmDefault").attr({"action":"AgendaInitial.do","method":"post","target":"_self","enctype":""}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn();}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};
	var fnRowInit       = "<tr><td>@1</td><td class='left'><input type='text' id='nSugTitle@1' name='nSugTitle' class='w100_p' maxlength='100'/></td><td><input type='text' id='nTimeStamp@1' name='nTimeStamp' class='w90_p center' maxlength='8'/></td><td><select id='nTimeMemb@1' name='nTimeMemb' class='w90_p center'><option value=''>선택하세요</option><c:if test="${!empty memList}"><c:forEach var="cate" varStatus="status" items="${memList}"><option value='${cate.memID}'>${cate.memName}</option></c:forEach></c:if></select></td><td><input type='text' id='nSugPosi@1' name='nSugPosi' class='w90_p center' maxlength='10'/></td><td><input type='hidden' name='nConfUUID'/><input type='button' class='w70 btn_small bt_green' value='행삭제' onclick='fnRowDelete($(this))'/></td></tr>";
	var fnRowAppend     = function(obj){
		if($("#lstView>tbody>tr").length<=1&&$("#lstView>tbody>tr:last>td").length<=1)$("#lstView>tbody>tr:last").remove();
		$("#lstView>tbody:last").append(fnRowInit.replace(/@1/gi,$("#lstView>tbody>tr").length+1));
		$("#lstView>tbody:last").find("input[type=text]").attr("autocomplete","off");
		$(document).scrollTop($(document).height());
	};
	var fnRowDelete     = function(obj){
		var tro = obj.parent().parent();
		var tdo = tr.children();
		if($.trim(tdo.eq(5).find("input").val())!="")$("#condDele").val($("#condDele").val()+tdo.eq(5).find("input").val()+"`");
		tro.remove();
		$("#lstView>tbody>tr").each(function(i){$("td:eq(0)",this).text(i+1);});
	};
	var fnFileUpdate    = function(){
		if($.trim($("#fileBase1").val())==""&&$("[id^='fileDele']").length==0){alert("첨부파일은 필수입력입니다.");$("#fileBase").focus();return false;}
		if(!fnValidBases("fileBase")) return false;
		$("#frmDefault").attr({"action":"AgendaInFile.do","method":"post","target":"_self","enctype":"multipart/form-data"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			contentType   : false , 
			processData   : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn();}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<input type='hidden' id='reEraCode'     name='reEraCode'     value='${reEraCode}'/>
<input type='hidden' id='reRndCode'     name='reRndCode'     value='${reRndCode}'/>
<input type='hidden' id='reKndCode'     name='reKndCode'     value='${reKndCode}'/>
<input type='hidden' id='reBgnDate'     name='reBgnDate'     value='${reBgnDate}'/>
<input type='hidden' id='reEndDate'     name='reEndDate'     value='${reEndDate}'/>
<input type='hidden' id='reMemID'       name='reMemID'       value='${reMemID}'/>
<input type='hidden' id='searchText'    name='searchText'    value='${searchText}'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="5"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>회의록 관리</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Unify/AgendaList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:38%;'/><col style='width:12%;'/><col style='width:38%;'/></colgroup>
					<tbody><tr>
						<th colspan='4'>회의명 : <c:if test="${'T' eq dataView.tempFlag}"><span class='td_blu'>[임시회의록]</span> </c:if>${dataView.confName}</th>
					</tr></tbody>
				</table>
				<h3>의안내역</h3>
				<table class='table table_rows' id='lstView'>
					<caption class='hide'>Board List</caption>
					<colgroup><col style='width:8%;'/><col style='width:44%;'/><col style='width:12%;'/><col style='width:12%;'/><col style='width:12%;'/><col style='width:12%;'/></colgroup>
					<thead><tr>
						<th>번호</th>
						<th>의안명칭</th>
						<th>영상시작위치</th>
						<th>발의의원</th>
						<th>탐색위치</th>
						<th>비고</th>
					</tr></thead>
					<tbody>
					<c:choose><c:when test="${ empty dataView.childTime}">
						<tr class='off h200'><td colspan='5'><%= noneData %></td></tr>
					</c:when><c:otherwise><c:forEach var="chld" varStatus="status" items="${dataView.childTime}"><c:if test="${'S' eq chld.sugType}">
						<tr class='off'>
							<td>${status.count}</td>
							<td class='left'>${chld.sugTitle}</td>
							<td><input type='text' id='sTimeStamp${status.count}' name='sTimeStamp' class='w90_p center' maxlength='8' value='${chld.timeStamp}'/></td>
							<td><select id='sTimeMemb${status.count}' name='sTimeMemb' class='w90_p center'><option value=''>선택하세요</option><c:if test="${!empty memList}"><c:forEach var="cate" varStatus="status" items="${memList}"><option value='${cate.memID}'<c:if test="${chld.timeMemb eq cate.memID}"> selected='selected'</c:if>>${cate.memName}</option></c:forEach></c:if></select></td>
							<td></td>
							<td><input type='hidden' name='sConfUUID' value='${chld.confUUID}'/></td>
						</tr>
					</c:if><c:if test="${'N' eq chld.sugType}">
						<tr class='off'>
							<td>${status.count}</td>
							<td class='left'><input type='text' id='nSugTitle${status.count}' name='nSugTitle' class='w100_p' maxlength='100' value='${chld.sugTitle}'/></td>
							<td><input type='text' id='nTimeStamp${status.count}' name='nTimeStamp' class='w90_p center' maxlength='8' value='${chld.timeStamp}'/></td>
							<td><select id='nTimeMemb${status.count}' name='nTimeMemb' class='w90_p center'><option value=''>선택하세요</option><c:if test="${!empty memList}"><c:forEach var="cate" varStatus="status" items="${memList}"><option value='${cate.memID}'<c:if test="${chld.timeMemb eq cate.memID}"> selected='selected'</c:if>>${cate.memName}</option></c:forEach></c:if></select></td>
							<td><input type='text' id='nSugPosi${status.count}' name='nSugPosi' class='w90_p center' maxlength='10' value='${chld.sugPosi}'/></td>
							<td><input type='hidden' name='nConfUUID' value='${chld.confUUID}'/><input type='button' class='w70 btn_small bt_green' value='행삭제' onclick='fnRowDelete($(this))'/></td>
						</tr>
					</c:if></c:forEach></c:otherwise></c:choose>
					</tbody>
				</table>
				<div class='btn_area right'><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
					<input type='button' class='w70 btn_medium bt_blue'   value='행추가'     onclick='fnRowAppend()'/>
					<input type='button' class='w70 btn_medium bt_red'    value='저장'       onclick='fnActUpdate()'/></c:if>
					<input type='button' class='w70 btn_medium bt_grey'   value='취소'       onclick='fnActReturn()'/>
					<input type='button' class='w70 btn_medium bt_grey'   value='목록'       onclick='fnActReturn("seq")'/><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
					<input type='button' class='w70 btn_medium bt_red'    value='초기화'     onclick='fnActInitial()'/></c:if>
				</div>
			</div>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Files View</caption>
					<colgroup><col style='width:12%;'/><col style='width:38%;'/><col style='width:12%;'/><col style='width:38%;'/></colgroup>
					<tbody><tr><c:choose><c:when test="${ empty dataFile}"><tr>
						<th>첨부파일</th>
						<td colspan='3' id='file_base'><input type='file' id='fileBase1' name='fileBase1' class='w90_p'/></td>
					</tr></c:when><c:otherwise><c:forEach var="file" varStatus="status" items="${dataFile}"><tr><th>첨부파일</th>
						<td colspan='3' id='file_base${status.count}'>
							<input type='button' id='fileDele${status.count}' class='w70 btn_small bt_grey' value='파일삭제' onclick='fcBaseRemove($(this),"${file.fileUUID}`")'/>&nbsp;&nbsp;
							<a href='javascript:void(0);' onclick='fnActDownload("${file.fileUUID}")'>${file.fileRel}</a>
						</td>
					</tr></c:forEach></c:otherwise></c:choose></tbody>
				</table>
				<%= allowFiles[5] %>
				<div class='btn_area right'><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
					<input type='button' class='w70 btn_medium bt_red'    value='파일저장'   onclick='fnFileUpdate()'/></c:if>
				</div>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>