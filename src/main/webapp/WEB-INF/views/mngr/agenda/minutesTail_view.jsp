<%
////////////////////////////////////////////////////////////
// Program ID  : minutesTail_view
// Description : 회의록 관리 > 회의록 관리 [상세조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "회의록 관리";
	String subFirst				= "";
	if(mngrEList!=null) for(int m=0;m<mngrEList.length;m++) if("T".equals(mngrEList[m][0])){subFirst=mngrEList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script src='<%= request.getContextPath() %>/plugins/smarteditor/js/service/HuskyEZCreator.js' charset='utf-8'></script>
	<script>
	var oEditors = [];
	$(document).ready(function(){
		$("#contDesc").each(function(index){
			var cont=$(this).attr("id");
			if(!cont||$(this).prop("nodeName")!="TEXTAREA") return true;
			nhn.husky.EZCreator.createInIFrame({oAppRef:oEditors,elPlaceHolder:cont,sSkinURI:"<%= request.getContextPath() %>/plugins/smarteditor/SmartEditor2Skin.html",fCreator:"createSEditor2"});<%--
			oEditors.getById[cont].exec("PASTE_HTML",["${confView}"]);--%>
		});
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"Minutes"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self","enctype":""}).submit();
	};
	var fnActDetail     = function(view){
		doActLoadShow();
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"MinutesView.do","method":"post","target":"_self","enctype":""}).submit();
	};<c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
	var fnActUpdate     = function(){
		oEditors.getById["contDesc"].exec("UPDATE_CONTENTS_FIELD",[]);
		if($.trim($("#contDesc").val())==""){alert("작성된 내용이 없습니다. 내용을 입력하세요.");oEditors.getById["contDesc"].exec("FOCUS");return false;}
		$("#frmDefault").attr({"action":"MinutesUpdate.do","method":"post","target":"_self","enctype":""}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn("seq");}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};
	var fUsed = false;
	var fnFileUpdate    = function(){
		$("[id^='fileBase']").each(function(indx,item){fUsed=$.trim($(item).val())=="";if(!fUsed){return fUsed;};});
		if(fUsed&&$("[id^='fileDele']").length==0){alert("첨부파일은 필수입력입니다.");$("#fileBase").focus();return false;}
		if(!fnValidBases("fileBase")) return false;
		$("#frmDefault").attr({"action":"MinutesInFile.do","method":"post","target":"_self","enctype":"multipart/form-data"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			contentType   : false , 
			processData   : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn();}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});
	};</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<input type='hidden' id='reEraCode'     name='reEraCode'     value='${reEraCode}'/>
<input type='hidden' id='reRndCode'     name='reRndCode'     value='${reRndCode}'/>
<input type='hidden' id='reKndCode'     name='reKndCode'     value='${reKndCode}'/>
<input type='hidden' id='reBgnDate'     name='reBgnDate'     value='${reBgnDate}'/>
<input type='hidden' id='reEndDate'     name='reEndDate'     value='${reEndDate}'/>
<input type='hidden' id='reMemID'       name='reMemID'       value='${reMemID}'/>
<input type='hidden' id='searchText'    name='searchText'    value='${searchText}'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="5"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>회의록 관리</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Tail/MinutesList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:38%;'/><col style='width:12%;'/><col style='width:38%;'/></colgroup>
					<tbody><tr>
						<th colspan='4'>회의명 : <c:if test="${'T' eq dataView.tempFlag}"><span class='td_blu'>[임시회의록]</span> </c:if>${dataView.confName}</th>
					</tr><tr>
						<td colspan='4' class='nonebd_l'><textarea id='contDesc' name='contDesc' rows='16' class='w99_p'>${confView}</textarea></td>
					</tr></tbody>
				</table>
				<div class='btn_area right'><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
					<input type='button' class='w70 btn_medium bt_red'    value='저장'       onclick='fnActUpdate()'/></c:if>
					<input type='button' class='w70 btn_medium bt_grey'   value='목록'       onclick='fnActReturn("seq")'/>
				</div>
			</div>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Files View</caption>
					<colgroup><col style='width:12%;'/><col style='width:38%;'/><col style='width:12%;'/><col style='width:38%;'/></colgroup>
					<tbody><tr><c:choose><c:when test="${ empty dataFile}"><tr>
						<th>첨부파일</th>
						<td colspan='3' id='file_base1'><input type='file' id='fileBase1' name='fileBase1' class='w90_p'/></td>
					</tr></c:when><c:otherwise><c:forEach var="file" varStatus="status" items="${dataFile}"><tr><th>첨부파일</th>
						<td colspan='3' id='file_base${status.count}'>
							<input type='button' id='fileDele${status.count}' class='w70 btn_small bt_grey' value='파일삭제' onclick='fcBaseRemove($(this),"${file.fileUUID}`")'/>&nbsp;&nbsp;
							<a href='javascript:void(0);' onclick='fnActDownload("${file.fileUUID}")'>${file.fileRel}</a>
							<p class='td_blu'>다운주소 : <c:if test="${!empty file.fileUri}"><%= webPath + webBase %>${file.fileUri}.do</c:if></p>
						</td>
					</tr></c:forEach></c:otherwise></c:choose></tbody>
				</table>
				<div class='search_area'><input type='button' id='fileAdd03' class='w70 btn_small bt_blue' value='파일추가'/> &nbsp; <%= allowFiles[3] %></div>
				<div class='btn_area right'><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">
					<input type='button' class='w70 btn_medium bt_red'    value='파일저장'   onclick='fnFileUpdate()'/></c:if>
				</div>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>