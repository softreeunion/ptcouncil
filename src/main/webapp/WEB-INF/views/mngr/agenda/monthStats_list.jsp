<%
////////////////////////////////////////////////////////////
// Program ID  : agendaStats_list
// Description : 접속 통계 > 접속통계 (월별) [목록조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "접속통계 (월별)";
	String subFirst				= "";
	if(mngrIList!=null) for(int m=0;m<mngrIList.length;m++) if("T".equals(mngrIList[m][0])){subFirst=mngrIList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<link rel='stylesheet' media='all' href='<%= webBase %>/jquerys/jqplot/jquery.jqplot.css'/>
	<script src='<%= webBase %>/jquerys/jqplot/jquery.jqplot.js'></script>
	<script src='<%= webBase %>/jquerys/jqplot/plugins/jqplot.canvasTextRenderer.js'></script>
	<script src='<%= webBase %>/jquerys/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js'></script>
	<script src='<%= webBase %>/jquerys/jqplot/plugins/jqplot.canvasAxisTickRenderer.js'></script>
	<script src='<%= webBase %>/jquerys/jqplot/plugins/jqplot.dateAxisRenderer.js'></script>
	<script src='<%= webBase %>/jquerys/jqplot/plugins/jqplot.categoryAxisRenderer.js'></script>
	<script src='<%= webBase %>/jquerys/jqplot/plugins/jqplot.pointLabels.js'></script>
	<script src='<%= webBase %>/jquerys/jqplot/plugins/jqplot.dateAxisRenderer.js'></script>
	<script src='<%= webBase %>/jquerys/jqplot/plugins/jqplot.highlighter.js'></script>
	<script>
	$(document).ready(function(){
		var jbWidth = $("#chart1").width();
		$.jqplot.config.enablePlugins = true;	
		$.jqplot.config.defaultHeight = 300;
		$.jqplot.config.defaultWidth = jbWidth-47;
		var line2 = [${dataRult}];
		month = {
			series:[{neighborThreshold:0}],
			axes: {
				xaxis:{
					label:"조회기간(월별)",
					renderer:$.jqplot.DateAxisRenderer,
					min:"${dpBgnDate}",
					max:"${dpEndDate}",
					tickInterval:"1 month",
					tickRenderer:$.jqplot.CanvasAxisTickRenderer,
					tickOptions:{formatString:"%Y-%#m",angle:-15,fontSize:"7pt"}
				},
				yaxis:{
					label:"접속자수",
					labelRenderer:$.jqplot.CanvasAxisLabelRenderer,
					tickOptions:{formatString:"%'d"}
				}
			}
		};
		var plot2 = $.jqplot("chart2",[line2],month);
		plot2.replot();
	});
	var fnActRetrieve   = function(page){
		doActLoadShow();
		$("#pageCurNo").val(isEmpty(page)?"1":page);
		$("#frmDefault").attr({"action":"MonthList.do","method":"post","target":"_self"}).submit();
	};
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="9"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>접속 통계</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Stats/YearsList.do'><%= subTitle %></a></li>
			</ul></div>
		</div>
		<h3><%= subTitle %></h3>
		<div class='search_area'>
			<table class='table table_cols'>
				<colgroup><col style='width:10%;'/><col style='width:23%;'/><col style='width:10%;'/><col style='width:23%;'/><col style='width:10%;'/><col style='width:24%;'/></colgroup>
				<tbody><tr>
					<th scope='col'>조회년도</th>
					<td colspan='5'>
						<select id='reStdYear' name='reStdYear' class='w80 center'>
						<c:forEach var="year" varStatus="status" begin="${reStdYear-6}" end="${reStdYear-1}">
							<option value='${year}'<c:if test="${year eq reStdYear}"> selected='selected'</c:if>>${year}</option>
						</c:forEach><c:forEach var="year" varStatus="status" begin="${reStdYear}" end="${reStdYear+2}">
							<option value='${year}'<c:if test="${year eq reStdYear}"> selected='selected'</c:if>>${year}</option>
						</c:forEach>
						</select>년 &nbsp; <span class='info01 light_grey'>결과는 조회년도의 월별로 조회됩니다.</span>
					</td>
				</tr></tbody>
			</table>
			<div class='btn_area center'>
				<input type='button' class='w70 btn_large bt_grey' value='검색'       onclick='fnActRetrieve()'/>
			</div>
		</div>
		<div class='btn_area'><div id='chart2'></div></div>
		<div class='board_list'>
			<div class='rows_table'><table class='table table_rows'>
				<caption class='hide'>월별접속자</caption>
				<colgroup><col style='width:50%;'/><col style='width:50%;'/></colgroup>
				<thead><tr>
					<th scope='col'>접속월</th>
					<th scope='col'>접속자수</th>
				</tr></thead>
				<tbody>
				<c:choose><c:when test="${ empty dataList}">
					<tr class='off h200'><td colspan='2'><%= noneData %></td></tr>
				</c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${dataList}">
					<tr>
						<td>${rult.vsnDate}</td>
						<td><fmt:formatNumber value="${rult.hitCount}" type="number"/></td>
					</tr>
				</c:forEach></c:otherwise></c:choose>
				</tbody>
			</table>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>