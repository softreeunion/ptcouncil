<%
////////////////////////////////////////////////////////////
// Program ID  : membStaff_edit
// Description : 계정관리 > 담당자 관리 [수정화면]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<%@ page import="egovframework.ptcc.cmmn.vo.*" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "전화번호 관리";
	BaseCommonVO userInfo		= (BaseCommonVO)request.getSession().getAttribute("MemRInfo");		// 관리자 로그인 내역
	String subFirst				= "";
	if(mngrDList!=null) for(int m=0;m<mngrDList.length;m++) if("T".equals(mngrDList[m][0])){subFirst=mngrDList[m][4];break;}
	//if( !egovframework.ptcc.cmmn.util.BaseCategory.getCheckOfAdmin( userInfo.getAuthRole() ) ) {
	//	out.println("<script>alert(\"접속권한이 없습니다.\\n\\n시스템 관리자에게 문의바랍니다.\");history.back();</script>");
	//} else {
	//	out.clearBuffer();
	out.clearBuffer();
%>
<!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script src='<%= request.getContextPath() %>/plugins/smarteditor/js/service/HuskyEZCreator.js' charset='utf-8'></script>
	<script>
		var oEditors = [];
		$(document).ready(function(){
			$("#contDesc").each(function(index){
				var cont=$(this).attr("id");
				if(!cont||$(this).prop("nodeName")!="TEXTAREA") return true;
				nhn.husky.EZCreator.createInIFrame({oAppRef:oEditors,elPlaceHolder:cont,sSkinURI:"<%= request.getContextPath() %>/plugins/smarteditor/SmartEditor2Skin.html",fCreator:"createSEditor2"});<%--
			oEditors.getById[cont].exec("PASTE_HTML",["${confView}"]);--%>
			});
		});
		var fnActReturn     = function(seq){
			doActLoadShow();
			$("#frmDefault").attr({"action":"PageEdit.do","method":"post","target":"_self"}).submit();
		};
		var fnActUpdate     = function(){
			oEditors.getById["contDesc"].exec("UPDATE_CONTENTS_FIELD",[]);
			var cont = $.trim(oEditors.getById["contDesc"].getv)=="";
			if($.trim($("#contDesc").val())==""){alert("작성된 내용이 없습니다. 내용을 입력하세요.");oEditors.getById["contDesc"].exec("FOCUS");return false;}
			if(confirm("<%= alertMsg[3] %>")==0) return false;
			$("#frmDefault").attr({"action":"/ptnhexa/phone/PageUpdate.do","method":"post","target":"_self"}).ajaxSubmit({
				dataType      : "json",
				cache         : false ,
				beforeSubmit  : function(data,form,options){doActLoadShow();},
				success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn();}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
				error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
			});
		};
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' name='page' value='phone' />

<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/mngr/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="2"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/ptnhexa/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='<%= webBase + subFirst %>'>메인 관리</a></li>
				<li><a href='<%= webBase %>/ptnhexa/Stand/LiveEdit.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='board_list'>
				<br/>
				<p style="color:#ef3a18">* 내용을 수정 후 저장하시면 자동 반영됩니다.</p>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:38%;'/><col style='width:12%;'/><col style='width:38%;'/></colgroup>
					<tbody><tr>
						<th scope='col'>내용</th>
						<td colspan='3'>
							<textarea id='contDesc' name='contDesc' rows='20' class='w99_p'>${dataView.contents}</textarea>
						</td>
					</tr></tbody>
				</table>
				<div style="height:500px !important;"></div>
				<div class='btn_area right'>
					<%--<c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole)}">--%>
					<input type='button' class='w70 btn_medium bt_red'    value='저장'       onclick='fnActUpdate()'/>
					<%--</c:if>--%>
				</div>
			</div>
		</div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/mngr/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html><%	//} %>