<%
////////////////////////////////////////////////////////////
// Program ID  : movieCoun_view
// Description : 청소년 > 모의회의 - 의회동영상 [상세조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "의회동영상";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" 청소년의회" %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scripty.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb03_03").addClass("on").removeClass("noChild");
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"movie"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self"}).submit();
	};
	var fnActDetail     = function(view){
		doActLoadShow();
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"movieView.do","method":"post","target":"_self"}).submit();
	};
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/youth/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/youth/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="4"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 모의회의 &gt; <%= subTitle %></div>
			<div class='table_view'><table>
				<caption class='hide'>Board View</caption>
				<colgroup><col style='width:10%;'/><col style='width:30%;'/><col style='width:10%;'/><col style='width:20%;'/><col style='width:10%;'/><col style='width:20%;'/></colgroup>
				<tbody><tr>
					<th>제목</th>
					<td colspan='5'>${dataView.bbsTitle}</td>
				</tr><tr>
					<th scope='col'>작성자</th>
					<td>${dataView.creName}</td>
					<th scope='col'>작성일</th>
					<td>${dataView.creDate}</td>
					<th scope='col'>조회수</th>
					<td>${dataView.hitCount}</td>
				</tr><tr class='h250'>
					<td colspan='6' class='nonebd_l'><c:if test="${!empty dataView.downUUID}"><video id='vod' oncontextmenu='return false;' width='100%' controls<% if( viewPoster ) { %> preload='metadata'<c:if test="${!empty dataView.fileUUID}"> poster='<% if( imgMode ) { %><%= webBase %>/GetImage.do?key=${dataView.fileUUID}<% } else { %><%= webBase + uploadPath %>${dataView.fileAbs}<% } %>'</c:if><% } else { %> autoplay preload='auto'<% } %>><% if( vodMode ) { %>
						<source src='<%= webBase %>/GetMovie.do?key=${dataView.downUUID}' type='video/${dataView.downExt}'/><% } else { %>
						<source src='<%= viewPort + webBase %>${dataView.downAbs}' type='video/${dataView.downExt}'/><% } %>
					</video></c:if></td>
				</tr></tbody>
			</table></div>
			<div class='btn_group right'>
				<a href='javascript:void(0);' class='btn_style03' onclick='fnActReturn("seq")'>목록</a>
			</div><c:if test="${'T' eq moveItem}">
			<div class='table_view table_nextprev'><table>
				<caption class='hide'>Previous/Next</caption>
				<colgroup><col style='width:10%;'/><col style='width:90%;'/></colgroup>
				<tbody><tr>
					<th scope='col'>이전글</th>
					<td><c:choose><c:when test="${ empty prevView}">이전 글이 존재 하지 않습니다.</c:when><c:otherwise>${prevView.gabSpace}<a href='javascript:void(0);' onclick='fnActDetail("${prevView.viewNo}")'><c:if test="${'Y' eq prevView.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${prevView.bbsTitle}</a></c:otherwise></c:choose></td>
				</tr><tr>
					<th scope='col'>다음글</th>
					<td><c:choose><c:when test="${ empty nextView}">다음 글이 존재 하지 않습니다.</c:when><c:otherwise>${nextView.gabSpace}<a href='javascript:void(0);' onclick='fnActDetail("${nextView.viewNo}")'><c:if test="${'Y' eq nextView.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${nextView.bbsTitle}</a></c:otherwise></c:choose></td>
				</tr></tbody>
			</table></div></c:if>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/youth/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>