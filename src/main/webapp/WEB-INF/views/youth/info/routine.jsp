<%
////////////////////////////////////////////////////////////
// Program ID  : routine
// Description : 청소년 > 의회소개 - 의회에서 하는 일
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "의회에서 하는 일";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" 청소년의회" %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scripty.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb01_03").addClass("on").removeClass("noChild");
	});
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/youth/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/youth/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="1"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 의회소개 &gt; <%= subTitle %></div>
			<div class='section'>
				<h4 class='h4_tit'>의회는 어떤 역할을 할까요?</h4>
				<p class='p_tit'>의회는 주민을 대표하여 지역의 정책결정자, 지역대표자 및 갈등조정자, 행정감시자로서 조례의 제·개정, 폐지 및 예산의 심의 확정, 결산승인의 권한을 가지고 있으며 집행부에 대하여 행정사무 감사 및 조사를 하고 주민이 제출한 청원 및 진정을 접수·처리하는 기능을 합니다. 또한 조직과 운영에 있어서 회의규칙 제정권, 회의의 개폐 및 회기결정권 등 의사운영의 자율권과 의장 부의장 불신임권, 질서유지권, 내부조사권 등의 내부운영에 관한 자율권을 가집니다.</p>
				<div class='grey_box m_left_20'><ul>
					<li>평택시의 법률이라고 할 수 있는 조례를 제정, 개정하거나 폐지하는 일</li>
					<li>시민이 내는 세금으로 짜여진 우리시의 예산을 심의, 확정하고 적정하게 쓰여졌는지 확인하는 일</li>
					<li>시의 재산을 관리하는 일과 주민에게 부담을 주는 사용료, 수수료, 지방세 등의 부과와 징수에 관한 사항을 의논하고 결정하는 일</li>
					<li>시청에서 일을 잘 하고 있는가 또는 일을 잘못 처리하고 있는 것은 없는가를 구정 질문과 행정 사무감사를 통해 감시하고 감독하는 일</li>
					<li>시민들이 간절히 바라는 일(청원과 진정)을 접수 처리하는 일</li>
				</ul></div>
			</div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/youth/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>