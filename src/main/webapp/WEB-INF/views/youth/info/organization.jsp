<%
////////////////////////////////////////////////////////////
// Program ID  : organization
// Description : 청소년 > 의회소개 - 의회구성
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "의회구성";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" 청소년의회" %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scripty.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb01_02").addClass("on").removeClass("noChild");
	});
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/youth/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/youth/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="1"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 의회소개 &gt; <%= subTitle %></div>
			<div class='section'>
				<h4 class='h4_tit'>의회 조직도</h4>
				<div class='img_area'><img src='<%= webBase %>/images/youth/organi_img.png' alt='조직도'/></div>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>의원</h4>
				<div class='list_style01'><ul>
					<li>시의원은 주민의 보통ㆍ평등ㆍ직접ㆍ비밀선거에 의해 선출되는 시민의 대표로서 평택시의회는 16명의 의원으로 구성되어 있다.</li>
				</ul></div>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>의장/부의장</h4>
				<div class='list_style01'><ul>
					<li>의원 중에서 의장과 부의장 각 1인을 무기명 투표로 선출한다.</li>
					<li>의장은 의회를 대표하고 의사를 정리하며, 회의장 내의 질서를 유지하고 의회의 사무를 감독하며, 부의장은 의장 유고시 그 직무를 대리한다.</li>
				</ul></div>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>위원회</h4>
				<div class='list_style01 m_b_20'><ul>
					<li>상임위원회는 집행기관의 부서별 소관업무에 따라 의회운영위원회, 자치행정위원회, 산업건설위원회 등 3개 위원회로 구성되어 있으며, 특정 안건을 심의ㆍ의결하기 위하여 필요한 경우에 특별위원회를 설치할 수 있다.</li>
				</ul></div>
				<div class='content_table m_left_20'><table>
					<caption class='hide'>상임위원회</caption>
					<colgroup><col style='width:18%;'/><col style='width:82%;'/></colgroup>
					<tbody><tr>
						<th>의회운영위원회 (6명)</th>
						<td scope='col' class='left'>의회 운영에 관한 사항, 의회사무국 소관에 관한 사항, 회의규칙 및 의회 운영과 관련된 각종 규칙에 관한 사항</td>
					</tr><tr>
						<th>자치행정위원회(7명)</th>
						<td class='left'>소통홍보관, 감사관, 안전총괄관, 기획조정실, 총무국, 사회복지국(복지재단, 청소년재단), 평택보건소, 송탄보건소, 한미협력사업단(국제교류재단), 문화예술회관, 영상정보운영사업소, 여성회관, 도서관, 출장소 및 읍·면·동 소관에 속하는 사항</td>
					</tr><tr>
						<th>산업건설위원회(8명)</th>
						<td class='left'>항만경제전략국, 환경농정국, 도시주택국, 건설교통국, 농업기술센터, 상하수도사업소, 차량등록사업소, 평택도시공사 소관에 속하는 사항</td>
					</tr></tbody>
				</table></div>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>의회사무국</h4>
				<div class='list_style01'><ul>
					<li>의회의 사무를 처리하기 위하여 설치된 의회사무국은 사무국장을 비롯하여 현재 26명의 직원으로 구성되어 있다.</li>
				</ul></div>
			</div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/youth/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>