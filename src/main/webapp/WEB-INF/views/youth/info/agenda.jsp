<%
////////////////////////////////////////////////////////////
// Program ID  : agenda
// Description : 청소년 > 의회에서 하는 일 - 의안처리
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "의안처리";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" 청소년의회" %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scripty.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb02_01").addClass("on").removeClass("noChild");
	});
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/youth/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/youth/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="2"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 의회에서 하는 일 &gt; <%= subTitle %></div>
			<div class='section'>
				<h4 class='h4_tit'>의안이란</h4>
				<p class='p_tit'>
					회의에서 심의하고 토의할 모든 종류의 안건을 의안이라 합니다.<br/>
					의안은 지방자치단체장(평택시장), 의원, 의회의 위원회에서 제안할 수 있으며, 제안 내용은 문서로 작성하여 제출합니다. 이를 “발의”(發議)라 하고 의원이 발의하는 경우 재적의원 1/5 이상이 발의하여야 한다는 규정이 있습니다.
				</p>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>평택시의회에서 논의되는 의안의 종류</h4>
				<div class='content_table m_left_20'><table>
					<caption class='hide'>사무국 정 현원 - 24명</caption>
					<colgroup>
						<col style='width:15%;'/>
						<col style='width:60%;'/>
						<col style='width:25%;'/>
					</colgroup>
					<tbody><tr>
						<th scope='col'>안 건</th>
						<th scope='col'>내 용</th>
						<th scope='col'>제 안 자</th>
					</tr><tr>
						<td>조례안</td>
						<td class='left'><ul>
							<li>평택시 조례를 새롭게 만든다는 제정 조례안</li>
							<li>기존의 조례를 수정한다는 개정 조례안</li>
							<li>기존의 조례를 폐지하려는 폐지 조례안</li>
						</ul></td>
						<td>평택시장, 위원회, <br/>재적의원 1/5 이상</td>
					</tr><tr>
						<td>예산안</td>
						<td class='left'><ul>
							<li>내년에 지출할 예산을 확정해달라고 제출하는 예산안</li>
							<li>작년에 계획한 예산 이외에 지출을 허가해 달라는 추가경정 예산안</li>
						</ul></td>
						<td>평택시장</td>
					</tr><tr>
						<td>결산</td>
						<td class='left'><ul>
							<li>작년에 지출한 내역을 승인해달라고 제출하는 결산 승인 요청안</li>
						</ul></td>
						<td>평택시장</td>
					</tr><tr>
						<td>청원</td>
						<td class='left'><ul>
							<li>시민이 억울한 일을 당하거나 평택시에 바라는 일이 있을 때 의원의 소개를 얻어 청원서 제출</li>
						</ul></td>
						<td>시민과 소개의원</td>
					</tr><tr>
						<td>기타 안건</td>
						<td class='left'><ul>
							<li>각종 동의&middot;승인안, 건의안, 결의안, 의견 청취 등</li>
						</ul></td>
						<td>위원회, 의원 10인 이상</td>
					</tr></tbody>
				</table></div>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>의안처리</h4>
				<div class='img_area'><img src='<%= webBase %>/images/youth/agenda01_01.png' alt='의안처리'/></div>
			</div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/youth/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>