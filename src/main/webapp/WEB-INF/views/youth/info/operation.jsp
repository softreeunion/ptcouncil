<%
////////////////////////////////////////////////////////////
// Program ID  : operation
// Description : 청소년 > 의회소개 - 의회운영
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "의회운영";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" 청소년의회" %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scripty.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb01_04").addClass("on").removeClass("noChild");
	});
	var doActProgram    = function() { 
		window.open("<%= webBase %>/cmmn/program.do","program","status=no,toolbar=no,resizable=no,menubar=no,location=yes,left=200,top=100,width=1024,height=800,scrollbars=yes");
	};
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/youth/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/youth/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="1"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 의회소개 &gt; <%= subTitle %></div>
			<div class='section'>
				<h4 class='h4_tit'>의회의 소집과 회기</h4>
				<div class='grey_info'>
					의회의 소집과 회기 정례회는 매년 6월 1일과 11월 19일에 45일 이내로 집회하며, 임시회는 시장 또는 재적의원 1/3 이상의 요구로 매 회기 15일 이내로 소집되고 연간 회의 일수는 정례회와 임시회를 합쳐 100일을 초과할 수 없다.<%--
					<div class='btn_group left'><a href='javascript:void(0);' class='s_btn_style03' onclick='doActProgram()'>연간 회기일정 보기</a></div>--%>
				</div>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>정례회</h4>
				<div class='content_table_row'><table>
					<caption class='hide'>정례회에 대한 정보</caption>
					<colgroup>
						<col style='width:15%;'/>
						<col style='width:85%;'/>
					</colgroup>
					<tbody><tr>
						<th>집회</th>
						<td scope='col'>매년 6월 1일과 11월 19일에 집회한다. 다만, 총선거가 실시되는 연도의 제 1차 정례회는 의회의 의결로 9월, 10월 중에 따로 정할 수 있다.</td>
					</tr><tr>
						<th>회기</th>
						<td>45일 이내</td>
					</tr><tr>
						<th>주요 활동</th>
						<td>결산서의 승인(제1차 정례회), 행정사무감사(제2차 정례회)<br/>예산안의 의결, 조례안 등 기타 안건을 처리함</td>
					</tr></tbody>
				</table></div>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>임시회</h4>
				<div class='content_table_row'><table>
					<caption class='hide'>임시회에 대한 정보</caption>
					<colgroup>
						<col style='width:15%;'/>
						<col style='width:85%;'/>
					</colgroup>
					<tbody><tr>
						<th>집회</th>
						<td scope='col'>시장 또는 재적의원 1/3 이상의 요구 시 집회함(지방자치법 제39조)</td>
					</tr><tr>
						<th>회기</th>
						<td>55일 이내</td>
					</tr><tr>
						<th>주요 활동</th>
						<td>주요 현안에 대한 집행부 측의 설명을 듣고 질의 답변을 함<br/>조례안, 추경안 등 계류된 안건을 심사함.</td>
					</tr></tbody>
				</table></div>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>특별위원회 운영</h4>
				<p class='p_tit'><b>특별위원회는</b> 특정한 안건을 심사 처리하기 위하여 필요한 경우에 본회의의 의결로 설치 운영하고 있으며, 특별위원회는 그 위원회에서 심사한 안건이 본회의에서 의결될 때까지 존속하며, 임기는 위원회의 존속기간으로 한다.</p>
			</div><%--
			<div class='section'>
				<h4 class='h4_tit'>위원회 진행절차</h4>
				<div class='oper_step m_left_20'><ul>
					<li><strong>1</strong><p>안건상정</p></li>
					<li><strong>2</strong><p>제안설명</p></li>
					<li><strong>3</strong><p>전문의원 검토보고</p></li>
					<li><strong>4</strong><p>질의·답변</p></li>
					<li><strong>5</strong><p>찬·반토론</p></li>
					<li><strong>6</strong><p>표결(의결)</p></li>
					<li><strong class='blue'>7</strong><p>본회의 상정</p></li>
				</ul></div>
				<div class='oper_info m_left_20'>※전문위원 : 각 위원회에는 의원이 아닌 전문지식을 가진 전문위원이 있으며, 의안 심사 등 관련 분야의 자료를 조사·연구하여 의원의 입법 활동을 지원합니다.</div>
			</div>--%>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/youth/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>