<%
////////////////////////////////////////////////////////////
// Program ID  : infoduce
// Description : 청소년 > 의회에서 하는 일 - 회의 진행방식
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "회의 진행방식";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" 청소년의회" %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scripty.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb02_03").addClass("on").removeClass("noChild");
	});
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/youth/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/youth/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="2"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 의회에서 하는 일 &gt; <%= subTitle %></div>
			<div class='section'>
				<h4 class='h4_tit'>회의는 어떻게 진행 되나요?</h4>
				<p class='p_tit'>임시회나 정례회가 개최되면 먼저 개회식을 갖고 본회의에서 그 회기 동안 처리할 안건등 의사일정을 확정하고 회기를 결정하여 운영함.</p>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>본 회의</h4>
				<p class='p_tit'>본회의는 의회의 전체 의원으로 구성되는 회의체로서 의회에 제출되는 모든 안건은 본회의의 의결을 거쳐 최종적으로 결정되며 특정한 사항을 제외하고는 일반 안건의 표결은 재적의원 과반수 출석과 출석의원 과반수 찬성으로 의결하는데 의장도 표결권을 가지며, 표결 결과가 가·부동 수인 경우에는 부결된 것으로 본다.</p>
				<div class='content_table_row m_left_20'><table>
					<caption class='hide'>회의 진행 방법</caption>
					<colgroup>
						<col style='width:20%;'/>
						<col style='width:80%;'/>
					</colgroup>
					<tbody><tr>
						<th scope='col'>① 개의 선포</th>
						<td>○ 의장이 &quot;00회 임시회 00차 회의&quot;개의 선언 → 의사봉 3타</td>
					</tr><tr>
						<th>② 보고</th>
						<td>○ 사무국장 보고 - 안건제출, 심사현황등 기타 필요한 사항</td>
					</tr><tr>
						<th>③ 의사일정 상정</th>
						<td>○ 당일 처리할 안건 순서대로 상정 - 조례안 등<br/>&nbsp;&nbsp;&nbsp;- 관계 공무원 출석요구의 건</td>
					</tr><tr>
						<th>④ 제안설명 (심사보고)</th>
						<td>○ 위원회 심사 거친 안건은 각 위원장이 심사보고<br/>○ 위원회 심사를 거치지 아니한 안건은 제안자 직접 제안설명<br/>&nbsp;&nbsp;&nbsp;- 안건 제출 배경 및 이유, 주요 골자 설명<br/>&nbsp;&nbsp;&nbsp;- 심사결과는 제안자, 제안이유, 주요골자, 심사경과, 소수의견, 심사결과 등 설명</td>
					</tr><tr>
						<th>⑤ 질의, 답변</th>
						<td>○ 질의 신청은 미리 의장에게 통지<br/>○ 상정된 안건에 대한 의문점을 묻는 것<br/>○ 답변은 제안설명한 자, 심사보고한 자가 함<br/>○ 질의와 답변을 마치면 질의 종결을 선포<br/>&nbsp;&nbsp;&nbsp;- 질의종결을 선포한 후에는 더 이상 질의하지 못함</td>
					</tr><tr>
						<th>⑥ 토 론</th>
						<td>○ 토론도 미리 의장에게 찬·반의 뜻을 분명히 기재 신청<br/>○ 반대토론을 먼저 한 후 찬성의 순으로 진행<br/>○ 토론을 마치면 토론 종결 선포<br/>○ 질의와 답변을 마치면 질의 종결을 선포<br/>&nbsp;&nbsp;&nbsp;- 토론 종결을 선포한 후에는 더 이상 토론하지 못함</td>
					</tr><tr>
						<th>⑦ 의 결 (표결)</th>
						<td>○ 일반적으로 토론이나 질의가 없는 경우 이의 유무 방식으로 의결<br/>○ 이의가 있는 경우 기립, 거수표결로 찬반 집계 의결<br/>○ 기명, 무기명 투표도 가능</td>
					</tr></tbody>
				</table></div>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>위원회</h4>
				<p class='p_tit'>효율적인 의회 운영과 각종 의안을 전문적으로 심사하기 위해 위원회를 두고 있으며 위원회는 본회의 심의에 앞서 전문적이고 예비적인 심사기능을 갖고 있으며 상임위원회는 조례에 의하여 구성되고 특별위원회는 본회의의 의결로 구성된다.</p>
				<div class='content_table_row m_left_20'><table>
					<caption class='hide'>회의 진행 방법</caption>
					<colgroup>
						<col style='width:20%;'/>
						<col style='width:80%;'/>
					</colgroup>
					<tbody><tr>
						<th scope='col'>① 개의선포</th>
						<td>○ 위원장이 &quot;00회 임시회 00위원회 0차 회의&quot;개의 선언 → 의사봉 3타</td>
					</tr><tr>
						<th>② 의사 일정 상정</th>
						<td>○ 조례안 상정 → 의사봉 3타</td>
					</tr><tr>
						<th>③ 제안설명</th>
						<td>○ 발의 의원 및 관계 공무원 설명</td>
					</tr><tr>
						<th>④ 전문위원 검토보고</th>
						<td>○ 회부된 안건의 문제점, 이해득실, 기타 필요한 사항을 조사 검토하게 하여 심사에 참고(활용)</td>
					</tr><tr>
						<th>⑤ 질의, 답변</th>
						<td>○ 질의 횟수 및 제한이 없음<br/>○ 질의할 위원은 사전 위원장에게 신청해야 함(구두로 가능)<br/>○ 질의, 답변이 끝나면 질의 종결 선포<br/>&nbsp;&nbsp;&nbsp;- 질의 종결을 선포한 후에는 더 이상 질의하지 못함</td>
					</tr><tr>
						<th>⑥ 토 론</th>
						<td>○ 사전에 위원장에게 토론 신청(구두로 가능)<br/>○ 반대토론 후 찬성토론<br/>○ 토론이 끝나면 토론 종결 선포<br/>&nbsp;&nbsp;&nbsp;- 토론 종결을 선포한 후에는 더 이상 토론하지 못함</td>
					</tr><tr>
						<th>⑦ 의 결 (표결)</th>
						<td>○ 이의 유무 및 기립, 거수표결 방법 주로 이용<br/>○ 위원장도 표결권을 가지나 가부동수일 경우 부결</td>
					</tr><tr>
						<th>⑧ 심사보고</th>
						<td>○ 위원회 심사 결과를 의장에게 보고</td>
					</tr></tbody>
				</table></div>
			</div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/youth/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>