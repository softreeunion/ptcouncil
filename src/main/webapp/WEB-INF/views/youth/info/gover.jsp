<%
////////////////////////////////////////////////////////////
// Program ID  : gover
// Description : 청소년 > 의회에서 하는 일 - 행정처리
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "행정처리";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" 청소년의회" %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scripty.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb02_02").addClass("on").removeClass("noChild");
	});
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/youth/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/youth/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="2"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 의회에서 하는 일 &gt; <%= subTitle %></div>
			<div class='section'>
				<h4 class='h4_tit'>청원처리</h4>
				<p class='p_tit'>
					시민이 평택시의회에 직접 의견을 내는 것을 청원이라 합니다. 공공기관의 불합리한 일 때문에 피해를 입거나 공공시설의 운영에 불만이 있을 때, 이를 고쳐달라고 의견을 제출할 수 있습니다. 청원은 접수되면 위원회의 심의를 받아 본회의에 상정되어 의안과 동일한 방법으로 처리됩니다. 따라서 민원이나 진정과는 구분되며 남용되지 않도록 평택시의원의 소개를 받아 접수하도록 규정하고 있습니다.<br/>
					<b>잠깐! 청원은 접수되지 않을 수도 있어요. 재판에 간섭하거나 법령에 위배되는 내용의 청원은 접수받지 않습니다.</b>
				</p>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>청원처리절차</h4>
					<div class='dl_style01'>
					<dl>
						<dt>
							<dl>
								<dt>01</dt>
								<dd>청원서 제출</dd>
							</dl>
						</dt>
						<dd>
							<ul>
								<li>의원의 소개를 얻어 제출합니다.</li>
							</ul>
						</dd>
					</dl>
					<dl>
						<dt>
							<dl>
								<dt>02</dt>
								<dd>청원의 수리</dd>
							</dl>
						</dt>
						<dd>
							<ul>
								<li>청원이 접수되면 의원들에게 배부됩니다.</li>
							</ul>
						</dd>
					</dl>
					<dl>
						<dt>
							<dl>
								<dt>03</dt>
								<dd>소관 위원회<br/> 회부ㆍ심사</dd>
							</dl>
						</dt>
						<dd>
							<ul>
								<li>관련 위원회로 보내지며 위원회에서 청원 내용이 타당한지 심사하여 본회의로 올려보낼지 결정합니다.</li>
							</ul>
						</dd>
					</dl>
					<dl>
						<dt>
							<dl>
								<dt>04</dt>
								<dd>본회의 심사ㆍ의결</dd>
							</dl>
						</dt>
						<dd>
							<ul>
								<li>평택시의회에서 해당 청원을 받아들일지 최종 심사를 하여 결정합니다.</li>
							</ul>
						</dd>
					</dl>
					<dl>
						<dt>
							<dl>
								<dt>05</dt>
								<dd>평택시장에게 이송</dd>
							</dl>
						</dt>
						<dd>
							<ul>
								<li>본회의에서 의결되면 평택시장에게 청원의 처리를 요구합니다.</li>
								<li>평택시장은 청원을 처리하여 결과를 의회에게 보고합니다.</li>
							</ul>
						</dd>
					</dl>
				</div>
			</div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/youth/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>