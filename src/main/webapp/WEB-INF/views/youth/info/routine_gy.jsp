<%
////////////////////////////////////////////////////////////
// Program ID  : routine
// Description : 청소년 > 의회소개 - 의회에서 하는 일
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "의회에서 하는 일";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" 청소년의회" %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scripty.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb01_03").addClass("on").removeClass("noChild");
	});
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/youth/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/youth/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="1"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 의회소개 &gt; <%= subTitle %></div>
			<div class='section'>
				<div class='grey_box m_left_20'>
					<h4 class='p_tit'>평택시의회에서는 <font color='red'>이런 일들을 합니다</font>.</h4>
					<p class='p_tit'>국회가 국민을 대표하여 나라일을 논의하고 결정하듯이 평택시의회는 시민이 선출한 의원으로 구성된 의결기간으로서 우리 지역의 일을 자체 능력으로 처리하면서 시민복지 증진을 위해 여러가지 일을 합니다.</p>
				</div>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>조례 제·개정 및 폐지</h4>
				<p class='p_tit'>우리 팽택시민이 지켜야할 법을 조례라고 합니다. 팽택시의회는 시민들에게 꼭 필요한 법을 새로 만들기도 하고, 시민들이 지키고 있는 법이 예전에 만들어져 오늘날에 맞지 않는 부분이 있다면 이를 수정하기도 합니다. 또한 만약 시민들에게 피해를 주고 있는 법이 있다면 폐지하도록 하기도 한답니다.</p>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>예산안 의결</h4>
				<p class='p_tit'>예산안이란 팽택시가 내년에 세금이 얼마나 걷힐지 예측하고 또 그 세금을 어디에 얼마만큼 쓸지 미리 계획을 세워 특정한 계수로 표시한 내년 살림살이 예정표를 말합니다. 의회는 매년 11월 20일 열리는 제2차 정례회에서 팽택시장에게 예산안을 제출받아 이를 심사합니다. 세금이 얼마나 어떻게 걷힐지 예측한 것이 타당한지, 또 세금을 쓰는 지출계획이 시민을 위한 것인지 혹여 낭비는 없는지 세밀하게 검사하는 것이지요. 심사 중에 잘못된 부분이 있으면 수정을 요구하기도 합니다. 예산안에 대한 모든 심사가 완료되면 승인을 의결하게 되고, 팽택시장은 내년부터 그 예산안 대로 세금을 걷고 시민을 위한 사업에 지출하게 됩니다.</p>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>결산승인</h4>
				<p class='p_tit'>작년 한해동안 팽택시에서 세금을 걷고 지출한 내역을 특정한 계수로 표시하는 것을 결산이라 하고, 의회에서는 매년 7월 5일 열리는 제1차 정례회에서 결산을 심사합니다. 결산 심사는 예산대로 지출이 잘 집행되었는지 또 예산집행 중에 불법적인 내용은 없었는지를 검사하여 최종적으로 승인여부를 결정합니다. 결산 승인이 중요한 이유는 결산을 심사한 내용을 바탕으로 행정사무감사를 실시하기 때문입니다. 행정사무감사에서는 결산 심사에서 1차적으로 심사했던 예산집행 내역을 좀더 상세하게 조사합니다.</p>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>시정질문</h4>
				<p class='p_tit'>지방자치단체에서 하는 일은 너무나 다양하고 복합적이어서 일반 시민들이 모든 내용을 알기가 어렵습니다. 이에 의회에서는 팽택시민을 대표하여 지방자치단체에 시민들이 궁금해하는 사항을 질문할 수 있습니다. 지방자치단체는 여러 가지가 있으나 그 중 우리 생활과 가장 밀접한 곳은 바로 팽택시 살림살이를 운영하는 팽택시입니다. 의회는 팽택시장과 공무원에게 출석을 요구하여 팽택시가 시행하고 있는 사업에 대한 의문점이나 지역주민들이 불편해하는 사항의 해결방법 등을 질문하고 답변을 듣습니다. 시민들은 시정질문을 통하여 팽택시가 시민을 위해 어떤 정책을 만들고 있으며 앞으로 팽택시의 정책은 어떤 방향으로 나아갈지를 확인합니다.</p>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>행정사무감사 및 조사</h4>
				<p class='p_tit'>행정사무감사 및 조사는 한 해동안의 팽택시 행정사무에 대하여 조사하고 문제가 없는지 확인하는 것입니다. 위의 시정질문이 팽택시의 일부 정책이나 미래 정책에 대한 포괄적인 답변을 듣는 것이라면, 행정사무감사 및 조사는 구체적으로 팽택시가 했던 업무를 검사하고 확인하는 것입니다. 이 중 행정사무감사는 매년 11월 20일에 열리는 2차 정례회 때마다 실시되며, 행정사무 전체를 대상으로 감사를 실시합니다. 행정사무조사는 이 중 좀 더 상세한 조사가 필요한 부분이 있을 때 회의를 소집하여 실시됩니다. 행정사무감사와 다른 점은 행정사무 중 특정사무에 대하여 조사한다는 점이며, 조사를 실시하기 위하여 재적의원의 1/3 이상이 조사를 위한 회의 소집을 요구하여야 하고 본회의의 의결을 통하여 조사를 할지 말지 결정한다는 것입니다.</p>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>청원심사</h4>
				<p class='p_tit'>시민들이 팽택시로부터 피해를 받았거나 팽택시 정책에 대한 요청이 있을 때, 이를 해결해 달라고 의회에 청원을 낼수 있습니다. 청원은 일반 민원이나 진정과 달리 시의원의 소개서를 받아 의회에 접수합니다. 이 청원서는 법령에 의거하여 의회에서 접수하며, 청원이 타당한지 심사하는 과정을 거쳐 최종 본회의에서 승인 여부를 의결하게 됩니다. 가결(승인)로 의결된 청원은 팽택시장에게 전송되어 시민의 요구사항 대로 처리되게 합니다.</p>
			</div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/youth/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>