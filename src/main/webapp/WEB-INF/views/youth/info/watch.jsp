<%
////////////////////////////////////////////////////////////
// Program ID  : watch
// Description : 청소년 > 방청참관 - 방청참관
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "방청참관";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" 청소년의회" %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scripty.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb03_01").addClass("on").removeClass("noChild");
	});
	var fnAppDown       = function(){
		var $form=$("#fileDown");
		if($form.length<=0){$form=$("<form/>").attr({id:"fileDown",name:"fileDown",method:"post"});$(document.body).append($form);}
		if($form.length<=1){$form.empty();}
		if($form.length<=1){$("<input/>").attr({type:"hidden",id:"fileAbs",name:"fileAbs",value:"application_of_visit.hwp|"}).appendTo($form);}
		if($form.length<=1){$("<input/>").attr({type:"hidden",id:"fileRel",name:"fileRel",value:"평택시의회_참관신청서.hwp|"}).appendTo($form);}
		if($form.length<=1){$form.attr({"action":"<%= webBase %>/cmmn/TempDown.do","method":"post"}).submit();}
	};
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/youth/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/youth/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="3"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 방청참관 &gt; <%= subTitle %></div>
			<div class='section'>
				<h4 class='h4_tit'>방청안내</h4>
				<p class='p_tit m_b_10'>평택시의회는 시민과 함께 하는 열린의회 구현과 더불어 시민의 의정 참여 기회를 제공코자 회기 중 (정례회, 임시회) 본회의 및 상임위원회 회의진행과정을 공개하고 있사오니 시민여러분의 적극 참여를 바랍니다.</p>
				<div class='watch_area m_left_20'><ul>
					<li><strong>일반 방청권</strong><p>일반인에게 교부하는 방청권</p></li>
					<li><strong>단체 방청권</strong><p>교육기관 또는 기타 단체의 신청에 의하여 그 대표 또는 책임자에게 교부합니다.</p></li>
					<li><strong>장기 방청권</strong><p>보도기관 종사자나 업무상 방청이 특히 필요한 관서의 직원에게 교부하고 방청권을 교부 받은자는 그 회기를 통하여 방청할 수 있습니다.</p></li>
				</ul></div>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>방문 방청신청 및 교부방법</h4>
				<div class='list_style01'><ul>
					<li>방청인은 주소, 성명, 직업 및 연령 등을 기재후 방청권을 신청하여 교부받습니다.</li>
					<li>단체방청권은 대표 또는 책임자에게 교부합니다.</li>
					<li>장기방청권은 보도기관 종사자등에게 교부하며, 장기방청권을 받은 자는 그 회기를 통하여 방청합니다.</li>
					<li>신청부서 : 평택시의회 의정팀 (☎ 031-8024-7564)</li>
				</ul></div>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>인터넷 방청신청 및 교부방법</h4>
				<div class='list_style01'><ul>
					<li>회의개최 당일 직접 방문, 신청허가를 받거나 인터넷 신청 후 방청허가를 받으신 분께 방청권을 교부합니다.</li>
					<li class='none'>※ 신분증 제시 후 방청권을 교부 받아 방청 할 수 있습니다.</li>
					<li>인터넷 방청신청은 회의 1일 전에 신청하여 주시기 바라며 방청허가 여부는 별도로 연락드립니다.</li>
					<li>본회의장 방청석이 협소한 관계로 방청인원은 5명 이내로 제한합니다.</li>
				</ul></div>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>단체 참관안내</h4>
				<div class='list_style01'><ul>
					<li>단체참관(10인 이상)은 미리 의회사무국으로 예약 후 참관신청서를 작성하시면 절차와 방법에 대하여 친절히 안내해 드리고 있습니다.</li>
					<li>방청 및 참관 문의 : 평택시의회 의정팀 (☎ 031-8024-7564)</li>
					<li class='none'>※ 참관 : 의회의 방청을 목적으로 하지 아니하는 의회내의 기구와 시설을 시찰함을 말합니다.</li>
				</ul></div>
			</div>
			<div class='section'>
				<h4 class='h4_tit'>방청인 준수사항</h4>
				<div class='list_style01'><ul>
					<li>회의장 안으로 진입하는 행위 및 모자·외투착용을 금합니다.</li>
					<li>음식물 섭취 및 흡연·신문 기타 서적류의 반입을 금지합니다.</li>
					<li>녹음·녹화·촬영에 대하여는 사전에 별도의 허가신청을 하여야 합니다.</li>
					<li>의원발언에 대한 가부표명 또는 박수 등 소란행위를 금지합니다.</li>
					<li>휴대폰은 진동으로 하거나 꺼 주십시오.</li>
					<li>경위공무원의 요구가 있을 때에는 언제든지 방청권을 제시하여야 하며 방청규칙을 위반 하였을 때에는 퇴장을 당할 수 있습니다. 신분증을 꼭 지참하시기 바랍니다.</li>
				</ul></div>
				<%--
				<div class='btn_group right'>
					<a href='javascript:void(0);' class='s_btn_style03' onclick='fnAppDown()'>참관신청서 다운로드</a>
				</div>
				--%>
			</div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/youth/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>