<%
////////////////////////////////////////////////////////////
// Program ID  : greetings
// Description : 청소년 > 의회소개 - 인사말
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "인사말";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" 청소년의회" %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scripty.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb01_01").addClass("on").removeClass("noChild");
	});
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/youth/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/youth/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="1"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 의회소개 &gt; <%= subTitle %></div>
			<div class='greetings'>
				<div class='img'><img src='<%= webBase %>/images/coun/img_greetings.png' alt='의장 이미지'/></div>
				<div class='txt'>
					<!--
					<h4>홈페이지를 방문해주신 청소년 여러분 환영합니다.</h4>
					<p>
						살기 좋은 평택 건설의 막중한 책임을 안고 제8대 평택시의회가 출범하였습니다.<br/>
						저를 비롯한 16명의 평택시의회 의원들은 시민과 함께 희망찬 평택시의 미래를 열고 시민들께서 부여해 주신 임무를 완수하기 위해 <br/><br/>
						<strong>
						살고 싶은 평택, 시민이 행복한 평택을 만드는 의회 <br/>
						시민의 입장에서 소통하고 화합하는 책임 있는 의회 <br/>
						시정의 견제·감시라는 본연의 역할에 충실한 의회 <br/>
						희망과 비전을 제시하는 정책의회”를 만들겠습니다.
						</strong> <br/><br/>
						시민 여러분께 공약하고 다짐했던 초심을 잃지 않겠습니다. <br/>
						50만 시민 모두 안팎으로 풍요로운 행복을 누릴 수 있도록 열정을 가지고 세심하게 살피겠습니다. <br/><br/>

						언제나 현장에서 시민들의 목소리를 경청하며, 시민들과 소통하며 함께 할 것을 굳게 약속드립니다. <br/>
						제8대 평택시의회에 대한 시민 여러분의 많은 관심과 성원을 부탁드립니다. <br/><br/>

						감사합니다.
					</p>
					<div class='sign'>평택시의회 의장<strong>권영화</strong></div>
					-->
					<div style="height:150px"></div>
					<h4 style="text-align: center">준비 중입니다.</h4>
					<div style="height:150px"></div>
					<div class='sign'>평택시의회 의장<strong>강정구</strong></div>
				</div>
			</div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/youth/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>