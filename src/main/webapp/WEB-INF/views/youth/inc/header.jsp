<%
////////////////////////////////////////////////////////////
// Program ID  : header
// Description : 청소년 / HEADER
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	int gnbSub				= 0;
	StringBuffer dispMenu	= new StringBuffer();
	StringBuffer dispFull	= new StringBuffer();
	StringBuffer dispMobi	= new StringBuffer();
	if( !(youthAList == null || youthAList.length <= 0) ) { 
		gnbSub++;
		dispMenu.append("<li>");
		dispMenu.append("\n"+ baseTab[6] +"<a href='"+ webBase + youthAList[ 0][4] +"'>의회소개</a>");
		dispMenu.append("\n"+ baseTab[6] +"<div id='gnb_sub"+ BaseUtility.zeroFill( gnbSub, 2 ) +"' class='gnb_sub'><ul>");
		dispFull.append("<li>");
		dispFull.append("\n"+ baseTab[6] +"<a href='"+ webBase + youthAList[ 0][4] +"'>의회소개</a>");
		dispFull.append("\n"+ baseTab[6] +"<div class='fullmenu_list_sub'><ul>");
		dispMobi.append("<li>");
		dispMobi.append("\n"+ baseTab[5] +"<a href='"+ webBase + youthAList[ 0][4] +"'>의회소개</a>");
		dispMobi.append("\n"+ baseTab[5] +"<div class='mobile_menu_list_sub'><ul>");
		for( int m = 0; m < youthAList.length; m++ ) { 
			if( "T".equals(youthAList[m][0]) && "1".equals(youthAList[m][2]) ) { 
				dispMenu.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(youthAList[m][1])?youthAList[m][4]+"' target='_blank":webBase+youthAList[m][4]) +"'>"+ youthAList[m][3] +"</a></li>");
				dispFull.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(youthAList[m][1])?youthAList[m][4]+"' target='_blank":webBase+youthAList[m][4]) +"'>"+ youthAList[m][3] +"</a>").append("N".equals(youthAList[m][1])?"</li>":"");
				if( "C".equals(youthAList[m][1]) ) dispFull.append("\n"+ baseTab[8] +"<ul>");
				dispMobi.append("\n"+ baseTab[6] +"<li><a href='"+ ("L".equals(youthAList[m][1])?youthAList[m][4]+"' target='_blank":webBase+youthAList[m][4]) +"'>"+ youthAList[m][3] +"</a>").append("N".equals(youthAList[m][1])?"</li>":"");
				if( "C".equals(youthAList[m][1]) ) dispMobi.append("\n"+ baseTab[7] +"<ul>");
			}
			if( "T".equals(youthAList[m][0]) && "2".equals(youthAList[m][2]) ) { 
				dispFull.append("\n"+ baseTab[9] +"<li><a href='"+ ("L".equals(youthAList[m][1])?youthAList[m][4]+"' target='_blank":webBase+youthAList[m][4]) +"'>"+ youthAList[m][3] +"</a></li>");
				dispMobi.append("\n"+ baseTab[8] +"<li><a href='"+ ("L".equals(youthAList[m][1])?youthAList[m][4]+"' target='_blank":webBase+youthAList[m][4]) +"'>"+ youthAList[m][3] +"</a></li>");
			}
//			if( m != youthAList.length-1 && "2".equals(youthAList[m][2]) ) { 
//				if( "1".equals(youthAList[m+1][2]) ) dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
//				if( "1".equals(youthAList[m+1][2]) ) dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
//			}
			if( "T".equals(youthAList[m][0]) && "E".equals(youthAList[m][1]) ) { 
				dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
				dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
			}
		}
		dispMenu.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispFull.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispMobi.append("\n"+ baseTab[5] +"</ul></div>").append("\n"+ baseTab[4] +"</li>");
	}
	if( !(youthBList == null || youthBList.length <= 0) ) { 
		gnbSub++;
		dispMenu.append("<li>");
		dispMenu.append("\n"+ baseTab[6] +"<a href='"+ webBase + youthBList[ 0][4] +"'>의회에서하는일</a>");
		dispMenu.append("\n"+ baseTab[6] +"<div id='gnb_sub"+ BaseUtility.zeroFill( gnbSub, 2 ) +"' class='gnb_sub'><ul>");
		dispFull.append("<li>");
		dispFull.append("\n"+ baseTab[6] +"<a href='"+ webBase + youthBList[ 0][4] +"'>의회에서하는일</a>");
		dispFull.append("\n"+ baseTab[6] +"<div class='fullmenu_list_sub'><ul>");
		dispMobi.append("<li>");
		dispMobi.append("\n"+ baseTab[5] +"<a href='"+ webBase + youthBList[ 0][4] +"'>의회에서하는일</a>");
		dispMobi.append("\n"+ baseTab[5] +"<div class='mobile_menu_list_sub'><ul>");
		for( int m = 0; m < youthBList.length; m++ ) { 
			if( "T".equals(youthBList[m][0]) && "1".equals(youthBList[m][2]) ) { 
				dispMenu.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(youthBList[m][1])?youthBList[m][4]+"' target='_blank":webBase+youthBList[m][4]) +"'>"+ youthBList[m][3] +"</a></li>");
				dispFull.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(youthBList[m][1])?youthBList[m][4]+"' target='_blank":webBase+youthBList[m][4]) +"'>"+ youthBList[m][3] +"</a>").append("N".equals(youthBList[m][1])?"</li>":"");
				if( "C".equals(youthBList[m][1]) ) dispFull.append("\n"+ baseTab[8] +"<ul>");
				dispMobi.append("\n"+ baseTab[6] +"<li><a href='"+ ("L".equals(youthBList[m][1])?youthBList[m][4]+"' target='_blank":webBase+youthBList[m][4]) +"'>"+ youthBList[m][3] +"</a>").append("N".equals(youthBList[m][1])?"</li>":"");
				if( "C".equals(youthBList[m][1]) ) dispMobi.append("\n"+ baseTab[7] +"<ul>");
			}
			if( "T".equals(youthBList[m][0]) && "2".equals(youthBList[m][2]) ) { 
				dispFull.append("\n"+ baseTab[9] +"<li><a href='"+ ("L".equals(youthBList[m][1])?youthBList[m][4]+"' target='_blank":webBase+youthBList[m][4]) +"'>"+ youthBList[m][3] +"</a></li>");
				dispMobi.append("\n"+ baseTab[8] +"<li><a href='"+ ("L".equals(youthBList[m][1])?youthBList[m][4]+"' target='_blank":webBase+youthBList[m][4]) +"'>"+ youthBList[m][3] +"</a></li>");
			}
//			if( m != youthBList.length-1 && "2".equals(youthBList[m][2]) ) { 
//				if( "1".equals(youthBList[m+1][2]) ) dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
//				if( "1".equals(youthBList[m+1][2]) ) dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
//			}
			if( "T".equals(youthBList[m][0]) && "E".equals(youthBList[m][1]) ) { 
				dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
				dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
			}
		}
		dispMenu.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispFull.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispMobi.append("\n"+ baseTab[5] +"</ul></div>").append("\n"+ baseTab[4] +"</li>");
	}
	if( !(youthCList == null || youthCList.length <= 0) ) { 
		gnbSub++;
		dispMenu.append("<li>");
		dispMenu.append("\n"+ baseTab[6] +"<a href='"+ webBase + youthCList[ 0][4] +"'>방청참관</a>");
		dispMenu.append("\n"+ baseTab[6] +"<div id='gnb_sub"+ BaseUtility.zeroFill( gnbSub, 2 ) +"' class='gnb_sub'><ul>");
		dispFull.append("<li>");
		dispFull.append("\n"+ baseTab[6] +"<a href='"+ webBase + youthCList[ 0][4] +"'>방청참관</a>");
		dispFull.append("\n"+ baseTab[6] +"<div class='fullmenu_list_sub'><ul>");
		dispMobi.append("<li>");
		dispMobi.append("\n"+ baseTab[5] +"<a href='"+ webBase + youthCList[ 0][4] +"'>방청참관</a>");
		dispMobi.append("\n"+ baseTab[5] +"<div class='mobile_menu_list_sub'><ul>");
		for( int m = 0; m < youthCList.length; m++ ) { 
			if( "T".equals(youthCList[m][0]) && "1".equals(youthCList[m][2]) ) { 
				dispMenu.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(youthCList[m][1])?youthCList[m][4]+"' target='_blank":webBase+youthCList[m][4]) +"'>"+ youthCList[m][3] +"</a></li>");
				dispFull.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(youthCList[m][1])?youthCList[m][4]+"' target='_blank":webBase+youthCList[m][4]) +"'>"+ youthCList[m][3] +"</a>").append("N".equals(youthCList[m][1])?"</li>":"");
				if( "C".equals(youthCList[m][1]) ) dispFull.append("\n"+ baseTab[8] +"<ul>");
				dispMobi.append("\n"+ baseTab[6] +"<li><a href='"+ ("L".equals(youthCList[m][1])?youthCList[m][4]+"' target='_blank":webBase+youthCList[m][4]) +"'>"+ youthCList[m][3] +"</a>").append("N".equals(youthCList[m][1])?"</li>":"");
				if( "C".equals(youthCList[m][1]) ) dispMobi.append("\n"+ baseTab[7] +"<ul>");
			}
			if( "T".equals(youthCList[m][0]) && "2".equals(youthCList[m][2]) ) { 
				dispFull.append("\n"+ baseTab[9] +"<li><a href='"+ ("L".equals(youthCList[m][1])?youthCList[m][4]+"' target='_blank":webBase+youthCList[m][4]) +"'>"+ youthCList[m][3] +"</a></li>");
				dispMobi.append("\n"+ baseTab[8] +"<li><a href='"+ ("L".equals(youthCList[m][1])?youthCList[m][4]+"' target='_blank":webBase+youthCList[m][4]) +"'>"+ youthCList[m][3] +"</a></li>");
			}
//			if( m != youthCList.length-1 && "2".equals(youthCList[m][2]) ) { 
//				if( "1".equals(youthCList[m+1][2]) ) dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
//				if( "1".equals(youthCList[m+1][2]) ) dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
//			}
			if( "T".equals(youthCList[m][0]) && "E".equals(youthCList[m][1]) ) { 
				dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
				dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
			}
		}
		dispMenu.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispFull.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispMobi.append("\n"+ baseTab[5] +"</ul></div>").append("\n"+ baseTab[4] +"</li>");
	}
	if( !(youthDList == null || youthDList.length <= 0) ) { 
		gnbSub++;
		dispMenu.append("<li>");
		dispMenu.append("\n"+ baseTab[6] +"<a href='"+ webBase + youthDList[ 0][4] +"'>모의의회</a>");
		dispMenu.append("\n"+ baseTab[6] +"<div id='gnb_sub"+ BaseUtility.zeroFill( gnbSub, 2 ) +"' class='gnb_sub'><ul>");
		dispFull.append("<li>");
		dispFull.append("\n"+ baseTab[6] +"<a href='"+ webBase + youthDList[ 0][4] +"'>모의의회</a>");
		dispFull.append("\n"+ baseTab[6] +"<div class='fullmenu_list_sub'><ul>");
		dispMobi.append("<li>");
		dispMobi.append("\n"+ baseTab[5] +"<a href='"+ webBase + youthDList[ 0][4] +"'>모의의회</a>");
		dispMobi.append("\n"+ baseTab[5] +"<div class='mobile_menu_list_sub'><ul>");
		for( int m = 0; m < youthDList.length; m++ ) { 
			if( "T".equals(youthDList[m][0]) && "1".equals(youthDList[m][2]) ) { 
				dispMenu.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(youthDList[m][1])?youthDList[m][4]+"' target='_blank":webBase+youthDList[m][4]) +"'>"+ youthDList[m][3] +"</a></li>");
				dispFull.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(youthDList[m][1])?youthDList[m][4]+"' target='_blank":webBase+youthDList[m][4]) +"'>"+ youthDList[m][3] +"</a>").append("N".equals(youthDList[m][1])?"</li>":"");
				if( "C".equals(youthDList[m][1]) ) dispFull.append("\n"+ baseTab[8] +"<ul>");
				dispMobi.append("\n"+ baseTab[6] +"<li><a href='"+ ("L".equals(youthDList[m][1])?youthDList[m][4]+"' target='_blank":webBase+youthDList[m][4]) +"'>"+ youthDList[m][3] +"</a>").append("N".equals(youthDList[m][1])?"</li>":"");
				if( "C".equals(youthDList[m][1]) ) dispMobi.append("\n"+ baseTab[7] +"<ul>");
			}
			if( "T".equals(youthDList[m][0]) && "2".equals(youthDList[m][2]) ) { 
				dispFull.append("\n"+ baseTab[9] +"<li><a href='"+ ("L".equals(youthDList[m][1])?youthDList[m][4]+"' target='_blank":webBase+youthDList[m][4]) +"'>"+ youthDList[m][3] +"</a></li>");
				dispMobi.append("\n"+ baseTab[8] +"<li><a href='"+ ("L".equals(youthDList[m][1])?youthDList[m][4]+"' target='_blank":webBase+youthDList[m][4]) +"'>"+ youthDList[m][3] +"</a></li>");
			}
//			if( m != youthDList.length-1 && "2".equals(youthDList[m][2]) ) { 
//				if( "1".equals(youthDList[m+1][2]) ) dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
//				if( "1".equals(youthDList[m+1][2]) ) dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
//			}
			if( "T".equals(youthDList[m][0]) && "E".equals(youthDList[m][1]) ) { 
				dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
				dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
			}
		}
		dispMenu.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispFull.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispMobi.append("\n"+ baseTab[5] +"</ul></div>").append("\n"+ baseTab[4] +"</li>");
	}

	out.clearBuffer(); %>	<div id='skipNavi'><a href='#contents'>본문 바로가기</a></div>
	<div class='top_link'>
		<div class='inner'><ul>
			<li><a href='<%= webBase %>/index.do'>평택시의회</a></li>
			<li><a href='javascript:void(0);'>상임위원회</a>
				<div class='top_link_sub'><ul>
					<li><a href='<%= webBase %>/oper/index.do'>의회운영위원회</a></li>
					<li><a href='<%= webBase %>/gvadm/index.do'>자치행정위원회</a></li>
					<li><a href='<%= webBase %>/welfare/index.do'>복지환경위원회</a></li>
					<li><a href='<%= webBase %>/build/index.do'>산업건설위원회</a></li>
				</ul></div>
			</li>
			<li><a href='<%= webBase %>/youth/index.do' class='on'>청소년의회</a></li>
		</ul></div>
	</div>
	<!-- Base Header -->
	<div id='header'>
		<div class='search_top'><div class='inner'>
			<h1><a href='<%= webBase %>/youth/index.do'><img src='<%= webBase %>/images/common/logo_youth.png' alt='<%= webTitle %> 청소년의회'/></a></h1>
			<div class='zoom'>
				<a href='javascript:void(0);' class='btn_plus'  onclick='zoomcontrol.zoomin();' ><img src='<%= webBase %>/images/icon/ico_plus.png'  alt='확대'/></a>
				<a href='javascript:void(0);' class='btn_minus' onclick='zoomcontrol.zoomout();'><img src='<%= webBase %>/images/icon/ico_minus.png' alt='축소'/></a>
			</div>
		</div></div>
		<div class='gnb_wrap'>
			<div class='gnb'>
				<div class='inner'>
					<ul><%= dispMenu.toString() %></ul>
				</div>
			</div>
			<div class='fullmenu'>
				<a href='javascript:void(0);' class='open_fullmenu'><img src='<%= webBase %>/images/icon/ico_menu.png' alt='전체메뉴'/></a>
				<div class='fullmenu_list'>
					<ul><%= dispFull.toString() %></ul>
				</div>
			</div>
		</div>
		<div class='bg_gnb_sub'></div>
		<div class='bg_fullmenu_list'></div>
	</div>
	<!-- //Base Header -->
	<!-- //mobile Header -->
	<div id='mobile_header'>
		<a href='javascript:void(0);' class='open_mobile_menu'><img src='<%= webBase %>/images/btns/btn_mobile_menu.png' alt='전체메뉴'/></a>
		<h1><a href='<%= webBase %>/youth/index.do'><img src='<%= webBase %>/images/common/logo_youth.png' alt='<%= webTitle %>'/></a></h1>
		<a href='javascript:void(0);' class='open_mobile_search'><img src='<%= webBase %>/images/btns/btn_mobile_search.png' alt='검색'/></a>
		<div class='mobile_menu'>
			<div class='logo'><img src='<%= webBase %>/images/common/logo_youth.png' alt='<%= webTitle %>'/></div>
			<a href='javascript:void(0);' class='close_mobile_menu'><img src='<%= webBase %>/images/btns/btn_close_mobile_gnb.png' alt='전체메뉴 닫기'/></a>
			<div class='mobile_menu_list'>
				<ul><%= dispMobi.toString() %></ul>
			</div>
		</div>
	</div>
	<!-- //mobile Header -->