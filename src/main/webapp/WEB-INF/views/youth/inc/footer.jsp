<%
////////////////////////////////////////////////////////////
// Program ID  : footer
// Description : 상임위 / FOOTER
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
//  v1.1    HexaMedia new0man      2020.03.19  언론보도 명칭 변경(언론보도 -> 보도자료)
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String tbParam			= BaseUtility.null2Char( request.getParameter("tb"), "T" );
	String qkParam			= BaseUtility.null2Char( request.getParameter("qk"), "F" );

	out.clearBuffer();
	out.print(baseTab[1] +"<div id='footer'>\n");
	out.print(baseTab[2] +"<div class='inner'>\n");
	out.print(baseTab[3] +"<div class='foot_logo'><img src='"+ webBase +"/images/common/logo_base.png' alt='"+ webTitle +"'/></div>\n");
	out.print(baseTab[3] +"<div class='foot_info'>\n");
//	out.print(baseTab[4] +"<div class='foot_menu'><ul>\n");
//	out.print(baseTab[5] +"<li><a href='"+ webBase + youthAList[ 0][4] +"'>의회소개</a></li>\n");
//	out.print(baseTab[5] +"<li><a href='"+ webBase + youthAList[ 4][4] +"'>개인정보취급방침</a></li>\n");
//	out.print(baseTab[5] +"<li id='visitor'>오늘:0명 &nbsp; 전체:0명</li>\n");
//	out.print(baseTab[4] +"</ul></div>\n");
	out.print(baseTab[4] +"<div class='foot_tel'><ul>\n");
	out.print(baseTab[5] +"<li>(17730)경기도 평택시 경기대로 1366(서정동)</li>\n");
	out.print(baseTab[5] +"<li>대표전화 <span>031-8024-7560(의정팀)</span> <span>031-8024-7570(의사팀)</span> <span>031-8024-7580(입법홍보팀)</span></li>\n");
	out.print(baseTab[4] +"</ul></div>\n");
	out.print(baseTab[4] +"<div class='copyright'>Copyright PyeongTaek City Council. All Rights Reserved.</div>\n");
	out.print(baseTab[3] +"</div>\n");
	if( markupValid ) { 
		out.print(baseTab[3] +"<div class='family_site'>\n");
		out.print(baseTab[4] +"<p><a href='http://validator.kldp.org/check?uri=referer' onclick='this.href=this.href.replace(/referer$/,encodeURIComponent(document.URL))'><img src='//validator.kldp.org/w3cimgs/validate/html5-blue.png' alt='Valid HTML 5' height='23' width='120'></a></p>");
		out.print(baseTab[3] +"</div>\n");
	}
//	out.print(baseTab[3] +"<div class='family_site'>\n");
//	out.print(baseTab[4] +"<label for='familySite' class='hide'>패밀리사이트 선택</label>\n");
//	out.print(baseTab[4] +"<select id='familySite'>\n");
//	out.print(baseTab[5] +"<option value=''>관련사이트</option>\n");
//	out.print(baseTab[5] +"<option value=''>관련사이트</option>\n");
//	out.print(baseTab[4] +"</select>\n");
//	out.print(baseTab[3] +"</div>\n");
	out.print(baseTab[2] +"</div>\n");
	out.print(baseTab[1] +"</div>\n");
	if( "T".equals(tbParam) ) out.print(baseTab[1] +"<input style='visibility:hidden;width:0;'/><label for='' class='hide'>Input</label>\n");
	if( "T".equals(qkParam) ) { 
		out.print(baseTab[1] +"<div class='right_quick'>\n");
		out.print(baseTab[2] +"<h2>QUICK</h2>\n");
		out.print(baseTab[2] +"<ul>\n");
		out.print(baseTab[3] +"<li><a href='"+ webBase + counEList[ 5][4] +"' id='right_quick01'>회의록 검색</a></li>\n");
		out.print(baseTab[3] +"<li><a href='"+ webBase + counBList[ 0][4] +"' id='right_quick02'>의원정보</a></li>\n");
		out.print(baseTab[3] +"<li><a href='"+ webBase + counGList[ 3][4] +"' id='right_quick03'>보도자료</a></li>\n");
		out.print(baseTab[3] +"<li><a href='"+ webBase + counGList[ 2][4] +"' id='right_quick04'>의회소식지</a></li>\n");
		out.print(baseTab[3] +"<li><a href='"+ webBase + counFList[ 1][4] +"' id='right_quick05'>의회에 바란다</a></li>\n");
		out.print(baseTab[3] +"<li><a href='"+ webBase + counDList[ 0][4] +"' id='right_quick06'>영상회의록</a></li>\n");
		out.print(baseTab[3] +"<li><a href='"+ webBase + "/coun/intro/phone.do" +"' id='right_quick07'>전화번호안내</a></li>\n");
		out.print(baseTab[2] +"</ul>\n");
		out.print(baseTab[1] +"</div>\n");
	}
//	out.print(baseTab[1] +"<div id='mask'          style='position:absolute;left:0;top:0;z-index:9000;display:none;'></div>\n");
//	out.print(baseTab[1] +"<div id='loadLayer'     style='width:0;z-index:9999;padding:0;border:0;display:none;'><img src='"+ webBase +"/images/common/loading.gif' style='width:64px;height:64px;'/></div>\n");
	out.print(baseTab[1] +"<div id='baseDN-modal'  style='display:none;' title='기본 프로그레스'><div id='progressBarLD' style='width:100%;height:22px;margin-top:38px;'></div></div>\n");
	out.print(baseTab[1] +"<div id='fileDN-modal'  style='display:none;' title='파일 다운로드'  ><div id='progressBarDN' style='width:100%;height:22px;margin-top:38px;'></div></div>\n");
	out.print(baseTab[1] +"<div id='errorDN-modal' style='display:none;' title='오류'           ><p style='margin-top:20px;'>엑셀파일 생성이 실패하였습니다.<br><br>관리자에게 문의바랍니다.</p></div>\n");
	out.print(baseTab[1] +"<div id='fileUP-modal'  style='display:none;' title='파일 업로드'    ><div id='progressBarUP' style='width:100%;height:22px;margin-top:38px;'></div></div>\n");
	out.print(baseTab[1] +"<div id='errorUP-modal' style='display:none;' title='오류'           ><p style='margin-top:20px;'>엑셀파일 처리가 실패하였습니다.<br><br>관리자에게 문의바랍니다.</p></div>\n");
	out.print(baseTab[1] +"<div id='progress'      class='progress'      title='파일 업로드'    ><div class='progress-bar progress-bar-success'></div></div>\n");
//	out.print(baseTab[1] +"<div id='popMovie'      class='pop_movie' style='display:none;'><a name='closeMovie' class='close_pop_movie'><img src='"+ webBase +"/image/btn/btn_close.png' alt='close'/></a><iframe frameborder='0'></iframe></div>\n");
//	out.print(baseTab[1] +"<div id='popVideo'      class='pop_video' style='display:none;'><a name='closeVideo' class='close_pop_video'><img src='"+ webBase +"/image/btn/btn_close.png' alt='close'/></a><iframe frameborder='0' allowfullscreen></iframe></div>\n"); %>