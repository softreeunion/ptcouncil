<%
////////////////////////////////////////////////////////////
// Program ID  : lefter
// Description : 청소년 / LEFTER
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String tbParam			= BaseUtility.null2Blank( request.getParameter("tb") );
	StringBuffer dispMenu	= new StringBuffer();
	int[] snbIdx			= {0,0};

	if( "1".equals(tbParam) && !(youthAList == null || youthAList.length <= 0) ) { 
		dispMenu.append(baseTab[2] +"<div id='snb'>\n");
		dispMenu.append(baseTab[3] +"<h2 class='h2_tit'>의회소개</h2>\n");
		dispMenu.append(baseTab[3] +"<ul>\n");
		for( int m = 0; m < youthAList.length; m++ ) {
			if( "1".equals(youthAList[m][2]) ) snbIdx[0]++;
			if( "T".equals(youthAList[m][0]) && "1".equals(youthAList[m][2]) ) { 
				dispMenu.append(baseTab[4] +"<li><a href='"+ ("L".equals(youthAList[m][1])?youthAList[m][4]+"' target='_blank":webBase+youthAList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2));
				dispMenu.append("'"+ ("N".equals(youthAList[m][1])?" class='noChild'":"") +">"+ youthAList[m][3] +"</a>"+ ("N".equals(youthAList[m][1])?"</li>":"") +"\n");
				if( "C".equals(youthAList[m][1]) ) dispMenu.append(baseTab[5] +"<ul>\n");
			}
			if( "T".equals(youthAList[m][0]) && "2".equals(youthAList[m][2]) ) { 
				snbIdx[1]++;
				dispMenu.append(baseTab[6] +"<li><a href='"+ ("L".equals(youthAList[m][1])?youthAList[m][4]+"' target='_blank":webBase+youthAList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2) +"_"+ BaseUtility.zeroFill(snbIdx[1],2));
				dispMenu.append("'>"+ youthAList[m][3] +"</a></li>\n");
			}
			if( "T".equals(youthAList[m][0]) && "E".equals(youthAList[m][1]) ) { 
				snbIdx[1] = 0;
				dispMenu.append(baseTab[5] +"</ul>\n").append(baseTab[4] +"</li>\n");
			}
		}
		dispMenu.append(baseTab[3] +"</ul>\n").append(baseTab[2] +"</div>\n");
	}
	if( "2".equals(tbParam) && !(youthBList == null || youthBList.length <= 0) ) { 
		dispMenu.append(baseTab[2] +"<div id='snb'>\n");
		dispMenu.append(baseTab[3] +"<h2 class='h2_tit'>의회에서하는일</h2>\n");
		dispMenu.append(baseTab[3] +"<ul>\n");
		for( int m = 0; m < youthBList.length; m++ ) {
			if( "1".equals(youthBList[m][2]) ) snbIdx[0]++;
			if( "T".equals(youthBList[m][0]) && "1".equals(youthBList[m][2]) ) { 
				dispMenu.append(baseTab[4] +"<li><a href='"+ ("L".equals(youthBList[m][1])?youthBList[m][4]+"' target='_blank":webBase+youthBList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2));
				dispMenu.append("'"+ ("N".equals(youthBList[m][1])?" class='noChild'":"") +">"+ youthBList[m][3] +"</a>"+ ("N".equals(youthBList[m][1])?"</li>":"") +"\n");
				if( "C".equals(youthBList[m][1]) ) dispMenu.append(baseTab[5] +"<ul>\n");
			}
			if( "T".equals(youthBList[m][0]) && "2".equals(youthBList[m][2]) ) { 
				snbIdx[1]++;
				dispMenu.append(baseTab[6] +"<li><a href='"+ ("L".equals(youthBList[m][1])?youthBList[m][4]+"' target='_blank":webBase+youthBList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2) +"_"+ BaseUtility.zeroFill(snbIdx[1],2));
				dispMenu.append("'>"+ youthBList[m][3] +"</a></li>\n");
			}
			if( "T".equals(youthBList[m][0]) && "E".equals(youthBList[m][1]) ) { 
				snbIdx[1] = 0;
				dispMenu.append(baseTab[5] +"</ul>\n").append(baseTab[4] +"</li>\n");
			}
		}
		dispMenu.append(baseTab[3] +"</ul>\n").append(baseTab[2] +"</div>\n");
	}
	if( "3".equals(tbParam) && !(youthCList == null || youthCList.length <= 0) ) { 
		dispMenu.append(baseTab[2] +"<div id='snb'>\n");
		dispMenu.append(baseTab[3] +"<h2 class='h2_tit'>방청참관</h2>\n");
		dispMenu.append(baseTab[3] +"<ul>\n");
		for( int m = 0; m < youthCList.length; m++ ) {
			if( "1".equals(youthCList[m][2]) ) snbIdx[0]++;
			if( "T".equals(youthCList[m][0]) && "1".equals(youthCList[m][2]) ) { 
				dispMenu.append(baseTab[4] +"<li><a href='"+ ("L".equals(youthCList[m][1])?youthCList[m][4]+"' target='_blank":webBase+youthCList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2));
				dispMenu.append("'"+ ("N".equals(youthCList[m][1])?" class='noChild'":"") +">"+ youthCList[m][3] +"</a>"+ ("N".equals(youthCList[m][1])?"</li>":"") +"\n");
				if( "C".equals(youthCList[m][1]) ) dispMenu.append(baseTab[5] +"<ul>\n");
			}
			if( "T".equals(youthCList[m][0]) && "2".equals(youthCList[m][2]) ) { 
				snbIdx[1]++;
				dispMenu.append(baseTab[6] +"<li><a href='"+ ("L".equals(youthCList[m][1])?youthCList[m][4]+"' target='_blank":webBase+youthCList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2) +"_"+ BaseUtility.zeroFill(snbIdx[1],2));
				dispMenu.append("'>"+ youthCList[m][3] +"</a></li>\n");
			}
			if( "T".equals(youthCList[m][0]) && "E".equals(youthCList[m][1]) ) { 
				snbIdx[1] = 0;
				dispMenu.append(baseTab[5] +"</ul>\n").append(baseTab[4] +"</li>\n");
			}
		}
		dispMenu.append(baseTab[3] +"</ul>\n").append(baseTab[2] +"</div>\n");
	}
	if( "4".equals(tbParam) && !(youthDList == null || youthDList.length <= 0) ) { 
		dispMenu.append(baseTab[2] +"<div id='snb'>\n");
		dispMenu.append(baseTab[3] +"<h2 class='h2_tit'>모의의회</h2>\n");
		dispMenu.append(baseTab[3] +"<ul>\n");
		for( int m = 0; m < youthDList.length; m++ ) {
			if( "1".equals(youthDList[m][2]) ) snbIdx[0]++;
			if( "T".equals(youthDList[m][0]) && "1".equals(youthDList[m][2]) ) { 
				dispMenu.append(baseTab[4] +"<li><a href='"+ ("L".equals(youthDList[m][1])?youthDList[m][4]+"' target='_blank":webBase+youthDList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2));
				dispMenu.append("'"+ ("N".equals(youthDList[m][1])?" class='noChild'":"") +">"+ youthDList[m][3] +"</a>"+ ("N".equals(youthDList[m][1])?"</li>":"") +"\n");
				if( "C".equals(youthDList[m][1]) ) dispMenu.append(baseTab[5] +"<ul>\n");
			}
			if( "T".equals(youthDList[m][0]) && "2".equals(youthDList[m][2]) ) { 
				snbIdx[1]++;
				dispMenu.append(baseTab[6] +"<li><a href='"+ ("L".equals(youthDList[m][1])?youthDList[m][4]+"' target='_blank":webBase+youthDList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2) +"_"+ BaseUtility.zeroFill(snbIdx[1],2));
				dispMenu.append("'>"+ youthDList[m][3] +"</a></li>\n");
			}
			if( "T".equals(youthDList[m][0]) && "E".equals(youthDList[m][1]) ) { 
				snbIdx[1] = 0;
				dispMenu.append(baseTab[5] +"</ul>\n").append(baseTab[4] +"</li>\n");
			}
		}
		dispMenu.append(baseTab[3] +"</ul>\n").append(baseTab[2] +"</div>\n");
	}

	out.clearBuffer();
	out.print(dispMenu.toString()); %>