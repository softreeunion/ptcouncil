<%
////////////////////////////////////////////////////////////
// Program ID  : exe_main
// Description : 메인화면으로 전환
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" isELIgnored="false" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	response.setHeader("pragma", "no-cache"); response.setHeader("cache-control", "no-cache"); response.setHeader("expires", "0");
	java.util.Enumeration headers = request.getHeaderNames();
	while(headers.hasMoreElements()) {
		String headerName = (String)headers.nextElement();
		String value = request.getHeader(headerName);
		System.out.println("headerName:"+headerName+" :::: "+value);
	} %>