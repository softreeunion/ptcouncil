<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String tbParam			= BaseUtility.null2Blank( request.getParameter("tb") );
	out.clearBuffer();%>
	var doActLoadShow   = function(){$.blockUI({message:$("#loadImage"),css:{border:"0px solid #a00",backgroundColor:"rgba(0,0,0,0.0)",color:"#fff"}});};
	var doActLoadHide   = function(){setTimeout($.unblockUI,100);};
	var doActLoadTop    = function(){$("html,body").animate({scrollTop:$("#frmDefault").offset().top},0);};
	var fnCheckPasswd   = function(){
		var chk_num = $("#userPWI").val().search(/[0-9]/g);
		var chk_eng = $("#userPWI").val().search(/[a-z]/ig);
		if(/(\w)\1\1\1/.test($("#userPWI").val())) {alert("비밀번호에 같은 문자를 4번 이상 사용하실 수 없습니다.");$("#userPWI").focus();return false;}
		if(!/^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{8,20}$/.test($("#userPWI").val())) {alert("비밀번호는 숫자와 영문자, 특수문자 조합으로 8~20자리를 사용해야 합니다.");$("#userPWI").focus();return false;}
		if(chk_num<0||chk_eng<0) {alert("비밀번호는 숫자와 영문자를 혼용해야 합니다.");$("#userPWI").focus();return false;}
		if($("#userPWI").val().search($("#userPNO").val())>-1) {alert("사용자ID가 포함된 비밀번호는 사용하실 수 없습니다.");$("#userPWI").focus();return false;}
		return true;
	};
	var doActPrint      = function(){window.open("<%= request.getContextPath() %>/cmmn/print.do","print","status=no,toolbar=no,resizable=no,menubar=no,location=yes,left=200,top=100,width=1024,height=800,scrollbars=yes");};
	var fnActClose      = function(){if(top.frames.length>0){top.window.open("about:blank","_self").close();}else if(self.frames.length<=0){self.window.open("about:blank","_self").close();}else{window.open("about:blank","_self").close();}};
	var fnThmbRemove    = function(seq,sid,tid){
		eval("$(\"#file_thmb"+seq+"\")").empty().append("<label for='fileThmb' class='hide'>첨부파일</label><input type='file' id='fileThmb' name='fileThmb' class='w90_p' accept='image/*'/><%= allowFiles[2] %>");
		$("#condDele").val($("#condDele").val()+sid+"`");
		$("#tImg").empty();
	};
	var fnNoneRemove    = function(seq,sid,tid){
		eval("$(\"#file_base"+seq+"\")").empty().append("<label for='fileBase' class='hide'>첨부파일</label><input type='file' id='fileBase' name='fileBase' class='w90_p'"+(isEmpty(tid)?"/><%= allowFiles[0] %>":" accept='video/*'/><%= allowFiles[5] %>"));
		$("#condDele").val($("#condDele").val()+sid+"`");
	};
	var fnBaseRemove    = function(seq,sid,tid){
		eval("$(\"#file_base"+seq+"\")").empty().append("<label for='fileBase"+seq+"' class='hide'>첨부파일</label><input type='file' id='fileBase"+seq+"' name='fileBase"+seq+"' class='w90_p'"+(isEmpty(tid)?"/><%= "m".equals(tbParam) ? allowFiles[0] : "" %>":" accept='video/*'/><%= "m".equals(tbParam) ? allowFiles[5] : "" %>"));
		$("#condDele").val($("#condDele").val()+sid+"`");
	};
	var fnActDownload   = function(fileID,fileTP){
		if( isEmpty(fileID)){alert("적용할 내역이 존재하지 않습니다.\n\n확인하시기 바랍니다.");return false;}
		var $form=$("#fileDown");
		if($form.length<=0){$form=$("<form/>").attr({id:"fileDown",name:"fileDown",method:"post"});$(document.body).append($form);}
		if($form.length<=1){$form.empty();}
		if(!isEmpty(fileID)){$("<input/>").attr({type:"hidden",id:"fileID",name:"fileID",value:fileID}).appendTo($form);}
		if(!isEmpty(fileTP)){$("<input/>").attr({type:"hidden",id:"fileType",name:"fileType",value:fileTP}).appendTo($form);}
		if($form.length<=1){$form.attr({"action":"<%= request.getContextPath() %>/cmmn/FileDown.do","method":"post"}).submit();}
	};
	var fnAppDownload   = function(fileID,fileTP){
		if( isEmpty(fileID)){alert("적용할 내역이 존재하지 않습니다.\n\n확인하시기 바랍니다.");return false;}
		var $form=$("#fileDown");
		if($form.length<=0){$form=$("<form/>").attr({id:"fileDown",name:"fileDown",method:"post"});$(document.body).append($form);}
		if($form.length<=1){$form.empty();}
		if(!isEmpty(fileID)){$("<input/>").attr({type:"hidden",id:"fileID",name:"fileID",value:fileID}).appendTo($form);}
		if(!isEmpty(fileTP)){$("<input/>").attr({type:"hidden",id:"fileType",name:"fileType",value:fileTP}).appendTo($form);}
		if($form.length<=1){$form.attr({"action":"<%= request.getContextPath() %>/cmmn/AppDown.do","method":"post"}).submit();}
	};
	var fnSrcDownload   = function(fileID,fileTP){
		if( isEmpty(fileID)){alert("적용할 내역이 존재하지 않습니다.\n\n확인하시기 바랍니다.");return false;}
		var $form=$("#fileDown");
		if($form.length<=0){$form=$("<form/>").attr({id:"fileDown",name:"fileDown",method:"post"});$(document.body).append($form);}
		if($form.length<=1){$form.empty();}
		if(!isEmpty(fileID)){$("<input/>").attr({type:"hidden",id:"fileID",name:"fileID",value:fileID}).appendTo($form);}
		if(!isEmpty(fileTP)){$("<input/>").attr({type:"hidden",id:"fileType",name:"fileType",value:fileTP}).appendTo($form);}
		if($form.length<=1){$form.attr({"action":"<%= request.getContextPath() %>/cmmn/SrcDown.do","method":"post"}).submit();}
	};
	var fnDocuViewer    = function(view,mode){
		var $form=$("#frmDetail");
		if($form.length<=0){$form=$("<form/>").attr({id:"frmDetail",name:"frmDetail",method:"get"});$(document.body).append($form);}
		if($form.length<=1){$form.empty();}
		if($form.length<=1){$("<input/>").attr({type:"hidden",id:"searchText",name:"searchText",value:$("#searchText").val()}).appendTo($form);}
		if(!isEmpty(view )){$("<input/>").attr({type:"hidden",id:"key",name:"key",value:view}).appendTo($form);}
		if(!isEmpty(mode )){$("<input/>").attr({type:"hidden",id:"kag",name:"kag",value:mode}).appendTo($form);}<%--
		if($form.length<=1){$form.attr({"action":"<%= request.getContextPath() %>/cmmn/DocuViewer.do","method":"get"}).submit();}--%>
		if($form.length<=1){$form.attr({"action":"<%= request.getContextPath() %>/cmmn/DocuViewer.do"+(!isEmpty(mode)?"#"+mode:""),"method":"get",target:"_blank"}).submit();}
	};
	var fnCastViewer    = function(view,mode,time){
		var $form=$("#frmDetail");
		if($form.length<=0){$form=$("<form/>").attr({id:"frmDetail",name:"frmDetail",method:"get"});$(document.body).append($form);}
		if($form.length<=1){$form.empty();}
		if(!isEmpty(view )){$("<input/>").attr({type:"hidden",id:"key",name:"key",value:view}).appendTo($form);}
		if(!isEmpty(mode )){$("<input/>").attr({type:"hidden",id:"kag",name:"kag",value:mode}).appendTo($form);}
		if(!isEmpty(time )){$("<input/>").attr({type:"hidden",id:"kps",name:"kps",value:time}).appendTo($form);}<%--
		if($form.length<=1){$form.attr({"action":"<%= request.getContextPath() %>/cmmn/CastViewer.do","method":"get"}).submit();}--%>
		if($form.length<=1){$form.attr({"action":"<%= request.getContextPath() %>/cmmn/CastViewer.do"+(!isEmpty(mode)?"#"+mode:""),"method":"get",target:"_blank"}).submit();}
	};
	var fnActActive     = function(view,mode){
		var   win=window.open("about:blank","acts","width=520,height=600,resizable=no,scrollbars=yes");
		var $form=$("#frmDetail");
		if($form.length<=0){$form=$("<form/>").attr({id:"frmDetail",name:"frmDetail",method:"post"});$(document.body).append($form);}
		if($form.length<=1){$form.empty();}
		if(!isEmpty(view )){$("<input/>").attr({type:"hidden",id:"viewNo",name:"viewNo",value:view}).appendTo($form);}
		if(!isEmpty(mode )){$("<input/>").attr({type:"hidden",id:"actMode",name:"actMode",value:mode}).appendTo($form);}
		if($form.length<=1){$form.attr({"action":"<%= request.getContextPath() %>/coun/activePopup.do","method":"post","target":"acts"}).submit();}
	};
	var fnImgPrint      = function(fileNM){
		var viewNo = $(".fotorama").fotorama().data("fotorama").activeFrame.img;
		var fileTP = "print";
		if( isEmpty(viewNo)){alert("적용할 내역이 존재하지 않습니다.\n\n확인하시기 바랍니다.");return false;}
		var $form=$("#fileDown");
		if($form.length<=0){$form=$("<form/>").attr({id:"fileDown",name:"fileDown",method:"post"});$(document.body).append($form);}
		if($form.length<=1){$form.empty();}
		if(!isEmpty(viewNo)){viewNo=viewNo.replace("/vimg/","/img/");}
		if(!isEmpty(viewNo)){$("<input/>").attr({type:"hidden",id:"viewNo",name:"viewNo",value:viewNo}).appendTo($form);}
		if(!isEmpty(fileNM)){$("<input/>").attr({type:"hidden",id:"condName",name:"condName",value:fileNM}).appendTo($form);}
		if(!isEmpty(fileTP)){$("<input/>").attr({type:"hidden",id:"condCate",name:"condCate",value:fileTP}).appendTo($form);}
		if($form.length<=1){$form.attr({"action":"<%= request.getContextPath() %>/cmmn/ImgsDown.do","method":"post"}).submit();}
	};
	var fnImgExpand     = function(fileNM,fileTP){
		var viewNo = $(".fotorama").fotorama().data("fotorama").activeFrame.img;
		if( isEmpty(viewNo)){alert("적용할 내역이 존재하지 않습니다.\n\n확인하시기 바랍니다.");return false;}
		var $form=$("#fileDown");
		if($form.length<=0){$form=$("<form/>").attr({id:"fileDown",name:"fileDown",method:"post"});$(document.body).append($form);}
		if($form.length<=1){$form.empty();}
		if(!isEmpty(viewNo)){viewNo=viewNo.replace("/vimg/","/img/");}
		if(!isEmpty(viewNo)){$("<input/>").attr({type:"hidden",id:"viewNo",name:"viewNo",value:viewNo}).appendTo($form);}
		if(!isEmpty(fileNM)){$("<input/>").attr({type:"hidden",id:"condName",name:"condName",value:fileNM}).appendTo($form);}
		if(!isEmpty(fileTP)){$("<input/>").attr({type:"hidden",id:"condCate",name:"condCate",value:fileTP}).appendTo($form);}
		if($form.length<=1){$form.attr({"action":"<%= request.getContextPath() %>/cmmn/ImgsDown.do","method":"post"}).submit();}
	};
	var fnImgDown       = function(fileTP){
		var fileID = $(".fotorama").fotorama().data("fotorama").activeFrame.img;
		if( isEmpty(fileID)){alert("적용할 내역이 존재하지 않습니다.\n\n확인하시기 바랍니다.");return false;}
		var $form=$("#fileDown");
		if($form.length<=0){$form=$("<form/>").attr({id:"fileDown",name:"fileDown",method:"post"});$(document.body).append($form);}
		if($form.length<=1){$form.empty();}
		if(!isEmpty(fileID)){fileID=fileID.replace("/vimg/","/img/");}
		if(!isEmpty(fileID)){$("<input/>").attr({type:"hidden",id:"fileAbs",name:"fileAbs",value:fileID}).appendTo($form);}
		if(!isEmpty(fileTP)){$("<input/>").attr({type:"hidden",id:"fileRel",name:"fileRel",value:fileTP}).appendTo($form);}
		if($form.length<=1){$form.attr({"action":"<%= request.getContextPath() %>/cmmn/LinkDown.do","method":"post"}).submit();}
	};
	var media_layer     = function(){
		var _lp=$("#layVideo");
		var _bg=_lp.prevAll().hasClass("bg");
		if(_bg){$(".layer").fadeIn();$(".layer_pop").hide();_lp.show();}else{_lp.fadeIn();$(".layer_pop").hide();_lp.show();}
		if(_lp.outerHeight()<$(document).height()){_lp.css("margin-top","-"+_lp.outerHeight()/2+"px");}else{_lp.css("top","0px");}
		if(_lp.outerWidth()<$(document).width()){_lp.css("margin-left","-"+_lp.outerWidth()/2+"px");}else{_lp.css("left","0px");}
		_lp.find("a.btn_c").click(function(e){e.preventDefault();$(".layer_media video").get(0).pause();if(_bg){$(".layer").fadeOut();}else{_lp.fadeOut();}});
		$(".layer .bg").click(function(e){e.preventDefault();$(".layer_media video").get(0).pause();$(".layer").fadeOut();});
	};
	var month_layer     = function(){
		var _lp=$("#layMonth");
		var _bg=_lp.prevAll().hasClass("bg");
		if(_bg){$(".layer").fadeIn();$(".layer_pop").hide();_lp.show();}else{_lp.fadeIn();$(".layer_pop").hide();_lp.show();}
		if(_lp.outerHeight()<$(document).height()){_lp.css("margin-top","-"+_lp.outerHeight()/2+"px");}else{_lp.css("top","0px");}
		if(_lp.outerWidth()<$(document).width()){_lp.css("margin-left","-"+_lp.outerWidth()/2+"px");}else{_lp.css("left","0px");}
		_lp.find("a.btn_c").click(function(e){e.preventDefault();if(_bg){$(".layer").fadeOut();}else{_lp.fadeOut();}});
		$(".layer .bg").click(function(e){e.preventDefault();$(".layer").fadeOut();});
	};