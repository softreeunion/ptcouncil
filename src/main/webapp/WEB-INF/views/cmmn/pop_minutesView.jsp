<%
////////////////////////////////////////////////////////////
// Program ID  : pop_minutesView
// Description : 전자회의록 보기
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
//  v1.1    HexaMedia new0man      2020.03.03  검색어 처리기능 개선
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "회의록";
	request.setAttribute("searchText", BaseUtility.null2Blank( request.getParameter("searchText") ));
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" > "+ subTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptg.jspf"/>
	<script>
	$(document).ready(function(){<c:if test="${'T' eq dataView.tempFlag}">
		alert(" 본 회의록은 회의내용의 신속한 정보제공을 위한 임시회의록으로서 완결본이 아니므로 열람에 참고하시기 바라며, 타인에게 이를 열람하게 하거나 전재·복사하게 하여서는 안 됨을 알려드립니다.");</c:if>
		$("input:text[name='sText']").keypress(function(e){if(e.which==13){doAppFinder();return false;}});
		$("#coun_find > a").click (function(){$("#coun_find > div").toggle();});
		$("#coun_info").mouseenter(function(){$("#coun_find > div").hide();$(this).find("div").show();});
		$("#coun_info").mouseleave(function(){$("#coun_find > div").hide();$(this).find("div").hide();});
		$("#coun_item").mouseenter(function(){$("#coun_find > div").hide();$(this).find("div").show();});
		$("#coun_item").mouseleave(function(){$("#coun_find > div").hide();$(this).find("div").hide();});<c:if test="${!empty confAG}">
		if($("a[name='${confAG}']").length>0){$("html,body").stop().animate({"scrollTop":$("a[name='${confAG}']:eq(0)").offset().top},600);};</c:if><%--
		$(window).scroll(function(){if($(document).scrollTop()>$(".council_top").offset().top){$(".council_top").addClass("menu-fixed");}else{$(".council_top").removeClass("menu-fixed");}});--%>
	});
	var showmem         = function(view,mode){fnActActive(view,mode);};
	var showapp         = function(view,mode){alert("본 기능은 제공하지 않습니다.");};
	var showdic         = function(view,mode){alert("용어 설명은 제공하지 않습니다.");};
	var doAppPrint      = function(){<c:choose><c:when test="${'T' eq dataView.tempFlag}">
		alert("본 회의록은 회의내용의 신속한 정보제공을 위한 임시회의록으로서 인쇄기능은 제공하지 않습니다.");</c:when><c:otherwise>
		var $container = $(".council_body").clone();
		var  cssTarget = "";
		$("style").each(function(index,item){cssTarget+=$("style").get(index).innerHTML;});
		var  popupDocu = window.open("","_blank","width=700,height=800");
		popupDocu.document.write("<!DOCTYPE html><html><head><style>"+cssTarget+"</style></head><body>"+$container[0].innerHTML+"</body></html>");
		popupDocu.document.close();
		popupDocu.focus();
		setTimeout(function(){popupDocu.print();popupDocu.close();},600);</c:otherwise></c:choose>
	};
	var doAppFinder     = function(){
		$(".council_body").removeHighlight().highlight($("#sText").val());
		if($(".highlight").length>0){$("html,body").stop().animate({scrollTop:$(".highlight").eq(0).offset().top},600);};
	};
	</script><c:if test="${!empty confStyle}">
	<style type='text/css'>${confStyle}</style></c:if>
</head>

<body onload='javascript:doAppFinder()'>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<div id='council_popup'><div class='council_wrap'>
	<div class='council_top'>
		<h2><%= webTitle +" " %><c:choose><c:when test="${'T' eq dataView.tempFlag}">임시회의록</c:when><c:otherwise>회의록</c:otherwise></c:choose></h2>
		<div class='coun_right_mn'><ul>
			<li id='coun_find'>
				<a href='javascript:void(0);'><img src='<%= webBase %>/images/coun/council_icon01.png' alt='단어찾기' title='단어찾기'/>단어찾기</a>
				<div class='btn_group center'><ul><input type='text' id='sText' name='sText' class='w60_p h25' maxlength='30' value='${searchText}'/> <a href='javascript:void(0);' class='s_btn_style04' onclick='doAppFinder()'>검색</a></ul></div>
			</li>
			<li id='coun_info'>
				<a href='javascript:void(0);'><img src='<%= webBase %>/images/coun/council_icon02.png' alt='안건보기' title='안건보기'/>안건보기</a><c:choose><c:when test="${ empty dataView.childInfo}">
				<div><ul><li>지정된 안건이 없습니다.</li></ul></div></c:when><c:otherwise>
				<div><ul><c:forEach var="chld" varStatus="status" items="${dataView.childInfo}"><c:if test="${'S' eq chld.sugType}">
					<li><a href='#${chld.sugPosi}'>${chld.sugTitle}</a></li></c:if></c:forEach>
				</ul></div></c:otherwise></c:choose>
			</li>
			<li id='coun_item'>
				<a href='javascript:void(0);'><img src='<%= webBase %>/images/coun/council_icon05.png' alt='부록보기' title='부록보기'/>부록보기</a><c:choose><c:when test="${ empty dataView.childItem}">
				<div><ul><li>지정된 부록이 없습니다.</li></ul></div></c:when><c:otherwise>
				<div><ul><c:forEach var="chld" varStatus="status" items="${dataView.childItem}">
					<li><a href='javascript:void(0);' onclick='fnAppDownload("${chld.appUUID}")'>${status.count}. ${chld.appTitle}</a></li></c:forEach>
				</ul></div></c:otherwise></c:choose>
			</li><c:if test="${'T' ne dataView.tempFlag}">
			<li><a href='javascript:void(0);' onclick='fnSrcDownload("${dataView.confUUID}")'><img src='<%= webBase %>/images/coun/council_icon03.png' alt='파일받기' title='파일받기'/>파일받기</a></li></c:if>
			<li><a href='javascript:void(0);' onclick='doAppPrint()'><img src='<%= webBase %>/images/coun/council_icon04.png' alt='본문인쇄' title='인쇄'/>본문인쇄</a></li>
		</ul></div>
	</div>
	<div id='top' class='council_body'>${confView}</div>
</div></div>
</form>
</body>
</html><% request.removeAttribute("searchText"); %>