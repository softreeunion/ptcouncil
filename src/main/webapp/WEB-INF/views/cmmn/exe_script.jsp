<%
////////////////////////////////////////////////////////////
// Program ID  : exe_script
// Description : 실행결과 및 메시지 출력
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" isELIgnored="false" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	response.setHeader("pragma", "no-cache"); response.setHeader("cache-control", "no-cache"); response.setHeader("expires", "0");
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle %></title>
	<script src='<%= webBase %>/jquerys/jquery-2.2.4.min.js'></script>
	<script>
	$(window).load(function(){
		function getCookie( name ) { 
			var nameOfCookie = name +"=";
			var x = 0;
			while( x <= document.cookie.length ) { 
				var y = (x + nameOfCookie.length);
				if( document.cookie.substring( x, y ) == nameOfCookie ) { 
					if( (endOfCookie = document.cookie.indexOf(";", y)) == -1 ) 
						endOfCookie = document.cookie.length;
					return unescape(document.cookie.substring(y, endOfCookie));
				}
				x = document.cookie.indexOf(" ", x) + 1;
				if( x == 0 )	break;
			}
			return "";
		};
		var strErrMessage   = "${message}";
		var strErrReturn    = "${error}";
		var strErrClose     = "${close}";
		var strOutScript    = "<c:out value='${script}' escapeXml='false'/>";
		var strActScript    = "${action}";
		var strReturnUrl    = "${returnUrl}";
		var strType         = "${Type}";
		var termDateYn      = "${termDateYn}";
		var fnActScript     = function( strActScript ) { 
			if( strActScript == "close" ) { 
				if( top.frames.length > 0 ) 
					top.window.open("about:blank","_self").close();
				else if( self.frames.length <= 0 ) 
					self.window.open("about:blank","_self").close();
				else 
					window.open("about:blank","_self").close();
			} else if( strActScript == "back"  ) { 
				history.back();
			} else { 
				top.location.replace(strActScript);
			}
		};
		if( strErrMessage.length > 0 ) {alert(strErrMessage);}
		if( strErrReturn.length  > 0 ) {alert(strErrReturn);history.back();}
		if( strErrClose.length   > 0 ) {alert(strErrClose);fnActScript( "close" );}
		if( strOutScript.length  > 0 ) {eval(strOutScript);}
		if( strActScript.length  > 0 ) {fnActScript( strActScript );}
		if( strReturnUrl.length  > 0 ) {top.location.replace(strReturnUrl);}
		if( termDateYn == "Y" ) { 
			location.href = "<%= webBase %>/index.do";
			if( getCookie("pwdInit") != "done" ) { 
				var wid = 405;
				var hei = 155;
				window.open("pwdInit.do","pwdInit","top="+ (screen.height / 2 - hei / 2) +"px,left="+ (screen.width  / 2 - wid / 2) +"px,width="+ wid +"px,height="+ hei +"px,scrollbars=no,resizable=yes"); 
			}
		}
		if( strType == "cmmnAuth" ) { 
			parent.location.href = strReturnUrl; 
		}
		if( strType == "cmmnAuthSucess" ) { 
			opener.parent.location.href = strReturnUrl;
		}
	});
	</script>
</head>

<body>
</body>
</html>