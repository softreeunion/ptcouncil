<%
////////////////////////////////////////////////////////////
// Program ID  : internalServerError
// Description : 오류메시지 표출(500 : 내부 서버 오류)
// Author      : HexaMedia new0man
// Write Date  : 2019.06.04
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v1.0    (주)헥사미디어 개발자  2014.00.00  최초 생성
//  v2.0    HexaMedia new0man      2019.06.04  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "ERROR";
	String msgTItle				= "알 수 없는 오류가 발생했습니다.";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +">"+ subTitle %></title>
	<link rel='shortcut icon' type='image/x-icon' href='<%= webBase %>/images/common/council.ico'>
	<link rel='stylesheet' media='all' href='<%= webBase %>/jquerys/jQuery-ui-1.11.4/jquery-ui.min.css'/>
	<script src='<%= webBase %>/jquerys/jquery-2.2.4.min.js'></script>
	<script src='<%= webBase %>/jquerys/jQuery-ui-1.11.4/jquery-ui.min.js'></script>
	<style>
	.table1{border:0;border-spacing:0px;padding:0px;}.table2{border:0;border-spacing:2px;padding:2px;}.table9{border:0;border-spacing:9px;padding:0px;}
	.w500{width:500px;}.w520{width:520px;}.w600{width:600px;}.w100p{width:100%;}
	.center{text-align:center;}.left{text-align:left;}.top{vertical-align:top;}
	.white{background:#fff;}
	</style>
	<script>
	jQuery.fn.center    = function(){this.css("position","absolute").css("top" ,Math.max(0,(($(window).height()-$(this).outerHeight())/2)+$(window).scrollTop()) +"px").css("left",Math.max(0,(($(window).width() -$(this).outerWidth() )/2)+$(window).scrollLeft())+"px");return this;};
	$(document).ready(function(){$(".w600.table1").center();});
	$(window).resize(function(){$(".w600.table1").center();});
	function fncGoAfterErrorPage(){history.back(-2);}
	</script>
</head>

<body>
<table class='w600 table1'>
	<tr><td class='center top'><table class='w600' style='background:url("<%= webBase %>/images/er_cmmn/blue_bg.jpg");'>
			<tr><td class='center'><table class='w100p table9'>
					<tr><td class='white'><table class='w100p table1'>
							<tr><td class='left'><img src='<%= webBase %>/images/er_cmmn/er_logo.jpg' style='width:379px;height:57px;' alt='er_logo'/></td></tr>
							<tr><td><br/><br/></td></tr>
							<tr><td class='center'>
									<table class='w520 table2'><tr>
										<td style='width:74px;'  class='center'><img src='<%= webBase %>/images/er_cmmn/danger.jpg' style='width:74px;height:74px;' alt='danger'/></td>
										<td style='width:399px;' class='left'><%= msgTItle %></td>
									</tr></table>
									<table class='w500 table2'></table>
							</td></tr>
							<tr><td><br/><br/></td></tr>
							<tr><td class='center'><a href='javascript:void(0);' onclick='fncGoAfterErrorPage()'><img src='<%= webBase %>/images/er_cmmn/go_history.jpg' style='width:90px;height:29px;' alt='go_history'/></a></td></tr>
					</table></td></tr>
			</table></td></tr>
	</table></td></tr>
</table>
</body>
</html>