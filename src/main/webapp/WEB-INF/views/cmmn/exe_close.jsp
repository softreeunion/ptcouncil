<%
////////////////////////////////////////////////////////////
// Program ID  : exe_close
// Description : 현재 사이트 종료
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" isELIgnored="false" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	response.setHeader("pragma", "no-cache"); response.setHeader("cache-control", "no-cache"); response.setHeader("expires", "0");
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle %></title>
	<script src='<%= webBase %>/jquerys/jquery-2.2.4.min.js'></script>
	<script>
	$(window).load(function(){
	if( top.frames.length > 0 ) 
		top.window.open("about:blank","_self").close();
	else if( self.frames.length <= 0 ) 
		self.window.open("about:blank","_self").close();
	else 
		window.open("about:blank","_self").close();
	});
	</script>
</head>

<body>
</body>
</html>