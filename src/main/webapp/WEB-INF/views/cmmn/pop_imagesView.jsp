<%
////////////////////////////////////////////////////////////
// Program ID  : pop_imagesView
// Description : 별도이미지 보기
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "포토뷰어";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" > "+ webTitle %></title>
	<style type='text/css'>
	@-ms-viewport{width:device-width;}
	@-o-viewport{width:device-width;}
	@viewport{width:device-width;}
	</style>
	<link rel='shortcut icon' type='image/x-icon' href='<%= request.getContextPath() %>/images/common/council.ico'>
	<link rel='stylesheet' media='all' href='<%= request.getContextPath() %>/jquerys/jQuery-ui-1.11.4/jquery-ui.min.css'/>
	<link rel='stylesheet' media='all' href='<%= request.getContextPath() %>/plugins/fotorama/fotorama.css'/>
	<script src='<%= request.getContextPath() %>/jquerys/jquery-2.2.4.min.js'></script>
	<script src='<%= request.getContextPath() %>/jquerys/jquery.form-4.2.2.js'></script>
	<script src='<%= request.getContextPath() %>/plugins/fotorama/fotorama.js'></script>
	<script>
	$(document).ready(function(){
		<c:if test="${'T' eq optsNo}">$(".fotorama").on("fotorama:load",function(e,fotorama,extra){window.print();});</c:if>
	});
	</script>
</head>
<c:choose><c:when test="${ empty viewNo}">
<body>
<div id='wrap'>
	<div id='container'>
		<div id='contents'>
			<div class='photo_view_title'><h2>${dataView.bbsTitle}</h2></div>
			<div class='fotorama' data-nav='thumbs' data-allowfullscreen='true' data-transition='crossfade' data-width='100%' data-height='100%'>
				<c:if test="${!empty chldFile}"><c:forEach var="rult" varStatus="status" items="${chldFile}">
				<a href='<%= imgDomain %>/img/${rult.fileUri}/${rult.fileName}'><img src='<%= imgDomain %>/mimg/${rult.fileUri}/${rult.fileName}'/></a>
				</c:forEach></c:if>
			</div>
		</div>
	</div>
</div>
</body>
</html>
</c:when><c:otherwise>
<body style='margin:0;overflow:hidden;background:#000;position:relative;'>
<div id='wrap'>
	<div class='fotorama' data-width='100%' data-height='100%' data-fit='cover' data-nav='none'><a href='${viewNo}'></a></div>
	<div style='position:absolute;bottom:0;right:15px;font-size:17px;font-weight:700;color:#fff;padding:5px 10px;z-index:1;'><p>${dataView.bbsTitle}</p></div>
</div>
</body>
</html>
</c:otherwise></c:choose>