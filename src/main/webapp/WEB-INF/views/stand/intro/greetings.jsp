<%
////////////////////////////////////////////////////////////
// Program ID  : greetings
// Description : 상임위 > 위원회소개 - 위원장 인사말
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= sndName.replaceAll("위원회","위원장") +" 인사말";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" "+ sndName %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scripts.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb01_01").addClass("on").removeClass("noChild");
	});
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/stand/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/stand/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="1"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 위원회소개 &gt; <%= subTitle %></div>
			<div class='section'>
<c:if test="${'stand' eq sndType or 'oper' eq sndType}">
				<div class='section'><div class='greetings'>
					<div class='img'><img src='<%= webBase %>/images/oper/img_greetings02.png' alt='의장 이미지'/></div>
					<div class='txt'>
						<h4>존경하는 50만 평택시민 여러분!<br/>평택시의회 의회운영위원회 홈페이지 방문을 진심으로 환영합니다.</h4>
						<p>
							의회운영위원회는 7명의 위원으로 구성되어 있으며 의사일정의 협의, 특별위원회의 구성, 상임위원회 간의 소관 조정, 기타 의회 운영에 관한 각종 안건 심의 등 평택시의회의 전반적인 운영 방향을 결정하는 동시에 의회 안팎의 다양한 의견들을 조율하는 핵심적 역할을 수행하고 있습니다.<br/><br/>
							우리 의회운영위원회는 진정한 지방자치를 실현하고 시민의 기대에 부응하는 평택시의회가 되도록 의정 활동을 펼쳐 평택시민의 다양한 과제와 기대를 충족시키고 시민에게 꿈과 희망을 주는 시민 중심의 의회를 만들기 위해 최선을 다할 것입니다.<br/><br/>
							그리고 의원들 상호 간의 신뢰를 바탕으로 다양한 전문성을 강화하고 이를 바탕으로 의정발전은 물론 시정에 기여 할 수 있도록 환경을 조성하며 항상 열려 있는 의회를 만들어 바람직한 의회 상을 정립하는 데 최선을 다하겠습니다.<br/><br/>
							시민 여러분의 많은 관심과 참여가 지방자치 발전의 밑거름이 됩니다. 많은 관심과 성원을 부탁드리며, 시민 여러분 모두의 가정에 건강과 행복이 늘 함께 하시길 기원 드립니다.<br/>
							감사합니다.
						</p>
						<div class='sign'>평택시의회 의회운영위원회 위원장<strong>이윤하</strong></div>
					</div>
				</div></div>
</c:if>
<c:if test="${'gvadm' eq sndType}">
				<div class='section'><div class='greetings'>
					<div class='img'><img src='<%= webBase %>/images/oper/img_greetings03.png' alt='의장 이미지'/></div>
					<div class='txt'>
						<h4>존경하는 50만 평택시민 여러분!<br/>평택시의회 홈페이지를 방문해 주신 시민 여러분께 진심으로 감사드립니다.</h4>
						<p>
							제8대 평택시의회 자치행정위원회 위원장 정일구입니다. 평택시의회 자치행정위원회 위원들은 50만 평택시민 한 분 한 분의 뜻이 골고루 시정에 반영될 수 있도록 창의적이고 혁신적인 자세로 일하고 있습니다.<br/><br/>
							시민 여러분의 목소리에 귀 기울이고, 필요한 곳에 전달될 수 있도록 더욱더 바쁘게 뛰겠습니다.<br/><br/>
							또한, 집행부에 대한 합리적 견제와 감시라는 의회 본연의 역할을 충실히 이행하며, 시민과 뜨거운 열정으로 소통하는 의회가 되도록 최선을 다하겠습니다. 시민의 행복과 평택의 발전을 위해 열과 성을 다해 일할 수 있도록 시민 여러분들의 많은 관심과 성원을 부탁드립니다. 감사합니다.
						 </p>
						<div class='sign'>평택시의회 자치행정위원회 위원장<strong>정일구</strong></div>
					</div>
				</div></div>
</c:if>
<c:if test="${'build' eq sndType}">
				<div class='section'><div class='greetings'>
					<div class='img'><img src='<%= webBase %>/images/oper/img_greetings04.png' alt='의장 이미지'/></div>
					<div class='txt'>
						<h4>존경하는 50만 평택시민 여러분!<br/>평택시의회 홈페이지를 방문해 주신 시민 여러분께 진심으로 감사드립니다.</h4>
						<p>
							제8대 평택시의회 산업건설위원회 위원장 김승겸입니다.<br/><br/>
							산업건설위원회는 항만경제, 환경 농정, 도시주택, 건설 교통, 상하수도 분야 등 51만 평택시민들의 실생활과 밀접하게 연관된 정책을 감시‧감독하는 위원회입니다.<br/><br/>
							언제나 현장에서 시민 여러분의 의견을 경청하고, 서로 간의 소통을 통해 누구나 살고 싶은 평택, 시민이 행복한 평택을 만들기 위해 열심히 노력하겠습니다.<br/><br/>
							앞으로도 시민 여러분들의 많은 관심과 성원을 부탁드립니다.<br/>
							감사합니다.
						</p>
						<div class='sign'>평택시의회 산업건설위원회 위원장<strong>김승겸</strong></div>
					</div>
				</div></div>
</c:if>
			</div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/stand/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>