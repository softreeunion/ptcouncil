<%
////////////////////////////////////////////////////////////
// Program ID  : direction
// Description : 상임위 > 위원회소개 - 위원회 운영방향
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= sndName +" 운영방향";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" "+ sndName %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scripts.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb01_03").addClass("on").removeClass("noChild");
	});
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/stand/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/stand/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="1"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 위원회소개 &gt; <%= subTitle %></div>
			<div class='section'>
				<div class='s_greetings'>
					<div class='txt'>
						<h4>세계 최고 수준의 입법ㆍ정책 생산과<br/>지방의회 발전 선도할 것</h4>
						<p>
							의회운영위원회는 110명의 시민대표로 구성된 평택시의회의 전반적인 운영방향을 결정하는 동시에, 의회 안팎의 다양한 의견들을 조율하는 핵심적 역할을 수행하고 있습니다. 제8대 전반기 운영위원회는 더욱 유능한 의회, 민주적 운영시스템을 갖춘 세계 최고 수준의 선진의회를 만들 것입니다.<br/><br/>
							<strong>첫째, 세계 최고의 입법ㆍ정책생산능력을 보유한 유능한 평택시의회를 만들겠습니다.</strong><br/>
							유능한 의회만이 평택시민의 삶을 근본적으로 변화ㆍ발전시킬 수 있습니다. 평택시의회는 고단하고 절박한 서민들의 삶을 행복한 삶으로 바꿀 수 있도록 입법ㆍ정책 역량을 키우겠습니다. 그리고 시정 현안에 대한 합리적인 비판과 함께 실천적인 정책대안을 제시하겠습니다. 또한 평택시의원의 의정활동 역량과 잠재력을 마음껏 발휘할 수 있도록 현재의 의정활동 지원시스템을 대폭 개선하겠습니다. 평택시의회는 민주적 의사결정 과정과 운영시스템을 갖추고 유능한 선진의정을 실현함으로써 세계 대도시 의회와 어깨를 나란히 할 것입니다.<br/><br/>
							<strong>둘째, 전국 지방의회의 맏형으로서 풀뿌리 민주주의의 근간인 지방자치 발전을 선도하고 지방분권을 가속화하는 데 앞장서겠습니다.</strong><br/>
							올해 지방분권의 의지를 담은 개헌안이 발표됐지만, 정치권의 이해타산에 밀려 좌초되고 말았습니다. 그러나 ‘자치’와 ‘분권’은 국민의 명령이고, 명약관화(明若觀火)한 시대정신입니다. 그리고 대한민국을 다시 일으킬 새로운 성장 동력입니다. 지난 27년간 우리나라 지방자치를 선도해 온 평택시의회는 전국 지방의원의 총의를 모아 지방분권을 위한 추동력 확보에 힘쓰겠습니다. 지방분권을 위한 개헌과 지방의회 발전을 위한 법률 제ㆍ개정이 조속히 진행될 수 있도록 국회와 정부를 지속적으로 설득해 나갈 것입니다.<br/><br/>
							<strong>셋째, 평택시의회는 집행부를 견지하는 강한 의회로 거듭날 것입니다.</strong><br/>
							지방분권의 확대 과정에서 단체장의 권한만 강화되면 강시장-약의회란 기형적 권력관계만 더욱 고착화되는 문제가 발생할 수 있습니다. 이럴 경우 제왕적 단체장의 지위가 더욱 공고해지고, 지방정부를 구성하는 지방의회와 집행기관의 동반 발전은 기대하기 어렵게 됩니다. 진정한 자치분권의 실현은 기울어진 양 기관의 관계를 바로 잡고, 지방의회에 강한 권한이 주어지는 것입니다. 이를 위해 지방의회가 책임성을 갖고 단체장의 전횡과 독주를 효과적으로 견제ㆍ감시할 수 있도록 헌법과 법률에 제도적 장치를 마련하겠습니다. 여기에는 정책보좌관제 도입, 의회사무처 인사권 독립, 의회 예산편성의 자율성 확보, 인사청문회 법제화 등 주요 현안과제가 포함될 것입니다. 구체적인 실행계획(action plan)과 추진 동력은 지방분권TF를 통해 확보할 것입니다.<br/></br>
							<strong>넷째, 시민의 목소리에 귀 기울이고, 시민과의 약속을 지키는 신뢰받는 의회를 만들겠습니다.</strong> <br/>
							시민대표인 시의원은 주민 한 분 한 분의 다양한 목소리를 담고 있습니다. 시민의 목소리가 자유롭게 토론되고, 그 결과가 시정에 제대로 반영될 수 있도록 민의의 ‘장’을 의회에 마련하겠습니다. 그리고 의원들의 공약실천을 돕는 의회를 만들겠습니다. 공약실천은 신뢰를 바탕으로 한 시민과의 약속이며, 민주주의의 책임정치가 제대로 실천되기 위한 필수적인 요건입니다. 의회운영위원회는 의원들의 공약 분석과 이행 가능성, 재원조달 방안, 진척상황 등을 종합적으로 관리하는 전담조직을 신설해 시민들과의 약속 이행을 적극 도울 것입니다.<br/><br/>
							유능한 지방의회는 대한민국을 새롭게 만들 수 있습니다. 외국 선진 도시의회 중에는 지방의회가 중심이 돼 지방행정과 주민복리를 이끌어가는 경우가 많습니다. 우리도 그러한 모범과 전형을 만들 수 있습니다. 의회운영위원회는 평택시민의 행복과 안전을 책임진다는 각오 속에 ‘유능하고 강한 의회’를 만들기 위해 한발 더 뛰겠습니다. 평택시민의 가치를 온전하게 담고 시민과 함께하는 의회로 거듭날 수 있도록 만전을 기울일 것입니다. 시민 여러분의 애정 어린 관심과 성원을 부탁드립니다.<br/><br/>
							<span class='span_txt'>※ 의회운영위원회는 평택시의회 상임위원회를 대표하는 13명 이내의 위원으로 구성돼 회기 등 의회 관련 규정의 제ㆍ개정, 특별위원회 구성에 관한 사항, 의회사무처ㆍ시장비서실ㆍ정무부시장실 등 3개 소관부서의 업무를 관장하며, 의원들의 의정활동을 뒷받침하는 중 추적 역할을 수행하고 있다.</span>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/stand/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>