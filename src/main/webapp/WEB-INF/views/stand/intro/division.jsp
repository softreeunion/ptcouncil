<%
////////////////////////////////////////////////////////////
// Program ID  : division
// Description : 상임위 > 위원회소개 - 소관부서
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= sndName +" 소관부서";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" "+ sndName %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scripts.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb01_02").addClass("on").removeClass("noChild");
	});
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/stand/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/stand/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="1"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 위원회소개 &gt; <%= subTitle %></div>
			<div class='section'>
<c:if test="${'stand' eq sndType or 'oper' eq sndType}">
				<h4 class='h4_tit'>소관부서</h4>
				<div class='grey_box_bg'><ul>
					<li>의회운영에 관한 사항</li>
					<li>의회사무국 소관에 관한 사항</li>
					<li>회의규칙 및 의회운영과 관련된 각종 규칙에 관한 사항</li>
				</ul></div>
</c:if>
<c:if test="${'gvadm' eq sndType}">
				<h4 class='h4_tit'>소관부서</h4>
				<div class='grey_box_bg'><ul>
					<li>소통홍보관, 감사관, 안전총괄관, 기획조정실, 총무국, 사회복지국(복지재단, 청소년재단), 평택보건소, 송탄보건소, 한미협력사업단(국제교류재단), 문화예술회관, 영상정보운영사업소, 여성회관, 도서관, 출장소 및 읍·면·동 소관에 속하는 사항</li>
				</ul></div>
</c:if>
<c:if test="${'build' eq sndType}">
				<h4 class='h4_tit'>소관부서</h4>
				<div class='grey_box_bg'><ul>
					<li>항만경제전략국, 환경농정국, 도시주택국, 건설교통국, 농업기술센터, 상하수도사업소, 차량등록사업소, 평택도시공사 소관에 속하는 사항</li>
				</ul></div>
</c:if>
			</div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/stand/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>