<%
////////////////////////////////////////////////////////////
// Program ID  : lefter
// Description : 상임위 / LEFTER
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String tbParam			= BaseUtility.null2Blank( request.getParameter("tb") );
	StringBuffer dispMenu	= new StringBuffer();
	int[] snbIdx			= {0,0};

	if( "1".equals(tbParam) && !(standAList == null || standAList.length <= 0) ) { 
		dispMenu.append(baseTab[2] +"<div id='snb'>\n");
		dispMenu.append(baseTab[3] +"<h2 class='h2_tit'>위원회소개</h2>\n");
		dispMenu.append(baseTab[3] +"<ul>\n");
		for( int m = 0; m < standAList.length; m++ ) {
			if( "1".equals(standAList[m][2]) ) snbIdx[0]++;
			if( "T".equals(standAList[m][0]) && "1".equals(standAList[m][2]) ) { 
				dispMenu.append(baseTab[4] +"<li><a href='"+ ("L".equals(standAList[m][1])?standAList[m][4]+"' target='_blank":webBase+standAList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2));
				dispMenu.append("'"+ ("N".equals(standAList[m][1])?" class='noChild'":"") +">"+ standAList[m][3] +"</a>"+ ("N".equals(standAList[m][1])?"</li>":"") +"\n");
				if( "C".equals(standAList[m][1]) ) dispMenu.append(baseTab[5] +"<ul>\n");
			}
			if( "T".equals(standAList[m][0]) && "2".equals(standAList[m][2]) ) { 
				snbIdx[1]++;
				dispMenu.append(baseTab[6] +"<li><a href='"+ ("L".equals(standAList[m][1])?standAList[m][4]+"' target='_blank":webBase+standAList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2) +"_"+ BaseUtility.zeroFill(snbIdx[1],2));
				dispMenu.append("'>"+ standAList[m][3] +"</a></li>\n");
			}
			if( "T".equals(standAList[m][0]) && "E".equals(standAList[m][1]) ) { 
				snbIdx[1] = 0;
				dispMenu.append(baseTab[5] +"</ul>\n").append(baseTab[4] +"</li>\n");
			}
		}
		dispMenu.append(baseTab[3] +"</ul>\n").append(baseTab[2] +"</div>\n");
	}
	if( "2".equals(tbParam) && !(standBList == null || standBList.length <= 0) ) { 
		dispMenu.append(baseTab[2] +"<div id='snb'>\n");
		dispMenu.append(baseTab[3] +"<h2 class='h2_tit'>위원소개</h2>\n");
		dispMenu.append(baseTab[3] +"<ul>\n");
		for( int m = 0; m < standBList.length; m++ ) {
			if( "1".equals(standBList[m][2]) ) snbIdx[0]++;
			if( "T".equals(standBList[m][0]) && "1".equals(standBList[m][2]) ) { 
				dispMenu.append(baseTab[4] +"<li><a href='"+ ("L".equals(standBList[m][1])?standBList[m][4]+"' target='_blank":webBase+standBList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2));
				dispMenu.append("'"+ ("N".equals(standBList[m][1])?" class='noChild'":"") +">"+ standBList[m][3] +"</a>"+ ("N".equals(standBList[m][1])?"</li>":"") +"\n");
				if( "C".equals(standBList[m][1]) ) dispMenu.append(baseTab[5] +"<ul>\n");
			}
			if( "T".equals(standBList[m][0]) && "2".equals(standBList[m][2]) ) { 
				snbIdx[1]++;
				dispMenu.append(baseTab[6] +"<li><a href='"+ ("L".equals(standBList[m][1])?standBList[m][4]+"' target='_blank":webBase+standBList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2) +"_"+ BaseUtility.zeroFill(snbIdx[1],2));
				dispMenu.append("'>"+ standBList[m][3] +"</a></li>\n");
			}
			if( "T".equals(standBList[m][0]) && "E".equals(standBList[m][1]) ) { 
				snbIdx[1] = 0;
				dispMenu.append(baseTab[5] +"</ul>\n").append(baseTab[4] +"</li>\n");
			}
		}
		dispMenu.append(baseTab[3] +"</ul>\n").append(baseTab[2] +"</div>\n");
	}
	if( "3".equals(tbParam) && !(standCList == null || standCList.length <= 0) ) { 
		dispMenu.append(baseTab[2] +"<div id='snb'>\n");
		dispMenu.append(baseTab[3] +"<h2 class='h2_tit'>의사일정</h2>\n");
		dispMenu.append(baseTab[3] +"<ul>\n");
		for( int m = 0; m < standCList.length; m++ ) {
			if( "1".equals(standCList[m][2]) ) snbIdx[0]++;
			if( "T".equals(standCList[m][0]) && "1".equals(standCList[m][2]) ) { 
				dispMenu.append(baseTab[4] +"<li><a href='"+ ("L".equals(standCList[m][1])?standCList[m][4]+"' target='_blank":webBase+standCList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2));
				dispMenu.append("'"+ ("N".equals(standCList[m][1])?" class='noChild'":"") +">"+ standCList[m][3] +"</a>"+ ("N".equals(standCList[m][1])?"</li>":"") +"\n");
				if( "C".equals(standCList[m][1]) ) dispMenu.append(baseTab[5] +"<ul>\n");
			}
			if( "T".equals(standCList[m][0]) && "2".equals(standCList[m][2]) ) { 
				snbIdx[1]++;
				dispMenu.append(baseTab[6] +"<li><a href='"+ ("L".equals(standCList[m][1])?standCList[m][4]+"' target='_blank":webBase+standCList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2) +"_"+ BaseUtility.zeroFill(snbIdx[1],2));
				dispMenu.append("'>"+ standCList[m][3] +"</a></li>\n");
			}
			if( "T".equals(standCList[m][0]) && "E".equals(standCList[m][1]) ) { 
				snbIdx[1] = 0;
				dispMenu.append(baseTab[5] +"</ul>\n").append(baseTab[4] +"</li>\n");
			}
		}
		dispMenu.append(baseTab[3] +"</ul>\n").append(baseTab[2] +"</div>\n");
	}
	if( "4".equals(tbParam) && !(standDList == null || standDList.length <= 0) ) { 
		dispMenu.append(baseTab[2] +"<div id='snb'>\n");
		dispMenu.append(baseTab[3] +"<h2 class='h2_tit'>위원회활동</h2>\n");
		dispMenu.append(baseTab[3] +"<ul>\n");
		for( int m = 0; m < standDList.length; m++ ) {
			if( "1".equals(standDList[m][2]) ) snbIdx[0]++;
			if( "T".equals(standDList[m][0]) && "1".equals(standDList[m][2]) ) { 
				dispMenu.append(baseTab[4] +"<li><a href='"+ ("L".equals(standDList[m][1])?standDList[m][4]+"' target='_blank":webBase+standDList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2));
				dispMenu.append("'"+ ("N".equals(standDList[m][1])?" class='noChild'":"") +">"+ standDList[m][3] +"</a>"+ ("N".equals(standDList[m][1])?"</li>":"") +"\n");
				if( "C".equals(standDList[m][1]) ) dispMenu.append(baseTab[5] +"<ul>\n");
			}
			if( "T".equals(standDList[m][0]) && "2".equals(standDList[m][2]) ) { 
				snbIdx[1]++;
				dispMenu.append(baseTab[6] +"<li><a href='"+ ("L".equals(standDList[m][1])?standDList[m][4]+"' target='_blank":webBase+standDList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2) +"_"+ BaseUtility.zeroFill(snbIdx[1],2));
				dispMenu.append("'>"+ standDList[m][3] +"</a></li>\n");
			}
			if( "T".equals(standDList[m][0]) && "E".equals(standDList[m][1]) ) { 
				snbIdx[1] = 0;
				dispMenu.append(baseTab[5] +"</ul>\n").append(baseTab[4] +"</li>\n");
			}
		}
		dispMenu.append(baseTab[3] +"</ul>\n").append(baseTab[2] +"</div>\n");
	}
	if( "5".equals(tbParam) && !(standEList == null || standEList.length <= 0) ) { 
		dispMenu.append(baseTab[2] +"<div id='snb'>\n");
		dispMenu.append(baseTab[3] +"<h2 class='h2_tit'>회의록</h2>\n");
		dispMenu.append(baseTab[3] +"<ul>\n");
		for( int m = 0; m < standEList.length; m++ ) {
			if( "1".equals(standEList[m][2]) ) snbIdx[0]++;
			if( "T".equals(standEList[m][0]) && "1".equals(standEList[m][2]) ) { 
				dispMenu.append(baseTab[4] +"<li><a href='"+ ("L".equals(standEList[m][1])?standEList[m][4]+"' target='_blank":webBase+standEList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2));
				dispMenu.append("'"+ ("N".equals(standEList[m][1])?" class='noChild'":"") +">"+ standEList[m][3] +"</a>"+ ("N".equals(standEList[m][1])?"</li>":"") +"\n");
				if( "C".equals(standEList[m][1]) ) dispMenu.append(baseTab[5] +"<ul>\n");
			}
			if( "T".equals(standEList[m][0]) && "2".equals(standEList[m][2]) ) { 
				snbIdx[1]++;
				dispMenu.append(baseTab[6] +"<li><a href='"+ ("L".equals(standEList[m][1])?standEList[m][4]+"' target='_blank":webBase+standEList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2) +"_"+ BaseUtility.zeroFill(snbIdx[1],2));
				dispMenu.append("'>"+ standEList[m][3] +"</a></li>\n");
			}
			if( "T".equals(standEList[m][0]) && "E".equals(standEList[m][1]) ) { 
				snbIdx[1] = 0;
				dispMenu.append(baseTab[5] +"</ul>\n").append(baseTab[4] +"</li>\n");
			}
		}
		dispMenu.append(baseTab[3] +"</ul>\n").append(baseTab[2] +"</div>\n");
	}
	if( "6".equals(tbParam) && !(standFList == null || standFList.length <= 0) ) { 
		dispMenu.append(baseTab[2] +"<div id='snb'>\n");
		dispMenu.append(baseTab[3] +"<h2 class='h2_tit'>의안정보</h2>\n");
		dispMenu.append(baseTab[3] +"<ul>\n");
		for( int m = 0; m < standFList.length; m++ ) {
			if( "1".equals(standFList[m][2]) ) snbIdx[0]++;
			if( "T".equals(standFList[m][0]) && "1".equals(standFList[m][2]) ) { 
				dispMenu.append(baseTab[4] +"<li><a href='"+ ("L".equals(standFList[m][1])?standFList[m][4]+"' target='_blank":webBase+standFList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2));
				dispMenu.append("'"+ ("N".equals(standFList[m][1])?" class='noChild'":"") +">"+ standFList[m][3] +"</a>"+ ("N".equals(standFList[m][1])?"</li>":"") +"\n");
				if( "C".equals(standFList[m][1]) ) dispMenu.append(baseTab[5] +"<ul>\n");
			}
			if( "T".equals(standFList[m][0]) && "2".equals(standFList[m][2]) ) { 
				snbIdx[1]++;
				dispMenu.append(baseTab[6] +"<li><a href='"+ ("L".equals(standFList[m][1])?standFList[m][4]+"' target='_blank":webBase+standFList[m][4]));
				dispMenu.append("' id='snb"+ BaseUtility.zeroFill(tbParam,2) +"_"+ BaseUtility.zeroFill(snbIdx[0],2) +"_"+ BaseUtility.zeroFill(snbIdx[1],2));
				dispMenu.append("'>"+ standFList[m][3] +"</a></li>\n");
			}
			if( "T".equals(standFList[m][0]) && "E".equals(standFList[m][1]) ) { 
				snbIdx[1] = 0;
				dispMenu.append(baseTab[5] +"</ul>\n").append(baseTab[4] +"</li>\n");
			}
		}
		dispMenu.append(baseTab[3] +"</ul>\n").append(baseTab[2] +"</div>\n");
	}

	out.clearBuffer();
	out.print(dispMenu.toString()); %>