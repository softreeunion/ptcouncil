<%
////////////////////////////////////////////////////////////
// Program ID  : header
// Description : 상임위 / HEADER
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	int gnbSub				= 0;
	StringBuffer dispMenu	= new StringBuffer();
	StringBuffer dispFull	= new StringBuffer();
	StringBuffer dispMobi	= new StringBuffer();
	if( !(standAList == null || standAList.length <= 0) ) { 
		gnbSub++;
		dispMenu.append("<li>");
		dispMenu.append("\n"+ baseTab[6] +"<a href='"+ webBase + standAList[ 0][4] +"'>위원회소개</a>");
		dispMenu.append("\n"+ baseTab[6] +"<div id='gnb_sub"+ BaseUtility.zeroFill( gnbSub, 2 ) +"' class='gnb_sub'><ul>");
		dispFull.append("<li>");
		dispFull.append("\n"+ baseTab[6] +"<a href='"+ webBase + standAList[ 0][4] +"'>위원회소개</a>");
		dispFull.append("\n"+ baseTab[6] +"<div class='fullmenu_list_sub'><ul>");
		dispMobi.append("<li>");
		dispMobi.append("\n"+ baseTab[5] +"<a href='"+ webBase + standAList[ 0][4] +"'>위원회소개</a>");
		dispMobi.append("\n"+ baseTab[5] +"<div class='mobile_menu_list_sub'><ul>");
		for( int m = 0; m < standAList.length; m++ ) { 
			if( "T".equals(standAList[m][0]) && "1".equals(standAList[m][2]) ) { 
				dispMenu.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(standAList[m][1])?standAList[m][4]+"' target='_blank":webBase+standAList[m][4]) +"'>"+ standAList[m][3] +"</a></li>");
				dispFull.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(standAList[m][1])?standAList[m][4]+"' target='_blank":webBase+standAList[m][4]) +"'>"+ standAList[m][3] +"</a>").append("N".equals(standAList[m][1])?"</li>":"");
				if( "C".equals(standAList[m][1]) ) dispFull.append("\n"+ baseTab[8] +"<ul>");
				dispMobi.append("\n"+ baseTab[6] +"<li><a href='"+ ("L".equals(standAList[m][1])?standAList[m][4]+"' target='_blank":webBase+standAList[m][4]) +"'>"+ standAList[m][3] +"</a>").append("N".equals(standAList[m][1])?"</li>":"");
				if( "C".equals(standAList[m][1]) ) dispMobi.append("\n"+ baseTab[7] +"<ul>");
			}
			if( "T".equals(standAList[m][0]) && "2".equals(standAList[m][2]) ) { 
				dispFull.append("\n"+ baseTab[9] +"<li><a href='"+ ("L".equals(standAList[m][1])?standAList[m][4]+"' target='_blank":webBase+standAList[m][4]) +"'>"+ standAList[m][3] +"</a></li>");
				dispMobi.append("\n"+ baseTab[8] +"<li><a href='"+ ("L".equals(standAList[m][1])?standAList[m][4]+"' target='_blank":webBase+standAList[m][4]) +"'>"+ standAList[m][3] +"</a></li>");
			}
//			if( m != standAList.length-1 && "2".equals(standAList[m][2]) ) { 
//				if( "1".equals(standAList[m+1][2]) ) dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
//				if( "1".equals(standAList[m+1][2]) ) dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
//			}
			if( "T".equals(standAList[m][0]) && "E".equals(standAList[m][1]) ) { 
				dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
				dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
			}
		}
		dispMenu.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispFull.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispMobi.append("\n"+ baseTab[5] +"</ul></div>").append("\n"+ baseTab[4] +"</li>");
	}
	if( !(standBList == null || standBList.length <= 0) ) { 
		gnbSub++;
		dispMenu.append("<li>");
		dispMenu.append("\n"+ baseTab[6] +"<a href='"+ webBase + standBList[ 0][4] +"'>위원소개1</a>");
		dispMenu.append("\n"+ baseTab[6] +"<div id='gnb_sub"+ BaseUtility.zeroFill( gnbSub, 2 ) +"' class='gnb_sub'><ul>");
		dispFull.append("<li>");
		dispFull.append("\n"+ baseTab[6] +"<a href='"+ webBase + standBList[ 0][4] +"'>위원소개1</a>");
		dispFull.append("\n"+ baseTab[6] +"<div class='fullmenu_list_sub'><ul>");
		dispMobi.append("<li>");
		dispMobi.append("\n"+ baseTab[5] +"<a href='"+ webBase + standBList[ 0][4] +"'>위원소개1</a>");
		dispMobi.append("\n"+ baseTab[5] +"<div class='mobile_menu_list_sub'><ul>");
		for( int m = 0; m < standBList.length; m++ ) { 
			if( "T".equals(standBList[m][0]) && "1".equals(standBList[m][2]) ) { 
				dispMenu.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(standBList[m][1])?standBList[m][4]+"' target='_blank":webBase+standBList[m][4]) +"'>"+ standBList[m][3] +"</a></li>");
				dispFull.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(standBList[m][1])?standBList[m][4]+"' target='_blank":webBase+standBList[m][4]) +"'>"+ standBList[m][3] +"</a>").append("N".equals(standBList[m][1])?"</li>":"");
				if( "C".equals(standBList[m][1]) ) dispFull.append("\n"+ baseTab[8] +"<ul>");
				dispMobi.append("\n"+ baseTab[6] +"<li><a href='"+ ("L".equals(standBList[m][1])?standBList[m][4]+"' target='_blank":webBase+standBList[m][4]) +"'>"+ standBList[m][3] +"</a>").append("N".equals(standBList[m][1])?"</li>":"");
				if( "C".equals(standBList[m][1]) ) dispMobi.append("\n"+ baseTab[7] +"<ul>");
			}
			if( "T".equals(standBList[m][0]) && "2".equals(standBList[m][2]) ) { 
				dispFull.append("\n"+ baseTab[9] +"<li><a href='"+ ("L".equals(standBList[m][1])?standBList[m][4]+"' target='_blank":webBase+standBList[m][4]) +"'>"+ standBList[m][3] +"</a></li>");
				dispMobi.append("\n"+ baseTab[8] +"<li><a href='"+ ("L".equals(standBList[m][1])?standBList[m][4]+"' target='_blank":webBase+standBList[m][4]) +"'>"+ standBList[m][3] +"</a></li>");
			}
//			if( m != standBList.length-1 && "2".equals(standBList[m][2]) ) { 
//				if( "1".equals(standBList[m+1][2]) ) dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
//				if( "1".equals(standBList[m+1][2]) ) dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
//			}
			if( "T".equals(standBList[m][0]) && "E".equals(standBList[m][1]) ) { 
				dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
				dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
			}
		}
		dispMenu.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispFull.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispMobi.append("\n"+ baseTab[5] +"</ul></div>").append("\n"+ baseTab[4] +"</li>");
	}
	if( !(standCList == null || standCList.length <= 0) ) { 
		gnbSub++;
		dispMenu.append("<li>");
		dispMenu.append("\n"+ baseTab[6] +"<a href='"+ webBase + standCList[ 0][4] +"'>의사일정</a>");
		dispMenu.append("\n"+ baseTab[6] +"<div id='gnb_sub"+ BaseUtility.zeroFill( gnbSub, 2 ) +"' class='gnb_sub'><ul>");
		dispFull.append("<li>");
		dispFull.append("\n"+ baseTab[6] +"<a href='"+ webBase + standCList[ 0][4] +"'>의사일정</a>");
		dispFull.append("\n"+ baseTab[6] +"<div class='fullmenu_list_sub'><ul>");
		dispMobi.append("<li>");
		dispMobi.append("\n"+ baseTab[5] +"<a href='"+ webBase + standCList[ 0][4] +"'>의사일정</a>");
		dispMobi.append("\n"+ baseTab[5] +"<div class='mobile_menu_list_sub'><ul>");
		for( int m = 0; m < standCList.length; m++ ) { 
			if( "T".equals(standCList[m][0]) && "1".equals(standCList[m][2]) ) { 
				dispMenu.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(standCList[m][1])?standCList[m][4]+"' target='_blank":webBase+standCList[m][4]) +"'>"+ standCList[m][3] +"</a></li>");
				dispFull.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(standCList[m][1])?standCList[m][4]+"' target='_blank":webBase+standCList[m][4]) +"'>"+ standCList[m][3] +"</a>").append("N".equals(standCList[m][1])?"</li>":"");
				if( "C".equals(standCList[m][1]) ) dispFull.append("\n"+ baseTab[8] +"<ul>");
				dispMobi.append("\n"+ baseTab[6] +"<li><a href='"+ ("L".equals(standCList[m][1])?standCList[m][4]+"' target='_blank":webBase+standCList[m][4]) +"'>"+ standCList[m][3] +"</a>").append("N".equals(standCList[m][1])?"</li>":"");
				if( "C".equals(standCList[m][1]) ) dispMobi.append("\n"+ baseTab[7] +"<ul>");
			}
			if( "T".equals(standCList[m][0]) && "2".equals(standCList[m][2]) ) { 
				dispFull.append("\n"+ baseTab[9] +"<li><a href='"+ ("L".equals(standCList[m][1])?standCList[m][4]+"' target='_blank":webBase+standCList[m][4]) +"'>"+ standCList[m][3] +"</a></li>");
				dispMobi.append("\n"+ baseTab[8] +"<li><a href='"+ ("L".equals(standCList[m][1])?standCList[m][4]+"' target='_blank":webBase+standCList[m][4]) +"'>"+ standCList[m][3] +"</a></li>");
			}
//			if( m != standCList.length-1 && "2".equals(standCList[m][2]) ) { 
//				if( "1".equals(standCList[m+1][2]) ) dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
//				if( "1".equals(standCList[m+1][2]) ) dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
//			}
			if( "T".equals(standCList[m][0]) && "E".equals(standCList[m][1]) ) { 
				dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
				dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
			}
		}
		dispMenu.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispFull.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispMobi.append("\n"+ baseTab[5] +"</ul></div>").append("\n"+ baseTab[4] +"</li>");
	}
	if( !(standDList == null || standDList.length <= 0) ) { 
		gnbSub++;
		dispMenu.append("<li>");
		dispMenu.append("\n"+ baseTab[6] +"<a href='"+ webBase + standDList[ 0][4] +"'>위원회활동</a>");
		dispMenu.append("\n"+ baseTab[6] +"<div id='gnb_sub"+ BaseUtility.zeroFill( gnbSub, 2 ) +"' class='gnb_sub'><ul>");
		dispFull.append("<li>");
		dispFull.append("\n"+ baseTab[6] +"<a href='"+ webBase + standDList[ 0][4] +"'>위원회활동</a>");
		dispFull.append("\n"+ baseTab[6] +"<div class='fullmenu_list_sub'><ul>");
		dispMobi.append("<li>");
		dispMobi.append("\n"+ baseTab[5] +"<a href='"+ webBase + standDList[ 0][4] +"'>위원회활동</a>");
		dispMobi.append("\n"+ baseTab[5] +"<div class='mobile_menu_list_sub'><ul>");
		for( int m = 0; m < standDList.length; m++ ) { 
			if( "T".equals(standDList[m][0]) && "1".equals(standDList[m][2]) ) { 
				dispMenu.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(standDList[m][1])?standDList[m][4]+"' target='_blank":webBase+standDList[m][4]) +"'>"+ standDList[m][3] +"</a></li>");
				dispFull.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(standDList[m][1])?standDList[m][4]+"' target='_blank":webBase+standDList[m][4]) +"'>"+ standDList[m][3] +"</a>").append("N".equals(standDList[m][1])?"</li>":"");
				if( "C".equals(standDList[m][1]) ) dispFull.append("\n"+ baseTab[8] +"<ul>");
				dispMobi.append("\n"+ baseTab[6] +"<li><a href='"+ ("L".equals(standDList[m][1])?standDList[m][4]+"' target='_blank":webBase+standDList[m][4]) +"'>"+ standDList[m][3] +"</a>").append("N".equals(standDList[m][1])?"</li>":"");
				if( "C".equals(standDList[m][1]) ) dispMobi.append("\n"+ baseTab[7] +"<ul>");
			}
			if( "T".equals(standDList[m][0]) && "2".equals(standDList[m][2]) ) { 
				dispFull.append("\n"+ baseTab[9] +"<li><a href='"+ ("L".equals(standDList[m][1])?standDList[m][4]+"' target='_blank":webBase+standDList[m][4]) +"'>"+ standDList[m][3] +"</a></li>");
				dispMobi.append("\n"+ baseTab[8] +"<li><a href='"+ ("L".equals(standDList[m][1])?standDList[m][4]+"' target='_blank":webBase+standDList[m][4]) +"'>"+ standDList[m][3] +"</a></li>");
			}
//			if( m != standDList.length-1 && "2".equals(standDList[m][2]) ) { 
//				if( "1".equals(standDList[m+1][2]) ) dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
//				if( "1".equals(standDList[m+1][2]) ) dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
//			}
			if( "T".equals(standDList[m][0]) && "E".equals(standDList[m][1]) ) { 
				dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
				dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
			}
		}
		dispMenu.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispFull.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispMobi.append("\n"+ baseTab[5] +"</ul></div>").append("\n"+ baseTab[4] +"</li>");
	}
	if( !(standEList == null || standEList.length <= 0) ) { 
		gnbSub++;
		dispMenu.append("<li>");
		dispMenu.append("\n"+ baseTab[6] +"<a href='"+ webBase + standEList[ 0][4] +"'>회의록</a>");
		dispMenu.append("\n"+ baseTab[6] +"<div id='gnb_sub"+ BaseUtility.zeroFill( gnbSub, 2 ) +"' class='gnb_sub'><ul>");
		dispFull.append("<li>");
		dispFull.append("\n"+ baseTab[6] +"<a href='"+ webBase + standEList[ 0][4] +"'>회의록</a>");
		dispFull.append("\n"+ baseTab[6] +"<div class='fullmenu_list_sub'><ul>");
		dispMobi.append("<li>");
		dispMobi.append("\n"+ baseTab[5] +"<a href='"+ webBase + standEList[ 0][4] +"'>회의록</a>");
		dispMobi.append("\n"+ baseTab[5] +"<div class='mobile_menu_list_sub'><ul>");
		for( int m = 0; m < standEList.length; m++ ) { 
			if( "T".equals(standEList[m][0]) && "1".equals(standEList[m][2]) ) { 
				dispMenu.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(standEList[m][1])?standEList[m][4]+"' target='_blank":webBase+standEList[m][4]) +"'>"+ standEList[m][3] +"</a></li>");
				dispFull.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(standEList[m][1])?standEList[m][4]+"' target='_blank":webBase+standEList[m][4]) +"'>"+ standEList[m][3] +"</a>").append("N".equals(standEList[m][1])?"</li>":"");
				if( "C".equals(standEList[m][1]) ) dispFull.append("\n"+ baseTab[8] +"<ul>");
				dispMobi.append("\n"+ baseTab[6] +"<li><a href='"+ ("L".equals(standEList[m][1])?standEList[m][4]+"' target='_blank":webBase+standEList[m][4]) +"'>"+ standEList[m][3] +"</a>").append("N".equals(standEList[m][1])?"</li>":"");
				if( "C".equals(standEList[m][1]) ) dispMobi.append("\n"+ baseTab[7] +"<ul>");
			}
			if( "T".equals(standEList[m][0]) && "2".equals(standEList[m][2]) ) { 
				dispFull.append("\n"+ baseTab[9] +"<li><a href='"+ ("L".equals(standEList[m][1])?standEList[m][4]+"' target='_blank":webBase+standEList[m][4]) +"'>"+ standEList[m][3] +"</a></li>");
				dispMobi.append("\n"+ baseTab[8] +"<li><a href='"+ ("L".equals(standEList[m][1])?standEList[m][4]+"' target='_blank":webBase+standEList[m][4]) +"'>"+ standEList[m][3] +"</a></li>");
			}
//			if( m != standEList.length-1 && "2".equals(standEList[m][2]) ) { 
//				if( "1".equals(standEList[m+1][2]) ) dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
//				if( "1".equals(standEList[m+1][2]) ) dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
//			}
			if( "T".equals(standEList[m][0]) && "E".equals(standEList[m][1]) ) { 
				dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
				dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
			}
		}
		dispMenu.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispFull.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispMobi.append("\n"+ baseTab[5] +"</ul></div>").append("\n"+ baseTab[4] +"</li>");
	}
	if( !(standFList == null || standFList.length <= 0) ) { 
		gnbSub++;
		dispMenu.append("<li>");
		dispMenu.append("\n"+ baseTab[6] +"<a href='"+ webBase + standFList[ 0][4] +"'>의안정보</a>");
		dispMenu.append("\n"+ baseTab[6] +"<div id='gnb_sub"+ BaseUtility.zeroFill( gnbSub, 2 ) +"' class='gnb_sub'><ul>");
		dispFull.append("<li>");
		dispFull.append("\n"+ baseTab[6] +"<a href='"+ webBase + standFList[ 0][4] +"'>의안정보</a>");
		dispFull.append("\n"+ baseTab[6] +"<div class='fullmenu_list_sub'><ul>");
		dispMobi.append("<li>");
		dispMobi.append("\n"+ baseTab[5] +"<a href='"+ webBase + standFList[ 0][4] +"'>의안정보</a>");
		dispMobi.append("\n"+ baseTab[5] +"<div class='mobile_menu_list_sub'><ul>");
		for( int m = 0; m < standFList.length; m++ ) { 
			if( "T".equals(standFList[m][0]) && "1".equals(standFList[m][2]) ) { 
				dispMenu.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(standFList[m][1])?standFList[m][4]+"' target='_blank":webBase+standFList[m][4]) +"'>"+ standFList[m][3] +"</a></li>");
				dispFull.append("\n"+ baseTab[7] +"<li><a href='"+ ("L".equals(standFList[m][1])?standFList[m][4]+"' target='_blank":webBase+standFList[m][4]) +"'>"+ standFList[m][3] +"</a>").append("N".equals(standFList[m][1])?"</li>":"");
				if( "C".equals(standFList[m][1]) ) dispFull.append("\n"+ baseTab[8] +"<ul>");
				dispMobi.append("\n"+ baseTab[6] +"<li><a href='"+ ("L".equals(standFList[m][1])?standFList[m][4]+"' target='_blank":webBase+standFList[m][4]) +"'>"+ standFList[m][3] +"</a>").append("N".equals(standFList[m][1])?"</li>":"");
				if( "C".equals(standFList[m][1]) ) dispMobi.append("\n"+ baseTab[7] +"<ul>");
			}
			if( "T".equals(standFList[m][0]) && "2".equals(standFList[m][2]) ) { 
				dispFull.append("\n"+ baseTab[9] +"<li><a href='"+ ("L".equals(standFList[m][1])?standFList[m][4]+"' target='_blank":webBase+standFList[m][4]) +"'>"+ standFList[m][3] +"</a></li>");
				dispMobi.append("\n"+ baseTab[8] +"<li><a href='"+ ("L".equals(standFList[m][1])?standFList[m][4]+"' target='_blank":webBase+standFList[m][4]) +"'>"+ standFList[m][3] +"</a></li>");
			}
//			if( m != standFList.length-1 && "2".equals(standFList[m][2]) ) { 
//				if( "1".equals(standFList[m+1][2]) ) dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
//				if( "1".equals(standFList[m+1][2]) ) dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
//			}
			if( "T".equals(standFList[m][0]) && "E".equals(standFList[m][1]) ) { 
				dispFull.append("\n"+ baseTab[8] +"</ul>").append("\n"+ baseTab[7] +"</li>");
				dispMobi.append("\n"+ baseTab[7] +"</ul>").append("\n"+ baseTab[6] +"</li>");
			}
		}
		dispMenu.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispFull.append("\n"+ baseTab[6] +"</ul></div>").append("\n"+ baseTab[5] +"</li>");
		dispMobi.append("\n"+ baseTab[5] +"</ul></div>").append("\n"+ baseTab[4] +"</li>");
	}

	out.clearBuffer(); %>	<div id='skipNavi'><a href='#contents'>본문 바로가기</a></div>
	<div class='top_link'>
		<div class='inner'><ul>
			<li><a href='<%= webBase %>/index.do'>평택시의회</a></li>
			<li><a href='javascript:void(0);' class='on'>상임위원회</a>
				<div class='top_link_sub'><ul>
					<li><a href='<%= webBase %>/oper/index.do'>의회운영위원회</a></li>
					<li><a href='<%= webBase %>/gvadm/index.do'>자치행정위원회</a></li>
					<li><a href='<%= webBase %>/welfare/index.do'>복지환경위원회</a></li>
					<li><a href='<%= webBase %>/build/index.do'>산업건설위원회</a></li>
				</ul></div>
			</li>
			<li><a href='<%= webBase %>/youth/index.do'>청소년의회</a></li>
		</ul></div>
	</div>
	<!-- Base Header -->
	<div id='header'>
		<div class='search_top'><div class='inner'>
			<h1><a href='<%= webBase +"/"+ sndType %>/index.do'><img src='<%= webBase %>/images/common/logo_<%= sndType %>.png' alt='<%= webTitle %>'/></a></h1>
			<div class='zoom'>
				<a href='javascript:void(0);' class='btn_plus'  onclick='zoomcontrol.zoomin();' ><img src='<%= webBase %>/images/icon/ico_plus.png'  alt='확대'/></a>
				<a href='javascript:void(0);' class='btn_minus' onclick='zoomcontrol.zoomout();'><img src='<%= webBase %>/images/icon/ico_minus.png' alt='축소'/></a>
			</div>
		</div></div>
		<div class='gnb_wrap'>
			<div class='gnb'>
				<div class='inner'>
					<ul><%= dispMenu.toString() %></ul>
				</div>
				<div class='bg_gnb_sub'></div>
			</div>
			<div class='fullmenu'>
				<a href='javascript:void(0);' class='open_fullmenu'><img src='<%= webBase %>/images/btns/btn_fullmenu.png' alt='전체메뉴'/></a>
				<div class='fullmenu_list'>
					<ul><%= dispFull.toString() %></ul>
				</div>
			</div>
		</div>
	</div>
	<!-- //Base Header -->
	<!-- //mobile Header -->
	<div id='mobile_header'>
		<a href='javascript:void(0);' class='open_mobile_menu'><img src='<%= webBase %>/images/btns/btn_mobile_menu.png' alt='전체메뉴'/></a>
		<h1><a href='<%= webBase +"/"+ sndType %>/index.do'><img src='<%= webBase %>/images/common/logo_<%= sndType %>.png' alt='<%= webTitle %>'/></a></h1>
		<a href='javascript:void(0);' class='open_mobile_search'><img src='<%= webBase %>/images/btns/btn_mobile_search.png' alt='검색'/></a>
		<div class='mobile_menu'>
			<div class='logo'><img src='<%= webBase %>/images/common/logo_<%= sndType %>.png' alt='<%= webTitle %>'/></div>
			<a href='javascript:void(0);' class='close_mobile_menu'><img src='<%= webBase %>/images/btns/btn_close_mobile_gnb.png' alt='전체메뉴 닫기'/></a>
			<div class='mobile_menu_list'>
				<ul><%= dispMobi.toString() %></ul>
			</div>
		</div>
	</div>
	<!-- //mobile Header -->