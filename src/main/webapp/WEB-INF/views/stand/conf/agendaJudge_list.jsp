<%
////////////////////////////////////////////////////////////
// Program ID  : agendaJudge_list
// Description : 상임위 > 의안정보 - 심사보고서 [목록조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= sndName +" 심사보고서";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" "+ sndName %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scripts.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb06_02").addClass("on").removeClass("noChild");
	});
	var fnActInitial    = function(){
		$("#condType").val("A");
		$("#condValue").val("");
		fnActRetrieve();
	};
	var fnActRetrieve   = function(page){
		doActLoadShow();
		$("#pageCurNo").val(isEmpty(page)?"1":page);
		$("#frmDefault").attr({"action":"agendaList.do","method":"post","target":"_self"}).submit();
	};
	var fnActDetail     = function(view){
		doActLoadShow();
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"agendaView.do","method":"post","target":"_self"}).submit();
	};
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/stand/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/stand/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="6"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 의안정보 &gt; <%= subTitle %></div>
			<div class='program_search'><table>
				<caption class='hide'>Search Detail</caption>
				<colgroup><col style='width:10%;'/><col style='width:90%;'/></colgroup>
				<tbody><tr>
					<th scope='col' class='center'>대수</th>
					<td><label for='reEraCode' class='hide'>대수</label>
						<select id='reEraCode' name='reEraCode' class='w100' onchange='fnActChanger()'><c:choose><c:when test="${ empty eraList}">
							<option value='08'<c:if test="${'08' eq reEraCode}"> selected='selected'</c:if>>제8대</option></c:when><c:otherwise><c:forEach var="cate" varStatus="status" items="${eraList}">
							<option value='${cate.eraCode}'<c:if test="${cate.eraCode eq reEraCode}"> selected='selected'</c:if>>${cate.eraName}</option></c:forEach></c:otherwise></c:choose>
						</select>
					</td>
				</tr><tr>
					<th class='center'>의안결과</th>
					<td><label for='reResCode' class='hide'>의안결과</label>
						<select id='reResCode' name='reResCode' class='w100'><option value=''>전체</option><c:if test="${!empty resList}"><c:forEach var="cate" varStatus="status" items="${resList}">
							<option value='${cate.resCode}'<c:if test="${cate.resCode eq reResCode}"> selected='selected'</c:if>>${cate.resName}</option></c:forEach></c:if>
						</select>
					</td>
				</tr><tr>
					<th class='center'>의안명</th>
					<td><label for='searchText' class='hide'>의안명</label><input type='text' id='searchText' name='searchText' class='w60_p' maxlength='30' value='${searchText}' placeholder='의안명을 입력하세요.'/></td>
				</tr><tr>
					<td colspan='2' class='center'><a href='javascript:void(0);' class='form_btn' onclick='fnActRetrieve()'>검색</a></td>
				</tr></tbody>
			</table></div>
			<div class='board_total'>전체 <strong>${listCnt}</strong>건 &nbsp; ${pageCurNo} / ${pageInfo.totalPageCount}</div>
			<div class='table_list'><table>
				<caption class='hide'>Board List</caption>
				<colgroup><col style='width:10%;'/><col style='width:30%;'/><col style='width:15%;'/><col style='width:15%;'/><col style='width:10%;'/><col style='width:10%;'/><col style='width:10%;'/></colgroup>
				<thead><tr>
					<th scope='col'>의안번호</th>
					<th scope='col'>의안명</th>
					<th scope='col'>발의자</th>
					<th scope='col'>소관위원회</th>
					<th scope='col'>발의일</th>
					<th scope='col'>처리일</th>
					<th scope='col'>처리결과</th>
				</tr></thead>
				<tbody>
				<c:choose><c:when test="${ empty dataList}">
					<tr class='h200'><td colspan='7'><%= noneData %></td></tr>
				</c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${dataList}">
					<tr class='pnc' onclick='fnActDetail("${rult.viewNo}")'>
						<td>${rult.billNum}</td>
						<td class='left' title='${rult.billTitle}'>${rult.billTitle}</td>
						<td>${rult.sugMan}</td>
						<td>${rult.comName}</td>
						<td>${rult.sugDate}</td>
						<td>${rult.genVote}</td>
						<td>${rult.genResName}</td>
					</tr>
				</c:forEach></c:otherwise></c:choose>
				</tbody>
			</table></div>
			<c:if test="${!empty pageInfo}"><div class='paging'><ui:pagination paginationInfo="${pageInfo}" type="baseText" jsFunction="fnActRetrieve" /></div></c:if>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/stand/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>