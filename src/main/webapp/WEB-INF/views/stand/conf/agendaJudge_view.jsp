<%
////////////////////////////////////////////////////////////
// Program ID  : agendaJudge_view
// Description : 상임위 > 의안정보 - 심사보고서 [상세조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= sndName +" 심사보고서";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" "+ sndName %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scripts.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb06_01").addClass("on").removeClass("noChild");
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"agenda"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self"}).submit();
	};
	var fnActDetail     = function(view){
		doActLoadShow(); 
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"agendaView.do","method":"post","target":"_self"}).submit();
	};
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='reEraCode'     name='reEraCode'     value='${reEraCode}'/>
<input type='hidden' id='reRndCode'     name='reRndCode'     value='${reRndCode}'/>
<input type='hidden' id='reSugCode'     name='reSugCode'     value='${reSugCode}'/>
<input type='hidden' id='reResCode'     name='reResCode'     value='${reResCode}'/>
<input type='hidden' id='reComCode'     name='reComCode'     value='${reComCode}'/>
<input type='hidden' id='searchRegi'    name='searchRegi'    value='${searchRegi}'/>
<input type='hidden' id='searchText'    name='searchText'    value='${searchText}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/stand/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/stand/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="6"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 의안정보 &gt; <%= subTitle %></div>
			<div class='table_view'><table>
				<caption class='hide'>Board View</caption>
				<colgroup><col style='width:13%;'/><col style='width:21%;'/><col style='width:12%;'/><col style='width:21%;'/><col style='width:12%;'/><col style='width:21%;'/></colgroup>
				<tbody><tr>
					<th>의안명</th>
					<td colspan='5'>${dataView.billTitle}</td>
				</tr><tr>
					<th scope='col'>대수</th>
					<td>${dataView.eraName}</td>
					<th scope='col'>회기</th>
					<td>${dataView.roundName}</td>
					<th scope='col'>의안번호</th>
					<td>${dataView.billNum}</td>
				</tr><tr>
					<th>발의(제출)일자</th>
					<td>${dataView.sugDate}</td>
					<th>발의(제출)자</th>
					<td colspan='3'>${dataView.sugMan}</td>
				</tr><tr>
					<th>공동발의자</th>
					<td colspan='5'>${dataView.sugWork}</td>
				</tr><tr>
					<th>의안종류</th>
					<td>${dataView.sugName}</td>
					<th>소관위원회</th>
					<td colspan='3'>${dataView.comName}</td>
				</tr><tr>
					<th>관련 회의록</th><%--
					<td colspan='5'>${dataView.confName}<c:if test="${!empty dataView.confCode and !empty dataView.confName}">&nbsp; &nbsp;<input type='button' class='btn_small bt_white' style='width:100px;cursor:pointer;' value='▶ 회의록보기' onclick='open_viewer("<%= webBase %>/viewer/viewer_frame.jsp?conf_code=${dataView.confCode}&conf_name=${dataView.confName}")'/></c:if></td>--%>
					<td colspan='5'>${dataView.confName}<c:if test="${!empty dataView.confCode and !empty dataView.confName}">&nbsp; &nbsp;<input type='button' class='btn_small bt_white' style='width:100px;cursor:pointer;' value='▶ 회의록보기' onclick='fnDocuViewer("${dataView.confUUID}")'/></c:if></td>
				</tr><tr>
					<th colspan='2' rowspan='3'>위원회 처리</th>
					<th>회부일자</th>
					<td>${dataView.comCon}</td>
					<th>보고일자</th>
					<td>${dataView.comRep}</td>
				</tr><tr>
					<th>상정일자</th>
					<td>${dataView.comIntro}</td>
					<th>의결일자</th>
					<td>${dataView.comVote}</td>
				</tr><tr>
					<th>심사결과</th>
					<td colspan='3'>${dataView.comResName}</td>
				</tr><tr>
					<th colspan='2' rowspan='3'>본회의 처리</th>
					<th>접수일자</th>
					<td>${dataView.genCon}</td>
					<th>보고일자</th>
					<td>${dataView.genRep}</td>
				</tr><tr>
					<th>상정일자</th>
					<td>${dataView.genIntro}</td>
					<th>의결일자</th>
					<td>${dataView.genVote}</td>
				</tr><tr>
					<th>심사결과</th>
					<td colspan='3'>${dataView.genResName}</td>
				</tr><tr>
					<th>자치단체 이송일</th>
					<td>${dataView.tranDate}</td>
					<th>공포일</th>
					<td>${dataView.publDate}</td>
					<th>공포번호</th>
					<td>${dataView.publNum}</td>
				</tr><tr>
					<th>메모</th>
					<td colspan='5'>${dataView.etc}</td>
				</tr><tr>
					<th>주요내용</th>
					<td colspan='5'>${dataView.contentsBR}</td>
				</tr><tr>
					<th>첨부파일</th>
					<td colspan='5'>${dataView.fileLinkS}</td>
				</tr></tbody>
			</table></div>
			<div class='btn_group right'>
				<a href='javascript:void(0);' class='btn_style03' onclick='fnActReturn("seq")'>목록</a>
			</div><c:if test="${'T' eq moveItem}">
			<div class='table_view table_nextprev'><table>
				<caption class='hide'>Previous/Next</caption>
				<colgroup><col style='width:10%;'/><col style='width:90%;'/></colgroup>
				<tbody><tr>
					<th scope='col'>이전글</th>
					<td><c:choose><c:when test="${ empty prevView}">이전 글이 존재 하지 않습니다.</c:when><c:otherwise><a href='javascript:void(0);' onclick='fnActDetail("${prevView.viewNo}")'>${prevView.billTitle}</a></c:otherwise></c:choose></td>
				</tr><tr>
					<th scope='col'>다음글</th>
					<td><c:choose><c:when test="${ empty nextView}">다음 글이 존재 하지 않습니다.</c:when><c:otherwise><a href='javascript:void(0);' onclick='fnActDetail("${nextView.viewNo}")'>${nextView.billTitle}</a></c:otherwise></c:choose></td>
				</tr></tbody>
			</table></div></c:if>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/stand/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>