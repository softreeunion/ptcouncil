<%
////////////////////////////////////////////////////////////
// Program ID  : index
// Description : 상임위 / INDEX
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" "+ sndName %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scripts.jspf"/>
	<script>
	$(document).ready(function(){
		$(".main_visual>ul").bxSlider({auto:true});
	});
	var fnActDetail     = function(view,acts) { 
		var $form=$("#frmDetail");
		if($form.length<=0){$form=$("<form/>").attr({id:"frmDetail",name:"frmDetail",method:"post"});$(document.body).append($form);}
		if($form.length<=1){$form.empty();}
		if(!isEmpty(view )){$("<input/>").attr({type:"hidden",id:"viewNo",name:"viewNo",value:view}).appendTo($form);}
		if($form.length<=1){$form.attr({"action":"<%= webBase %>/${sndType}/"+acts+".do","method":"post","target":"_self"}).submit();}
	};
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/stand/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<div id='contents' class='main_visual'><ul><c:if test="${!empty mainImage}"><c:forEach var="rult" varStatus="status" items="${mainImage}"><c:choose><c:when test="${'F' eq rult.bbsSiteType}">
			<li><% if( imgMode ) { %><img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/><% } else { %><img src='<%= webBase + uploadPath %>${rult.fileAbs}' alt='${rult.bbsTitle}'/><% } %></li></c:when><c:otherwise>
			<li><a href='${rult.bbsSiteUri}'<c:if test="${'N' eq rult.bbsSiteType}"> target='_blank'</c:if>><% if( imgMode ) { %><img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/><% } else { %><img src='<%= webBase + uploadPath %>${rult.fileAbs}' alt='${rult.bbsTitle}'/><% } %></a></li></c:otherwise></c:choose></c:forEach></c:if>
		</ul></div>
		<div class='main_section01'>
			<div class='inner'><ul>
				<li><a href='<%= webBase + standAList[ 0][4] %>'><img src='<%= webBase %>/images/main/stand_icon01.png' alt='인사말'/></a></li>
				<li><a href='<%= webBase + standAList[ 1][4] %>'><img src='<%= webBase %>/images/main/stand_icon02.png' alt='소관부서'/></a></li>
				<li><a href='<%= webBase + standBList[ 0][4] %>'><img src='<%= webBase %>/images/main/stand_icon03.png' alt='위원소개'/></a></li>
				<li><a href='<%= webBase + standCList[ 0][4] %>'><img src='<%= webBase %>/images/main/stand_icon04.png' alt='의사일정'/></a></li>
				<li><a href='<%= webBase + standFList[ 0][4] %>'><img src='<%= webBase %>/images/main/stand_icon05.png' alt='의안정보'/></a></li>
			</ul></div>
		</div>
		<div class='main_section02'>
			<div class='notice'>
				<div class='tab_area'><ul>
					<li><a href='javascript:void(0);' class='on'>의안정보</a></li>
					<li><a href='javascript:void(0);'>전자회의록</a></li><%--
					<li><a href='javascript:void(0);'>언론보도</a></li>--%>
				</ul></div>
				<div class='tab_cont'>
					<!-- 의안정보 -->
					<div class='item' style='display:block;'>
						<div class='list'>
							<ul><c:choose><c:when test="${ empty mainItem}">
								<li class='center'><%= viewData %></li></c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${mainItem}">
								<li><a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")'>${rult.billTitle}</a></li></c:forEach></c:otherwise></c:choose>
							</ul>
							<a href='<%= webBase + standFList[ 0][4] %>' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='의안정보 더보기'></a>
						</div>
					</div>
					<!-- //의안정보 -->
					<!-- 전자회의록 -->
					<div class='item'>
						<div class='list'>
							<ul><c:choose><c:when test="${ empty mainConf}">
								<li class='center'><%= viewData %></li></c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${mainConf}"><%--
								<li><a href='javascript:void(0);' onclick='open_viewer("<%= webBase %>/viewer/viewer_frame.jsp?conf_code=${rult.confCode}&conf_name=${rult.confName}")'><c:if test="${'T' eq rult.tempFlag}"><span class='td_blu'>[임시회의록]</span> </c:if>${rult.confName}</a></li>--%>
								<li><a href='javascript:void(0);' onclick='fnDocuViewer("${rult.confUUID}")'><c:if test="${'T' eq rult.tempFlag}"><span class='td_blu'>[임시회의록]</span> </c:if>${rult.confName}</a></li></c:forEach></c:otherwise></c:choose>
							</ul>
							<a href='<%= webBase + standEList[ 0][4] %>' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='회의록 더보기'></a>
						</div>
					</div>
					<!-- //전자회의록 --><%--
					<!-- 언론보도 -->
					<div class='item'>
						<div class='list'>
							<ul><c:choose><c:when test="${ empty mainPress}">
								<li class='center'><%= viewData %></li></c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${mainPress}">
								<li><a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")'>${rult.bbsTitle}</a><span class='date'>${rult.creDate}</span></li></c:forEach></c:otherwise></c:choose>
							</ul>
							<a href='<%= webBase + standDList[ 1][4] %>' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='언론보도 더보기'></a>
						</div>
					</div>
					<!-- //언론보도 -->--%>
				</div>
			</div>
			<div class='gallery'>
				<h2>의정활동 갤러리</h2>
				<a href='<%= webBase + standDList[ 0][4] %>' class='btn_more'><img src='<%= webBase %>/images/btns/btn_more.png' alt='의정활동 갤러리 더보기'/></a>
				<div class='gallery_list'><ul><c:choose><c:when test="${ empty mainPhoto}">
					<li class='center'><%= viewData %></li></c:when><c:otherwise><c:forEach var="rult" varStatus="status" items="${mainPhoto}">
					<li><a href='javascript:void(0);' onclick='fnActDetail("${rult.viewNo}","${rult.actMode}")'><% if( isGallery ) { %>
							<div class='img'><img src='<%= imgDomain %>/mimg/${rult.fileUri}/${rult.fileName}' alt='${rult.bbsTitle}'/></div><% } else { %>
							<div class='img'><img src='<%= webBase %>/GetImage.do?key=${rult.fileUUID}' alt='${rult.bbsTitle}'/></div><% } %>
							<div class='txt'><p>${fn:substring(rult.bbsTitle,0,fn:length(rult.bbsTitle)-3)}</p><span class='date'><% if( isGallery ) { %>${rult.takeDate}<% } else { %>${rult.creDate}<% } %></span></div>
					</a></li></c:forEach></c:otherwise></c:choose>
				</ul></div>
			</div>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/stand/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>