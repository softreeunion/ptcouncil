<%
	////////////////////////////////////////////////////////////
// Program ID  : memberCoun_list
// Description : 상임위 > 위원소개 - 위원소개 [목록조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= sndName +" 위원소개";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" "+ sndName %></title>
	<jsp:directive.include file="/WEB-INF/views/cmmn/add_scripts.jspf"/>
	<script>
		$(document).ready(function(){
			$("#snb02_01").addClass("on").removeClass("noChild");
		});
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
	<div class='sub' id='wrap'>
		<!-- gnb -->
		<jsp:include page="/WEB-INF/views/stand/inc/header.jsp" flush="true"/>
		<!-- //gnb -->
		<div id='container'>
			<!-- lnb -->
			<jsp:include page="/WEB-INF/views/stand/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="2"/></jsp:include>
			<!-- //lnb -->
			<div id='contents'>
				<%-- <h3 class='h3_tit'><%= subTitle %></h3>
                <div class='path'>home &gt; 위원소개 &gt; <%= subTitle %></div>  --%>
				<div class='section'>
					<c:if test="${'stand' eq sndType or 'oper' eq sndType}">
						<div class='path'>home &gt; 위원회소개 &gt; 의회운영위원회 위원소개</div>
						<h3 class='h3_tit'>의회운영위원회 위원소개</h3></br>
						<div class='member_list'><ul>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09004.jpg' alt='최재영 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09004","A")'>자세히</a></span></dt>
								<dd><p>최재영</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원장</td></tr>
									<tr><th>선거구</th><td>나(중앙,서정)</td></tr>
									<tr><th>정당명</th><td>더불어민주당</td></tr>
									<tr><th>휴대폰</th><td>-</td></tr>
									<tr><th>이메일</th><td>jy710729@hanmail.net</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09008.jpg' alt='최준구 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09008","A")'>자세히</a></span></dt>
								<dd><p>최준구</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>부위원장</td></tr>
									<tr><th>선거구</th><td>라(비전1,동삭)</td></tr>
									<tr><th>정당명</th><td>국민의힘</td></tr>
									<tr><th>휴대폰</th><td>-</td></tr>
									<tr><th>이메일</th><td>jkchoi19@naver.com</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09001.jpg' alt='이관우 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09001","A")'>자세히</a></span></dt>
								<dd><p>이관우</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
									<tr><th>선거구</th><td>가(진위,서탄,지산,송북,신장1·2)</td></tr>
									<tr><th>정당명</th><td>국민의힘</td></tr>
									<tr><th>휴대폰</th><td>010-9054-3872</td></tr>
									<tr><th>이메일</th><td>dd3872@hanmail.net</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09016.jpg' alt='정일구 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09016","A")'>자세히</a></span></dt>
								<dd><p>정일구</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
									<tr><th>선거구</th><td>사(팽성,고덕,신평,원평,고덕)</td></tr>
									<tr><th>정당명</th><td>국민의힘</td></tr>
									<tr><th>휴대폰</th><td>-</td></tr>
									<tr><th>이메일</th><td>ilgoo21@naver.com</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09014.jpg' alt='김승겸 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09014","A")'>자세히</a></span></dt>
								<dd><p>김승겸</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
									<tr><th>선거구</th><td>사(팽성,고덕,신평,원평,고덕)</td></tr>
									<tr><th>정당명</th><td>더불어민주당</td></tr>
									<tr><th>휴대폰</th><td>010-5413-8213</td></tr>
									<tr><th>이메일</th><td>seunggyum123@hanmail.net</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09015.jpg' alt='이기형 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09015","A")'>자세히</a></span></dt>
								<dd><p>이기형</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
									<tr><th>선거구</th><td>사(팽성,고덕,신평,원평,고덕)</td></tr>
									<tr><th>정당명</th><td>더불어민주당</td></tr>
									<tr><th>휴대폰</th><td>010-5390-3482</td></tr>
									<tr><th>이메일</th><td>isowkenmay@hanmail.net</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
						</ul></div>
					</c:if>
					<c:if test="${'gvadm' eq sndType}">
						<div class='path'>home &gt; 위원소개 &gt; <%= subTitle %></div>
						<h3 class='h3_tit'><%= subTitle %></h3>
						<div class='member_list'><ul>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09016.jpg' alt='정일구 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09016","A")'>자세히</a></span></dt>
								<dd><p>정일구</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원장</td></tr>
									<tr><th>선거구</th><td>사(팽성,고덕,신평,원평,고덕)</td></tr>
									<tr><th>정당명</th><td>국민의힘</td></tr>
									<tr><th>휴대폰</th><td>-</td></tr>
									<tr><th>이메일</th><td>ilgoo21@naver.com</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09002.jpg' alt='이종원 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09002","A")'>자세히</a></span></dt>
								<dd><p>이종원</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>부위원장</td></tr>
									<tr><th>선거구</th><td>가(진위,서탄,지산,송북,신장1·2)</td></tr>
									<tr><th>정당명</th><td>더불어민주당</td></tr>
									<tr><th>휴대폰</th><td>-</td></tr>
									<tr><th>이메일</th><td>hot88heart@hanmail.net</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09004.jpg' alt='최재영 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09004","A")'>자세히</a></span></dt>
								<dd><p>최재영</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
									<tr><th>선거구</th><td>나(중앙,서정)</td></tr>
									<tr><th>정당명</th><td>더불어민주당</td></tr>
									<tr><th>휴대폰</th><td>-</td></tr>
									<tr><th>이메일</th><td>jy710729@hanmail.net</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09010.jpg' alt='김혜영 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09010","A")'>자세히</a></span></dt>
								<dd><p>김혜영</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
									<tr><th>선거구</th><td>마(비전2,용이)</td></tr>
									<tr><th>정당명</th><td>국민의힘</td></tr>
									<tr><th>휴대폰</th><td>-</td></tr>
									<tr><th>이메일</th><td>hyeyoung5558@naver.com</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09008.jpg' alt='최준구 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09008","A")'>자세히</a></span></dt>
								<dd><p>최준구</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
									<tr><th>선거구</th><td>라(비전1,동삭)</td></tr>
									<tr><th>정당명</th><td>국민의힘</td></tr>
									<tr><th>휴대폰</th><td>-</td></tr>
									<tr><th>이메일</th><td>jkchoi19@naver.com</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09012.jpg' alt='류정화 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09012","A")'>자세히</a></span></dt>
								<dd><p>류정화</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
									<tr><th>선거구</th><td>바(안중,포승,청북,오성,현덕)</td></tr>
									<tr><th>정당명</th><td>더불어민주당</td></tr>
									<tr><th>휴대폰</th><td>010-7372-3883</td></tr>
									<tr><th>이메일</th><td>rjhtee@naver.com</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
						</ul></div>
					</c:if>
					<c:if test="${'welfare' eq sndType}">
						<div class='path'>home &gt; 위원소개 &gt; <%= subTitle %></div>
						<h3 class='h3_tit'><%= subTitle %></h3>
						<div class='member_list'><ul>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09014.jpg' alt='김승겸 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09014","A")'>자세히</a></span></dt>
								<dd><p>김승겸</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원장</td></tr>
									<tr><th>선거구</th><td>사(팽성,고덕,신평,원평,고덕)</td></tr>
									<tr><th>정당명</th><td>더불어민주당</td></tr>
									<tr><th>휴대폰</th><td>010-5413-8213</td></tr>
									<tr><th>이메일</th><td>seunggyum123@hanmail.net</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09017.jpg' alt='김순이 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09017","A")'>자세히</a></span></dt>
								<dd><p>김순이</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>부위원장</td></tr>
									<tr><th>선거구</th><td>비례대표</td></tr>
									<tr><th>정당명</th><td>국민의힘</td></tr>
									<tr><th>휴대폰</th><td>-</td></tr>
									<tr><th>이메일</th><td>nike708@nate.com</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09001.jpg' alt='이관우 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09001","A")'>자세히</a></span></dt>
								<dd><p>이관우</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
									<tr><th>선거구</th><td>가(진위,서탄,지산,송북,신장1·2)</td></tr>
									<tr><th>정당명</th><td>국민의힘</td></tr>
									<tr><th>휴대폰</th><td>010-9054-3872</td></tr>
									<tr><th>이메일</th><td>dd3872@hanmail.net</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09018.jpg' alt='최선자 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09018","A")'>자세히</a></span></dt>
								<dd><p>최선자</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
									<tr><th>선거구</th><td>비례대표</td></tr>
									<tr><th>정당명</th><td>더불어민주당</td></tr>
									<tr><th>휴대폰</th><td>010-5611-8400</td></tr>
									<tr><th>이메일</th><td>sunja8400@hanmail.net</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09007.jpg' alt='김명숙 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09007","A")'>자세히</a></span></dt>
								<dd><p>김명숙</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
									<tr><th>선거구</th><td>라(비전1,동삭)</td></tr>
									<tr><th>정당명</th><td>더불어민주당</td></tr>
									<tr><th>휴대폰</th><td>010-4040-8690</td></tr>
									<tr><th>이메일</th><td>kimms8690@gmail.com</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
						</ul></div>
					</c:if>
					<c:if test="${'build' eq sndType}">
						<div class='path'>home &gt; 위원소개 &gt; <%= subTitle %></div>
						<h3 class='h3_tit'><%= subTitle %></h3>
						<div class='member_list'><ul>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09015.jpg' alt='이기형 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09015","A")'>자세히</a></span></dt>
								<dd><p>이기형</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원장</td></tr>
									<tr><th>선거구</th><td>사(팽성,고덕,신평,원평,고덕)</td></tr>
									<tr><th>정당명</th><td>더불어민주당</td></tr>
									<tr><th>휴대폰</th><td>010-5390-3482</td></tr>
									<tr><th>이메일</th><td>isowkenmay@hanmail.net</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09005.jpg' alt='소남영 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09005","A")'>자세히</a></span></dt>
								<dd><p>소남영</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>부위원장</td></tr>
									<tr><th>선거구</th><td>다(송탄,통복,세교)</td></tr>
									<tr><th>정당명</th><td>국민의힘</td></tr>
									<tr><th>휴대폰</th><td>010-3757-4475</td></tr>
									<tr><th>이메일</th><td>youngjinso@naver.com</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09003.jpg' alt='김영주 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09003","A")'>자세히</a></span></dt>
								<dd><p>김영주</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원장</td></tr>
									<tr><th>선거구</th><td>나(중앙,서정)</td></tr>
									<tr><th>정당명</th><td>국민의힘</td></tr>
									<tr><th>휴대폰</th><td>010-8634-0467</td></tr>
									<tr><th>이메일</th><td>rla0wn@nate.com</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09011.jpg' alt='강정구 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09011","A")'>자세히</a></span></dt>
								<dd><p>강정구</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
									<tr><th>선거구</th><td>바(안중,포승,청북,오성,현덕)</td></tr>
									<tr><th>정당명</th><td>국민의힘</td></tr>
									<tr><th>휴대폰</th><td>010-3710-9279</td></tr>
									<tr><th>이메일</th><td>kjk9279@hanmail.net</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09006.jpg' alt='이윤하 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09006","A")'>자세히</a></span></dt>
								<dd><p>이윤하</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
									<tr><th>선거구</th><td>다(송탄,통복,세교)</td></tr>
									<tr><th>정당명</th><td>더불어민주당</td></tr>
									<tr><th>휴대폰</th><td>010-2390-7771</td></tr>
									<tr><th>이메일</th><td>daeha101@hanmail.net</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
							<li><dl>
								<dt><p><img src='<%= webBase %>/images/former/09009.jpg' alt='김산수 의원'/></p><span><a href='javascript:void(0);' onclick='fnActActive("09009","A")'>자세히</a></span></dt>
								<dd><p>김산수</p><table>
									<colgroup><col style='width:20%;'/><col style='width:80%;'/></colgroup>
									<tbody>
									<tr><th class='b_none'>직&nbsp; &nbsp;위</th><td class='b_none'>위원</td></tr>
									<tr><th>선거구</th><td>마(비전2,용이)</td></tr>
									<tr><th>정당명</th><td>더불어민주당</td></tr>
									<tr><th>휴대폰</th><td>010-6234-4794</td></tr>
									<tr><th>이메일</th><td>sandsoo@hanmail.net</td></tr>
									</tbody>
								</table></dd>
							</dl></li>
						</ul></div>
					</c:if>
				</div>
			</div>
		</div>
		<!-- footer -->
		<jsp:include page="/WEB-INF/views/stand/inc/footer.jsp" flush="true"/>
		<!-- //footer -->
	</div>
</form>
</body>
</html>
