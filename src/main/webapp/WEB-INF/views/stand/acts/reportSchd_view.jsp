<%
////////////////////////////////////////////////////////////
// Program ID  : reportSchd_view
// Description : 상임위 > 의사일정 - 의사일정 [상세조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "의사일정";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" "+ sndName %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scripts.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb03_01").addClass("on").removeClass("noChild");
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"report"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self"}).submit();
	};
	var fnActDetail     = function(view){
		doActLoadShow();
		$("#viewNo").val(view);
		$("#frmDefault").attr({"action":"reportView.do","method":"post","target":"_self"}).submit();
	};
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/stand/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/stand/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="3"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 의사일정 &gt; <%= subTitle %></div>
			<div class='table_view'><table>
				<caption class='hide'>Board View</caption>
				<colgroup><col style='width:10%;'/><col style='width:30%;'/><col style='width:10%;'/><col style='width:20%;'/><col style='width:10%;'/><col style='width:20%;'/></colgroup>
				<tbody><tr>
					<th>제목</th>
					<td colspan='5'>${dataView.bbsTitle}</td>
				</tr><tr>
					<th scope='col'>작성자</th>
					<td>${dataView.creName}</td>
					<th scope='col'>작성일</th>
					<td>${dataView.creDate}</td>
					<th scope='col'>조회수</th>
					<td>${dataView.hitCount}</td>
				</tr><tr>
					<th>첨부파일</th>
					<td colspan='5'>${dataView.fileLinkS}</td>
				</tr><tr class='h200'>
					<th>내용</th>
					<td colspan='5' class='cont'><c:choose><c:when test="${'P' eq dataView.contPattern}">${dataView.contDescBR}</c:when><c:otherwise>${dataView.contDesc}</c:otherwise></c:choose></td>
				</tr></tbody>
			</table></div>
			<div class='btn_group right'>
				<a href='javascript:void(0);' class='btn_style03' onclick='fnActReturn("seq")'>목록</a>
			</div><c:if test="${'T' eq moveItem}">
			<div class='table_view table_nextprev'><table>
				<caption class='hide'>Previous/Next</caption>
				<colgroup><col style='width:10%;'/><col style='width:90%;'/></colgroup>
				<tbody><tr>
					<th scope='col'>이전글</th>
					<td><c:choose><c:when test="${ empty prevView}">이전 글이 존재 하지 않습니다.</c:when><c:otherwise>${prevView.gabSpace}<a href='javascript:void(0);' onclick='fnActDetail("${prevView.viewNo}")'><c:if test="${'Y' eq prevView.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${prevView.bbsTitle}</a></c:otherwise></c:choose></td>
				</tr><tr>
					<th scope='col'>다음글</th>
					<td><c:choose><c:when test="${ empty nextView}">다음 글이 존재 하지 않습니다.</c:when><c:otherwise>${nextView.gabSpace}<a href='javascript:void(0);' onclick='fnActDetail("${nextView.viewNo}")'><c:if test="${'Y' eq nextView.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${nextView.bbsTitle}</a></c:otherwise></c:choose></td>
				</tr></tbody>
			</table></div></c:if>
		</div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/stand/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>