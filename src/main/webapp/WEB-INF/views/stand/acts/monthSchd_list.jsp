<%
////////////////////////////////////////////////////////////
// Program ID  : monthSchd_list
// Description : 상임위 > 의사일정 - 의사일정 [목록조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= sndName +" 의사일정";
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle +" "+ sndName %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scripts.jspf"/>
	<script>
	$(document).ready(function(){
		$("#snb03_01").addClass("on").removeClass("noChild");
	});
	var fnActRetrieve   = function(date,stat){
		doActLoadShow();
		$("#reStdDate").val(isEmpty(date)?"${reCurrDate}":date);
		$("#reStdStat").val(isEmpty(stat)?"C":stat);
		$("#frmDefault").attr({"action":"monthList.do","method":"post","target":"_self"}).submit();
	};
	var fnActMonth      = function(view,acts){
		$.ajax({
			type          : "post", 
			url           : "<%= webBase %>/coun/monthView.do", 
			dataType      : "html", 
			data          : {viewNo:view,stdDate:acts,actMode:"D"}, 
			success       : function(data,status,xhr  ){$("#layMonth").empty();$("#layMonth").html(data);month_layer();}, 
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);}
		}); 
	};
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='reStdStat'     name='reStdStat'     value='${reStdStat}'/>
<input type='hidden' id='reStdDate'     name='reStdDate'     value='${reStdDate}'/>
<div class='sub' id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/stand/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='container'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/stand/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="3"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'>
			<h3 class='h3_tit'><%= subTitle %></h3>
			<div class='path'>home &gt; 의사일정 &gt; <%= subTitle %></div>
			<div class='section'>
				<div class='calender_area'>
					<div class='calenda_header'>
						<div class='cal_left'><span class='title'>${dispDate}</span></div>
						<div class='cal_right'><div class='icon_guide'>
							<a href='javascript:void(0);' onclick='fnActRetrieve("${reCurrDate}","C" )' class='cal_btn01'>오늘</a>
							<a href='javascript:void(0);' onclick='fnActRetrieve("${reStdDate }","PY")' class='cal_btn02'>이전 해</a>
							<a href='javascript:void(0);' onclick='fnActRetrieve("${reStdDate }","PM")' class='cal_btn03'>이전 달</a>
							<a href='javascript:void(0);' onclick='fnActRetrieve("${reStdDate }","NM")' class='cal_btn03'>다음 달</a>
							<a href='javascript:void(0);' onclick='fnActRetrieve("${reStdDate }","NY")' class='cal_btn02'>다음 해</a>
						</div></div>
					</div>
					<table class='calendar'>
						<caption class='hide'>의사일정</caption>
						<colgroup><col style='width:12%;'/><col style='width:15%;'/><col style='width:15%;'/><col style='width:15%;'/><col style='width:15%;'/><col style='width:15%;'/><col style='width:13%;'/></colgroup>
						<thead><tr class='weekdays'>
							<th class='sun'>일</th>
							<th>월</th>
							<th>화</th>
							<th>수</th>
							<th>목</th>
							<th>금</th>
							<th class='sat'>토</th>
						</tr></thead>
						<tbody><c:choose><c:when test="${ empty dispDocu}"><tr class='off h200'><td colspan='7'><%= noneData %></td></tr></c:when><c:otherwise>${dispDocu}</c:otherwise></c:choose></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class='layer'><div class='bg'></div><div class='layer_pop' id='layMonth'></div></div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/stand/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>