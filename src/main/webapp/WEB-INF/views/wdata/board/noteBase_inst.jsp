<%
////////////////////////////////////////////////////////////
// Program ID  : noteArch_inst
// Description : 게시판관리 > 의회자료실 [등록화면]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "의정팀";
	String subFirst				= "";
	if(mngrFList!=null) for(int m=0;m<mngrFList.length;m++) if("T".equals(mngrFList[m][0])){subFirst=mngrFList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script>
	$(document).ready(function(){
		$("[id^='creOpen']").change(function(){$("#creHide").val($("[id^='creOpen']:checked").val());});
		$("[id^='creOpen']").eq(1).prop("checked",true);
		$("#bbsTitle").focus();
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"Notice"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self","enctype":""}).submit();
	};<c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole or fn:contains(MemRInfo.userComm, '06'))}">
	var fnActUpdate     = function(){
        if($.trim($("#bbsTitle").val())==""){alert("제목은 필수입력입니다.");$("#bbsTitle").focus();return false;}
        if($("[id^='creOpen']:checked").length<=0){alert("공개여부는 필수선택입니다.");$("[id^='creOpen']:eq(0)").focus();return false;}
        if($.trim($("#crePasswd").val())==""){alert("비밀번호는 필수입력입니다.");$("#crePasswd").focus();return false;}
        if($.trim($("#contDesc").val())==""){alert("작성된 내용이 없습니다. 내용을 입력하세요.");$("#contDesc").focus();return false;}
        $("#frmDefault").attr({"action":"NoticeCreate.do","method":"post","target":"_self","enctype":"multipart/form-data"}).ajaxSubmit({
            dataType      : "json",
            cache         : false ,
            contentType   : false ,
            processData   : false ,
            beforeSubmit  : function(data,form,options){doActLoadShow();},
            beforeSend    : function(){$(".progress").show();$(".progress-bar").width("0%");},
            uploadProgress: function(event,position,total,percentComplete){$(".progress-bar").width(percentComplete+"%");if(percentComplete==100){$(".progress").hide();};},
            success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn("seq");}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
            error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
        });
    };</c:if>
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/wdata/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/wdata/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="6"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/webdata/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='#'>의회자료실</a></li>
				<li><a href='#'>사무국</a></li>
				<li><a href='<%= webBase %>/webdata/base/NoticeList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %></h3>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:88%;'/></colgroup>
					<tbody><tr>
						<th scope='col'>제목 *</th>
						<td>
                            <input type='text' id='bbsTitle' name='bbsTitle' class='w90_p' maxlength='400' />
                        </td>
					</tr><tr>
						<th>공개여부 *</th>
						<td colspan='3'>
							<input type='hidden' id='creHide' name='creHide'/>
							<input type='radio' id='creOpen1' name='creOpen' value='Y'/> <label for='creOpen1'>비공개</label>
							<input type='radio' id='creOpen2' name='creOpen' value='N'/> <label for='creOpen2'>공개</label>
						</td>
					</tr><tr>
                        <th scope='col'>비밀번호 *</th>
                        <td>
                            <%--<input type='password' id='crePasswd' name='crePasswd' class='w20_p' maxlength='40' />--%>
                            <input type="password" style="display: block; width:0px; height:0px; border: 0;">
                            <input type="password" id='crePasswd' name='crePasswd' class='w20_p' maxlength='40' placeholder="비밀번호를 입력해주세요" autocomplete="off" ref="pwdInput"/>
                        </td>
                    </tr><tr>
						<th>내용 *</th>
						<td><textarea id='contDesc' name='contDesc' rows='16' class='w90_p'></textarea></td>
					</tr><tr>
						<th>첨부파일</th>
						<td id='file_base1'><input type='file' id='fileBase1' name='fileBase1' class='w90_p'/></td>
					</tr></tbody>
				</table>
				<div class='search_area'><input type='button' id='fileAdd01' class='w70 btn_small bt_blue' value='파일추가'/> &nbsp; <%= allowFiles[3] %></div>
				<div class='btn_area right'><c:if test="${('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole or fn:contains(MemRInfo.userComm, '06'))}">
					<input type='button' class='w70 btn_medium bt_red'    value='저장'       onclick='fnActUpdate()'/></c:if>
					<input type='button' class='w70 btn_medium bt_grey'   value='목록'       onclick='fnActReturn("seq")'/>
				</div>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/wdata/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>