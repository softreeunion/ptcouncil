<%
////////////////////////////////////////////////////////////
// Program ID  : noteArch_view
// Description : 게시판관리 > 의회자료실 [상세조회]
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String subTitle				= "산업건설위원회";
	boolean viewReply			= false;
	String subFirst				= "";
	if(mngrFList!=null) for(int m=0;m<mngrFList.length;m++) if("T".equals(mngrFList[m][0])){subFirst=mngrFList[m][4];break;}
	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= subTitle +" &gt; "+ webTitle %></title>
<jsp:directive.include file="/WEB-INF/views/cmmn/add_scriptm.jspf"/>
	<script>
	$(document).ready(function(){
	});
	var fnActReturn     = function(seq){
		doActLoadShow();
		$("#frmDefault").attr({"action":"Notice"+(seq=="seq"?"List":"View")+".do","method":"post","target":"_self"}).submit();
	};
	var fnActDetail     = function(view,opts,cid,uid){
		var show=false;
		if(opts=="Y") {
			if(cid == uid) {
				show = true;
			} else {
				show = false;
			}
		} else {
			show = true;
		}
        <c:if test="${'SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole}">
        show = true;
        </c:if>
		if (show) {
			doActLoadShow();
			$("#viewNo").val(view);
			$("#frmDefault").attr({"action":"NoticeView.do","method":"post","target":"_self"}).submit();
		} else {
			alert("비공개 게시물입니다");
			return;
		}
	};
	// var fnActDetail     = function(view){
	// 	doActLoadShow();
	// 	$("#viewNo").val(view);
	// 	$("#frmDefault").attr({"action":"NoticeView.do","method":"post","target":"_self"}).submit();
	// };
	<c:if test="${!empty dataView and ('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole or fn:contains(MemRInfo.userComm, '04'))}">
	var fnActModify     = function(){<c:choose><c:when test="${'0' eq dataView.ansCount}">
		doActLoadShow();
		$("#frmDefault").attr({"action":"NoticeEdit.do","method":"post","target":"_self"}).submit();</c:when><c:otherwise>
		alert("답변글이 있어 수정이 제한됩니다.");return false;</c:otherwise></c:choose>
	};
	var fnActDelete     = function(){<c:choose><c:when test="${'0' eq dataView.ansCount}">
		if(confirm("<%= alertMsg[4].replaceAll("[@]", "현재") %>")==0) return false;
		$("#frmDefault").attr({"action":"NoticeDelete.do","method":"post","target":"_self"}).ajaxSubmit({
			dataType      : "json", 
			cache         : false , 
			beforeSubmit  : function(data,form,options){doActLoadShow();},
			success       : function(data,status,xhr  ){if(!isEmpty(data.message)){alert(data.message);}if(data.isError=="T"){doActLoadHide();}else{fnActReturn("seq");}if(!isEmpty(data.returnUrl)){location.href=data.returnUrl;}},
			error         : function(xhr ,status,error){console.log(xhr.responseText);alert("Code: "+xhr.status+"\nMessage: "+error);doActLoadHide();}
		});</c:when><c:otherwise>
		alert("답변글이 있어 삭제가 제한됩니다.");return false;</c:otherwise></c:choose>
	};</c:if><% if( viewReply ) { %><c:if test="${!empty dataView}">
	var fnActReply      = function(){
		doActLoadShow();
		$("#frmDefault").attr({"action":"NoticeReEdit.do","method":"post","target":"_self"}).submit();
	};</c:if><% } %>
	</script>
    <script>
        function viewPopPasswd(type){
            $('#popPasswd').css('visibility','visible');
            // $('#popPasswd').css('visibility','hidden');
            $("#actionType").val(type);

			<c:if test="${'SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole}">
			$("#crePasswdCon").val(${dataView.crePasswd});
			</c:if>
        }
        function compPasswd(pwd){
            if($.trim($("#crePasswdCon").val())==""){alert("비밀번호를 입력해주세요.");$("#crePasswdCon").focus();return false;}
            if($.trim($("#crePasswdCon").val())==pwd) {
                // 수정, 삭제 처리
                if( $("#actionType").val()=="mod" ) {
                    fnActModify(); // 수정
                } else if(  $("#actionType").val()=="del" ) {
                    fnActDelete(); // 삭제
                }
            } else {
                alert("비밀번호가 틀립니다. 비밀번호를 다시 입력해주세요.");$("#crePasswdCon").focus();return false;
            }
        }
    </script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='viewNo'        name='viewNo'        value='${viewNo}'/>
<input type='hidden' id='actMode'       name='actMode'       value='<c:if test="${ empty dataView}">Create</c:if><c:if test="${!empty dataView}">Update</c:if>'/>
<input type='hidden' id='pageCurNo'     name='pageCurNo'     value='${pageCurNo}'/>
<input type='hidden' id='pagePerNo'     name='pagePerNo'     value='${pagePerNo}'/>
<input type='hidden' id='condCate'      name='condCate'      value='${condCate}'/>
<input type='hidden' id='condName'      name='condName'      value='${condName}'/>
<input type='hidden' id='condType'      name='condType'      value='${condType}'/>
<input type='hidden' id='condValue'     name='condValue'     value='${condValue}'/>
<input type='hidden' id='condDele'      name='condDele'/>
<div id='wrap'>
	<!-- gnb -->
<jsp:include page="/WEB-INF/views/wdata/inc/header.jsp" flush="true"/>
	<!-- //gnb -->
	<div id='content_wrap'>
		<!-- lnb -->
<jsp:include page="/WEB-INF/views/wdata/inc/lefter.jsp" flush="true"><jsp:param name="tb" value="6"/></jsp:include>
		<!-- //lnb -->
		<div id='contents'><div class='inner'>
			<div class='path'><div class='inner'><ul>
				<li><a href='<%= webBase %>/webdata/index.do'><img src='<%= webBase %>/images/icon/ico_home.png' alt='home'/></a></li>
				<li><a href='#'>의회자료실</a></li>
				<li><a href='#'>상임위원회</a></li>
				<li><a href='<%= webBase %>/webdata/build/NoticeList.do'><%= subTitle %></a></li>
			</ul></div></div>
			<h3><%= subTitle %> <!-- 나중에 삭제 --><%--(${MemRInfo.authRole}) (${MemRInfo.userComm}) (${MemRInfo.userPID})--%></h3>
			<div class='board_list'>
				<table class='table table_view'>
					<caption class='hide'>Board View</caption>
					<colgroup><col style='width:12%;'/><col style='width:38%;'/><col style='width:12%;'/><col style='width:38%;'/></colgroup>
					<tbody><tr>
						<th>제목</th>
						<td colspan='3'>${dataView.bbsTitle}</td>
					</tr><tr>
						<th scope='col'>작성자</th>
						<td>${dataView.creName}</td>
						<th scope='col'>작성일</th>
						<td>${dataView.creDate}</td>
					</tr><tr>
						<th>공개여부</th>
						<td>${dataView.creHideName}</td>
						<th>조회수</th>
						<td>${dataView.hitCount}</td>
					</tr><tr>
						<th>첨부파일</th>
						<td colspan='3'>${dataView.fileLinkS}</td>
					</tr><tr class='h200'>
						<th>내용</th>
						<td colspan='3'><c:choose><c:when test="${'P' eq dataView.contPattern}">${dataView.contDescBR}</c:when><c:otherwise>${dataView.contDesc}</c:otherwise></c:choose></td>
					</tr></tbody>
				</table>
                <div id="popPasswd" style="width:100%; height:100px; z-index:1; visibility: hidden">
                    <table class='table table_view'>
                        <caption class='hide'>Board View</caption>
                        <colgroup><col style='width:12%;'/><col style='width:38%;'/><col style='width:12%;'/><col style='width:38%;'/></colgroup>
                        <tbody><tr>
                            <th>비밀번호 확인</th>
                            <td colspan='3'>
                                <input type="password" style="display: block; width:0px; height:0px; border: 0;">
                                <input type="password" id='crePasswdCon' name='crePasswdCon' class='w20_p' maxlength='40' placeholder="비밀번호를 입력해주세요" autocomplete="off" ref="pwdInput"/>
                                <input type='button' class='w70 btn_medium bt_green' value='확인' onclick='compPasswd("${dataView.crePasswd}")' style="cursor: pointer"/>
                                <input type="hidden" id="actionType" name="actionType">
                            </td>
                        </tr></tbody>
                    </table>
                </div>
				<div class='btn_area right'>
					<% if( viewReply ) { %>
					<c:if test="${!empty dataView}">
					<input type='button' class='w70 btn_medium bt_blue'   value='답변'       onclick='fnActReply()'/>
					<c:if test="${'0' eq dataView.ansCount and ('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole or fn:contains(MemRInfo.userComm, '04'))}">
					<input type='button' class='w70 btn_medium bt_blue'   value='수정'       onclick='fnActModify()'/>
					<input type='button' class='w70 btn_medium bt_red'    value='삭제'       onclick='fnActDelete()'/>
					</c:if>
					</c:if>
					<% } else { %>
					<c:if test="${!empty dataView and '0' eq dataView.ansCount and ('SU' eq MemRInfo.authRole or 'CM' eq MemRInfo.authRole or 'GM' eq MemRInfo.authRole or fn:contains(MemRInfo.userComm, '04'))}">
                    <%--<input type='button' class='w70 btn_medium bt_blue'   value='수정'       onclick='fnActModify()'/>--%>
					<%--<input type='button' class='w70 btn_medium bt_red'    value='삭제'       onclick='fnActDelete()'/>--%>
                    <input type='button' class='w70 btn_medium bt_blue'   value='수정'       onclick='viewPopPasswd("mod")'/>
                    <input type='button' class='w70 btn_medium bt_red'    value='삭제'       onclick='viewPopPasswd("del")'/>
					</c:if>
					<% } %>
					<input type='button' class='w70 btn_medium bt_grey'   value='목록'       onclick='fnActReturn("seq")'/>
				</div><c:if test="${'T' eq moveItem}">
				<div class='board_list'><table class='table table_view'>
					<caption class='hide'>Previous/Next</caption>
					<colgroup><col style='width:12%;'/><col style='width:88%;'/></colgroup>
					<tbody><tr>
						<th scope='col'>이전글</th>
						<td><c:choose><c:when test="${ empty prevView}">이전 글이 존재 하지 않습니다.</c:when><c:otherwise>${prevView.gabSpace}<a href='javascript:void(0);' onclick='fnActDetail("${prevView.viewNo}","${prevView.creHide}","${prevView.creUser}","${MemRInfo.userPID}")'><c:if test="${'Y' eq prevView.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${prevView.bbsTitle}</a></c:otherwise></c:choose></td>
					</tr><tr>
						<th scope='col'>다음글</th>
						<td><c:choose><c:when test="${ empty nextView}">다음 글이 존재 하지 않습니다.</c:when><c:otherwise>${nextView.gabSpace}<a href='javascript:void(0);' onclick='fnActDetail("${nextView.viewNo}","${nextView.creHide}","${nextView.creUser}","${MemRInfo.userPID}")'><c:if test="${'Y' eq nextView.creHide && false}"><img src='<%= webBase %>/images/icon/ico_lock.png' class='vam' alt='lock'/></c:if>${nextView.bbsTitle}</a></c:otherwise></c:choose></td>
					</tr></tbody>
				</table></div></c:if>
			</div>
		</div></div>
	</div>
	<!-- footer -->
<jsp:include page="/WEB-INF/views/wdata/inc/footer.jsp" flush="true"/>
	<!-- //footer -->
</div>
</form>
</body>
</html>