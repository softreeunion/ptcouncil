<%
////////////////////////////////////////////////////////////
// Program ID  : lefter
// Description : 관리자 LEFTER
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String tbParam				= BaseUtility.null2Blank( request.getParameter("tb") );

	out.clearBuffer();

	out.print(baseTab[2] +"<div id='lnb'>\n");
	out.print(baseTab[3] +"<h2>비밀번호 변경</h2>\n");
	out.print(baseTab[3] +"<ul>\n");
	out.print(baseTab[3] +"</ul>\n");
	out.print(baseTab[2] +"</div>");
%>