<%
////////////////////////////////////////////////////////////
// Program ID  : lefter
// Description : 관리자 LEFTER
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String tbParam				= BaseUtility.null2Blank( request.getParameter("tb") );

	out.clearBuffer();
	// 메뉴 - 1.계정 관리
//	if( "1".equals(tbParam) ) {
//		out.print(baseTab[2] +"<div id='lnb'>\n");
//		out.print(baseTab[3] +"<h2>계정 관리</h2>\n");
//		out.print(baseTab[3] +"<ul>\n");
//		if(mngrAList!= null) for(int m=0;m<mngrAList.length;m++) if("T".equals(mngrAList[m][0]))
//			out.print(baseTab[4] +"<li><a href='"+ webBase + mngrAList[m][4] +"'>"+ mngrAList[m][3] +"</a></li>\n");
//		out.print(baseTab[3] +"</ul>\n");
//		out.print(baseTab[2] +"</div>");
//	}
//	// 메뉴 - 2.메인 관리
//	if( "2".equals(tbParam) ) {
//		out.print(baseTab[2] +"<div id='lnb'>\n");
//		out.print(baseTab[3] +"<h2>메인 관리</h2>\n");
//		out.print(baseTab[3] +"<ul>\n");
//		if(mngrBList!=null) for(int m=0;m<mngrBList.length;m++) if("T".equals(mngrBList[m][0]))
//			out.print(baseTab[4] +"<li><a href='"+ webBase + mngrBList[m][4] +"'>"+ mngrBList[m][3] +"</a></li>\n");
//		out.print(baseTab[3] +"</ul>\n");
//		out.print(baseTab[2] +"</div>");
//	}
//	// 메뉴 - 3.의원 관리
//	if( "3".equals(tbParam) ) {
//		out.print(baseTab[2] +"<div id='lnb'>\n");
//		out.print(baseTab[3] +"<h2>의원 관리</h2>\n");
//		out.print(baseTab[3] +"<ul>\n");
//		if(mngrCList!=null) for(int m=0;m<mngrCList.length;m++) if("T".equals(mngrCList[m][0]))
//			out.print(baseTab[4] +"<li><a href='"+ webBase + mngrCList[m][4] +"'>"+ mngrCList[m][3] +"</a></li>\n");
//		out.print(baseTab[3] +"</ul>\n");
//		out.print(baseTab[2] +"</div>");
//	}
//	// 메뉴 - 4.의정 활동
//	if( "4".equals(tbParam) ) {
//		out.print(baseTab[2] +"<div id='lnb'>\n");
//		out.print(baseTab[3] +"<h2>의정 활동</h2>\n");
//		out.print(baseTab[3] +"<ul>\n");
//		if(mngrDList!=null) for(int m=0;m<mngrDList.length;m++) if("T".equals(mngrDList[m][0]))
//			out.print(baseTab[4] +"<li><a href='"+ webBase + mngrDList[m][4] +"'>"+ mngrDList[m][3] +"</a></li>\n");
//		out.print(baseTab[3] +"</ul>\n");
//		out.print(baseTab[2] +"</div>");
//	}
//	// 메뉴 - 5.회의록 관리
//	if( "5".equals(tbParam) ) {
//		out.print(baseTab[2] +"<div id='lnb'>\n");
//		out.print(baseTab[3] +"<h2>회의록 관리</h2>\n");
//		out.print(baseTab[3] +"<ul>\n");
//		if(mngrEList!=null) for(int m=0;m<mngrEList.length;m++) if("T".equals(mngrEList[m][0]))
//			out.print(baseTab[4] +"<li><a href='"+ webBase + mngrEList[m][4] +"'>"+ mngrEList[m][3] +"</a></li>\n");
//		out.print(baseTab[3] +"</ul>\n");
//		out.print(baseTab[2] +"</div>");
//	}
	// 메뉴 - 6.게시판 관리
//	if( "6".equals(tbParam) ) {
		out.print(baseTab[2] +"<div id='lnb'>\n");
		out.print(baseTab[3] +"<h2>의회자료실</h2>\n");
		out.print("<div class='wdata_menu'>상임위원회</div>\n");
		out.print(baseTab[3] +"<ul>\n");
		out.print(baseTab[4] +"<li><a href='/webdata/oper/NoticeList.do'>의회운영위원회</a></li>\n");
		out.print(baseTab[4] +"<li><a href='/webdata/gvadm/NoticeList.do'>기획행정위원회</a></li>\n");
		out.print(baseTab[4] +"<li><a href='/webdata/welfare/NoticeList.do'>복지환경위원회</a></li>\n");
		out.print(baseTab[4] +"<li><a href='/webdata/build/NoticeList.do'>산업건설위원회</a></li>\n");
		out.print(baseTab[3] +"</ul>\n");
		out.print("<div class='wdata_menu'>의회사무국</div>\n");
		out.print(baseTab[3] +"<ul>\n");
		out.print(baseTab[4] +"<li><a href='/webdata/base/NoticeList.do'>의정팀</a></li>\n");
		out.print(baseTab[4] +"<li><a href='/webdata/proc/NoticeList.do'>의사팀</a></li>\n");
		out.print(baseTab[4] +"<li><a href='/webdata/prom/NoticeList.do'>홍보팀</a></li>\n");
		out.print(baseTab[4] +"<li><a href='/webdata/supp/NoticeList.do'>입법지원팀</a></li>\n");
		out.print(baseTab[3] +"</ul>\n");
		out.print("<br/>\n");
		out.print(baseTab[2] +"</div>");
//	}
	// 메뉴 - 7.상임위원회
//	if( "7".equals(tbParam) ) {
//		out.print(baseTab[2] +"<div id='lnb'>\n");
//		out.print(baseTab[3] +"<h2>상임위원회</h2>\n");
//		out.print(baseTab[3] +"<ul>\n");
//		if(mngrGList!=null) for(int m=0;m<mngrGList.length;m++) if("T".equals(mngrGList[m][0]))
//			out.print(baseTab[4] +"<li><a href='"+ webBase + mngrGList[m][4] +"'>"+ mngrGList[m][3] +"</a></li>\n");
//		out.print(baseTab[3] +"</ul>\n");
//		out.print(baseTab[2] +"</div>");
//	}
//	// 메뉴 - 8.청소년의회
//	if( "8".equals(tbParam) ) {
//		out.print(baseTab[2] +"<div id='lnb'>\n");
//		out.print(baseTab[3] +"<h2>청소년의회</h2>\n");
//		out.print(baseTab[3] +"<ul>\n");
//		if(mngrHList!=null) for(int m=0;m<mngrHList.length;m++) if("T".equals(mngrHList[m][0]))
//			out.print(baseTab[4] +"<li><a href='"+ webBase + mngrHList[m][4] +"'>"+ mngrHList[m][3] +"</a></li>\n");
//		out.print(baseTab[3] +"</ul>\n");
//		out.print(baseTab[2] +"</div>");
//	}
//	// 메뉴 - 9.접속통계
//	if( "9".equals(tbParam) ) {
//		out.print(baseTab[2] +"<div id='lnb'>\n");
//		out.print(baseTab[3] +"<h2>접속통계</h2>\n");
//		out.print(baseTab[3] +"<ul>\n");
//		if(mngrIList!=null) for(int m=0;m<mngrIList.length;m++) if("T".equals(mngrIList[m][0]))
//			out.print(baseTab[4] +"<li><a href='"+ webBase + mngrIList[m][4] +"'>"+ mngrIList[m][3] +"</a></li>\n");
//		out.print(baseTab[3] +"</ul>\n");
//		out.print(baseTab[2] +"</div>");
//	}
%>