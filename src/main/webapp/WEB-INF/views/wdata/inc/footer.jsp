<%
////////////////////////////////////////////////////////////
// Program ID  : footer
// Description : 관리자 FOOTER
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	String tbParam				= BaseUtility.null2Char( request.getParameter("tb"), "T" );

	out.clearBuffer();
	out.print(baseTab[1] +"<div id='footer'>\n");
	out.print(baseTab[2] +"<p>Copyright PyeongTaek City Council. All Rights Reserved.</p>\n");
	out.print(baseTab[2] +"<div class='go_btn'><ul>\n");
//	out.print(baseTab[3] +"<li><a href='#wrap'  ><img src='"+ webBase +"/images/icon/ico_arrow_up.png'   alt='UP'/></a></li>\n");
//	out.print(baseTab[3] +"<li><a href='#footer'><img src='"+ webBase +"/images/icon/ico_arrow_down.png' alt='DN'/></a></li>\n");
	out.print(baseTab[2] +"</ul></div>\n");
	out.print(baseTab[1] +"</div>\n");
	if( "T".equals(tbParam) ) out.print(baseTab[1] +"<input style='visibility:hidden;width:0;'/><label for='' class='hide'>Input</label>\n");
	out.print(baseTab[1] +"<div id='baseDN-modal'  style='display:none;' title='기본 프로그레스'><div id='progressBarLD' style='width:100%;height:22px;margin-top:38px;'></div></div>\n");
	out.print(baseTab[1] +"<div id='fileDN-modal'  style='display:none;' title='파일 다운로드'  ><div id='progressBarDN' style='width:100%;height:22px;margin-top:38px;'></div></div>\n");
	out.print(baseTab[1] +"<div id='errorDN-modal' style='display:none;' title='오류'           ><p style='margin-top:20px;'>엑셀파일 생성이 실패하였습니다.<br><br>관리자에게 문의바랍니다.</p></div>\n");
	out.print(baseTab[1] +"<div id='fileUP-modal'  style='display:none;' title='파일 업로드'    ><div id='progressBarUP' style='width:100%;height:22px;margin-top:38px;'></div></div>\n");
	out.print(baseTab[1] +"<div id='errorUP-modal' style='display:none;' title='오류'           ><p style='margin-top:20px;'>엑셀파일 처리가 실패하였습니다.<br><br>관리자에게 문의바랍니다.</p></div>\n");
	out.print(baseTab[1] +"<div id='progress'      class='progress'      title='파일 업로드'    ><div class='progress-bar'></div></div>\n"); %>