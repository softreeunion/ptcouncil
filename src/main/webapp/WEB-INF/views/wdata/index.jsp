<%
////////////////////////////////////////////////////////////
// Program ID  : index
// Description : 관리자 INDEX
// Author      : HexaMedia new0man
// Write Date  : 2019.07.01
////////////////////////////////////////////////////////////
// 버전No   개발(운영)담당         수정일자    수정근거
//  v0.1    HexaMedia new0man      2019.07.01  최초 생성
//  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
////////////////////////////////////////////////////////////
%>
<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<jsp:directive.include file="/WEB-INF/views/cmmn/inc/incTagLib.jspf"/>
<jsp:directive.include file="/WEB-INF/views/cmmn/cm_variable.jspf"/>
<%	out.clearBuffer(); %><!DOCTYPE html>
<html lang='ko' xmlns='http://www.w3.org/1999/xhtml'>
<head><%= webMeta.replaceAll("[`]", "\n\t") %>
	<title><%= webTitle %></title>
	<link rel='shortcut icon' type='image/x-icon' href='<%= webBase %>/images/common/council.ico'>
	<link rel='stylesheet' media='all' href='<%= webBase %>/jquerys/jQuery-ui-1.11.4/jquery-ui.min.css'/>
	<link rel='stylesheet' media='all' href='<%= webBase %>/css/reset_m.css'/>
	<link rel='stylesheet' media='all' href='<%= webBase %>/css/style_m.css'/>
	<script src='<%= webBase %>/jquerys/jquery-2.2.4.min.js'></script>
	<script src='<%= webBase %>/jquerys/jQuery-ui-1.11.4/jquery-ui.min.js'></script>
	<script src='<%= webBase %>/scripts/rsa/jsbn.js'></script>
	<script src='<%= webBase %>/scripts/rsa/rsa.js'></script>
	<script src='<%= webBase %>/scripts/rsa/prng4.js'></script>
	<script src='<%= webBase %>/scripts/rsa/rng.js'></script>
	<script>
	$(document).ready(function(){
		$("#k_username").focus();
		$("#frmDefault input").keypress(function(e){
			if(e.which == 13) { 
				if($.trim($("#k_username").val())=="") { 
					alert("아이디를 입력 해주세요.");
					$("#k_username").focus();
					return false;
				} else if($.trim($("#k_userpass").val())=="") { 
					alert("비밀번호를 입력 해주세요.");
					$("#k_userpass").focus();
					return false;
				} else { 
					fnActLogin();
				}
			}
		});
	});
	var fnActLogin      = function(){
		var rsa = new RSAKey();
		if( $.trim($("#k_username").val())=="" ) { 
			alert("아이디를 입력 해주세요.");
			$("#k_username").focus();
			return false;
		} else if( $.trim($("#k_userpass").val())=="" ) { 
			alert("비밀번호를 입력 해주세요.");
			$("#k_userpass").focus();
			return false;
		}
		rsa.setPublic($("#rsaKeyModulus").val(),$("#rsaKeyExponent").val());
		$("#j_username").val(rsa.encrypt($("#k_username").val()));
		$("#j_userpass").val(rsa.encrypt($("#k_userpass").val()));
		$("#k_username").val("");
		$("#k_userpass").val("");
		$("#frmDefault").attr({"action":"<%= webBase %>/webdata/huin_proc.do","method":"post"}).submit();
	};
	</script>
</head>

<body>
<form id='frmDefault' name='frmDefault'>
<input type='hidden' id='rsaKeyModulus' name='rsaKeyModulus' value='${RSAModulus}'/>
<input type='hidden' id='rsaKeyExponent' name='rsaKeyExponent' value='${RSAExponent}'/>
<input type='hidden' id='j_username' name='j_username'>
<input type='hidden' id='j_userpass' name='j_userpass'>
<div id='login_wrap'>
	<div class='login_area2'>
		<h2><img src='<%= webBase %>/images/common/logo_base.png' alt='<%= webTitle %>'/> </h2>
		<div style="font-size:24px;color:#333;padding-bottom:30px;"><img src='<%= webBase %>/images/common/folder2-open.svg' alt='자료실' style="width:26px;margin-top:-6px"/>&nbsp;평택시의회 자료실입니다.</div>
		<div class='form_area'>
			<input type='text' id='k_username' name='k_username' maxlength='20' placeholder='아이디'/>
			<input type='password' id='k_userpass' name='k_userpass' maxlength='20' placeholder='비밀번호'/>
			<span><a class='log_btn' href='javascript:void(0);' onclick='fnActLogin()'>로그인</a></span>
		</div>
		<span class='login_info'>평택시의회 자료를 관리하는 공간입니다.</span>
	</div>
</div>
<input style='visibility:hidden;width:0px;'></form>
</body>
</html>