<%@page import="org.springframework.util.StringUtils"%>
<%@ page language="java" contentType="text/html; charset=euc-kr" pageEncoding="euc-kr"%>
<%@ page import="java.io.*"%>
<%@ page import="council.search.util.ParamUtil"%>
<%@ page import="council.search.util.SearchUtil"%>
<%@ page import="council.search.util.ServletInfo"%>
<%
	request.setCharacterEncoding("euc-kr");
	String conf_code = (String)request.getParameter("conf_code");
	String howview = ParamUtil.getReqParameter((String)request.getParameter("howview"),"0"); //전체보기(0) 혹은 쪽보기(1)
	//쪽보기
	int tot_line = ParamUtil.getIntParameter(request.getParameter("tot_line"));	
	int page_line = 40; 
	int now_page = ParamUtil.getIntParameter(request.getParameter("page"));
	//폰트
	String font = ParamUtil.getReqParameter((String)request.getParameter("font"),"1111601"); //크기 + 모양 + 간격	
	int size = Integer.parseInt(font.substring(0,2));
	String fontkind = font.substring(2,3);
	if (fontkind.equals("1")) fontkind="굴림";
	if (fontkind.equals("2")) fontkind="궁서"; 
	if (fontkind.equals("3")) fontkind="돋음";
	if (fontkind.equals("4")) fontkind="바탕";
	int height = Integer.parseInt(font.substring(3,6));
	String lnk = font.substring(6,7);
	if (lnk.equals("0")) lnk="black";
	if (lnk.equals("1")) lnk="blue";

	String search = ParamUtil.getReqParameter((String)request.getParameter("search")); //키워드

	//System.out.println(search);
	//System.out.println((String)request.getParameter("search"));
	
	String str = null;
//	String DocBase = request.getSession().getServletContext().getRealPath("/");
String DocBase = egovframework.com.cmm.util.BaseUtility.getFileBase();
	ServletInfo Info = new ServletInfo();

	if (howview.equals("1")) {
		str = SearchUtil.getReadFile(DocBase+Info.getHtmlPath()+"/"+conf_code+".html",page_line,now_page);
	}else{ //전체보기
		str = SearchUtil.getReadFile(DocBase+Info.getHtmlPath()+"/"+conf_code+".html");
	}

	//검색어가 있으면 빨간색 링크 처리
	if(!StringUtils.isEmpty(str)){
		if (!search.trim().equals("")){
			String[] tmp = str.split(search); 
			String fileString = "";
			String font_start = "";
			StringBuffer textbuffer = new StringBuffer(); 
			int j = 0;
			int count = tmp.length;

			str = "";
			for (int i=0;i<count;i++){ 
				j = i + 1;
				if (i != 0) {font_start = "</font></a>";} //처음에는 써줄 필요 없음
				String font_end="<a href=#a"+j+" name=a"+i+"><font color=green>"; //일단 정해주고 나서 마지막에만 지워줌
						
				if (i == count-1) { //막판엔 찍지 말자.
					font_end = "";
					search = "";
				}
				textbuffer.append(font_start + tmp[i] + font_end + search);	
			}
			str = textbuffer.toString();
		}
	}

	//자바스크립트
	String jscss = "<SCRIPT language=JavaScript>" + "\r\n";
	jscss=jscss+"function showapp(conf_code,sug_seq){window.open('top_apps.jsp?conf_code='+conf_code+'&sug_seq='+sug_seq,'applist','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=450,height=300');}" + "\r\n"; //안건 부록
	jscss=jscss+"function showmem(mem_id){window.open('mem_profile.jsp?mem_id='+mem_id,'memprofile','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=500,height=440');}" + "\r\n"; //의원 프로필
	jscss=jscss+"function showdic(dic_seq){window.open('top_dic.jsp?dic_seq='+dic_seq,'dictionary','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=auto,resizable=no,width=480,height=440');}" + "\r\n"; //의회 용어
	jscss=jscss+"</script>" + "\r\n";

	//스타일 시트
	int Tsize = size + 15;
	int Ssize = size + 5;
	int Sugsize = size + 2;
	jscss=jscss+"<style type=text/css>" + "\r\n";
	jscss=jscss+"<!--" + "\r\n";
	jscss=jscss+"body{background-color:#f5fefa}" + "\r\n";
	jscss=jscss+"span{font-size:"+ size +"pt;line-height:"+ height +"%;font-family:"+fontkind+";}" + "\r\n";
	jscss=jscss+".Title{font-size:"+ Tsize +"pt;line-height:"+ height;
	jscss=jscss+"%;font-family:"+fontkind+";font-weight:\"bold\";text-align:center;}" + "\r\n";
	jscss=jscss+".Sub{font-size:"+ Ssize +"pt;line-height:"+ height +"%;font-family:"+fontkind+";font-weight:\"bold\";text-align:right;}" + "\r\n";
	jscss=jscss+".Sug{font-size:"+ Sugsize +"pt;line-height:"+ height +"%;font-family:"+fontkind+";font-weight:\"bold\";text-align:right;}" + "\r\n";
	jscss=jscss+"A:link{color:"+ lnk +";text-decoration:none} "  + "\r\n";
	jscss=jscss+"A:active{color:blue;text-decoration:none}" + "\r\n";
	jscss=jscss+"A:visited{color:"+ lnk +";text-decoration:none}" + "\r\n";
	jscss=jscss+"A:hover{color:red;text-decoration:underline}" + "\r\n";
	jscss=jscss+"hr{color:#AEE0B3;}" + "\r\n";
	jscss=jscss+"BODY {	" + "\r\n";
	jscss=jscss+"scrollbar-3dlight-color:595959;" + "\r\n";
	jscss=jscss+"scrollbar-arrow-color:ffffff;" + "\r\n";
	jscss=jscss+"scrollbar-base-color:CFCFCF;" + "\r\n";
	jscss=jscss+"scrollbar-darkshadow-color:FFFFFF;" + "\r\n";
	jscss=jscss+"scrollbar-face-color:CFCFCF;" + "\r\n";
	jscss=jscss+"scrollbar-highlight-color:FFFFFF;" + "\r\n";
	jscss=jscss+"scrollbar-shadow-color:595959 }" + "\r\n";
	jscss=jscss+".HStyle0{text-indent:0.000pt;margin-right:0.000pt;margin-top:0.000pt;margin-bottom:0.000pt;}" + "\r\n";
	jscss=jscss+"-->" + "\r\n";
	jscss=jscss+"</style>" + "\r\n";

	//html
	out.println("<html>" + "\r\n");
	out.println("<head>" + "\r\n");
	out.println(jscss  + "\r\n");
	out.println("</head>" + "\r\n");
	out.println("<body leftmargin=10 topmargin=10 marginwidth=0 marginheight=0 background=images/bodybg.gif>" + "\r\n");
	out.println("<table width='100%' height='100%' border='0' cellpadding='0' cellspacing='0'><tr><td>" + "\r\n");
	out.println(str + "\r\n");
	out.println("</td></tr></table></body>" + "\r\n");
	out.println("</html>");
%>