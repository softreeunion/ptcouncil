<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.Statement"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ page import="java.sql.ResultSet"%>
<%@ page import="java.sql.SQLException"%>
<%@ page import="council.search.jdbc.DBManager"%>
<%@ page import="council.search.util.ParamUtil"%>
<%@ page import="council.search.util.SearchUtil"%>
<%@ page import="council.search.util.ServletInfo"%>
<%
	String contextRoot = request.getContextPath();
	String conf_code = ParamUtil.getReqParameter((String)request.getParameter("conf_code"));
	String sug_seq = ParamUtil.getReqParameter((String)request.getParameter("sug_seq"));
	String where = "";
	ServletInfo Info = new ServletInfo();
	
	if (sug_seq.length() == 6) where="and A.app_name='"+conf_code+"-"+sug_seq+"'"; //텍스트 부록링크
	if (!sug_seq.equals("") && sug_seq.length() < 6) where="and A.sug_seq='"+sug_seq+"'"; //안건 부록링크

	StringBuffer query = new StringBuffer(); 
	query.append("select A.app_name, A.app_ext, A.app_sub, B.sug_title from sug_app A, conf_sug B, conf_ms C ");
	query.append("where A.sug_seq = B.sug_seq and B.conf_seq = C.conf_seq and C.conf_code='" + conf_code);
	query.append("' "+ where +" order by A.sug_seq");
	
    ArrayList arr_app_name = new ArrayList();
	ArrayList arr_app_ext = new ArrayList();
	ArrayList arr_app_sub = new ArrayList();
	ArrayList arr_sug_title = new ArrayList();
	int total = 0;

    ResultSet rs = null;
    PreparedStatement pstmt = null;
    Connection conn = null;
    try{
    	conn = DBManager.getConnection();
		pstmt = conn.prepareStatement(query.toString());
    	rs = pstmt.executeQuery();
		int i = 0;
    	while(rs.next()){
			arr_app_name.add(rs.getString("app_name"));
			arr_app_ext.add(rs.getString("app_ext"));
			arr_app_sub.add(rs.getString("app_sub"));
			arr_sug_title.add(rs.getString("sug_title"));
			total++;
    	}
    } finally{
    	DBManager.close(pstmt,conn);
    }

	//텍스트 부록링크일 경우 바로 열고 창 닫는다
	if (sug_seq.length() == 6) {
		out.print ("<script>");
		out.print ("window.open('"+contextRoot+"/"+Info.getAppPath()+"/"+(String)arr_app_name.get(0)+"."+(String)arr_app_ext.get(0)+"','부록파일','resizable=yes,scrollbars=yes');");
		out.print ("self.close();");
		out.print ("</script>");
	}
%>
<html>
<head>
<title><%//=SearchUtil.getCodeToName(conf_code)%></title>
<link rel="stylesheet" href="<%=contextRoot%>/search/include/text.css" type="text/css">
</head>
<body bgcolor="#FFFFFF" leftmargin="5" topmargin="0" marginwidth="10" marginheight="0">
<!-- 안건 부록 리스트 시작 -->
<table align=center>
	<tr>
		<td align=center>
		<b><font color=491010 size=4>안건 부록 리스트</font></b> 
		<br><div align=right>[총 <%=total%> 건]</div>
		</td>
	</tr>
	<%
	String sug_title = "";	
	for (int i=0;i<arr_app_name.size();i++){
	%>
	<tr>
		<td>
		<%if (!sug_title.equals((String)arr_sug_title.get(i))){%>
		<br><b><font color=61809D><%=(String)arr_sug_title.get(i)%></font></b><br>
		<%
		}
		%>
		&nbsp;&nbsp;&nbsp;<a href="#" onclick="javascript:window.open('<%=Info.getAppPath()+"/"+(String)arr_app_name.get(i)+"."+(String)arr_app_ext.get(i)%>','부록파일','resizable=yes,scrollbars=yes');"><%=(String)arr_app_sub.get(i)%>&nbsp;<img src='images/dum.gif' height='16' width='15' border='0'></a>
		</td>
	</tr>
	<%
		sug_title = (String)arr_sug_title.get(i);
	}
	if (total == 0) out.println("<tr><td align=center>부록이 없습니다.</td></tr>");
	%>	
	<tr>
		<td height=10></td>
	</tr>
	<tr>
		<td align=center><input type=image src="/search/images/close.jpg" width="56" height="29" onclick="self.close();"> </td>
	</tr>
</table>
<!-- 안건 부록 리스트 끝 -->
</body>
</html>
