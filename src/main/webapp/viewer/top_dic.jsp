<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.Statement"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ page import="java.sql.ResultSet"%>
<%@ page import="java.sql.SQLException"%>
<%@ page import="council.search.jdbc.DBManager"%>
<%@ page import="council.search.util.ParamUtil"%>
<%@ page import="council.search.util.Korean"%>
<%
	String contextRoot = request.getContextPath();	
	String dic_seq = ParamUtil.getReqParameter((String)request.getParameter("dic_seq"));	
	//String dic_word = Korean.toKorean(ParamUtil.getReqParameter((String)request.getParameter("dic_word"))); //키워드
	int total = 0;	

	String dic_word = request.getParameter("dic_word") == null ? "" : (String)request.getParameter("dic_word");

	StringBuffer query = new StringBuffer(); 
	query.append("select dic_word, dic_explain from dic_ms where ");
	if (!dic_seq.equals("")) {
		query.append("dic_seq = '"+dic_seq+"'");
	}else{
		query.append("dic_word like '%"+dic_word+"%' order by dic_word asc");
	}	
	
    ArrayList arr_dic_word = new ArrayList();
	ArrayList arr_dic_explain = new ArrayList();

    ResultSet rs = null;
    PreparedStatement pstmt = null;
    Connection conn = null;
    try{
    	conn = DBManager.getConnection();
		pstmt = conn.prepareStatement(query.toString());
    	rs = pstmt.executeQuery();
		int i = 0;
    	while(rs.next()){
			arr_dic_word.add(rs.getString("dic_word"));
			arr_dic_explain.add(rs.getString("dic_explain"));	
			total++;
    	}
    } finally{
    	DBManager.close(pstmt,conn);
    }
%>
<html>
<head>
<title>의회용어사전</title>
<link rel="stylesheet" href="<%=contextRoot%>/search/include/text.css" type="text/css">
</head>
<body bgcolor="#FFFFFF" leftmargin="5" topmargin="0" marginwidth="10" marginheight="0" onLoad="sch_dic.dic_word.focus();">
<!-- 의회용어사전 시작 -->
<table align=center>
	<tr>
		<td align=left>
		<form method="GET" action="<%=contextRoot%>/search/top_dic.jsp" name=sch_dic>
		찾고자 하시는 의회용어를 입력하세요
		<input type="text" size="15" maxlength=20 name="dic_word" value="<%=dic_word%>">
		<input type=image src="<%=contextRoot%>/search/images/bt_search.gif" width="54" height="19" border="0" align="absmiddle">
		</form>
		</td>
	</tr>
	<%	
	for (int i=0;i<arr_dic_word.size();i++){
	%>
	<tr>
		<td>
		<table width="100%">
			<tr> 				
				<td width="100%" align=left><b><font color="#990000">[<%=(String)arr_dic_word.get(i)%>]</font></b><br></td>
			</tr>
			<tr>
				<td width="100%"><div align="justify"><%=(String)arr_dic_explain.get(i)%></div></td>
			</tr>
			<tr>
				<td height="10"></td>
			</tr>
		</table>
		</td>
	</tr>
	<%		
	}
	if (total == 0) out.println("<tr><td align=center>해당 용어에 대한 설명이 없습니다.</td></tr>");
	%>	
	<tr>
		<td height=10></td>
	</tr>
	<tr>
		<td align=center><input type=button class=button onclick="self.close();" value='닫 기'> </td>
	</tr>
</table>
<!-- 의회용어사전 끝 -->
</body>
</html>
