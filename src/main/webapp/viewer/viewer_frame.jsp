<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="council.search.util.ParamUtil"%>
<%@ page import="java.util.*, java.text.*"%>
<%
String conf_code = (String)request.getParameter("conf_code");
String conf_name = (String)request.getParameter("conf_name");
String ag = ParamUtil.getReqParameter((String)request.getParameter("ag"),"a0"); //안건 바로 가기 정보가 없으면 #a0로 보냄 
String keyword = (String)request.getParameter("keyword");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
<% 
Date nowDate = new Date();
SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd");
//if((conf_code.equals("0802031002018111900") || conf_code.equals("0802031002018111901") || conf_code.equals("0802032032018112001") || conf_code.equals("0802031002018113002")) && Integer.parseInt(ft.format(nowDate)) < 20190115) { 
if(conf_code.compareTo("0802042032019052300") >= 0 && Integer.parseInt(ft.format(nowDate)) < 20190115) { 
%>
alert("본 회의록은 회의내용의 신속한 정보제공을 위한 임시회의록으로서 완결본이 아니므로 열람에 참고하시기 바라며, 타인에게 이를 열람하게 하거나 전재·복사하게 하여서는 안 됨을 알려드립니다.");
<% } %>
</script>
</head>
<frameset rows="100,*" frameborder="0" border="0">
	<frame name="top" scrolling="no" src="viewer_top.jsp?conf_code=<%=conf_code%>&conf_name=<%=conf_name%>&search=<%=keyword%>" marginwidth="0" marginheight="6" noresize> 
	<frame name="main" src="viewer_main.jsp?conf_code=<%=conf_code%>&conf_name=<%=conf_name%>&font=1111601&search=<%=keyword%>#<%=ag%>" marginwidth="50" marginheight="50">
	<noframes>
	<body>
	<p>This page uses frames, but your browser doesn't support them.</p>
	</body>
	</noframes>
</frameset>
</html>