<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="council.search.util.ParamUtil"%>
<%@ page import="council.search.util.Korean"%>
<%
	String conf_code = (String)request.getParameter("conf_code");
	String font = (String)request.getParameter("font");
	String word = (String)request.getParameter("word"); //키워드
	String howview = (String)request.getParameter("howview");
	int now_page = ParamUtil.getIntParameter(request.getParameter("page"));
	int tot_line = ParamUtil.getIntParameter(request.getParameter("tot_line"));
%>
<html>
<head>
<title>회의록내 단어찾기</title>
<script>
/* --------------------------------------------------
   스트링에서 왼쪽공백제거 (Left Trim)
-------------------------------------------------- */
function LTrim(SrcString)
{
   len = SrcString.length;
   for(i=0;i<len;i++)
   {
      if(SrcString.substring(0,1) == " ")
      {
         SrcString = SrcString.substring(1);
      }
      else
      {
         break;
      }
   }
   return SrcString;
}

/* --------------------------------------------------
   스트링에서 오른쪽공백제거 (Right Trim)
-------------------------------------------------- */
function RTrim(SrcString)
{
   len = SrcString.length;
   for(i=len;i>0;i--)
   {
      if(SrcString.substring(i-1) == " ")
      {
         SrcString = SrcString.substring(0,i-1);
      }
      else
      {
         break;
      }
   }
   return SrcString;        
}

/* --------------------------------------------------
   스트링에서 양쪽공백제거 (Left Right Trim)
-------------------------------------------------- */
function Trim(SrcString)
{
   rtnStr = RTrim(LTrim(SrcString))
   return rtnStr;
}

<%
if (word != null) {
	word = Korean.toKorean(word);
%>
	opener.location.replace('viewer_top.jsp?conf_code=<%=conf_code%>&font=<%=font%>&search=<%=word%>&howview=<%=howview%>&page=<%=now_page%>&tot_line=<%=tot_line%>');		

	opener.parent.main.location.replace('viewer_main.jsp?conf_code=<%=conf_code%>&font=<%=font%>&search=<%=word%>&howview=<%=howview%>&page=<%=now_page%>&tot_line=<%=tot_line%>#a0');
	self.close();
<%
}else{
%>
	function send() {
		if (Trim(find.word.value) == "") {
			alert("\n찾으실 단어를 입력해 주세요.");
			document.find.word.focus();
			return;
		}
		find.submit();
	}
<%
}
%>
</script>
<style type='text/css'>
  A:link    { color: black; text-decoration:none;     }
  A:visited { color: #505050; text-decoration:none;     }
  A:active  { text-decoration:none;     }
  A:hover   { color: #0040b0; text-decoration:underline;}
  .small    { font-family:돋움; font-size:8pt  }
  .norm     { font-family:굴림; font-size:9pt  }
  .big      { font-family:굴림; font-size:14pt  }
  .button   { height:19px; border-width:1; border-style:ridge; border-color:#d0d0d0; background-color:#dfdfdf;}
  .editbox  { border:1 solid black; background-color:white; }
  .admin    { border:1 solid black; background-color:white; }
  select    { background-color:white;}
  p,br,body,table,td,select,input,form,textarea,option { font-family:굴림; font-size:9pt; }
  .text1     { color:#000000; }
  .text2     { color:#000000; }
</style>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="find.word.focus()">
<form name="find" method=post action="top_fword.jsp">
<input type=hidden name=conf_code value=<%=conf_code%>>
<input type=hidden name=font value=<%=font%>>
<input type=hidden name=howview value=<%=howview%>>
<input type=hidden name=page value=<%=now_page%>>
<input type=hidden name=tot_line value=<%=tot_line%>>
<center>
      
    <table width=170 align=center cellspacing=0 bgcolor="#FFFFFF">
      <tr> 
        <td align=right colspan=2><div align="center"><img src="images/searching.jpg" width="170" height="44"></div></td>
      </tr>
      <tr> 
        <td align=right colspan=2><div align="center"><img src="images/line.jpg" width="170" height="1"></div></td>
      </tr>
      <tr> 
        <td width=80 height="40" align=right><div align="center"><b><font color=486E95>단어 입력 : </font></b></div></td>
        <td width="96" height=40 align=left> <input type=text class=linetext size=12 maxlength=16 name=word>
          &nbsp; </td>
      </tr>
      <tr> 
        <td align=right colspan=2><div align="center"><img src="images/line.jpg" width="170" height="1"></div></td>
      </tr>
      <tr> 
        <td align=center height=35 colspan=2> <input type=button class=button onclick="send();" value='찾기'> 
          &nbsp; <input type=button class=button onclick="self.close();" value='취소'> 
        </td>
      </tr>

    </table> 
</center>
</form>
</body>
</html>
