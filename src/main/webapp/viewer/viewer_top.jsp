<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.io.*"%>
<%@ page import="council.search.util.ParamUtil"%>
<%@ page import="council.search.util.SearchUtil"%>
<%@ page import="council.search.util.ServletInfo"%>
<%@ page import="council.search.util.Korean"%>
<%@ page import="java.util.*, java.text.*"%>
<%
	String contextRoot = request.getContextPath();
	String conf_code = (String)request.getParameter("conf_code");
	String conf_name = (String)request.getParameter("conf_name");

	String howview = (String)request.getParameter("howview"); //전체보기(0) 혹은 쪽보기(1)
	int page_line = 40; 
	int now_page = ParamUtil.getIntParameter(request.getParameter("page"));
	String search = Korean.toKorean((String)request.getParameter("search")); //키워드
	String font = (String)request.getParameter("font");
	int tot_line = ParamUtil.getIntParameter(request.getParameter("tot_line"));	
//	String DocBase = getServletContext().getRealPath("/");
String DocBase = egovframework.com.cmm.util.BaseUtility.getFileBase();

	String inLine = null;

	if (howview == null) howview = "0";	
	if (tot_line == 0) {
		
		ServletInfo Info = new ServletInfo();
		RandomAccessFile fout = null;
		
		try{
			fout = new RandomAccessFile(DocBase+Info.getHtmlPath()+"/"+conf_code+".html", "r");
			while( (inLine=fout.readLine()) != null ){ 
				tot_line = tot_line+1;
			}
			fout.close();
		}catch(FileNotFoundException fileNotException){
			
		}finally{
			try{
				fout.close();
			}catch(Exception ee){
			}
		}
		
	}
	
	int page_num = (int)tot_line / page_line;	 //전체 페이지 갯수 계산
	if (((float)tot_line/(float)page_line!=(int)(tot_line/page_line))) page_num=page_num+1;

	if (now_page == 0) now_page = 1;
	if (search == null) search = "keyword";
	if (font == null) font = "1111601";
%>
<html>
<head>
<title>회의록 보기 - 메뉴</title>
<link rel="stylesheet" href="./include/text.css" type="text/css">
<script language="JavaScript" src="./include/js.js"></script>
<script language="JavaScript" type="text/JavaScript">

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//의회용어사전 새창
function OpenWordWin(szhref,winname) {
        AnotherWin = window.open(szhref,winname, 
        "toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=650,height=420");
}
//단어 찾기 새창
function OpenWin(szhref,winname) {
        AnotherWin = window.open(szhref,winname, 
        "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=200,height=130");
}
//안건목록 새창
function OpenSug(szhref,winname) {
        AnotherWin = window.open(szhref,winname, 
        "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=400,height=300");
}
//인쇄 시작

var da = (document.all) ? 1 : 0; 
var pr = (window.print) ? 1 : 0; 
var mac = (navigator.userAgent.indexOf("Mac") != -1);  
 
function printPage(frame, arg) { 
  if (frame == window) { 
    printThis(); 
  } else { 
    link = arg; // 전역변수  
    printFrame(frame); 
  } 
  return false; 
} 
 
function printThis() { 
  if (pr) { // NS4, IE5 
    window.print(); 
  } else if (da && !mac) { // IE4 (Windows) 
    vbPrintPage(); 
  } else { // 다른 브라우저
    alert("Sorry, your browser doesn't support this feature.");
  } 
} 
 
function printFrame(frame) {
  if (pr && da) { // IE5
    frame.focus();
    window.print();
    link.focus();
  } else if (pr) { // NS4
    frame.print(); 
  } else if (da && !mac) { // IE4 (Windows)
    frame.focus(); 
    setTimeout("vbPrintPage(); link.focus();", 100);
  } else { // 다른 브라우저
    alert("Sorry, your browser doesn't support this feature.");
  } 
} 
 
if (da && !pr && !mac)
  with (document) {
  writeln('<OBJECT ID="WB" WIDTH="0" HEIGHT="0" CLASSID="clsid:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>');
  writeln('<' + 'SCRIPT LANGUAGE="VBScript">');
  writeln('Sub window_onunload');
  writeln('  On Error Resume Next');
  writeln('  Set WB = nothing');
  writeln('End Sub');
  writeln('Sub vbPrintPage');
  writeln('  OLECMDID_PRINT = 6');
  writeln('  OLECMDEXECOPT_DONTPROMPTUSER = 2');
  writeln('  OLECMDEXECOPT_PROMPTUSER = 1');
  writeln('  On Error Resume Next');
  writeln('  WB.ExecWB OLECMDID_PRINT, OLECMDEXECOPT_DONTPROMPTUSER');
  writeln('End Sub');
  writeln('<' + '/SCRIPT>');
}
//인쇄 끝

function send(howview,page) {
	fontsize=form.fontsize.value;	
	fontfamily=form.fontfamily.value;
	lineheight=form.lineheight.value;
	lnk=form.lnk.value;

	if (page == "gopage") {		
		page=form.gopage.value;	
		if (isNaN(page)) {
			alert("숫자만 입력하세요");
			form.gopage.focus();
			form.gopage.select();
			return false;
		}
		if (page < 1) {page = 1;}
		if (page > <%=page_num%>) {page = <%=page_num%>;}
	}
	parent.main.location="viewer_main.jsp?conf_code=<%=conf_code%>&font="+fontsize+fontfamily+lineheight+lnk+"&search=<%=search%>&howview="+howview+"&page="+page+"&tot_line=<%=tot_line%>#a0"; 	location="viewer_top.jsp?conf_code=<%=conf_code%>&font="+fontsize+fontfamily+lineheight+lnk+"&search=<%=search%>&howview="+howview+"&page="+page+"&tot_line=<%=tot_line%>#a0"; 
}

function showlist() {
	window.open("top_apps.jsp?conf_code=<%=conf_code%>","applist","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=450,height=300");
}

function full() {
	window.open("viewer_frame.jsp?conf_code=<%=conf_code%>&KeyWord=<%=search%>", "fs", "fullscreen,scrollbars")
}
</script>
<meta http-equiv='Content-Type' content='text/html; charset=euc-kr'>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/icon0702.gif','images/icon0602.gif','images/icon0502.gif','images/icon0402.gif','images/icon0302.gif','images/icon0102.gif','images/butt0102.gif')">
<table width="100%" height="76" border="0" cellpadding="0" cellspacing="0" background="images/top_back.gif">
  <form name="form" onsubmit="send('<%=howview%>','gopage');return false;">
  <tr>
    <td height="35" colspan="2">
		<table width="920" height="35" border="0" cellpadding="0" cellspacing="0" background="images/top_title_back.gif">
        <tr>
          <td width="15" height="7"></td>
          <td width="700" height="7"></td>
          <td width="65" height="7"></td>
        </tr>
        <tr>
          <td width="15">&nbsp;</td>
          <td class="middle"><strong><%=conf_name%></strong></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height="41" colspan="2">
	<table width="920" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="5">&nbsp;</td>
          <td>
		  <table width="300" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="80">
				<select name="fontsize">
				  <option value=12>글자크기</option>  
				  <option value=11 <%if (font.substring(0,2).equals("11")) out.println("selected");%>>11</option>
				  <option value=12 <%if (font.substring(0,2).equals("12")) out.println("selected");%>>12</option>           
				  <option value=13 <%if (font.substring(0,2).equals("13")) out.println("selected");%>>13</option>
				  <option value=14 <%if (font.substring(0,2).equals("14")) out.println("selected");%>>14</option>           
				  <option value=15 <%if (font.substring(0,2).equals("15")) out.println("selected");%>>15</option>
				  <option value=16 <%if (font.substring(0,2).equals("16")) out.println("selected");%>>16</option>           
				  <option value=17 <%if (font.substring(0,2).equals("17")) out.println("selected");%>>17</option>
				  <option value=18 <%if (font.substring(0,2).equals("18")) out.println("selected");%>>18</option>           
				  <option value=19 <%if (font.substring(0,2).equals("19")) out.println("selected");%>>19</option>
				  <option value=20 <%if (font.substring(0,2).equals("20")) out.println("selected");%>>20</option>
				</select>
				</td>
                <td width="80">
				<select name="fontfamily">
				  <option value="1">글자모양</option> 
				  <option value="1" <%if (font.substring(2,3).equals("1")) out.println("selected");%>>굴림</option>
				  <option value="2" <%if (font.substring(2,3).equals("2")) out.println("selected");%>>궁서</option>       
				  <option value="3" <%if (font.substring(2,3).equals("3")) out.println("selected");%>>돋음</option>
				  <option value="4" <%if (font.substring(2,3).equals("4")) out.println("selected");%>>바탕</option>
				</select>
				</td>
                <td width="70">
				<select name="lineheight">
				  <option value=150>줄간격</option>   
				  <option value=110 <%if (font.substring(3,6).equals("110")) out.println("selected");%>>110</option>
				  <option value=120 <%if (font.substring(3,6).equals("120")) out.println("selected");%>>120</option>       
				  <option value=130 <%if (font.substring(3,6).equals("130")) out.println("selected");%>>130</option>
				  <option value=140 <%if (font.substring(3,6).equals("140")) out.println("selected");%>>140</option>       
				  <option value=150 <%if (font.substring(3,6).equals("150")) out.println("selected");%>>150</option>
				  <option value=160 <%if (font.substring(3,6).equals("160")) out.println("selected");%>>160</option>       
				  <option value=170 <%if (font.substring(3,6).equals("170")) out.println("selected");%>>170</option>
				  <option value=180 <%if (font.substring(3,6).equals("180")) out.println("selected");%>>180</option>       
				  <option value=190 <%if (font.substring(3,6).equals("190")) out.println("selected");%>>190</option>
				  <option value=200 <%if (font.substring(3,6).equals("200")) out.println("selected");%>>200</option>
				</select>
				</td>
                <td width="70">
				<select name="lnk">
				  <option value=1 <%if (font.substring(6,7).equals("1")) out.println("selected");%>>링크보기</option>   
				  <option value=0 <%if (font.substring(6,7).equals("0")) out.println("selected");%>>링크안보기</option>      
				</select>
				</td>
                <td><a href="javascript:send('<%=howview%>','<%=now_page%>');" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image7','','images/butt0102.gif',1)"><img src="images/butt01.gif" name="Image7" width="50" height="20" border="0"></a></td>
              </tr>
          </table>
		  </td>
          <td>&nbsp;</td>
          <td width="70"><a href='javascript:OpenWin("top_fword.jsp?conf_code=<%=conf_code%>&font=<%=font%>&search=<%=search%>&howview=<%=howview%>&page=<%=now_page%>&tot_line=<%=tot_line%>","find")' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image6','','images/icon0102.gif',1)" title="회의록내 단어 검색을 하실 수 있습니다"><img src="images/icon01.gif" name="Image6" width="70" height="41" border="0"></a></td>
          <td width="35">
<% 
Date nowDate = new Date();
SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd");
//if(!((conf_code.equals("0802031002018111900") || conf_code.equals("0802031002018111901") || conf_code.equals("0802032032018112001") || conf_code.equals("0802031002018113002")) && Integer.parseInt(ft.format(nowDate)) < 20190115)) { 
if(!(conf_code.compareTo("0802042032019052300") >= 0 && Integer.parseInt(ft.format(nowDate)) < 20190115)) { 
%>
		  <a HREF="#" onClick="return printPage(parent.main, this)" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image5','','images/icon0202.gif',1)" title="현재 열람하시는 회의록을 인쇄하실 수 있습니다"><img src="images/icon02.gif" name="Image5" width="35" height="31" border="0"></a>
<% } %>
		  </td>	
		  <td width="70"><a HREF="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image20','','images/icon0702.gif',1)" title="현재 열람하시는 회의록을 보이스아이인쇄하실 수 있습니다"><img src="images/icon07.gif" name="Image20" width="70" height="31" border="0" alt='음성정보출력'  style="cursor:pointer" onclick = "window.open('/pub/voice/voice.jsp?code=viewer&view_mod=1&conf_code=<%=conf_code%>&font=<%=font%>&search=<%=search%>&howview=<%=howview%>&page=<%=now_page%>&tot_line=<%=tot_line%>','음성정보출력','height=600,width=670,toolbar=0,menubar=0,scrollbars=1,resizable=1,status=0')" style="cursor:pointer"></a></td>
          <td width="70"><a href="javascript:showlist();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image4','','images/icon0302.gif',1)" title="부록목록을 확인하실 수 있습니다"><img src="images/icon03.gif" name="Image4" width="70" height="41" border="0"></a></td>
          <td width="70">
<% 
//if(!((conf_code.equals("0802031002018111900") || conf_code.equals("0802031002018111901") || conf_code.equals("0802032032018112001") || conf_code.equals("0802031002018113002")) && Integer.parseInt(ft.format(nowDate)) < 20190115)) { 
if(!(conf_code.compareTo("0802042032019052300") >= 0 && Integer.parseInt(ft.format(nowDate)) < 20190115)) { 
%>
		<a href="/datas/hwp/<%=conf_code%>.hwp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image3','','images/icon0402.gif',1)" title="현재 회의록을 한글문서(hwp)로 다운받으실 수 있습니다" target=_blank><img src="images/icon04.gif" name="Image3" width="70" height="41" border="0"></a>
<% } %>
</td>
          <td width="70"><a href='javascript:OpenWordWin("top_dic.jsp","의회용어검색")' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image2','','images/icon0502.gif',1)" title="궁금하신 의회용어의 뜻을 찾으실 수 있습니다"><img src="images/icon05.gif" name="Image2" width="70" height="41" border="0"></a></td>
          <td width="70"><%if (!howview.equals("1")) {%><a href="javascript:send('1','1');" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image8','','images/icon_pageview0102.gif',1)" title="회의록을 페이지별로 보실수 있습니다"><img src="images/icon_pageview01.gif" name="Image8" width="70" height="41" border="0"></a><%}else{%><a href="javascript:send('0','<%=now_page%>');" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image8','','images/icon_allview0102.gif',1)" title="회의록을 한 페이지에서 보실수 있습니다"><img src="images/icon_allview01.gif" name="Image8" width="70" height="41" border="0"></a><%}%></td>
          <td width="70"><a href="javascript:full();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image9','','images/icon_allwindow0102.gif',1)"><img src="images/icon_allwindow01.gif" name="Image9" width="70" height="41" border="0"></a></td>
          <td width="35"><a href="javascript:parent.close();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image1','','images/icon0602.gif',1)" title="현재 회의록 창을 닫습니다"><img src="images/icon06.gif" name="Image1" width="35" height="31" border="0"></a></td>
        </tr>
      </table></td>
  </tr>  
  <!--  -->
  <tr>
    <td height="24" background="images/step_back01.gif">
	<div style="visibility:<%if (!howview.equals("1")) {out.println("hidden");} else {out.println("show");}%>"> 
	<table width="530" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="70">
		  <a href="javascript:send('1','1');" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image10','','images/icon_first0102.gif',1)"><img src="images/icon_first01.gif" name="Image10" width="70" height="24" border="0"></a>
		  </td>
          <td width="70">
		  <a 
		  <%if (now_page == 1) {%>
		  href="javascript:alert('처음 페이지입니다.');"
		  <%}else{%>
		  href="javascript:send('1','<%=now_page-1%>');" title=<%=now_page-1%> 
		  <%}%>
		  onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image11','','images/icon_back0102.gif',1)"><img src="images/icon_back01.gif" name="Image11" width="70" height="24" border="0"></a>
		  </td>
          <td width="70">
		  <a
		  <%if (now_page == page_num) {%>
		  href="javascript:alert('마지막 페이지입니다.');"
		  <%}else{%>
		  href="javascript:send('1','<%=now_page+1%>');" title=<%=now_page+1%> 
		  <%}%>
		  onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image12','','images/icon_next0102.gif',1)"><img src="images/icon_next01.gif" name="Image12" width="70" height="24" border="0"></a>
		  </td>
          <td width="70">
		  <a href="javascript:send('1','<%=page_num%>');" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image13','','images/icon_last0102.gif',1)"><img src="images/icon_last01.gif" name="Image13" width="70" height="24" border="0"></a>
		  </td>
          <td width=70 align=right>
		  &nbsp;<input type="text" name="gopage" value="<%=now_page%>" size=3 maxlength=3 style='BACKGROUND-COLOR: white; BORDER-TOP-WIDTH: 0px; BORDER-BOTTOM: #000000 1px dotted; BORDER-LEFT-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px;text-align:right;text-valign:bottom'> / <%=page_num%>
		  </td>
          <td align=left>&nbsp;				 
		  <a href="javascript:send('<%=howview%>','gopage')" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image14','','images/butt_go0102.gif',1)"><img src="images/butt_go01.gif" name="Image14" width="50" height="24" border="0"></a> 
		  </td>
        </tr>
    </table>
	</div>	
	</td>
	<td width=200><a href="javascript:OpenSug('<%=contextRoot%>/servlet/CSearch?cmd=suglist&conf_code=<%=conf_code%>','SugList')">[안건 목록 보기]</a></td>
  </tr>
  </form>
</table>
</body>
</html>