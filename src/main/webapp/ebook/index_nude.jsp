<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="inc_skin.jsp" %>
<%
	catimage = get_param1(request.getParameter("catimage"),5);
	Dir = get_param1(request.getParameter("Dir"),5);
	String start = check_start(request.getParameter("start"));
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>Electronic Catalog(전자 카탈로그)</title>
<style>
	body{margin:0;}
	form{margin:0;display:inline;}
	.mainmenu{margin:auto;width:1000px;height:40px;background-color:#444444;text-align:center;line-height:40px;}
	.maincont{margin:auto;width:1000px;height:700px;background-color:#ffffff;}
	a{text-decoration:none;color:#F5F5F5;}
	#ifcata{width:1000px;height:700px;border:0;overflow:hidden;}
</style>
<script>
function go_page(str){
	var cataObj = window.frames["ifcata"].document.getElementById("cataSwf");
	if(cataObj != null) cataObj.goPage(str);
}
function go_direct(str){
	var cataObj = window.frames["ifcata"].document.getElementById("cataSwf");
	if(cataObj != null) cataObj.goDirect(str);
}
</script>
</head>

<body>
<div class="mainmenu">
	<a href="javascript:go_page('first');">처음</a> &nbsp;
	<a href="javascript:go_page('prev');">이전</a> &nbsp;
	<a href="javascript:go_page('next');">다음</a> &nbsp;
	<a href="javascript:go_page('last');">끝</a> &nbsp;
	<a href="javascript:go_page('slide');">슬라이드</a> &nbsp;
	<a href="javascript:go_direct('10');">10페이지</a> &nbsp;
	<a href="javascript:go_page('explorer');">탐색기</a> &nbsp;
	<a href="javascript:go_page('print');">인쇄</a> &nbsp;
	<form name="showform" action="">
		<input type="text" name="pageno" size="10">
	</form>
</div>

<div class="maincont">
<iframe id="ifcata" name="ifcata" src="ecatalog.jsp?Dir=<%=Dir%>&amp;start=<%=start%>&amp;catimage=<%=catimage%>"></iframe>
</div>

</body>
</html>
