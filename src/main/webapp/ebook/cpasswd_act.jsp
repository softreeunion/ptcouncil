<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%@ include file="inc_skin.jsp" %>
<%
	catimage = get_param1(request.getParameter("catimage"),5);
	Dir = get_param1(request.getParameter("Dir"),5);

	String start = check_start(request.getParameter("start"));
	String callmode = check_callmode(request.getParameter("callmode"));
	String eclang = check_eclang(request.getParameter("eclang"));
	String userCate = check_cate(request.getParameter("Cate"));
	String um = check_um(request.getParameter("um"),"um");

	String qtxt = get_qtxt3(request.getParameter("qtxt"),50);
	String docu = get_param3(request.getParameter("docu"),10);

	String thpasswd = request.getParameter("thpasswd");
	if(thpasswd == null) thpasswd = "";

	String qstring = "Dir=" + Dir + "&catimage=" + catimage + "&start=" + start + "&callmode=" + callmode + "&eclang=" + eclang;
	if(!userCate.equals("")) qstring += "&Cate=" + userCate;

	pathOfOriCata = webSysDir + "/catImage";
	pathOfCata = webSysDir + "/catImage" + catimage;
	pathOfDir = webSysDir + "/catImage" + catimage + "/" + Dir;

	String catpasswd = "";
	String alert = "";
	String location = "";
	String source;

	File f = new File(pathOfDir);
	if(!f.exists()){
		alert = "디렉토리나 파일이 존재하지 않습니다.";
		location = "ecatalog_cpasswd.jsp?"+qstring+"&docu="+docu;
	}
	else{
		f = new File(pathOfDir + "/ecatalog.txt");
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);
			if(in == -1) continue;

			if(skey.equals("catpasswd")){
				catpasswd = source.substring(in+1);
				break;
			}
		}
		fin.close();
		bin.close();

		if(catpasswd.equals("")){
			location = "ecatalog"+docu+".jsp?"+qstring;
		}
		else if(thpasswd.equals("")){
			alert = "비밀번호가 틀립니다. 다시 입력하십시오!";
			location = "ecatalog_cpasswd.jsp?"+qstring+"&docu="+docu;
		}
		else if(!catpasswd.equals(thpasswd)){
			alert = "비밀번호가 틀립니다. 다시 입력하십시오!";
			location = "ecatalog_cpasswd.jsp?"+qstring+"&docu="+docu;
		}
		else{
			Cookie c_cook_passwd = new Cookie("cook_cpasswd", catpasswd);
			response.addCookie(c_cook_passwd);
	
			// alert = "비밀번호가 맞습니다. eBook을 실행합니다.";
			if(docu.equals("t") || docu.equals("s")) location = "./access/ecatalog"+docu+".jsp?"+qstring;
			else if(docu.equals("exp")) location = "./access/explorer.jsp?"+qstring+"&um="+um;
			else if(docu.equals("cont")) location = "./access/contents.jsp?"+qstring+"&um="+um;
			else if(docu.equals("mark")) location = "./access/bookmark.jsp?"+qstring+"&um="+um;
			else if(docu.equals("combo")) location = "./access/combo.jsp?"+qstring+"&um="+um;
			else if(docu.equals("mail")) location = "./access/email.jsp?"+qstring+"&um="+um;
			else if(docu.equals("search")) location = "./access/search.jsp?"+qstring+"&um="+um+"&qtxt="+qtxt;
			else location = "ecatalog"+docu+".jsp?"+qstring;
		}
	}
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Electronic Catalog(전자 카탈로그) - 비밀번호 확인</title>
<style>
	html, body{margin:0px;overflow:hidden;width:100%;height:100%;}
	*{font-size:12px;}
</style>
<script>
function onload_func(){
	var aw = document.body.clientWidth;
	var ah = document.body.clientHeight;

	document.getElementById("div_cpasswd").style.marginTop = (ah/2 - 25) + "px";

	<%
		if(!alert.equals("")) out.print("alert('"+alert+"');\n");
		out.print("window.location = '"+location+"';\n");
	%>
}
</script>
</head>

<body onload="onload_func();">

<div id="div_cpasswd" style="margin:0 auto;width:250px;height:50px;line-height:30px;">
	<span style="font-weight:bold;color:#cccccc;"><%=alert%></span><br>
</div>

</body>
</html>
