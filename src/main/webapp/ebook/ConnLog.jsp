<%!
public class ConnLog{
	public String today = "";
	public String toStr = "";
	public String streamCharset = "";

	FileInputStream fin;
	BufferedReader bin;

	public ConnLog(String chars){
		streamCharset = chars;
	}

	public void calDate(){
		Calendar aCalendar = Calendar.getInstance();
		int to_year = aCalendar.get(Calendar.YEAR);
		int to_month = aCalendar.get(Calendar.MONTH) + 1;
		int to_day = aCalendar.get(Calendar.DAY_OF_MONTH);

		int to_hour = aCalendar.get(Calendar.HOUR_OF_DAY);
		int to_minute = aCalendar.get(Calendar.MINUTE);
		int to_second = aCalendar.get(Calendar.SECOND);

		today = to_year + twoChar(to_month) + twoChar(to_day);
		toStr = to_year + "-" + twoChar(to_month) + "-" + twoChar(to_day) + " " + twoChar(to_hour) + ":" + twoChar(to_minute) + ":" + twoChar(to_second);
	}

	public String twoChar(int n){
		if(n < 10) return "0" + n;
		else return  "" + n;
	}

	public boolean parse_cookie(String nowDir, String cook_data){
		StringTokenizer st = new StringTokenizer(cook_data, ":");
		while(st.hasMoreTokens()){
			if(st.nextToken().equals(nowDir)) return true;
		}
		return false;
	}

	public void setMoveFile(String path, String fnfrt){
		String moveFile = "";
		int slen = fnfrt.length();

		File f = new File(path + "/log" + catimage + "_" + this.today + ".txt");
		if(!f.exists()){
			f = new File(path);
			File[] folders = f.listFiles();
			for(int i = 0;i < folders.length;i++){
				String temp = folders[i].getName();
				if(folders[i].isDirectory() || temp.length() < slen || temp.charAt(0) == '.' || temp.equals("XULL")) continue;
				if(temp.substring(0,slen).equals(fnfrt)){
					moveFile = temp;
					break;
				}
			}
		}

		if(!moveFile.equals("")) movePrevLogFile(path, moveFile, slen-1);
	}

	public String movePrevLogFile(String path, String moveFile, int plen){
		String source = "";
		String log_folder = moveFile.substring(plen+1,plen+5);
		File f = new File(path + "/" + log_folder);
		if(!f.exists()) f.mkdirs();
	
		int total = 0;
		int mbtotal = 0;
		Hashtable totalHash = new Hashtable();
		Hashtable mobileHash = new Hashtable();
	
		f = new File(path + "/" + moveFile);
		if(!f.exists()) return "can't find move file!";
	
		int Tmax = 0;
		try {
			fin = new FileInputStream(f);
			bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;
				if(source.indexOf("\t") < 0) continue;
	
				String[] aaa = mysplit(source, "\t");
				if(aaa[0].equals("0")){
					total++;
					String[] bbb = get_machine(source);
					if(!bbb[1].equals("d")) mbtotal++;
				}
	
				int s = 0;
				try{ s = Integer.parseInt(aaa[1]); }
				catch(Exception e){	continue; }

				if(s > Tmax) Tmax = s;
			}
			bin.close();
			fin.close();
		}
		catch(IOException e) {
			return "can't open move file!";
		}

		try {
			fin = new FileInputStream(f);
			bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;
				if(source.indexOf("\t") < 0) continue;
		
				String[] aaa = mysplit(source, "\t");
				String s = aaa[1];
				if(totalHash.containsKey(s)){
					String vl = (String)totalHash.get(s);
					int n = 0;

					try{ n = Integer.parseInt(vl); }
					catch(Exception e){	continue; }

					n++;
					totalHash.put(s, Integer.toString(n));
				}
				else{
					totalHash.put(s, "1");
				}

				String[] bbb = get_machine(aaa[4]);
				if(!bbb[1].equals("d")){
					if(mobileHash.containsKey(s)){
						String vl = (String)mobileHash.get(s);
						int n = 0;

						try{ n = Integer.parseInt(vl); }
						catch(Exception e){	continue; }

						n++;
						mobileHash.put(s, Integer.toString(n));
					}
					else{
						mobileHash.put(s, "1");
					}
				}
			}
			bin.close();
			fin.close();
		}
		catch(IOException e) {
			return "can't open move file!";
		}

		StringBuffer strTotal = new StringBuffer("");
		Enumeration enumerationKey1 = totalHash.keys();
		while(enumerationKey1.hasMoreElements()){
			String keys = (String)enumerationKey1.nextElement();
			String vl = (String)totalHash.get(keys);
			strTotal.append(keys + ":" + vl + "|");
		}

		StringBuffer strmbTotal = new StringBuffer("");
		Enumeration enumerationKey2 = mobileHash.keys();
		while(enumerationKey2.hasMoreElements()){
			String keys = (String)enumerationKey2.nextElement();
			String vl = (String)mobileHash.get(keys);
			strmbTotal.append(keys + ":" + vl + "|");
		}

		String pdata = moveFile.substring(plen+1,plen+9) + "\t" + total + "\t" + strTotal.toString() + "\t" + mbtotal + "\t" + strmbTotal.toString();
	
		File f1 = new File(path + "/" + moveFile);
		File f2 = new File(path + "/" + log_folder + "/" + moveFile);
		f1.renameTo(f2);

		try {
			String pathOfLogFile = path + "/" + log_folder + "/" + log_folder + catimage + ".txt";
			StringBuffer cont = new StringBuffer();

			f = new File(pathOfLogFile);
			if(f.exists()){
				FileInputStream fin = new FileInputStream(f);
				BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
				while((source = bin.readLine()) != null){
					source = strTrimEach(source);
					if(source.equals("")) continue;

					cont.append(source).append("\n");
				}
				bin.close();
				fin.close();
			}

			FileOutputStream fos = new FileOutputStream(pathOfLogFile);
			OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
			osw.write(cont.toString() + pdata + "\n");
			osw.close();
		}
		catch(IOException e) {
			return "can't open write file!";
		}

		return "";
	}

	public void recordLog(String path, String s){
		try{
			FileOutputStream fos = new FileOutputStream(path, true);
			OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
			osw.write(s);
			osw.close();
		}
		catch(IOException e) {
		}
	}
}
%>
