<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="admin.jsp" %>
<%
	String[] subar = {"basic", "최상위 분류", "",""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 최상위 분류</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	main.maincnt table tr *:nth-of-type(6n+1){text-align:center;}
	main.maincnt table tr *:nth-of-type(6n+2){text-align:left;}
	main.maincnt table tr *:nth-of-type(6n+3){text-align:left;}
	main.maincnt table tr *:nth-of-type(6n+4){text-align:center;}
	main.maincnt table tr *:nth-of-type(6n+5){text-align:left;}
	main.maincnt table tr *:nth-of-type(6n+6){text-align:center;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function checkup_del(num){
	document.delform.no.value = num;

	if(confirm("삭제하시겠습니까?")) document.delform.submit();
	else return;
}
function check_query(){
	if(trimVal(document.searchform.qtxt.value) == ""){
		alert("검색어를 입력해주세요.");
		document.searchform.qtxt.focus();
		return false;
	}
	return true;
}
</script>
</head>

<body onload="onload_func();">

<%@ include file="inc_menu.jsp" %>
<%
	String qtxt = request.getParameter("qtxt");
	if(qtxt == null) qtxt = "";

	String AddLink = "";
	int colspan = 6;
	int Punit = 30;
	int Pg = 1;
	int Pnum = 0;
	int itemLen = 0;

	StringBuffer list_buf = new StringBuffer();
	f = new File(pathOfOriCata + "/catimage.txt");
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, "\t");
			if(!qtxt.equals("") && aaa[1].indexOf(qtxt) > 0) continue;
			itemLen++;
		}
		fin.close();
		bin.close();

		int[] bvar = new int[4];
		bvar = db_board_start(itemLen, Punit, request.getParameter("Pg"));
		int bb = bvar[0];
		int bf = bvar[1];
		Pg = bvar[2];
		Pnum = bvar[3];

		String befno = "";
		int i = 1;
		int k = 0;
		fin = new FileInputStream(f);
		bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, "\t");			// folder \t dist \t name \t date
			if(!qtxt.equals("") && aaa[2].indexOf(qtxt) == -1) continue;		

			if(bb <= i && i <= bf){
				list_buf.append("<tr>\n");

				if(k > 0) list_buf.append("<td><a href=\"dist_act.jsp?act=seq&amp;Pg="+Pg+"&amp;sd="+befno+":"+aaa[0]+"\">↑</a></td>\n");
				else list_buf.append("<td></td>\n");

				list_buf.append("<td><a href=\"dist_det.jsp?act=mod&amp;catimage="+aaa[0]+"&amp;Pg="+Pg+"\">"+aaa[2]+"</a></td>\n");
				list_buf.append("<td>"+aaa[3]+"</td>\n");
				list_buf.append("<td>"+aaa[0]+"</td>\n");
				list_buf.append("<td><a href=\"pcata_dist.jsp?cataimage="+aaa[0]+"\">");
				list_buf.append("<img src=\""+confServer+"/pix/link.gif\" width=\"5\" height=\"9\" alt=\"\"> 분류로 이동</a></td>\n");
				list_buf.append("<td><a href=\"javascript:checkup_del('"+aaa[0]+"');\">삭제</a></td></tr>\n");

				befno = aaa[0];
			}
			i++;
			k++;
		}
	}

	if(list_buf.toString().equals("")) list_buf.append("<tr><td colspan=\"6\">저장된 최상위 분류가 없습니다.</td></tr>\n");
%>
<main id="maincnt" class="maincnt">

<p class="table-title">최상위 분류 목록 : 전체 <%=itemLen%>개</p>
<table>
	<thead>
	<tr>
		<th>순</th>
		<th>제목</th>
		<th>일자</th>
		<th>catImage(+)</th>
		<th>이동</th>
		<th>삭제</th>
	</tr>
	</thead>
	<tbody>
	<%=list_buf.toString()%>
	</tbody>
</table>
<p class="table-paging"><%=db_board_nextNumber(Pnum, Pg, AddLink, "pcata_dist.jsp")%></p>

<div class="table-qmenu">
	<form name="searchform" method="get" action="pcata_dist.jsp" onsubmit="return check_query()">
	<p><input type="button" value="최상위 분류 추가" onclick="window.location='dist_det.jsp?act=add'"></p>
	<p>
		<input type="search" name="qtxt" size="20" class="px" value="<%=qtxt%>" required>
		<input type="submit" value="제목 검색">
	</p>
	</form>
</div>

<div class="nullform">
<form name="delform" method="post" action="dist_act.jsp" onsubmit="return checkup(this, 'add')">
<input type="hidden" name="no" value="">
<input type="hidden" name="act" value="del">
</form>
</div>

</main>

<%@ include file="inc_copy.jsp" %>
