<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat, java.text.NumberFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	String subar[] = {"addon", "방문자 접속기록", "name",""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 방문자 접속기록(2)</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	main table td{height:21px;text-align:center;}
	img{vertical-align:middle;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
</head>

<body onload="onload_func('connlog.jsp');">
<%@ include file="inc_menu.jsp" %>
<%
	String ccate = request.getParameter("ccate");
	if(ccate == null) ccate = "";

	String p2Code = "";
	if(!ccate.equals("")){
		if(ccate.substring(2,10).equals("00000000")) p2Code = ccate.substring(0,2);
		else p2Code = ccate.substring(0,4);
	}

	Vector aimDir = new Vector();
	Vector mainCate = new Vector();
	StringBuffer ccate_buf = new StringBuffer();
	String path = pathOfCata + "/catakind.txt";
	f = new File(path);
	if(f.exists()){
		Hashtable hash = new Hashtable();
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;
	
			String[] aaa = mysplit(source, "|");
			String t_cate = aaa[0];
			hash.put("cateCond" + t_cate, aaa[1]);
			hash.put("cateName" + t_cate, aaa[2]);
			hash.put("cateDir"  + t_cate, aaa[3]);

			if(t_cate.substring(2,10).equals("00000000")){
				mainCate.addElement(t_cate);
			}
			else{
				String t_cate2 = "";
				if(t_cate.substring(4,10).equals("000000")) t_cate2 = t_cate.substring(0,2) + "00000000";
				else if(t_cate.substring(6,10).equals("0000")) t_cate2 = t_cate.substring(0,4) + "00000000";

				String t_temp2 = hash.containsKey("linkCate" + t_cate2) ? (String)hash.get("linkCate" + t_cate2) : null;

				if(t_temp2 == null) hash.put("linkCate" + t_cate2, t_cate);
				else hash.put("linkCate" + t_cate2, t_temp2 + "|" + t_cate);

				if(p2Code.length() == 2 && t_cate.substring(0,2).equals(p2Code) && !t_cate.substring(2,4).equals("00")){
					aimDir.addElement(aaa[3]);
				}
				else if(p2Code.length() == 4 && t_cate.substring(0,4).equals(p2Code)){
					aimDir.addElement(aaa[3]);
				}
			}
		}
		bin.close();
		fin.close();

		int alen = mainCate.size();
		for(int i = 0;i < alen;i++){
			String t_cate = (String)mainCate.elementAt(i);
			String lesc = t_cate.equals(ccate) ? "selected" : "";

			String t_name = hash.containsKey("cateName"+t_cate) ? (String)hash.get("cateName"+t_cate) : "";
			ccate_buf.append("<option value=\""+t_cate+"\" class=\"cate\" "+lesc+">"+t_name+"</option>\n");

			String t_link = hash.containsKey("linkCate"+t_cate) ? (String)hash.get("linkCate"+t_cate) : "";
			if(t_link == null) continue;

			String[] aaa = mysplit(t_link,"|");
			int ken = aaa.length;
			if(ken == 0) continue;
			for(int k = 0;k < ken;k++){
				String t_cate2 = aaa[k];
				lesc = t_cate2.equals(ccate) ? "selected" : "";

				String t_cond = hash.containsKey("cateCond"+t_cate2) ? (String)hash.get("cateCond"+t_cate2) : "";
				if(t_cond.equals("")) continue;
				String cclass = (t_cond.charAt(2) == 'D') ? "xcata" : "pcata";
				if(t_cond.charAt(0) == '0'){
					t_name = hash.containsKey("cateName"+t_cate2) ? (String)hash.get("cateName"+t_cate2) : "";
					ccate_buf.append("<option value=\""+t_cate2+"\" class=\""+cclass+"\" "+lesc+">&nbsp;&nbsp;"+t_name+"</option>\n");

					String t_link2 = hash.containsKey("linkCate"+t_cate2) ? (String)hash.get("linkCate"+t_cate2) : "";
					if(t_link2 == null) continue;

					String[] bbb = mysplit(t_link2,"|");
					int jen = bbb.length;
					if(jen == 0) continue;
					for(int j = 0;j < jen;j++){
						String t_cate3 = bbb[j];
						t_name = hash.containsKey("cateName"+t_cate3) ? (String)hash.get("cateName"+t_cate3) : "";
						lesc = t_cate3.equals(ccate) ? "selected" : "";
						t_cond = hash.containsKey("cateCond"+t_cate3) ? (String)hash.get("cateCond"+t_cate3) : "";
						if(t_cond.equals("")) continue;
						cclass = (t_cond.charAt(0) == 'D') ? "xcata" : "pcata";
						ccate_buf.append("<option value=\""+t_cate3+"\" class=\""+cclass+"\" "+lesc+">&nbsp;&nbsp;&nbsp;&nbsp;->"+t_name+"</option>\n");
					}
				}
				else{
					ccate_buf.append("<option value=\""+t_cate2+"\" class=\""+cclass+"\" "+lesc+">&nbsp;&nbsp;->"+t_name+"</option>\n");
				}
			}
		}
	}


	// 날짜관련
	String cday = request.getParameter("cday");
	if(cday == null) cday = "";
	if(cday.equals("")){
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		cday = sdf.format(date);
	}

	String cyear = request.getParameter("cyear");
	if(cyear == null) cyear = "";
	if(cyear.equals("")) cyear = cday.substring(0,4);

	String cmonth = request.getParameter("cmonth");
	if(cmonth == null) cmonth = "";
	if(cmonth.equals("")) cmonth = cday.substring(4,6);

	String cym = cyear + cmonth;

	StringBuffer year_buf = new StringBuffer();
	f = new File(pathOfAcc + "/log");
	if(f.exists()){
		File[] folders = f.listFiles();
		for(int i = 0;i < folders.length;i++){
			String temp = folders[i].getName();
			if(!temp.equals(".") && !temp.equals("..") && folders[i].isDirectory()){
				String selc = cyear.equals(temp) ? "selected" : "";
				year_buf.append("<option value=\"").append(temp).append("\" ").append(selc).append(">").append(temp).append("년</option>\n");
			}
		}
	}

	StringBuffer month_buf = new StringBuffer();
	for(int i = 1;i < 13;i++){
		String selc = (Integer.parseInt(cmonth) == i) ? "selected" : "";
		String m = (i < 10) ? "0"+i : ""+i;
		month_buf.append("<option value=\"").append(m).append("\" ").append(selc).append(">").append(i).append("월</option>\n");
	}


	Hashtable mcnt = new Hashtable();						// 전체접속수
	Hashtable bcnt = new Hashtable();						// 모바일접속수

	int total = 0;
	int mtotal = 0;
	int maxcnt = 0;
	int maxmcnt = 0;
	StringBuffer conn_buf = new StringBuffer();
	path = pathOfAcc + "/log/" + cyear + "/" + cyear + cook_catimage + ".txt";

	f = new File(path);
	if(f.exists()){
		if(ccate.equals("")){
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				String[] aaa = mysplit(source, "\t");

				String cymd = aaa[0];
				if(!cymd.substring(0,6).equals(cym)) continue;

				String t_day = cymd.substring(6,8);
				String hashkey = "skey" + t_day;

				int nd = strToInt(aaa[1]);
				if(mcnt.containsKey(hashkey)){
					String s1 = (String)mcnt.get(hashkey);
					nd += strToInt(s1);
					mcnt.put(hashkey, aaa[1]);
				}
				else{
					mcnt.put(hashkey, aaa[1]);
				}

				if(nd > maxcnt) maxcnt = nd;

				if(aaa[3] != null && !aaa[3].equals("")){
					nd = strToInt(aaa[3]);
					if(bcnt.containsKey(hashkey)){
						String s1 = (String)bcnt.get(hashkey);
						nd += strToInt(s1);
						bcnt.put(hashkey, aaa[3]);
					}
					else{
						bcnt.put(hashkey, aaa[3]);
					}
					if(nd > maxmcnt) maxmcnt = nd;
				}
			}
			bin.close();
			fin.close();
		}
		else{
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				String[] aaa = mysplit(source, "\t");

				String cymd = aaa[0];
				if(!cymd.substring(0,6).equals(cym)) continue;

				String t_day = cymd.substring(6,8);
				String hashkey = "skey" + t_day;
				int nd = 0;

				String[] bbb = mysplit(aaa[2], "|");
				for(int k = 0;k < bbb.length;k++){
					String t_conn = bbb[k];
					int in = t_conn.indexOf(":");
					String skey = t_conn.substring(0, in);
					String sval = t_conn.substring(in+1);

					if(aimDir.contains(skey)){
						nd += strToInt(sval);
					}
				}

				if(mcnt.containsKey(hashkey)){
					String s1 = (String)mcnt.get(hashkey);
					nd += strToInt(s1);
					mcnt.put(hashkey, Integer.toString(nd));
				}
				else{
					mcnt.put(hashkey, Integer.toString(nd));
				}

				if(nd > maxcnt) maxcnt = nd;

				if(aaa[3] != null && !aaa[3].equals("")){
					nd = 0;
					String[] ccc = mysplit(aaa[4], "|");
					for(int j = 0;j < ccc.length;j++){
						String t_conn = ccc[j];
						int in = t_conn.indexOf(":");
						String skey = t_conn.substring(0, in);
						String sval = t_conn.substring(in+1);

						if(aimDir.contains(skey)){
							nd += strToInt(sval);
						}
					}

					if(bcnt.containsKey(hashkey)){
						String s1 = (String)bcnt.get(hashkey);
						nd += strToInt(s1);
						bcnt.put(hashkey, Integer.toString(nd));
					}
					else{
						bcnt.put(hashkey, Integer.toString(nd));
					}
					if(nd > maxmcnt) maxmcnt = nd;
				}

			}
			bin.close();
			fin.close();
		}

		NumberFormat nf = NumberFormat.getNumberInstance();
		Calendar cal = Calendar.getInstance();

		cal.set(Integer.parseInt(cyear), Integer.parseInt(cmonth)-1, 1);
		int week = cal.get(Calendar.DAY_OF_WEEK);
		cal.add(Calendar.MONTH, 1);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		int lastday = cal.get(Calendar.DAY_OF_MONTH);

		for(int i = 1;i <= lastday;i++){
			String aclass = "b2tdff";
			if(week == 8) week = 0;
			if(week == 0) aclass = "b2tdf7";

			String t_day = "" + i;
			if(i < 10) t_day = "0" + i;
			String hashkey = "skey" + t_day;

			conn_buf.append("<tr><td>"+i+"일</td>\n");
			if(!mcnt.containsKey(hashkey) || maxcnt == 0){
				conn_buf.append("<td>0</td>\n");
				conn_buf.append("<td>0%</td>\n");
			}
			else{
				String s1 = (String)mcnt.get(hashkey);
				int num = strToInt(s1);
				total += num;

				conn_buf.append("<td>"+nf.format(num)+"</td>\n");
				conn_buf.append("<td>"+(int)(num*100/maxcnt)+"%</td>\n");
			}

			if(!bcnt.containsKey(hashkey) || maxmcnt == 0){
				conn_buf.append("<td>0</td>\n");
				conn_buf.append("<td>0%</td>\n");
			}
			else{
				String s2 = (String)bcnt.get(hashkey);
				int bum = strToInt(s2);
				mtotal += bum;

				conn_buf.append("<td>"+nf.format(bum)+"</td>\n");
				conn_buf.append("<td>"+(int)(bum*100/maxmcnt)+"%</td>\n");
			}
			conn_buf.append("</tr>\n");

			week++;
		}
	}
%>
<main id="maincnt" class="maincnt">

<p class="table-title">숫자로 보기</p>
<p class="h3right"><a href="connlog.jsp">그래프로 보기</a></p>
<br>

<div class="normleft">
<form name="connform" method="get" action="connlog2.jsp">
<span>접속년월</span>
<select name="cyear">
	<%=year_buf%>
</select>
<select name="cmonth" onchange="document.connform.submit();">
	<%=month_buf%>
</select>

<span style="margin-left:20px;">분류/카탈로그</span>
<select name="ccate" onchange="document.connform.submit();">
	<option value="">전체</option>
	<%=ccate_buf%>
</select>
</form>
</div>
<p class="normright"><a href="connlog_xls.jsp?ccate=<%=ccate%>&amp;cyear=<%=cyear%>&amp;cmonth=<%=cmonth%>"><img 
	src="<%=confServer%>/pix/file_xls.gif" width="16" height="16" alt="엑셀파일 다운로드"> 엑셀다운</a></p>

<table>
	<thead>
	<tr>
		<th>일자</th>
		<th>전체접속수</th>
		<th>최고대비</th>
		<th>모바일접속수</th>
		<th>최고대비</th>
	</tr>
	</thead>
	<%=conn_buf%>
	<tr class="sum">
		<td>합</td>
		<td><%=total%></td>
		<td>&nbsp;</td>
		<td><%=mtotal%></td>
		<td>&nbsp;</td>
	</tr>
</table>

<p class="secthelp2">
	- '모바일접속수'는 '전체접속수' 중에서 모바일로 접속한 수입니다.
</p>

</main>

<%@ include file="inc_copy.jsp" %>
