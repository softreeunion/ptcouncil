<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="admin.jsp" %>
<%
	if(cook_dir.equals("")){
		out.println(echo_script(snc_catalog("selfolder"), "basic.jsp", "",""));
		return;
	}

	String[] subar = {"file", "기타파일", "name",""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 기타파일</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	main table tr *:nth-of-type(7n+1){text-align:center;}
	main table tr *:nth-of-type(7n+2){text-align:left;}
	main table tr *:nth-of-type(7n+3){text-align:left;}
	main table tr *:nth-of-type(7n+4){text-align:left;}
	main table tr *:nth-of-type(7n+5){text-align:right;}
	main table tr *:nth-of-type(7n+6){text-align:right;}
	main table tr *:nth-of-type(7n+7){text-align:center;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function checksel(){
	if(confirm("설정값을 저장하시겠습니까?")) return true;
	else return false;
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>
<%
	String downfile = "";
	String downtext = "";
	String reimage = "";
	String retext = "";

	f = new File(pathOfDir + "/ecatalog.txt");
	if(f.exists()){
		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);

			if(skey.equals("downfile")) downfile = source.substring(in+1);
			else if(skey.equals("downtext")) downtext = source.substring(in+1);
			else if(skey.equals("reimage")) reimage = source.substring(in+1);
			else if(skey.equals("retext")) retext = source.substring(in+1);
		}
		fin.close();
		bin.close();
	}

	StringBuffer download_buf = new StringBuffer();
	StringBuffer reimage_buf = new StringBuffer();

	f = new File(pathOfDir);
	if(f.exists()){
		File[] folders = f.listFiles();
		for(int i = 0;i < folders.length;i++){
			String temp = folders[i].getName();
			if(!temp.equals(".") && !temp.equals("..") && !temp.endsWith("txt")){
				if(temp.endsWith("jpg") || temp.endsWith("gif") || temp.endsWith("png")){
					if(temp.equals(reimage)) reimage_buf.append("<option value=\"").append(temp).append("\" selected>").append(temp).append("</option>\n");
					else reimage_buf.append("<option value=\"").append(temp).append("\">").append(temp).append("</option>\n");
				}
				else{
					if(temp.equals(downfile)) download_buf.append("<option value=\"").append(temp).append("\" selected>").append(temp).append("</option>\n");
					else download_buf.append("<option value=\"").append(temp).append("\">").append(temp).append("</option>\n");
				}
			}
		}
	}
%>
<main id="maincnt" class="maincnt">
<form name="downform" action="action.jsp" method="post" onsubmit="return checksel()">

<section>
<h3>다운로드 파일</h3>
<p class="title">선택</p>
<p class="desc">
	<select name="downfile">
		<option value="">파일을 선택하세요</option>
		<%=download_buf.toString()%>
	</select>
</p>
<p class="title">문구</p>
<p class="desc"><input type="text" size="50" name="downtext" class="px" value="<%=downtext%>"></p>
<p class="secthelp2">
- 파일을 선택하시면 좌측하단 카탈로그 이동 콤보박스 옆에 파일의 아이콘과 문구가 나란히 표시됩니다.<br>
- 문구에 색상을 넣기 원하시면 "문구|FF6600"과 같이 [문구]['|' 문자][16진수 웹컬러] 형태로 입력하시면 됩니다.
</p>
</section>

<section>
<h3>대표 이미지</h3>
<p class="title">선택</p>
<p class="desc">
	<select name="reimage">
		<option value="">파일을 선택하세요</option>
		<%=reimage_buf.toString()%>
	</select>
</p>
<p class="title">문구</p>
<p class="desc"><input type="text" size="50" name="retext" class="px" value="<%=retext%>"></p>
<p class="secthelp2">
	- 파일을 선택하지 않았지만 디렉토리 내에 <b>reimage</b>.jpg/gif/png 파일이 있으면 대표 이미지로 인식합니다.<br>
	- 문구를 입력하지 않으시면 기본환경의 '웹브라우저 타이틀바 제목'으로 대체하고 이것마저 없으면 카탈로그 이름으로 대체합니다.
</p>
</section>
<br>

<p><a href="file_add.jsp?kd=cata"><img src="<%=confServer%>/pix/link.gif" width="5" height="9" alt="바로가기"> 파일 업로드 바로가기</a></p>

<div class="submitbtn"><input type="submit" value="저장하기"></div>

<input type="hidden" name="act" value="downfile">
</form>

</main>

<%@ include file="inc_copy.jsp" %>
