<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	if(cook_dir.equals("")){
		out.println(echo_script(snc_catalog("selfolder"), "basic.jsp", "",""));
		return;
	}

	String entire_server = (request.getServerPort() == 80) ? request.getServerName() : request.getServerName() + ":" + request.getServerPort();
	String linkDir = "http://" + entire_server + wasServer;

	String[] subar = {"addon", "CD카탈로그", "name", ""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - CD카탈로그</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	img{vertical-align:middle;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>

<main id="maincnt" class="maincnt">

<section>
<h3>CD카탈로그 생성 프로그램</h3>
<p>
	<a href="<%=webServer%>/cdinfo/makeCD.exe"><img src="<%=confServer%>/pix/exeicon.gif" width="24" height="24" alt="프로그램 아이콘">
	CD카탈로그 생성 프로그램 열기...</a>
</p>
<p class="secthelp2">
	- 열기/저장/취소 창이 뜨면 열기를 선택하십시요.<br>
	- 바로 실행하시거나 저장하신 후 실행시키셔도 됩니다.
</p>
</section>

<section>
<h3>입력항목</h3>
<p>
	카탈로그가 설치된 서버 주소
	<input type="text" name="udmenu" value="<%=linkDir%>" class="px" size="80">
</p>
<p class="secthelp2">텍스트 박스내용을 복사하신 후 프로그램의 입력항목에 붙여 넣으시면 됩니다.</p>
</section>

</main>

<%@ include file="inc_copy.jsp" %>
