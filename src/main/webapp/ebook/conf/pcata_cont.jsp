<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%@ include file="common_func.jsp" %>
<%
	if(cook_dir.equals("")){
		out.println(echo_script(snc_catalog("selfolder"), "basic.jsp", "",""));
		return;
	}

	f = new File(pathOfCata + "/catakind.txt");
	if(!f.exists()){
		out.println(echo_script(snc_catalog("basicconf"), "basic.jsp", "",""));
		return;
	}

	String unifyCond = "";
	String pCode = get_parentCode(cook_cate);

	{
		f = new File(pathOfCata + "/catakind.txt");
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;
			if(source.substring(0,10).equals(pCode)) unifyCond = source.substring(12,13);
		}
		fin.close();
		bin.close();
	}

	String ini1var = "";
	String ini2var = "";
	String pageno = "";
	String subjcomment = "";

	f = new File(pathOfDir + "/ecatalog.txt");
	if(f.exists()){
		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);

			if(skey.equals("ini1var")) ini1var = source.substring(in+1);
			else if(skey.equals("ini2var")) ini2var = source.substring(in+1);
			else if(skey.equals("pageno")) pageno = source.substring(in+1);
		}
		fin.close();
		bin.close();
	}

	if(ini1var.equals("") || pageno.equals("")){
		out.println(echo_script("먼저 기본환경을 설정하셔야 합니다.", "basic.jsp", "",""));
		return;
	}

	char pshowkind = 'N';
	if(ini2var.length() > 9) pshowkind = ini2var.charAt(9);
	char ini1FS = ini1var.charAt(11);

	String[] subar = {"basic", "내용구성", "name",""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 내용구성</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
var cataPages = <%=pageno%>;
var seqDragObj;
function allowDrop(e){
	e.preventDefault();
}
function drag(e){
	seqDragObj = e.target.parentNode;
}
function drop(e){
	e.preventDefault();
	var hitObj = e.target.parentNode;
	hitObj.insertAdjacentHTML('afterend', seqDragObj.outerHTML);
	seqDragObj.outerHTML = "";
}
function setNotUse(obj){
	var figobj = obj.parentNode;
	var imgobj = figobj.getElementsByTagName("img")[0];
	if(obj.className == "normal"){
		obj.className = "notuse";
		figobj.setAttribute("data-cond", "20000");
		imgobj.className = "notuse";
	}
	else{
		obj.className = "normal";
		figobj.setAttribute("data-cond", "10000");
		imgobj.className = "normal";
	}
}
function checkup_seq(){
	var seqobj = document.getElementById('sfileseq');
	var arFigure = seqobj.getElementsByTagName("figure");

	var resstr = "";
	var page = 1;
	for(var i = 0;i < arFigure.length;i++){
		if(page > cataPages) break;
		resstr += arFigure[i].getAttribute('data-cond') + ":" + arFigure[i].getAttribute('data-fname') + "\n";
		page++;
	}

	document.fseqform.cont.value = resstr;

	if(confirm("설정값을 저장하시겠습니까?")) return true;
	else return false;
}
function checkup(){
	if(confirm("설정값을 저장하시겠습니까?")) return true;
	else return false;
}
function xmlnext(fname){
	newWindow(2, 'nw_xmledit.jsp?fname=' + fname, 800, 600, 0, 0, 'xe');
}
function elementdisable(){
	if(document.seqform.fileseq[0].checked || document.seqform.fileseq[2].checked){
		document.seqform.pshowkind.disabled = true;
	}
	else{
		document.seqform.pshowkind.disabled = false;
	}
}
function onload_func2(){
	elementdisable();
	onload_func();
}
</script>
</head>

<body onload="onload_func2();">

<%@ include file="inc_menu.jsp" %>

<main id="maincnt" class="maincnt">
<form name="seqform" action="action.jsp" method="post" onsubmit="return checkup()">
<section>
<h3>페이지 리소스</h3>
<fieldset>
<legend class="title">파일 이름 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명" class="helpshow" 
 onmouseover="show_helpdiv(this, 'help_resource1')" onmouseout="hide_helpdiv();"></legend>
<p class="desc">
	<label><input type="radio" name="fileseq" value="N" id="id_seq1" onclick="elementdisable();" checked required>페이지 번호</label>&nbsp;
	<label><input type="radio" name="fileseq" value="F" id="id_seq2" onclick="elementdisable();" <% if(ini1FS == 'F') out.print("checked"); %>>별도 지정</label>&nbsp;
	<label><input type="radio" name="fileseq" value="X" id="id_seq3" onclick="elementdisable();" <% if(ini1FS == 'X') out.print("checked"); %>>XML 데이터</label>
</p>
</fieldset>
<p class="title">별도 지정 선택시</p>
<p class="desc"><label><input type="checkbox" name="pshowkind" value="F" id="id_pshowkind" <% if(pshowkind == 'F') out.print("checked"); %>>파일 이름으로 페이지 번호 표시</label>
<img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명" class="helpshow" onmouseover="show_helpdiv(this, 'help_resource4')" onmouseout="hide_helpdiv();"></p>

<p id="help_resource1" class="secthelp">
- '페이지 번호'는 페이지 번호를 3자리수에 맞춰 이름을 설정합니다.(1: 001.jpg, 2: 002.jpg, ...)<br>
- '별도 지정'을 선택하고 '페이지 리소스 저장하기'를 누르시면 아래에 지정할 수 있는 내용이 표출됩니다.<br>
- 'XML 데이터'는 도움말을 참고하여 지정한 XML 형식으로 저장합니다.
</p>
<p id="help_resource4" class="secthelp">확장자를 제외한 파일이름을 페이지 번호로 표시합니다.<br>(001.jpg: 1, 028.jpg: 28, ...)</p>

<input type="hidden" name="ini1var" value="<%=ini1var%>">
<input type="hidden" name="ini2var" value="<%=ini2var%>">
<input type="hidden" name="act" value="fileseq">
<input type="hidden" name="unifycond" value="<%=unifyCond%>">
<input type="hidden" name="cate" value="<%=cook_cate%>">
</section>

<div class="submitbtn2"><input type="submit" value="페이지 리소스 저장하기"></div>
</form>

<% if(ini1FS == 'F'){ %>
<form name="fseqform" action="action2.jsp" method="post" onsubmit="return checkup_seq()">
<section>
<h3>별도 지정 내용</h3>
<div id="sfileseq" ondragover="allowDrop(event);">
<%
	String webpath = webServer + "/catImage" + cook_catimage + "/" + cook_dir;
	int seq = 0;
	Vector arFname = new Vector();
	f = new File(pathOfDir+"/fileseq.txt");
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String dist = source.substring(0, in);
			String fname = source.substring(in+1);
			String sfname = "s" + fname;

			File f1 = new File(pathOfDir+"/"+sfname);
			if(f.exists()){
				out.print("<figure id='fq_"+seq+"' draggable='true' ondrop=\"drop(event)\" ondragstart=\"drag(event)\" ");
				out.print("data-fname='"+fname+"' data-cond='" + dist + "'>\n");
				String className = "normal";
				if(dist.charAt(0) == '2') className = "notuse";
				out.print("<img src='" + webpath+"/"+sfname+"' class='"+className+"' alt=''>\n");
				out.print("<figcaption onclick=\"setNotUse(this);\" class='"+className+"'>"+sfname+"</figcaption>\n</figure>\n");
				seq++;
			}
			arFname.addElement(sfname);
		}
		fin.close();
		bin.close();

		f = new File(pathOfDir);
		if(f.exists()){
			File[] folders = f.listFiles();
			for(int i = 0;i < folders.length;i++){
				String temp = folders[i].getName();
				String ext = get_extension(temp);
				if(!temp.equals(".") && !temp.equals("..") && temp.charAt(0) == 's' && (ext.equals("jpg") || ext.equals("png")) && !arFname.contains(temp)){
					String fname = temp.substring(1);
					out.print("<figure id='fq_"+seq+"' draggable='true' ondrop=\"drop(event)\" ondragstart=\"drag(event)\" ");
					out.print("data-fname='"+fname+"' data-cond='10000'>\n");
					out.print("<img src=\""+webpath+"/"+temp+"\" class=\"normal\" alt=''>\n");
					out.print("<figcaption onclick=\"setNotUse(this);\" class=\"normal\">"+temp+"</figcaption>\n</figure>\n");
					seq++;
				}
			}
		}
	}
	else{
		Vector vt = new Vector();
		f = new File(pathOfDir);
		if(f.exists()){
			File[] folders = f.listFiles();
			for(int i = 0;i < folders.length;i++){
				String temp = folders[i].getName();
				String ext = get_extension(temp);
				if(!temp.equals(".") && !temp.equals("..") && temp.charAt(0) == 's' && (ext.equals("jpg") || ext.equals("png"))){
					vt.addElement(temp);
				}
			}
		}

		String list_buf = "";
		int alen = vt.size();
		if(alen > 0){
			Collections.sort(vt);
			for(int i = 0;i < alen;i++){
				String sfname = (String)vt.get(i);
				String fname = sfname.substring(1);
				out.print("<figure id=\"fq_"+seq+"\" draggable=\"true\" ondrop=\"drop(event)\" ondragstart=\"drag(event)\" ");
				out.print("data-fname=\""+fname+"\" data-cond=\"10000\">\n");
				out.print("<img src=\""+webpath+"/"+sfname+"\" class=\"normal\" alt=''>\n");
				out.print("<figcaption onclick=\"setNotUse(this);\" class=\"normal\">"+sfname+"</figcaption>\n</figure>\n");
				seq++;
			}
		}
	}
%>
</div>
<div class="secthelp2">
	- 이미지를 드래그 드롭하여 원하는 위치에 넣습니다.<br>
	- 파일이름을 클릭하면 형태가 변합니다. 빨간색 파일이름은 이미지를 사용하지 않으며 공백으로 처리됩니다.
</div>
</section>

<div class="submitbtn2"><input type="submit" value="별도 지정 내용 저장하기"></div>
<input type="hidden" name="cont" value="">
<input type="hidden" name="act" value="fileseqcont">
</form>

<% } else if(ini1FS == 'X'){ %>
<%
	String xmlDataFile = pathOfDir + "/bookdata.xml";
	int filesize = 0;
	String moddate = "";

	File f1 = new File(xmlDataFile);
	if(f1.exists()){
		filesize = ((int)f1.length()/1000);
		SimpleDateFormat sdf = new SimpleDateFormat(get_date());
		moddate = sdf.format(new Date(f1.lastModified()));
	}
%>
<section>
<h3>XML 데이터 파일</h3>
<dl>
	<dt>파일이름</dt>
	<dd>bookdata.xml</dd>
	<dt>파일크기</dt>
	<dd><%=filesize%> KB</dd>
	<dt>최종변경일</dt>
	<dd><%=moddate%></dd>
	<dt>파일편집</dt>
	<dd><% if(!moddate.equals("")){ %><input type="button" value="xml 직접편집" onclick="xmlnext('bookdata.xml')"><% } %></dd>
</dl>
</section>
<% } %>

</main>

<%@ include file="inc_copy.jsp" %>
