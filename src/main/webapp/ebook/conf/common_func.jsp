<%!
	public String deputvar(String s, String d){
		if(d == null || d.equals("")) return s;
		else return d;
	}

	public String get_ini1var(String s, String t, String[] POST){		// P:post data, F:file data
		String d = "YNYRS"+"NNLNF"+"NNANN"+"NYNNN"+"Y";
		if(s.equals("")) return d;

		StringBuffer ini1var = new StringBuffer();
		ini1var.append((t.substring(0,1).equals("F")) ? deputvar(t.substring(0,1), s.substring(0,1)) : deputvar(d.substring(0,1), POST[0]));
		ini1var.append((t.substring(1,2).equals("F")) ? deputvar(t.substring(1,2), s.substring(1,2)) : deputvar(d.substring(1,2), POST[1]));
		ini1var.append((t.substring(2,3).equals("F")) ? deputvar(t.substring(2,3), s.substring(2,3)) : deputvar(d.substring(2,3), POST[2]));
		ini1var.append((t.substring(3,4).equals("F")) ? deputvar(t.substring(3,4), s.substring(3,4)) : deputvar(d.substring(3,4), POST[3]));
		ini1var.append((t.substring(4,5).equals("F")) ? deputvar(t.substring(4,5), s.substring(4,5)) : deputvar(d.substring(4,5), POST[4]));
		ini1var.append((t.substring(5,6).equals("F")) ? deputvar(t.substring(5,6), s.substring(5,6)) : deputvar(d.substring(5,6), POST[5]));
		ini1var.append((t.substring(6,7).equals("F")) ? deputvar(t.substring(6,7), s.substring(6,7)) : deputvar(d.substring(6,7), POST[6]));
		ini1var.append((t.substring(7,8).equals("F")) ? deputvar(t.substring(7,8), s.substring(7,8)) : deputvar(d.substring(7,8), POST[7]));
		ini1var.append((t.substring(8,9).equals("F")) ? deputvar(t.substring(8,9), s.substring(8,9)) : deputvar(d.substring(8,9), POST[8]));
		ini1var.append((t.substring(9,10).equals("F"))? deputvar(t.substring(9,10), s.substring(9,10)) : deputvar(d.substring(9,10), POST[9]));
		ini1var.append((t.substring(10,11).equals("F")) ? deputvar(t.substring(10,11), s.substring(10,11)) : deputvar(d.substring(10,11), POST[10]));
		ini1var.append((t.substring(11,12).equals("F")) ? deputvar(t.substring(11,12), s.substring(11,12)) : deputvar(d.substring(11,12), POST[11]));
		ini1var.append((t.substring(12,13).equals("F")) ? deputvar(t.substring(12,13), s.substring(12,13)) : deputvar(d.substring(12,13), POST[12]));
		ini1var.append((t.substring(13,14).equals("F")) ? deputvar(t.substring(13,14), s.substring(13,14)) : deputvar(d.substring(13,14), POST[13]));
		ini1var.append((t.substring(14,15).equals("F")) ? deputvar(t.substring(14,15), s.substring(14,15)) : deputvar(d.substring(14,15), POST[14]));
		ini1var.append((t.substring(15,16).equals("F")) ? deputvar(t.substring(15,16), s.substring(15,16)) : deputvar(d.substring(15,16), POST[15]));
		ini1var.append((t.substring(16,17).equals("F")) ? deputvar(t.substring(16,17), s.substring(16,17)) : deputvar(d.substring(16,17), POST[16]));
		ini1var.append((t.substring(17,18).equals("F")) ? deputvar(t.substring(17,18), s.substring(17,18)) : deputvar(d.substring(17,18), POST[17]));
		ini1var.append((t.substring(18,19).equals("F")) ? deputvar(t.substring(18,19), s.substring(18,19)) : deputvar(d.substring(18,19), POST[18]));
		ini1var.append((t.substring(19,20).equals("F")) ? deputvar(t.substring(19,20), s.substring(19,20)) : deputvar(d.substring(19,20), POST[19]));
		ini1var.append((t.substring(20,21).equals("F")) ? deputvar(t.substring(20,21), s.substring(20,21)) : deputvar(d.substring(20,21), POST[20]));

		return ini1var.toString();
	}

	public String get_ini2var(String s, String t, String[] POST){		// P:post data, F:file data
		String d = "NNJNN"+"NNNYN"+"NNNDN"+"XYIYR"+"N";
		if(s.equals("")) return d;

		if(s.length() < d.length()) s += d.substring(s.length(), d.length());

		StringBuffer ini2var = new StringBuffer();
		ini2var.append((t.substring(0,1).equals("F")) ? deputvar(t.substring(0,1), s.substring(0,1)) : deputvar(d.substring(0,1), POST[0]));
		ini2var.append((t.substring(1,2).equals("F")) ? deputvar(t.substring(1,2), s.substring(1,2)) : deputvar(d.substring(1,2), POST[1]));
		ini2var.append((t.substring(2,3).equals("F")) ? deputvar(t.substring(2,3), s.substring(2,3)) : deputvar(d.substring(2,3), POST[2]));
		ini2var.append((t.substring(3,4).equals("F")) ? deputvar(t.substring(3,4), s.substring(3,4)) : deputvar(d.substring(3,4), POST[3]));
		ini2var.append((t.substring(4,5).equals("F")) ? deputvar(t.substring(4,5), s.substring(4,5)) : deputvar(d.substring(4,5), POST[4]));
		ini2var.append((t.substring(5,6).equals("F")) ? deputvar(t.substring(5,6), s.substring(5,6)) : deputvar(d.substring(5,6), POST[5]));
		ini2var.append((t.substring(6,7).equals("F")) ? deputvar(t.substring(6,7), s.substring(6,7)) : deputvar(d.substring(6,7), POST[6]));
		ini2var.append((t.substring(7,8).equals("F")) ? deputvar(t.substring(7,8), s.substring(7,8)) : deputvar(d.substring(7,8), POST[7]));
		ini2var.append((t.substring(8,9).equals("F")) ? deputvar(t.substring(8,9), s.substring(8,9)) : deputvar(d.substring(8,9), POST[8]));
		ini2var.append((t.substring(9,10).equals("F")) ? deputvar(t.substring(9,10), s.substring(9,10)) : deputvar(d.substring(9,10), POST[9]));
		ini2var.append((t.substring(10,11).equals("F")) ? deputvar(t.substring(10,11), s.substring(10,11)) : deputvar(d.substring(10,11), POST[10]));
		ini2var.append((t.substring(11,12).equals("F")) ? deputvar(t.substring(11,12), s.substring(11,12)) : deputvar(d.substring(11,12), POST[11]));
		ini2var.append((t.substring(12,13).equals("F")) ? deputvar(t.substring(12,13), s.substring(12,13)) : deputvar(d.substring(12,13), POST[12]));
		ini2var.append((t.substring(13,14).equals("F")) ? deputvar(t.substring(13,14), s.substring(13,14)) : deputvar(d.substring(13,14), POST[13]));
		ini2var.append((t.substring(14,15).equals("F")) ? deputvar(t.substring(14,15), s.substring(14,15)) : deputvar(d.substring(14,15), POST[14]));
		ini2var.append((t.substring(15,16).equals("F")) ? deputvar(t.substring(15,16), s.substring(15,16)) : deputvar(d.substring(15,16), POST[15]));
		ini2var.append((t.substring(16,17).equals("F")) ? deputvar(t.substring(16,17), s.substring(16,17)) : deputvar(d.substring(16,17), POST[16]));
		ini2var.append((t.substring(17,18).equals("F")) ? deputvar(t.substring(17,18), s.substring(17,18)) : deputvar(d.substring(17,18), POST[17]));
		ini2var.append((t.substring(18,19).equals("F")) ? deputvar(t.substring(18,19), s.substring(18,19)) : deputvar(d.substring(18,19), POST[18]));
		ini2var.append((t.substring(19,20).equals("F")) ? deputvar(t.substring(19,20), s.substring(19,20)) : deputvar(d.substring(19,20), POST[19]));
		ini2var.append((t.substring(20,21).equals("F")) ? deputvar(t.substring(20,21), s.substring(20,21)) : deputvar(d.substring(20,21), POST[20]));

		return ini2var.toString();
	}

	public String get_askinvar(String s1, String t, String[] POST){
		String[] s = new String[10];
		StringTokenizer st = new StringTokenizer(s1, "|");
		int i = 0;
		while(st.hasMoreTokens()){
			s[i] = st.nextToken();
			i++;
		}

		String[] d = {"001","001","001","001","004","002","001","001","001","001"};
		StringBuffer skinvar = new StringBuffer();
		skinvar.append((t.substring(0,1).equals("F")) ? deputvar(d[0], s[0]) : deputvar(d[0], POST[0]));
		skinvar.append((t.substring(1,2).equals("F")) ? "|" + deputvar(d[1], s[1]) : "|" + deputvar(d[1], POST[1]));
		skinvar.append((t.substring(2,3).equals("F")) ? "|" + deputvar(d[2], s[2]) : "|" + deputvar(d[2], POST[2]));
		skinvar.append((t.substring(3,4).equals("F")) ? "|" + deputvar(d[3], s[3]) : "|" + deputvar(d[3], POST[3]));
		skinvar.append((t.substring(4,5).equals("F")) ? "|" + deputvar(d[4], s[4]) : "|" + deputvar(d[4], POST[4]));
		skinvar.append((t.substring(5,6).equals("F")) ? "|" + deputvar(d[5], s[5]) : "|" + deputvar(d[5], POST[5]));
		skinvar.append((t.substring(6,7).equals("F")) ? "|" + deputvar(d[6], s[6]) : "|" + deputvar(d[6], POST[6]));
		skinvar.append((t.substring(7,8).equals("F")) ? "|" + deputvar(d[7], s[7]) : "|" + deputvar(d[7], POST[7]));
		skinvar.append((t.substring(8,9).equals("F")) ? "|" + deputvar(d[8], s[8]) : "|" + deputvar(d[8], POST[8]));
		skinvar.append((t.substring(9,10).equals("F"))? "|" + deputvar(d[9], s[9]) : "|" + deputvar(d[9], POST[9]));

		return skinvar.toString();
	}

	public String get_bskinvar(String s1, String t, String[] POST){
		String[] s = new String[10];
		StringTokenizer st = new StringTokenizer(s1, "|");
		int i = 0;
		while(st.hasMoreTokens()){
			s[i] = st.nextToken();
			i++;
		}

		String[] d = {"001","001","001","001","001","001","001","000","001","001"};
		StringBuffer skinvar = new StringBuffer();
		skinvar.append((t.substring(0,1).equals("F")) ?  deputvar(d[0], s[0]) :       deputvar(d[0], POST[0]));
		skinvar.append((t.substring(1,2).equals("F")) ? "|" + deputvar(d[1], s[1]) : "|" + deputvar(d[1], POST[1]));
		skinvar.append((t.substring(2,3).equals("F")) ? "|" + deputvar(d[2], s[2]) : "|" + deputvar(d[2], POST[2]));
		skinvar.append((t.substring(3,4).equals("F")) ? "|" + deputvar(d[3], s[3]) : "|" + deputvar(d[3], POST[3]));	// descript(gal) <- alert
		skinvar.append((t.substring(4,5).equals("F")) ? "|" + deputvar(d[4], s[4]) : "|" + deputvar(d[4], POST[4]));
		skinvar.append((t.substring(5,6).equals("F")) ? "|" + deputvar(d[5], s[5]) : "|" + deputvar(d[5], POST[5]));
		skinvar.append((t.substring(6,7).equals("F")) ? "|" + deputvar(d[6], s[6]) : "|" + deputvar(d[6], POST[6]));
		skinvar.append((t.substring(7,8).equals("F")) ? "|" + deputvar(d[7], s[7]) : "|" + deputvar(d[7], POST[7]));
		skinvar.append((t.substring(8,9).equals("F")) ? "|" + deputvar(d[8], s[8]) : "|" + deputvar(d[8], POST[8]));
		skinvar.append((t.substring(9,10).equals("F")) ? "|" +deputvar(d[9], s[9]) : "|" + deputvar(d[9], POST[9]));	// search

		return skinvar.toString();
	}

	public int[] db_board_start(int myrow_total, int Punit, String Pg){
		int[] subar = new int[4];
		subar[3] = (int)((myrow_total-1)/Punit) + 1;

		if(Pg == null || Pg == "") subar[2] = 1;
		else subar[2] = Integer.parseInt(Pg);

		subar[0] = (subar[2]-1) * Punit;
		subar[1] = (subar[2] == subar[3]) ? myrow_total : subar[0] + Punit;

		return subar;
	}

	public String db_board_nextNumber(int Pnum, int Pg, String AddLink, String scname){
		int i = 0;
		StringBuffer rstr = new StringBuffer();
		if(Pnum == 1) return "";
		if(!AddLink.equals("")) AddLink = AddLink + "&";

		int f = (int)((Pg-1)/10 + 1) * 10;
		if(f > 10) rstr.append("<a href=\"").append(scname).append("?").append(AddLink).append("Pg=").append(f-10).append("\"><b><-</b></a>&nbsp;&nbsp;");
		for(i = (f-9);i <= Pnum;i++){
			if(i == Pg) rstr.append("[").append(i).append("] &nbsp;");
			else rstr.append("<a href=\"").append(scname).append("?").append(AddLink).append("Pg=").append(i).append("\">[").append(i).append("]</a>&nbsp;");
			if(i >= f) break;
		}
		if(i < Pnum) rstr.append("&nbsp;<a href=\"").append(scname).append("?").append(AddLink).append("Pg=").append(i+1).append("\"><b>-></b>&nbsp;");

		return rstr.toString();
	}

	public String get_accessheader(){
		String jspTag1 = "<" + "%";
		String jspTag2 = "%" + ">";

		String source = jspTag1 + "\n"
		+ "	out.println(\"<script language='Javascript'>\");\n"
		+ "	out.println(\"alert('You can not access this file directly.');\");\n"
		+ "	out.println(\"window.location = 'login.jsp';\");\n"
		+ "	out.println(\"</script>\");\n"
		+ "	out.close();\n"
		+ jspTag2 + "\n";

		return source;
	}

	public String digit3Number(int hi){
		if(hi < 10) return "00" + hi;
		else if(hi < 100) return "0" + hi;
		else return "" + hi;
	}

	public String digit4Number(int hi){
		if(hi < 10) return "000" + hi;
		else if(hi < 100) return "00" + hi;
		else if(hi < 1000) return "0" + hi;
		else return "" + hi;
	}

	public String del_lineBraker(String s){
		s = s.replaceAll("\r","");
		s = s.replaceAll("\n","");
		return s;
	}

	/*public String getMultipartFileName(final Part part) {
		final String partHeader = part.getHeader("content-disposition");
		for(String content : part.getHeader("content-disposition").split(";")){
			if(content.trim().startsWith("filename")){
				return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
			}
		}
		return null;
	}*/
%>
