<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat, java.util.Calendar" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	String Pg = request.getParameter("Pg");
	String act = request.getParameter("act");

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	FileInputStream fin;
	BufferedReader bin;

	String path = pathOfOriCata + "/catimage.txt";

	if(act.equals("add")){
		String name = request.getParameter("name");
		String distmove = request.getParameter("distmove");
		if(distmove == null) distmove = "";
		if(distmove.equals("")) distmove = "N";

		String distvar = distmove + "0000";
		String issuedate = request.getParameter("issuedate");
		String issue_date = "";
		if(!issuedate.equals("") && issuedate.indexOf("-") > -1){
			String[] aaa = mysplit(issuedate, "-");
			Calendar cd = Calendar.getInstance();
			cd.set(Integer.parseInt(aaa[0]), Integer.parseInt(aaa[1])-1, Integer.parseInt(aaa[2]));
			issue_date = sdf.format(new Date(cd.getTimeInMillis())).toString();
		}
		else{
			issue_date = sdf.format(new Date()).toString();
		}

		String qtxt = request.getParameter("qtxt");

		StringBuffer cont = new StringBuffer();
		int dirval = 0;

		f = new File(path);
		if(f.exists()){
			fin = new FileInputStream(f);
			bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				String[] aaa = mysplit(source, "\t");
				if(!aaa[0].equals("")){
					if(Integer.parseInt(aaa[0]) > dirval) dirval = Integer.parseInt(aaa[0]);
				}

				cont.append(source).append("\n");
			}
			fin.close();
			bin.close();
		}

		dirval++;

		String folderval = "catImage" + dirval;

		f = new File(pathOfAcc + "/" + folderval);
		if(!f.exists()){
			f.mkdirs();
		}

		FileOutputStream fos = new FileOutputStream(path);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(dirval + "\t" + distvar + "\t" + name + "\t" + issue_date + "\n");
		osw.write(cont.toString());
		osw.close();

		out.println(echo_script("추가되었습니다.", "pcata_dist.jsp", "",""));
	}
	else if(act.equals("mod")){
		String name = request.getParameter("name");
		String catimage = request.getParameter("catimage");
		String qtxt = request.getParameter("qtxt");

		String issuedate = request.getParameter("issuedate");
		String issue_date = "";
		if(!issuedate.equals("") && issuedate.indexOf("-") > -1){
			String[] aaa = mysplit(issuedate, "-");
			Calendar cd = Calendar.getInstance();
			cd.set(Integer.parseInt(aaa[0]), Integer.parseInt(aaa[1])-1, Integer.parseInt(aaa[2]));
			issue_date = sdf.format(new Date(cd.getTimeInMillis())).toString();
		}


		String distmove = request.getParameter("distmove");
		if(distmove == null) distmove = "";
		if(distmove.equals("")) distmove = "N";

		String distvar = distmove + "0000";

		StringBuffer cont = new StringBuffer();
		f = new File(path);
		fin = new FileInputStream(f);
		bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, "\t");
			if(aaa[0].equals(catimage)){
				cont.append(aaa[0]+"\t"+distvar+"\t"+name+"\t"+issue_date+"\n");
			}
			else{
				cont.append(source).append("\n");
			}
		}
		fin.close();
		bin.close();

		FileOutputStream fos = new FileOutputStream(path);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.close();

		out.println(echo_script("변경되었습니다.", "dist_det.jsp?act=mod&catimage="+catimage+"&Pg="+Pg+"&qtxt="+qtxt, "",""));
	}
	else if(act.equals("seq")){
		String sd = request.getParameter("sd");
		String[] aftcate = mysplit(sd, ":");		// from : to

		String prev_catimage = aftcate[0];
		String next_catimage = aftcate[1];

		String prev_cont = "";
		String next_cont = "";

		f = new File(path);
		fin = new FileInputStream(f);
		bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, "\t");				// dir \t dist \t name \t date
			if(aaa[0].equals(prev_catimage)) prev_cont = source + "\n";
			else if(aaa[0].equals(next_catimage)) next_cont = source + "\n";
		}
		fin.close();
		bin.close();

		StringBuffer cont = new StringBuffer();
		fin = new FileInputStream(f);
		bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, "\t");				// dir \t dist \t name \t date
			if(aaa[0].equals(prev_catimage)){
				cont.append(next_cont).append(prev_cont);
			}
			else if(aaa[0].equals(next_catimage)){
			}
			else{
				cont.append(source).append("\n");
			}
		}
		fin.close();
		bin.close();

		FileOutputStream fos = new FileOutputStream(path);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.close();

		out.println(echo_script("순서가 변경되었습니다.", "pcata_dist.jsp?Pg="+Pg, "",""));
	}
	else if(act.equals("del") || act.equals("confdel")){
		String catimage = request.getParameter("no");

		if(act.equals("del")){
			int k = 0;
			f = new File(pathOfAcc + "/catImage" + catimage);
			File[] xolders = f.listFiles();
			for(int i = 0;i < xolders.length;i++){
				String temp = xolders[i].getName();
				if(!temp.equals(".") && !temp.equals("..")) k++;
			}

			if(k > 0){
				out.print("<!DOCTYPE html>\n");
				out.print("<html lang=\"ko\">\n");
				out.print("<head>\n");
				out.print("<meta charset=\"utf-8\">\n");
				out.print("<title>eCatalog conf - 삭제 확인</title>\n");
				out.print("</head>\n");
				out.print("<body>\n");
				out.print("<script>\n");
				out.print("if(confirm('폴더내 파일이 존재합니다. 계속하시겠습니까?')) window.location = \"dist_act.jsp?act=confdel&no="+catimage+"\";\n");
				out.print("else window.location = \"pcata_dist.asp\";\n");
				out.print("</script>\n");
				out.print("</body>\n");
				out.print("</html>\n");
				return;
			}
		}

		StringBuffer cont = new StringBuffer();
		f = new File(path);
		fin = new FileInputStream(f);
		bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, "\t");				// dir \t dist \t name \t date
			if(!aaa[0].equals(catimage)){
				cont.append(source).append("\n");
			}
		}
		fin.close();
		bin.close();

		FileOutputStream fos = new FileOutputStream(path);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.close();

		f = new File(pathOfAcc + "/catImage" + catimage);
		File[] xolders = f.listFiles();
		for(int i = 0;i < xolders.length;i++){
			if(xolders[i].isFile()){
				xolders[i].delete();
			}
		}

		f.delete();			// delete directory

		out.println(echo_script("삭제되었습니다.", "pcata_dist.jsp?Pg="+Pg, "",""));
	}
%>
