<?php
	require "admin.php";
	require "../send/mailcont.php";

	$act = isset($_POST['act']) ? $_POST['act'] : "";
	$catimage = isset($_POST['catimage']) ? $_POST['catimage'] : "";			// catimage
	$cook_id = isset($_POST['cook_id']) ? $_POST['cook_id'] : "";				// mblog
	$cook_dir = isset($_POST['cook_dir']) ? $_POST['cook_dir'] : "";
	$mailadapt = isset($_POST['mailadapt']) ? $_POST['mailadapt'] : "";		// mail
	$logoadapt = isset($_POST['logoadapt']) ? $_POST['logoadapt'] : "";		// logo

	$entire_server = ($_SERVER['SERVER_PORT'] == "80") ? $_SERVER['SERVER_NAME'] : $_SERVER['SERVER_NAME'].":".$_SERVER['SERVER_PORT'];
	$linkUrl = "http://".$entire_server.str_replace("/conf/nw_mailpre.php","",$_SERVER['SCRIPT_NAME']);
	$linkQstring = $cook_dir."&catimage=".$catimage;

	if($act == "mail_def"){
		$confpath = $pathOfOriCata."/mail.txt";
		$handle = opendir($pathOfOriCata);
		while($file = readdir($handle)){
			if(ereg("^\.",$file)) continue;
			else if(ereg("^malogo",$file)){
				$logopath = "catImage/".$file;
				break;
			}
		}
		closedir($handle);
	}
	else{
		if(file_exists($pathOfCata."/".$cook_dir."/mail.txt")){
			$confpath = $pathOfCata."/".$cook_dir."/mail.txt";
		}
		else{
			$act_string = "데이터가 없어서 페이지를 표시할 수 없습니다.";
			if($lolang == "english") $act_string = "The data file does not exist so that the document will not be displayed.";
			echo_script($act_string,"","","window.close();\n");
			exit();
		}

		$logopath = "";
		$tdir = ($logoadapt != "") ? $logoadapt : $cook_dir;
		$path = $pathOfCata."/".$tdir;
		$handle = opendir($path);
		while($file = readdir($handle)){
			if(ereg("^\.",$file)) continue;
			else if(ereg("^malogo",$file)){
				$logopath = "catImage".$catimage."/".$tdir."/".$file;
				break;
			}
		}
		closedir($handle);

		if($logopath == ""){
			$handle = opendir($pathOfOriCata);
			while($file = readdir($handle)){
				if(ereg("^\.",$file)) continue;
				else if(ereg("^malogo",$file)){
					$logopath = "catImage/".$file;
					break;
				}
			}
			closedir($handle);
		}
	}

	$j = 0;
	$mailskin = "";
	$adminCont = "";
	if(file_exists($confpath)){
		$fp = fopen($confpath, "r");
		while($source = fgets($fp,1024)){
			if(ereg("^mailskin:",$source)) $mailskin = substr(trim($source),9);
			else if(ereg("^copyright:",$source)) $copyCont = substr(trim($source),11);
			else if(trim($source) == "") $j = 1;
			else if($j == 1) $adminCont .= $source."<br>";
		}
		fclose($fp);
	}

	$userCont = "고객의 메일 내용";
	if($lolang == "english") $userCont = "Mail contents of customer";

	if($mailskin == "" || strcmp($mailskin,"001") == 0){
		echo get_mailBody($linkUrl, $linkQstring, $logopath);
	}
	else{
		echo get_mailBody2($linkUrl, $linkQstring, $logopath);
	}
?>
