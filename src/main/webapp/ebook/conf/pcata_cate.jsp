<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="admin.jsp" %>
<%
	String cate = request.getParameter("cate");
	if(cate == null){
		out.println(echo_script("", "basic.jsp", "",""));
		return;
	}
	if(!cate.equals("") && check_numalphaVar(cate, 10) == false){
		out.print("cate is not acceptable!");
		return;
	}

	cook_cate = cate;
	String[] subar = {"basic", "분류 설정", "basic",""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 분류설정</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	main.maincnt table tr *:nth-of-type(4n+1){text-align:center;}
	main.maincnt table tr *:nth-of-type(4n+2){text-align:left;}
	main.maincnt table tr *:nth-of-type(4n+3){text-align:right;}
	main.maincnt table tr *:nth-of-type(4n+4){text-align:center;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function checkup(Object, kd){
	if(confirm("설정값을 저장하시겠습니까?")) return true;
	else return false;
}
function order_set(obj, val){
	var tsd = document.cateform.sd.value;
	var cont = "";

	if(obj.checked == false){
		var aaa = tsd.split(",");
		for(i = 0;i < aaa.length;i++){
			if(aaa[i] != val) cont += (cont == "") ? aaa[i] : "," + aaa[i];
		}
		document.cateform.sd.value = cont;
	}
	else{
		if(tsd == "") document.cateform.sd.value = val;
		else document.cateform.sd.value += "," + val;
	}
}
function play_audio(fname){
	document.getElementById('audioctrl').style.display = "block";
	var audioPlayer = document.getElementById('audioplayer');
	audioPlayer.setAttribute('src', fname);
	audioPlayer.play();
}
function hide_audioctrl(){
	document.getElementById('audioctrl').style.display = "none";
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>
<%
	String catedata = "";
	String unifdata = "";
	f = new File(pathOfCata + "/category.txt");
	if(f.exists()){
		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;
			if(source.substring(0,10).equals(cate)){
				String stitle = source.substring(11,15);
				if(stitle.equals("cate")) catedata = source;
				else if(stitle.equals("unif")) unifdata = source;
			}
		}
		fin.close();
		bin.close();
	}

	char fmove = '0';
	char onemove = '0';
	String bascadapt = "";
	String logoadapt = "";
	String mailadapt = "";
	String musicadapt = "";

	if(!catedata.equals("")){
		String[] aaa = mysplit(catedata,"\t");
		fmove = aaa[2].charAt(0);
		onemove = aaa[2].charAt(1);
		bascadapt = aaa[3];
		logoadapt = aaa[4];
		mailadapt = aaa[5];
		musicadapt = aaa[6];
	}

	StringBuffer selclogo_buf = new StringBuffer();
	selclogo_buf.append("<option value=''>개별적용</option>\n");
	StringBuffer selcmail_buf = new StringBuffer();
	selcmail_buf.append("<option value=''>개별적용</option>\n");
	StringBuffer selcbasc_buf = new StringBuffer();
	selcbasc_buf.append("<option value=''>개별적용</option>\n");

	String vartemp = "";
	String serviceCond = "";
	String unifyCond = "";
	String firstDir = "";

	String path = pathOfCata + "/catakind.txt";
	f = new File(path);
	if(f.exists()){
		int cateStep = get_cateStep(cate);
		int plen = 2 * cateStep;
		String pnCode = cate.substring(0,plen);

		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String tcate = source.substring(0,10);
			if(tcate.equals(cate)){
				serviceCond = source.substring(11,12);
				unifyCond = source.substring(12,13);
			}
			else if(source.substring(0,plen).equals(pnCode) && !source.substring(13,14).equals("0")){
				String[] aaa = mysplit(source, "|");		// cate|setting|name|etc
				if(i == 0) firstDir = aaa[3];

				if(logoadapt.equals(aaa[3])) selclogo_buf.append("<option value=\"").append(aaa[3]).append("\" selected>").append(aaa[2]).append("</option>\n");
				else selclogo_buf.append("<option value=\"").append(aaa[3]).append("\">").append(aaa[2]).append("</option>\n");

				if(mailadapt == null || mailadapt.equals(aaa[3])) selcmail_buf.append("<option value=\"").append(aaa[3]).append("\" selected>").append(aaa[2]).append("</option>\n");
				else selcmail_buf.append("<option value=\"").append(aaa[3]).append("\">").append(aaa[2]).append("</option>\n");

				if(bascadapt == null || bascadapt.equals(aaa[3])) selcbasc_buf.append("<option value=\"").append(aaa[3]).append("\" selected>").append(aaa[2]).append("</option>\n");
				else selcbasc_buf.append("<option value=\"").append(aaa[3]).append("\">").append(aaa[2]).append("</option>\n");

				i++;
			}
		}
		fin.close();
		bin.close();
	}

	String sd;
	String[] amusic;
	if(musicadapt.equals("") && musicadapt.equals("none")){
		sd = "";
		amusic = new String[1];
		amusic[0] = "";
	}
	else{
		sd = musicadapt;
		amusic = mysplit(musicadapt,",");
	}

	path = pathOfAcc + "/media";
	String conf_file = path + "/media.txt";

	int mfile_qty = 0;				// quantity of music files
	StringBuffer sbuf = new StringBuffer();
	f = new File(conf_file);
	if(f.exists()){
		int i = 1;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, "|");		// cate|setting|name|etc

			String chec = "";
			if(arraySearch(aaa[2], amusic) != -1) chec = "checked";

			File f1 = new File(path + "/" + aaa[2]);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			String dcont = sdf.format(new Date(f1.lastModified()));

			sbuf.append("<tr>\n");
			sbuf.append("<td><input type=\"checkbox\" name=\"asd").append(i).append("\" value=\"");
			sbuf.append(aaa[2]).append("\" ").append(chec).append(" onclick=\"order_set(this, '").append(aaa[2]).append("');\"></td>\n");
			sbuf.append("<td><a href=\"javascript:play_audio('").append(path).append("/").append(aaa[2]).append("');\">").append(aaa[1]).append("</a></td>\n");
			sbuf.append("<td>").append((int)f1.length()/1000).append(" KB</td>\n");
			sbuf.append("<td>").append(dcont).append("</td></tr>\n");
			i++;
		}
		fin.close();
		bin.close();
		mfile_qty = i;
	}

	if(sbuf.toString().equals("")){
		sbuf.append("<tr><td colspan=\"4\">음악 파일이 없습니다.</td></tr>");
	}
%>
<main id="maincnt" class="maincnt">
<form name="cateform" method="post" action="action2.jsp" onsubmit="return checkup()">

<section>
<h3>기본 정보</h3>
<p class="title">코드값</p>
<p class="desc"><%=cate%></p>
<p class="title">서비스 상태 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_basic1')" onmouseout="hide_helpdiv();"></p>
<p class="desc"><label><input type="checkbox" name="fservice" value="D" id="id_fservice" <% if(serviceCond.equals("D")) out.print("checked"); %>>서비스 중지</label></p>
<p class="title">폴더 통합</p>
<p class="desc"><label><input type="checkbox" name="unify" value="U" id="id_unify" <% if(unifyCond.equals("U")) out.print("checked");%>>분류 내 폴더를 하나로 묶음</label>
<% if(unifyCond.equals("U")){ %><input type="button" value="묶음 카탈로그 보기"
  onclick="ecatalog('<%=scname%>','fixed','<%=firstDir%>&catimage=<%=cook_catimage%><%=mblogLink%>&callmode=admin_unify','')">
<% } %>
</p>
<p class="title">폴더간 이동 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_basic2')" onmouseout="hide_helpdiv();"></p>
<p class="desc"><label><input type="checkbox" name="fmove" value="1" id="id_fmove" <% if(fmove == '1') out.print("checked"); %>>이동 안함</label></p>
<p id="help_basic1" class="secthelp">하위에 있는 모든 분류/카탈로그에 적용됩니다.</p>
<p id="help_basic2" class="secthelp">이동 안함을 선택하시면 '디자인/스킨 > 스킨 > 이동 콤보박스' 선택은 무시됩니다.</p>
</section>

<section>
<h3>전체/개별 적용</h3>
<p class="title">기본환경</p>
<p class="desc"><select name="bascadapt"><%=selcbasc_buf%></select></p>
<p class="title">로고</p>
<p class="desc"><select name="logoadapt"><%=selclogo_buf%></select></p>
<p class="title">메일</p>
<p class="desc"><select name="mailadapt"><%=selcmail_buf%></select></p>
<p class="secthelp2">
	- 기본환경에서 '개별적용' 이외의 폴더를 선택하시면 선택된 폴더의 페이지 수를 제외한 모든 설정이 분류 내의 폴더에 적용됩니다.<br>
	- 로고/메일은 '개별적용'을 선택하실 경우 폴더 내에 파일이 실제로 있을 경우에만 적용됩니다.
</p>
</section>

<section>
<h3>음악</h3>
<p class="title">전체 적용 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_music1')" onmouseout="hide_helpdiv();"></p>
<p class="desc"><label><input type="checkbox" name="music_notapp" value="1" id="id_mnotapp"
  <% if(musicadapt.equals("none")) out.print("checked"); %>>분류 내에는 음악을 적용하지 않음</label></p>
<p class="title">선택 적용 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_music2')" onmouseout="hide_helpdiv();"></p>
<div class="desc3">
<table>
	<colgroup>
		<col style="width:7%;"><col style="width:56%;"><col style="width:10%;"><col style="width:27%;">
	</colgroup>
	<thead>
	<tr>
		<th>선택</th>
		<th>제목</th>
		<th>크기</th>
		<th>최종변경일</th>
	</tr>
	</thead>
	<%=sbuf.toString()%>
</table>
</div>
<div id="audioctrl"><audio id="audioplayer" controls ondblclick="hide_audioctrl();">Your browser does not support the audio element.</audio></div>
<p id="help_music1" class="secthelp">체크하시면 '선택 적용'에서 선택된 파일들은 무시됩니다.</p>
<p id="help_music2" class="secthelp">클릭한 순서도 저장됩니다.</p>
</section>

<div class="submitbtn"><input type="submit" value="저장하기"></div>

<input type="hidden" name="pre_fservice" value="<%=serviceCond%><%=unifyCond%>">
<input type="hidden" name="mfile_qty" value="<%=mfile_qty%>">
<input type="hidden" name="cate" value="<%=cate%>">
<input type="hidden" name="sd" value="<%=sd%>">
<input type="hidden" name="act" value="catebasic">
</form>

</main>

<%@ include file="inc_copy.jsp" %>
