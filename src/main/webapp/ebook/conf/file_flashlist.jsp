<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="admin.jsp" %>
<%
	String[] subar = {"file", "링크파일", "", "flash"};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 링크파일</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	main table tr *:nth-of-type(6n+1){text-align:center;}
	main table tr *:nth-of-type(6n+2){text-align:left;}
	main table tr *:nth-of-type(6n+3){text-align:center;}
	main table tr *:nth-of-type(6n+4){text-align:left;}
	main table tr *:nth-of-type(6n+5){text-align:right;}
	main table tr *:nth-of-type(6n+6){text-align:center;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function conf_del(num){
	if(confirm("파일을 삭제하시겠습니까?")){
		document.delform.dir.value = document.flashform["dir"+num].value;
		document.delform.submit();
	}
}
function checkup(num){
	if(document.flashform["title"+num].value == ""){
		alert("타이틀을 입력해주세요.");
		return;
	}

	if(confirm("변경하시겠습니까?")){
		document.modform.title.value = document.flashform["title"+num].value;
		document.modform.flw.value = document.flashform["flw"+num].value;
		document.modform.flh.value = document.flashform["flh"+num].value;
		document.modform.dir.value = document.flashform["dir"+num].value;
		document.modform.submit();
	}
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>
<%
	String AddLink = "";
	int Punit = 20;
	int Pg = 0;
	int Pnum = 0;

	int itemLen = 0;
	StringBuffer sbuf = new StringBuffer();
	f = new File(pathOfAcc + "/flash/flash.txt");
	if(f.exists()){
		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;
			i++;
		}
		fin.close();
		bin.close();

		itemLen = i;

		int[] bvar = new int[4];
		bvar = db_board_start(itemLen, Punit, request.getParameter("Pg"));
		int bb = bvar[0];
		int bf = bvar[1];
		Pg = bvar[2];
		Pnum = bvar[3];

		int ki = 0;
		i = 1;
		String path = pathOfAcc + "/flash";
		fin = new FileInputStream(f);
		bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			if(bb < i && i <= bf){
				String[] aaa = mysplit(source, ":");

				File f1 = new File(path + "/" + aaa[2]);
				int fsize = (int)f1.length()/1000;

				sbuf.append("<tr><td>").append(i).append("</td>\n");
				sbuf.append("<td><input type=\"text\" size=\"30\" name=\"title").append(ki).append("\" value=\"").append(aaa[1]).append("\" class=\"qx\">/\n");
				sbuf.append("<input type=\"text\" size=\"4\" name=\"flw").append(ki).append("\" value=\"").append(aaa[3]).append("\" class=\"qx\"> x ");
				sbuf.append("<input type=\"text\" size=\"4\" name=\"flh").append(ki).append("\" value=\"").append(aaa[4]).append("\" class=\"qx\">\n");
				sbuf.append("<input type=\"hidden\" name=\"dir").append(ki).append("\" value=\"").append(aaa[0]).append("\">");
				sbuf.append("<input type=\"button\" value=\"변경\" onclick=\"checkup('").append(ki).append("')\">&nbsp;</td>\n");
				sbuf.append("<td><a href=\"file_flashview.jsp?fname=").append(aaa[2]).append("&amp;fsize=").append(aaa[3]).append("x").append(aaa[4]).append("\">보기</a></td>\n");
				sbuf.append("<td>").append(aaa[2]).append("</td>\n");
				sbuf.append("<td>").append(fsize).append(" KB</td>\n");
				sbuf.append("<td><a href=\"#\" onclick=\"conf_del('").append(ki).append("');\">삭제</a></td></tr>\n");
			}
			i++;
			ki++;
		}
		fin.close();
		bin.close();
	}

	if(sbuf.toString().equals("")) sbuf.append("<tr><td colspan=\"7\" class=\"b2tdff alignctr\">파일이 없습니다.</td></tr>");
%>
<main id="maincnt" class="maincnt">
<form name="flashform" method="post" action="#">
<p class="table-title">링크 파일 목록 : 전체 <%=itemLen%>개</p>
<table>
	<colgroup>
		<col style="width:7%;"><col style="width:50%;"><col style="width:10%;"><col style="width:15%;"><col style="width:12%;"><col style="width:6%;">
	</colgroup>
	<thead>
	<tr>
		<th>순서</th>
		<th>제목/(너비x높이)</th>
		<th>보기</th>
		<th>파일이름</th>
		<th>크기</th>
		<th>삭제</th>
	</tr>
	</thead>
	<%=sbuf.toString()%>
</table>
<p class="table-paging"><%=db_board_nextNumber(Pnum, Pg, AddLink, "file_flashlist.jsp")%></p>
<input type="hidden" name="act" value="mdel">
<input type="hidden" name="kd" value="flash">
</form>

<div class="table-qmenu">
<p><a href="file_add.jsp?kd=<%=subar[3]%>"><img src="<%=confServer%>/pix/fileadd.png" width="59" height="19" alt="파일추가"></a></p>
<p></p>
</div>

<br>

<div id="movieplay"></div>

<div class="nullform">
<form name="modform" method="post" action="media_act.jsp">
<input type="hidden" name="title" value="">
<input type="hidden" name="flw" value="">
<input type="hidden" name="flh" value="">
<input type="hidden" name="dir" value="">
<input type="hidden" name="act" value="mod">
<input type="hidden" name="kd" value="flash">
</form>

<form name="delform" method="post" action="media_act.jsp">
<input type="hidden" name="dir" value="">
<input type="hidden" name="act" value="del">
<input type="hidden" name="kd" value="flash">
</form>
</div>

</main>

<%@ include file="inc_copy.jsp" %>
