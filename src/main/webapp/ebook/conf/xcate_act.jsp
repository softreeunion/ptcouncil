<%@ page import="java.io.*, java.util.*, com.oreilly.servlet.MultipartRequest, com.oreilly.servlet.multipart.DefaultFileRenamePolicy" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="unify_func.jsp" %>
<%!
	/* category \t cate \t [fmove][onemove] \t bascadapt \t logoadapt \t mailadapt \t media_file
	   category \t unif \t totalpages \t folder \t pageno \t fileseq \t alink \t pagesound \t picdata
	   substr($aaa[1],2,1) substr($source,13,1): 0(category)/C(catalog)/G(gallery)
	*/

	public String nextChar(String str){
		if(str == null) return "01";
		if(str.equals("")) return "01";

		char an = str.charAt(0);
		char bn = str.charAt(1);

		if(bn == 90) return nextAscii(an) + "1";
		else return str.substring(0,1) + nextAscii(bn);
	}

	public char nextAscii(int ac){		// 48 ~ 57 : 0 ~ 9 | 65 ~ 90 : A ~ Z | 97 ~ 122 : a ~ z
		if(ac == 57) ac = 65;
		//else if(ac == 90) ac = 97;
		else ac = ac + 1;

		return (char)ac;
	}

	public String biggerChar(String s1, String s2){
		if(s1.equals("") || s1.equals(s2)) return s2;
		if(s2.equals("")) return s1;

		char s1a = s1.charAt(0);
		char s1b = s1.charAt(1);
		char s2a = s2.charAt(0);
		char s2b = s2.charAt(1);

		String res = "";
		if(s2a > s1a) res = s2;
		else if(s2a < s1a) res = s1;
		else if(s2a == s1a){
			if(s2b > s1b) res = s2;
			else if(s2b < s1b) res = s1;
		}
		return res;
	}

	public String getMaxCate(String cate, Vector acate){
		String resChar = "";
		int alen = acate.size();
		if(cate.equals("0000000000")){							// Root에 추가
			for(int i = 0;i < alen;i++){
				String t_cate = (String)acate.elementAt(i);
				if(t_cate.substring(2,10).equals("00000000")){
					resChar = biggerChar(resChar,t_cate.substring(0,2));
				}
			}
			if(resChar.equals("")) return "0100000000";
			else return nextChar(resChar) + "00000000";
		}
		else if(cate.substring(2,10).equals("00000000")){		// 1차 분류에 추가
			String frtcate = cate.substring(0,2);
			for(int i = 0;i < alen;i++){
				String t_cate = (String)acate.elementAt(i);
				if(t_cate.substring(0,2).equals(frtcate) && t_cate.substring(4,10).equals("000000")){
					resChar = biggerChar(resChar,t_cate.substring(2,4));
				}
			}
			if(resChar.equals("")) return frtcate + "01000000";
			else return frtcate + nextChar(resChar) + "000000";
		}
		else if(cate.substring(4,10).equals("000000")){			// 2차 분류에 추가
			String frtcate = cate.substring(0,4);
			for(int i = 0;i < alen;i++){
				String t_cate = (String)acate.elementAt(i);
				if(t_cate.substring(0,4).equals(frtcate) && t_cate.substring(6,10).equals("0000")){
					resChar = biggerChar(resChar,t_cate.substring(4,6));
				}
			}
			if(resChar.equals("")) return frtcate + "010000";
			else return frtcate + nextChar(resChar) + "0000";
		}
		else if(cate.substring(6,10).equals("0000")){			// 3차 분류에 추가
			String frtcate = cate.substring(0,6);
			for(int i = 0;i < alen;i++){
				String t_cate = (String)acate.elementAt(i);
				if(t_cate.substring(0,6).equals(frtcate) && t_cate.substring(8,10).equals("00")){
					resChar = biggerChar(resChar,t_cate.substring(6,8));
				}
			}
			if(resChar.equals("")) return frtcate + "0100";
			else return frtcate + nextChar(resChar) + "00";
		}
		else if(cate.substring(8,10).equals("00")){				// 4차 분류에 추가
			String frtcate = cate.substring(0,8);
			for(int i = 0;i < alen;i++){
				String t_cate = (String)acate.elementAt(i);
				if(t_cate.substring(0,8).equals(frtcate)){
					resChar = biggerChar(resChar,t_cate.substring(8,10));
				}
			}
			if(resChar.equals("")) return frtcate + "01";
			else return frtcate + nextChar(resChar);
		}
		return "";
	}
%>
<%@ include file="admin.jsp" %>
<%
	request.setCharacterEncoding("utf-8");
	String cate = "";
	String act = "";
	String kd = "";
	String name = "";
	String sort = "";

	int filesize = 10*1024*1024;		// Maximum file Upload Size : 10Mega
	MultipartRequest multi = null;
	try{
		multi = new MultipartRequest(request, pathOfOriCata, filesize, "utf-8", new DefaultFileRenamePolicy());

		if(multi.getParameter("cate") != null) cate = multi.getParameter("cate");
		if(multi.getParameter("act") != null) act = multi.getParameter("act");
		if(multi.getParameter("kd") != null) kd = multi.getParameter("kd");
		if(multi.getParameter("sort") != null) sort = multi.getParameter("sort");
		if(multi.getParameter("name") != null){
			name = java.net.URLDecoder.decode(multi.getParameter("name"), "utf-8");			// jump packing
			//name = new String(multi.getParameter("name").getBytes("UTF-8"), "KSC5601");	// jump packing
			//name = new String(multi.getParameter("name").getBytes("8859_1"), "KSC5601");	// jump packing
			name = name.replaceAll("|","");
		}
	}
	catch(IOException e){
	}

	String path = pathOfCata + "/catakind.txt";
	int dirval = 0;

	if(act.equals("add")){
		if(cate.length() != 10){
			out.println("notdata");
			return;
		}

		if(kd.equals("")) kd = "0";

		Vector acate = new Vector();
		StringBuffer cont = new StringBuffer();
		f = new File(path);
		if(f.exists()){
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				String[] aaa = mysplit(source, "|");		// cate|setting|name|etc
				acate.addElement(aaa[0]);
				if(!aaa[3].equals("")){
					if(Integer.parseInt(aaa[3]) > dirval) dirval = Integer.parseInt(aaa[3]);
				}
				cont.append(source).append("\n");
			}
			fin.close();
			bin.close();
		}

		dirval++;
		String maxcode = getMaxCate(cate, acate);
		String addedval = "";

		if(kd.equals("0")){
			addedval = maxcode + "|AF000|" + name + "|";
			cont.append(addedval+"\n");
		}
		else{
			addedval = maxcode + "|AF" + kd + "00|" + name + "|" + dirval;
			cont.append(addedval+"\n");

			try{
				f = new File(pathOfCata + "/" + dirval);
				if(!f.exists()){
					f.mkdirs();
				}
			}
			catch(Exception e){
				out.print(e.toString());
				return;
			}
		}

		FileOutputStream fos = new FileOutputStream(path);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.close();

		out.print("added:"+addedval);
	}
	else if(act.equals("move")){
		String unifyCond1 = "";
		String unifyCond2 = "";
		String[] aftcate = mysplit(cate, ":");			// current category:target category:current folder
		String currCode = aftcate[0];
		String targetCode = aftcate[1];
		String sfolder = aftcate[2];

		String pCode = get_parentCode(currCode);

		Vector acate = new Vector();
		f = new File(path);
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, "|");		// cate|setting|name|etc
			acate.addElement(aaa[0]);
		}
		fin.close();
		bin.close();

		String maxcode = getMaxCate(targetCode, acate);

		StringBuffer cont = new StringBuffer();
		String movecont = "";
		fin = new FileInputStream(f);
		bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String tcate = source.substring(0,10);
			if(tcate.equals(currCode)){
				String[] aaa = mysplit(source, "|");		// cate|setting|name|etc
				movecont = maxcode + "|" + aaa[1] + "|" + aaa[2] + "|" + aaa[3] + "\n";
			}
			else{
				if(tcate.equals(pCode)){					// first-step category to come
					unifyCond1 = source.substring(12,13);
				}
				else if(tcate.equals(targetCode)){
					unifyCond2 = source.substring(12,13);	// first-step category to go
				}
				cont.append(source).append("\n");
			}
		}
		fin.close();
		bin.close();

		{
			FileOutputStream fos = new FileOutputStream(path);
			OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
			osw.write(cont.toString() + movecont);
			osw.close();
		}

		cont = new StringBuffer();
		String currcont = "";
		String mycate = targetCode + "\tunif";

		path = pathOfCata + "/category.txt";
		f = new File(path);
		if(f.exists()){
			fin = new FileInputStream(f);
			bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				if(source.substring(0,10).equals(pCode)){
					if(source.substring(11,15).equals("cate")) currcont = source;
				}
				else if(source.substring(0,15).equals(mycate)){
					continue;
				}
				else{
					cont.append(source).append("\n");
				}
			}
			fin.close();
			bin.close();

			if(!currcont.equals("")){
				String[] aaa = mysplit(currcont,"\t");		// 3,4,5 (basic,logo,mail)
				if(aaa[3].equals(sfolder)) aaa[3] = "";
				if(aaa[4].equals(sfolder)) aaa[4] = "";
				if(aaa[5].equals(sfolder)) aaa[5] = "";
				currcont = arrayJoin("\t",aaa);

				{
					FileOutputStream fos = new FileOutputStream(path);
					OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
					osw.write(cont.toString()+currcont+"\n");
					osw.close();
				}
			}
		}

		out.print("moved:"+targetCode+":"+movecont);
	}
	else if(act.equals("mod")){								// only name
		if(cate.length() != 10){
			out.println("notdata");
			return;
		}

		StringBuffer cont = new StringBuffer();
		f = new File(path);
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String tcate = source.substring(0,10);
			if(tcate.equals(cate)){
				String[] aaa = mysplit(source, "|");		// cate|setting|name|etc
				cont.append(aaa[0]).append("|").append(aaa[1]).append("|").append(name).append("|").append(aaa[3]).append("\n");
			}
			else cont.append(source).append("\n");
		}
		fin.close();
		bin.close();

		FileOutputStream fos = new FileOutputStream(path);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.close();

		out.print("modified");
	}
	else if(act.equals("turn")){
		String[] aaa = mysplit(cate, ":");		// from : to : current kd
		String currCode = aaa[0];
		String targetCode = aaa[1];
		String skd = aaa[2];

		String pCode = get_parentCode(currCode);

		String unifyCond = "";
		String turnCont = "";

		f = new File(path);
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String tcate = source.substring(0,10);
			if(tcate.equals(currCode)){
				turnCont = source + "\n";
				break;
			}
		}
		fin.close();
		bin.close();

		StringBuffer cont = new StringBuffer();
		fin = new FileInputStream(f);
		bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String tcate = source.substring(0,10);
			if(tcate.equals(targetCode))
				cont.append(source).append("\n").append(turnCont);
			else if(!tcate.equals(currCode)){
				cont.append(source).append("\n");
			}

			if(tcate.equals(pCode))	unifyCond = source.substring(12,13);
		}
		fin.close();
		bin.close();

		{
			FileOutputStream fos = new FileOutputStream(path);
			OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
			osw.write(cont.toString());
			osw.close();
		}

		path = pathOfCata + "/category.txt";
		if(unifyCond.equals("U") && !skd.equals("0")){
			f = new File(path);
			if(f.exists()){
				UnifyFunc unifyInfo = new UnifyFunc(pathOfCata, streamCharset);
				String cont1 = unifyInfo.get_unifyCont(path, pCode+"\tunif");
				String thiscont = unifyInfo.get_unifContLine(pCode);

				{
					FileOutputStream fos = new FileOutputStream(path);
					OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
					osw.write(cont1);
					osw.write(thiscont);
					osw.close();
				}
			}
		}
		out.print("turned:"+pCode);
	}
	else if(act.equals("del")){
		if(cate.length() != 10){
			out.println("notdata");
			return;
		}

		String pCode = get_parentCode(cate);
		String thiscont = "";
		String parentcont = "";
		String cateFolder = "";
		String unifyCond = "";

		StringBuffer cont = new StringBuffer();

		f = new File(path);
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String tcate = source.substring(0,10);
			if(tcate.equals(cate)){
				thiscont = source;
				continue;
			}
			else if(tcate.equals(pCode)){
				parentcont = source;
			}
			cont.append(source).append("\n");
		}
		fin.close();
		bin.close();

		String cateKind = thiscont.substring(13,14);
		String[] aaa = mysplit(thiscont, "|");		// cate|setting|name|etc

		if(cateKind.equals("G") || cateKind.equals("C")){
			cateFolder = aaa[3];		
			unifyCond = thiscont.substring(12,13);

			String fpath = pathOfCata + "/" + cateFolder;
			if(cateFolder.equals("")){
				out.print("actresult=notdata");
				return;
			}

			f = new File(fpath);
			if(f.exists()){
				int k = 0;
				File[] xolders = f.listFiles();
				for(int i = 0;i < xolders.length;i++){
					String temp = xolders[i].getName();
					if(!temp.equals(".") && !temp.equals("..") && !temp.endsWith("txt")) k++;
				}

				if(k > 0){
					out.print("actresult=existdata");
					return;
				}

				for(int i = 0;i < xolders.length;i++){
					if(xolders[i].isFile()){
						xolders[i].delete();
					}
				}
	
				f.delete();			// delete directory
			}
		}

		FileOutputStream fos = new FileOutputStream(path);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.close();

		path = pathOfCata + "/category.txt";
		if(cateKind.equals("0")){							// category
			unifyCond = thiscont.substring(12,13);

			f = new File(path);
			if(unifyCond.equals("U") && f.exists()){
				UnifyFunc unifyInfo = new UnifyFunc(pathOfCata, streamCharset);
				String cont1 = unifyInfo.get_unifyCont(path, cate);
				fos = new FileOutputStream(path);
				osw = new OutputStreamWriter(fos, streamCharset);
				osw.write(cont1);
				osw.close();
			}
		}
		else{
			unifyCond = parentcont.substring(12,13);

			cont = new StringBuffer();
			String currcont = "";

			f = new File(path);
			if(f.exists()){
				fin = new FileInputStream(f);
				bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
				while((source = bin.readLine()) != null){
					source = strTrimEach(source);
					if(source.equals("")) continue;

					if(source.substring(0,10).equals(pCode)){
						if(source.substring(11,15).equals("cate")) currcont = source;
					}
					else{
						cont.append(source).append("\n");
					}
				}
				fin.close();
				bin.close();

				if(!currcont.equals("")){
					String[] bbb = mysplit(currcont,"\t");		// 3,4,5 (basic,logo,mail)
					if(bbb[3].equals(cateFolder)) bbb[3] = "";
					if(bbb[4].equals(cateFolder)) bbb[4] = "";
					if(bbb[5].equals(cateFolder)) bbb[5] = "";
					currcont = arrayJoin("\t",bbb);
					currcont += "\n";

					UnifyFunc unifyInfo = new UnifyFunc(pathOfCata, streamCharset);
					if(unifyCond.equals("U")) currcont += unifyInfo.get_unifContLine(pCode);

					fos = new FileOutputStream(path);
					osw = new OutputStreamWriter(fos, streamCharset);
					osw.write(cont.toString()+currcont+"\n");
					osw.close();
				}
			}
		}
	
		out.print("deleted");
	}
	else{
		f = new File(path);
		StringBuffer cont = new StringBuffer();
		if(sort.equals("")){
			if(f.exists()){
				FileInputStream fin = new FileInputStream(f);
				BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
				while((source = bin.readLine()) != null){
					source = strTrimEach(source);
					if(source.equals("")) continue;
					cont.append(source).append("\n");
				}
				fin.close();
				bin.close();
			}
		}
		else if(sort.equals("name")){
			Vector arSort = new Vector();
			if(f.exists()){
				FileInputStream fin = new FileInputStream(f);
				BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
				while((source = bin.readLine()) != null){
					source = strTrimEach(source);
					if(source.equals("")) continue;
					String[] aaa = mysplit(source,"|");
					arSort.addElement(aaa[2]+"|"+source);
				}
				fin.close();
				bin.close();
			}

			Collections.sort(arSort);
			int itemLen = arSort.size();
			for(int i = 0;i < itemLen;i++){
				source = (String)arSort.get(i);
				String[] aaa = mysplit(source,"|");
				cont.append(aaa[1]).append("|").append(aaa[2]).append("|").append(aaa[3]).append("|").append(aaa[4]).append("\n");
			}
		}

		if(cont.equals("")) out.print("");
		else out.print(cont.toString());
	}
%>
