<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat, com.oreilly.servlet.MultipartRequest,com.oreilly.servlet.multipart.DefaultFileRenamePolicy" contentType="text/plain;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="admin.jsp" %>
<%
	String act = request.getParameter("act");
	if(act == null) act = "";
	if(!act.equals("list") && !act.equals("del")){
		out.print("act is not acceptable!");
		return;
	}

	String kd, catimage, Dir, sd;
	kd = catimage = Dir = sd = "";
	if(act.equals("list")){
		kd = request.getParameter("kd");
		if(kd == null) kd = "";

		catimage = request.getParameter("catimage");
		if(catimage == null) catimage = "";

		Dir = request.getParameter("Dir");
		if(Dir == null) Dir = "";
	}
	else{
		int filesize = 10*1024*1024;		//10Mega
		MultipartRequest multi = null;

		sd = "";
		try{
			multi = new MultipartRequest(request, pathOfCata, filesize, new DefaultFileRenamePolicy());

			catimage = multi.getParameter("catimage");
			if(catimage == null) catimage = "";

			Dir = multi.getParameter("Dir");
			if(Dir == null) Dir = "";

			kd = multi.getParameter("kd");
			if(kd == null) kd = "";

			sd = multi.getParameter("sd");
			if(sd == null) sd = "";

		} catch(IOException ioe){
			out.print(ioe.toString());
		} catch(Exception ex){
			out.print(ex.toString());
		} finally{
		}
	}

	if(!kd.equals("") && !kd.equals("cata") && !kd.equals("sound") && !kd.equals("image")){
		out.print("Error:kd is not acceptable!");
		return;
	}

	if(check_numVar(catimage) == false){
		out.print("Error:catimage is not acceptable!");
		return;
	}


	if(Dir.equals("")){
		out.print("Error:no Dir parameter!");
		return;
	}

	String dirpath = pathOfAcc + "/catImage" + catimage + "/" + Dir;
	f = new File(dirpath);
	if(!f.exists()){
		out.print("Error:no exists Dir! : "+dirpath);
		return;
	}

	if(kd.equals("sound")){
		f = new File(dirpath + "/sound");
		if(!f.exists()){
			f.mkdirs();
		}
		dirpath = dirpath + "/sound";
	}
	else if(kd.equals("image")){
		f = new File(dirpath + "/image");
		if(!f.exists()){
			f.mkdirs();
		}
		dirpath = dirpath + "/image";
	}

	if(act.equals("list")){
		Vector vt = new Vector();
		if(f.exists()){
			File[] folders = f.listFiles();
			for(int i = 0;i < folders.length;i++){
				String temp = folders[i].getName();
				if(temp.equals(".") || temp.equals("..") || folders[i].isDirectory()) continue;
				vt.addElement(temp);
			}
		}

		int lenc = 0;
		StringBuffer sbuf = new StringBuffer();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		int alen = vt.size();
		if(alen > 0){
			Collections.sort(vt);

			for(int i = 0;i < alen;i++){
				String temp = (String)vt.get(i);
				File f1 = new File(dirpath + "/" + temp);
				String dcont = sdf.format(new Date(f1.lastModified()));
				sbuf.append(temp).append("|").append(f1.length()).append("|").append(dcont).append("\n");
			}
		}
		else{
			sbuf.append("");
		}

		out.print(sbuf.toString());
	}
	else if(act.equals("del")){
		//sd = repl_injection(sd);
		String[] alist = mysplit(sd, ",");

		for(int i = 0;i < alist.length;i++){
			if(!alist[i].equals("")){
				f = new File(dirpath+"/"+alist[i]);
				//out.println(dirpath+"/"+alist[i]);
				if(f.exists()) f.delete();
			}
		}

		out.println("deleted:files");
	}
%>
