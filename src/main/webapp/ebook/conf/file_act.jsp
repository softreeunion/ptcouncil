<%@ page import="com.oreilly.servlet.MultipartRequest, com.oreilly.servlet.multipart.DefaultFileRenamePolicy, java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="admin.jsp" %>
<%
	String act = request.getParameter("act");
	if(act == null) act = "";

	String kd = request.getParameter("kd");
	if(kd == null) kd = "";

	String atta_dir = pathOfDir + "/";
	if(kd.equals("sound")) atta_dir = pathOfDir + "/sound/";
	else if(kd.equals("image")) atta_dir = pathOfDir + "/image/";

	int filesize = 10*1024*1024;		// Maximum file Upload Size : 10Mega
	if(act.equals("add")){
		MultipartRequest multi = null;
		multi = new MultipartRequest(request, atta_dir, filesize, new DefaultFileRenamePolicy());

		String pixsave_name = multi.getFilesystemName("pixfile");
		String pixfile_name = multi.getOriginalFileName("pixfile");
		String orisame = multi.getParameter("orisame");
		String fname = new String(multi.getParameter("fname").getBytes("8859_1"), streamCharset);

		if(multi.getParameter("orisame") == null) orisame = "";
		else orisame = new String(multi.getParameter("orisame").getBytes("8859_1"), streamCharset);

		f = new File(pathOfDir + "/sound");
		if(!f.exists()){
			f.mkdirs();
		}

		String act_string = snc_catalog("notform");
		if(orisame.equals("1")){
			if(pixfile_name.equals("") || pixfile_name.indexOf("/") > -1 || pixfile_name.indexOf("cgi") > -1 || pixfile_name.indexOf("jsp") > -1){
				out.println(echo_text(act_string+"(1)"));
				return;
			}
			if(!pixsave_name.equals(pixfile_name)){
				f = new File(atta_dir + pixsave_name);
				f.delete();
				out.println(echo_text(act_string + " : "+pixfile_name+" -> "+pixsave_name));
				return;
			}
		}
		else{
			if(fname.equals("") || fname.indexOf("/") > -1 || fname.indexOf("cgi") > -1 || fname.indexOf("jsp") > -1){
				out.println(echo_text(act_string+"(3)"));
				return;
			}
			if(!pixsave_name.equals(fname)){
				f = new File(atta_dir + pixsave_name);
				File f1 = new File(atta_dir + fname);
				f.renameTo(f1);
			}
		}

		act_string = snc_catalog("added");
		if(kd.equals("sound")) out.println(echo_script(act_string, "file_soundlist.jsp", "",""));
		else if(kd.equals("image")) out.println(echo_script(act_string, "file_imagelist.jsp", "",""));
		else if(kd.equals("pito")) out.println(echo_script(act_string, "pitocom.jsp?act=added", "",""));
		else out.println(echo_script(act_string, "file_catalist.jsp", "",""));
	}
	else if(act.equals("fmd")){
		String fname = request.getParameter("fname");
		String newfname = request.getParameter("newfname");
		if(newfname.equals("") || newfname.indexOf("/") > -1 || newfname.indexOf("cgi") > -1 || newfname.indexOf("jsp") > -1){
			out.println(echo_text(snc_catalog("notform")));
			return;
		}

		f = new File(atta_dir + fname);
		File f1 = new File(atta_dir + newfname);
		f.renameTo(f1);

		out.println(echo_script(snc_catalog("changed"), "filedet.jsp?kd=" + kd + "&fname=" + newfname, "",""));
	}
	else if(act.equals("mod")){
		MultipartRequest multi = null;
		multi = new MultipartRequest(request, atta_dir, filesize, new DefaultFileRenamePolicy());
		String fname = new String(multi.getParameter("fname").getBytes("8859_1"), streamCharset);
		String pixsave_name = multi.getFilesystemName("pixfile");

		if(fname.equals("")){
			out.println(echo_text(snc_catalog("notform")));
			return;
		}

		f = new File(atta_dir + fname);
		f.delete();

		File f1 = new File(atta_dir + pixsave_name);
		f1.renameTo(f);

		out.println(echo_script(snc_catalog("changed"), "file_det.jsp?kd=" + kd + "&fname=" + fname, "",""));
	}
	else if(act.equals("del")){
		String fname = request.getParameter("fname");
		if(fname.equals("")){
			out.println(echo_text(snc_catalog("notform")));
			return;
		}

		f = new File(atta_dir + fname);
		f.delete();

		if(kd.equals("sound")) out.println(echo_script(snc_catalog("deleted"), "file_soundlist.jsp", "",""));
		else if(kd.equals("image")) out.println(echo_script(snc_catalog("deleted"), "file_imagelist.jsp", "",""));
		else out.println(echo_script(snc_catalog("deleted"), "file_catalist.jsp", "",""));
	}
	else if(act.equals("alldel")){
		f = new File(pathOfDir);
		if(kd.equals("sound")) f = new File(pathOfDir + "/sound");
		else if(kd.equals("image")) f = new File(pathOfDir + "/image");

		if(f.exists()){
			File[] folders = f.listFiles();
			for(int i = 0;i < folders.length;i++){
				String temp = folders[i].getName();
				if(!temp.equals(".") && !temp.equals("..")){
					File f1 = new File(atta_dir + temp);
					f1.delete();
				}
			}
		}

		if(kd.equals("sound")) out.println(echo_script(snc_catalog("deleted"), "file_soundlist.jsp", "",""));
		else if(kd.equals("image")) out.println(echo_script(snc_catalog("deleted"), "file_imagelist.jsp", "",""));
		else out.println(echo_script(snc_catalog("deleted"), "file_catalist.jsp", "",""));
	}
	else if(act.equals("logdel")){
		String fname = request.getParameter("fname");
		if(fname.equals("")){
			out.println(echo_text(snc_catalog("notform")));
			return;
		}

		f = new File(pathOfDir + "/" + fname);
		f.delete();

		out.println(echo_script(snc_catalog("deleted"), "logo.jsp", "",""));
	}
	else if(act.equals("splogdel")){
		String fname = request.getParameter("fname");
		if(fname.equals("")){
			out.println(echo_text(snc_catalog("notform")));
			return;
		}

		f = new File(pathOfCata + "/" + fname);
		f.delete();

		out.println(echo_script(snc_catalog("deleted"), "logo_def.jsp", "",""));
	}
	else if(act.equals("mdel")){
		String[] sd = request.getParameterValues("sd");
		for(int i = 0;i < sd.length;i++){
			if(!sd[i].equals("")){
				f = new File(atta_dir + sd[i]);
				f.delete();
			}
		}

		if(kd.equals("sound")) out.println(echo_script(snc_catalog("deleted"), "file_soundlist.jsp", "",""));
		else if(kd.equals("image")) out.println(echo_script(snc_catalog("deleted"), "file_imagelist.jsp", "",""));
		else out.println(echo_script(snc_catalog("deleted"), "file_catalist.jsp", "",""));
	}
	else if(act.equals("catalogo_def") || act.equals("catalogo_each") || act.equals("maillogo_def") || act.equals("maillogo_each") || act.equals("splogo_def") || act.equals("splogo_each")){
		MultipartRequest multi = null;
		multi = new MultipartRequest(request, pathOfAcc, filesize, new DefaultFileRenamePolicy());

		String pixsave_name = multi.getFilesystemName("pixfile");
		String pixfile_name = multi.getOriginalFileName("pixfile");
		String vari = multi.getParameter("vari");
		if(vari == null) vari = "";

		String path = pathOfOriCata;
		String dirLoc = "logo_def.jsp";
		if(vari.equals("admin")){
			path = pathOfDir;
			dirLoc = "logo_admin.jsp";
		}
		else if(act.equals("catalogo_def") || act.equals("maillogo_def") || act.equals("splogo_def")){
			path = pathOfCata;
			dirLoc = "logo_def.jsp";
		}
		else if(act.equals("catalogo_each") || act.equals("maillogo_each") || act.equals("splogo_each")){
			path = pathOfDir;
			dirLoc = "logo.jsp";
		}

		String appendix;
		if(act.equals("maillogo_def") || act.equals("maillogo_each")) appendix = "malogo";
		else if(act.equals("splogo_def") || act.equals("splogo_each")) appendix = "splogo";
		else appendix = "logo";
		int aplen = appendix.length();

		String logoFile = "";
		f = new File(path);
		File[] folders = f.listFiles();
		for(int i = 0;i < folders.length;i++){
			String temp = folders[i].getName();
			if(temp == null || temp.length() < aplen || temp.substring(0,1).equals(".")) continue;
			if(temp.length() > aplen && temp.substring(0,aplen).equals(appendix)) logoFile = path + "/" + temp;
		}

		String ext = pixfile_name.substring(pixfile_name.indexOf("."));
		if((act.equals("catalogo_def") || act.equals("catalogo_each")) && pixfile_name.indexOf("jpg") == -1 && pixfile_name.indexOf("gif") == -1 && pixfile_name.indexOf("png") == -1 && pixfile_name.indexOf("swf") == -1){
			out.println(echo_script("파일형식을 jpg/gif/png/swf로 맞춰주시기 바랍니다.", dirLoc, "",""));
			return;
		}
		if((act.equals("maillogo_def") || act.equals("maillogo_each") || act.equals("splogo_def") || act.equals("splogo_each")) && pixfile_name.indexOf("jpg") == -1 && pixfile_name.indexOf("gif") == -1 && pixfile_name.indexOf("png") == -1){
			out.println(echo_script("파일형식을 jpg/gif/png로 맞춰주시기 바랍니다.", dirLoc, "",""));
			return;
		}

		if(!logoFile.equals("")){
			f = new File(logoFile);
			if(f.exists()) f.delete();
		}

		f = new File(pathOfAcc + "/" + pixsave_name);
		File f1 = new File(path + "/" + appendix + ext);
		f.renameTo(f1);

		out.println(echo_script(snc_catalog("changed"), dirLoc, "",""));
	}
%>
