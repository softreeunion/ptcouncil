<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat, java.lang.Integer" contentType="text/html;charset=utf-8" %>
<%@ include file="ImageSize.jsp" %>
<%@ include file="admin.jsp" %>
<%
	String pxf = request.getParameter("pxf");

	Hashtable ahash = new Hashtable();
	f = new File(pathOfDir + "/ecatalog.txt");
	if(f.exists()){
		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);
			String sval = source.substring(in+1);

			ahash.put(skey, sval);
		}
		fin.close();
		bin.close();
	}

	int[] imgsize = new int[2];
	boolean fileExist = false;
	f = new File(pathOfDir + "/" + pxf);
	if(f.exists()){
		fileExist = true;
		ImageSize inImage = new ImageSize();
		inImage.setImage(pathOfDir + "/" + pxf);
		imgsize[0] = inImage.getWidth();
		imgsize[1] = inImage.getHeight();
	}

	int i = 0;
	int k = 0;

	StringBuffer link_buf = new StringBuffer("");
	f = new File(pathOfDir + "/alink.txt");
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, "\t");
			if(aaa[0].equals(pxf)){
				String coord = "";
				String[] arcd = mysplit(aaa[1],",");
				int clen = arcd.length;
				if(clen == 4){
					coord = arcd[0] + "," + arcd[1] + " " + arcd[2] + "," + arcd[1] + " " + arcd[2] + "," + arcd[3] + " " + arcd[0] + "," + arcd[3];
				}
				else{
					coord = coord + arcd[0] + "," + arcd[1];
					for(i = 2;i < clen;i+=2){
						coord = coord + " " + arcd[i] + "," + arcd[i+1];
					}
				}
				link_buf.append("arLink["+k+"] = \"" + coord + "\t" + aaa[2] + "\t" + aaa[3] + "\";\n");
				k++;
			}
		}
		fin.close();
		bin.close();
	}
	String webpath = webServer + "/catImage" + cook_catimage + "/" + cook_dir;
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 페이지링크(새창)</title>
<style>
	*{font-size:11px;font-family:돋움;}
	html, body{margin:0px;overflow:hidden;width:100%;height:100%;}
	img{border:0;vertical-align:middle;}
	form{display:inline;}
	input.px, textarea.px{background-color:#EFF9FC;border:1px solid #444444;}	/* general form */

	a:link, a:visited, a:active{text-decoration:none;}
	a:hover{text-decoration:underline;}

	#linkpix{display:block;width:auto;height:auto;z-index:1;}
	#linkdiv{position:absolute;z-index:1;}

	#linkform{z-index:3;display:block;position:absolute;left:0;bottom:0;width:100%;height:23px;border-top:1px solid #000000;background-color:#ffffff;}
	#linkform p{display:block;float:left;height:23px;padding:0;margin:0 0 0 5px;line-height:23px;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script>
var imgWidth = <%=imgsize[0]%>;
var imgHeight = <%=imgsize[1]%>;
var imgFname = "<%=pxf%>";
var svgObj, svgDiv;
var befX, befY;
var currMode = "standard";
var arLink = new Array();
<%=link_buf%>
var imgsrc = "<%=webpath%>/<%=pxf%>";
var bigDragRect;

function onload_func(){
	bigDragRect = {top:document.body.clientHeight-imgHeight-24, left:document.body.clientWidth-imgWidth, bottom:0, right:0};
}
function svgLoaded(script){
	svgObj = script;
	svgDiv = document.getElementById("linkpix");
}

function svgDowned(){
	befX = get_svgDivX();
	befY = get_svgDivY();
}
function get_svgDivX(){
	return parseInt(svgDiv.style.left.replace("px",""));
}
function get_svgDivY(){
	return parseInt(svgDiv.style.top.replace("px",""));
}
function svgMoved(dx, dy){
	var rx, ry;

	if(document.body.clientWidth > imgWidth){
		rx = befX;
	}
	else{
		rx = befX + dx;
		if(rx > 0) rx = 0;
		else if(rx < bigDragRect.left) rx = bigDragRect.left;
	}

	if(document.body.clientHeight-24 > imgHeight){
		ry = befY;
	}
	else{
		ry = befY + dy;
		if(ry > 0) ry = 0;
		else if(ry < bigDragRect.top) ry = bigDragRect.top;
	}

	svgDiv.style.left = rx + "px";
	svgDiv.style.top = ry + "px";
}
function set_value(s1, n){
	if(n == -1){
		document.showform.coords.value = s1;
		document.showform.title.value = "";
		document.showform.url.value = "";
		document.showform.ckno.value = "";
		document.showform.asubmit.value = "추가";
		document.showform.adel.disabled = false;
	}
	else{
		var aritem = arLink[n].split("\t");				// coords \t title \t linkurl
		document.showform.coords.value = s1;
		document.showform.title.value = aritem[1];
		document.showform.url.value = aritem[2];
		document.showform.ckno.value = (n+1);
		document.showform.asubmit.value = "변경";
		document.showform.adel.disabled = false;
	}
}
function set_value2(s){
	document.showform.coords.value = s;
}
function change_mode(s, bool){
	if(s == "") currMode = (currMode == "standard") ? "input" : "standard";
	else currMode = s;
	document.getElementById("modepix").src = "<%=confServer%>/pix/nwpix_"+currMode+".gif";
	if(bool == true) svgObj.change_mode(s, false);
}
function checkup(){
	var dm = document.showform;
	if(dm.coords.value == ""){
		alert("좌표를 선택하셔야합니다.");
		return false;
	}
	if(dm.title.value == ""){
		alert("타이틀을 입력해주세요.");
		dm.title.focus();
		return false;
	}
	else if(dm.url.value == ""){
		alert("링크URL을 입력해주세요.");
		dm.url.focus();
		return false;
	}

	if(dm.ckno.value != ""){
		if(confirm("변경하시겠습니까?")){
			dm.act.value = "sod";
			return true;
		}
		else return false;
	}
	else{
		if(confirm("추가하시겠습니까?")){
			dm.act.value = "add";
			return true;
		}
		else return false;
	}
}
function checkdel(){
	if(document.showform.ckno.value == ""){
		alert("삭제하실 링크를 선택하셔야 합니다.");
		return;
	}

	if(confirm("삭제하시겠습니까?")){
		document.showform.act.value = "del";
		document.showform.submit();	
	}
	else return;
}
function totalReset(){
	window.location = "nw_pagelink.jsp?pxf=<%=pxf%>";
}
</script>
</head>

<body onload="onload_func();">

<div id="linkpix" style="position:absolute;z-index:3;left:0px;top:0px;">
<object id="linksvg" width="<%=imgsize[0]%>" height="<%=imgsize[1]%>" data="<%=confServer%>/linkpage.svg" type="image/svg+xml"></object>
</div>

<div id="linkform">
<form name="showform" method="post" action="link_act.jsp" onsubmit="return checkup()">
<p><a href="javascript:change_mode('',true);"><img src="<%=confServer%>/pix/nwpix_standard.gif" width="21" height="21" id="modepix" alt="표준모드"></a></p>
<p><label>좌표 <input type="text" name="coords" size="35" value="" class="px" readonly></label></p>
<p><label>타이틀 <input type="text" name="title" size="30" value="" class="px"></label></p>
<p><label>링크 <input type="text" name="url" size="40" value="" class="px"></label></p>
<p><input type="submit" name="asubmit" value="입력"></p>
<p><input type="button" name="adel" value="삭제" onclick="checkdel()"></p>
<p><input type="button" name="areconf" value="새로고침" onclick="totalReset()"></p>

<input type="hidden" name="act" value="">
<input type="hidden" name="ckno" value="">
<input type="hidden" name="pxf" value="<%=pxf%>">
<input type="hidden" name="loc" value="slpix">
</form>
</div>

</body>
</html>
