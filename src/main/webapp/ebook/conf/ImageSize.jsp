<%!
 public class ImageSize 
{

    private int width;
    private int height;

    private InputStream in;
    private int bitBuf;
    private int bitPos;


	/**
	 * 생성자<br>
	 * 생성후 반드시 setImage() 메쏘드를 통하여 이미지 파일을 세팅해야 한다.
	 *
	 */
    public ImageSize()
	{ }

	/**
	 * ImageFile의 유효성을 체크<br>
	 * gif, jpg, png, swf 지원
	 *
	 * @return 유효하면 true, 그렇지 않으면 false
	 */
    public boolean check() 
	{
        width = 0;
        height = 0;

        try {
            int b1 = read() & 0xff;
            int b2 = read() & 0xff;
            if (b1 == 0x47 && b2 == 0x49) {
                return checkGif();
            }
            else if (b1 == 0xff && b2 == 0xd8) {
                return checkJpeg();
            }
            else if (b1 == 0x89 && b2 == 0x50) {
                return checkPng();
            }
            else if (b1 == 0x46 && b2 == 0x57) {
                return checkSwf();
            }
            else {
                return false;
            }
        } catch (IOException ioe) {
            return false;
        }
    }

	/**
	 * Gif ImageFile의 유효성을 체크
	 *
	 * @return 유효하면 true, 그렇지 않으면 false
	 */
    private boolean checkGif() throws IOException 
	{
        final byte[] GIF_MAGIC_87A = {0x46, 0x38, 0x37, 0x61};
        final byte[] GIF_MAGIC_89A = {0x46, 0x38, 0x39, 0x61};
        byte[] a = new byte[11]; 

        if (read(a) != 11) {
            return false;
        }
        if ((!equals(a, 0, GIF_MAGIC_89A, 0, 4)) &&
            (!equals(a, 0, GIF_MAGIC_87A, 0, 4))) {
            return false;
        }
        width = getShortLittleEndian(a, 4);
        height = getShortLittleEndian(a, 6);

        return (width>0 && height>0);
    }

	/**
	 * Jpeg ImageFile의 유효성을 체크
	 *
	 * @return 유효하면 true, 그렇지 않으면 false
	 */
    private boolean checkJpeg() throws IOException 
	{
        byte[] data = new byte[12];
        while (true) {
            if (read(data, 0, 4) != 4) {
                return false;
            }
            int marker = getShortBigEndian(data, 0);
            int size = getShortBigEndian(data, 2);

            if((marker & 0xff00) != 0xff00){
                return false; 
            }
            if(marker >= 0xffc0 && marker <= 0xffcf && marker != 0xffc4 && marker != 0xffc8) {
                if (read(data, 0, 6) != 6) {
                    return false;
                }
                width = getShortBigEndian(data, 3);
                height = getShortBigEndian(data, 1);

                return (width>0 && height>0);
            } 
            else{
                skip(size - 2);
            }
        }
    }

	/**
	 * Png ImageFile의 유효성을 체크
	 *
	 * @return 유효하면 true, 그렇지 않으면 false
	 */
    private boolean checkPng() throws IOException 
	{
        final byte[] PNG_MAGIC = {0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a};
        byte[] a = new byte[24];
        if (read(a) != 24) {
            return false;
        }
        if (!equals(a, 0, PNG_MAGIC, 0, 6)) {
            return false;
        }
        width = getIntBigEndian(a, 14);
        height = getIntBigEndian(a, 18);

        return (width > 0 && height > 0);
    }


	/**
	 * Swf ImageFile의 유효성을 체크
	 *
	 * @return 유효하면 true, 그렇지 않으면 false
	 */
    private boolean checkSwf() throws IOException 
	{
        byte[] a = new byte[6];
        if (read(a) != a.length) {
            return false;
        }
        int bitSize = (int)readUBits( 5 );
        int minX = (int)readSBits( bitSize );
        int maxX = (int)readSBits( bitSize );
        int minY = (int)readSBits( bitSize );
        int maxY = (int)readSBits( bitSize );
        width = maxX/20; 
        height = maxY/20;  
        return (width > 0 && height > 0);
    }

	/**
	 * 두개의 자료가 일치하는지 체크
	 *
	 * @param a1 첫번째 바이트 배열
	 * @param offs1 첫번째 offset
	 * @param a2 두번째 바이트 배열
	 * @param offs2 두번째 offset
	 * @param num 일치하는지 확인할 바이트수
	 * @return 일치하면 true, 그렇지 않으면 false
	 */
    private boolean equals(byte[] a1, int offs1, byte[] a2, int offs2, int num) 
	{
        while (num-- > 0) {
            if (a1[offs1++] != a2[offs2++]) {
                return false;
            }
        }
        return true;
    }
    
	private int getIntBigEndian(byte[] a, int offs) 
	{
        return
            (a[offs] & 0xff) << 24 | 
            (a[offs + 1] & 0xff) << 16 | 
            (a[offs + 2] & 0xff) << 8 | 
            a[offs + 3] & 0xff;
    }
    private int getIntLittleEndian(byte[] a, int offs) 
	{
        return
            (a[offs + 3] & 0xff) << 24 | 
            (a[offs + 2] & 0xff) << 16 | 
            (a[offs + 1] & 0xff) << 8 | 
            a[offs] & 0xff;
    }
    private int getShortBigEndian(byte[] a, int offs) 
	{
        return
            (a[offs] & 0xff) << 8 | 
            (a[offs + 1] & 0xff);
    }
    private int getShortLittleEndian(byte[] a, int offs) 
	{
        return (a[offs] & 0xff) | (a[offs + 1] & 0xff) << 8;
    }
    private int read() throws IOException 
	{
        return in.read();
    }
    private int read(byte[] a) throws IOException 
	{
        if (in != null) {
            return in.read(a);
        }
        return 0;
    }
    private int read(byte[] a, int offset, int num) throws IOException 
	{
        if (in != null) {
            return in.read(a, offset, num);
        }
        return 0;
    }
    private long readUBits( int numBits ) throws IOException
    {
        if (numBits == 0) {
            return 0;
        }
        int bitsLeft = numBits;
        long result = 0;
        if (bitPos == 0) { 
            bitBuf = in.read();
            bitPos = 8;
        }
        
        while(true){
            int shift = bitsLeft - bitPos;
            if(shift > 0){
                result |= bitBuf << shift;
                bitsLeft -= bitPos;

                bitBuf = in.read();
                bitPos = 8;
            }
            else{
                result |= bitBuf >> -shift;
                bitPos -= bitsLeft;
                bitBuf &= 0xff >> (8 - bitPos);    

                return result;
            }
        }        
    }
    
    private int readSBits( int numBits ) throws IOException
	{
        long uBits = readUBits( numBits );
        if(( uBits & (1L << (numBits - 1))) != 0 ){
            uBits |= -1L << numBits;
        }
        return (int)uBits;        
    }  
    private void skip(int num) throws IOException 
	{
        in.skip(num);
    }

	/**
	 * 이미지 파일의 높이를 반환
	 *
	 * @return 이미지 파일의 높이 (pixel)
	 */
    public int getHeight() 
	{
        return height;
    }
	
	/**
	 * 이미지 파일의 넓이를 반환
	 *
	 * @return 이미지 파일의 넓이 (pixel)
	 */
    public int getWidth() 
	{
        return width;
    }

	/**
	 * 이미지 파일 세팅
	 *
	 * @param imagepath 이미지 파일의 경로 (절대경로)
	 * @return 파일이 유효하면 true, 존재하지 않거나 유효하지 않으면 false
	 */
    public boolean setImage(String imagepath)
	{
        try{
            in = new FileInputStream(imagepath);    
            return check();
        }
        catch(Exception e){
            return false;
        }
        finally{
            try{if(in != null) in.close();}catch(Exception ie){}
        }
    }
}
%>
