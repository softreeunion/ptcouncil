<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="lang.jsp" %>
<%@ include file="common_func.jsp" %>
<%
	request.setCharacterEncoding("utf-8");
	String act = request.getParameter("act");
	if(act == null) act = "";

	if(act.equals("idpass")){
		String jspTag1 = "<" + "%";
		String jspTag2 = "%" + ">";

		String member_id = request.getParameter("mid");
		String passwd1 = request.getParameter("passwd1");

		File f = new File(passSysDir);
		if(f.exists()){
			out.println(echo_script("이미 설정되었습니다. 다시 설정하실 수 없습니다.", "login.jsp", "",""));
			return;
		}

		String source_header = get_accessheader();

		FileOutputStream fos = new FileOutputStream(passSysDir);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(source_header + member_id + ":" + passEncode(passwd1));
		osw.close();

		out.println(echo_script("아이디와 비밀번호가 설정되었습니다.", "login.jsp", "",""));
	}
%>
