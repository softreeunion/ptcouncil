<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	String kd = request.getParameter("kd");
	if(kd == null) kd = "cata";

	String ent_server = (request.getServerPort() == 80) ? request.getServerName() : request.getServerName() + ":" + request.getServerPort();
	String linkDir = "http://" + ent_server + wasServer + "/conf/";

	String action = "file_act.jsp";
	String gfilename = "기본파일";
	String listfname = "file_catalist.jsp";

	if(kd.equals("media")){
		action = "media_act.jsp";
		gfilename = "음악파일";
		listfname = "file_medialist.jsp";
	}
	else if(kd.equals("flash") || kd.equals("flashdir")){
		action = "media_act.jsp";
		gfilename = "링크파일";
		listfname = "file_flashlist.jsp";
	}
	else if(kd.equals("sound")){
		action = "file_act.jsp";
		gfilename = "소리파일";
		listfname = "file_soundlist.jsp";
	}
	else if(kd.equals("image")){
		action = "file_act.jsp";
		gfilename = "그림파일";
		listfname = "file_imagelist.jsp";
	}
	else if(kd.equals("cata") || kd.equals("bimg")){
		kd = "cata";
	}
	else{
		out.print("kd is not acceptable!");
		return;
	}

	String vars = "catimage="+cook_catimage+"&amp;Dir=" + cook_dir + "&amp;charset=" + charset + "&amp;url=" + linkDir + "&amp;lolang=" + lolang + "&amp;ext=jsp&amp;kd=" + kd + "&amp;csrfName=&amp;csrfValue=";
	if(variOfCata.equals("mblog")) vars += "&amp;tid=" + admin_id;

	String[] subar = {"file", gfilename + " 추가", "name", kd};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 파일추가</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	main table td, main table th{font-size:11px;font-family:Tahoma;}
	main table tr td:nth-of-type(3n+1){text-align:left;}
	main table tr td:nth-of-type(3n+2){text-align:right;}
	main table tr td:nth-of-type(3n+3){text-align:left;}

	#progressbar{display:none;position:absolute;left:300px;top:250px;width:310px;height:110px;overflow:hidden;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script src="<%=confServer%>/fileadd.js"></script>
<script>
function checkup(){
	var dm = document.addform;

<% if(kd.equals("cata") || kd.equals("sound")){ %>
	if(dm.orisame.checked == false && trimVal(dm.fname.value) == ""){
		alert("파일이름을 입력하셔야 합니다.");
		dm.fname.focus();
		return false;
	}
<% } else if(kd.equals("media") || kd.equals("flash")){ %>
	if(trimVal(dm.title.value) == ""){
		alert("제목을 입력하셔야 합니다.");
		dm.title.focus();
		return false;
	}
<% } %>

	if(trimVal(dm.pixfile.value) == ""){
		alert("파일을 선택하셔야 합니다.");
		dm.pixfile.focus();
		return false;
	}
}
function onload_func2(){
	onload_func('<%=listfname%>');
	init_fupload("jsp","<%=kd%>","<%=cook_catimage%>","<%=cook_dir%>");
}
</script>
</head>

<body onload="onload_func2();">
<%@ include file="inc_menu.jsp" %>

<main id="maincnt" class="maincnt">

<p class="table-title"><%=gfilename%> 추가</p>
<form name="addform" method="post" enctype="multipart/form-data" action="<%=action%>?act=add&kd=<%=kd%>" onsubmit="return checkup()">

<% if(kd.equals("cata") || kd.equals("bimg") || kd.equals("sound")){ %>
<p>파일이름 <input type="text" name="fname" size="20" class="px" <% if(kd.equals("bimg")){ %>value="background.jpg" readonly<% } %>>
<label><input type="checkbox" name="orisame" value="1" id="id_same" <% if(kd.equals("bimg")){ %>onclick="this.checked=false;"<% } %>>
선택한 파일이름 적용</label> <img src="./pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_filesel')" onmouseout="hide_helpdiv();"></p>
<% } else if(kd.equals("media")){ %>
<p>제목 <img src="./pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_filesel')" onmouseout="hide_helpdiv();"><br>
<input type="text" name="title" size="50" class="px"></p>
<% } else if(kd.equals("flash")){ %>
<p>제목<br>
<input type="text" name="title" size="50" class="px"></p>
<p>너비 x 높이<br>
<input type="text" name="flw" size="5" class="px"> x <input type="text" name="flh" size="5" class="px"></p>
<% } else if(kd.equals("image")){ %>
<input type="hidden" name="orisame" value="1">
<% } %>

<p>파일선택 <input type="file" name="pixfile" required> <input type="submit" value="추가하기"></p>

<input type="hidden" name="act" value="add">
<input type="hidden" name="kd" value="<%=kd%>">
</form>

<% if(kd.equals("cata") || kd.equals("sound")){ %>
<p id="help_filesel" class="secthelp">'선택한 파일 이름 적용' 선택시에는 파일이름을 입력하지 않으셔도 되며 PC의 파일이름이 그대로 적용됩니다.</p>
<% } else if(kd.equals("media")){ %>
<p id="help_filesel" class="secthelp">제목은 방문자가 음악파일을 선택할 때 보여집니다.</p>
<% } else if(kd.equals("flash")){ %>
<p id="help_filesel" class="secthelp2">
	- 제목은 링크파일이 호출될 때 만들어지는 창의 상단에 보여지게 됩니다.<br>
	- 너비와 높이는 플래시 파일이 본문에서 호출될 때 보여주는 크기입니다. 실제크기와 상관없이 지정 가능합니다.
</p>
<% } %>

<% if(kd.equals("cata") || kd.equals("sound") || kd.equals("image")){ %>
<section>
<h3>다중 업로드</h3>
<div id="fleft" ondragover="leftDragOver(event)" ondrop="leftDrop(event)">
<table class="filegrid">
	<colgroup>
		<col style="width:40%;"><col style="width:20%;"><col style="width:30%;">
	</colgroup>
	<thead>
	<tr>
		<th>파일이름</th>
		<th>크기</th>
		<th>변경일</th>
	</tr>
	</thead>
	<tbody id="fleft_body">
	</tbody>
</table>
</div>

<div id="fcenter">
	<p class="filecontainer"><input type="file" name="selfile" multiple onchange="UploadObj.fileSelected(event);"></p>
	<input type="button" id="localdel" name="localdel" value="목록에서
제거" onclick="localdelbtnClick()"><br>
	<input type="button" name="localsel" value="전체선택" onclick="localallbtnClick()"><br>
	<button onclick="uploadbtnClick()"><img src="pix/file_upload.png" width="24" height="23" alt="파일 업로드"></button><br>
	<input type="button" name="svrsel" value="전체선택" onclick="svrallbtnClick()"><br>
	<input type="button" name="svrdel" value="서버에서
삭제" onclick="svrdelbtnClick()">
</div>

<div id="fright">
<form name="uploadform" method="post" action="#" onsubmit="return false;">
<table class="filegrid">
	<colgroup>
		<col style="width:40%;"><col style="width:20%;"><col style="width:30%;">
	</colgroup>
	<thead>
	<tr>
		<th>파일이름</th>
		<th>크기</th>
		<th>변경일</th>
	</tr>
	</thead>
	<tbody id="fright_body">
	</tbody>
</table>
</form>
</div>

<div class="clearbreaker"></div>

<p class="secthelp2">다중업로드 시에는 PC의 파일이름이 그대로 적용됩니다.</p>
</section>

<div id="progressbar">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="310" height="110">
	<rect x="0" y="0" width="310" height="110" rx="3" fill="#F5F5F5" stroke="#333333" stroke-width="1" />
	<text id="pgbar_title" x="30" y="30" fill="#000000"></text>
	<rect x="30.5" y="44.5" width="250" height="18" fill="#FFFFFF" stroke="#555555" stroke-width="1" />
	<rect x="30.5" y="74.5" width="250" height="18" fill="#FFFFFF" stroke="#555555" stroke-width="1" />
	<rect id="pgbar_indiv" x="33.5" y="49.5" width="0" height="6" fill="#FF0000" /><!-- max : 242 -->
	<rect id="pgbar_total" x="33.5" y="79.5" width="0" height="6" fill="#FF0000" />
</svg>
</div>
<% } %>

</main>

<%@ include file="inc_copy.jsp" %>
