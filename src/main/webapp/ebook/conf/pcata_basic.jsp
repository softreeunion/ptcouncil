<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="ImageSize.jsp" %>
<%@ include file="admin.jsp" %>
<%
	f = new File(pathOfCata + "/catakind.txt");
	if(!f.exists() || cook_dir.equals("")){
		out.println(echo_script(snc_catalog("selfolder"), "basic.jsp", "",""));
		return;
	}

	String[] subar = {"basic", "카탈로그 설정", "basic",""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 기본환경</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function checkup(){
	var dm = document.cataform;

	if(trimVal(dm.pageno.value) == ""){
		alert("페이지수를 기입하셔야 합니다.");
		dm.pageno.focus();
		return false;
	}
	if(trimVal(dm.smwidth.value) == ""){
		alert("페이지 너비를 기입하셔야 합니다.");
		dm.smwidth.focus();
		return false;
	}
	if(trimVal(dm.smheight.value) == ""){
		alert("페이지 높이를 기입하셔야 합니다.");
		dm.smheight.focus();
		return false;
	}
	if(trimVal(dm.bgwidth.value) == ""){
		alert("확대 너비를 기입하셔야 합니다.");
		dm.bgwidth.focus();
		return false;
	}
	if(trimVal(dm.bgheight.value) == ""){
		alert("확대 높이를 기입하셔야 합니다.");
		dm.bgheight.focus();
		return false;
	}

	if(isNotnum(dm.smwidth.value) == true || isNotnum(dm.smheight.value) == true || isNotnum(dm.bgwidth.value) == true 
	  || isNotnum(dm.bgheight.value) == true || isNotnum(dm.pageno.value) == true){
		alert("데이터형식이 맞지 않습니다. 숫자이외의 문자를 제거하십시오.");
		return false;
	}

	if(dm.shopcolor.value != "" && dm.shopcolor.value.length != 6){
		alert("6자리수 웹칼라로 표현해주시기 바랍니다.");
		dm.shopcolor.focus();
		return false;
	}

	if(confirm("설정값을 저장하시겠습니까?")) return true;
	else return false;
}
</script>
</head>

<body onload="onload_func();">

<%@ include file="inc_menu.jsp" %>
<%
	String serviceCond = "";
	String unifyCond = "";

	f = new File(pathOfCata + "/catakind.txt");
	if(f.exists()){
		String t2cate = cook_cate.substring(0,2);
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String tcate = source.substring(0,10);
			if(tcate.equals(cook_cate)){
				serviceCond = source.substring(11,12);
			}
			else if(tcate.equals(cook_pcode)){
				unifyCond = source.substring(12,13);
			}
		}
		fin.close();
		bin.close();
	}

	String bgchoice = "";
	String bground = "";
	String blankcolor = "";
	String vartemp = "";

	Hashtable ahash = new Hashtable();
	f = new File(pathOfDir + "/ecatalog.txt");
	if(f.exists()){
		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);
			String sval = source.substring(in+1);

			ahash.put(skey, sval);
		}
		fin.close();
		bin.close();

		vartemp = ahash.containsKey("bground") ? (String)ahash.get("bground") : "";
		if(vartemp.indexOf("-") > 0 && vartemp.length() > 1){
			String[] aaa = mysplit(vartemp,"-");
			bgchoice = aaa[0];
			bground = aaa[1];
			if(aaa.length > 2) blankcolor = aaa[2];
		}
		
	}

	String varbskin = ahash.containsKey("bskin") ? (String)ahash.get("bskin") : "";
	if(varbskin.equals("")) varbskin = "001|001|001|001|001|001|001|001|001|001";
	String[] bskin = mysplit(varbskin, "|");

	String ini1var = ahash.containsKey("ini1var") ? (String)ahash.get("ini1var") : "";
	if(ini1var.equals("")) ini1var = "                      ";

	String ini2var = ahash.containsKey("ini2var") ? (String)ahash.get("ini2var") : "";
	if(ini2var.equals("")) ini2var = "      ";
	if(ini2var.length() < 7) ini2var += "      ";

	String pageno = ahash.containsKey("pageno") ? (String)ahash.get("pageno") : "";
	String smwidth = ahash.containsKey("smwidth") ? (String)ahash.get("smwidth") : "";
	String smheight = ahash.containsKey("smheight") ? (String)ahash.get("smheight") : "";
	String bgwidth = ahash.containsKey("bgwidth") ? (String)ahash.get("bgwidth") : "";
	String bgheight = ahash.containsKey("bgheight") ? (String)ahash.get("bgheight") : "";
	String reimage = ahash.containsKey("reimage") ? (String)ahash.get("reimage") : "";
	String frpage = ahash.containsKey("frpage") ? (String)ahash.get("frpage") : "";
	String showstart = ahash.containsKey("showstart") ? (String)ahash.get("showstart") : "";
	String shopcolor = ahash.containsKey("shopcolor") ? (String)ahash.get("shopcolor") : "";
	String shopstr = ahash.containsKey("shopstr") ? (String)ahash.get("shopstr") : "";
	String twitter = ahash.containsKey("twitter") ? (String)ahash.get("twitter") : "";
	String facebook = ahash.containsKey("facebook") ? (String)ahash.get("facebook") : "";
	String kakao = ahash.containsKey("kakao") ? (String)ahash.get("kakao") : "";
	String logolink = ahash.containsKey("logolink") ? (String)ahash.get("logolink") : "";
	String titlebar = ahash.containsKey("titlebar") ? (String)ahash.get("titlebar") : "";
	String startdate = ahash.containsKey("startdate") ? (String)ahash.get("startdate") : "";
	String enddate = ahash.containsKey("enddate") ? (String)ahash.get("enddate") : "";
	String outpage = ahash.containsKey("outpage") ? (String)ahash.get("outpage") : "";
	String prepage = ahash.containsKey("prepage") ? (String)ahash.get("prepage") : "";

	String sm_viewsize = "";
	String bg_viewsize = "";

	String fileext = "jpg";
	if(ini2var.charAt(2) == 'P') fileext = "png";

	ImageSize inImage = new ImageSize();
	int[] sizea = new int[2];

	f = new File(pathOfDir + "/s001.jpg");
	if(f.exists()){
		inImage.setImage(pathOfDir + "/s001.jpg");
		sizea[0] = inImage.getWidth();
		sizea[1] = inImage.getHeight();
		sm_viewsize = "작은 이미지 파일의 실제크기 : " + sizea[0] + " x " + sizea[1];

		if(smwidth.equals("")){
			smwidth = Integer.toString(sizea[0]);
			smheight = Integer.toString(sizea[1]);
		}
	}

	f = new File(pathOfDir + "/001.jpg");
	if(f.exists()){
		inImage.setImage(pathOfDir + "/001.jpg");
		sizea[0] = inImage.getWidth();
		sizea[1] = inImage.getHeight();
		bg_viewsize = "- 큰 이미지 파일의 실제크기 : " + sizea[0] + " x " + sizea[1];

		if(bgwidth.equals("")){
			bgwidth = Integer.toString(sizea[0]);
			bgheight = Integer.toString(sizea[1]);
		}
	}

	if(reimage.equals("")){
		f = new File(pathOfDir + "/reimage.jpg");
		if(f.exists()) reimage = "reimage.jpg";
		else{
			f = new File(pathOfDir + "/reimage.gif");
			if(f.exists()) reimage = "reimage.gif";
			else{
				f = new File(pathOfDir + "/reimage.png");
				if(f.exists()) reimage = "reimage.png";
			}
		}
	}
%>
<main id="maincnt" class="maincnt">
<form name="cataform" action="action.jsp" method="post" onsubmit="return checkup()">
<section>
<h3>기본 정보</h3>
<dl>
	<dt>최상위 분류</dt>
	<dd><%=curr_catimage%></dd>
	<dt>폴더값 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
	  class="helpshow" onmouseover="show_helpdiv(this, 'help_folder')" onmouseout="hide_helpdiv();"></dt>
	<dd><%=cook_dir%></dd>
	<dt>링크URL <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
	  class="helpshow" onmouseover="show_helpdiv(this, 'help_linkurl')" onmouseout="hide_helpdiv();"></dt>
	<dd>&lt;a href="javascript:ecatalog('<%=scname%>','fixed','<%=cook_dir%>&amp;catimage=<%=cook_catimage%>');"&gt; &lt;/a&gt;</dd>
	<dt>서비스 상태</dt>
	<dd><label><input type="checkbox" name="fservice" value="D" id="id_fservice" <% if(serviceCond.equals("D")) out.print("checked"); %>>서비스 중지</label></dd>
	<dt>서비스 기간</dt>
	<dd><input type="date" name="startdate" value="<%=startdate%>" size="10" class="px"> ~ 
		<input type="date" name="enddate" value="<%=enddate%>" size="10" class="px"> (시작 ~ 종료), 예) 2001-01-01</dd>
</dl>
<p id="help_folder" class="secthelp">'폴더값'은 카탈로그 관련파일이 위치하는 서버 상의 실제 디렉토리 이름입니다.</p>
<p id="help_linkurl" class="secthelp">'링크 URL'을 이용하여 HTML 문서에서 카탈로그에 하이퍼링크(&lt;a&gt;태그)를 생성할 때 사용하시면 됩니다.</p>
</section>

<section>
<h3>페이지 기본</h3>
<p class="title"><label for="pageno">페이지 수</label></p>
<p class="desc"><input type="number" name="pageno" id="pageno" value="<%=pageno%>" class="px" required></p>
<p class="title">작은그림 크기 <% if(!sm_viewsize.equals("")){ %><img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_smsize')" onmouseout="hide_helpdiv();"><% } %></p>
<p class="desc">
	<label>너비 <input type="number" name="smwidth" value="<%=smwidth%>" class="px" required></label>&nbsp;
	<label>높이 <input type="number" name="smheight" value="<%=smheight%>" class="px" required></label>&nbsp;
	<a href="design_effect.jsp"><img src="<%=confServer%>/pix/link.gif" width="5" height="9" alt=""> 창크기와 동기화 설정</a>
</p>
<p class="title">큰그림 크기 <% if(!bg_viewsize.equals("")){ %><img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_bgsize')" onmouseout="hide_helpdiv();"><% } %></p>
<p class="desc">
	<label>너비 <input type="number" name="bgwidth" value="<%=bgwidth%>" class="px" required></label>&nbsp;
	<label>높이 <input type="number" name="bgheight" value="<%=bgheight%>" class="px" required></label>&nbsp;
	<a href="design_effect.jsp"><img src="<%=confServer%>/pix/link.gif" width="5" height="9" alt=""> 확대 설정</a>
</p>
<fieldset>
<legend class="title">파일 형식</legend>
<p class="desc">
	<label><input type="radio" name="fileext" value="J" id="id_ext1" checked>JPG</label>&nbsp;
	<label><input type="radio" name="fileext" value="P" id="id_ext2" <% if(ini2var.charAt(2) == 'P') out.print("checked"); %>>PNG</label>
</p>
</fieldset>
<p class="title"><label for="catpasswd">빈페이지 색상</label></p>
<p class="desc"><input type="text" name="blankcolor" id="blankcolor" size="8" maxlength="6" value="<%=blankcolor%>" class="px"></p>
<p class="title"><label for="outpage">외부 페이지</label> <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_outpage')" onmouseout="hide_helpdiv();"></p>
<p class="desc"><input type="text" name="outpage" id="outpage" size="50" value="<%=outpage%>" class="px"></p>
<!--<p class="title"><label for="prepage">무료 페이지</label> <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_prepage')" onmouseout="hide_helpdiv();"></p>
<p class="desc"><input type="text" name="prepage" size="50" value="" class="px"></p>-->
<p class="title">기타</p>
<p class="desc">
<label><input type="checkbox" name="onesmc" value="Y" id="id_onesmc" <% if(ini2var.charAt(5) == 'Y') out.print("checked"); %>>한장에 1페이지</label>&nbsp;
</p>
<p class="secthelp2">실제 작은그림은 다른 사이즈로 보여질 수 있습니다.</p>
<p id="help_outpage" class="secthelp">외부 페이지는 ','로 항목을 구분하고 '삽입을 시작할 페이지번호:폴더값'의 형식으로 입력합니다.<br>
예) 2:3,4:7...</p>
<p id="help_prepage" class="secthelp">무료 페이지는 '-'로 구간을 지정하고 ','로 항목을 구분합니다. ex) 0-4, 7-10, ...</p>
<% if(!sm_viewsize.equals("")){ %><p id="help_smsize" class="secthelp"><%=sm_viewsize%></p><% } %>
<% if(!bg_viewsize.equals("")){ %><p id="help_bgsize" class="secthelp"><%=bg_viewsize%></p><% } %>
</section>

<section>
<h3>시작과 끝</h3>
<fieldset>
<legend class="title">시작 페이지 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_startpage')" onmouseout="hide_helpdiv();"></legend>
<p class="desc">
	<label><input type="radio" name="frpage" value="0" id="id_frpage1" checked>0페이지</label>&nbsp;
	<label><input type="radio" name="frpage" value="1" id="id_frpage2" <% if(frpage.equals("1")) out.print("checked"); %>>1페이지</label>
</p>
</fieldset>
<p class="title">표시 시작페이지 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_startpage3')" onmouseout="hide_helpdiv();"></p>
<p class="desc"><input type="number" name="showstart" value="<%=showstart%>" class="px"></p>
<fieldset>
<legend class="title">끝 페이지 도달</legend>
<p class="desc">
	<label><input type="radio" name="lastpage" value="A" id="id_lastpage1" checked>중지</label>&nbsp;
	<label><input type="radio" name="lastpage" value="C" id="id_lastpage2" <% if(ini1var.charAt(12) == 'C') out.print("checked"); %>>첫 
	  페이지로 계속 연결</label>
</p>
</fieldset>
<p class="title">기타</p>
<p class="desc">
<label><input type="checkbox" name="anieffect" value="Y" id="id_ani" <% if(ini1var.charAt(14) == 'Y') out.print("checked"); %>>시작 애니메이션</label>
  <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명" class="helpshow" onmouseover="show_helpdiv(this, 'help_startpage2')" onmouseout="hide_helpdiv();">
</p>
<p id="help_startpage" class="secthelp">시작페이지를 '0'으로 설정하시면 첫페이지의 좌측에는 빈 페이지가 삽입됩니다.</p>
<p id="help_startpage2" class="secthelp">시작페이지를 '1'로 설정하시면 표지 애니메이션은 무시됩니다.</p>
<p id="help_startpage3" class="secthelp">'표시 시작페이지'를 설정하시면 설정한 페이지 부터 1페이지로 표시되며 설정된 페이지에서 카탈로그가 시작하게 됩니다.</p>
</section>

<section>
<h3>배경</h3>
<fieldset>
<legend class="title">선택</legend>
<p class="desc">
	<label for="id_bgchoice1"><input type="radio" name="bgchoice" value="color" id="id_bgchoice1" checked>색상</label>
	<label><input type="radio" name="bgchoice" value="image" id="id_bgchoice2" <% if(bgchoice.equals("image")) out.print("checked"); %>>그림 반복</label>&nbsp;
	<label><input type="radio" name="bgchoice" value="left" id="id_bgchoice3" <% if(bgchoice.equals("left")) out.print("checked"); %>>그림 좌측정렬</label>&nbsp;
	<label><input type="radio" name="bgchoice" value="center" id="id_bgchoice4" <% if(bgchoice.equals("center")) out.print("checked"); %>>그림 가운데정렬</label>&nbsp;
	<label><input type="radio" name="bgchoice" value="right" id="id_bgchoice5" <% if(bgchoice.equals("right")) out.print("checked"); %>>그림 우측정렬</label>
</p>
</fieldset>
<p class="title">색상 선택시 색상</p>
<p class="desc"><input type="text" name="bground" value="<%=bground%>" class="px" size="8" maxlength="6"> 
  <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명" class="helpshow" onmouseover="show_helpdiv(this, 'help_color')" onmouseout="hide_helpdiv();"></p>
<p class="title">그림 선택시</p>
<p class="desc"><a href="file_add.jsp?kd=bimg"><img src="<%=confServer%>/pix/link.gif" width="5" height="9" alt=""> 파일관리에서 "background.jpg"로 저장</a>
  <% f = new File(pathOfDir + "/background.jpg"); if(f.exists()){ %>&nbsp;<a href="file_det.jsp?fname=background.jpg">[파일보기]</a>]<% } %>
<p id="help_color" class="secthelp">16진수 RGB 웹칼라 형태, 6자리수</p>
</section>

<section>
<h3>링크</h3>
<p class="title">구분색상</p>
<p class="desc"><input type="text" name="shopcolor" value="<%=shopcolor%>" class="px" size="8" maxlength="6">
  <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명" class="helpshow" onmouseover="show_helpdiv(this, 'help_link')" onmouseout="hide_helpdiv();"></p>
<p class="title">문구</p>
<p class="desc"><input type="text" name="shopstr" value="<%=shopstr%>" class="px" size="50"></p>
<p id="help_link" class="secthelp">16진수 RGB 웹칼라 형태, 6자리수, 'xxxxxx'는 투명</p>
</section>

<section>
<h3>SNS 문구</h3>
<p class="title">트위터</p>
<p class="desc"><input type="text" name="twitter" value="<%=twitter%>" class="px" size="80"></p>
<p class="title">페이스북</p>
<p class="desc"><input type="text" name="facebook" value="<%=facebook%>" class="px" size="80"></p>
<p class="title">카카오</p>
<p class="desc"><input type="text" name="kakao" value="<%=kakao%>" class="px" size="80"></p>
<p class="secthelp"></p>
</section>

<section>
<h3>웹브라우저 타이틀바 제목</h3>
<p class="desc2">
	<input type="text" name="titlebar" size="50" value="<%=titlebar%>" class="px">
</p>
</section>

<div class="repfloat">
<h3>대표 이미지</h3>
<% if(reimage.equals("")){ %>
<p>대표 이미지가 없습니다.</p>
<% } else{ %>
<p><img src="<%=webServer%>/catImage<%=cook_catimage%>/<%=cook_dir%>/<%=reimage%>" width="220" height="190" id="reimage" alt="대표이미지"></p>
<% } %>
<p>
	<a href="file_down.jsp"><img src="<%=confServer%>/pix/link.gif" width="5" height="9" alt=""> 설정</a> &nbsp;
	<a href="file_add.jsp?kd=cata"><img src="<%=confServer%>/pix/link.gif" width="5" height="9" alt=""> 파일 업로드</a>
</p>
</div>

<div class="submitbtn"><input type="submit" value="저장하기"></div>

<input type="hidden" name="pre_fservice" value="<%=serviceCond%>">
<input type="hidden" name="unifycond" value="<%=unifyCond%>">
<input type="hidden" name="cate" value="<%=cook_cate%>">
<input type="hidden" name="act" value="basic">
<input type="hidden" name="bskin" value="<%=bskin%>">
<input type="hidden" name="def_askin" value="001|001|001|001|001|001|001|001|001|001">
<input type="hidden" name="def_bskin" value="001|001|001|001|001|001|001|001|001|001">

</form>
</main>

<%@ include file="inc_copy.jsp" %>

