<%@ page pageEncoding="utf-8" %>
<%@ include file="lang.jsp" %>
<%
	request.setCharacterEncoding("utf-8");

	String sess_id = (String)session.getAttribute("sess_id");
	if(sess_id == null) sess_id = "";

	String admin_id = "";
	String cook_adminkd = "";
	String cook_catimage = "";

	if(!sess_id.equals("")){
		String[] temp_adminid = mysplit(sess_id,":");
		admin_id = temp_adminid[0];
		cook_adminkd = temp_adminid[1];
		cook_catimage = temp_adminid[2];
	}

	String cook_index = "cook_dir" + cook_catimage;
	String cook_cate = "";
	String cook_dir = "";

	if(request.getCookies() != null){
		Cookie[] cookies = request.getCookies();
		for(int i = 0;i < cookies.length;i++){
			Cookie theCookie = cookies[i];
			if(theCookie.getName().equals(cook_index)){
				String[] temp_cookindex = mysplit(cookDecode(theCookie.getValue()),"|");
				cook_cate = temp_cookindex[0];
				if(temp_cookindex.length > 1) cook_dir = temp_cookindex[1];
			}
		}
	}

	String variOfCata = "";
	String pathOfCata = webSysDir + "/catImage" + cook_catimage;
	String pathOfOriCata = webSysDir + "/catImage";
	String pathOfDir  = pathOfCata + "/" + cook_dir;
	String pathOfAcc  = webSysDir;
	String skinOfCata = "";

	String cook_pcode = cook_cate.equals("") ? "" : get_parentCode(cook_cate);
	String scname = request.getServletPath().substring(0, request.getServletPath().indexOf("/conf/"));
	String mblogLink = variOfCata.equals("mblog") ? "&tid="+admin_id : "";
	String param = cook_dir+"&catimage="+cook_catimage+mblogLink+"&callmode=admin";

	FileInputStream fin;
	BufferedReader bin;
	String source;
	File f;
%>
