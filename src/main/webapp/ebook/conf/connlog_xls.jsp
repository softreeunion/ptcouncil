<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat, java.text.NumberFormat" contentType="text/html;charset=euc-kr" %>
<%@ include file="admin.jsp" %>
<%
	String ccate = request.getParameter("ccate");
	if(ccate == null) ccate = "";

	//if(!ccate.equals("") && check_numalphaVar(ccate, 10) == false){
	//	out.print("ccate is not acceptable!");
	//	return;
	//}

	Vector aimDir = new Vector();

	String p2Code = "";
	if(!ccate.equals("")){
		if(ccate.substring(2,10).equals("00000000")) p2Code = ccate.substring(0,2);
		else p2Code = ccate.substring(0,4);
	}

	String currName = "";
	String path = pathOfCata + "/catakind.txt";
	f = new File(path);
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, "|");
			String t_cate = aaa[0];

			if(t_cate.substring(2,10).equals("00000000")){
				if(ccate.equals(t_cate)) currName = aaa[2];
			}
			else{
				if(p2Code.length() == 2 && t_cate.substring(0,2).equals(p2Code) && !t_cate.substring(2,4).equals("00")){
					aimDir.addElement(aaa[3]);
				}
				else if(p2Code.length() == 4 && t_cate.substring(0,4).equals(p2Code)){
					aimDir.addElement(aaa[3]);
				}
			}
			if(ccate.equals(t_cate)){
				currName = aaa[2];
			}
		}
		bin.close();
		fin.close();
	}

	// 날짜관련
	String cday = request.getParameter("cday");
	if(cday == null) cday = "";

	if(cday.equals("")){
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		cday = sdf.format(date);
	}

	String cyear = request.getParameter("cyear");
	if(cyear == null) cyear = "";
	if(cyear.equals("")) cyear = cday.substring(0,4);
	else{
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		int ccy = strToInt(sdf.format(date));

		if(check_year(cyear, ccy) == false){
			out.print("cyear is not acceptable!");
			return;
		}
	}

	String cmonth = request.getParameter("cmonth");
	if(cmonth == null) cmonth = "";
	if(cmonth.equals("")) cmonth = cday.substring(4,6);

	String cym = cyear + cmonth;

	Hashtable mcnt = new Hashtable();
	Hashtable bcnt = new Hashtable();
	int total = 0;
	int mtotal = 0;
	int maxcnt = 0;
	int maxmcnt = 0;
	StringBuffer conn_buf = new StringBuffer();
	path = pathOfAcc + "/log/" + cyear + "/" + cyear + ".txt";

	f = new File(path);
	if(f.exists()){
		if(ccate.equals("")){
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				String[] aaa = mysplit(source, "\t");
				String cymd = aaa[0];
				if(!cymd.substring(0,6).equals(cym)) continue;

				String t_day = cymd.substring(6,8);
				String hashkey = "skey" + t_day;

				int nd = strToInt(aaa[1]);
				if(mcnt.containsKey(hashkey)){
					String s1 = (String)mcnt.get(hashkey);
					nd += strToInt(s1);
					mcnt.put(hashkey, aaa[1]);
				}
				else{
					mcnt.put(hashkey, aaa[1]);
				}

				if(nd > maxcnt) maxcnt = nd;

				if(aaa[3] != null && !aaa[3].equals("")){
					nd = strToInt(aaa[3]);
					if(bcnt.containsKey(hashkey)){
						String s1 = (String)bcnt.get(hashkey);
						nd += strToInt(s1);
						bcnt.put(hashkey, aaa[3]);
					}
					else{
						bcnt.put(hashkey, aaa[3]);
					}
					if(nd > maxmcnt) maxmcnt = nd;
				}
			}
			bin.close();
			fin.close();
		}
		else{
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				String[] aaa = mysplit(source, "\t");
				String cymd = aaa[0];
				if(!cymd.substring(0,6).equals(cym)) continue;

				String t_day = cymd.substring(6,8);
				String hashkey = "skey" + t_day;
				int nd = 0;

				StringTokenizer st2 = new StringTokenizer(aaa[2], "|");
				while(st2.hasMoreTokens()){
					String t_conn = st2.nextToken();
					if(t_conn == null || t_conn.equals("")) continue;

					int in = t_conn.indexOf(":");
					String skey = t_conn.substring(0, in);
					String sval = t_conn.substring(in+1);

					if(aimDir.contains(skey)){
						nd += strToInt(sval);
					}
				}

				if(mcnt.containsKey(hashkey)){
					String s1 = (String)mcnt.get(hashkey);
					nd += strToInt(s1);
					mcnt.put(hashkey, Integer.toString(nd));
				}
				else{
					mcnt.put(hashkey, Integer.toString(nd));
				}

				if(nd > maxcnt) maxcnt = nd;

				if(aaa[3] != null && !aaa[3].equals("")){
					nd = 0;
					st2 = new StringTokenizer(aaa[4], "|");
					while(st2.hasMoreTokens()){
						String t_conn = st2.nextToken();
						if(t_conn == null || t_conn.equals("")) continue;

						int in = t_conn.indexOf(":");
						String skey = t_conn.substring(0, in);
						String sval = t_conn.substring(in+1);

						if(aimDir.contains(skey)){
							nd += strToInt(sval);
						}
					}

					if(bcnt.containsKey(hashkey)){
						String s1 = (String)bcnt.get(hashkey);
						nd += strToInt(s1);
						bcnt.put(hashkey, Integer.toString(nd));
					}
					else{
						bcnt.put(hashkey, Integer.toString(nd));
					}
					if(nd > maxmcnt) maxmcnt = nd;
				}

			}
			bin.close();
			fin.close();
		}

		NumberFormat nf = NumberFormat.getNumberInstance();
		Calendar cal = Calendar.getInstance();

		cal.set(Integer.parseInt(cyear), Integer.parseInt(cmonth)-1, 1);
		int week = cal.get(Calendar.DAY_OF_WEEK);
		cal.add(Calendar.MONTH, 1);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		int lastday = cal.get(Calendar.DAY_OF_MONTH);

		for(int i = 1;i <= lastday;i++){
			String aclass = "b2tdff";
			if(week == 8) week = 0;
			if(week == 0) aclass = "b2tdf7";

			String t_day = "" + i;
			if(i < 10) t_day = "0" + i;
			String hashkey = "skey" + t_day;

			conn_buf.append(i+"일\t");
			if(!mcnt.containsKey(hashkey) || maxcnt == 0){
				conn_buf.append("0\t0%\t");
			}
			else{
				String s1 = (String)mcnt.get(hashkey);
				int num = strToInt(s1);
				total += num;
				conn_buf.append(nf.format(num)+"\t"+(int)(num*100/maxcnt)+"%\t");
			}

			if(!bcnt.containsKey(hashkey) || maxmcnt == 0){
				conn_buf.append("0\t0%");
			}
			else{
				String s2 = (String)bcnt.get(hashkey);
				int bum = strToInt(s2);
				mtotal += bum;

				conn_buf.append(nf.format(bum)+"\t"+(int)(bum*100/maxmcnt)+"%");
			}
			conn_buf.append("\n");

			week++;
		}
	}

	response.setHeader("Content-Type","application/vnd.ms-excel");
	response.setHeader("Content-disposition", "attachment;filename=connlog.xls");

	if(currName.equals("")) currName = "전체";
	else{
		if(charset.equals("utf-8")) currName = new String(currName.getBytes("euc-kr"), "euc-kr");
	}

	out.print("'" + currName + "'의 " + cyear + "년 " + cmonth + "월 접속로그\n");
	out.print("일자\t전체접속수\t백분율(최고대비)\t모바일접속수\t백분율(최고대비)\n");
	out.print(conn_buf.toString());
	out.print("합\t"+total+"\t\t"+mtotal+"\t\n");
%>
