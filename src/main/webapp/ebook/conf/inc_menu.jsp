<%@ page pageEncoding="utf-8" %>
<script>
var outCode = "<%=cook_cate%>";
var outMoveKind = "<%=subar[2]%>";
confServer = "<%=confServer%>";
webServer = "<%=webServer%>";
</script>

<div id="divmessage"></div>

<div id="context"></div>
<div id="processing"></div>
<div id="dragmove"></div>

<header class="docheader">
<div class="hdrlogo"><img src="<%=confServer%>/pix/logo.gif" width="149" height="45" alt="로고"></div>
<div class="hdrlogout"><a href="logact.jsp?act=logout"><img src="<%=confServer%>/pix/tm_logout.gif" width="34" height="12" alt="로그아웃"></a></div>
<nav>
<ul>
	<li onmouseover="tmenu_mouseOver(event,this,0);"><a href="pcata_basic.jsp"><img 
		src="<%=confServer%>/pix/tm_basic<% if(subar[0].equals("basic")) out.print("_over"); %>.png" width="95" height="22" alt="기본환경"></a>
	<ul id="tm_general" class="t2menu">
		<li onclick="simpleLink('pcata_cate.jsp?cate=<%=cook_pcode%>',1);"><a href="pcata_cate.jsp?cate=<%=cook_pcode%>">분류 설정</a></li>
		<li onclick="simpleLink('pcata_basic.jsp',1);"><a href="pcata_basic.jsp">카탈로그 설정</a></li>
		<li onclick="simpleLink('pcata_cont.jsp',1);"><a href="pcata_cont.jsp">내용구성</a></li>
		<% if(cook_adminkd.equals("1")){ %>
		<li onclick="simpleLink('pcata_dist.jsp',1);"><a href="pcata_dist.jsp">최상위 분류</a></li>
		<% } %>
	</ul></li>

	<li onmouseover="tmenu_mouseOver(event,this,1);"><a href="design_skin.jsp"><img 
		src="<%=confServer%>/pix/tm_design<% if(subar[0].equals("design")) out.print("_over"); %>.png" width="106" height="22" alt="디자인/효과"></a>
	<ul id="tm_design" class="t2menu">
		<li onclick="simpleLink('design_skin.jsp',1);"><a href="design_skin.jsp">스킨</a></li>
		<li onclick="simpleLink('design_effect.jsp',1);"><a href="design_effect.jsp">효과</a></li>
		<li onclick="simpleLink('logo.jsp',1);"><a href="logo.jsp">로고</a></li>
		<li onclick="simpleLink('logo_def.jsp',1);"><a href="logo_def.jsp">기본 로고</a></li>
		<li onclick="simpleLink('design_expert.jsp',1);"><a href="design_expert.jsp">전문가 설정</a></li>
	</ul></li>

	<li onmouseover="tmenu_mouseOver(event,this,2);"><a href="link_title.jsp"><img 
		src="<%=confServer%>/pix/tm_link<% if(subar[0].equals("link")) out.print("_over"); %>.png" width="71" height="22" alt="링크"></a>
	<ul id="tm_link" class="t2menu">
		<li onclick="simpleLink('link_title.jsp',1);"><a href="link_title.jsp">링크 목록</a></li>
		<li onclick="simpleLink('link_page.jsp',1);"><a href="link_page.jsp">페이지 링크</a></li>
		<li onclick="simpleLink('link_sound.jsp',1);"><a href="link_sound.jsp">페이지 소리</a></li>
		<!--<li onclick="simpleLink('picdata.jsp',1);"><a href="picdata.jsp">페이지 정보</a></li>-->
		<li onclick="simpleLink('contents.jsp',1);"><a href="contents.jsp">목차</a></li>
		<li onclick="simpleLink('search.jsp',1);"><a href="search.jsp">검색</a></li>
	</ul></li>

	<li onmouseover="tmenu_mouseOver(event,this,3);"><a href="file_catalist.jsp"><img 
		src="<%=confServer%>/pix/tm_file<% if(subar[0].equals("file")) out.print("_over"); %>.png" width="73" height="22" alt="파일"></a>
	<ul id="tm_file" class="t2menu">
		<li	onclick="simpleLink('file_catalist.jsp',1);"><a href="file_catalist.jsp">기본 파일</a></li>
		<li	onclick="simpleLink('file_soundlist.jsp',1);"><a href="file_soundlist.jsp">소리 파일</a></li>
		<li	onclick="simpleLink('file_imagelist.jsp',1);"><a href="file_imagelist.jsp">그림 파일</a></li>
		<li onclick="simpleLink('file_medialist.jsp',1);"><a href="file_medialist.jsp">음악 파일</a></li>
		<li onclick="simpleLink('file_flashlist.jsp',1);"><a href="file_flashlist.jsp">링크 파일</a></li>
		<li onclick="simpleLink('file_down.jsp',1);"><a href="file_down.jsp">기타 파일</a></li>
	</ul></li>

	<li onmouseover="tmenu_mouseOver(event,this,4);"><a href="mail.jsp"><img 
		src="<%=confServer%>/pix/tm_addon<% if(subar[0].equals("addon")) out.print("_over"); %>.png" width="89" height="22" alt="부가기능"></a>
	<ul id="tm_addon" class="t2menu">
		<li onclick="simpleLink('mail.jsp',1);"><a href="mail.jsp">메일</a></li>
		<li onclick="simpleLink('mail_def.jsp',1);"><a href="mail_def.jsp">기본 메일</a></li>
		<li onclick="simpleLink('connlog.jsp',1);"><a href="connlog.jsp">방문자 접속기록</a></li>
		<!--cdbreak:end1//-->
		<li onclick="simpleLink('etconf.jsp',1);"><a href="etconf.jsp">기타 설정</a></li>
	</ul></li>

	<li onmouseover="tmenu_mouseOver(event,this,5);"><a href="admin_add.jsp"><img 
		src="<%=confServer%>/pix/tm_admin<% if(subar[0].equals("admin")) out.print("_over"); %>.png" width="80" height="22" alt="관리자"></a>
	<ul id="tm_admin" class="t2menu">
		<li onclick="simpleLink('admin_add.jsp',1);"><a href="admin_add.jsp">추가/변경</a></li>
		<li onclick="simpleLink('admin_connlog.jsp',1);"><a href="admin_connlog.jsp">접속기록</a></li>
	</ul></li>

	<li><a href="help.jsp"" class="tmpixa"><img 
		src="<%=confServer%>/pix/tm_help<% if(subar[0].equals("help")) out.print("_over"); %>.png" width="84" height="22" alt="도움말"></a></li>
</ul>
</nav>

<%
	String selc_buf = "";
	String curr_catimage = "기본 분류";
	if(cook_adminkd.equals("1") || cook_adminkd.equals("2")){
		String path = pathOfOriCata + "/catimage.txt";
		f = new File(path);
		if(f.exists()){
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				String[] aaa = mysplit(source, "\t");
				String selc = "";
				if(cook_catimage.equals(aaa[0])){
					selc = "selected";
					curr_catimage = aaa[2];
				}
				selc_buf += "<option value=\""+aaa[0]+"\" "+selc+">"+aaa[2]+"</option>\n";
			}
			fin.close();
			bin.close();
		}
	}
%>
<h1><%=curr_catimage%><% if(!selc_buf.equals("")){ %><a href="javascript:show_basicselect('basicselect');"><span class='btn'>(▼)</span></a><% } %></h1>
<h2 id="explhier"></h2>
<h3><% if(!subar[1].equals("")) out.print("> "+subar[1]); %></h3>

<div class="cataview">
<% if(!cook_dir.equals("")){ %>
	<!--flashbreak:end1//-->
	<!--html5break:end1//-->
	<!--bothbreak1//-->
	<span>미리보기 :</span>
	<a href="javascript:ecatalog('<%=scname%>','fixed','<%=param%>','');"><img src="<%=confServer%>/pix/view_flashbook.png" width="45" height="18" alt="카탈로그 보기"></a>
	<a href="javascript:ecatalog('*<%=scname%>','fixed','<%=param%>','');"><img src="<%=confServer%>/pix/view_html5book.png" width="45" height="18" alt="카탈로그 보기"></a>
	<!--bothbreak:end1//-->
<% } %>
</div>

<% if(!selc_buf.equals("")){ %>
<div id="basicselect">
<form name="menumoveform" method="get" action="<%=request.getServletPath()%>">
<select name="cataimage" onchange="go_selcform(document.menumoveform, this, '<%=cook_catimage%>')">
	<option value="B" <% if(cook_catimage.equals("")) out.print("selected"); %>>기본 분류</option>
	<%=selc_buf%>
</select>
</form>
</div>
<% } %>
</header>

<div id="maincontainer" class="maincontainer">
<div id="explctn">
<form name="sclassform" method="post" action="#" onsubmit="return false;">
<div id="sclass"></div>
</form>
</div>
<div id="sdivider" onmousedown="sdiv_mouseDown(event);"></div>
