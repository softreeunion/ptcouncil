<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	Hashtable ahash = new Hashtable();
	f = new File(pathOfOriCata + "/bookcase.txt");
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);
			String sval = source.substring(in+1);

			ahash.put(skey, sval);
		}
		fin.close();
		bin.close();
	}

	String bcskin = ahash.containsKey("bcskin") ? (String)ahash.get("bcskin") : "";
	String ini1var = ahash.containsKey("ini1var") ? (String)ahash.get("ini1var") : "";
	String basicnick = ahash.containsKey("basicnick") ? (String)ahash.get("basicnick") : "";

	char bcflash = 'N';
	if(ini1var.length() > 0) bcflash = ini1var.charAt(0);
	char catmove = 'N';
	if(ini1var.length() > 1) catmove = ini1var.charAt(1);

	String[] subar = {"addon", "기타설정", "name",""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 기타 설정</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	section label img{vertical-align:middle;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function checkup(){
	if(confirm("설정값을 저장하시겠습니까?")) return true;
	else return false;
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>

<main id="maincnt" class="maincnt">

<form name="effectform" action="action2.jsp" method="post" onsubmit="return checkup()">
<section>
<h3>책장</h3>
<fieldset>
<legend class="title">스킨</legend>
<p class="desc">
<label><input type="radio" name="bcskin" value="001" id="id_bcskin1" <% if(bcskin.equals("001")) out.print("checked"); %>>
	<img src="<%=webServer%>/skin/pix/sbookcase1.jpg" width="100" height="119" alt="책장스킨1"></label>
<label><input type="radio" name="bcskin" value="002" id="id_bcskin2" <% if(bcskin.equals("002")) out.print("checked"); %>>
	<img src="<%=webServer%>/skin/pix/sbookcase2.jpg" width="100" height="119" alt="책장스킨2"></label>
<label><input type="radio" name="bcskin" value="003" id="id_bcskin3" <% if(bcskin.equals("003")) out.print("checked"); %>>
	<img src="<%=webServer%>/skin/pix/sbookcase3.jpg" width="100" height="119" alt="책장스킨3"></label>
</p>
</fieldset>

<fieldset>
<legend class="title">분류 콤보 박스</legend>
<p class="desc">
<label><input type="radio" name="catmove" value="N" id="id_catmove1" <% if(catmove == 'N') out.print("checked"); %>>사용하지 않음</label>&nbsp;
<label><input type="radio" name="catmove" value="Y" id="id_catmove2" <% if(catmove == 'Y') out.print("checked"); %>>최상위 & 분류 선택 이동</label>&nbsp;
<label><input type="radio" name="catmove" value="C" id="id_catmove3" <% if(catmove == 'C') out.print("checked"); %>>분류만 선택 이동</label>
</p>
</fieldset>
<p class="title">최상위 & 분류 선택 이동시 '기본분류' 표시이름</p>
<p class="desc"><input type="text" name="basicnick" value="<%=basicnick%>" class="px" size="30"></p>
</section>

<div class="submitbtn"><input type="submit" value="저장하기"></div>
<input type="hidden" name="act" value="bookcase">
</form>

</main>

<%@ include file="inc_copy.jsp" %>
