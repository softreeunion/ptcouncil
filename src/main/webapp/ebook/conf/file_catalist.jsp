<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="admin.jsp" %>
<%
	if(cook_dir.equals("")){
		out.println(echo_script(snc_catalog("selfolder"), "basic.jsp", "",""));
		return;
	}

	String[] subar = {"file", "기본파일", "name", "cata"};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 기본파일</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	main table tr *:nth-of-type(5n+1){text-align:center;}
	main table tr *:nth-of-type(5n+2){text-align:left;}
	main table tr *:nth-of-type(5n+3){text-align:right;}
	main table tr *:nth-of-type(5n+4){text-align:center;}
	main table tr *:nth-of-type(5n+5){text-align:center;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function checkup(){
	var dm = document.delform;
	var jud = 0;
	for(i = 0;i < dm.elements.length;i++){
		if(dm.elements[i].type == "checkbox" && dm.elements[i].name != "ckall" && dm.elements[i].checked){
			jud++;
		}
	}

	if(jud == 0){
		alert("삭제하실 파일을 선택하셔야 합니다.");
		return false;
	}

	if(confirm("선택하신 파일을 삭제하시겠습니까?")) return true;
	else return false;
}
function checkall(obj){
	var dm = document.delform;
	var bool = (obj.checked == true) ? true : false;
	for(i = 0;i < dm.elements.length;i++){
		if(dm.elements[i].type == "checkbox") dm.elements[i].checked = bool;
	}
}
function checkalldel(){
	if(confirm("디렉토리 내 파일전체가 지워집니다. 계속 하시겠습니까?")) return true;
	else return false;
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>
<%
	String AddLink = "";
	int Punit = 20;
	int Pg = 0;
	int Pnum = 0;

	Vector vt = new Vector();
	f = new File(pathOfDir);
	if(f.exists()){
		File[] folders = f.listFiles();
		for(int i = 0;i < folders.length;i++){
			String temp = folders[i].getName();
			if(!temp.equals(".") && !temp.equals("..") && !temp.endsWith("txt")){
				vt.addElement(temp);
			}
		}
	}

	int itemLen = 0;
	StringBuffer sbuf = new StringBuffer();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	if(vt.size() > 0){
		Collections.sort(vt);
		itemLen = vt.size();

		int[] bvar = new int[4];
		bvar = db_board_start(itemLen, Punit, request.getParameter("Pg"));
		int bb = bvar[0];
		int bf = bvar[1];
		Pg = bvar[2];
		Pnum = bvar[3];

		for(int i = 0;i < itemLen;i++){
			int counts = i + 1;
			if(bb < counts && counts <= bf){
				String temp = (String)vt.get(i);
				File f1 = new File(pathOfDir + "/" + temp);
				String dcont = sdf.format(new Date(f1.lastModified()));
				int fsize = (int)f1.length()/1000;

				String aclass = "b2tdff";
				if(i%2 == 0) aclass = "b2tdf7";

				sbuf.append("<tr><td>"+(i+1)+"</td>\n");
				sbuf.append("<td><a href=\"file_det.jsp?fname="+temp+"&amp;kd=cata\">"+temp+"</a></td>\n");
				sbuf.append("<td><a href=\"file_det.jsp?fname="+temp+"&amp;kd=cata\">"+fsize+" KB</a></td>\n");
				sbuf.append("<td>"+dcont+"</td>");
				sbuf.append("<td><input type=\"checkbox\" name=\"sd\" value=\""+temp+"\"></td></tr>\n");
			}
		}
	}
	else sbuf.append("<tr><td colspan=\"5\" class=\"b2tdff alignctr\">파일이 없습니다.</td></tr>\n");
%>
<main id="maincnt" class="maincnt">
<form name="delform" method="post" action="file_act.jsp" onsubmit="return checkup()">
<p class="table-title">기본 파일 목록 : 전체 <%=itemLen%>개</p>
<table>
	<colgroup>
		<col style="width:10%;"><col style="width:30%;"><col style="width:20%;"><col style="width:30%;"><col style="width:10%;">
	</colgroup>
	<thead>
	<tr>
		<th>&nbsp;</th>
		<th>파일이름</th>
		<th>크기</th>
		<th>최종변경일</th>
		<th>
			<input type="submit" value="삭제">
			<input type="checkbox" name="ckall" onclick="checkall(this)">
		</th>
	</tr>
	</thead>
	<%=sbuf%>
</table>
<p class="table-paging"><%=db_board_nextNumber(Pnum, Pg, AddLink, "file_catalist.jsp")%></p>
<input type="hidden" name="act" value="mdel">
</form>

<div class="table-qmenu">
<form name="delallform" method="post" action="file_act.jsp" onsubmit="return checkalldel()">
<p><a href="file_add.jsp?kd=<%=subar[3]%>"><img src="<%=confServer%>/pix/fileadd.png" width="59" height="19" alt="파일추가"></a></p>
<p>
	<input type="image" src="<%=confServer%>/pix/delall.gif" alt="디렉토리 내 파일 전체 지우기">
	<input type="hidden" name="act" value="alldel">
</p>
</form>
</div>

</main>

<%@ include file="inc_copy.jsp" %>
