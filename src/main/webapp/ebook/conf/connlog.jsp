<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	if(cook_dir.equals("")){
		out.println(echo_script(snc_catalog("selfolder"), "basic.jsp", "",""));
		return;
	}

	Hashtable kname = new Hashtable();
	StringBuffer idx_buf = new StringBuffer();
	int cateStep = get_cateStep(cook_cate);
	f = new File(pathOfCata + "/catakind.txt");
	if(f.exists()){
		int k = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source,"|");
			if(get_cateStep(aaa[0]) != cateStep || aaa[1].charAt(2) == '0') continue;

			kname.put(aaa[3],aaa[2]);
			idx_buf.append("idxbuf["+k+"] = \""+ source + "\";\n");
			k++;
		}
		bin.close();
		fin.close();
	}

	String cday = request.getParameter("cday");
	if(cday == null){
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		cday = sdf.format(date);
	}

	String cyear = request.getParameter("cyear");
	if(cyear == null) cyear = "";
	if(cyear.equals("")) cyear = cday.substring(0,4);

	StringBuffer sel_buf = new StringBuffer();
	f = new File(pathOfAcc + "/log");
	if(f.exists()){
		File[] folders = f.listFiles();
		for(int i = 0;i < folders.length;i++){
			String temp = folders[i].getName();
			if(!temp.equals(".") && !temp.equals("..") && folders[i].isDirectory()){
				String selc = cyear.equals(temp) ? "selected" : "";
				sel_buf.append("<option value=\"").append(temp).append("\" ").append(selc).append(">").append(temp).append("년</option>\n");
			}
		}
	}

	int total = 0;
	Hashtable cnt = new Hashtable();
	Vector vt = new Vector();
	StringBuffer subtotal = new StringBuffer();

	f = new File(pathOfAcc + "/log/log_" + cday + ".txt");
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, "\t");
			if(aaa[0].equals("0")) total = total + 1;
			if(cnt.containsKey(aaa[1])){
				int ints = Integer.parseInt((String)cnt.get(aaa[1]));
				ints++;
				cnt.remove(aaa[1]);
				cnt.put(aaa[1], ints+"");
			}
			else{
				vt.addElement(aaa[1]);
				cnt.put(aaa[1], "1");
			}
		}
		bin.close();
		fin.close();

		for(Enumeration e = vt.elements(); e.hasMoreElements();){
			String key = (String)e.nextElement();
			String val = (String)cnt.get(key);
			String name = (String)kname.get(key);

			if(subtotal.toString().equals(""))
				subtotal.append(name).append("(<b>").append(val).append("</b>)\n");
			else
				subtotal.append(", ").append(name).append("(<b>").append(val).append("</b>)\n");
		}      
	}

	String subar[] = {"addon", "방문자 접속기록", "name",""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 방문자 접속기록(1)</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	section label img{vertical-align:middle;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
var logPath = "<%=webServer%>/log";
var currYear = <%=cyear%>;
var catImage = "<%=cook_catimage%>";
var graphSvg;
var total = <%=total%>;

function svgLoaded(script){
}
function getIndex(){
	var idxbuf = new Array();
	<%=idx_buf%>
	return idxbuf;
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>

<main id="maincnt" class="maincnt">

<section>
<h3>현재 정보</h3>
<dl>
	<dt>오늘</dt>
	<dd><%=cday.substring(0,4)%>-<%=cday.substring(4,6)%>-<%=cday.substring(6,8)%></dd>
	<dt>전체 접속수</dt>
	<dd><b><%=total%></b></dd>
	<dt>현재 폴더 접속수</dt>
	<dd><% if(total != 0) out.print(subtotal.toString()); %></dd>
</dl>
<p class="h3right"><a href="connlog2.jsp">숫자로 보기</a></p>
<p class="secthelp2">
	- 최초 접속수는 카탈로그에 최초로 접속한 숫자이며, 개별 접속수는 개별 카탈로그에 접속한 숫자입니다.<br>
	- 최초 접속한 후 카탈로그 상에서 이동하면 개별접속으로 기록됩니다. 그러므로 최초접속과 개별접속의 합계는 같지 않습니다.
</p>
</section>

<section>
<h3>년도별 그래프</h3>
<object id="logsvg" width="700" height="450" data="<%=confServer%>/conngraph.svg" type="image/svg+xml"></object>
<div class="h3rightcombo">
<form name="cntform" method="get" action="connlog.jsp">
<select name="cyear" onchange="document.cntform.submit();">
	<%=sel_buf.toString()%>
</select>
</form>
</div>
<p class="secthelp2">
- x축의 월을 클릭하시면 해당 월의 그래프와 접속수를 보실 수 있습니다.<br>
- 해당 월 그래프의 일자를 클릭하시면 일별 폴더별/시간별 통계를 보실 수 있습니다.<br>
- 범례의 텍스트를 클릭하시면 해당 그래프가 차례로 숨겨짐/보여짐을 반복합니다.<br>
- 그래프는 전체 접속수만 표시합니다. 모바일 접속수는 상단의 '숫자로 보기'를 클릭하시기 바랍니다.
</p>
</section>
</main>

<%@ include file="inc_copy.jsp" %>
