<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	String fname = request.getParameter("fname");
	StringBuffer cont = new StringBuffer();

	if(fname.equals("display.txt")) f = new File(pathOfOriCata + "/" + fname);
	else f = new File(pathOfDir + "/" + fname);
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			cont.append(source+"\n");
		}
		fin.close();
		bin.close();
	}
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - xml file(새창)</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	textarea.nx{width:95%;height:500px;border:1px solid #000000;background-color:#E8FAFF;overflow-y:scroll;font-family:Consolas,Courier New,돋움;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script>
function check_up(){
	var dm = document.editform;
	if(dm.xmldata.value == ""){
		alert("내용이 없습니다.");
		return false;
	}

	if(!confirm("변경하시겠습니까?")) return false;
}
</script>
</head>

<body>

<main class="openedcnt">
<form name="editform" method="post" action="action2.jsp" onsubmit="return check_up()">

<p><%=fname%> 파일편집<br>
<textarea name="xmldata" class="nx"><%=strTrim(cont.toString())%></textarea>
</p>
<div class="submitbtn2"><input type="submit" value="저장하기"></div>

<input type="hidden" name="fname" value="<%=fname%>">
<% if(fname.equals("display.txt")){ %>
<input type="hidden" name="act" value="txtmod">
<% } else{ %>
<input type="hidden" name="act" value="xmlmod">
<% } %>
</form>

</main>

</body>
</html>
