<%@ page pageEncoding="utf-8" %>
<%!
	String charset = "utf-8";
	String streamCharset = "utf-8";
	String lolang = "korean";
	String default_font = "굴림";
	String default2_font = "돋움";
	String od_platform = "both";

	String webServer = "/ebook";
	String webSysDir = "/tomcat9/webapps/ROOT/ebook";
	String wasServer = "/ebook";
	String wasSysDir = "/tomcat9/webapps/ROOT/ebook";

	String confServer = webServer + "/conf";
	String passSysDir = webSysDir + "/catImage/PasSaD.jsp";

	public String echo_script(String para_str, String loc, String wind, String addscript){
		String restr = "<!DOCTYPE html>\n"
		+ "<html lang=\"ko\">\n"
		+ "<head>\n"
		+ "<meta charset=\"utf-8\">\n"
		+ "<title>eCatalog conf - " + para_str + "</title>\n"
		+ "</head>\n"
		+ "<body>\n"
		+ "<script>\n";
		if(!para_str.equals("")) restr += "alert(\"" + para_str + "\");\n";
		if(!loc.equals("")){
			if(wind.equals("")) wind = "window";
			restr += wind + ".location = \"" + loc + "\";\n";
		}
		if(!addscript.equals("")) restr += addscript;
		restr += "</script>\n"
		+ "</body>\n"
		+ "</html>\n";
		return restr;
	}

	public String echo_text(String para_str){
		return "<!DOCTYPE html>\n"
		+ "<html lang=\"ko\">\n"
		+ "<head>\n"
		+ "<meta charset=\"utf-8\">\n"
		+ "<title>eCatalog conf - " + para_str + "</title>\n"
		+ "</head>\n"
		+ "<body>\n"
		+ "<p>" + para_str + "</p>\n"
		+ "</body>\n"
		+ "</html>\n";
	}

	public String get_date(){
		return "yyyy년 MM월 dd일 HH시 mm분";
	}

	public String snc_catalog(String para_str){
		if(para_str.equals("selfolder")) return "먼저 좌측 탐색기에서 카탈로그를 선택해주세요.";
		else if(para_str.equals("basicconf"))		return "먼저 기본환경을 설정하셔야 합니다.";
		else if(para_str.equals("saved"))			return "설정값이 저장되었습니다.";
		else if(para_str.equals("saved2"))			return "저장되었습니다.";
		else if(para_str.equals("deleted"))			return "삭제되었습니다.";
		else if(para_str.equals("added"))			return "추가되었습니다.";
		else if(para_str.equals("changed"))			return "변경되었습니다.";
		else if(para_str.equals("ordered"))			return "순서가 변경되었습니다.";
		else if(para_str.equals("notform"))			return "데이터형식이 맞지 않습니다.";
		else if(para_str.equals("setidpass"))		return "아이디와 비밀번호를 설정하셔야 합니다.";
		else if(para_str.equals("shouldlogin"))		return "로그인하셔야 합니다.";
		else if(para_str.equals("permission"))		return "디렉토리 퍼미션이 맞지 않습니다. 퍼미션을 변경하여 주십시오.";
		else if(para_str.equals("uploaderror"))		return "파일업로드시 에러가 발생하였습니다.";
		return "snc_catalog";
	}

	public static String get_itemKd(String s){
		if(s.equals("0")) return "category";
		else if(s.equals("C")) return "catalog";
		else if(s.equals("G")) return "gallery";
		return "category";
	}

	public static int get_cateStep(String cate){
		if(cate.substring(2,10).equals("00000000")) return 1;
		else if(cate.substring(4,10).equals("000000")) return 2;
		else if(cate.substring(6,10).equals("0000")) return 3;
		else if(cate.substring(8,10).equals("00")) return 4;
		return 5;
	}

	public static String get_parentCode(String cate){
		if(cate.substring(2,10).equals("00000000")) return "0000000000";
		else if(cate.substring(4,10).equals("000000")) return cate.substring(0,2)+"00000000";
		else if(cate.substring(6,10).equals("0000")) return cate.substring(0,4)+"000000";
		return "0000000000";
	}
	public boolean check_numalphaVar(String s, int cn){
		int alen = s.length();
		if(cn != -1){
			if(alen != cn) return false;
		}

		for(int i = 0;i < alen;i++){
			char c = s.charAt(i);
			if((c >= '0' && c <= '9') || (c >= 'A' && c <= 'z')) continue;
			else return false;
		}
		return true;
	}
	public boolean check_numVar(String s){
		int alen = s.length();
		for(int i = 0;i < alen;i++){
			char c = s.charAt(i);
			if((c >= '0' && c <= '9')) continue;
			else return false;
		}
		return true;
	}

	public boolean validate_email(String email){
		boolean valid = false;
		java.util.regex.Pattern p = java.util.regex.Pattern.compile("^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$");
		java.util.regex.Matcher m = p.matcher(email);
		if(m.matches()) valid = true;
		return valid;
	}

	public boolean validate_fname(String fname){
		java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("[^/./\\:*?\"<>|]");
		return !pattern.matcher(fname).find();
	}
	public String cookEncode(String s) throws java.io.IOException {
		if(s.equals("")) return s;

		String arstr = "";
		String nostr = "";
		for(int i = 0;i < s.length();i++){
			char c = s.charAt(i);
			int ts = (int)c;
			arstr += ts;
			nostr += String.valueOf(ts).length();
		}

		s = "";
		for(int i = 0;i < arstr.length();i++){
			int n = strToInt(arstr.substring(i,i+1));
			if(n > 0) n = 10 - n;
			s += n;
		}
		return base64Encode(nostr+"0"+s);
	}
	public String cookDecode(String s) throws java.io.IOException {
		if(s == null || s.equals("")) return "";

		String str = base64Decode(s);
		String nostr = "";
		String arstr = "";
		for(int i = 0;i < str.length();i++){
			char c = str.charAt(i);
			if(c == '0'){
				nostr = str.substring(0, i);
				arstr = str.substring(i+1);
				break;
			}
		}
		str = "";
		for(int i = 0;i < arstr.length();i++){
			int n = Character.getNumericValue(arstr.charAt(i));
			if(n > 0) n = 10 - n;
			str += n;
		}

		String res = "";
		int j = 0;
		for(int i = 0;i < nostr.length();i++){
			int n = Character.getNumericValue(nostr.charAt(i));
			int c = strToInt(str.substring(j,j+n));
			res += (char)c;
			j = j + n;
		}
		return res;
	}

	public String base64Encode(String str) throws java.io.IOException {
		if(str == null || str.equals("")){
			return "";
		}
		else{
			sun.misc.BASE64Encoder encoder = new sun.misc.BASE64Encoder();
			byte[] b1 = str.getBytes("UTF-8");
			String result = encoder.encode(b1);
			return result;
		}
	}

	public String base64Decode(String str) throws java.io.IOException {
		if(str == null || str.equals("")){
			return "";
		}
		else{
			sun.misc.BASE64Decoder decoder = new sun.misc.BASE64Decoder();
			byte[] b1 = decoder.decodeBuffer(str);
			String result = new String(b1);
			return result;
		}
	}

	public String passEncode(String str) throws java.io.IOException {
		byte[] buf = digest("MD5", str.getBytes("UTF-8"));
		return base64Encode(new String(buf));
	}

	public String passDecode(String str) throws java.io.IOException {
		return base64Decode(str);
	}

	public byte[] digest(String alg, byte[] input) throws java.io.IOException {
		byte[] buf;

		try{
			java.security.MessageDigest md = java.security.MessageDigest.getInstance(alg);
			buf = md.digest(input);
		}
		catch(java.security.NoSuchAlgorithmException e){
			 buf = "".getBytes();
		}
		return buf;
	}

	public static int strToInt(String s){
		if(s == null || s == "") return 0;
		try{
			int n = Integer.parseInt(s);
			return n;
		}
		catch(NumberFormatException e){
		}
		return 0;
	}

	public static String arrayJoin(String glue, String array[]){
		String result = "";
		for (int i = 0; i < array.length; i++) {
			result += array[i];
			if(i < array.length-1) result += glue;
		}
		return result;
	}

	public static String[] mysplit(String s, String delimiter){
		java.util.Vector v = new java.util.Vector();
		if(s == null){
			return null;
		}
		else if(s == ""){
			v.addElement("");
		}
		else{
			int idx;
			idx = s.indexOf(delimiter);
			while(idx > -1){
				if(idx == 0) v.addElement("");
				else v.addElement(s.substring(0, idx));

				s = s.substring(idx+1);
				idx = s.indexOf(delimiter);
			}

			v.addElement(s);			
		}

		String[] aaa = new String[v.size()];
		for(int i = 0;i < v.size();i++){
			aaa[i] = (String)v.elementAt(i);
		}
		return aaa;
	}
	public boolean check_year(String s, int ccy){
		if(check_numVar(s) == false) return false;

		int tcy = strToInt(s);
		if(tcy == 0 || tcy > ccy) return false;
		return true;
	}
	public String repl_injection(String s){
		s = s.replaceAll("--", "");
		s = s.replaceAll(";", "");
		s = s.replaceAll("'", "");
		s = s.replaceAll("\"", "");
		s = s.replaceAll("\\\\", "");
		s = s.replaceAll(":", "");
		s = s.replaceAll("select", "");
		s = s.replaceAll("insert", "");
		s = s.replaceAll("union", "");
		s = s.replaceAll("and", "");
		return s;
	}
	public String strTrim(String s){
		int len = s.length();
		int i = 0;
		for(i = (len-1);i >= 0;i--){
			char c = s.charAt(i);
			if(c != ' ' && c != '\n' && c != '\r') break;
		}

		if(i == (len-1)) return s;
		else return s.substring(0, i+1);
	}

	public String replStr(String s, String a, String b){
		int la = a.length();
		int lb = b.length();
		int ls = s.length();
		String r = "";

		int i = 0;
		while(i < (ls-la+1)){
			if(s.substring(i,i+la).equals(a)){
				r += b;
				i += la;
			}
			else{
				r += s.charAt(i);
				i++;
			}
		}
		
		r += s.substring(i,ls);
		return r;
	}

	public int arraySearch(String subb, String[] ar){
		int kx = -1;
		for(int i = 0;i < ar.length;i++){
			if(ar[i] == null) continue;
			if(ar[i].equals(subb)){
				kx = i;
				break;
			}
		}
		return kx;
	}
   
    public String twoChar(int n){
        if(n < 10) return "0" + n;
        else return "" + n;
    }
	public String threeChar(int n){
		if(n < 10) return "00" + n;
		else if(n < 100) return "0" + n;
		else return "" + n;
	}
 
	public String get_extension(String str){
		int n = str.indexOf(".");
		if(n == -1) return "";
		return str.substring(n+1);
	}

	public String get_namepart(String str){
		int n = str.indexOf(".");

		if(n == -1) return "";
		return str.substring(0, n);
	}

	public String strTrimEach(String str){
		if(str == null) return "";
		if(str.equals("")) return "";

		String s = str.replaceAll("[\\r\\n ]+$","");
		s = s.replaceAll("^[\\r\\n ]+","");

		return s;
	}
%>
