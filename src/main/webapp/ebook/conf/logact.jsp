<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="lang.jsp" %>
<%
	String pathOfOriCata = webSysDir + "/catImage";

	String act = request.getParameter("act");
	if(act == null) act = "";

	File f = new File(passSysDir);
	if(!f.exists()){
		out.println(echo_script(snc_catalog("setidpass"), "idpass.jsp", "",""));
		return;
	}

	String sess_id = "";
	String admin_id = "";
	if(act.equals("logout")){
		sess_id = (String)session.getAttribute("sess_id");
		if(sess_id == null) sess_id = "";
		if(sess_id.equals("")){
			out.println(echo_script("로그인되지 않은 상태입니다.", "login.jsp", "",""));
			return;
		}
		else{
			session.removeAttribute("sess_id");
			out.println(echo_script("로그아웃 되었습니다.", "login.jsp", "",""));
			return;
		}
	}

	admin_id = request.getParameter("mid");
	if(admin_id == null) admin_id = "";
	admin_id = repl_injection(admin_id);

	String passwd = request.getParameter("passwd");
	if(passwd == null) passwd = "";
	passwd = passEncode(repl_injection(passwd));

	String idmem = request.getParameter("idmem");
	if(idmem == null) idmem = "";
	else if(!idmem.equals("1")) idmem = "1";

	String jspTags = "%" + ">";
	int j = 0;
	int idCheck = 0;
	int passCheck = 0;
	int adminkind = 0;				// 1:super, 2:general_super, 3:general
	String catimage = "";

	String source;
	FileInputStream fin = new FileInputStream(f);
	BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
	while((source = bin.readLine()) != null){
		source = strTrimEach(source);
		if(source.equals("")) continue;

		if(source.equals(jspTags)) j = 1;
		else if(j == 1){
			adminkind++;
			String[] admins = mysplit(source, ":");
			if(admin_id.equals(admins[0])){
				idCheck = 1;
				if(passwd.equals(admins[1])){
					passCheck = 1;
					if(admins.length > 2){
						if(!admins[2].equals("")) catimage = admins[2];
					}
				}
				break;
			}
		}
	}
	fin.close();
	bin.close();

	if(j == 0){
		out.println(echo_script("아이디와 비밀번호를 설정하셔야 합니다.", "idpass.jsp", "",""));
		return;
	}
	else if(idCheck == 0){
		out.println(echo_script("아이디를 확인하십시오.", "login.jsp", "",""));
		return;
	}
	else if(passCheck == 0){
		out.println(echo_script("비밀번호를 확인하십시오.", "login.jsp", "",""));
		return;
	}

	if(adminkind != 1){
		if(catimage.equals("")) adminkind = 2;
		else adminkind = 3;
	}

	sess_id = admin_id + ":" + adminkind + ":" + catimage;		// ID : kd : catimage
	session.setAttribute("sess_id", sess_id);

	if(idmem.equals("1")){
		Cookie c_admin_idmem = new Cookie("admin_idmem", admin_id);
		c_admin_idmem.setMaxAge(3600 * 24 * 30);
		response.addCookie(c_admin_idmem);
	}
	else{
		Cookie c_admin_idmem = new Cookie("admin_idmem", "");
		response.addCookie(c_admin_idmem);
	}

	Cookie c_upload = new Cookie("cook_upload", passEncode(admin_id+request.getRemoteAddr()));
	response.addCookie(c_upload);

	StringBuffer cont = new StringBuffer();
	f = new File(pathOfOriCata + "/conn.txt");
	if(f.exists()){
		int i = 9;
		StringBuffer s = new StringBuffer();
		fin = new FileInputStream(f);
		bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			if(in > -1){
				String taa = source.substring(0, in);
				if(admin_id.equals(taa)){
					if(i >= 1) s.append(source).append("\n");
					i--;
				}
			}
		}
		fin.close();
		bin.close();

		SimpleDateFormat sdf = new SimpleDateFormat(get_date());
		FileOutputStream fos = new FileOutputStream(pathOfOriCata + "/conn.txt");
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(s.toString() + admin_id + ":" + sdf.format(new Date()) + " FROM " + request.getRemoteAddr() + "\n");
		osw.close();
	}
	else{
		SimpleDateFormat sdf = new SimpleDateFormat(get_date());
		FileOutputStream fos = new FileOutputStream(pathOfOriCata + "/conn.txt");
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(admin_id + ":" + sdf.format(new Date()) + " FROM " + request.getRemoteAddr() + "\n");
		osw.close();
	}

	String cook_url = "";
	if(request.getCookies() != null){
		Cookie[] cookies = request.getCookies();
		for(int i = 0;i < cookies.length;i++){
			Cookie theCookie = cookies[i];
			if(theCookie.getName().equals("cook_url")){
				cook_url = theCookie.getValue();
				break;
			}
		}
	}

	if(!cook_url.equals("")){
		if(cook_url.indexOf("act") >= 0) out.print(echo_script("","basic.jsp","",""));
		else out.print(echo_script("",cook_url,"",""));
	}
	else out.print(echo_script("","index.jsp","",""));
%>
