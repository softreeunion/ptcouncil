<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="unify_func.jsp" %>
<%@ include file="admin.jsp" %>
<%
	String act = request.getParameter("act");
	if(act == null) act = "";

	String act_string = snc_catalog("saved");

	if(act.equals("catebasic")){
		String cate = request.getParameter("cate");
		if(cate == null) cate = "";

		if(check_numalphaVar(cate, 10) == false){
			out.print(echo_text(snc_catalog("notform")+"(cate:"+cate+")"));
			return;
		}

		f = new File(pathOfCata);
		if(variOfCata.equals("mblog")){
			if(!f.exists())	f.mkdirs();
		}
		else{
			if(!f.canWrite()){
				echo_script(snc_catalog("permission"),"pcata_cate.jsp?cate="+cate,"","");
				return;
			}
		}

		int cateStep = get_cateStep(cate);
		int plen = 2 * cateStep;
		String pnCode = cate.substring(0,plen);		// 자신의 앞쪽 코드
		int rlen = 10 - plen - 2;
		String r0 = "0000000000".substring(0,rlen);

		String pre_fservice = request.getParameter("pre_fservice");
		String fservice = request.getParameter("fservice");
		if(fservice == null) fservice = "A";

		String unify = request.getParameter("unify");
		if(unify == null) unify = "F";		// F: individual, U: unification

		fservice += unify;
		if(!fservice.equals(pre_fservice)){
			StringBuffer cont = new StringBuffer();
			f = new File(pathOfCata + "/catakind.txt");
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;
	
				String tcate = source.substring(0,10);
				if(tcate.equals(cate)){
					String[] aaa = mysplit(source, "|");		// cate|setting|name|etc
					cont.append(aaa[0]).append("|").append(fservice).append(aaa[1].substring(2,5)).append("|").append(aaa[2]).append("|").append(aaa[3]).append("\n");
				}
				else cont.append(source).append("\n");
			}
			fin.close();
			bin.close();
	
			FileOutputStream fos = new FileOutputStream(pathOfCata + "/catakind.txt");
			OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
			osw.write(cont.toString());
			osw.close();
		}

		String fmove = request.getParameter("fmove");
		if(fmove == null) fmove = "0";			// 0: move, 1: not allowed to move

		String onemove = request.getParameter("onemove");
		if(onemove == null) onemove = "N";		// Y: one kind move, N: not one kind move

		String catflash = request.getParameter("catflash");
		if(catflash == null) catflash = "N";		// 0: flash, H: html5, N: parent

		String dist = fmove + onemove + catflash + "00";

		String bascadapt = request.getParameter("bascadapt");
		if(bascadapt == null) bascadapt = "";

		String logoadapt = request.getParameter("logoadapt");
		if(logoadapt == null) logoadapt = "";

		String mailadapt = request.getParameter("mailadapt");
		if(mailadapt == null) mailadapt = "";

		String music_notapp = request.getParameter("music_notapp");
		if(music_notapp == null) music_notapp = "";

		String mfile_qty = request.getParameter("mfile_qty");

		String media_file = request.getParameter("sd");
		if(music_notapp.equals("1")) media_file = "none";

		String thiscont = cate+"\tcate\t"+dist+"\t"+bascadapt+"\t"+logoadapt+"\t"+mailadapt+"\t"+media_file+"\n";
		UnifyFunc unifyInfo = new UnifyFunc(pathOfCata, streamCharset);
		if(unify.equals("U")){
			thiscont += unifyInfo.get_unifContLine(cate);
		}

		String cont1 = unifyInfo.get_unifyCont(pathOfCata+"/category.txt", cate);

		FileOutputStream fos = new FileOutputStream(pathOfCata + "/category.txt");
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont1);
		osw.write(thiscont);
		osw.close();

		out.println(echo_script(act_string, "pcata_cate.jsp?cate=" + cate, "",""));
	}
	else if(act.equals("mail") || act.equals("mail_def")){
		String mailskin = request.getParameter("mailskin");
		String copyright = strTrimEach(request.getParameter("copyright"));
		String mbody = strTrimEach(request.getParameter("mbody"));

		String path = pathOfCata + "/mail.txt";
		if(act.equals("mail")) path = pathOfDir + "/mail.txt";

		if(mbody.equals("") && copyright.equals("")){
			f = new File(path);
			if(f.exists()) f.delete();
		}

		FileOutputStream fos = new FileOutputStream(path);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write("mailskin:" + mailskin + "\ncopyright:" + copyright + "\n\n" + mbody);
		osw.close();

		if(act.equals("mail")) out.println(echo_script(act_string, "mail.jsp", "",""));
		else if(act.equals("mail_def")) out.println(echo_script(act_string, "mail_def.jsp", "",""));
	}
	else if(act.equals("maildel")){
		f = new File(pathOfDir + "/mail.txt");
		if(f.exists()) f.delete();

		out.println(echo_script(snc_catalog("deleted"), "mail.jsp", "",""));
	}
	else if(act.equals("bookcase")){
		String bcskin = request.getParameter("bcskin");
		String bcflash = request.getParameter("bcflash");
		String catmove = request.getParameter("catmove");
		String basicnick = request.getParameter("basicnick");

		if(bcflash == null) bcflash = "";
		if(bcflash.equals("")) bcflash = "N";

		if(catmove == null) catmove = "";
		if(catmove.equals("")) catmove = "N";

		if(basicnick == null) basicnick = "";

		FileOutputStream fos = new FileOutputStream(pathOfOriCata + "/bookcase.txt");
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write("bcskin:"+bcskin+"\nini1var:"+bcflash+catmove+"\nbasicnick:"+basicnick);
		osw.close();

		out.println(echo_script(act_string, "etconf.jsp", "",""));
	}
	else if(act.equals("picdata")){
		String sdfname = request.getParameter("sdfname");
		String Pg = request.getParameter("Pg");

		if(sdfname.equals("")){
			echo_text(snc_catalog("notform"));
			return;
		}

		String[] asdfname = mysplit(sdfname,":");

		f = new File(pathOfDir + "/picdata.txt");

		// 페이징을 적용하였기 때문에
		StringBuffer cont = new StringBuffer();
		if(f.exists()){
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				int in = source.indexOf(":");
				String skey = source.substring(0, in);
				//String sval = source.substring(in+1);

				if(arraySearch(skey, asdfname) == -1){
					cont.append(source).append("\n");
				}
			}
			fin.close();
			bin.close();
		}

		int alen = asdfname.length;
		for(int i = 1;i < alen;i++){
			String fname = asdfname[i];
			String minusext = fname.substring(0, fname.indexOf("."));

			String pdext = request.getParameter("pd"+minusext);
			if(pdext == null) pdext = "";

			String linestr = pdext.replaceAll("\n","");
			linestr = linestr.replaceAll("\r","");
			cont.append(fname+"\t"+linestr+"\n");
		}

		FileOutputStream fos = new FileOutputStream(f);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.close();

		out.println(echo_script(act_string, "picdata.jsp?Pg="+Pg, "",""));
	}
	else if(act.equals("sounddata")){
		String cont = request.getParameter("cont");

		String path = pathOfDir + "/pagesound.txt";
		FileOutputStream fos = new FileOutputStream(path);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont);
		osw.close();

		out.println(echo_script(act_string, "link_sound.jsp", "",""));
	}
	else if(act.equals("fileseqcont")){
		String cont = request.getParameter("cont");

		String path = pathOfDir + "/fileseq.txt";
		FileOutputStream fos = new FileOutputStream(path);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont);
		osw.close();

		out.println(echo_script(act_string, "pcata_cont.jsp", "",""));
	}
	else if(act.equals("xmlmod")){
		String fname = request.getParameter("fname");
		f = new File(pathOfDir+"/"+fname);
		if(!f.exists()){
			out.print(echo_script("편집할 파일이 존재하지 않습니다.", "nw_xmledit.jsp?fname="+fname,"",""));
			return;
		}

		String xmldata = request.getParameter("xmldata");
		if(xmldata == null) xmldata = "";

		String resdata = "";
		int jud = 0;

		int alen = xmldata.length();
		for(int i = 0;i < alen;i++){
			if(jud == 0 && xmldata.substring(i,9).equals("<content>")){
				resdata += "<content>";
				i += 9;
				jud = 1;
				continue;
			}
			else if(jud == 1 && xmldata.substring(i,10).equals("</content>")){
				resdata += "</content>";
				i += 10;
				jud = 0;
				continue;
			}

			String c = xmldata.substring(i,1);
			if(jud == 1 && c.equals("<")) resdata += "&lt;";
			else resdata += c;
		}

		FileOutputStream fos = new FileOutputStream(f);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(resdata);
		osw.close();

		out.print(echo_script(snc_catalog("changed"), "nw_xmledit.jsp?fname="+fname,"",""));
	}
	else if(act.equals("txtmod")){
		String fname = request.getParameter("fname");
		f = new File(pathOfOriCata+"/"+fname);
		String xmldata = request.getParameter("xmldata");

		FileOutputStream fos = new FileOutputStream(f);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(xmldata);
		osw.close();

		out.print(echo_script(snc_catalog("changed"), "nw_xmledit.jsp?fname="+fname,"",""));
	}
%>
