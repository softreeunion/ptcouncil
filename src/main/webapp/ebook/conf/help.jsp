<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	String[] subar = {"help", "", "name", ""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 도움말</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
var helpWin;
function view_help(){
	if(helpWin && !helpWin.closed) helpWin.focus();
	else helpWin = newWindow(3,'<%=webServer%>/manual/index.html',1000,700,0,0,'help');
}
function onload_func2(){
	view_help();
	onload_func();
}
</script>
</head>

<body onload="onload_func2();">
<%@ include file="inc_menu.jsp" %>

<main id="maincnt" class="maincnt">
	<p style="margin:150px 0 150px 0;line-height:150%;color:#999999;width:100%;text-align:center;">
		도움말은 별도창에서 실행됩니다.<br>창이 보이지 않으시면 아래의 실행하기 버튼을 클릭하시기 바랍니다.<br>
		<input type="button" value="도움말 실행하기" onclick="view_help();">
	</p>
</main>

<%@ include file="inc_copy.jsp" %>
