<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="admin.jsp" %>
<%
	String Pg = request.getParameter("Pg");

	String act = request.getParameter("act");
	if(act == null) act = "";

	String catimage = request.getParameter("catimage");
	if(catimage == null) catimage = "";

	if(act.equals("") && catimage.equals("")){
		response.sendRedirect("pcata_dist.jsp");
		return;
	}

	String kact = "추가";
	String submitStr = "추가하기";

	if(act.equals("mod") && !catimage.equals("")){
		kact = "변경";
		submitStr = "변경하기";
	}

	String[] subar = {"basic", "최상위 분류 "+kact, "",""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 최상위 분류</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function checkup(obj, kd){
	if(trimVal(obj.name.value) == ""){
		alert("제목을 입력해주세요.");
		obj.name.focus();
		return false;
	}

	if(kd == "add") ask = "추가하시겠습니까?";
	else ask = "변경하시겠습니까?";

	if(!confirm(ask)) return false;
}
</script>
</head>

<body onload="onload_func('pcata_dist.jsp');">

<%@ include file="inc_menu.jsp" %>
<%
	String VARdist = "";
	String VARname = "";
	String VARissuedate = "";

	f = new File(pathOfOriCata + "/catimage.txt");
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, "\t");
			if(aaa[0].equals(catimage)){
				VARdist = aaa[1];
				VARname = aaa[2];
				VARissuedate = aaa[3];
			}
		}

		fin.close();
		bin.close();
	}

	if(VARdist.equals("")) VARdist = "     ";
%>
<main id="maincnt" class="maincnt">

<form name="addform" method="post" action="dist_act.jsp" onsubmit="return checkup(this, '<%=act%>')">
<p class="table-title">최상위 분류 <%=kact%></p>
<p><label>제목<br>
<input type="text" name="name" size="50" value="<%=VARname%>" class="px" required></label></p>
<p><label>일자<br>
<input type="text" name="issuedate" size="15" value="<%=VARissuedate%>" class="px"></label></p>
<p><label><input type="checkbox" name="distmove" value="Y" id="id_distmove" <% if(VARdist.charAt(0) == 'Y') out.print("checked"); %>>분류끼리 이동함</label></p>
<% if(act.equals("mod")){ %>
<p><img src="pix/link.gif" width="5" height="9" alt=""> <a href="pcata_dist.jsp?cataimage=<%=catimage%>">현재 최상위 분류로 이동</a></p>
<% } %>

<div class="submitbtn2"><input type="submit" value="<%=submitStr%>"></div>

<input type="hidden" name="catimage" value="<%=catimage%>">
<input type="hidden" name="act" value="<%=act%>">
<input type="hidden" name="Pg" value="<%=Pg%>">
</form>

</main>

<%@ include file="inc_copy.jsp" %>
