<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	if(cook_dir.equals("")){
		out.println(echo_script(snc_catalog("selfolder"), "basic.jsp", "",""));
		return;
	}

	String ini1var = "";
	String ini2var = "";
	String idxwidth = "";
	String idxheight = "";

	f = new File(pathOfDir + "/ecatalog.txt");
	if(f.exists()){
		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);

			if(skey.equals("ini1var")) ini1var = source.substring(in+1);
			else if(skey.equals("ini2var")) ini2var = source.substring(in+1);
			else if(skey.equals("idxwidth")) idxwidth = source.substring(in+1);
			else if(skey.equals("idxheight")) idxheight = source.substring(in+1);
		}
		fin.close();
		bin.close();
	}

	if(ini1var.equals("")){
		out.println(echo_script(snc_catalog("basicconf"), "basic.jsp", "",""));
		return;
	}

	String rcont = "";
	StringBuffer cont = new StringBuffer();
	f = new File(pathOfDir + "/mokcha.txt");
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			cont.append(source).append("\n");
		}
		fin.close();
		bin.close();

		rcont = strTrim(cont.toString());
	}

	String[] subar = {"link", "목차", "name", ""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 목차</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function checkup(){
	if(confirm("설정값을 저장하시겠습니까?")) return true;
	else return false;
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>

<main id="maincnt" class="maincnt">
<form name="contform" method="post" action="action.jsp" onsubmit="return checkup()">

<section>
<h3>목차 설정(데스크톱)</h3>
<fieldset>
<legend class="title">형태</legend>
<p class="desc">
	<label><input type="radio" name="conttype" value="H" id="id_type1" checked>메뉴아래 한줄</label>&nbsp;
	<label><input type="radio" name="conttype" value="R" id="id_type2" <% if(ini1var.charAt(3) == 'R') out.print("checked"); %>>사각상자</label>
</p>
</fieldset>
</section>

<fieldset>
<legend class="h3legend">사각상자 선택시 옵션</legend>
<p class="title">시작 숨김</p>
<p class="desc"><input type="checkbox" name="conthide" value="N" id="id_hide" <% if(ini1var.charAt(2) == 'N') out.print("checked"); %>>시작시 숨기기</label></p>
<p class="title">크기 <img src="./pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_mokcha1')" onmouseout="hide_helpdiv();"></p>
<p class="desc">
	<label>너비 <input type="number" name="idxwidth" value="<%=idxwidth%>" class="px"></label>&nbsp;
	<label>높이 <input type="number" name="idxheight" value="<%=idxheight%>" class="px"></label>
</p>
</fieldset>
<p id="help_mokcha1" class="secthelp">값을 입력하지 않으시면 기본 210x270 크기로 설정됩니다.</p>

<section>
<h3>목차 설정(모바일)</h3>
<fieldset>
<legend class="title">형태</legend>
<p class="desc">
	<label><input type="radio" name="mconttype" value="H" id="id_mtype1" checked>메뉴아래 한줄</label>&nbsp;
	<label><input type="radio" name="mconttype" value="R" id="id_mtype2" <% if(ini2var.charAt(19) == 'R') out.print("checked"); %>>새창</label>
</p>
</fieldset>
</section>
<br>

<p class="dtitle">내용 <img src="./pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_mokcha3')" onmouseout="hide_helpdiv();"></p>
<p class="ddesc"><textarea name="contents" rows="15" cols="90" class="px"><%=cont%></textarea></p>
<p class="secthelp2">
	- '페이지번호:제목'의 형식으로 기입합니다.<br>
	- 2차 분류는 내용에 '-'를 붙여 1차내용 바로 뒤에 '-페이지번호:제목'의 형식으로 기입합니다.<br>
	- 자동 줄바꿈은 지원되지 않습니다. 한 줄 전체에 'nextline'이라고 넣어주십시오.('메뉴아래 한줄'을 선택했을 경우만 줄바꿈 됩니다)
</p>

<div class="submitbtn"><input type="submit" value="저장하기"></div>

<input type="hidden" name="ini1var" value="<%=ini1var%>">
<input type="hidden" name="ini2var" value="<%=ini2var%>">
<input type="hidden" name="act" value="contents">
</form>

</main>

<%@ include file="inc_copy.jsp" %>
