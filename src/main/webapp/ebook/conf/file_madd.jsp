<%@ page import="com.oreilly.servlet.MultipartRequest,com.oreilly.servlet.multipart.DefaultFileRenamePolicy,java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="admin_xull.jsp" %>
<%
	String jspTags = "%" + ">";

	int filesize = 10*1024*1024;		//10Mega
	MultipartRequest multi = null;

	String catimage = "";
	String Dir = "";
	String url = "";
	String kd = "";
	String tid = "";
	String cupload = "";

	try{
		multi = new MultipartRequest(request, pathOfCata, filesize, new DefaultFileRenamePolicy());

		catimage = multi.getParameter("catimage");
		if(catimage == null) catimage = "";

		Dir = multi.getParameter("Dir");
		if(Dir == null) Dir = "";

		url = multi.getParameter("url");
		if(url == null) url = "";

		kd = multi.getParameter("kd");
		if(kd == null) kd = "";

		tid = multi.getParameter("tid");
		if(tid == null) tid = "";

		cupload = multi.getParameter("cupload");
		if(cupload == null) cupload = "";

	} catch(IOException ioe){
		out.print(ioe.toString());
	} catch(Exception ex){
		out.print(ex.toString());
	} finally{
	}

	if(Dir.equals("")){
		out.print("no Dir parameter!");
		return;
	}

	if(admin_id.equals("")){
		if(cupload.equals("")){
			out.print("No cook_upload Data!");
			return;
		}

		String ip = request.getRemoteAddr();
		int j = 0;

		f = new File(passSysDir);
		fin = new FileInputStream(f);
		bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			if(source.equals(jspTags)) j = 1;
			else if(j == 1){
				String[] admins = mysplit(source, ":");
				String cmr = passEncode(admins[0]+ip);
				if(cmr.equals(cupload)){
					admin_id = admins[0];
					break;
				}
			}
		}
		fin.close();
		bin.close();
	}

	if(admin_id.equals("")){
		out.print("No login ID!");
		return;
	}

	String dirpath = pathOfAcc + "/catImage" + catimage;
	if(!tid.equals("")) dirpath = webSysDir + "/home/" + tid + "/catImage" + catimage;

	dirpath += "/" + Dir;
	f = new File(dirpath);
	if(!f.exists()){
		out.print("no exists Dir! : "+dirpath);
		return;
	}

	if(kd.equals("sound")) dirpath = dirpath + "/sound";
	else if(kd.equals("image")) dirpath = dirpath + "/image";

	f = new File(dirpath);
	if(!f.exists()) f.mkdirs();

	//String entire_server = (request.getServerPort() == 80) ? request.getServerName() : request.getServerName() + ":" + request.getServerPort();
	//String linkDir = "http://" + entire_server + wasServer + "/conf/";
	//if(!url.equals(linkDir)) return;

	String pixsave_name = multi.getFilesystemName("Filedata");
	String pixfile_name = multi.getOriginalFileName("Filedata");

	f = new File(pathOfCata + "/" + pixsave_name);
	File f1 = new File(dirpath + "/" + pixfile_name);
	f.renameTo(f1);

	out.print("added:"+pixfile_name);
%>
