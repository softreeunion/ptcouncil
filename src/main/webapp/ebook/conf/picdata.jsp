<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="admin.jsp" %>
<%
	if(cook_dir.equals("")){
		out.println(echo_script(snc_catalog("selfolder"), "basic.jsp", "",""));
		return;
	}
	String[] subar = {"link", "페이지 정보", "name",""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 페이지 정보</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	div.thinfo{display:block;}
	div.thinfo:after{clear:both;content:"";display:table;}
	div.thinfo > p:first-child{display:block;float:left;margin:5px 0 0 0;padding:0;clear:both;}
	div.thinfo > p:nth-child(2){display:block;float:left;margin:22px 0 0 10px;padding:0;}
	textarea.infotxt{border:1px solid #000000;background-color:#E8FAFF;padding:1px;width:600px;height:45px;}
	img.thpx{border:1px solid #444444;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function checkup(){
	if(confirm("설정값을 저장하시겠습니까?")) return true;
	else return false;
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>
<%
	String ini1var = "";
	String ini2var = "";
	String pageno = "";
	f = new File(pathOfDir + "/ecatalog.txt");
	if(f.exists()){
		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);

			if(skey.equals("ini1var")) ini1var = source.substring(in+1);
			else if(skey.equals("ini2var")) ini2var = source.substring(in+1);
			else if(skey.equals("pageno")) pageno = source.substring(in+1);
		}
		fin.close();
		bin.close();
	}

	if(ini1var.equals("")){
		out.println(echo_script(snc_catalog("basicconf"), "basic.jsp", "",""));
		return;
	}

	char ini1FS = ini1var.charAt(11);
	char ini2_smrepl = ini2var.charAt(7);
	char fileExt = ini2var.charAt(2);

	Hashtable ht = new Hashtable();
	f = new File(pathOfDir + "/picdata.txt");
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf("\t");
			if(in == -1) continue;

			String skey = source.substring(0, in);
			ht.put(skey, source.substring(in+1));
		}
		fin.close();
		bin.close();
	}

	String webpath = webServer + "/catImage" + cook_catimage + "/" + cook_dir;
	String AddLink = "";
	int Punit = 10;
	int Pg = 0;
	int Pnum = 0;
	int ei = 0;

	StringBuffer fnamestr = new StringBuffer();
	StringBuffer list_buf = new StringBuffer();

	f = new File(pathOfDir + "/fileseq.txt");
	if(ini1FS == 'F' && f.exists()){
	FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;
			ei++;
		}
		fin.close();
		bin.close();
	
		int[] bvar = new int[4];
		bvar = db_board_start(ei, Punit, request.getParameter("Pg"));
		int bb = bvar[0];
		int bf = bvar[1];
		Pg = bvar[2];
		Pnum = bvar[3];

		ei = 1;
		fin = new FileInputStream(f);
		bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			if(bb < ei && ei <= bf){
				int in = source.indexOf(":");
				String fname = source.substring(in+1);
				in = fname.indexOf(".");
				String minusext = fname.substring(0, in);
				String desc = ht.containsKey(fname) ? (String)ht.get(fname) : "";
				desc = desc.replaceAll("\\r","\r");
				desc = desc.replaceAll("\\n","\n");

				list_buf.append("<div class=\"thinfo\">\n");
				list_buf.append("<p><label for=\"id"+fname+"\">"+fname+"</label><br>\n");
				list_buf.append("<img src=\""+webpath+"/s"+fname+"\" width=\"70\" height=\"50\" class=\"thpx\" alt=\"s"+fname+"\"></p>\n");
				list_buf.append("<p><textarea name=\"pd"+minusext+"\" id=\"id"+fname+"\" class=\"infotxt\">"+desc+"</textarea></p></div>\n");

				fnamestr.append(":"+fname);
			}
			ei++;
		}
		fin.close();
		bin.close();
	}
	else{
		Vector vt = new Vector();
		f = new File(pathOfDir);
		if(f.exists()){
			File[] folders = f.listFiles();
			for(int i = 0;i < folders.length;i++){
				String temp = folders[i].getName();
				if(temp.equals(".") && temp.equals("..") && temp.substring(0,1).equals("s")) continue;
				if((fileExt == 'J' && temp.toLowerCase().endsWith("jpg")) || (fileExt == 'P' && temp.toLowerCase().endsWith("png"))){
					vt.addElement(temp);
				}
			}
		}

		if(vt.size() > 0){
			Collections.sort(vt);
			int itemLen = vt.size();

			int[] bvar = new int[4];
			bvar = db_board_start(itemLen, Punit, request.getParameter("Pg"));
			int bb = bvar[0];
			int bf = bvar[1];
			Pg = bvar[2];
			Pnum = bvar[3];

			for(int i = bb;i < bf;i++){
				String fname = (String)vt.get(i);
				int in = fname.indexOf(".");
				String minusext = fname.substring(0, in);
				String desc = ht.containsKey(fname) ? (String)ht.get(fname) : "";
				desc = desc.replaceAll("\\r","\r");
				desc = desc.replaceAll("\\n","\n");

				list_buf.append("<div class=\"thinfo\">\n");
				list_buf.append("<p><label for=\"id"+fname+"\">"+fname+"</label><br>\n");
				list_buf.append("<img src=\""+webpath+"/s"+fname+"\" width=\"70\" height=\"50\" class=\"thpx\" alt=\"s"+fname+"\"></p>\n");
				list_buf.append("<p><textarea name=\"pd"+minusext+"\" id=\"id"+fname+"\" class=\"infotxt\">"+desc+"</textarea></p></div>\n");

				fnamestr.append(":"+fname);
			}
		}
		else list_buf.append("<div style=\"text-align:center;width:100%;\">파일이 없습니다.</div>");
	}
%>
<main id="maincnt" class="maincnt">
<form name="picdform" action="action2.jsp" method="post" onsubmit="return checkup()">

<p class="table-title">페이지 정보를 입력해주세요. <img src="./pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_picdata')" onmouseout="hide_helpdiv();"></p>

<%=list_buf.toString()%>

<p class="table-paging"><%=db_board_nextNumber(Pnum, Pg, AddLink, "picdata.jsp")%></p>

<div class="submitbtn2"><input type="submit" value="저장하기"></div>

<input type="hidden" name="Pg" value="<%=Pg%>">
<input type="hidden" name="sdfname" value="<%=fnamestr%>">
<input type="hidden" name="act" value="picdata">
</form>

<p id="help_picdata" class="secthelp">페이지 정보는 '디자인/효과 > 효과 > 페이지 넘김'에서 넘김 종류를 (갤러리)로 선택하셔야 표출됩니다.</p>

</main>

<%@ include file="inc_copy.jsp" %>
