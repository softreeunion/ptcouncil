<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="admin.jsp" %>
<%
	String AddLink = "";
	int Punit = 20;
	int Pg = 0;
	int Pnum = 0;

	int ki = 0;
	int itemLen = 0;
	StringBuffer sbuf = new StringBuffer();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	f = new File(pathOfAcc + "/media/media.txt");
	if(f.exists()){
		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			i++;
		}
		fin.close();
		bin.close();

		itemLen = i;

		int[] bvar = new int[4];
		bvar = db_board_start(itemLen, Punit, request.getParameter("Pg"));
		int bb = bvar[0];
		int bf = bvar[1];
		Pg = bvar[2];
		Pnum = bvar[3];

		i = 1;
		String webpath = webServer + "/media";
		String path = pathOfAcc + "/media";
		fin = new FileInputStream(f);
		bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			if(bb < i && i <= bf){
				String[] aaa = mysplit(source, ":");

				File f1 = new File(path + "/" + aaa[2]);
				String dcont = sdf.format(new Date(f1.lastModified()));

				sbuf.append("<tr>\n");
				sbuf.append("<td><input type=\"text\" name=\"sorder"+ki+"\" value=\""+i+"\" size=\"2\" class=\"qx\"></td>\n");
				sbuf.append("<td><input type=\"text\" size=\"35\" name=\"title"+ki+"\" value=\""+aaa[1]+"\" class=\"qx\">\n");
				sbuf.append("<input type=\"hidden\" name=\"dir"+ki+"\" value=\""+aaa[0]+"\">");
				sbuf.append("<input type=\"button\" value=\"변경\" onclick=\"checkup('"+ki+"')\"></td>");
				sbuf.append("<td>"+aaa[2]+"</td>\n");
				sbuf.append("<td><a href=\"javascript:play_audio('"+webpath+"/"+aaa[2]+"');\">PLAY</a></td>\n");
				sbuf.append("<td>").append((int)f1.length()/1000).append(" KB</td>\n");
				sbuf.append("<td>").append(dcont).append("</td>\n");
				sbuf.append("<td><a href=\"javascript:conf_del('").append(ki).append("');\">삭제</a></td></tr>\n");
			}
			i++;
			ki++;
		}
	}

	if(sbuf.toString().equals("")) sbuf.append("<tr><td colspan=\"7\">파일이 없습니다.</td></tr>");

	String[] subar = {"file", "음악파일", "","media"};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 음악파일</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	main table tr *:nth-of-type(7n+1){text-align:center;}
	main table tr *:nth-of-type(7n+2){text-align:left;}
	main table tr *:nth-of-type(7n+3){text-align:left;}
	main table tr *:nth-of-type(7n+4){text-align:left;}
	main table tr *:nth-of-type(7n+5){text-align:right;}
	main table tr *:nth-of-type(7n+6){text-align:right;}
	main table tr *:nth-of-type(7n+7){text-align:center;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function sorder_up(){
	var dm = document.odform;
	dm.sorder.value = "";
	dm.dirs.value = "";

	if(document.mediaform.elements.length == 1) return;

	for(var i = 0;i < <%=ki%>;i++){
		for(var j = 0;j < <%=ki%>;j++){
			if(i != j && document.mediaform["sorder"+i].value == document.mediaform["sorder"+j].value){
				alert("같은 순서값이 있습니다.");
				return;
			}
		}
		if(document.mediaform["sorder"+i].value == ""){
			alert("순서값이 없습니다.");
			document.mediaform["sorder"+i].focus();
			return;
		}
		else{
			dm.sorder.value += (dm.sorder.value == "") ? document.mediaform["sorder"+i].value : ":" + document.mediaform["sorder"+i].value;
			dm.dirs.value += (dm.dirs.value == "") ? document.mediaform["dir"+i].value : ":" + document.mediaform["dir"+i].value;
		}
	}

	if(confirm("순서를 저장하시겠습니까?")) dm.submit();
	else return;
}
function conf_del(num){
	if(confirm("파일을 삭제하시겠습니까?")){
		document.delform.dir.value = document.mediaform["dir"+num].value;
		document.delform.submit();
	}
	else return;
}
function checkup(num){
	if(document.mediaform["title"+num].value == ""){
		alert("타이틀을 입력해주세요.");
		return;
	}

	if(confirm("변경하시겠습니까?")){
		document.modform.title.value = document.mediaform["title"+num].value;
		document.modform.dir.value = document.mediaform["dir"+num].value;
		document.modform.submit();
	}
}

function play_audio(fname){
	document.getElementById('audioctrl').style.display = "block";
	var audioPlayer = document.getElementById('audioplayer');
	audioPlayer.setAttribute('src', fname);
	audioPlayer.play();
}
function hide_audioctrl(){
	document.getElementById('audioctrl').style.display = "none";
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>

<main id="maincnt" class="maincnt">
<form name="mediaform" method="post" action="#">
<p class="table-title">음악 파일 목록 : <%=itemLen%>개 파일</p>
<table>
	<colgroup>
		<col style="width:7%;"><col style="width:40%;"><col style="width:10%;">
		<col style="width:7%;"><col style="width:10%;"><col style="width:20%;"><col style="width:6%;">
	</colgroup>
	<thead>
	<tr>
		<th><input type="button" value="순서" onclick="sorder_up()"></th>
		<th>제목</th>
		<th>파일명</th>
		<th>연주</th>
		<th>크기</th>
		<th>최종변경일</th>
		<th>삭제</th>
	</tr>
	</thead>
	<%=sbuf.toString()%>
</table>
<p class="table-paging"><%=db_board_nextNumber(Pnum, Pg, AddLink, "file_medialist.jsp")%></p>
</form>

<div class="table-qmenu">
<p><a href="file_add.jsp?kd=<%=subar[3]%>"><img src="<%=confServer%>/pix/fileadd.png" width="59" height="19" alt="파일추가"></a></p>
<p></p>
</div>

<div id="audioctrl"><audio id="audioplayer" controls ondblclick="hide_audioctrl();">Your browser does not support the audio element.</audio></div>

<div class="nullform">
<form name="modform" method="post" action="media_act.jsp">
<input type="hidden" name="title" value="">
<input type="hidden" name="dir" value="">
<input type="hidden" name="act" value="mod">
<input type="hidden" name="kd" value="media">
</form>

<form name="odform" method="post" action="media_act.jsp">
<input type="hidden" name="sorder" value="">
<input type="hidden" name="dirs" value="">
<input type="hidden" name="act" value="odmod">
<input type="hidden" name="kd" value="media">
</form>

<form name="delform" method="post" action="media_act.jsp">
<input type="hidden" name="dir" value="">
<input type="hidden" name="act" value="del">
<input type="hidden" name="kd" value="media">
</form>
</div>

</main>

<%@ include file="inc_copy.jsp" %>
