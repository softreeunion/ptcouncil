<%@ page pageEncoding="utf-8" %>
<%@ include file="lang.jsp" %>
<%
	request.setCharacterEncoding("utf-8");

	String sess_id = (String)session.getAttribute("sess_id");
	if(sess_id == null) sess_id = "";

	String admin_id = "";
	String cook_adminkd = "";
	String cook_catimage = "";

	if(sess_id.equals("")){
		Cookie c_cookurl = new Cookie("cook_url", request.getServletPath());
		response.addCookie(c_cookurl);

		File f = new File(passSysDir);
		if(!f.exists()){
			out.print(echo_script(snc_catalog("setidpass"),"idpass.jsp","",""));
		}
		else{
			out.print(echo_script(snc_catalog("shouldlogin"),"login.jsp","",""));
		}
		return;
	}
	else{
		String[] temp_adminid = mysplit(sess_id,":");
		admin_id = temp_adminid[0];
		cook_adminkd = temp_adminid[1];
		if(temp_adminid.length > 2) cook_catimage = temp_adminid[2];
	}

	String cataimage = request.getParameter("cataimage");
	if(cataimage == null) cataimage = "";

	if(!cataimage.equals("") && (cook_adminkd.equals("1") || cook_adminkd.equals("2"))){
		if(cataimage.equals("B")) cook_catimage = "";
		else cook_catimage = cataimage;

		if(!cook_catimage.equals("") && check_numVar(cook_catimage) == false){
			out.print(echo_script(snc_catalog("notform")+"(catimage)","basic.jsp","",""));
			return;
		}
		session.setAttribute("sess_id", admin_id+":"+cook_adminkd+":"+cook_catimage);
	}

	String cook_index = "cook_dir" + cook_catimage;
	String cook_cate = "";
	String cook_dir = "";

	if(request.getCookies() != null){
		Cookie[] cookies = request.getCookies();
		for(int i = 0;i < cookies.length;i++){
			Cookie theCookie = cookies[i];
			if(theCookie.getName().equals(cook_index)){
				String[] temp_cookindex = mysplit(cookDecode(theCookie.getValue()),":");
				cook_cate = temp_cookindex[0];
				if(temp_cookindex.length > 1) cook_dir = temp_cookindex[1];
			}
		}
	}

	String catadir = request.getParameter("catadir");
	if(catadir == null) catadir = "";

	if(!catadir.equals("")){
		Cookie c_cookindex = new Cookie(cook_index, cookEncode(catadir));
		response.addCookie(c_cookindex);

		String[] temp_cookindex = mysplit(catadir,":");
		cook_cate = temp_cookindex[0];
		if(temp_cookindex.length > 1) cook_dir = temp_cookindex[1];
	}

	if(!cook_cate.equals("") && check_numalphaVar(cook_cate, 10) == false){
		out.print(echo_text(snc_catalog("notform")+"(cate:"+cook_cate+")"));
		return;
	}

	if(!cook_dir.equals("") && check_numVar(cook_dir) == false){
		out.print(echo_text(snc_catalog("notform")+"(dir)"));
		return;
	}

	String variOfCata = "";
	String pathOfCata = webSysDir + "/catImage" + cook_catimage;
	String pathOfOriCata = webSysDir + "/catImage";
	String pathOfDir  = pathOfCata + "/" + cook_dir;
	String pathOfAcc  = webSysDir;
	String skinOfCata = "";

	String cook_pcode = cook_cate.equals("") ? "" : get_parentCode(cook_cate);
	String scname = request.getServletPath().substring(0, request.getServletPath().indexOf("/conf/"));
	String mblogLink = variOfCata.equals("mblog") ? "&tid="+admin_id : "";
	String param = cook_dir+"&catimage="+cook_catimage+mblogLink+"&callmode=admin";

	String source = "";
	File f;
%>
