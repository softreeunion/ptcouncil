<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="admin_xull.jsp" %>
<%
	String admin_idmem = "";
	if(request.getCookies() != null){
		Cookie[] cookies = request.getCookies();
		for(int i = 0;i < cookies.length;i++){
			Cookie theCookie = cookies[i];
			if(theCookie.getName().equals("admin_idmem")){
				admin_idmem = theCookie.getValue();
				break;
			}
		}
	}
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 관리자 로그인</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<script src="<%=webServer%>/common.js"></script>
<script>
function focus_this(){
	if(document.loginform.mid.value == "") document.loginform.mid.focus();
	else document.loginform.passwd.focus();
}
function check_memlog(dm){
	if(trimVal(dm.mid.value) == ""){
		alert("아이디를 입력하셔야 합니다.");
		dm.mid.focus();
		return false;
	}
	if(trimVal(dm.passwd.value) == ""){
		alert("비밀번호를 입력하셔야 합니다.");
		dm.passwd.focus();
		return false;
	}
}
</script>
</head>

<body onload="focus_this()">

<form name="loginform" action="logact.jsp" method="post" onsubmit="return check_memlog(this)">
<main class="mlogin">
	<header>
		<p><img src="<%=confServer%>/pix/logo.gif" width="149" height="45" alt="로고"></p>
		<h1>관리자 로그인</h1>
	</header>

	<dl>
		<dt>아이디</dt>
		<dd><input type="text" name="mid" size="15" value="<%=admin_idmem%>" required></dd>
		<dt>비밀번호</dt>
		<dd><input type="password" name="passwd" size="15" required> <input type="submit" value="로그인"></dd>
		<dt></dt>
		<dd><label><input type="checkbox" name="idmem" value="1" checked> 아이디 기억하기</label></dd>
	</dl>

	<footer>
		<p>- 회원가입하는 별도의 절차는 없습니다.</p>
		<p>- 전자 카탈로그 설정 관리자 페이지로 로그인합니다.</p>
	</footer>
</main>
</form>

</body>
</html>
