<%@ page import="com.oreilly.servlet.MultipartRequest,com.oreilly.servlet.multipart.DefaultFileRenamePolicy,java.io.*, java.util.*, java.text.SimpleDateFormat, java.awt.image.*" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%@ include file="common_func.jsp" %>
<%
	int filesize = 10*1024*1024;		//10Mega

	String act = request.getParameter("act");
	if(act == null) act = "";

	String kd = request.getParameter("kd");
	if(kd == null) kd = "";

	String listfname = "file_medialist.jsp";
	String atta_dir = pathOfAcc + "/media/";
	String conf_file = atta_dir + "media.txt";

	if(kd.equals("flash")){
		listfname = "file_flashlist.jsp";
		atta_dir = pathOfAcc + "/flash/";
		conf_file = atta_dir + "flash.txt";
	}

	if(act.equals("add")){
		int[] exname = new int[1000];

		StringBuffer cont = new StringBuffer();
		f = new File(conf_file);
		if(f.exists()){
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				cont.append(source).append("\n");

				String[] aaa = mysplit(source, ":");
				exname[Integer.parseInt(aaa[0])] = 1;
			}
			bin.close();
			fin.close();
		}

		int i = 0;
		for(i = 0;i < 1000;i++){
			if(exname[i] == 0) break;
		}

		MultipartRequest multi = null;
		multi = new MultipartRequest(request, atta_dir, filesize, new DefaultFileRenamePolicy());

		String pixsave_name = multi.getFilesystemName("pixfile");
		String pixfile_name = multi.getOriginalFileName("pixfile");

		String exten = get_extension(pixfile_name);
		if(exten.equals("php") || exten.equals("php3") || exten.equals("jsp") || exten.equals("cgi") || exten.equals("exe")){
			out.print(echo_text("파일업로드시 에러가 발생하였습니다.(check character)"));
			return;
		}

		String fname = "";
		if(i < 10) fname = "00" + i + "." + exten;
		else if(i < 100) fname = "0" + i + "." + exten;
		else fname = i + "." + exten;

		f = new File(atta_dir + pixsave_name);
		File f1 = new File(atta_dir + fname);
		f.renameTo(f1);

		String title = new String(multi.getParameter("title").getBytes("8859_1"), streamCharset);
		if(title == null) title = "";
		title = strTrim(title);

		if(kd.equals("flash")){
			String flw = multi.getParameter("flw");
			String flh = multi.getParameter("flh");

			FileOutputStream fos = new FileOutputStream(conf_file);
			OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
			if(cont.toString().equals("")) osw.write(i + ":" + title + ":" + fname + ":" + flw + ":" + flh);
			else osw.write(i + ":" + title + ":" + fname + ":" + flw + ":" + flh + "\n" + cont.toString());
			osw.close();
		}
		else{
			FileOutputStream fos = new FileOutputStream(conf_file);
			OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
			if(cont.equals("")) osw.write(i + ":" + title + ":" + fname);
			else osw.write(i + ":" + title + ":" + fname + "\n" + cont.toString());
			osw.close();
		}

		out.println(echo_script(snc_catalog("added"), listfname, "",""));
	}
	else if(act.equals("mod")){
		String dir = request.getParameter("dir");
		String title = strTrim(request.getParameter("title"));

		if(dir.equals("")){
			out.println(echo_text(snc_catalog("notform")+"(no dir)"));
			return;
		}

		StringBuffer cont = new StringBuffer();
		f = new File(conf_file);
		if(kd.equals("flash")){
			String flw = request.getParameter("flw");
			String flh = request.getParameter("flh");

			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				String[] aaa = mysplit(source, ":");

				if(dir.equals(aaa[0])) cont.append(aaa[0]).append(":").append(title).append(":").append(aaa[2]).append(":").append(flw).append(":").append(flh).append("\n");
				else cont.append(source).append("\n");
			}
			bin.close();
			fin.close();
		}
		else{
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				String[] aaa = mysplit(source, ":");
				if(dir.equals(aaa[0])) cont.append(aaa[0]).append(":").append(title).append(":").append(aaa[2]).append("\n");
				else cont.append(source).append("\n");
			}
			bin.close();
			fin.close();
		}

		FileOutputStream fos = new FileOutputStream(conf_file);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.close();

		out.println(echo_script(snc_catalog("changed"), listfname, "",""));
	}
	else if(act.equals("odmod")){
		Hashtable hm_subx = new Hashtable();

		f = new File(conf_file);
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, ":");
			hm_subx.put(aaa[0], aaa[1]);
		}
		bin.close();
		fin.close();

		String sorder = request.getParameter("sorder");
		String dirs = request.getParameter("dirs");

		if(sorder.indexOf(":") > -1){
			StringTokenizer st = new StringTokenizer(sorder, ":");
			int i = st.countTokens();

			String[] bbs = new String[i+1];
			String[] bbd = new String[i+1];
			String[] ccc = new String[i+1];

			st = new StringTokenizer(sorder, ":");
			i = 0;
			while(st.hasMoreTokens()){
				bbs[i] = st.nextToken();
				i++;
			}
			
			st = new StringTokenizer(dirs, ":");
			i = 0;
			while(st.hasMoreTokens()){
				bbd[i] = st.nextToken();
				i++;
			}		

			for(i = 0;i < bbs.length;i++){
				if(bbs[i] == null || bbs[i].equals("")) continue;
				int k = Integer.parseInt(bbs[i]);
				ccc[k] = bbd[i];
			}

			StringBuffer cont = new StringBuffer();
			for(i = 0;i < bbs.length;i++){
				if(ccc[i] != null){
					cont.append((String)hm_subx.get(ccc[i])).append("\n");
				}
			}

			FileOutputStream fos = new FileOutputStream(conf_file);
			OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
			osw.write(cont.toString());
			osw.close();
		}
		out.print(echo_script("순서가 변경되었습니다.", listfname, "", ""));
	}
	else if(act.equals("del")){
		String dir = request.getParameter("dir");
		if(dir.equals("")){
			out.println(echo_text(snc_catalog("notform")+"(no dir)"));
			return;
		}

		StringBuffer cont = new StringBuffer();
		String delfile = "";
		f = new File(conf_file);
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, ":");

			if(dir.equals(aaa[0])) delfile = aaa[2];
			else cont.append(source).append("\n");
		}
		bin.close();
		fin.close();

		FileOutputStream fos = new FileOutputStream(conf_file);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.close();

		f = new File(atta_dir + delfile);
		if(f.exists()) f.delete();

		out.println(echo_script("삭제되었습니다.", listfname, "",""));
	}
%>
