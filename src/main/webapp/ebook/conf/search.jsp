<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	if(cook_dir.equals("")){
		out.println(echo_script(snc_catalog("selfolder"), "basic.jsp", "",""));
		return;
	}

	String[] subar = {"link", "검색", "name",""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 검색</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function checkup(){
	if(confirm("설정값을 저장하시겠습니까?")) return true;
	else return false;
}
function elementdisable(){
	if(document.searchform.searchmethod[0].checked){
		document.searchform.searchurl.disabled = true;
		document.searchform.searchcont.disabled = false;
	}
	else if(document.searchform.searchmethod[1].checked){
		document.searchform.searchurl.disabled = false;
		document.searchform.searchcont.disabled = true;
	}
	else{
		document.searchform.searchurl.disabled = true;
		document.searchform.searchcont.disabled = true;
	}
}
function onload_func2(){
	elementdisable();
	onload_func();
}
</script>
</head>

<body onload="onload_func2();">
<%@ include file="inc_menu.jsp" %>
<%
	String unifyCond = "";
	String pCode = get_parentCode(cook_cate);
	{
		f = new File(pathOfCata + "/catakind.txt");
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;
			if(source.substring(0,10).equals(pCode)) unifyCond = source.substring(12,13);
		}
		fin.close();
		bin.close();
	}

	String ini1var = "";
	String searchurl = "";
	f = new File(pathOfDir + "/ecatalog.txt");
	if(f.exists()){
		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);

			if(skey.equals("ini1var")) ini1var = source.substring(in+1);
			else if(skey.equals("searchurl")) searchurl = source.substring(in+1);
		}
		fin.close();
		bin.close();
	}

	if(ini1var.equals("")){
		out.println(echo_script(snc_catalog("basicconf"), "basic.jsp", "",""));
		return;
	}

	char ini1FS = ini1var.charAt(11);
	char searchmethod = ini1var.charAt(9);

	String xmlradio = "";
	if(ini1FS != 'X') xmlradio = "disabled";

	String rcont = "";
	StringBuffer cont = new StringBuffer();
	f = new File(pathOfDir + "/search.txt");
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			cont.append(source).append("\n");
		}
		fin.close();
		bin.close();

		rcont = strTrim(cont.toString());
	}
%>
<main id="maincnt" class="maincnt">
<form name="searchform" action="action.jsp" method="post" onsubmit="return checkup()">

<fieldset>
<legend class="dtitle">검색 종류</legend>
<p class="ddesc">
<label><input type="radio" name="searchmethod" value="F" onclick="elementdisable();" id="id_method1"
  <% if(searchmethod == 'F') out.print("checked"); %>>아래 내용입력</label>&nbsp;
<label><input type="radio" name="searchmethod" value="U" onclick="elementdisable();" id="id_method2"
  <% if(searchmethod == 'U') out.print("checked"); %>>외부 URL연결</label>&nbsp;
<label><input type="radio" name="searchmethod" value="A" onclick="elementdisable();" id="id_method3"
  <% if(searchmethod == 'A') out.print("checked"); %>>자동 검색</label>&nbsp;
<label><input type="radio" name="searchmethod" value="C" onclick="elementdisable();" id="id_method6"
  <%=xmlradio%><% if(searchmethod == 'C') out.print("checked"); %>>XML 속성 & 본문</label>
  <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명" class="helpshow" onmouseover="show_helpdiv(this, 'help_xmlsearch')" onmouseout="hide_helpdiv();">
</p>
</fieldset>
<p id="help_xmlsearch" class="secthelp">XML은 '기본환경 > 내용구성'을 'XML 데이터'로 했을 때 선택할 수 있습니다.</p>
<br>

<p class="dtitle">내용 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_search1')" onmouseout="hide_helpdiv();"></p>
<p class="ddesc"><textarea name="searchcont" rows="15" cols="90" class="px"><%=cont%></textarea></p>
<p id="help_search1" class="secthelp">
	- '1:전자카탈로그'와 같이 '페이지번호:검색문구'의 형식으로 입력합니다.<br>
	- 자동검색은 일정한 형식의 xml 파일을 필요로 합니다.(옵션 사항)
</p>
<br>

<p class="dtitle">외부 URL <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_search2')" onmouseout="hide_helpdiv();"></p>
<p class="ddesc"><input type="text" size="90" name="searchurl" class="px" value="<%=searchurl%>"></p>
<p id="help_search2" class="secthelp">
	- URL 연결시 카탈로그에서 입력한 값이 qstr변수에 담겨져 GET방식으로 URL에 전달됩니다.(?qstr=검색어)<br>
	- URL 연결시 결과값은 내용과 같이 '페이지번호:검색문구'의 형식으로 출력하시면 됩니다.
</p>

<div class="submitbtn"><input type="submit" value="저장하기"></div>

<input type="hidden" name="ini1var" value="<%=ini1var%>">
<input type="hidden" name="act" value="search">
<input type="hidden" name="unifycond" value="<%=unifyCond%>">
<input type="hidden" name="cate" value="<%=cook_cate%>">
</form>

</main>

<%@ include file="inc_copy.jsp" %>
