<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="admin.jsp" %>
<%
	if(cook_dir.equals("")){
		out.println(echo_script(snc_catalog("selfolder"), "basic.jsp", "",""));
		return;
	}

	String qtxt = request.getParameter("qtxt");
	if(qtxt == null) qtxt = "";

	String qfname = request.getParameter("qfname");
	if(qfname == null) qfname = "";

	Vector dntPix = new Vector();

	int smwidth = 0;
	int smheight = 0;
	f = new File(pathOfDir + "/ecatalog.txt");
	if(f.exists()){
		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;
	
			int in = source.indexOf(":");
			String skey = source.substring(0, in);
			if(skey.equals("smwidth")) smwidth = Integer.parseInt(source.substring(in+1));
			else if(skey.equals("smheight")) smheight = Integer.parseInt(source.substring(in+1));
		}
		fin.close();
		bin.close();
	}

	int thumbWidth = 0;
	int thumbHeight = 100;
	if(smheight > 0){
		thumbWidth = (int)(thumbHeight*smwidth/smheight);

		if(thumbWidth > thumbHeight){
			thumbWidth = 100;
			thumbHeight = (int)(thumbWidth*smheight/smwidth);
		}
	}

	String AddLink = "qtxt="+qtxt+"&qfname="+qfname;
	int Punit = 30;
	int Pg = 0;
	int Pnum = 0;
	int itemLen = 0;
	int ki = 0;			// only for showing number

	StringBuffer list_buf = new StringBuffer();
	f = new File(pathOfDir + "/alink.txt");
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, "\t");
			if((!qfname.equals("") && qfname.equals(aaa[0])) || qfname.equals("")){
				if(!qtxt.equals("") && aaa[2].indexOf(qtxt) < 0) continue;
				itemLen++;
			}
		}
		fin.close();
		bin.close();

		int[] bvar = new int[4];
		bvar = db_board_start(itemLen, Punit, request.getParameter("Pg"));
		int bb = bvar[0];
		int bf = bvar[1];
		Pg = bvar[2];
		Pnum = bvar[3];

		int ii = 0;			// unique link file
		int ei = 1;			// total sequence number
		int qi = 1;			// paging sequence number
		int befno = 0;

		fin = new FileInputStream(f);
		bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, "\t");

			if((!qfname.equals("") && qfname.equals(aaa[0])) || qfname.equals("")){
				if(qtxt.equals("") || aaa[2].indexOf(qtxt) >= 0){
					if(bb < qi && qi <= bf){
						list_buf.append("<tr>\n");

						if(!qfname.equals("")){
							if(ki == 0) list_buf.append("<td>&nbsp;</td>\n");
							else list_buf.append("<td><a href=\"link_act.jsp?act=seq&qfname="+qfname+"&qtxt="+qtxt+"&Pg="+Pg+"&sd="+befno+":"+ei+"\">↑</a></td>\n");
						}
						else list_buf.append("<td>").append(ki+1).append("</td>\n");

						String acoord = (aaa[1].length() > 15) ? aaa[1].substring(0,13)+"..." : aaa[1];

						list_buf.append("<td><input type=\"checkbox\" name=\"ckno"+ki+"\" value=\""+ei+"\"></td>\n");
						list_buf.append("<td><input type=\"text\" name=\"name"+ki+"\" size=\"30\" class=\"qx\" value=\""+aaa[2]+"\"></td>\n");
						list_buf.append("<td><input type=\"text\" name=\"url"+ki+"\" size=\"40\" class=\"qx\" value=\""+aaa[3]+"\"></td>\n");
						list_buf.append("<td><span style=\"cursor:pointer\" onclick=\"open_pagelink('"+aaa[0]+"')\" ");
						list_buf.append("onMouseOver=\"show_thumb(event,'s"+aaa[0]+"')\" onMouseOut=\"notDivShow()\">"+aaa[0]+"</span></td>\n");
						list_buf.append("<td>"+acoord+"</td>\n");
						list_buf.append("<td><input type=\"hidden\" name=\"no"+ki+"\" value=\""+ei+"\">");
						list_buf.append("<input type=\"button\" value=\"변경\" onclick=\"check_mod('"+ki+"')\"></td>\n</tr>\n");

						befno = ei;
						ki++;
					}
					qi++;
				}
				ei++;

				if(ii == 0){
					dntPix.addElement(aaa[0]);
					ii++;
				}
				else{
					if(!dntPix.contains(aaa[0])){
						dntPix.addElement(aaa[0]);
					}
				}
			}
		}
	}

	StringBuffer fname_buf = new StringBuffer("<option value=\"\">페이지</option>\n");
	if(dntPix.size() > 0){
		for(int i = 0;i < dntPix.size();i++){
			String temp = (String)dntPix.get(i);
			String selc = "";

			if(temp.equals(qfname)) selc = "selected";
			fname_buf.append("<option value=\"").append(temp).append("\" ").append(selc).append(">").append(temp).append("</option>\n");
		}
	}


	if(list_buf.toString().equals("")) list_buf.append("<tr><td colspan=\"7\" style=\"text-align:center;\">링크가 없습니다.</td></tr>\n");

	String webpath = webServer + "/catImage" + cook_catimage + "/" + cook_dir;
	String subar[] = {"link", "링크목록", "name", ""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 링크목록</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	main.maincnt table tr *{text-align:left;}
	main.maincnt table tr *:nth-of-type(4n+2){text-align:center;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
var currnum = -1;
function check_mod(num){
	if(trimVal(document.linkform["name"+num].value) == ""){
		alert("타이틀을 입력해주세요.");
		document.linkform["name"+num].focus();
		return;
	}

	if(trimVal(document.linkform["url"+num].value) == ""){
		alert("링크URL을 입력해주세요.");
		document.linkform["url"+num].focus();
		return;
	}

	if(confirm("변경하시겠습니까?")){
		document.modform.name.value = document.linkform["name"+num].value;
		document.modform.url.value = document.linkform["url"+num].value;
		document.modform.no.value = document.linkform["no"+num].value;
		document.modform.submit();
	}
	currnum = -1;
}
function check_del(){
	var str = "";
	for(var i = 0;i < <%=ki%>;i++){
		if(document.linkform["ckno"+i].checked){
			str = (str == "") ? document.linkform["ckno"+i].value : str + ":" + document.linkform["ckno"+i].value;
		}
	}

	if(str == ""){
		alert("삭제하실 링크를 선택하셔야 합니다.");
		return;
	}

	if(confirm("선택하신 링크를 삭제하시겠습니까?")){
		document.delform.sd.value = str;
		document.delform.submit();
	}
}
function check_query(){
	if(trimVal(document.searchform.qtxt.value) == ""){
		alert("검색어를 입력해주세요.");
		document.searchform.qtxt.focus();
		return false;
	}
}
function go_selcfname(obj){
	if(obj.value == "" && "<%=qfname%>" == "") return;
	document.filterform.qfname.value = obj.value;
	document.filterform.submit();
}
function show_thumb(e, imgsrc){
	if(!e) e = event;

	document.getElementById('divmessage').innerHTML = "<img src=\"<%=webpath%>/"+imgsrc+"\" width=\"<%=thumbWidth%>\" height=\"<%=thumbHeight%>\">";
	document.getElementById('divmessage').style.visibility = "visible";
	document.getElementById('divmessage').style.left = e.clientX + 10 + document.documentElement.scrollLeft + "px";
	document.getElementById('divmessage').style.top = e.clientY + 15 + document.documentElement.scrollTop + "px";
}
function notDivShow(){
	document.getElementById('divmessage').style.visibility = "hidden";
}
function open_pagelink(imgname){
	var property = "toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,scrollbars=no,copyhistory=no,";
	property += "width=1000,height=700,left=0,top=0"
	window.open('nw_pagelink.jsp?pxf='+imgname,'a',property);
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>

<main id="maincnt" class="maincnt">
<form name="linkform" method="get" action="link_title.jsp?qtxt=<%=qtxt%>">
<p class="table-title">링크 목록 : 전체 <%=itemLen%>개</p>
<table>
	<tr>
		<th>순</th>
		<th><input type="button" value="삭제" onclick="check_del();"></th>
		<th>타이틀</th>
		<th>링크URL</th>
		<th><select name="qfname" onchange="go_selcfname(this)"><%=fname_buf%></select></th>
		<th>좌표</th>
		<th>작업</th>
	</tr>
	<%=list_buf%>
</table>
<p class="table-paging"><%=db_board_nextNumber(Pnum, Pg, AddLink, "link_title.jsp")%></p>
</form>

<div class="table-qmenu">
<form name="searchform" method="get" action="link_title.jsp" onsubmit="return check_query()">
<p></p>
<p>
	<input type="search" name="qtxt" size="20" class="px" value="<%=qtxt%>" required>
	<input type="submit" value="제목 검색">
	<input type="hidden" name="qfname" value="<%=qfname%>">
</p>
</form>
</div>
<br>

<section class="secthelp3">
<h3>링크URL에 대한 설명</h3>
<dl>
	<dt>[page]페이지번호</dt>
	<dd>다른 페이지로 이동, ex) [page]10</dd>
	<dt>[win][1,100,100]http://url/</dt>
	<dd>별도창으로 다른 웹페이지 호출<br>
	- 1 : 별도창 종류 [<b>1</b> -> 스크롤바 없음, <b>2</b> -> 스크롤바 있음, <b>3</b> -> 부모창에 대한 링크]<br>
	- 100,100 : 별도창의 너비와 높이</dd>
	<dt>[swf]파일이름</dt>
	<dd>마우스 클릭시 플래시 파일 호출, ex) [swf]index.swf</dd>
	<dt>[sta]파일이름</dt>
	<dd>페이지 이동후 플래시 파일 자동 불러옴, ex) [sta]index.swf</dd>
	<dt>[sta/swf][center]파일이름</dt>
	<dd>화면의 가운데에 위치하고 닫기버튼 생성, ex) [sta][center]index.swf</dd>
	<dt>[flv]파일이름</dt>
	<dd>flv 동영상 호출, ex) [flv]001.flv</dd>
	<dt>[mp3]파일이름</dt>
	<dd>mp3 음악 호출, ex) [mp3]001.mp3</dd>
	<dt>[ico][파일이름]링크URL</dt>
	<dd>페이지 이동 후 '파일이름' 아이콘 자동 불러옴. 링크URL은 위의 규칙을 따름</dd>
</dl>
<p>더 자세한 정보는 <a href="help.jsp">도움말 페이지</a>에서 확인하십시오</p>
</section>

<div class="nullform">
	<form name="modform" method="post" action="link_act.jsp">
	<input type="hidden" name="name" value="">
	<input type="hidden" name="url" value="">
	<input type="hidden" name="no" value="">
	<input type="hidden" name="Pg" value="<%=Pg%>">
	<input type="hidden" name="qfname" value="<%=qfname%>">
	<input type="hidden" name="qtxt" value="<%=qtxt%>">
	<input type="hidden" name="act" value="mod">
	</form>

	<form name="delform" method="post" action="link_act.jsp">
	<input type="hidden" name="sd" value="">
	<input type="hidden" name="Pg" value="<%=Pg%>">
	<input type="hidden" name="loc" value="link_title">
	<input type="hidden" name="act" value="del">
	</form>

	<form name="filterform" method="get" action="link_title.jsp">
	<input type="hidden" name="qfname" value="">
	<input type="hidden" name="qtxt" value="<%=qtxt%>">
	</form>
</div>

</main>

<%@ include file="inc_copy.jsp" %>
