<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	String memberid = request.getParameter("mid");
	String act = request.getParameter("act");

	String kact = "추가";
	String ksubj = "관리자 추가";

	if(act.equals("mod")){
		kact = "변경";
		ksubj = "관리자 변경";
	}

	String path = passSysDir;
	String jspTags = "%" + ">";
	String logincate = "";

	if(act.equals("mod")){
		int j = 0;

		f = new File(path);
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			if(source.equals(jspTags)) j = 1;
			else if(j == 1){
				String[] aaa = mysplit(source, ":");
				if(aaa[0].equals(memberid)){
					logincate = aaa[2];
					break;
				}
			}
		}
		fin.close();
		bin.close();
	}

	StringBuffer selc_buf = new StringBuffer();
	path = pathOfOriCata + "/catimage.txt";

	f = new File(path);
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, "\t");				// folder, dist, name, date
			if(aaa[0].equals(logincate)){
				selc_buf.append("<option value=\"").append(aaa[0]).append("\" selected>").append(aaa[2]).append("</option>\n");
			}
			else{
				selc_buf.append("<option value=\"").append(aaa[0]).append("\">").append(aaa[2]).append("</option>\n");
			}
		}
		fin.close();
		bin.close();
	}
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - <%=ksubj%>(새창)</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<script src="<%=webServer%>/common.js"></script>
<script>
function checkup(dm, act){
	if(trimVal(dm.mid.value) == ""){
		alert("아이디를 입력하셔야 합니다.");
		dm.mid.focus();
		return false;
	}

	if(act == "adminadd"){
		if(trimVal(dm.passwd1.value) == ""){
			alert("비밀번호를 입력하셔야 합니다.");
			dm.passwd1.focus();
			return false;
		}
		if(trimVal(dm.passwd2.value) == ""){
			alert("비밀번호확인을 입력하셔야 합니다.");
			dm.passwd2.focus();
			return false;
		}
	
		if(dm.passwd1.value != dm.passwd2.value){
			alert("비밀번호 1, 2의 입력내용이 서로 틀립니다. 확인해주세요.");
			dm.passwd1.focus();
			return false;
		}
	}
	else{
		if(dm.passwd1.value != "" && dm.passwd1.value != dm.passwd2.value){
			alert("비밀번호 1, 2의 입력내용이 서로 틀립니다. 확인해주세요.");
			dm.passwd1.focus();
			return false;
		}
	}

	dm.act.value = act;

	return true;
}
</script>
</head>

<body>

<main class="openedcnt">
<form name="addform" action="admin_act.jsp" method="post" onsubmit="return checkup(document.addform,'admin<%=act%>')">
<p><%=ksubj%></p>
<p><label>아이디<br>
<input type="text" name="mid" size="15" class="px" value="<%=memberid%>" required></label></p>
<p><label>비밀번호(1)<br>
<input type="password" name="passwd1" size="15" class="px" required></label></p>
<p>비밀번호확인(2)<br>
<input type="password" name="passwd2" size="15" class="px" required></label></p>
<p>최상위 분류<br>
<select name="dist">
	<option value="">관리할 분류를 선택하세요.</option>
	<%=selc_buf%>
</select>
</p>

<div class="submitbtn2"><br><input type="submit" value=" <%=kact%> "></div>
	
<input type="hidden" name="preid" value="<%=memberid%>">
<input type="hidden" name="act" value="admin<%=act%>">
</form>

<p class="secthelp2">분류을 지정하지 않으시면 모든 분류를 제어할 수 있습니다.</p>

</main>

</body>
</html>
