<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	if(cook_dir.equals("")){
		out.println(echo_script(snc_catalog("selfolder"), "basic.jsp", "",""));
		return;
	}

	String[] subar = {"design", "효과", "name", ""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 효과</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	label{margin-right:5px;}
	input{vertical-align:middle;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function checkup(){
	if(confirm("설정값을 저장하시겠습니까?")) return true;
	else return false;
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>
<%
	String askinG = "";
	Hashtable ahash = new Hashtable();
	f = new File(pathOfDir + "/ecatalog.txt");
	if(f.exists()){
		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);
			String sval = source.substring(in+1);

			if(skey.equals("askin")) askinG = sval;
			else ahash.put(skey, sval);
		}
		fin.close();
		bin.close();
	}

	if(askinG.equals("")) askinG = "001|001|001|001|001|001|001|001|001|001";

	String[] askin = mysplit(askinG, "|");
	String ini1var = ahash.containsKey("ini1var") ? (String)ahash.get("ini1var") : "";
	if(ini1var.equals("")) ini1var = "                      ";

	String ini2var = ahash.containsKey("ini2var") ? (String)ahash.get("ini2var") : "";
	if(ini2var.equals("") || ini2var.length() != 5) ini2var += "NNNNN";

	String slidetime = ahash.containsKey("slidetime") ? (String)ahash.get("slidetime") : "";
	String bigslidetime = ahash.containsKey("bigslidetime") ? (String)ahash.get("bigslidetime") : "";
	String margin = ahash.containsKey("margin") ? (String)ahash.get("margin") : "";
	String mmargin = ahash.containsKey("mmargin") ? (String)ahash.get("mmargin") : "";
	String midwidth = ahash.containsKey("midwidth") ? (String)ahash.get("midwidth") : "";
%>
<main id="maincnt" class="maincnt">
<form name="effectform" action="action.jsp" method="post" onsubmit="return checkup()">

<fieldset>
<legend class="h3legend">음향효과</legend>
<p class="desc5">
<label><input type="checkbox" name="inimusic" value="Y" id="id_inimusic" <% if(ini1var.charAt(1) == 'Y') out.print("checked"); %>>시작시 음악 자동실행</label>
<label><input type="checkbox" name="oversnd" value="Y" id="id_oversnd" <% if(ini1var.charAt(6) == 'Y') out.print("checked"); %>>넘김</label>
<label><input type="checkbox" name="alertsnd" value="Y" id="id_alertsnd" <% if(ini1var.charAt(10) == 'Y') out.print("checked"); %>>경고창</label>
</p>
<p class="secthelp4">
- 모바일에는 적용되지 않습니다.
</p>
</fieldset>

<fieldset>
<legend class="h3legend">페이지 넘김</legend>
<p class="title">데스크톱</p>
<p class="desc"><select name="moving">
	<option value="001" <% if(askin[5].equals("001")) out.print("selected"); %>>밀어 넘김</option>
	<option value="002" <% if(askin[5].equals("002")) out.print("selected"); %>>가운데중심 넘김</option>
	<option value="003" <% if(askin[5].equals("003")) out.print("selected"); %>>위로올려 넘김</option>
	<option value="005" <% if(askin[5].equals("005")) out.print("selected"); %>>말아 넘김</option>
	<option value="006" <% if(askin[5].equals("006")) out.print("selected"); %>>반원회전 넘김</option>
	<option value="007" <% if(askin[5].equals("007")) out.print("selected"); %>>3D회전/열림</option>

	<option value="008" <% if(askin[5].equals("008")) out.print("selected"); %>>파일 캐비넷</option>
	<option value="009" <% if(askin[5].equals("009")) out.print("selected"); %>>프로펠러</option>
	<option value="004" <% if(askin[5].equals("004")) out.print("selected"); %>>회전 육면체</option>
	<option value="010" <% if(askin[5].equals("010")) out.print("selected"); %>>흩어져 넘김</option>

	<option value="301" <% if(askin[5].equals("301")) out.print("selected"); %>>없음(갤러리)</option>
	<option value="302" <% if(askin[5].equals("302")) out.print("selected"); %>>알파(갤러리)</option>
	<option value="303" <% if(askin[5].equals("303")) out.print("selected"); %>>블라인드(갤러리)</option>
	<option value="304" <% if(askin[5].equals("304")) out.print("selected"); %>>이동(갤러리)</option>
</select>
</p>
<!--html5break1//--> <!--bothbreak1//-->
<p class="title">모바일</p>
<p class="desc"><select name="movingm">
	<option value="001" <% if(askin[9].equals("001")) out.print("selected"); %>>밀어 넘김</option>
	<option value="002" <% if(askin[9].equals("002")) out.print("selected"); %>>가운데중심 넘김</option>
	<option value="003" <% if(askin[9].equals("003")) out.print("selected"); %>>위로올려 넘김</option>
	<option value="005" <% if(askin[9].equals("005")) out.print("selected"); %>>말아 넘김</option>
	<option value="006" <% if(askin[9].equals("006")) out.print("selected"); %>>반원회전 넘김</option>

	<option value="009" <% if(askin[9].equals("009")) out.print("selected"); %>>프로펠러</option>
	<option value="004" <% if(askin[9].equals("004")) out.print("selected"); %>>회전 육면체</option>
	<option value="010" <% if(askin[9].equals("010")) out.print("selected"); %>>흩어져 넘김</option>
	<option value="041" <% if(askin[9].equals("041")) out.print("selected"); %>>드래그 넘김</option>

	<option value="301" <% if(askin[9].equals("301")) out.print("selected"); %>>없음(갤러리)</option>
	<option value="302" <% if(askin[9].equals("302")) out.print("selected"); %>>알파(갤러리)</option>
	<option value="303" <% if(askin[9].equals("303")) out.print("selected"); %>>블라인드(갤러리)</option>
	<option value="304" <% if(askin[9].equals("304")) out.print("selected"); %>>이동(갤러리)</option>
</select>
</p>
<!--html5break:end1//--> <!--bothbreak:end1//-->
<p class="title">속도</p>
<p class="desc">
<label><input type="radio" name="overeffect" value="F" id="id_over1" checked>빠르게</label>
<label><input type="radio" name="overeffect" value="S" id="id_over2" <% if(ini1var.charAt(4) == 'S') out.print("checked"); %>>부드럽게</label>
</p>
</fieldset>

<fieldset>
<legend class="h3legend">자동넘김 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_slide')" onmouseout="hide_helpdiv();"></legend>
<p class="title">일반상태</p>
<p class="desc"><label>중간 멈춤 시간 <input type="text" name="slidetime" size="10" class="px" value="<%=slidetime%>"></label></p>
<p class="title">확대상태</p>
<p class="desc"><label>중간 멈춤 시간 <input type="text" name="bigslidetime" size="10" class="px" value="<%=bigslidetime%>"></label>
-> <label><input type="checkbox" name="largeslide" value="Y" id="id_largeslide" <% if(ini1var.charAt(13) == 'Y') out.print("checked"); %>>확대상태에서 자동넘김</label></p>
<p id="help_slide" class="secthelp">
- 1,000은 1초입니다. 기본 설정값은 작은 이미지가 800이며, 확대 이미지 1,600입니다.<br>
- '확대상태에서 자동넘김'을 체크하지 않으시면 확대상태에서도 일반상태로 변경 후 자동넘김이 작동합니다.<br>
- 모바일에도 설정값이 적용됩니다.
</p>
</fieldset>

<fieldset>
<legend class="h3legend">브라우저 창 크기 변경에 따른 작은 그림 크기(데스크톱)</legend>
<p class="title">동작</p>
<p class="desc">
	<label><input type="radio" name="synchro" value="N" id="id_synchro1" <% if(ini1var.charAt(17) == 'N') out.print("checked"); %>>기본 치수 유지</label>
	<label><input type="radio" name="synchro" value="Y" id="id_synchro2" <% if(ini1var.charAt(17) == 'Y') out.print("checked"); %>>여백 동기화</label>
	<label><input type="radio" name="synchro" value="S" id="id_synchro3" <% if(ini1var.charAt(17) == 'S') out.print("checked"); %>>이미지가 창보다 클 때 여백 동기화</label>
</p>
<p class="title">여백 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_margin')" onmouseout="hide_helpdiv();"></p>
<p class="desc"><input type="text" name="margin" size="5" class="px" value="<%=margin%>"></p>
<p id="help_margin" class="secthelp">
	- '여백 동기화'는 가로/세로의 비율 유지하여 폭과 높이 중 '여백' 조건을 충족하는 것을 기준으로 합니다.<br>
	- '여백'은 창의 시작면부터 이미지까지의 거리를 말합니다.(단위 : 픽셀)
</p>
</fieldset>

<fieldset>
<legend class="h3legend">확대</legend>
<p class="desc3">
<label><input type="checkbox" name="msenlarge" value="N" id="id_enlarge" <% if(ini1var.charAt(16) == 'N') out.print("checked"); %>>확대기능 제거</label>
<label><input type="checkbox" name="synlarge" value="Y" id="id_synlarge" <% if(ini1var.charAt(5) == 'Y') out.print("checked"); %>>좌우그림 같이 보여주기</label>
<label><input type="checkbox" name="en2step" value="Y" id="id_en2step" <% if(ini2var.charAt(3) == 'Y') out.print("checked"); %>>2단확대</label>
-> <label>2단확대 적용시 중간 너비 <input type="text" name="midwidth" size="5" value="<%=midwidth%>" class="px"></label></p>
</fieldset>

<fieldset>
<legend class="h3legend">모바일</legend>
<p class="desc3">
<label><input type="checkbox" name="spdrag" value="Y" id="id_spdrag" <% if(ini2var.charAt(10) == 'Y') out.print("checked"); %>>드래그 넘김</label>
<label><input type="checkbox" name="spbigimage" value="Y" id="id_spbigimage" <% if(ini2var.charAt(11) == 'Y') out.print("checked"); %>>큰그림만 사용(확대기능 제거)</label>
<label><input type="checkbox" name="spwidget" value="Y" id="id_spwidget" <% if(ini2var.charAt(14) == 'Y') out.print("checked"); %>>하단 페이징</label>
</p>
</fieldset>

<fieldset>
<legend class="h3legend">브라우저 창 크기 변경에 따른 작은 그림 크기(모바일)</legend>
<p class="title">동작</p>
<p class="desc">
	<label><input type="radio" name="msynchro" value="N" id="id_msynchro2" <% if(ini2var.charAt(13) == 'N') out.print("checked"); %>>기본 치수 유지</label>
	<label><input type="radio" name="msynchro" value="Y" id="id_msynchro3" <% if(ini2var.charAt(13) == 'Y') out.print("checked"); %>>여백 동기화</label>
	<label><input type="radio" name="msynchro" value="S" id="id_msynchro4" <% if(ini2var.charAt(13) == 'S') out.print("checked"); %>>이미지가 창보다 클 때 여백 동기화</label>
	<label><input type="radio" name="msynchro" value="D" id="id_msynchro1" <% if(ini2var.charAt(13) == 'D') out.print("checked"); %>>데스크톱 여백 동기화</label>
</p>
<p class="title">여백</p>
<p class="desc"><input type="text" name="mmargin" size="5" class="px" value="<%=mmargin%>"></p>
</fieldset>

<div class="submitbtn"><input type="submit" value="저장하기"></div>

<input type="hidden" name="act" value="effect">
<input type="hidden" name="askin" value="<%=askinG%>">
</form>

</main>

<%@ include file="inc_copy.jsp" %>
