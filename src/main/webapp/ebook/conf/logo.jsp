<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="ImageSize.jsp" %>
<%@ include file="admin.jsp" %>
<%
	if(cook_dir.equals("")){
		out.println(echo_script(snc_catalog("selfolder"), "basic.jsp", "",""));
		return;
	}

	String[] subar = {"design", "개별 로고", "name", ""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 개별 로고</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function checkup(obj){
	if(trimVal(obj.pixfile.value) == ""){
		alert("파일을 선택하셔야 합니다.");
		obj.pixfile.focus();
		return false;
	}

	if(confirm("변경하시겠습니까?")) return true;
	else return false;
}
function checkup_adapt(){
	if(confirm("설정값을 저장하시겠습니까?")) return true;
	else return false;
}
function checkup_del(){
	if(confirm("파일을 삭제하시겠습니까?")) return true;
	else return false;
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>
<%
	String pCode = get_parentCode(cook_cate);

	String catedata = "";
	String unifdata = "";

	f = new File(pathOfCata + "/category.txt");
	if(f.exists()){
		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			if(source.substring(0,10).equals(pCode)){
				if(source.substring(11,15).equals("cate")) catedata = source;
				else if(source.substring(11,15).equals("unif")) unifdata = source;
			}
		}
		fin.close();
		bin.close();
	}

	String logoadapt = "";
	if(!catedata.equals("")){
		String[] aaa = mysplit(catedata,"\t");
		logoadapt = aaa[4];
		//dist = aaa[2];
		//bascadapt = aaa[3];
		//mailadapt = aaa[5];
		//musicadapt = aaa[6];
	}

	String leftpos = "";
	String toppos = "";
	String logolink = "";
	f = new File(pathOfDir + "/ecatalog.txt");
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);
			if(skey.equals("logopos")){
				String logopos = source.substring(in+1);
				if(logopos.indexOf("x") > 0){
					StringTokenizer st = new StringTokenizer(logopos, "x");
					leftpos = (st.hasMoreTokens()) ? st.nextToken() : "";
					toppos  = (st.hasMoreTokens()) ? st.nextToken() : "";
				}
			}
			else if(skey.equals("logolink")){
				logolink = source.substring(in+1);
			}
		}
		fin.close();
		bin.close();
	}


	String buf_logoadapt = "개별적용";
	if(!logoadapt.equals("")){
		String logoConfName = "";
		f = new File(pathOfCata + "/catakind.txt");
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, "|");		// cate|setting|name|etc
			if(aaa[3].equals(logoadapt)){
				logoConfName = aaa[2];
				break;
			}
		}
		fin.close();
		bin.close();

		buf_logoadapt = "'" + logoConfName + "' 설정내용 전체적용";
	}

	String logoFile = "";
	String logoFileUrl = "";
	String malogoFile = "";
	String malogoFileUrl = "";
	String splogoFile = "";
	String splogoFileUrl = "";

	f = new File(pathOfDir);
	File[] folders = f.listFiles();
	for(int i = 0;i < folders.length;i++){
		String temp = folders[i].getName();
		if(temp.equals(".") || temp.equals("..")) continue;

		if(temp.length() > 4 && temp.substring(0,4).equals("logo")){
			logoFile = temp;
			logoFileUrl = webServer + "/catImage" + cook_catimage + "/" + cook_dir + "/" + temp;
		}
		else if(temp.length() > 6 && temp.substring(0,6).equals("malogo")){
			malogoFile = temp;
			malogoFileUrl = webServer + "/catImage" + cook_catimage + "/" + cook_dir + "/" + temp;
		}
		else if(temp.length() > 6 && temp.substring(0,6).equals("splogo")){
			splogoFile = temp;
			splogoFileUrl = webServer + "/catImage" + cook_catimage + "/" + cook_dir + "/" + temp;
		}
	}
%>
<main id="maincnt" class="maincnt">
<%
	SimpleDateFormat sdf = new SimpleDateFormat(get_date());
	ImageSize inImage = new ImageSize();

	int fsize1 = 0;
	String fdate1 = "";
	int[] logo_aaa = {0, 0};
	boolean logoExist = false;

	if(!logoFile.equals("")){
		logoExist = true;
		if(logoFile.endsWith("swf")){
			logo_aaa[0] = 100;
			logo_aaa[1] = 100;
		}
		else{
			inImage.setImage(pathOfDir + "/" + logoFile);
			logo_aaa[0] = inImage.getWidth();
			logo_aaa[1] = inImage.getHeight();
		}

		f = new File(pathOfDir + "/" + logoFile);
		fsize1 = (int)f.length()/1000;
		fdate1 = sdf.format(new Date(f.lastModified()));
	}
%>

<% if(logoExist == true){ %>
<section>
<h3>메인 로고 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_logo')" onmouseout="hide_helpdiv();"></h3>
<p class="desc4">
<% if(logoFile.endsWith("swf")){ %>
	<embed width="<%=logo_aaa[0]%>" height="<%=logo_aaa[1]%>" type="application/x-shockwave-flash" src="<%=logoFileUrl%>">
<% } else{ %>
	<img src="<%=logoFileUrl%>" alt="로고파일">
<% } %>
</p>
<p class="title">파일크기</p>
<p class="desc"><%=fsize1%> KB</p>
<p class="title">최종변경일</p>
<p class="desc"><%=fdate1%></p>
<p class="title">그림크기</p>
<p class="desc"><%=logo_aaa[0]%> x <%=logo_aaa[1]%></p>
<p class="title">파일변경</p>
<div class="desc">
	<form name="cataform" method="post" enctype="multipart/form-data" action="file_act.jsp?act=catalogo_each" onsubmit="return checkup(this)">
	<input type="file" name="pixfile" required>
	<input type="submit" value="변경하기">
	<input type="hidden" name="vari" value="indep">
	<input type="hidden" name="act" value="catalogo_each">
	</form>
</div>
<p class="title">파일삭제</p>
<div class="desc">
	<form name="catadelform" method="post" action="file_act.jsp" onsubmit="return checkup_del()">
	<input type="submit" value="삭제하기">
	<input type="hidden" name="fname" value="<%=logoFile%>">
	<input type="hidden" name="act" value="logdel">
	</form>
</div>
<p id="help_logo" class="secthelp">GIF/PNG/JPG/SWF 파일형식의 파일을 사용하실 수 있습니다.</p>
</section>

<% } else{ %>
<section>
<h3>메인 로고 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_logo')" onmouseout="hide_helpdiv();"></h3>
<p class="desc4">현재 폴더용 메인로고는 설정되어 있지 않습니다.</p>
<p class="title">파일설정</p>
<div class="desc">
	<form name="cataform" method="post" enctype="multipart/form-data" action="file_act.jsp?act=catalogo_each" onsubmit="return checkup(this)">
	<input type="file" name="pixfile" required>
	<input type="submit" value="설정하기">
	<input type="hidden" name="vari" value="indep">
	<input type="hidden" name="act" value="catalogo_each">
	</form>
</div>
<p id="help_logo" class="secthelp">GIF/PNG/JPG/SWF 파일형식의 파일을 사용하실 수 있습니다.</p>
</section>
<% } %>

<form name="posform" method="post" action="action.jsp" onsubmit="return checkup_adapt()">
<div class="nullform"></div>

<section>
<h3>메인 로고 위치 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_logopos')" onmouseout="hide_helpdiv();"></h3>
<p class="desc3">
	<label>x <input type="number" name="leftpos" value="<%=leftpos%>" class="px"></label>&nbsp;
	<label>y <input type="number" name="toppos" value="<%=toppos%>" class="px"></label>
	<input type="submit" value="설정하기">
</p>
<p id="help_logopos" class="secthelp">좌측상단으로 부터의 x축, y축 거리를 픽셀단위로 입력합니다.</p>
</section>

<section>
<h3>로고 클릭시 이동할 페이지(URL) <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_logourl')" onmouseout="hide_helpdiv();"></h3>
<p class="desc3">
	<input type="text" name="logolink" size="50" value="<%=logolink%>" class="px">
	<input type="submit" value="설정하기">
</p>
<p id="help_logourl" class="secthelp">
	- 절대경로를 사용하실 경우는 앞에 'http://'를 사용하셔야 합니다.<br>
	- 상대경로는 main.swf 파일이 있는 디렉토리를 기준으로 합니다.
</p>
</section>

<input type="hidden" name="act" value="logopos">
</form>

<%
	int fsize2 = 0;
	String fdate2 = "";
	int[] logo_bbb = {0, 0};
	boolean malogoExist = false;

	if(!malogoFile.equals("")){
		malogoExist = true;
		inImage.setImage(pathOfDir + "/" + malogoFile);
		logo_bbb[0] = inImage.getWidth();
		logo_bbb[1] = inImage.getHeight();

		f = new File(pathOfDir + "/" + malogoFile);
		fsize2 = (int)f.length()/1000;
		fdate2 = sdf.format(new Date(f.lastModified()));
	}
%>

<% if(malogoExist == true){ %>
<section>
<h3>메일발송 로고 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_maillogo')" onmouseout="hide_helpdiv();"></h3>
<p class="desc4"><img src="<%=malogoFileUrl%>" alt="메일발송 로고파일"></p>
<p class="title">파일크기</p>
<p class="desc"><%=fsize2%> KB</p>
<p class="title">최종변경일</p>
<p class="desc"><%=fdate2%></p>
<p class="title">그림크기</p>
<p class="desc"><%=logo_bbb[0]%> x <%=logo_bbb[1]%></p>
<p class="title">파일변경</p>
<div class="desc">
	<form name="mailform" method="post" enctype="multipart/form-data" action="file_act.jsp?act=maillogo_each" onsubmit="return checkup(this)">
	<input type="file" name="pixfile" required>
	<input type="submit" value="변경하기">
	<input type="hidden" name="vari" value="indep">
	<input type="hidden" name="act" value="maillogo_each">
	</form>
</div>
<p class="title">파일삭제</p>
<div class="desc">
	<form name="maildelform" method="post" action="file_act.jsp" onsubmit="return checkup_del()">
	 <input type="submit" value="삭제하기">
	<input type="hidden" name="fname" value="<%=malogoFile%>">
	<input type="hidden" name="act" value="logdel">
	</form>
</div>
<p id="help_maillogo" class="secthelp">GIF/PNG/JPG 파일형식의 파일을 사용하실 수 있습니다.</p>
</section>

<% } else{ %>
<section>
<h3>메일발송 로고 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_maillogo')" onmouseout="hide_helpdiv();"></h3>
<p class="desc4">현재 폴더용 메일로고는 설정되어 있지 않습니다.</p>
<p class="title">파일설정</p>
<div class="desc">
	<form name="mailform" method="post" enctype="multipart/form-data" action="file_act.jsp?act=maillogo_each" onsubmit="return checkup(this)">
	<input type="file" name="pixfile" required>
	<input type="submit" value="설정하기">
	<input type="hidden" name="vari" value="indep">
	<input type="hidden" name="act" value="maillogo_each">
	</form>
</div>
<p id="help_maillogo" class="secthelp">GIF/PNG/JPG 파일형식의 파일을 사용하실 수 있습니다.</p>
</section>
<% } %>

</main>

<%@ include file="inc_copy.jsp" %>
