<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	String[] subar = {"addon", "기본 메일", "", ""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 기본 메일</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	section label img{vertical-align:middle;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function checkup(){
	var dm = document.mailform;

	if(trimVal(dm.mbody.value) == ""){
		alert("메일본문의 내용이 없습니다.");
		dm.mbody.focus();
		return false;
	}

	if(trimVal(dm.copyright.value) == ""){
		alert("저작권 내용이 없습니다.");
		dm.copyright.focus();
		return false;
	}

	if(confirm("설정값을 저장하시겠습니까?")){
		dm.action = "action2.jsp";
		dm.target = "";
		return true;
	}
	else return false;
}
function preview(){
	var dm = document.mailform;

	if(trimVal(dm.mbody.value) == "" && trimVal(dm.copyright.value) == ""){
		alert("내용이 없으면 실행할 수 없습니다.");
		return;
	}

	var property = "toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,scrollbars=yes,copyhistory=no,";
	property += "width=630,height=550,left=0,top=0";
	window.open("../xull.html","preWin",property);

	dm.action = "nw_mailpre.jsp";
	dm.target = "preWin";
	dm.submit();
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>
<%
	int j = 0;
	String mailskin = "";
	String copyright = "";
	StringBuffer cont = new StringBuffer();
	f = new File(pathOfCata + "/mail.txt");
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);

			if(source.equals("")) j = 1;
			else if(j == 1) cont.append(source).append("\n");
			else if(source.substring(0,10).equals("copyright:")) copyright = source.substring(11);
			else if(source.substring(0,9).equals("mailskin:")) mailskin = source.substring(9);
		}
		fin.close();
		bin.close();
	}

	String mbody = strTrim(cont.toString());
%>
<main id="maincnt" class="maincnt">
<form name="mailform" action="action2.jsp" method="post" onsubmit="return checkup()">

<p><label>메일 본문에 첨가할 내용<br>
<textarea name="mbody" rows="10" cols="100" class="px"><%=mbody%></textarea></label></p>

<p><label>저작권 내용<br>
<textarea name="copyright" rows="3" cols="100" class="px"><%=copyright%></textarea></label></p>

<fieldset>
<legend class="normlegend">스킨</legend><br>
<label><input type="radio" name="mailskin" value="001" id="id_mskin1" <% if(mailskin.equals("") || mailskin.equals("001")) out.print("checked"); %>>
	<img src="<%=webServer%>/skin/pix/mailskin001.jpg" width="127" height="101" alt="메일스킨 001"></label>
<label><input type="radio" name="mailskin" value="002" id="id_mskin2" <% if(mailskin.equals("002")) out.print("checked"); %>>
	<img src="<%=webServer%>/skin/pix/mailskin002.jpg" width="127" height="101" alt="메일스킨 002"></label>
</fieldset>

<div class="submitbtn"><input type="submit" value="저장하기"></div>

<input type="hidden" name="act" value="mail_def">
<input type="hidden" name="cook_dir" value="def">
</form>

<% if(!copyright.equals("") && !mbody.equals("")){ %>
<p><a href="javascript:preview();"><img src="<%=confServer%>/pix/link.gif" width="5" height="9" alt=""> 메일 내용 미리보기</a></p>
<% } %>

</main>

<%@ include file="inc_copy.jsp" %>
