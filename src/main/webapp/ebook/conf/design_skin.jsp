<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	if(cook_dir.equals("")){
		out.println(echo_script(snc_catalog("selfolder"), "basic.jsp", "",""));
		return;
	}

	String[] subar = {"design", "스킨", "name", ""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 스킨</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	.mainmenu{height:250px;overflow:auto;}
	table td img{display:block;float:left;}
	input{vertical-align:middle;}
	label{margin-right:5px;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function checkup(){
	if(confirm("설정값을 저장하시겠습니까?")) return true;
	else return false;
}
</script>
</head>

<body onload="onload_func();">

<%@ include file="inc_menu.jsp" %>
<%
	String askinG = "";
	String bskinG = "";
	String ini1var = "";
	String ini2var = "";

	f = new File(pathOfDir + "/ecatalog.txt");
	if(f.exists()){
		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);
			String sval = source.substring(in+1);

			if(skey.equals("askin")) askinG = sval;
			else if(skey.equals("bskin")) bskinG = sval;
			else if(skey.equals("ini1var")) ini1var = sval;
			else if(skey.equals("ini2var")) ini2var = sval;
		}
		fin.close();
		bin.close();
	}

	if(ini1var.equals("")) ini1var = "                      ";
	if(ini2var.equals("")) ini2var = "                      ";

	if(askinG.equals("")) askinG = "001|001|001|001|001|001|001|001|001|001";
	if(bskinG.equals("")) bskinG = "001|001|001|001|001|001|001|001|001|001";
	String[] askin = mysplit(askinG, "|");
	String[] bskin = mysplit(bskinG, "|");

	Vector vt_flash = new Vector();
	f = new File(pathOfAcc + "/skin");
	File[] xolders = f.listFiles();
	int alen = xolders.length;
	for(int i = 0;i < alen;i++){
		String temp = xolders[i].getName();
		if(temp.equals(".") || temp.equals("..") || !temp.endsWith("swf")) continue;
		if(temp.length() != 14 && temp.length() != 15) continue;
		int inx = temp.lastIndexOf(".");
		if(!temp.substring(0,7).equals("display") || inx == -1) continue;
		vt_flash.addElement(temp.substring(7,inx));
	}
	Collections.sort(vt_flash);

	Vector vt_html5 = new Vector();
	f = new File(pathOfAcc + "/skin5");
	xolders = f.listFiles();
	alen = xolders.length;
	for(int i = 0;i < alen;i++){
		String temp = xolders[i].getName();
		if(temp.equals(".") || temp.equals("..") || !temp.endsWith("css")) continue;
		if(temp.length() != 9 && temp.length() != 10) continue;
		int inx = temp.lastIndexOf(".");
		if(!temp.substring(0,2).equals("mm") || inx == -1) continue;
		vt_html5.addElement(temp.substring(2,inx));
	}
	Collections.sort(vt_html5);

	Vector vt;
	if(od_platform.equals("both") || od_platform.equals("flash") || od_platform.equals("")){
		vt = (Vector)vt_flash.clone();
	}
	else{
		vt = (Vector)vt_html5.clone();
	}

	int itemLen = vt.size();
	int k = 60;
	StringBuffer userskin = new StringBuffer("");
	for(int i = 0;i < itemLen;i++){
		String seq = (String)vt.get(i);
		char firstval = seq.charAt(0);
		char secondval = seq.charAt(1);
		char lastval = seq.charAt(seq.length()-1);

		if(firstval == '4' || firstval == '6' || firstval == '9'){
			f = new File(pathOfAcc + "/skin/pix/mainmenu" + seq + ".jpg");
			String shotpix = f.exists() ? "mainmenu"+seq+".jpg" : "mainmenu"+seq.substring(0,1)+"00.jpg";
			String chec = askin[0].equals(seq) ? "checked" : "";

			userskin.append("<tr><td><input type=\"radio\" name=\"mainmenu\" value=\""+seq+"\" id=\"id_mainmenu"+k+"\" "+chec+"> "+seq+"</td>\n");
			userskin.append("<td><label for=\"id_mainmenu"+k+"\"><img src=\""+webServer+"/skin/pix/"+shotpix+"\" ");
			userskin.append("width=\"383\" height=\"57\" alt=\"메인메뉴 "+seq+"\"></label></td>\n");
			userskin.append("</tr>\n");
			k++;
		}
	}

	StringBuffer usrmskin = new StringBuffer("");
	if(od_platform.equals("both") || od_platform.equals("html5") || od_platform.equals("")){
		itemLen = vt_html5.size();
		for(int i = 0;i < itemLen;i++){
			String seq = (String)vt_html5.get(i);
			char firstval = seq.charAt(0);
			char secondval = seq.charAt(1);
			char lastval = seq.charAt(seq.length()-1);

			if(firstval == '4' || firstval == '6' || firstval == '9'){
				f = new File(pathOfAcc + "/skin/pix/mainmenu" + seq + ".jpg");
				String shotpix = f.exists() ? "mainmenu"+seq+".jpg" : "mainmenu"+seq.substring(0,1)+"00.jpg";
				String chec = askin[7].equals(seq) ? "checked" : "";

				usrmskin.append("<tr><td><input type=\"radio\" name=\"mskin\" value=\""+seq+"\" id=\"id_mskin"+k+"\" "+chec+"> "+seq+"</td>\n");
				usrmskin.append("<td><label for=\"id_mskin"+k+"\"><img src=\""+webServer+"/skin/pix/"+shotpix+"\" ");
				usrmskin.append("width=\"383\" height=\"57\" alt=\"메인메뉴 "+seq+"\"></label></td>\n");
				usrmskin.append("</tr>\n");
				k++;
			}
		}
	}
%>
<main id="maincnt" class="maincnt">
<form name="skinform" action="action.jsp" method="post" onsubmit="return checkup()">

<fieldset>
<legend class="h3legend">메인메뉴(데스크톱)</legend>
<div class="mainmenu">
<table>
	<tr>
		<td><input type="radio" name="mainmenu" value="001" id="id_mainmenu1" <% if(askin[0].equals("001")) out.print("checked"); %> required> 001</td>
		<td><label for="id_mainmenu1"><img src="<%=webServer%>/skin/pix/mainmenu001.jpg" width="383" height="50" alt="메인메뉴 001"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mainmenu" value="010" id="id_mainmenu2" <% if(askin[0].equals("010")) out.print("checked"); %>> 010</td>
		<td><label for="id_mainmenu2"><img src="<%=webServer%>/skin/pix/mainmenu010.jpg" width="383" height="50" alt="메인메뉴 010"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mainmenu" value="011" id="id_mainmenu3" <% if(askin[0].equals("011")) out.print("checked"); %>> 011</td>
		<td><label for="id_mainmenu3"><img src="<%=webServer%>/skin/pix/mainmenu011.jpg" width="383" height="57" alt="메인메뉴 011"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mainmenu" value="012" id="id_mainmenu12" <% if(askin[0].equals("012")) out.print("checked"); %>> 012</td>
		<td><label for="id_mainmenu12"><img src="<%=webServer%>/skin/pix/mainmenu012.jpg" width="383" height="57" alt="메인메뉴 012"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mainmenu" value="013" id="id_mainmenu13" <% if(askin[0].equals("013")) out.print("checked"); %>> 013</td>
		<td><label for="id_mainmenu13"><img src="<%=webServer%>/skin/pix/mainmenu013.jpg" width="383" height="57" alt="메인메뉴 013"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mainmenu" value="016" id="id_mainmenu4" <% if(askin[0].equals("016")) out.print("checked"); %>> 016</td>
		<td><label for="id_mainmenu4"><img src="<%=webServer%>/skin/pix/mainmenu016.jpg" width="383" height="57" alt="메인메뉴 016"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mainmenu" value="017" id="id_mainmenu11" <% if(askin[0].equals("017")) out.print("checked"); %>> 017</td>
		<td><label for="id_mainmenu11"><img src="<%=webServer%>/skin/pix/mainmenu017.jpg" width="383" height="57" alt="메인메뉴 017"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mainmenu" value="018" id="id_mainmenu5" <% if(askin[0].equals("018")) out.print("checked"); %>> 018</td>
		<td><label for="id_mainmenu5"><img src="<%=webServer%>/skin/pix/mainmenu018.jpg" width="383" height="57" alt="메인메뉴 018"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mainmenu" value="020" id="id_mainmenu6" <% if(askin[0].equals("020")) out.print("checked"); %>> 020(하단)</td>
		<td><label for="id_mainmenu6"><img src="<%=webServer%>/skin/pix/mainmenu020.jpg" width="383" height="57" alt="메인메뉴 020"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mainmenu" value="021" id="id_mainmenu7" <% if(askin[0].equals("021")) out.print("checked"); %>> 021(하단)</td>
		<td><label for="id_mainmenu7"><img src="<%=webServer%>/skin/pix/mainmenu021.jpg" width="383" height="57" alt="메인메뉴 021"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mainmenu" value="030" id="id_mainmenu8" <% if(askin[0].equals("030")) out.print("checked"); %>> 030(좌측)</td>
		<td><label for="id_mainmenu8"><img src="<%=webServer%>/skin/pix/mainmenu030.jpg" width="383" height="57" alt="메인메뉴 030"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mainmenu" value="301" id="id_mainmenu9" <% if(askin[0].equals("301")) out.print("checked"); %>> 301(갤러리)</td>
		<td><label for="id_mainmenu9"><img src="<%=webServer%>/skin/pix/mainmenu301.jpg" width="383" height="57" alt="메인메뉴 301"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mainmenu" value="302" id="id_mainmenu10" <% if(askin[0].equals("302")) out.print("checked"); %>> 302(갤러리)</td>
		<td><label for="id_mainmenu10"><img src="<%=webServer%>/skin/pix/mainmenu302.jpg" width="383" height="57" alt="메인메뉴 302"></label></td>
	</tr>
	<%=userskin.toString()%>
</table>
</div>
</fieldset>

<fieldset style="clear:both;">
<legend class="h3legend">메뉴 옵션</legend>
<p class="desc5">
<label><input type="checkbox" name="rightmenu" value="Y" id="id_rightmenu" <% if(ini1var.charAt(18) == 'Y') out.print("checked"); %>>마우스 
우측버튼 클릭시 팝업메뉴 제거</label><img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명" class="helpshow" onmouseover="show_helpdiv(this, 'help_rightmenu')" onmouseout="hide_helpdiv();">
<label><input type="checkbox" name="bookshadow" value="000" id="id_shadow" <% if(askin[3].equals("000")) out.print("checked"); %>>책가운데 그림자 효과 적용 안함</label>
<label><input type="checkbox" name="hidemailbtn" value="Y" id="id_hidemailbtn" <% if(ini2var.charAt(4) == 'Y') out.print("checked"); %>>메일발송 사용 안함</label>
<label><input type="checkbox" name="altmouse" value="N" id="id_alt" <% if(ini1var.charAt(0) == 'N') out.print("checked"); %>>마우스 풍선말 보이지 않음</label>
</p>
<p id="help_menuhide" class="secthelp">'메뉴 자동숨기기'는 스킨에 따라 적용되지 않을 수 있습니다.</p>
<p id="help_rightmenu" class="secthelp">'우측버튼 클릭시 팝업메뉴 제거' 플래시 버전에만 적용됩니다.</p>
</fieldset>

<fieldset style="clear:both;float:left;width:370px;">
<legend class="h3legend">책갈피 버튼 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_optadapt')" onmouseout="hide_helpdiv();"></legend>
<p class="desc5">
<label><input type="radio" name="spacerbutton" value="001" id="id_spacer1" <% if(askin[2].equals("001")) out.print("checked"); %>> 
  <img src="<%=webServer%>/skin/pix/spacerbutton001.gif" width="17" height="29" alt="책갈피버튼 001"></label>
<label><input type="radio" name="spacerbutton" value="002" id="id_spacer2" <% if(askin[2].equals("002")) out.print("checked"); %>> 
  <img src="<%=webServer%>/skin/pix/spacerbutton002.gif" width="17" height="29" alt="책갈피버튼 002"></label>
<label><input type="radio" name="spacerbutton" value="003" id="id_spacer3" <% if(askin[2].equals("003")) out.print("checked"); %>> 
  <img src="<%=webServer%>/skin/pix/spacerbutton003.gif" width="17" height="29" alt="책갈피버튼 003"></label>
<label><input type="radio" name="spacerbutton" value="004" id="id_spacer4" <% if(askin[2].equals("004")) out.print("checked"); %>> 
  <img src="<%=webServer%>/skin/pix/spacerbutton004.gif" width="17" height="29" alt="책갈피버튼 004"></label>
<label><input type="radio" name="spacerbutton" value="005" id="id_spacer6" <% if(askin[2].equals("005")) out.print("checked"); %>>스티커</label>
<label><input type="radio" name="spacerbutton" value="000" id="id_spacer5" <% if(askin[2].equals("000")) out.print("checked"); %>>적용 안함</label>
</p>
<div id="help_optadapt" class="secthelp">메인메뉴 스킨에 따라 선택된 것이 적용되지 않을 수 있습니다.</div>
</fieldset>

<fieldset style="float:right;width:370px;">
<legend class="h3legend">도움말</legend>
<p class="desc5">
<label><input type="radio" name="help" value="001" id="id_help1" <% if(bskin[4].equals("001")) out.print("checked"); %>>기본</label>
<label><input type="radio" name="help" value="002" id="id_help2" <% if(bskin[4].equals("002")) out.print("checked"); %>>간편</label>
</p>
</fieldset>

<fieldset style="clear:both;float:left;width:370px;">
<legend class="h3legend">이전다음 버튼 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_optadapt')" onmouseout="hide_helpdiv();"></legend>
<p class="desc5">
<label><input type="radio" name="onsmimage" value="001" id="id_onsmimage1" <% if(askin[4].equals("001")) out.print("checked"); %>> 
  <img src="<%=webServer%>/skin/pix/onsmimage001.gif" width="30" height="29" alt="이전다음버튼 001"></label>
<label><input type="radio" name="onsmimage" value="002" id="id_onsmimage2" <% if(askin[4].equals("002")) out.print("checked"); %>> 
  <img src="<%=webServer%>/skin/pix/onsmimage002.gif" width="23" height="29" alt="이전다음버튼 002"></label>
<label><input type="radio" name="onsmimage" value="003" id="id_onsmimage3" <% if(askin[4].equals("003")) out.print("checked"); %>> 
  <img src="<%=webServer%>/skin/pix/onsmimage003.gif" width="24" height="29" alt="이전다음버튼 003"></label>
<label><input type="radio" name="onsmimage" value="004" id="id_onsmimage4" <% if(askin[4].equals("004")) out.print("checked"); %>> 
  <img src="<%=webServer%>/skin/pix/onsmimage004.gif" width="30" height="29" alt="이전다음버튼 004"></label>
<label><input type="radio" name="onsmimage" value="008" id="id_onsmimage5" <% if(askin[4].equals("008")) out.print("checked"); %>> 
  <img src="<%=webServer%>/skin/pix/onsmimage008.gif" width="30" height="29" alt="이전다음버튼 008"></label>
</p>
</fieldset>

<fieldset style="float:right;width:370px;">
<legend class="h3legend">확대모드 상하좌우 버튼</legend>
<p class="desc5">
<label><input type="radio" name="onbigimage" value="001" id="id_onbigimage1" <% if(askin[6].equals("001")) out.print("checked"); %>> 
  <img src="<%=webServer%>/skin/pix/onbigimage001.gif" width="22" height="29" alt="확대모드 상하좌우 버튼 001"></label>
<label><input type="radio" name="onbigimage" value="002" id="id_onbigimage2" <% if(askin[6].equals("002")) out.print("checked"); %>> 
  <img src="<%=webServer%>/skin/pix/onbigimage002.gif" width="30" height="29" alt="확대모드 상하좌우 버튼 002"></label>
<label><input type="radio" name="onbigimage" value="003" id="id_onbigimage3" <% if(askin[6].equals("003")) out.print("checked"); %>> 
  <img src="<%=webServer%>/skin/pix/onbigimage003.gif" width="29" height="29" alt="확대모드 상하좌우 버튼 003"></label>
<label><input type="radio" name="onbigimage" value="004" id="id_onbigimage4" <% if(askin[6].equals("004")) out.print("checked"); %>> 
  <img src="<%=webServer%>/skin/pix/onbigimage004.gif" width="33" height="29" alt="확대모드 상하좌우 버튼 004"></label>
<label><input type="radio" name="onbigimage" value="005" id="id_onbigimage5" <% if(askin[6].equals("005")) out.print("checked"); %>> 
  <img src="<%=webServer%>/skin/pix/onbigimage005.gif" width="19" height="29" alt="확대모드 상하좌우 버튼 005"></label>
</p>
</fieldset>

<fieldset style="clear:both;float:left;width:370px;">
<legend class="h3legend">인쇄</legend>
<p class="desc5">
<label><input type="radio" name="print" value="001" id="id_print1" <% if(bskin[6].equals("001")) out.print("checked"); %>>구간설정</label>
<label><input type="radio" name="print" value="999" id="id_print2" <% if(bskin[6].equals("999")) out.print("checked"); %>>확대모드에서 한장씩</label>
<label><input type="radio" name="print" value="000" id="id_print3" <% if(bskin[6].equals("000")) out.print("checked"); %>>적용 안함</label>
</p>
</fieldset>

<fieldset style="float:right;width:370px;">
<legend class="h3legend">이동 콤보박스 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_combo')" onmouseout="hide_helpdiv();"></legend>
<p class="desc5">
<label><input type="radio" name="combo" value="001" id="id_combo1" <% if(bskin[0].equals("001")) out.print("checked"); %>>분류내 이동</label>
<label><input type="radio" name="combo" value="002" id="id_combo2" <% if(bskin[0].equals("002")) out.print("checked"); %>>전체 이동</label>
<label><input type="radio" name="combo" value="000" id="id_combo3" <% if(bskin[0].equals("000")) out.print("checked"); %>>적용 안함</label>
</p>
</fieldset>
<div id="help_combo" class="secthelp">카탈로그끼리 이동할 수 있는 콤보박스를 말합니다. 전체이동은 최상위 분류 내의 모든 분류/카탈로그를 표시합니다.</div>

<!--html5break1//--> <!--bothbreak1//-->
<fieldset style="clear:both;">
<legend class="h3legend">메인메뉴(모바일)</legend>
<div class="mainmenu">
<table>
	<tr>
		<td><input type="radio" name="mskin" value="001" id="id_mskin1" <% if(askin[7].equals("001")) out.print("checked"); %> required> 001</td>
		<td><label for="id_mskin1"><img src="<%=webServer%>/skin/pix/mainmenu001.jpg" width="383" height="50" alt="메인메뉴 001"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mskin" value="010" id="id_mskin2" <% if(askin[7].equals("010")) out.print("checked"); %>> 010</td>
		<td><label for="id_mskin2"><img src="<%=webServer%>/skin/pix/mainmenu010.jpg" width="383" height="50" alt="메인메뉴 010"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mskin" value="011" id="id_mskin3" <% if(askin[7].equals("011")) out.print("checked"); %>> 011</td>
		<td><label for="id_mskin3"><img src="<%=webServer%>/skin/pix/mainmenu011.jpg" width="383" height="57" alt="메인메뉴 011"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mskin" value="012" id="id_mskin10" <% if(askin[7].equals("012")) out.print("checked"); %>> 012</td>
		<td><label for="id_mskin10"><img src="<%=webServer%>/skin/pix/mainmenu012.jpg" width="383" height="57" alt="메인메뉴 012"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mskin" value="016" id="id_mskin16" <% if(askin[7].equals("016")) out.print("checked"); %>> 016</td>
		<td><label for="id_mskin16"><img src="<%=webServer%>/skin/pix/mainmenu016.jpg" width="383" height="57" alt="메인메뉴 016"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mskin" value="017" id="id_mskin17" <% if(askin[7].equals("017")) out.print("checked"); %>> 017</td>
		<td><label for="id_mskin17"><img src="<%=webServer%>/skin/pix/mainmenu017.jpg" width="383" height="57" alt="메인메뉴 017"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mskin" value="018" id="id_mskin18" <% if(askin[7].equals("018")) out.print("checked"); %>> 018</td>
		<td><label for="id_mskin18"><img src="<%=webServer%>/skin/pix/mainmenu018.jpg" width="383" height="57" alt="메인메뉴 018"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mskin" value="020" id="id_mskin20" <% if(askin[7].equals("020")) out.print("checked"); %>> 020(하단)</td>
		<td><label for="id_mskin20"><img src="<%=webServer%>/skin/pix/mainmenu020.jpg" width="383" height="57" alt="메인메뉴 020"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mskin" value="021" id="id_mskin21" <% if(askin[7].equals("021")) out.print("checked"); %>> 021(하단)</td>
		<td><label for="id_mskin21"><img src="<%=webServer%>/skin/pix/mainmenu021.jpg" width="383" height="57" alt="메인메뉴 021"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mskin" value="030" id="id_mskin30" <% if(askin[7].equals("030")) out.print("checked"); %>> 030(좌측)</td>
		<td><label for="id_mskin30"><img src="<%=webServer%>/skin/pix/mainmenu030.jpg" width="383" height="57" alt="메인메뉴 030"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mskin" value="301" id="id_mskin301" <% if(askin[7].equals("301")) out.print("checked"); %>> 301(갤러리)</td>
		<td><label for="id_mskin301"><img src="<%=webServer%>/skin/pix/mainmenu301.jpg" width="383" height="57" alt="메인메뉴 301"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mskin" value="302" id="id_mskin302" <% if(askin[7].equals("302")) out.print("checked"); %>> 302(갤러리)</td>
		<td><label for="id_mskin302"><img src="<%=webServer%>/skin/pix/mainmenu302.jpg" width="383" height="57" alt="메인메뉴 302"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mskin" value="041" id="id_mskin41" <% if(askin[7].equals("041")) out.print("checked"); %>> 041(모바일)</td>
		<td><label for="id_mskin41"><img src="<%=webServer%>/skin/pix/mainmenu041.jpg" width="383" height="57" alt="메인메뉴 041"></label></td>
	</tr>
	<tr>
		<td><input type="radio" name="mskin" value="042" id="id_mskin42" <% if(askin[7].equals("042")) out.print("checked"); %>> 042(모바일)</td>
		<td><label for="id_mskin42"><img src="<%=webServer%>/skin/pix/mainmenu042.jpg" width="383" height="57" alt="메인메뉴 042"></label></td>
	</tr>
	<%=usrmskin.toString()%>
</table>
</div>
</fieldset>
<!--html5break:end1//--> <!--bothbreak:end1//-->

<!--flashbreak:end1//-->

<div class="submitbtn"><input type="submit" value="저장하기"></div>

<input type="hidden" name="askin" value="<%=askinG%>">
<input type="hidden" name="bskin" value="<%=bskinG%>">
<input type="hidden" name="act" value="skin">
</form>

</main>

<%@ include file="inc_copy.jsp" %>
