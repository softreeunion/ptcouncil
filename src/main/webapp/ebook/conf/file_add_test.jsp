<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	String kd = request.getParameter("kd");
	if(kd == null) kd = "";

	String ent_server = (request.getServerPort() == 80) ? request.getServerName() : request.getServerName() + ":" + request.getServerPort();
	String linkDir = "http://" + ent_server + wasServer + "/conf/";

	String[] subar = {"file", "", "multi","cata"};
	String action = "file_act.jsp";

	if(kd.equals("media")){
		action = "media_act.jsp";
		subar[1] = "음악파일";
		subar[2] = "";
		subar[3] = "media";
	}
	else if(kd.equals("flash")){
		action = "flash_act.jsp";
		subar[1] = "링크파일";
		subar[2] = "";
		subar[3] = "flash";
	}
	else{
		action = "file_act.jsp";
		subar[1] = "기본파일";
		subar[2] = "";
		subar[3] = "cata";
	}
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 업로드 테스트</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script>
function checkup(){
	if(confirm("추기하시겠습니까?")) return true;
	return false;
}
function checkdel(){
	if(confirm("삭제하시겠습니까?")) return true;
	return false;
}
</script>
</head>

<body>
<%@ include file="inc_menu.jsp" %>


<main class="maincnt">

<div style="margin-top:50px;">
<form name="addform" method="post" enctype="multipart/form-data" action="file_madd.jsp" onsubmit="return checkup()">
	<span style="width:100"><img src="pix/li.gif" width="5" height="5" align="middle"> 파일선택</span>
	<input type="file" name="Filedata" size="50" class="px">
	<input type="submit" value="추가하기" class="sx">
	<input type="hidden" name="kd" value="cata">
	<input type="hidden" name="catimage" value="<%=cook_catimage%>">
	<input type="hidden" name="Dir" value="<%=cook_dir%>">
	<input type="hidden" name="url" value="http://www.ikissyou.com/IKY_Ebook/conf/">
</form>
</div>


<div style="margin-top:50px;">
<form name="delform" method="post" enctype="multipart/form-data" action="swf_filelist.jsp?act=del&&kd=cata&Dir=1&catimage=" onsubmit="return checkdel()">
	<span style="width:100"><img src="pix/li.gif" width="5" height="5" align="middle"> 파일삭제</span>
	<input type="hidden" name="sd" value="test_browser.pdf">
	<input type="submit" value="삭제하기" class="sx">
</form>
</div>

</main>

<%@ include file="inc_copy.jsp" %>
