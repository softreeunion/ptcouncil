<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	/* For standard version Only. (v) Nude version */
	if(cook_dir.equals("")){
		out.println(echo_script(snc_catalog("selfolder"), "basic.jsp", "",""));
		return;
	}

	String[] subar = {"design", "전문가 설정", "name", ""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 전문가 설정</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	label{margin-right:5px;}
	input{vertical-align:middle;}
	textarea{white-space:pre-line;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function checkup(){
	if(confirm("설정값을 저장하시겠습니까?")) return true;
	else return false;
}
function xmlnext(fname){
	newWindow(2, 'nw_xmledit.jsp?fname=' + fname, 800, 600, 0, 0, 'xe');
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>
<%
	String askinG = "";
	Hashtable ahash = new Hashtable();
	f = new File(pathOfDir + "/ecatalog.txt");
	if(f.exists()){
		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);
			String sval = source.substring(in+1);

			if(skey.equals("askin")) askinG = sval;
			else ahash.put(skey, sval);
		}
		fin.close();
		bin.close();
	}

	if(askinG.equals("")) askinG = "001|001|001|001|001|001|001|001|001|001";

	String[] askin = mysplit(askinG, "|");
	String ini1var = ahash.containsKey("ini1var") ? (String)ahash.get("ini1var") : "";
	if(ini1var.equals("")) ini1var = "                      ";

	String ini2var = ahash.containsKey("ini2var") ? (String)ahash.get("ini2var") : "";
	if(ini2var.equals("") || ini2var.length() != 5) ini2var += "NNNNN";
	
	String catpasswd = ahash.containsKey("catpasswd") ? (String)ahash.get("catpasswd") : "";
	String etcopt = ahash.containsKey("etcopt") ? (String)ahash.get("etcopt") : "";
%>
<main id="maincnt" class="maincnt">
<form name="effectform" action="action.jsp" method="post" onsubmit="return checkup()">

<section>
<h3>기본 옵션</h3>
<p class="title"><label for="catpasswd">비밀번호</label></p>
<p class="desc"><input type="password" name="catpasswd" id="catpasswd" size="8" value="<%=catpasswd%>" class="px"></p>

<p class="title">기타</p>
<p class="desc">
<label><input type="checkbox" name="cipher" value="Y" id="id_cipher" <% if(ini2var.charAt(6) == 'Y') out.print("checked"); %>>암호화 전송</label>
<label><input type="checkbox" name="inifocus" value="Y" id="id_inifocus" <% if(ini1var.charAt(19) == 'Y') out.print("checked"); %>>시작시 포커스 제거</label>
<label><input type="checkbox" name="xmltxt" value="T" id="id_xmltxt" <% if(ini2var.charAt(15) == 'T') out.print("checked"); %>>xml확장자를 txt확장자로</label>
</p>

<p class="secthelp2">- 비밀번호를 설정하시면 비밀번호를 입력해야만 카탈로그를 볼 수 있습니다.</p>
</section>

<section>
<h3>메뉴 옵션</h3>
<p class="desc5">
<label><input type="checkbox" name="menuhide" value="Y" id="id_menuhide" <% if(ini1var.charAt(15) == 'Y') out.print("checked"); %>>메뉴 자동숨기기</label>
<label><input type="checkbox" name="lowerhide" value="Y" id="id_lowerhide" <% if(ini1var.charAt(8) == 'Y') out.print("checked"); %>>하단메뉴 감추기</label>
<label><input type="checkbox" name="exptotal" value="N" id="id_exptotal" <% if(ini1var.charAt(20) == 'N') out.print("checked"); %>>'탐색'에서 '전체' 버튼 삭제</label>
</p>
<p class="secthelp2">- '메뉴 자동숨기기'는 스킨에 따라 적용되지 않을 수 있습니다.</p>
</section>
<!--html5break1//--> <!--bothbreak1//-->
<section>
<h3>모바일 옵션</h3>
<fieldset>
<legend class="title">렌더링</legend>
<p class="desc">
<label><input type="radio" name="imgrender" value="I" id="id_imgrender1" <% if(ini2var.charAt(17) == 'I') out.print("checked"); %>>image 렌더링</label>&nbsp;
<label><input type="radio" name="imgrender" value="C" id="id_imgrender2" <% if(ini2var.charAt(17) == 'C') out.print("checked"); %>>canvas 렌더링</label>
</p>
</fieldset>
<fieldset>
<legend class="title">큰그림만 사용시</legend>
<p class="desc">
<label><input type="radio" name="pchzoom" value="Y" id="id_pchzoom1" <% if(ini2var.charAt(18) == 'Y') out.print("checked"); %>>카탈로그 핀치줌</label>&nbsp;
<label><input type="radio" name="pchzoom" value="N" id="id_pchzoom2" <% if(ini2var.charAt(18) == 'N') out.print("checked"); %>>브라우저 핀치줌</label>
</p>
</fieldset>

<p class="title">기타</p>
<p class="desc">
<label><input type="checkbox" name="smsizediff" value="Y" id="id_smsizediff" <% if(ini2var.charAt(20) == 'Y') out.print("checked"); %>>이미지 크기 페이지마다 다름</label>
</p>
</section>

<section>
<h3>사용자 스킨</h3>
<p class="desc5"><button type="button" onclick="xmlnext('display.txt')">display.txt 파일 편집</button></p>
</section>
<!--html5break:end1//--> <!--bothbreak:end1//-->
<section>
<h3>검색 옵션</h3>
<p class="desc">
<label><input type="checkbox" name="casesen" value="N" id="id_casesen" <% if(ini2var.charAt(16) == 'N') out.print("checked"); %>>대소문자 구분없음
</p>
</section>

<section>
<h3>기타 옵션</h3>
<p class="desc">
<textarea name="etcopt" rows="5" cols="90" class="px"><%=etcopt%></textarea>
</p>
</section>

<div class="submitbtn"><input type="submit" value="저장하기"></div>

<input type="hidden" name="act" value="expert">
<input type="hidden" name="askin" value="<%=askinG%>">
</form>

</main>

<%@ include file="inc_copy.jsp" %>
