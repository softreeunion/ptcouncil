<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="unify_func.jsp" %>
<%@ include file="admin.jsp" %>
<%
	/*	ecatalog : ini1var,frpage,pageno,smwidth,smheight,bgwidth,bgheight,askin,bskin,margin
		           bground,titlebar,shopcolor,shopstr,logolink,slidetime,bigslidetime,searchurl,downfile,downtext
		askin    : mainmenu|bookdeco|spacerbutton|bookshadow|onsmimage|moving|onbigimage|guide|explorer|movingm
		bskin    : combo|soundmenu|mokcha|descript(gal)|help|mail|print|maindeco|spacer|search
		ini1var  : altmouse,inimusic,indexview,indextype,overtype,pairview,oversound,japan,soundhide,searchtype
		           alertsound,fileseq,lastpage,largeslide,startani,menuautohide,clickenlarge,synchro,contextmenu,inifocus,exptotal
		ini2var  : pagesound,soundend/turnover,file_extension,enlarge_2step,hidemailbtn,onesmc,cipher,smrepl(gal),soundautoplay,pshowkind
		           spdrag,spbigimage,ptflash,msynchro,spwidget,xmltxt,casesen,imgrender,pchzoom,mconttype,smsizediff */

	String[] postvar = new String[21];
	String[] postvar2 = new String[21];
	String[] spa = new String[10];
	String[] spb = new String[10];

	String act_string = snc_catalog("saved");
	String act = request.getParameter("act");
	if(act == null) act = "";

	if(act.equals("basic")){			// from pcata_basic
		String cate = request.getParameter("cate");
		if(cate == null) cate = "";

		if(check_numalphaVar(cate, 10) == false){
			act_string = snc_catalog("notform");
			echo_script(act_string+"(cate)", "pcata_basic.jsp","","");
			return;
		}

		String pre_fservice = request.getParameter("pre_fservice");
		if(pre_fservice == null) pre_fservice = "";

		String fservice = request.getParameter("fservice");
		if(fservice == null) fservice = "A";

		String unifyCond = request.getParameter("unifycond");
		if(unifyCond == null) unifyCond = "";

		if(!fservice.equals(pre_fservice)){
			StringBuffer cont = new StringBuffer();
			f = new File(pathOfCata + "/catakind.txt");
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;
	
				String tcate = source.substring(0,10);
				if(tcate.equals(cate)){
					String[] aaa = mysplit(source, "|");				// cate|setting|name|etc
					cont.append(aaa[0]).append("|").append(fservice).append(aaa[1].substring(1,5)).append("|").append(aaa[2]).append("|").append(aaa[3]).append("\n");
				}
				else cont.append(source).append("\n");
			}
			fin.close();
			bin.close();
	
			FileOutputStream fos = new FileOutputStream(pathOfCata + "/catakind.txt");
			OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
			osw.write(cont.toString());
			osw.close();
		}

		String ini1var = "";
		String ini2var = "";
		String askin = "";
		String bskin = "";

		String slidetime = "";
		String bigslidetime = "";
		String searchurl = "";
		String downfile = "";
		String downtext = "";
		String margin = "";
		String mmargin = "";
		String midwidth = "";
		String logolink = "";
		String logopos = "";
		String idxwidth = "";
		String idxheight = "";
		String reimage = "";
		String retext = "";
		String catpasswd = "";
		String etcopt = "";
	
		f = new File(pathOfDir + "/ecatalog.txt");
		if(f.exists()){
			int i = 0;
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;
	
				int in = source.indexOf(":");
				String skey = source.substring(0, in);
				String sval = source.substring(in+1);
	
				if(skey.equals("ini1var")) ini1var = sval;
				else if(skey.equals("ini2var")) ini2var = sval;
				else if(skey.equals("askin")) askin = sval;
				else if(skey.equals("bskin")) bskin = sval;
				else if(skey.equals("slidetime")) slidetime = sval;
				else if(skey.equals("bigslidetime")) bigslidetime = sval;
				else if(skey.equals("searchurl")) searchurl = sval;
				else if(skey.equals("downfile")) downfile = sval;
				else if(skey.equals("downtext")) downtext = sval;
				else if(skey.equals("margin")) margin = sval;
				else if(skey.equals("mmargin")) mmargin = sval;
				else if(skey.equals("midwidth")) midwidth = sval;
				else if(skey.equals("logolink")) logolink = sval;
				else if(skey.equals("logopos")) logopos = sval;
				else if(skey.equals("idxwidth")) idxwidth = sval;
				else if(skey.equals("idxheight")) idxheight = sval;
				else if(skey.equals("reimage")) reimage = sval;
				else if(skey.equals("retext")) retext = sval;
				else if(skey.equals("catpasswd")) catpasswd = sval;
				else if(skey.equals("etcopt")) etcopt = sval;
			}
			fin.close();
			bin.close();
		}

		postvar[12] = request.getParameter("lastpage");
		postvar[14] = request.getParameter("anieffect");
		postvar2[2] = request.getParameter("fileext");
		postvar2[5] = request.getParameter("onesmc");
		postvar2[7] = request.getParameter("smrepl");

		if(ini1var.equals("")) ini1var = get_ini1var(ini1var, "FFFFF"+"FFFFF"+"FFPFP"+"FFFFF"+"F", postvar);
		String ini1varG = get_ini1var(ini1var, "FFFFF"+"FFFFF"+"FFPFP"+"FFFFF"+"F", postvar);		// lastpage, startani

		if(ini2var.equals("")) ini2var = get_ini2var(ini2var, "FFPFF"+"PPPFF"+"FFFFF"+"FFFFF"+"F", postvar2);
		String ini2varG = get_ini2var(ini2var, "FFPFF"+"PPPFF"+"FFFFF"+"FFFFF"+"F", postvar2);					// file_extension, onesmc, cipher, smrepl(gal)

		if(askin.equals("")) askin = request.getParameter("def_askin");
		if(bskin.equals("")) bskin = request.getParameter("def_bskin");

		String twitter = request.getParameter("twitter");
		if(twitter == null) twitter = "";
		String facebook = request.getParameter("facebook");
		if(facebook == null) facebook = "";
		String kakao = request.getParameter("kakao");
		if(kakao == null) kakao = "";

		// gallery에서 없을 수 있음.
		String frpage = request.getParameter("frpage");
		if(frpage == null) frpage = "";
		String bgwidth = request.getParameter("bgwidth");
		if(bgwidth == null) bgwidth = "";
		String bgheight = request.getParameter("bgheight");
		if(bgheight == null) bgheight = "";
		String startdate = request.getParameter("startdate");
		if(startdate == null) startdate = "";
		String enddate = request.getParameter("enddate");
		if(enddate == null) enddate = "";
		String outpage = request.getParameter("outpage");
		if(outpage == null) outpage = "";
		String prepage = request.getParameter("prepage");
		if(prepage == null) prepage = "";

		StringBuffer cont = new StringBuffer();
		cont.append("ini1var:").append(ini1varG).append("\n");
		cont.append("ini2var:").append(ini2varG).append("\n");
		cont.append("frpage:").append(frpage).append("\n");
		cont.append("pageno:").append(request.getParameter("pageno")).append("\n");
		cont.append("showstart:").append(request.getParameter("showstart")).append("\n");
		cont.append("smwidth:").append(request.getParameter("smwidth")).append("\n");
		cont.append("smheight:").append(request.getParameter("smheight")).append("\n");
		cont.append("bgwidth:").append(bgwidth).append("\n");
		cont.append("bgheight:").append(bgheight).append("\n");
		cont.append("midwidth:").append(midwidth).append("\n");
		cont.append("idxwidth:").append(idxwidth).append("\n");
		cont.append("idxheight:").append(idxheight).append("\n");
		cont.append("reimage:").append(reimage).append("\n");
		cont.append("retext:").append(retext).append("\n");
		cont.append("askin:").append(askin).append("\n");
		cont.append("bskin:").append(bskin).append("\n");
		cont.append("margin:").append(margin).append("\n");
		cont.append("mmargin:").append(margin).append("\n");
		cont.append("bground:").append(request.getParameter("bgchoice")).append("-").append(request.getParameter("bground")).append("-").append(request.getParameter("blankcolor")).append("\n");
		cont.append("titlebar:").append(request.getParameter("titlebar")).append("\n");
		cont.append("shopcolor:").append(request.getParameter("shopcolor")).append("\n");
		cont.append("shopstr:").append(request.getParameter("shopstr")).append("\n");
		cont.append("logolink:").append(logolink).append("\n");
		cont.append("logopos:").append(logopos).append("\n");
		cont.append("slidetime:").append(slidetime).append("\n");
		cont.append("bigslidetime:").append(bigslidetime).append("\n");
		cont.append("searchurl:").append(searchurl).append("\n");
		cont.append("downfile:").append(downfile).append("\n");
		cont.append("downtext:").append(downtext).append("\n");
		cont.append("catpasswd:").append(catpasswd).append("\n");
		cont.append("etcopt:").append(etcopt).append("\n");
		cont.append("twitter:").append(twitter).append("\n");
		cont.append("facebook:").append(facebook).append("\n");
		cont.append("kakao:").append(kakao).append("\n");
		cont.append("startdate:").append(startdate).append("\n");
		cont.append("enddate:").append(enddate).append("\n");
		cont.append("outpage:").append(outpage).append("\n");
		cont.append("prepage:").append(prepage).append("\n");

		FileOutputStream fos = new FileOutputStream(pathOfDir + "/ecatalog.txt");
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.close();

		if(unifyCond.equals("U")){
			int cateStep = get_cateStep(cate);
			int plen = 2 * cateStep;
			String pnCode = cate.substring(0,plen-2);			// 부모의 앞쪽코드
			String pCode = cate.substring(0,plen-2) + "0000000000".substring(0,10-pnCode.length());

			UnifyFunc unifyInfo = new UnifyFunc(pathOfCata, streamCharset);
			String thiscont = unifyInfo.get_unifContLine(pCode);

			// unif 내용만 업데이트 해야하기 때문에 "cate\tunif"를 인자로 넘김.
			String cont1 = unifyInfo.get_unifyCont(pathOfCata+"/category.txt", pCode+"\tunif");				// "cate\tunif"를 제외한 내용을 $path에서 가져옴.

			fos = new FileOutputStream(pathOfDir + "/category.txt");
			osw = new OutputStreamWriter(fos, streamCharset);
			osw.write(cont1);
			osw.write(thiscont);
			osw.close();
		}
		out.println(echo_script(act_string, "pcata_basic.jsp", "",""));
	}
	else if(act.equals("skin")){
		String ini1var = "";
		String ini2var = "";

		StringBuffer cont = new StringBuffer();
		f = new File(pathOfDir + "/ecatalog.txt");
		if(f.exists()){
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;
	
				int in = source.indexOf(":");
				String skey = source.substring(0, in);
				String sval = source.substring(in+1);
	
				if(skey.equals("askin") || skey.equals("bskin")) continue;

				if(skey.equals("ini1var")) ini1var = sval;
				else if(skey.equals("ini2var")) ini2var = sval;
				else cont.append(source).append("\n");
			}
			fin.close();
			bin.close();
		}

		spa[0] = (request.getParameter("mainmenu") == null) ? "" : request.getParameter("mainmenu");
		spa[2] = request.getParameter("spacerbutton");
		spa[3] = request.getParameter("bookshadow");
		spa[4] = request.getParameter("onsmimage");
		spa[6] = request.getParameter("onbigimage");
		spa[7] = request.getParameter("mskin");

		spb[0] = request.getParameter("combo");
		spb[4] = request.getParameter("help");
		spb[6] = request.getParameter("print");

		postvar[0] = request.getParameter("altmouse");
		postvar[18] = request.getParameter("rightmenu");
		postvar2[4] = request.getParameter("hidemailbtn");

		String raskin = get_askinvar(request.getParameter("askin"), "PFPPPFPPFF", spa);		// mainmenu, spacerbutton, bookshadow, onsmimage, onbigimage, mskin
		String rbskin = get_bskinvar(request.getParameter("bskin"), "PFFFPFPFFF", spb);		// combo, help, print

		if(ini1var.equals("")) ini1var = get_ini1var(ini1var, "PFFFF"+"FFFFF"+"FFFFF"+"FFFPF"+"F", postvar);
		String ini1varG = get_ini1var(ini1var, "PFFFF"+"FFFFF"+"FFFFF"+"FFFPF"+"F", postvar);		// altmouse,contextmenu

		if(ini2var.equals("")) ini2var = get_ini2var(ini2var, "FFFFP"+"FFFFF"+"FFFFP"+"FFFFF"+"F", postvar2);
		String ini2varG = get_ini2var(ini2var, "FFFFP"+"FFFFF"+"FFFFF"+"FFFFF"+"F", postvar2);					// hidemailbtn

		FileOutputStream fos = new FileOutputStream(pathOfDir + "/ecatalog.txt");
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.write("askin:" + raskin + "\n" + "bskin:" + rbskin + "\nini1var:" + ini1varG + "\nini2var:" + ini2varG + "\n");
		osw.close();

		out.println(echo_script(act_string, "design_skin.jsp", "",""));
	}
	else if(act.equals("effect")){
		String ini1var = "";
		String ini2var = "";

		StringBuffer cont = new StringBuffer();
		f = new File(pathOfDir + "/ecatalog.txt");
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			if(in == -1) continue;
	
			String skey = source.substring(0, in);
			String sval = source.substring(in+1);
			if(sval == null) sval = "";

			if(skey.equals("ini1var")) ini1var = sval;
			else if(skey.equals("ini2var")) ini2var = sval;
			else if(!skey.equals("slidetime") && !skey.equals("bigslidetime") && !skey.equals("askin") && !skey.equals("margin") && !skey.equals("mmargin") && !skey.equals("midwidth")){
				cont.append(source).append("\n");
			}
		}
		fin.close();
		bin.close();

		postvar[1] = request.getParameter("inimusic");
		postvar[4] = request.getParameter("overeffect");
		postvar[5] = request.getParameter("synlarge");
		postvar[6] = request.getParameter("oversnd");
		postvar[10] = request.getParameter("alertsnd");
		postvar[13] = request.getParameter("largeslide");
		postvar[16] = request.getParameter("msenlarge");
		postvar[17] = request.getParameter("synchro");

		postvar2[3] = request.getParameter("en2step");
		postvar2[10] = request.getParameter("spdrag");
		postvar2[11] = request.getParameter("spbigimage");
		postvar2[12] = "";	//request.getParameter("ptflash");
		postvar2[13] = request.getParameter("msynchro");
		postvar2[14] = request.getParameter("spwidget");

		spa[5] = request.getParameter("moving");
		spa[9] = request.getParameter("movingm");

		String ini1varG = get_ini1var(ini1var, "FPFFP"+"PPFFF"+"PFFPF"+"FPPFF"+"F", postvar);			//inimusic,overeffect,synlarge,oversnd,alertsnd,largeslide,msenlarge,synchro
		String ini2varG = get_ini2var(ini2var, "FFFPF"+"FFFFF"+"PPPPP"+"FFFFF"+"F", postvar2);				//enlarge_2step,spdrag,spbigimage,ptflash,msynchro,spwidget
		String raskin = get_askinvar(request.getParameter("askin"), "FFFFFPFFFP", spa);					//moving, movingm

		cont.append("ini1var:").append(ini1varG).append("\n");
		cont.append("ini2var:").append(ini2varG).append("\n");
		cont.append("askin:").append(raskin).append("\n");
		cont.append("midwidth:").append(request.getParameter("midwidth")).append("\n");
		cont.append("slidetime:").append(request.getParameter("slidetime")).append("\n");
		cont.append("bigslidetime:").append(request.getParameter("bigslidetime")).append("\n");
		cont.append("margin:").append(request.getParameter("margin")).append("\n");
		cont.append("mmargin:").append(request.getParameter("mmargin")).append("\n");

		FileOutputStream fos = new FileOutputStream(pathOfDir + "/ecatalog.txt");
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.close();

		out.println(echo_script(act_string, "design_effect.jsp", "",""));
	}
	else if(act.equals("expert")){
		String catpasswd = request.getParameter("catpasswd");
		if(catpasswd == null) catpasswd = "";
		String etcopt = request.getParameter("etcopt");
		if(etcopt == null) etcopt = "";

		String ini1var = "";
		String ini2var = "";

		StringBuffer cont = new StringBuffer();
		f = new File(pathOfDir + "/ecatalog.txt");
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			if(in == -1) continue;
	
			String skey = source.substring(0, in);
			String sval = source.substring(in+1);
			if(sval == null) sval = "";

			if(skey.equals("ini1var")) ini1var = sval;
			else if(skey.equals("ini2var")) ini2var = sval;
			else if(!skey.equals("catpasswd") && !skey.equals("etcopt")) cont.append(source).append("\n");
		}
		fin.close();
		bin.close();

		postvar[8] = request.getParameter("lowerhide");
		postvar[15] = request.getParameter("menuhide");
		postvar[19] = request.getParameter("inifocus");
		postvar[20] = request.getParameter("exptotal");

		postvar2[6] = request.getParameter("cipher");
		postvar2[15] = request.getParameter("xmltxt");
		postvar2[16] = request.getParameter("casesen");
		postvar2[17] = request.getParameter("imgrender");
		postvar2[18] = request.getParameter("pchzoom");
		postvar2[20] = request.getParameter("smsizediff");

		String ini1varG = get_ini1var(ini1var, "FFFFF"+"FFFPF"+"FFFFF"+"PFFFP"+"P", postvar);			//lowerhide,menuhide,inifocus,exptotal
		String ini2varG = get_ini2var(ini2var, "FFFFF"+"FPFFF"+"FFFFF"+"PPPPF"+"P", postvar2);			//cipher, xmltxt, casesen, imgrender, pchzoom, smsizediff

		cont.append("ini1var:").append(ini1varG).append("\n");
		cont.append("ini2var:").append(ini2varG).append("\n");
		cont.append("catpasswd:").append(catpasswd).append("\n");
		cont.append("etcopt:").append(etcopt).append("\n");

		FileOutputStream fos = new FileOutputStream(pathOfDir + "/ecatalog.txt");
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.close();

		out.println(echo_script(act_string, "design_expert.jsp", "", ""));
	}
	else if(act.equals("contents")){
		postvar[2] = request.getParameter("conthide");
		postvar[3] = request.getParameter("conttype");
		postvar2[19] = request.getParameter("mconttype");

		FileOutputStream fos = new FileOutputStream(pathOfDir + "/mokcha.txt");
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(request.getParameter("contents"));
		osw.close();

		StringBuffer cont = new StringBuffer();
		f = new File(pathOfDir + "/ecatalog.txt");
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);
			//String sval = source.substring(in+1);

			if(!skey.equals("ini1var") && !skey.equals("ini2var") && !skey.equals("idxwidth") && !skey.equals("idxheight")){
				cont.append(source).append("\n");
			}
		}
		fin.close();
		bin.close();

		String ini1varG = get_ini1var(request.getParameter("ini1var"), "FFPPF"+"FFFFF"+"FFFFF"+"FFFFF"+"F", postvar);		// conthide, conttype
		String ini2varG = get_ini2var(request.getParameter("ini2var"), "FFFFF"+"FFFFF"+"FFFFF"+"FFFFP"+"F", postvar2);		// mconttype

		String idxwidth = request.getParameter("idxwidth");
		String idxheight = request.getParameter("idxheight");

		fos = new FileOutputStream(pathOfDir + "/ecatalog.txt");
		osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.write("ini1var:" + ini1varG + "\nini2var:" + ini2varG + "\nidxwidth:" + idxwidth + "\nidxheight:" + idxheight + "\n");
		osw.close();

		out.println(echo_script(act_string, "contents.jsp", "",""));
	}
	else if(act.equals("search")){
		String unifyCond = request.getParameter("unifycond");

		String searchurl = request.getParameter("searchurl");
		if(searchurl == null) searchurl = "";
		String searchcont = strTrimEach(request.getParameter("searchcont"));

		FileOutputStream fos = new FileOutputStream(pathOfDir + "/search.txt");
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(searchcont);
		osw.close();

		postvar[9] = request.getParameter("searchmethod");

		StringBuffer cont = new StringBuffer();
		f = new File(pathOfDir + "/ecatalog.txt");
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);
			String sval = source.substring(in+1);

			if(!skey.equals("ini1var") && !skey.equals("searchurl")){
				cont.append(source).append("\n");
			}
		}
		fin.close();
		bin.close();

		String ini1varG = get_ini1var(request.getParameter("ini1var"), "FFFFF"+"FFFFP"+"FFFFF"+"FFFFF"+"F", postvar);		// searchmethod

		fos = new FileOutputStream(pathOfDir + "/ecatalog.txt");
		osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.write("ini1var:" + ini1varG + "\n");
		osw.write("searchurl:" + searchurl + "\n");
		osw.close();

		if(unifyCond.equals("U")){
			String cate = request.getParameter("cate");
			int cateStep = get_cateStep(cate);
			int plen = 2 * cateStep;
			String pnCode = cate.substring(0,plen-2);			// 부모의 앞쪽코드
			String pCode = cate.substring(0,plen-2) + "0000000000".substring(0,10-pnCode.length());

			UnifyFunc unifyInfo = new UnifyFunc(pathOfCata, streamCharset);
			String thiscont = unifyInfo.get_unifContLine(pCode);

			// unif 내용만 업데이트 해야하기 때문에 "cate\tunif"를 인자로 넘김.
			String cont1 = unifyInfo.get_unifyCont(pathOfCata+"/category.txt", pCode+"\tunif");				// "cate\tunif"를 제외한 내용을 $path에서 가져옴.

			fos = new FileOutputStream(pathOfDir + "/category.txt");
			osw = new OutputStreamWriter(fos, streamCharset);
			osw.write(cont1);
			osw.write(thiscont);
			osw.close();
		}

		out.println(echo_script(act_string, "search.jsp", "",""));
	}
	else if(act.equals("fileseq")){
		String cate = request.getParameter("cate");
		String unifyCond = request.getParameter("unifycond");

		postvar[11] = request.getParameter("fileseq");
		postvar2[9] = request.getParameter("pshowkind");

		String ini1varG = get_ini1var(request.getParameter("ini1var"), "FFFFF"+"FFFFF"+"FPFFF"+"FFFFF"+"F", postvar);		// fileseq
		String ini2varG = get_ini2var(request.getParameter("ini2var"), "FFFFF"+"FFFFP"+"FFFFF"+"FFFFF"+"F", postvar2);			// pshowkind

		StringBuffer cont = new StringBuffer();
		f = new File(pathOfDir + "/ecatalog.txt");
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);

			if(skey.equals("ini1var")){
				cont.append("ini1var:").append(ini1varG).append("\n");
			}
			else if(skey.equals("ini2var")){
				cont.append("ini2var:").append(ini2varG).append("\n");
			}
			else{
				cont.append(source).append("\n");
			}
		}
		fin.close();
		bin.close();

		FileOutputStream fos = new FileOutputStream(pathOfDir + "/ecatalog.txt");
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.close();

		if(unifyCond.equals("U")){
			int cateStep = get_cateStep(cate);
			int plen = 2 * cateStep;
			String pnCode = cate.substring(0,plen-2);			// 부모의 앞쪽코드
			String pCode = cate.substring(0,plen-2) + "0000000000".substring(0,10-pnCode.length());

			UnifyFunc unifyInfo = new UnifyFunc(pathOfCata, streamCharset);
			String thiscont = unifyInfo.get_unifContLine(pCode);

			// unif 내용만 업데이트 해야하기 때문에 "cate\tunif"를 인자로 넘김.
			String cont1 = unifyInfo.get_unifyCont(pathOfCata+"/category.txt", pCode+"\tunif");				// "cate\tunif"를 제외한 내용을 $path에서 가져옴.

			fos = new FileOutputStream(pathOfDir + "/category.txt");
			osw = new OutputStreamWriter(fos, streamCharset);
			osw.write(cont1);
			osw.write(thiscont);
			osw.close();
		}

		out.println(echo_script(act_string, "pcata_cont.jsp", "",""));
	}
	else if(act.equals("downfile")){
		String downfile = request.getParameter("downfile");
		String downtext = request.getParameter("downtext");
		String reimage = request.getParameter("reimage");
		String retext = request.getParameter("retext");

		StringBuffer cont = new StringBuffer();
		f = new File(pathOfDir + "/ecatalog.txt");
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);
			String sval = source.substring(in+1);

			if(!skey.equals("downfile") && !skey.equals("downtext") && !skey.equals("reimage") && !skey.equals("retext")){
				cont.append(source).append("\n");
			}
		}
		fin.close();
		bin.close();

		FileOutputStream fos = new FileOutputStream(pathOfDir + "/ecatalog.txt");
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.write("downfile:" + downfile + "\n" + "downtext:" + downtext + "\n");
		osw.write("reimage:" + reimage + "\n" + "retext:" + retext + "\n");
		osw.close();

		out.println(echo_script(act_string, "file_down.jsp", "",""));
	}
	else if(act.equals("pagesound")){
		postvar2[0] = request.getParameter("applyps");
		postvar2[1] = request.getParameter("soundturn");
		postvar2[8] = request.getParameter("soundautoplay");
		String ini2varG = get_ini2var(request.getParameter("ini2var"), "PPFFF"+"FFFPF"+"FFFFF"+"FFFFF"+"F", postvar2);			//apply pagesound, sound end turn-over,soundautoplay

		StringBuffer cont = new StringBuffer();
		f = new File(pathOfDir + "/ecatalog.txt");
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);
			//String sval = source.substring(in+1);

			if(!skey.equals("ini2var")){
				cont.append(source).append("\n");
			}
		}
		fin.close();
		bin.close();

		FileOutputStream fos = new FileOutputStream(pathOfDir + "/ecatalog.txt");
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.write("ini2var:" + ini2varG + "\n");
		osw.close();

		out.println(echo_script(act_string, "link_sound.jsp", "",""));
	}
	else if(act.equals("logopos")){
		String logopos = request.getParameter("leftpos") + "x" + request.getParameter("toppos");
		String logolink = request.getParameter("logolink");

		StringBuffer cont = new StringBuffer();
		f = new File(pathOfDir + "/ecatalog.txt");
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);

			if(!skey.equals("logopos") && !skey.equals("logolink")){
				cont.append(source).append("\n");
			}
		}
		fin.close();
		bin.close();

		FileOutputStream fos = new FileOutputStream(pathOfDir + "/ecatalog.txt");
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString() + "logopos:" + logopos + "\n" + "logolink:" + logolink + "\n");
		osw.close();

		out.println(echo_script(snc_catalog("saved"), "logo.jsp", "",""));
	}
%>
