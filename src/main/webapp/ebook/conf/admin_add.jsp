<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	String[] subar = {"admin", "추가/변경", "name", ""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 관리자 추가/변경</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	section table tr *:nth-of-type(3n+1){text-align:left;}
	section table tr *:nth-of-type(3n+2){text-align:left;}
	section table tr *:nth-of-type(3n+3){text-align:left;}
	section table td div:first-child{display:block;float:left;width:30%;}
	section table td div:nth-child(2){display:block;float:left;width:50%;}
	section table td div:nth-child(3){display:block;float:left;width:20%;}
	img{vertical-align:middle;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function checkup(dm, act){
	if(trimVal(dm.mid.value) == ""){
		alert("아이디를 입력하셔야 합니다.");
		dm.mid.focus();
		return false;
	}
	if(trimVal(dm.passwd1.value) == ""){
		alert("비밀번호를 입력하셔야 합니다.");
		dm.passwd1.focus();
		return false;
	}
	if(trimVal(dm.passwd2.value) == ""){
		alert("비밀번호확인을 입력하셔야 합니다.");
		dm.passwd2.focus();
		return false;
	}

	if(dm.passwd1.value != dm.passwd2.value){
		alert("비밀번호 1, 2의 입력내용이 서로 틀립니다. 확인해주세요.");
		dm.passwd1.focus();
		return false;
	}
	dm.act.value = act;
}

function checkdel(dm, act){
	if(confirm("삭제하시겠습니까?")){
		dm.act.value = "admindel";
		dm.submit();
	}
	else return;
}
function adminnext(act, mid){
	newWindow(2, 'nw_adminmod.jsp?act=' + act + '&mid=' + mid, 500, 300, 50, 50, 'adm');
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>
<%
	String jspTags = "%" + ">";
	String[] loginid;
	String[] loginpass;
	int i = 0;
	int j = 0;

	f = new File(passSysDir);
	FileInputStream fin = new FileInputStream(f);
	BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
	while((source = bin.readLine()) != null){
		source = strTrimEach(source);
		if(source.equals("")) continue;

		if(source.equals(jspTags)) j = 1;
		else if(j == 1) i++;
	}
	fin.close();
	bin.close();

	loginid = new String[i];
	loginpass = new String[i];

	i = 0;
	j = 0;
	fin = new FileInputStream(f);
	bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
	while((source = bin.readLine()) != null){
		source = strTrimEach(source);
		if(source.equals("")) continue;

		if(source.equals(jspTags)) j = 1;
		else if(j == 1){
			int in = source.indexOf(":");
			if(in >= 0){
				loginid[i] = source.substring(0, in);
				loginpass[i] = source.substring(in+1);
				i++;
			}
		}
	}
	fin.close();
	bin.close();

	String akind = "general";
	if(loginid[0].equals(admin_id))	akind = "super";

	/* hashing category name */
	Hashtable ahash = new Hashtable();

	f = new File(pathOfOriCata + "/catimage.txt");
	if(f.exists()){
		fin = new FileInputStream(f);
		bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, "\t");				// folder, dist, name, date
			ahash.put("adminCate"+aaa[0], aaa[2]);
		}
		fin.close();
		bin.close();
	}
%>
<main id="maincnt" class="maincnt">

<form name="modform" action="admin_act.jsp" method="post" onsubmit="return checkup(document.modform, 'idpass');">
<section>
<h3>현재 접속정보 변경</h3>
<p><label>아이디<br>
<input type="text" name="mid" size="15" class="px" value="<%=admin_id%>" <% if(akind.equals("general")) out.print("readonly"); %>></label></p>
<p><label>비밀번호(1)<br>
<input type="password" name="passwd1" size="15" class="px"></label></p>
<p><label>비밀번호확인(2)<br>
<input type="password" name="passwd2" size="15" class="px"></label> <input type="submit" value="아이디/비밀번호 변경"></p>
<input type="hidden" name="preid" value="<%=admin_id%>">
<input type="hidden" name="act" value="idpass">
</section>
</form>

<% if(akind.equals("super")){ %>
<section>
<h3>추가된 관리자</h3>
<% if(loginid.length == 1){ %>
<p>추가된 관리자가 없습니다.</p>
<% } else{ %>
<table>
	<colgroup>
		<col style="width:30%;"><col style="width:50%;"><col style="width:20%;">
	</colgroup>
	<thead>
	<tr>
		<th>아이디</th>
		<th>관리하는 분류</th>
		<th></th>
	</tr>
	</thead>
	<%
	String aclass = "";
	for(i = 1;i < loginid.length;i++){
		String bgcolor = "#f7f7f7";
		if(i%2 == 1) bgcolor = "#ffffff";

		String passncate = loginpass[i];
		int in = passncate.lastIndexOf(":");

		String logincate = passncate.substring(in+1);
		String distname = ahash.containsKey("adminCate"+logincate) ? (String)ahash.get("adminCate"+logincate) : "";
	%>
	<tr><td colspan="3">
		<form name="mems<%=i%>" method="post" action="admin_act.jsp" onsubmit="return checkup(document.mems<%=i%>,'adminmod')">
		<div><input type="text" name="mid" size="12" class="px" value="<%=loginid[i]%>"></div>
		<div><%=distname%></div>
		<div>
			<input type="button" value=" 변경 " onclick="adminnext('mod','<%=loginid[i]%>')">
			<input type="button" value=" 삭제 " onclick="checkdel(document.mems<%=i%>,'admindel')">
		</div>
		<input type="hidden" name="preid" value="<%=loginid[i]%>">
		<input type="hidden" name="act" value="adminmod">
		</form>
	</td></tr>
	<% } %>
</table>
<% } %>
<p><input type="button" value=" 관리자 추가 " onclick="adminnext('add','')"></p>
</section>

<% } %>
</main>

<%@ include file="inc_copy.jsp" %>
