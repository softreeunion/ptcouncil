<%@ page import="java.io.*, java.util.regex.*, java.util.regex.*" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	String[] subar = {"intro", "","basic", ""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 기본환경</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
</head>

<body onload="onload_func();">

<%@ include file="inc_menu.jsp" %>

<main id="maincnt" class="maincnt">
	<p class="intropara">eCatalog 관리자 모드 접속을 환영합니다.<br>좌측의 탐색기에서 분류나 폴더를 선택하세요.</p>
</main>

<%@ include file="inc_copy.jsp" %>
