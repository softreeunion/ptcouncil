<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="admin.jsp" %>
<%
	String[] subar = {"file", "링크파일 보기", "", "flash"};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 링크파일 보기</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	body{margin:0px;overflow:hidden;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script>
function onload_func(){
	show_currSubmenu('file_flashlist.jsp');
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>
<%
	String fname = request.getParameter("fname");
	String exten = get_extension(fname).toLowerCase();

	String[] aaa = new String[2];
	StringTokenizer st = new StringTokenizer(request.getParameter("fsize"), "x");
	aaa[0] = st.nextToken();
	aaa[1] = st.nextToken();

	String vars = "";
	String webpath = webServer + "/flash/" + fname;
%>
<main class="maincnt">
<p class="table-title">링크 파일 보기</p>
<br>

<% if(exten.equals("mp4") || exten.equals("avi")){ %>
<video id="linkvideo" autoplay="autoplay" controls="controls" onerror="failed(event)">
	<source src="<%=webpath%>" type="video/<%=exten%>">
	Your browser does not support the video element.
</video>

<% } else{
	if(exten.equals("flv")){
		vars = "charset="+charset+"&lolang="+lolang+"&ext=jsp&url="+webServer+"&fname="+fname;
		webpath = confServer + "flvview.swf";
	}
%>
<object id="swfview" type="application/x-shockwave-flash" data="<%=webpath%>" standby="swf파일보기" width="<%=aaa[0]%>" height="<%=aaa[1]%>">
<param name="movie" value="<%=webpath%>">
<param name="flashVars" value="<%=vars%>">
<param name="bgcolor" value="#FFFFFF">
<param name="allowScriptAccess" value="always">
<param name="quality" value="high">
</object>
<% } %>
</main>

</body>
</html>
