<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="admin.jsp" %>
<%
	if(cook_dir.equals("")){
		out.println(echo_script(snc_catalog("selfolder"), "basic.jsp", "",""));
		return;
	}

	String ini1var = "";
	String ini2var = "";
	String pageno = "";
	int smwidth = 0;
	int smheight = 0;

	f = new File(pathOfDir + "/ecatalog.txt");
	if(f.exists()){
		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);

			if(skey.equals("ini1var")) ini1var = source.substring(in+1);
			else if(skey.equals("ini2var")) ini2var = source.substring(in+1);
			else if(skey.equals("pageno")) pageno = source.substring(in+1);
			else if(skey.equals("smwidth")) smwidth = Integer.parseInt(source.substring(in+1));
			else if(skey.equals("smheight")) smheight = Integer.parseInt(source.substring(in+1));
		}
		fin.close();
		bin.close();
	}

	if(ini1var.equals("")){
		out.println(echo_script(snc_catalog("basicconf"), "basic.jsp", "",""));
		return;
	}

	char fileExt = ini2var.charAt(2);
	String fileExtension = ".jpg";
	if(fileExt == 'P') fileExtension = ".png";

	char ini1FS = ini1var.charAt(11);
	char ini2PageSound = ini2var.charAt(0);
	int pageNumber = Integer.parseInt(pageno);

	Hashtable ahash = new Hashtable();
	f = new File(pathOfDir + "/pagesound.txt");
	if(f.exists()){
		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = get_namepart(source.substring(0, in));
			String sval = source.substring(in+1);

			ahash.put(skey, sval);
		}
		fin.close();
		bin.close();
	}

	int thumbWidth = 0;
	int thumbHeight = 100;
	if(smheight > 0){
		thumbWidth = (int)(thumbHeight*smwidth/smheight);

		if(thumbWidth > thumbHeight){
			thumbWidth = 100;
			thumbHeight = (int)(thumbWidth*smheight/smwidth);
		}
	}

	String webpath = webServer + "/catImage" + cook_catimage + "/" + cook_dir;
	String[] subar = {"link", "페이지별 소리", "name",""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 페이지 소리</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	div.mleft table{width:100%;border-spacing:0px;margin:0;}
	div.mleft table th{background-color:#EFF9FC;color:#375F6A;font-weight:bold;height:20px;
		border-top:0px solid #B6D1D9;border-bottom:1px solid #B6D1D9;font-size:11px;font-family:돋움;}
	div.mleft table tr th:nth-of-type(3n+2){text-align:left;}
	div.mleft table tr th:nth-of-type(3n+3){text-align:left;}
	div.mleft table tr td:nth-of-type(3n+1){text-align:center;}
	div.mleft table tr td:nth-of-type(3n+2){text-align:left;}
	div.mleft table tr td:nth-of-type(3n+3){text-align:left;}
	div.mleft table td, div.mleft table td a{font-size:8pt;font-family:Tahoma;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
var soundDragObj;
function checkup(){
	if(!confirm("설정값을 저장하시겠습니까?")) return;

	var acont = "";
	var len = document.soundform.elements.length;
	for(var i = 0;i < len;i++){
		if(document.soundform.elements[i].name == "filesel"){
			acont += document.soundform.elements[i].value + ":" + document.soundform.elements[i-1].value + "\r\n";
		}
	}

	document.addform.cont.value = acont;
	document.addform.submit();
}

function show_thumb(e, imgsrc){
	if(!e) e = event;
	document.getElementById("divmessage").innerHTML = "<img src=\"<%=webpath%>/"+imgsrc+"\" width=\"<%=thumbWidth%>\" height=\"<%=thumbHeight%>\">";
	document.getElementById("divmessage").style.visibility = "visible";
	document.getElementById("divmessage").style.left = e.clientX + 10 + document.documentElement.scrollLeft + "px";
	document.getElementById("divmessage").style.top = e.clientY + 15 + document.documentElement.scrollTop + "px";
}
function notDivShow(){
	document.getElementById("divmessage").style.visibility = "hidden";
}
function play_audio(fname){
	document.getElementById('audioctrl').style.display = "block";
	var audioPlayer = document.getElementById('audioplayer');
	audioPlayer.setAttribute('src', fname);
	audioPlayer.play();
}
function hide_audioctrl(){
	document.getElementById('audioctrl').style.display = "none";
}
function drag(e){
	soundDragObj = e.target.parentNode.getAttribute("data-fname");
}
function allowDrop(e){
	e.preventDefault();
}
function drop(e, obj){
	e.preventDefault();
	obj.value = soundDragObj;
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>

<main id="maincnt" class="maincnt">

<form name="conform" action="action.jsp" method="post">
<section>
<h3>페이지 소리</h3>
<fieldset>
<legend class="title">적용</legend>
<p class="desc">
	<label><input type="radio" name="applyps" value="N" id="id_applyps1" <% if(ini2PageSound == 'N') out.print("checked"); %>>적용하지 않음</label>&nbsp;
	<label><input type="radio" name="applyps" value="Y" id="id_applyps2" <% if(ini2PageSound == 'Y') out.print("checked"); %>>페이지별 소리</label>&nbsp;
	<label><input type="radio" name="applyps" value="P" id="id_applyps3" <% if(ini2PageSound == 'P') out.print("checked"); %>>링크</label>
</p>
</fieldset>
<p class="title">적용 시</p>
<p class="desc">
	<label><input type="checkbox" name="soundturn" value="Y" id="id_soundturn" <% if(ini2var.charAt(1) == 'Y') out.print("checked"); %>>소리 종료시 자동으로 페이지 넘김</label>&nbsp;
	<label><input type="checkbox" name="soundautoplay" value="N" id="id_soundplay" <% if(ini2var.charAt(8) == 'N') out.print("checked"); %>>자동 재생하지 않음</label>
</p>
<input type="hidden" name="ini2var" value="<%=ini2var%>">
<input type="hidden" name="act" value="pagesound">
</section>

<div class="submitbtn2"><input type="submit" value="페이지 소리 저장하기"></div>
</form>

<% if(ini2PageSound == 'Y'){ %>
<form name="soundform" method="post" action="#">
<section>
<h3>지정내용</h3>
<div class="mleft" ondragover="allowDrop(event);">
<table>
	<tr>
		<th>순</th>
		<th>파일</th>
		<th>소리파일</th>
	</tr>
<%
	if(ini1FS == 'F'){
		f = new File(pathOfDir + "/fileseq.txt");
		if(f.exists()){
			int k = 1;
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin,streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				int in = source.indexOf(":");
				String skey = get_namepart(source.substring(0, in));
				String fname = source.substring(in+1);
				String namepart = get_namepart(fname);

				File f1 = new File(pathOfDir + "/" + fname);
				if(f1.exists()){
					String soundvar = ahash.containsKey(namepart) ? (String)ahash.get(namepart) : "";

					out.print("<tr><td>"+k+"</td>\n");
					out.print("<td><a href=\"file_det.jsp?fname="+fname+"\" onMouseOver=\"show_thumb(event,'s"+fname+"')\" onMouseOut=\"notDivShow()\">"+fname+"</a></td>\n");
					out.print("<td><input type=\"text\" name=\"soundfile\" size=\"30\" value=\""+soundvar+"\" class=\"rx\" ondrop=\"drop(event,this)\">");
					out.print("<input type=\"hidden\" name=\"filesel\" value=\""+fname+"\"></td></tr>\n");
					k++;
				}
			}
			fin.close();
			bin.close();
		}
	}
	else{
		for(int i = 0;i < pageNumber;i++){
			int k = i + 1;
			String namepart = digit3Number(k);
			String fname = namepart + ".jpg";

			File f1 = new File(pathOfDir + "/" + fname);
			if(f1.exists()){
				String soundvar = ahash.containsKey(namepart) ? (String)ahash.get(namepart) : "";

				out.print("<tr><td>"+k+"</td>\n");
				out.print("<td><a href=\"file_det.jsp?fname="+fname+"\" onMouseOver=\"show_thumb(event,'s"+fname+"')\" onMouseOut=\"notDivShow()\">"+fname+"</a></td>\n");
				out.print("<td><input type=\"text\" name=\"soundfile\" size=\"30\" value=\""+soundvar+"\" class=\"rx\" ondrop=\"drop(event,this)\">");
				out.print("<input type=\"hidden\" name=\"filesel\" value=\""+fname+"\"></td></tr>\n");
			}
		}
	}
%>
</table>
</div>

<div class="mright">
<ul class="sndlist">
<%
	f = new File(pathOfDir + "/sound");
	if(f.exists()){
		int k = 0;
		File[] folders = f.listFiles();
		for(int i = 0;i < folders.length;i++){
			String temp = folders[i].getName();
			if(!temp.equals(".") && !temp.equals("..") && (temp.endsWith("mp3") || temp.endsWith("MP3"))){
				out.print("<li draggable=\"true\" ondragstart=\"drag(event)\" data-fname=\""+temp+"\">");
				out.print("<a href=\"javascript:play_audio('"+webpath+"/"+temp+"');\">"+temp+"</a></li>\n");
			}
		}
	}
%>
</ul>
</div>
</section>

<div id="audioctrl"><audio id="audioplayer" controls ondblclick="hide_audioctrl();">Your browser does not support the audio element.</audio></div>

<div class="submitbtn2"><input type="button" value="지정내용 저장하기" onclick="checkup();"></div>
</form>

<p class="secthelp2">
	- 원하시는 mp3 파일을 드래그하여 네모상자에 넣습니다.<br>
	- 지정된 페이지의 mp3 파일을 삭제하시려면 파일이름을 삭제하십시오.<br>
	- mp3 파일이름을 클릭하시면 파일이 재생됩니다.
</p>

<div class="nullform">
<form name="addform" method="post" action="action2.jsp">
<input type="hidden" name="cont" value="">
<input type="hidden" name="act" value="sounddata">
</form>
</div>

<% } %>

</main>

<%@ include file="inc_copy.jsp" %>
