<%!
public class UnifyFunc{
	public String pathOfCata;
	public String streamCharset;

    public UnifyFunc(String s, String chars){
		pathOfCata = s;
		streamCharset = chars;
    }

	public String get_unifContLine(String cate){
		String source = "";
		Vector a_dir = new Vector();

		int cateStep = get_cateStep(cate);
		int plen = 2 * cateStep;
		String pnCode = cate.substring(0,plen);		// front code
		int rlen = 10 - plen - 2;
		String r0 = "0000000000".substring(0,rlen);

		File f = new File(pathOfCata+"/catakind.txt");
		if(f.exists()){
			try{
				FileInputStream fin = new FileInputStream(f);
				BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
				while((source = bin.readLine()) != null){
					source = strTrimEach(source);
					if(source.equals("")) continue;

					StringTokenizer st = new StringTokenizer(source, "|");		// cate|setting|name|etc
					String[] aaa = new String[4];
					aaa[0] = (st.hasMoreTokens()) ? st.nextToken() : "";
					aaa[1] = (st.hasMoreTokens()) ? st.nextToken() : "";
					aaa[2] = (st.hasMoreTokens()) ? st.nextToken() : "";
					aaa[3] = (st.hasMoreTokens()) ? st.nextToken() : "";
	
					if(aaa[0].substring(0,plen).equals(pnCode) && !aaa[0].substring(plen,plen+2).equals("00") && aaa[0].substring(plen+2,10).equals(r0)){
						if(aaa[1].substring(0,1).equals("A") && aaa[1].substring(2,3).equals("0")){
							a_dir.addElement(aaa[3]);
						}
					}
				}
				fin.close();
				bin.close();
			}
			catch(Exception e){
            	return "";
    		}
		}

		String adir_str = "";
		String apageno_str = "";
		String afileseq_str = "";
		String alink_str = "";
		String asound_str = "";
		String apic_str = "";
		String asearch_str = "";

		int alen = a_dir.size();
		int tno = 0;
		for(int i = 0;i < alen;i++){
			String t_dir = (String)a_dir.get(i);
			f = new File(pathOfCata + "/" + t_dir + "/ecatalog.txt");
			if(!f.exists()) continue;

			adir_str = str_join(adir_str, t_dir);
			int t_pageno = 0;
			String t_ini1var = "";
			String t_ini2var = "";

			try{
				FileInputStream fin = new FileInputStream(f);
				BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
				while((source = bin.readLine()) != null){
					source = strTrimEach(source);
					if(source.equals("")) continue;
			
					int in = source.indexOf(":");
					if(in == -1) continue;

					String skey = source.substring(0, in);
					String sval = source.substring(in+1);
			
					if(skey.equals("pageno")){
						t_pageno = Integer.parseInt(sval);
					}
					else if(skey.equals("ini1var")){
						t_ini1var = sval;
					}
					else if(skey.equals("ini2var")){
						t_ini2var = sval;
					}
				}
				fin.close();
				bin.close();
			}
			catch(Exception e){
	           	return "";
	       	}

			tno += t_pageno;
			apageno_str = str_join(apageno_str, Integer.toString(t_pageno));

			// fileseq
			String t_fileseq = t_ini1var.substring(11,12);
			if(t_fileseq.equals("F")){
				f = new File(pathOfCata+"/"+t_dir+"/fileseq.txt");
				afileseq_str = f.exists() ? str_join(afileseq_str,"F") : str_join(afileseq_str, "N");
			}
			else if(t_fileseq.equals("X")){
				afileseq_str = str_join(afileseq_str, "X");
			}
			else{
				afileseq_str = str_join(afileseq_str, "N");
			}

			// search, U: url, A: auto, F: file, C:content
			String t_var = t_ini1var.substring(9,10);
			if(t_var.equals("F")){
				f = new File(pathOfCata+"/"+t_dir+"/search.txt");
				asearch_str = f.exists() ? str_join(asearch_str,"F") : str_join(asearch_str, "N");
			}
			else if(t_var.equals("A")){
				f = new File(pathOfCata+"/"+t_dir+"/search.xml");
				asearch_str = f.exists() ? str_join(asearch_str,"A") : str_join(asearch_str, "N");
			}
			else{
				asearch_str = str_join(asearch_str, t_var);			// U/C
			}

			// alink
			f = new File(pathOfCata + "/" + t_dir + "/alink.txt");
			alink_str = f.exists() ? str_join(alink_str,"Y") : str_join(alink_str,"N");

			// pagesound, N:Basis, Y: page sound, P: link
			String t_sound = t_ini2var.substring(0,1);
			f = new File(pathOfCata + "/" + t_dir + "/pagesound.txt");
			t_var = "N";
			if(t_sound.equals("P")) t_var = "P";
			else if(t_sound.equals("Y") && f.exists()) t_var = "Y";
			asound_str = str_join(asound_str, t_var);

			// picdata
			f = new File(pathOfCata + "/" + t_dir + "/picdata.txt");
			t_var = f.exists() ? "Y" : "N";
			apic_str = str_join(apic_str, t_var);
		}

		// category \t unif \t totalpages \t folder \t pageno \t fileseq \t alink \t pagesound
		return cate+"\tunif\t"+tno+"\t"+adir_str+"\t"+apageno_str+"\t"+afileseq_str+"\t"+alink_str+"\t"+asound_str+"\t"+apic_str+"\t"+asearch_str+"\n";
	}

	// return value except for cate
	public String get_unifyCont(String path, String cate){
		File f = new File(path);
		if(!f.exists()) return "";

		String source = "";
		StringBuffer cont = new StringBuffer();
		try{
			int alen = cate.length();
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;
			
				if(!source.substring(0,alen).equals(cate)){
					cont.append(source).append("\n");
				}
			}
			fin.close();
			bin.close();
		}
		catch(Exception e){
           	return "";
       	}
       	return cont.toString();
	}

	private String str_join(String strsum, String s){
		if(strsum.equals("")) strsum = s;
		else strsum = strsum + "|" + s;

		return strsum;
	}
}
%>
