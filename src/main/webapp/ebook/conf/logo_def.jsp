<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="ImageSize.jsp" %>
<%@ include file="admin.jsp" %>
<%
	if(cook_dir.equals("")){
		out.println(echo_script(snc_catalog("selfolder"), "basic.jsp", "",""));
		return;
	}

	String[] subar = {"design", "기본 로고", "", ""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 기본 로고</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function checkup_del(){
	if(confirm("삭제하시겠습니까?")) document.logodelform.submit();
	else return;
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>
<%
	String logoFile = "";
	String logoFileUrl = "";
	String malogoFile = "";
	String malogoFileUrl = "";
	String splogoFile = "";
	String splogoFileUrl = "";
	String splogoFileName = "";

	f = new File(pathOfOriCata);
	File[] folders = f.listFiles();
	for(int i = 0;i < folders.length;i++){
		String temp = folders[i].getName();
		if(temp.equals(".") || temp.equals("..")) continue;

		if(temp.length() > 4 && temp.substring(0,4).equals("logo")){
			logoFile = pathOfOriCata + "/" + temp;
			logoFileUrl = webServer + "/catImage/" + temp;
		}	
		else if(temp.length() > 6 && temp.substring(0,6).equals("malogo")){
			malogoFile = pathOfOriCata + "/" + temp;
			malogoFileUrl = webServer + "/catImage/" + temp;
		}
		else if(temp.length() > 6 && temp.substring(0,6).equals("splogo")){
			splogoFile = pathOfOriCata + "/" + temp;
			splogoFileUrl = webServer + "/catImage/" + temp;
			splogoFileName = temp;
		}
	}
%>
<main id="maincnt" class="maincnt">
<%
	SimpleDateFormat sdf = new SimpleDateFormat(get_date());
	ImageSize inImage = new ImageSize();

	int fsize1 = 0;
	String fdate1 = "";
	int[] logo_aaa = {0, 0};
	boolean logoExist = false;

	if(!logoFile.equals("")){
		logoExist = true;
		if(logoFile.endsWith("swf")){
			logo_aaa[0] = 100;
			logo_aaa[1] = 100;
		}
		else{
			inImage.setImage(logoFile);
			logo_aaa[0] = inImage.getWidth();
			logo_aaa[1] = inImage.getHeight();
		}

		f = new File(logoFile);
		fsize1 = (int)f.length()/1000;
		fdate1 = sdf.format(new Date(f.lastModified()));
	}
%>

<% if(logoExist == true){ %>
<section>
<h3>메인 로고 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_logo')" onmouseout="hide_helpdiv();"></h3>
<p class="desc4">
<% if(logoFile.endsWith("swf")){ %>
	<embed width="<%=logo_aaa[0]%>" height="<%=logo_aaa[1]%>" type="application/x-shockwave-flash" src="<%=logoFileUrl%>">
<% } else{ %>
	<img src="<%=logoFileUrl%>" alt="로고파일">
<% } %>
</p>
<p class="title">파일크기</p>
<p class="desc"><%=fsize1%> KB</p>
<p class="title">최종변경일</p>
<p class="desc"><%=fdate1%><</p>
<p class="title">그림크기</p>
<p class="desc"><%=logo_aaa[0]%> x <%=logo_aaa[1]%></p>
<p class="title">파일변경</p>
<div class="desc">
	<form name="cataform" method="post" enctype="multipart/form-data" action="file_act.jsp?act=catalogo_def" onsubmit="return checkup(this)">
	<input type="file" name="pixfile" required>
	<input type="submit" value="변경하기">
	<input type="hidden" name="vari" value="indep">
	<input type="hidden" name="act" value="catalogo_def">
	</form>
</div>
<p id="help_logo" class="secthelp">GIF/PNG/JPG/SWF 파일형식의 파일을 사용하실 수 있습니다.</p>
</section>

<% } else{ %>
<section>
<h3>메인 로고 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_logo')" onmouseout="hide_helpdiv();"></h3>
<p class="desc4">현재 폴더용 메인로고는 설정되어 있지 않습니다.</p>
<p class="title">파일설정</p>
<div class="desc">
	<form name="cataform" method="post" enctype="multipart/form-data" action="file_act.jsp?act=catalogo_def" onsubmit="return checkup(this)">
	<input type="file" name="pixfile" required>
	<input type="submit" value="설정하기">
	<input type="hidden" name="vari" value="indep">
	<input type="hidden" name="act" value="catalogo_def">
	</form>
</div>
<p id="help_logo" class="secthelp">GIF/PNG/JPG/SWF 파일형식의 파일을 사용하실 수 있습니다.</p>
</section>
<% } %>

<%
	int fsize2 = 0;
	String fdate2 = "";
	int[] logo_bbb = {0, 0};
	boolean malogoExist = false;

	if(!malogoFile.equals("")){
		malogoExist = true;
		inImage.setImage(malogoFile);
		logo_bbb[0] = inImage.getWidth();
		logo_bbb[1] = inImage.getHeight();

		f = new File(malogoFile);
		fsize2 = (int)f.length()/1000;
		fdate2 = sdf.format(new Date(f.lastModified()));
	}
%>

<% if(malogoExist == true){ %>
<section>
<h3>메일발송 로고 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_maillogo')" onmouseout="hide_helpdiv();"></h3>
<p class="desc4"><img src="<%=malogoFileUrl%>" alt="메일발송 로고파일"></p>
<p class="title">파일크기</p>
<p class="desc"><%=fsize2%> KB</p>
<p class="title">최종변경일</p>
<p class="desc"><%=fdate2%></p>
<p class="title">그림크기</p>
<p class="desc"><%=logo_bbb[0]%> x <%=logo_bbb[1]%></p>
<p class="title">파일변경</p>
<div class="desc">
	<form name="mailform" method="post" enctype="multipart/form-data" action="file_act.jsp?act=maillogo_def" onsubmit="return checkup(this)">
	<input type="file" name="pixfile" required>
	<input type="submit" value="변경하기">
	<input type="hidden" name="vari" value="indep">
	<input type="hidden" name="act" value="maillogo_def">
	</form>
</div>
<p id="help_maillogo" class="secthelp">GIF/PNG/JPG 파일형식의 파일을 사용하실 수 있습니다.</p>
</section>

<% } else{ %>
<section>
<h3>메일발송 로고 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_maillogo')" onmouseout="hide_helpdiv();"></h3>
<p class="desc4">현재 폴더용 메일로고는 설정되어 있지 않습니다.</p>
<p class="title">파일설정</p>
<div class="desc">
	<form name="mailform" method="post" enctype="multipart/form-data" action="file_act.jsp?act=maillogo_def" onsubmit="return checkup(this)">
	<input type="file" name="pixfile" required>
	<input type="submit" value="설정하기">
	<input type="hidden" name="vari" value="indep">
	<input type="hidden" name="act" value="maillogo_def">
	</form>
</div>
<p id="help_maillogo" class="secthelp">GIF/PNG/JPG 파일형식의 파일을 사용하실 수 있습니다.</p>
</section>
<% } %>

<%
	int fsize3 = 0;
	String fdate3 = "";
	int[] logo_ccc = {0, 0};
	boolean splogoExist = false;

	if(!splogoFile.equals("")){
		splogoExist = true;
		inImage.setImage(splogoFile);
		logo_ccc[0] = inImage.getWidth();
		logo_ccc[1] = inImage.getHeight();

		f = new File(splogoFile);
		fsize3 = (int)f.length()/1000;
		fdate3 = sdf.format(new Date(f.lastModified()));
	}
%>

<% if(splogoExist == true){ %>
<section>
<h3>모바일 로고 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_splogo')" onmouseout="hide_helpdiv();"></h3>
<p class="desc4"><img src="<%=splogoFileUrl%>" alt="모바일 로고파일"></p>
<p class="title">파일크기</p>
<p class="desc"><%=fsize3%> KB</p>
<p class="title">최종변경일</p>
<p class="desc"><%=fdate3%></p>
<p class="title">그림크기</p>
<p class="desc"><%=logo_ccc[0]%> x <%=logo_ccc[1]%></p>
<p class="title">파일변경</p>
<div class="desc">
	<form name="spform" method="post" enctype="multipart/form-data" action="file_act.jsp?act=splogo_def" onsubmit="return checkup(this)">
	<input type="file" name="pixfile" required>
	<input type="submit" value="변경하기">
	<input type="hidden" name="vari" value="indep">
	<input type="hidden" name="act" value="splogo_def">
	</form>
</div>
<p class="title">파일삭제</p>
<div class="desc">
	<form name="spdelform" method="post" action="file_act.jsp" onsubmit="return checkup_del()">
	<input type="submit" value="삭제하기">
	<input type="hidden" name="fname" value="<%=splogoFileName%>">
	<input type="hidden" name="act" value="splogdel">
	</form>
</div>
<p id="help_splogo" class="secthelp">GIF/PNG/JPG 파일형식의 파일을 사용하실 수 있습니다.</p>
</section>

<% } else{ %>
<section>
<h3>모바일 로고 <img src="<%=confServer%>/pix/qshow.gif" width="17" height="15" alt="설명"
  class="helpshow" onmouseover="show_helpdiv(this, 'help_splogo')" onmouseout="hide_helpdiv();"></h3>
<p class="desc4">모바일 로고는 설정되지 않았습니다.</p>
<p class="title">파일설정</p>
<div class="desc">
	<form name="mailform" method="post" enctype="multipart/form-data" action="file_act.jsp?act=splogo_def" onsubmit="return checkup(this)">
	<input type="file" name="pixfile" required>
	<input type="submit" value="설정하기">
	<input type="hidden" name="vari" value="indep">
	<input type="hidden" name="act" value="splogo_def">
	</form>
</div>
<p id="help_splogo" class="secthelp">GIF/PNG/JPG 파일형식의 파일을 사용하실 수 있습니다.</p>
</section>
<% } %>

</main>

<%@ include file="inc_copy.jsp" %>
