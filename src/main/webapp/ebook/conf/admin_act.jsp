<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="admin.jsp" %>
<%
	String jspTags = "%" + ">";
	String path = passSysDir;
	String act = request.getParameter("act");
	if(act == null) act = "";

	if(act.equals("idpass")){							/* self from admin_add.jsp */
		String member_id = request.getParameter("mid");
		String preid = request.getParameter("preid");
		String passwd1 = request.getParameter("passwd1");
		String passwd2 = request.getParameter("passwd2");
		String dist = request.getParameter("dist");

		/* check for password file and cookie value */
		f = new File(path);
		if(!f.exists() || admin_id.equals("")){
			out.println(echo_script("아이디와 비밀번호를 설정하셔야 합니다.", "admin_add.jsp", "",""));
			return;
		}

		if(passwd1.equals("") || !passwd1.equals(passwd2)){
			out.println(echo_script("비밀번호 1, 2의 입력내용이 서로 틀립니다. 확인해주세요.", "admin_add.jsp", "",""));
			return;
		}

		int j = 0;
		StringBuffer idpassBuf = new StringBuffer();
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			if(source.equals(jspTags)) j = 1;
			else if(j == 1){
				int in = source.indexOf(":");
				String skey = source.substring(0, in);
				String sval = source.substring(in+1);

				int in2 = sval.lastIndexOf(":");
				String tPass = "";
				String tDist = "";
				
				if(in2 > -1){
					tPass = sval.substring(0, in2);
					tDist = sval.substring(in2+1);
					if(tDist == null) tDist = "";
				}

				if(skey.equals(preid)){
					idpassBuf.append(member_id).append(":").append(passEncode(passwd1)).append(":").append(tDist).append("\n");
				}
				else{
					idpassBuf.append(source).append("\n");
				}
			}
		}
		fin.close();
		bin.close();

		String source_header = get_accessheader();

		FileOutputStream fos = new FileOutputStream(path);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(source_header + "\n" + idpassBuf.toString());
		osw.close();

		session.setAttribute("sess_id", member_id+":"+cook_adminkd+":"+cook_catimage);

		out.println(echo_script("변경되었습니다.", "admin_add.jsp", "",""));
	}
	else if(act.equals("adminmod")){							/* general by super from nw_adminmod.jsp */
		String member_id = request.getParameter("mid");
		String preid = request.getParameter("preid");
		String passwd1 = request.getParameter("passwd1");
		String passwd2 = request.getParameter("passwd2");
		String dist = request.getParameter("dist");
		StringBuffer idpassBuf = new StringBuffer();


		/* check for password file and cookie value */
		f = new File(path);
		if(!f.exists() || admin_id.equals("")){
			out.println(echo_script("아이디와 비밀번호를 설정하셔야 합니다.", "nw_adminmod.jsp?act=mod&mid="+preid, "",""));
			return;
		}

		/* check whether change the password */
		boolean passChange = true;
		if(passwd1.equals("")) passChange = false;
		if(!passwd1.equals(passwd2)){
			out.println(echo_script("비밀번호 1, 2의 입력내용이 서로 틀립니다. 확인해주세요.", "nw_adminmod.jsp?act=mod&mid="+preid, "",""));
			return;
		}


		int j = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			if(source.equals(jspTags)) j = 1;
			else if(j == 1){
				int in = source.indexOf(":");
				String sval = source.substring(0, in);
				if(!sval.equals(preid) && sval.equals(member_id)){											/* check whether same as current id */
					out.println(echo_script("이미 존재하는 아이디입니다.", "nw_adminmod.jsp?act=mod&mid="+preid, "",""));
					return;
				}
			}
		}
		fin.close();
		bin.close();

		j = 0;
		fin = new FileInputStream(f);
		bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			if(source.equals(jspTags)) j = 1;
			else if(j == 1){
				int in = source.indexOf(":");
				String skey = source.substring(0, in);
				String sval = source.substring(in+1);

				int in2 = sval.lastIndexOf(":");
				String tPass = "";
				String tDist = "";

				if(in2 > -1){
					tPass = sval.substring(0, in2);
					tDist = sval.substring(in2+1);
					if(tDist == null) tDist = "";
				}

				if(skey.equals(preid)){
					if(passChange == true){
						idpassBuf.append(member_id).append(":").append(passEncode(passwd1)).append(":").append(dist).append("\n");
					}
					else{
						idpassBuf.append(member_id).append(":").append(tPass).append(":").append(dist).append("\n");
					}
				}
				else{
					idpassBuf.append(source).append("\n");
				}
			}
		}
		fin.close();
		bin.close();

		String source_header = get_accessheader();

		FileOutputStream fos = new FileOutputStream(path);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(source_header + "\n" + idpassBuf.toString());
		osw.close();

		out.println(echo_script("변경되었습니다.", "admin_add.jsp","opener","window.close();\n"));
	}
	else if(act.equals("adminadd")){
		String member_id = request.getParameter("mid");
		String passwd1 = request.getParameter("passwd1");
		String passwd2 = request.getParameter("passwd2");
		String dist = request.getParameter("dist");
		StringBuffer idpassBuf = new StringBuffer();

		if(member_id.equals("")){
			out.println(echo_script("아이디가 없습니다.", "admin_add.jsp", "",""));
			return;
		}

		f = new File(path);
		if(!f.exists() || admin_id.equals("")){
			out.println(echo_script("아이디와 비밀번호를 설정하셔야 합니다.", "nw_adminmod.jsp?act=add", "",""));
			return;
		}

		if(passwd1.equals("")){
			out.println(echo_script("비밀번호에 내용이 없습니다.", "nw_adminmod.jsp?act=add", "",""));
			return;
		}

		if(!passwd1.equals(passwd2)){
			out.println(echo_script("비밀번호 1, 2의 입력내용이 서로 틀립니다. 확인해주세요.", "nw_adminmod.jsp?act=add", "",""));
			return;
		}

		int j = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			if(source.equals(jspTags)) j = 1;
			else if(j == 1){
				int in = source.indexOf(":");
				String skey = source.substring(0, in);
				if(skey.equals(member_id)){
					out.println(echo_script("이미 존재하는 아이디입니다.", "nw_adminmod.jsp?act=add", "",""));
					return;
				}
				else{
					idpassBuf.append(source).append("\n");
				}
			}
		}
		fin.close();
		bin.close();

		String source_header = get_accessheader();

		FileOutputStream fos = new FileOutputStream(path);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(source_header + "\n" + idpassBuf.toString() + member_id + ":" + passEncode(passwd1) + ":" + dist);
		osw.close();

		out.println(echo_script("관리자가 추가되었습니다.", "admin_add.jsp","opener","window.close();\n"));
	}
	else if(act.equals("admindel")){
		String member_id = request.getParameter("mid");
		StringBuffer idpassBuf = new StringBuffer();

		int j = 0;
		f = new File(path);
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			if(source.equals(jspTags)) j = 1;
			else if(j == 1){
				int in = source.indexOf(":");
				String skey = source.substring(0, in);
				if(!skey.equals(member_id)){
					idpassBuf.append(source).append("\n");
				}
			}
		}
		fin.close();
		bin.close();

		String source_header = get_accessheader();

		FileOutputStream fos = new FileOutputStream(path);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(source_header + "\n" + idpassBuf.toString());
		osw.close();

		out.println(echo_script("관리자가 삭제되었습니다.", "admin_add.jsp", "",""));
	}
%>
