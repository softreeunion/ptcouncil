<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%@ include file="common_func.jsp" %>
<%@ include file="ImageSize.jsp" %>
<%
	String fname = request.getParameter("fname");
	String kd = request.getParameter("kd");
	if(kd == null) kd = "";

	String detfpath = pathOfDir + "/";
	String listfname = "file_catalist.jsp";
	String webpath = webServer + "/catImage" + cook_catimage + "/" + cook_dir + "/";

	if(kd.equals("sound")){
		detfpath = pathOfDir + "/sound/";
		webpath = webServer + "/catImage" + cook_catimage + "/" + cook_dir + "/sound/";
		listfname = "file_soundlist.jsp";
	}
	else if(kd.equals("image")){
		detfpath = pathOfDir + "/image/";
		webpath = webServer + "/catImage" + cook_catimage + "/" + cook_dir + "/image/";
		listfname = "file_imagelist.jsp";
	}	

	String exten = get_extension(fname).toLowerCase();

	if(validate_fname(fname) == true){
		out.print(echo_script(snc_catalog("notform")+"(file name)", listfname, "", ""));
		return;
	}

	f = new File(detfpath + fname);
	if(!f.exists()){
		out.print(echo_script("파일이 존재하지 않습니다.", listfname, "", ""));
		return;
	}

	String[] subar = {"file", "파일내용", "", kd};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 파일내용</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function checkup_mod(){
	var dm = document.modform;

	if(trimVal(dm.pixfile.value) == ""){
		alert("파일을 선택하셔야 합니다.");
		dm.pixfile.focus();
		return false;
	}
	if(confirm("파일을 변경하시겠습니까?")) return true;
	else return false;
}
function checkup_change(){
	var dm = document.changeform;

	if(trimVal(dm.newfname.value) == ""){
		alert("파일이름을 입력하셔야 합니다.");
		dm.newfname.focus();
		return false;
	}	
	if(confirm("파일이름을 변경하시겠습니까?")) return true;
	else return false;
}
function checkup_del(){
	var dm = document.delform;
	if(confirm("파일을 삭제하시겠습니까?")) return true;
	else return false;
}
function go_xmledit(pix){
	newWindow(2, 'nw_xmledit.jsp?fname=' + pix, 800, 600, 0, 0, 'a');
}
</script>
</head>

<body onload="onload_func('<%=listfname%>');">

<%@ include file="inc_menu.jsp" %>
<%
	SimpleDateFormat sdf = new SimpleDateFormat(get_date());
	File f1 = new File(detfpath + fname);
%>
<main id="maincnt" class="maincnt">
<p class="table-title">파일 자세히</p>
<dl>
	<dt>파일이름</dt>
	<dd><%=fname%></dd>
	<dt>파일크기</dt>
	<dd><%=((int)f1.length()/1000)%> KB</dd>
	<dt>최종변경일</dt>
	<dd><%=sdf.format(new Date(f1.lastModified()))%></dd>
	<% if(exten.equals("jpg") || exten.equals("gif") || exten.equals("png")){ %>
	<%
		ImageSize inImage = new ImageSize();
		inImage.setImage(pathOfDir + "/" + fname);
		int bbb_x = inImage.getWidth();
		int bbb_y = inImage.getHeight();
	%>
	<dt>그림크기</dt>
	<dd><%=bbb_x%> x <%=bbb_y%> (너비 x 높이)</dd>
	<% } %>
</dl>

<form name="modform" method="post" enctype="multipart/form-data" action="file_act.jsp" onsubmit="return checkup_mod()">
<p><label>파일변경<br>
	<input type="file" name="pixfile" required></label>
	<input type="submit" value="변경하기">
</p>
<input type="hidden" name="act" value="mod">
<input type="hidden" name="kd" value="<%=kd%>">
<input type="hidden" name="fname" value="<%=fname%>">
</form>

<form name="changeform" method="post" action="file_act.jsp" onsubmit="return checkup_change()">
<p><label>파일이름 변경<br>
	<input type="text" name="newfname" size="20" class="px" required></label>
	<input type="submit" value="이름 변경하기">
</p>
<input type="hidden" name="act" value="fmd">
<input type="hidden" name="kd" value="<%=kd%>">
<input type="hidden" name="fname" value="<%=fname%>">
</form>

<form name="delform" method="post" action="file_act.jsp" onsubmit="return checkup_del()">
<p><label>파일삭제<br>
	<input type="submit" value="삭제하기"></label>
</p>

<input type="hidden" name="fname" value="<%=fname%>">
<input type="hidden" name="kd" value="<%=kd%>">
<input type="hidden" name="act" value="del">
</form>

<% if(exten.equals("jpg") || exten.equals("gif") || exten.equals("png")){ %>
<p><img src="<%=(webpath+fname)%>" alt="이미지"></p>
<% } else if(exten.equals("pdf")){ %>
<p>파일보기<br>
	<input type="button" value="보기" onclick="window.location='<%=(detfpath+fname)%>';">
</p>
<% } else if(exten.equals("xml")){ %>
<p>파일편집<br>
	<input type="button" value="편집하기" onclick="go_xmledit('<%=fname%>')">
</p>
<% } %>

</main>

<%@ include file="inc_copy.jsp" %>
