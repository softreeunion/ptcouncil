<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat, java.lang.Integer" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	String act = request.getParameter("act");
	if(act == null) act = "";

	String path = pathOfDir + "/alink.txt";
	if(act.equals("add")){
		StringBuffer cont = new StringBuffer();
		f = new File(path);
		if(f.exists()){
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				cont.append(source).append("\n");
			}
			bin.close();
			fin.close();
		}

		String pxf = request.getParameter("pxf");
		String coords = request.getParameter("coords");
		String title = request.getParameter("title");
		String url = request.getParameter("url");

		int in = pxf.indexOf(".");

		FileOutputStream fos = new FileOutputStream(path);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString() + pxf + "\t" + coords + "\t" + title + "\t" + url);
		osw.close();

		out.println(echo_script(snc_catalog("added"),"nw_pagelink.jsp?pxf="+pxf,"",""));
	}
	else if(act.equals("mod")){
		String name = request.getParameter("name");
		String url = request.getParameter("url");
		int no = Integer.parseInt(request.getParameter("no"));
		String Pg = request.getParameter("Pg");
		String qfname = request.getParameter("qfname");
		if(qfname == null) qfname = "";

		StringBuffer cont = new StringBuffer();
		int i = 1;
		f = new File(path);
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			if(i == no){
				String[] ccc = mysplit(source, "\t");
				cont.append(ccc[0]).append("\t").append(ccc[1]).append("\t").append(name).append("\t").append(url).append("\n");
			}
			else cont.append(source).append("\n");
			i++;
		}
		bin.close();
		fin.close();

		FileOutputStream fos = new FileOutputStream(path);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.close();

		out.print(echo_script(snc_catalog("changed"), "link_title.jsp?Pg="+Pg+"&qfname="+qfname, "",""));
	}
	else if(act.equals("sod")){
		String pxf = request.getParameter("pxf");
		String coords = request.getParameter("coords");
		String title = request.getParameter("title");
		String url = request.getParameter("url");
		int ckno = Integer.parseInt(request.getParameter("ckno"));

		int i = 1;
		StringBuffer cont = new StringBuffer();
		f = new File(path);
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);

			String[] ccc = mysplit(source, "\t");
			if(!pxf.equals(ccc[0])){
				if(!source.equals("")) cont.append(source).append("\n");
				continue;
			}

			if(i == ckno) cont.append(pxf).append("\t").append(coords).append("\t").append(title).append("\t").append(url).append("\n");
			else cont.append(source).append("\n");
			i++;
		}
		bin.close();
		fin.close();

		FileOutputStream fos = new FileOutputStream(path);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.close();

		out.println(echo_script(snc_catalog("changed"), "nw_pagelink.jsp?pxf="+pxf,"",""));
	}
	else if(act.equals("seq")){
		String Pg = request.getParameter("Pg");
		String qtxt = request.getParameter("qtxt");
		String qfname = request.getParameter("qfname");
		String sd = request.getParameter("sd");

		if(sd.indexOf(":") < 0){
			out.println(echo_text(snc_catalog("notform")+"(no sd colon)"));
			return;
		}

		StringTokenizer st = new StringTokenizer(sd, ":");
		int prevno = Integer.parseInt(st.nextToken());
		int currno = Integer.parseInt(st.nextToken());

		StringBuffer prev_cont = new StringBuffer();
		StringBuffer cont = new StringBuffer();

		int i = 1;
		f = new File(path);
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			if(i == prevno) prev_cont.append(source).append("\n");
			else if(i == currno) cont.append(source).append("\n").append(prev_cont.toString());
			else cont.append(source).append("\n");
			i++;
		}
		bin.close();
		fin.close();

		FileOutputStream fos = new FileOutputStream(path);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.close();

		out.println(echo_script(snc_catalog("ordered"), "link_title.jsp?Pg="+Pg+"&qtxt="+qtxt+"&qfname="+qfname, "",""));
	}
	else if(act.equals("del")){
		String sd = request.getParameter("sd");
		String ckno = request.getParameter("ckno");
		String Pg = request.getParameter("Pg");
		if(Pg == null) Pg = "";

		String[] asd;

		if(sd == null) sd = ckno;

		if(sd.indexOf(":") > 0){
			StringTokenizer st = new StringTokenizer(sd, ":");
			int cnt = st.countTokens();
			asd = new String[cnt];
			cnt = 0;
			while(st.hasMoreTokens()){
				asd[cnt] = st.nextToken();
				cnt++;
			}
		}
		else{
			asd = new String[1];
			asd[0] = sd;
		}

		int i = 1;
		String pxf = request.getParameter("pxf");
		StringBuffer cont = new StringBuffer();
		f = new File(path);
		if(pxf == null){
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				int j = 0;
				for(int k = 0;k < asd.length;k++){
					if(Integer.parseInt(asd[k]) == i) j = 1;
				}

				if(j == 0) cont.append(strTrimEach(source)).append("\n");
				i++;
			}
			bin.close();
			fin.close();
		}
		else{
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				String[] ccc = new String[2];
				StringTokenizer st = new StringTokenizer(source, "\t");
				ccc[0] = st.nextToken();

				if(!pxf.equals(ccc[0])){
					cont.append(source).append("\n");
					continue;
				}

				int j = 0;
				for(int k = 0;k < asd.length;k++){
					if(Integer.parseInt(asd[k]) == i) j = 1;
				}

				if(j == 0) cont.append(source).append("\n");
				i++;
			}
			bin.close();
			fin.close();
		}

		FileOutputStream fos = new FileOutputStream(path);
		OutputStreamWriter osw = new OutputStreamWriter(fos, streamCharset);
		osw.write(cont.toString());
		osw.close();

		String loc = request.getParameter("loc");
		if(loc.equals("slpix")) out.println(echo_script(snc_catalog("deleted"), "nw_pagelink.jsp?pxf="+pxf,"",""));
		else out.println(echo_script(snc_catalog("deleted"), loc+".jsp?Pg="+Pg,"",""));
	}
%>
