<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="common_func.jsp" %>
<%@ include file="ImageSize.jsp" %>
<%@ include file="admin.jsp" %>
<%
	if(cook_dir.equals("")){
		out.println(echo_script(snc_catalog("selfolder"), "basic.jsp", "",""));
		return;
	}

	f = new File(pathOfDir);
	if(!f.exists()){
		out.println(echo_script("지정하신 폴더가 없습니다.", "basic.jsp", "",""));
		return;
	}

	String[] bbb;

	String AddLink = "";
	int Punit = 30;
	int Pg = 0;
	int Pnum = 0;

	String ini1var = "";
	String ini2var = "";
	int pageno = 0;

	f = new File(pathOfDir + "/ecatalog.txt");
	if(f.exists()){
		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);
			if(skey.equals("ini1var")) ini1var = source.substring(in+1);
			else if(skey.equals("ini2var")) ini2var = source.substring(in+1);
			else if(skey.equals("pageno")) pageno = Integer.parseInt(source.substring(in+1));
		}
		fin.close();
		bin.close();
	}

	if(ini1var.equals("")){
		out.println(echo_script(snc_catalog("basicconf"), "basic.jsp", "",""));
		return;
	}

	int h = 0;
	int[] sizea = new int[2];
	char ini1FS = ini1var.charAt(11);

	char fileExt = ini2var.charAt(2);
	String fileExtension = ".jpg";
	if(fileExt == 'P') fileExtension = ".png";

	f = new File(pathOfDir + "/s001" + fileExtension);
	if(f.exists()){
		ImageSize inImage = new ImageSize();
		inImage.setImage(pathOfDir + "/s001" + fileExtension);
		sizea[0] = inImage.getWidth();
		sizea[1] = inImage.getHeight();
	}
	else{
		sizea[0] = 0;
		sizea[1] = 0;
	}

	if(sizea[0] == 0) h = 0;
	else h = (int)(sizea[1] * 100 / sizea[0]);

	String webpath = webServer + "/catImage" + cook_catimage + "/" + cook_dir;
	StringBuffer list_buf = new StringBuffer();

	if(ini1FS == 'X'){
		int[] bvar = new int[4];
		bvar = db_board_start(pageno, Punit, request.getParameter("Pg"));
		int bb = bvar[0];
		int bf = bvar[1];
		Pg = bvar[2];
		Pnum = bvar[3];

		for(int i = 0;i < pageno;i++){
			if(bb < (i+1) && (i+1) <= bf){
				String fname = digit3Number(i+1)+".xml";
				list_buf.append("<div onclick=\"open_pagelink('"+fname+"')\" title=\""+fname+"\">"+(i+1)+" page</div>\n");
			}
		}
	}
	else if(ini1FS == 'F'){
		int alen = 0;
		f = new File(pathOfDir + "/fileseq.txt");
		if(f.exists()){
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;
				alen++;
			}
			fin.close();
			bin.close();

			int[] bvar = new int[4];
			bvar = db_board_start(alen, Punit, request.getParameter("Pg"));
			int bb = bvar[0];
			int bf = bvar[1];
			Pg = bvar[2];
			Pnum = bvar[3];

			int i = 1;
			fin = new FileInputStream(f);
			bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				if(bb < i && i <= bf){
					int in = source.indexOf(":");
					String fname = source.substring(in+1);
					list_buf.append("<div><a href=\"javascript:open_pagelink('"+fname+"');\" title=\""+fname+"\"><img src=\""+webpath+"/"+fname+"\" alt=\""+fname+"\"></a></div>\n");
				}
				i++;
			}
			fin.close();
			bin.close();
		}
	}
	else{
		f = new File(pathOfDir);
		Vector vt = new Vector();
		Vector vt1 = new Vector();
		Vector vt2 = new Vector();
		File[] folders = f.listFiles();
		for(int i = 0;i < folders.length;i++){
			String temp = folders[i].getName();
			if(!temp.equals(".") && !temp.equals("..") && temp.substring(0,1).equals("s") && temp.endsWith(fileExtension)){
				if(temp.length() < 9) vt1.addElement(temp.substring(1,temp.length()));
				else vt2.addElement(temp.substring(1,temp.length()));
			}
		}
		Collections.sort(vt1);
		Collections.sort(vt2);

		vt.addAll(vt1);
		vt.addAll(vt2);
		int alen = vt.size();

		int[] bvar = new int[4];
		bvar = db_board_start(alen, Punit, request.getParameter("Pg"));
		int bb = bvar[0];
		int bf = bvar[1];
		Pg = bvar[2];
		Pnum = bvar[3];

		for(int i = 0;i < alen;i++){
			int counts = i + 1;
			String temp = (String)vt.get(i);

			if(bb < counts && counts <= bf){
				list_buf.append("<div><a href=\"javascript:open_pagelink('"+temp+"');\" title=\""+temp+"\"><img src=\""+webpath+"/"+temp+"\" alt=\""+temp+"\"></a></div>\n");
			}
		}
	}

	String[] subar = {"link", "페이지링크", "name", ""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 페이지 링크</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	div.linkpage img{width:100px;height:<%=h%>px;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function open_pagelink(imgname){
	var property = "toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,scrollbars=no,copyhistory=no,";
	property += "width=1000,height=700,left=0,top=0"
	window.open('nw_pagelink.jsp?pxf='+imgname,'a',property);
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>

<main id="maincnt" class="maincnt">

<p class="table-title">작업하실 이미지를 클릭하십시오.</p>
<div class="linkpage"><%=list_buf%></div>
<p class="table-paging"><%=db_board_nextNumber(Pnum, Pg, AddLink, "link_page.jsp")%></p>

<section class="secthelp3">
<h3>링크URL에 대한 설명</h3>
<dl>
	<dt>[page]페이지번호</dt>
	<dd>다른 페이지로 이동, ex) [page]10</dd>
	<dt>[win][1,100,100]http://url/</dt>
	<dd>별도창으로 다른 웹페이지 호출<br>
	- 1 : 별도창 종류 [<b>1</b> -> 스크롤바 없음, <b>2</b> -> 스크롤바 있음, <b>3</b> -> 부모창에 대한 링크]<br>
	- 100,100 : 별도창의 너비와 높이</dd>
	<dt>[swf]파일이름</dt>
	<dd>마우스 클릭시 플래시 파일 호출, ex) [swf]index.swf</dd>
	<dt>[sta]파일이름</dt>
	<dd>페이지 이동후 플래시 파일 자동 불러옴, ex) [sta]index.swf</dd>
	<dt>[sta/swf][center]파일이름</dt>
	<dd>화면의 가운데에 위치하고 닫기버튼 생성, ex) [sta][center]index.swf</dd>
	<dt>[flv]파일이름</dt>
	<dd>flv 동영상 호출, ex) [flv]001.flv</dd>
	<dt>[mp3]파일이름</dt>
	<dd>mp3 음악 호출, ex) [mp3]001.mp3</dd>
	<dt>[ico][파일이름]링크URL</dt>
	<dd>페이지 이동 후 '파일이름' 아이콘 자동 불러옴. 링크URL은 위의 규칙을 따름</dd>
</dl>
<p>더 자세한 정보는 <a href="help.jsp">도움말 페이지</a>에서 확인하십시오</p>
</section>

</main>

<%@ include file="inc_copy.jsp" %>
