<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	String[] subar = {"admin", "접속기록", "name", ""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 관리자 접속기록</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<style>
	main table tr *:nth-of-type(3n+1){text-align:center;}
	main table tr *:nth-of-type(3n+2){text-align:left;}
	main table tr *:nth-of-type(3n+3){text-align:left;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>

<main id="maincnt" class="maincnt">

<table>
	<thead>
	<tr>
		<th></th>
		<th>접속 시간</th>
		<th>IP</th>
	</tr>
	</thead>
<%
	String[] aconn = new String[10];
	StringBuffer conn_buf = new StringBuffer();

	int i = 9;
	f = new File(pathOfCata + "/conn.txt");
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String storeid = source.substring(0, in);
			if(i >= 0 && storeid.equals(admin_id)) aconn[i] = source.substring(in+1);
			i--;
		}
		fin.close();
		bin.close();

		int k = 1;
		for(i = 0;i < 10;i++){
			if(aconn[i] == null || aconn[i].equals("")) continue;

			String aclass = "b2tdff";
			if(k%2 == 0) aclass = "b2tdf7";

			StringTokenizer st = new StringTokenizer(aconn[i], "FROM");
			String[] ccc = new String[2];
			ccc[0] = (st.hasMoreTokens()) ? st.nextToken() : "";
			ccc[1] = (st.hasMoreTokens()) ? st.nextToken() : "";

			out.print("<tr><td>"+k+"</td>\n");
			out.print("<td>"+ccc[0]+"</td>\n");
			out.print("<td>"+ccc[1]+"</td></tr>\n");
			k++;
		}
	}
%>
</table>

</main>

<%@ include file="inc_copy.jsp" %>
