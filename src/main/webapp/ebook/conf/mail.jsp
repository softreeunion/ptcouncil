<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat" contentType="text/html;charset=utf-8" %>
<%@ include file="admin.jsp" %>
<%
	if(cook_dir.equals("")){
		out.println(echo_script(snc_catalog("selfolder"), "basic.jsp", "",""));
		return;
	}

	String[] subar = {"addon", "메일 발송", "multi", ""};
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog conf - 메일 발송</title>
<link rel="stylesheet" type="text/css" href="<%=confServer%>/style.css">
<script src="<%=webServer%>/common.js"></script>
<script src="<%=confServer%>/common_conf.js"></script>
<script src="<%=confServer%>/explorer.js"></script>
<script>
function checkup(){
	var dm = document.mailform;

	if(trimVal(dm.mbody.value) == ""){
		alert("메일본문의 내용이 없습니다.");
		dm.mbody.focus();
		return false;
	}

	if(trimVal(dm.copyright.value) == ""){
		alert("저작권 내용이 없습니다.");
		dm.copyright.focus();
		return false;
	}

	if(confirm("설정값을 저장하시겠습니까?")){
		dm.action = "action2.jsp";
		dm.target = "";
		return true;
	}
	else return false;
}
function preview(){
	var dm = document.mailform;

	if(trimVal(dm.mbody.value) == "" && trimVal(dm.copyright.value) == ""){
		alert("내용이 없으면 실행할 수 없습니다.");
		return;
	}

	var property = "toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,scrollbars=yes,copyhistory=no,";
	property += "width=635,height=550,left=0,top=0";
	window.open("../xull.html","preWin",property);

	dm.action = "nw_mailpre.jsp";
	dm.target = "preWin";
	dm.submit();
}
function checkup_del(){
	if(confirm("삭제하시겠습니까?")) document.delform.submit();
	else return;
}
</script>
</head>

<body onload="onload_func();">
<%@ include file="inc_menu.jsp" %>
<%
	String pCode = get_parentCode(cook_cate);

	String catedata = "";
	String unifdata = "";
	f = new File(pathOfCata + "/category.txt");
	if(f.exists()){
		int i = 0;
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			if(source.substring(0,10).equals(pCode)){
				if(source.substring(11,15).equals("cate")) catedata = source;
				else if(source.substring(11,15).equals("unif")) unifdata = source;
			}
		}
		fin.close();
		bin.close();
	}

	String mailadapt = "";
	String logoadapt = "";
	if(!catedata.equals("")){
		String[] aaa = mysplit(catedata,"\t");
		logoadapt = aaa[4];
		mailadapt = aaa[5];
		//dist = aaa[2];
		//bascadapt = aaa[3];
		//musicadapt = aaa[6];
	}

	String buf_mailadapt = "개별적용";
	String mailConfName = "";
	String mailLink = "";
	if(!mailadapt.equals("")){
		f = new File(pathOfCata + "/catakind.txt");
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, "|");		// cate|setting|name|etc
			if(aaa[3].equals(mailadapt)){
				mailConfName = aaa[2];
				mailLink = aaa[1] + ":" + aaa[3];
				break;
			}
		}
		fin.close();
		bin.close();

		buf_mailadapt = "'" + mailConfName + "' 설정내용 전체적용";
	}

	int j = 0;
	String mailskin = "";
	String copyright = "";
	StringBuffer cont = new StringBuffer();
	f = new File(pathOfDir + "/mail.txt");
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);

			if(source.equals("")) j = 1;
			else if(j == 1) cont.append(source).append("\n");
			else if(source.substring(0,10).equals("copyright:")) copyright = source.substring(11);
			else if(source.substring(0,9).equals("mailskin:")) mailskin = source.substring(9);
		}
		fin.close();
		bin.close();
	}

	String mbody = strTrim(cont.toString());
%>
<main id="maincnt" class="maincnt">
<form name="mailform" action="action2.jsp" method="post" onsubmit="return checkup()">

<p><label>메일 본문에 첨가할 내용<br>
<textarea name="mbody" rows="10" cols="100" class="px"><%=mbody%></textarea></label></p>

<p><label>저작권 내용<br>
<textarea name="copyright" rows="3" cols="100" class="px"><%=copyright%></textarea></label></p>

<fieldset>
<legend class="normlegend">스킨</legend><br>
<label><input type="radio" name="mailskin" value="001" id="id_mskin1" <% if(mailskin.equals("") || mailskin.equals("001")) out.print("checked"); %>>
	<img src="<%=webServer%>/skin/pix/mailskin001.jpg" width="127" height="101" alt="메일스킨 001"></label>
<label><input type="radio" name="mailskin" value="002" id="id_mskin2" <% if(mailskin.equals("002")) out.print("checked"); %>>
	<img src="<%=webServer%>/skin/pix/mailskin002.jpg" width="127" height="101" alt="메일스킨 002"></label>
</fieldset>

<div class="submitbtn"><input type="submit" value="저장하기"></div>

<input type="hidden" name="act" value="mail">
<input type="hidden" name="cook_dir" value="<%=cook_dir%>">
<input type="hidden" name="catimage" value="<%=cook_catimage%>">
<input type="hidden" name="mailadapt" value="<%=mailadapt%>">
<input type="hidden" name="logoadapt" value="<%=logoadapt%>">
</form>


<% if(!copyright.equals("") && !mbody.equals("")){ %>
<p><a href="javascript:preview();"><img src="<%=confServer%>/pix/link.gif" width="5" height="9" alt=""> 메일 내용 미리보기</a></p>
<p><a href="javascript:checkup_del();"><img src="<%=confServer%>/pix/link.gif" width="5" height="9" alt=""> 설정된 내용 삭제하기</p>
<% } %>

<p><a href="pcata_cate.jsp?cate=<?jsp echo $pCode; ?>"><img src="<%=confServer%>/pix/link.gif" width="5" height="9" alt=""> 분류 설정 바로가기</a> (분류의 메일 설정 : <?jsp echo $buf_mailadapt; ?>)</p>

<div class="nullform">
<form name="delform" method="post" action="action2.jsp">
<input type="hidden" name="act" value="maildel">
</form>
</div>

</main>

<%@ include file="inc_copy.jsp" %>
