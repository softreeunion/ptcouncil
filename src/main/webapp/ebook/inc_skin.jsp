<%@ page pageEncoding="utf-8" %>
<%!
	String charset = "utf-8";
	static String streamCharset = "utf-8";
	String lolang = "korean";
	String default_font = "굴림";
	String od_platform = "both";

	static String webServer = "/ebook";
	static String webSysDir = "/data/webSrc/ptcouncilTOBE/ebook";
	static String wasServer = "/ebook";

	static String catimage = "";
	static String Dir = "";

	static String pathOfOriCata = "";
	static String pathOfCata = "";
	static String pathOfDir = "";

	public String get_userSkin(String askin, String incview){
		String display = askin;
		if(askin.length() > 5){
			String[] aaa = mysplit(askin, "|");
			display = incview.equals("d") ? aaa[0] : aaa[7];
		}
		char firstval = display.charAt(0);
		if(firstval == '4' || firstval == '6' || firstval == '9') return display;
		return "";
	}

	public void set_userSkinDef(HashMap Display){
		Display.put("logo", "");
		Display.put("help", "");
	}

	public void set_userSkin(String nskin, HashMap Display) throws FileNotFoundException, UnsupportedEncodingException, IOException {
		String findstr = "[" + nskin + "]";
		boolean found = false;
		String source = "";
		File f = new File(pathOfOriCata + "/display.txt");
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")){
				if(found == true) break;
				continue;
			}
			if(source.length() > 2 && source.substring(0,2).equals("//")) continue;
			if(source.equals(findstr)){
				found = true;
				continue;
			}
			if(found == true){
				int in = source.indexOf(":");
				if(in == -1) continue;
				String skey = source.substring(0, in);
				String sval = source.substring(in+1);
				Display.put(skey, sval);
			}
		}
		fin.close();
		bin.close();
	}

	public void set_skin(String incview, HashMap Skin, String askin, String bskin, HashMap Category, HashMap Display){
		String[] aaa = mysplit(askin, "|");

		String suser = (String)Display.get("user");
		Skin.put("display", "001");
		if(incview.equals("d")){
			if(!suser.equals("") && !strNullObject(Display.get("skinc_d")).equals("")) Skin.put("display", (String)Display.get("skinc_d"));
			else Skin.put("display", aaa[0]);
		}
		else if(incview.equals("m")){
			if(!suser.equals("") && !strNullObject(Display.get("skinc_m")).equals("")) Skin.put("display", (String)Display.get("skinc_m"));
			else Skin.put("display", aaa[7]);
		}
		else if(incview.equals("s") || incview.equals("e")){
			Skin.put("display", aaa[7]);
			if(!aaa[7].equals("001") && !aaa[7].equals("002") && !aaa[7].equals("041") && !aaa[7].equals("042")) Skin.put("display", "001");
		}
		else if(incview.equals("t")){
			Skin.put("display", aaa[7]);
			if(!aaa[7].equals("001") && !aaa[7].equals("002")) Skin.put("display", "001");
		}

		String display = (String)Skin.get("display");
	
		Skin.put("mmType", "deco");
		Skin.put("bookdeco", aaa[1]);
		Skin.put("spacerbutton", aaa[2]);
		Skin.put("smcenter", aaa[3]);
		Skin.put("onsmimage", aaa[4]);
		Skin.put("onsmskin", "000");
		if(incview.equals("m")) Skin.put("smc", aaa[9]);
		else Skin.put("smc", aaa[5]);
		Skin.put("smcdockind", get_smcdockind(aaa[5]));
		Skin.put("smGallery", "false");
		Skin.put("onbigimage", aaa[6]);
		Skin.put("mskin", aaa[7]);
		Skin.put("explorer", aaa[8]);
		Skin.put("loader", aaa[9]);

		String[] bbb = mysplit(bskin, "|");
		Skin.put("combokind", bbb[0]);
		if(Category.get("fmove").equals("1")) Skin.put("combokind", "000");
		Skin.put("enc", bbb[1]);
		Skin.put("indexmc", bbb[2]);
		Skin.put("descriptmc", bbb[3]);
		Skin.put("helpmc", bbb[4]);
		Skin.put("mailmc", bbb[5]);
		Skin.put("printKindmc", bbb[6]);
		Skin.put("maindeco", bbb[7]);
		Skin.put("spacermc", bbb[8]);

		Skin.put("cpskin", "001");
		Skin.put("media", display);
		if(Category.get("music").equals("none")) Skin.put("media", "000");
		Skin.put("newin", get_newinskin(display,incview));
		Skin.put("idxnewin", "newinE"+get_newinskin(display,incview));
		Skin.put("skinc", display);

		if(display.equals("001") || display.equals("041")){
			Skin.put("mmType", "top");
		}
		else if(display.equals("011")){
			Skin.put("spacerbutton", "011");
		}
		else if(display.equals("012")){
			Skin.put("cpskin", "012");
			if(incview.equals("d")) Skin.put("media", "000");
		}
		else if(display.equals("013")){
			Skin.put("onsmimage", "013");
		}
		else if(display.equals("016")){
			Skin.put("cpskin", "016");
			Skin.put("bookdeco", "016");
			Skin.put("onsmimage", "016");
			Skin.put("onsmskin", "016");
		}
		else if(display.equals("017")){
			Skin.put("cpskin", "016");
			Skin.put("skinc", "016");
			Skin.put("bookdeco", "016");
			Skin.put("onsmimage", "016");
			Skin.put("onsmskin", "017");
		}
		else if(display.equals("018")){
			Skin.put("mmType", "bottom");
			Skin.put("cpskin", "016");
			Skin.put("skinc", "016");
			Skin.put("bookdeco", "016");
			Skin.put("onsmimage", "016");
			Skin.put("onsmskin", "018");
		}
		else if(display.equals("020")){
			Skin.put("mmType", "bottom");
			Skin.put("media", "000");
		}
		else if(display.equals("021")){
			Skin.put("mmType", "bottom");
			Skin.put("media", "000");
			Skin.put("cpskin", "016");
		}
		else if(display.equals("301")){
			Skin.put("mmType", "bottom");
		}
		else if(display.equals("302")){
			Skin.put("skinc", "301");
		}

		if(!suser.equals("")){
			if(incview.equals("d")){
				Skin.put("display", aaa[0]);
				if(!strNullObject(Display.get("newin")).equals("")) Skin.put("newin", (String)Display.get("newin"));
				if(!strNullObject(Display.get("help")).equals("")) Skin.put("helpmc","002");
			}
			else if(incview.equals("m")){
				Skin.put("display", aaa[7]);
			}
			if(!strNullObject((String)Display.get("onsmimage")).equals("")) Skin.put("onsmimage", (String)Display.get("onsmimage"));
			if(!strNullObject((String)Display.get("bookdeco")).equals("")) Skin.put("bookdeco", (String)Display.get("bookdeco"));
			if(!strNullObject((String)Display.get("cpskin")).equals("")) Skin.put("cpskin", (String)Display.get("cpskin"));
			if(!strNullObject((String)Display.get("spacerbutton")).equals("")) Skin.put("spacerbutton", (String)Display.get("spacerbutton"));
			if(!strNullObject((String)Display.get("indexmc")).equals("")) Skin.put("indexmc", (String)Display.get("indexmc"));
			if(!strNullObject((String)Display.get("etcopt")).equals("")){
				String source, skey, sval;
				int in;
				String[] ccc = mysplit((String)Display.get("etcopt"),"&");
				for(int i = 0;i < ccc.length;i++){
					source = ccc[i];
					in = source.indexOf("=");
					if(in == -1) continue;
					skey = source.substring(0, in);
					sval = source.substring(in+1);
					Display.put(skey, sval);
				}
			}
		}

		String smc = (String)Skin.get("smc");
		if(incview.equals("d")){
			if(smc.equals("004") || smc.equals("007") || smc.equals("008") || smc.charAt(0) == '3'){
				Skin.put("smcenter", "000");
				Skin.put("spacerbutton", "000");
				Skin.put("bookdeco", "000");

				if(smc.charAt(0) == '3') Skin.put("smGallery", "true");
				if(smc.equals("004") || smc.equals("007")) Skin.put("onsmimage", "000");
			}
		}
		else{
			Skin.put("smcenter", "000");
			Skin.put("bookdeco", "000");
			Skin.put("onsmimage", "000");
			if(!Skin.get("spacerbutton").equals("000")) Skin.put("spacerbutton", "041");
		}

		char firstval = display.charAt(0);
		if(firstval == '4' || firstval == '6' || firstval == '9') Skin.put("cssnDir", get_cssnDirskin((String)Skin.get("display")));
		else Skin.put("cssnDir", (String)Skin.get("display"));
	}

	public String get_cssnDirskin(String display){
		return display;
		//char firstval = display.charAt(0);
		//if(firstval != '6' && firstval != '7' && firstval != '9') return display;
		//char lastval = display.charAt(display.length()-1);
		//return (lastval > 96 && lastval < 123) ? display.substring(0, display.length()-1) : display;
	}

	public String get_newinskin(String skin, String incview){
		if(incview.equals("m")) return "041";
		else if(skin.equals("012") || skin.equals("013")) return "012";
		else if(skin.equals("016") || skin.equals("017") || skin.equals("018") || skin.equals("021")) return "016";
		return "001";
	}

	public void set_newintitle(String r, String display, String path, HashMap Display){
		String d2 = get_cssnDirskin(display);
		File f = new File(path+"/mm"+d2+"/"+r+"_wintitle.png");
		if(f.exists()) Display.put("titlepix", "mm"+d2+"/"+r+"_wintitle.png");
		else{
			f = new File(path+"/mm"+d2+"/"+r+"_wintitle.jpg");
			if(f.exists()) Display.put("titlepix", "mm"+d2+"/"+r+"_wintitle.jpg");
			else{
				f = new File(path+"/mm"+d2+"/"+r+"_wintitle.gif");
				if(f.exists()) Display.put("titlepix", "mm"+d2+"/"+r+"_wintitle.gif");
				else Display.put("titlepix", "");
			}
		}

		if(!Display.get("titlepix").equals("")){
			ImageSize inImage = new ImageSize();
			inImage.setImage(path+"/"+Display.get("titlepix"));
			Display.put("titlepix_w", inImage.getWidth());
			Display.put("titlepix_h", inImage.getHeight());
		}
		else{
			Display.put("titlepix_w", 0);
			Display.put("titlepix_h", 0);
		}
	}

	public String get_indextype(String skin, String type, String incview, HashMap Skin){
		if(incview.equals("m") && type.equals("H")){
			Skin.put("idxnewin", "indexsect5mD"+skin);
		}
		if(incview.equals("m") && (skin.equals("016") || skin.equals("017") || skin.equals("030"))) return "R";
		if(incview.equals("m") || skin.equals("001") || skin.equals("010") || skin.equals("011") || skin.substring(0,1).equals("6")) return type;
		return "R";
	}

	public int[] get_skindimension(String idxtype, String skin, String incview, String mokchaFile){			// x, y, right, bottom -> ...
		int[] d = {0, 0, 0, 0, 0, 0, 0, 0};
		if(skin.equals("001")){
			if(incview.equals("m")){
				if(idxtype.equals("H") && mokchaFile.equals("true")){ d[1] = 65; d[3] = 35; }
				else{ d[1] = 41; d[3] = 35; }
			}
			else if(idxtype.equals("H")){ d[1] = 72; d[7] = 18; }
			else{ d[1] = 50; d[7] = 18; }
		}
		else if(skin.equals("010")){
			if(incview.equals("m")){
				if(idxtype.equals("H") && mokchaFile.equals("true")){ d[1] = 66; d[3] = 34; }
				else{ d[1] = 40; d[3] = 34; }
			}
			else if(idxtype.equals("H")){ d[0] = 10; d[1] = 77; d[2] = 10; d[3] = 25; }
			else{ d[0] = 10; d[1] = 54; d[2] = 10; d[3] = 25; }
		}
		else if(skin.equals("011")){
			if(incview.equals("m")){
				if(idxtype.equals("H") && mokchaFile.equals("true")){ d[1] = 67; d[3] = 34; }
				else{ d[1] = 40; d[3] = 34; }
			}
			else if(idxtype.equals("H")){ d[0] = 45; d[1] = 105; d[2] = 129; d[3] = 51; }
			else{ d[0] = 45; d[1] = 83; d[2] = 129; d[3] = 51; }
		}
		else if(skin.equals("012")){
			if(incview.equals("m")){
				if(idxtype.equals("H") && mokchaFile.equals("true")){ d[1] = 72; d[3] = 41; }
				else{ d[1] = 43; d[3] = 41; }
			}
			else{ d[1] = 60; d[3] = 70; }
		}
		else if(skin.equals("013")){
			d[1] = 60; d[3] = 40;
		}
		else if(skin.equals("016") || skin.equals("017")){
			if(incview.equals("m")){ d[1] = 60; d[3] = 50; }
			else{ d[1] = 60; d[3] = 70; }
		}
		else if(skin.equals("018")){
			if(incview.equals("m") && idxtype.equals("H") && mokchaFile.equals("true")){ d[1] = 28; d[3] = 70; d[7] = 70; }
			else{ d[3] = 70; d[7] = 70; }
		}
		else if(skin.equals("020")){
			if(incview.equals("m") && idxtype.equals("H") && mokchaFile.equals("true")){ d[1] = 28; d[3] = 50; d[7] = 50; }
			else{ d[3] = 50; d[7] = 50; }
		}
		else if(skin.equals("021")){
			d[3] = 70; d[7] = 70;
		}
		else if(skin.equals("030")){
			if(incview.equals("m")){ d[3] = 30; }
			else{ d[0] = 14; }
		}
		else if(skin.equals("301")){			// gallery
			if(incview.equals("m")){ d[3] = 63; }
			else{ d[3] = 80; }
		}
		else if(skin.equals("302")){			// gallery
			if(incview.equals("m")){ d[0] = 7; d[1] = 60; d[2] = 7; d[3] = 85; }
			else{ d[0] = 7; d[1] = 60; d[2] = 7; d[3] = 105; }
		}
		else if(skin.equals("041")){			// only m
			if(idxtype.equals("H") && mokchaFile.equals("true")){ d[1] = 81; }
			else{ d[1] = 40; }
		}
		else if(skin.equals("042")){			// only m
			if(idxtype.equals("H") && mokchaFile.equals("true")){ d[1] = 70; d[3] = 60; }
			else{ d[1] = 40; d[3] = 60; }
		}
		return d;
	}
	public boolean check_skindimension2(String s){
		if(strTrimEach(s).equals("")) return false;
		if(s.length()-s.replaceAll(",","").length() != 7) return false;
		return true;
	}
	public int[] get_skindimension2(String s, String idxtype, String mokchaFile){			// x, y, right, bottom -> ...
		int[] ardim = {0, 0, 0, 0, 0, 0, 0, 0};
		String[] ar = mysplit(s, ",");
		for(int i = 0;i < ar.length;i++){
			ardim[i] = Integer.parseInt(strTrimEach(ar[i]));
			if(i > 6) break;
		}
		if(idxtype.equals("H") && mokchaFile.equals("true") && ar.length > 8) ardim[1] = Integer.parseInt(strTrimEach(ar[8]));
		return ardim;
	}

	public String get_smcdockind(String smc){
		if(smc.equals("001") || smc.equals("004") || smc.equals("005") || smc.equals("007")	|| smc.equals("008") || smc.equals("009") || smc.charAt(0) == '3'){
			return "loadall";
		}
		return "loadeach";
	}

	public String get_bodyskin(String s){		// mobile
		if(s.equals("002")) return "002";
		return "001";
	}

	// checking parameters
	public String check_start(String s){
		if(s == null) return "";
		if(s.indexOf("-") > -1) s = s.substring(0, s.indexOf("-"));
		return get_param1(s, 5);
	}
	public String check_cate(String s){
		if(s == null) return "";
		String res = get_numalphaVar(s);
		if(res.length() != 10) return "";
		return res;
	}
	public String check_callmode(String s){
		if(s == null) return "normal";
		if(!s.equals("normal") && !s.equals("admin") && !s.equals("cdmake") && !s.equals("normal_unify") && !s.equals("admin_unify") && !s.equals("cdmake_unify") && !s.equals("")) return "normal";
		return s;
	}
	public String check_eclang(String s){
		if(s == null) return "";
		if(!s.equals("english") && !s.equals("korean") && !s.equals("")) return "";
		return s;
	}
	public String check_um(String s, String skd){
		if(s == null) return "s";
		if(skd.equals("um") && !s.equals("s") && !s.equals("t") && !s.equals("m") && !s.equals("e") && !s.equals("d")) return "s";
		else if(skd.equals("wm") && !s.equals("s") && !s.equals("e")) return "s";
		return s;
	}
	public String check_docu(String s, String skd){
		if(s == null) return "";
		String res = get_alphaVar(s);
		if(res.length() > 6) return res.substring(0,6);
		return res;	
	}
	public String check_cpage(String s){
		if(s == null) return "";
		if(s.equals("empty") || s.equals("view") || s.equals("")) return s;
		return get_param1(s, 5);
	}
	public boolean check_fname(String fname){
		java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("[/\\\\:*?^\"'<>|]");
		return !pattern.matcher(fname).find();
	}
	public boolean check_dirname(String dname){
		java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("[a-z0-9A-Z_.-]");
		return pattern.matcher(dname).find();
	}
	public boolean check_email(String email){
		boolean valid = false;
		java.util.regex.Pattern p = java.util.regex.Pattern.compile("^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$");
		java.util.regex.Matcher m = p.matcher(email);
		if(m.matches()) valid = true;
		return valid;
	}
	public String check_mainmenu(String s, String skd){
		if(s == null) return "001";
		if(s.equals("")) return "001";
		return s;
	}

	public String get_param1(String s, int maxlen){
		if(s == null) return "";
		s = get_numVar(s);
		if(s.length() > maxlen) return s.substring(0, maxlen);
		return s;
	}
	public String get_param2(String s, int maxlen){
		if(s == null) return "";
		s = get_numalphaVar(s);
		if(s.length() > maxlen) return s.substring(0, maxlen);
		return s;
	}
	public String get_param3(String s, int maxlen){
		if(s == null) return "";
		s = get_alphaVar(s);
		if(s.length() > maxlen) return s.substring(0, maxlen);
		return s;
	}
	public String get_qtxt1(String s, int maxlen){
		if(s == null) return "";
		s = s.replaceAll("[`~!@#$%^&*+[\\\\]\\\\\\\\;\\',./{}|:\\\"<>?\\\\=]","");			// all special except for ()_-
		if(s.length() > maxlen) return s.substring(0, maxlen);
		return s;
	}
	public String get_qtxt2(String s, int maxlen){
		if(s == null) return "";
		s = s.replaceAll("\\<.*?\\>","");													// strip tags
		if(s.length() > maxlen) return s.substring(0,maxlen);
		return s;
	}
	public String get_qtxt3(String s, int maxlen){
		if(s == null) return "";
		s = s.replaceAll("[`~!@#$%^&*()_+[\\\\]\\\\\\\\;\\',./{}|:\\\"<>?\\\\=-]","");		// all special
		if(s.length() > maxlen) return s.substring(0,maxlen);
		return s;
	}

	public boolean check_numalphaVar(String s, int cn){
		int alen = s.length();
		if(cn != -1){
			if(alen != cn) return false;
		}

		for(int i = 0;i < alen;i++){
			char c = s.charAt(i);
			if((c >= '0' && c <= '9') || (c >= 'A' && c <= 'z')) continue;
			else return false;
		}
		return true;
	}
	public boolean check_numVar(String s){
		int alen = s.length();
		for(int i = 0;i < alen;i++){
			char c = s.charAt(i);
			if(c >= '0' && c <= '9') continue;
			else return false;
		}
		return true;
	}
	public String get_numalphaVar(String s){
		String resstr = "";
		int alen = s.length();
		for(int i = 0;i < alen;i++){
			char c = s.charAt(i);
			if((c >= '0' && c <= '9') || (c >= 'A' && c <= 'z')) resstr += String.valueOf(c);
		}
		return resstr;
	}
	public String get_numVar(String s){
		String resstr = "";
		int alen = s.length();
		for(int i = 0;i < alen;i++){
			char c = s.charAt(i);
			if(c >= '0' && c <= '9') resstr += String.valueOf(c);
		}
		return resstr;
	}
	public String get_alphaVar(String s){
		String resstr = "";
		int alen = s.length();
		for(int i = 0;i < alen;i++){
			char c = s.charAt(i);
			if(c >= 'A' && c <= 'z') resstr += String.valueOf(c);
		}
		return resstr;
	}

	static public String[] get_machine(String agent){
		String[] res = {"", "d"};
		if(agent.indexOf("iPad") >= 0){ res[0] = "i-Pad"; res[1] = "t"; }
		else if(agent.indexOf("SHW-M18") >= 0){ res[0] = "Galaxy Tab"; res[1] = "t"; } 			// galaxy Tab 7.0(180S/K/L/W)
		else if(agent.indexOf("SHW-M38") >= 0){ res[0] = "Galaxy Tab 10.1"; res[1] = "t"; } 	// galaxy Tab 10.1(380S/K/W)
		else if(agent.indexOf("SHV-E14") >= 0){ res[0] = "Galaxy Tab 8.9"; res[1] = "t"; } 		// galaxy Tab 8.9(140S/K/L)
		else if(agent.indexOf("SHV-E15") >= 0){ res[0] = "Galaxy Tab 7.7"; res[1] = "t"; } 		// galaxy Tab 7.7(150S)
		else if(agent.indexOf("SM-T") >= 0){ res[0] = "Galaxy Tab3 8.0"; res[1] = "t"; }		// galaxy Tab3 8.0
		else if(agent.indexOf("SHV-E23") >= 0 || agent.indexOf("SHW-M48") >= 0 || agent.indexOf("SM-P60") >= 0){ res[0] = "Galaxy Note 10.1"; res[1] = "t"; }
		else if(agent.indexOf("SHW-M50") >= 0){ res[0] = "Galaxy Note 8.0"; res[1] = "t"; }
		else if(agent.indexOf("MSIE") >= 0 && agent.indexOf("Touch") >= 0){ res[0] = "MS Surface"; res[1] = "t"; }
		else if(agent.indexOf("iPhone") >= 0){ res[0] = "i-Phone"; res[1] = "s"; }
		else if(agent.indexOf("Android") >= 0){ res[0] = "Android"; res[1] = "s"; }
		return res;
	}

	public String get_itemKd(String s){
		if(s.equals("0")) return "category";
		else if(s.equals("C")) return "catalog";
		else if(s.equals("G")) return "gallery";
		return "category";
	}

	static public int get_cateStep(String cate){
		if(cate.substring(2,10).equals("00000000")) return 1;
		else if(cate.substring(4,10).equals("000000")) return 2;
		else if(cate.substring(6,10).equals("0000")) return 3;
		else if(cate.substring(8,10).equals("00")) return 4;
		return 5;
	}

	public String get_parentCode(String cate){
		if(cate.substring(2,10).equals("00000000")) return "0000000000";
		else if(cate.substring(4,10).equals("000000")) return cate.substring(0,2)+"00000000";
		else if(cate.substring(6,10).equals("0000")) return cate.substring(0,4)+"000000";
		return "0000000000";
	}

	public String get_extension(String str){
		int n = str.indexOf(".");
		if(n == -1) return "";
		return str.substring(n+1);
	}

	public String strTrimEach(String str){
		if(str == null) return "";
		if(str.equals("")) return "";

		String s = str.replaceAll("[\\r\\n ]+$","");
		s = s.replaceAll("^[\\r\\n ]+","");

		return s;
	}
	public static String[] mysplit(String s, String delimiter){
		java.util.Vector v = new java.util.Vector();
		if(s == null){
			return null;
		}
		else if(s == ""){
			v.addElement("");
		}
		else{
			int idx;
			idx = s.indexOf(delimiter);
			while(idx > -1){
				if(idx == 0) v.addElement("");
				else v.addElement(s.substring(0, idx));

				s = s.substring(idx+1);
				idx = s.indexOf(delimiter);
			}

			v.addElement(s);			
		}

		String[] aaa = new String[v.size()];
		for(int i = 0;i < v.size();i++){
			aaa[i] = (String)v.elementAt(i);
		}
		return aaa;
	}
	public static String arrayJoin(String glue, int array[]){
		StringBuffer result = new StringBuffer();
		result.append(array[0]);
		for(int i = 1;i < array.length;i++){
			result.append(glue).append(array[i]);
		}
		return result.toString();
	}
	public static String strNullObject(Object o){
		if(o == null) return "";
		return (String)o;
	}
%>
<%!
 public class ImageSize 
{

    private int width;
    private int height;

    private InputStream in;
    private int bitBuf;
    private int bitPos;


	/**
	 * 생성자<br>
	 * 생성후 반드시 setImage() 메쏘드를 통하여 이미지 파일을 세팅해야 한다.
	 *
	 */
    public ImageSize()
	{ }

	/**
	 * ImageFile의 유효성을 체크<br>
	 * gif, jpg, png, swf 지원
	 *
	 * @return 유효하면 true, 그렇지 않으면 false
	 */
    public boolean check() 
	{
        width = 0;
        height = 0;

        try {
            int b1 = read() & 0xff;
            int b2 = read() & 0xff;
            if (b1 == 0x47 && b2 == 0x49) {
                return checkGif();
            }
            else if (b1 == 0xff && b2 == 0xd8) {
                return checkJpeg();
            }
            else if (b1 == 0x89 && b2 == 0x50) {
                return checkPng();
            }
            else if (b1 == 0x46 && b2 == 0x57) {
                return checkSwf();
            }
            else {
                return false;
            }
        } catch (IOException ioe) {
            return false;
        }
    }

	/**
	 * Gif ImageFile의 유효성을 체크
	 *
	 * @return 유효하면 true, 그렇지 않으면 false
	 */
    private boolean checkGif() throws IOException 
	{
        final byte[] GIF_MAGIC_87A = {0x46, 0x38, 0x37, 0x61};
        final byte[] GIF_MAGIC_89A = {0x46, 0x38, 0x39, 0x61};
        byte[] a = new byte[11]; 

        if (read(a) != 11) {
            return false;
        }
        if ((!equals(a, 0, GIF_MAGIC_89A, 0, 4)) &&
            (!equals(a, 0, GIF_MAGIC_87A, 0, 4))) {
            return false;
        }
        width = getShortLittleEndian(a, 4);
        height = getShortLittleEndian(a, 6);

        return (width>0 && height>0);
    }

	/**
	 * Jpeg ImageFile의 유효성을 체크
	 *
	 * @return 유효하면 true, 그렇지 않으면 false
	 */
    private boolean checkJpeg() throws IOException 
	{
        byte[] data = new byte[12];
        while (true) {
            if (read(data, 0, 4) != 4) {
                return false;
            }
            int marker = getShortBigEndian(data, 0);
            int size = getShortBigEndian(data, 2);

            if((marker & 0xff00) != 0xff00){
                return false; 
            }
            if(marker >= 0xffc0 && marker <= 0xffcf && marker != 0xffc4 && marker != 0xffc8) {
                if (read(data, 0, 6) != 6) {
                    return false;
                }
                width = getShortBigEndian(data, 3);
                height = getShortBigEndian(data, 1);

                return (width>0 && height>0);
            } 
            else{
                skip(size - 2);
            }
        }
    }

	/**
	 * Png ImageFile의 유효성을 체크
	 *
	 * @return 유효하면 true, 그렇지 않으면 false
	 */
    private boolean checkPng() throws IOException 
	{
        final byte[] PNG_MAGIC = {0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a};
        byte[] a = new byte[24];
        if (read(a) != 24) {
            return false;
        }
        if (!equals(a, 0, PNG_MAGIC, 0, 6)) {
            return false;
        }
        width = getIntBigEndian(a, 14);
        height = getIntBigEndian(a, 18);

        return (width > 0 && height > 0);
    }


	/**
	 * Swf ImageFile의 유효성을 체크
	 *
	 * @return 유효하면 true, 그렇지 않으면 false
	 */
    private boolean checkSwf() throws IOException 
	{
        byte[] a = new byte[6];
        if (read(a) != a.length) {
            return false;
        }
        int bitSize = (int)readUBits( 5 );
        int minX = (int)readSBits( bitSize );
        int maxX = (int)readSBits( bitSize );
        int minY = (int)readSBits( bitSize );
        int maxY = (int)readSBits( bitSize );
        width = maxX/20; 
        height = maxY/20;  
        return (width > 0 && height > 0);
    }

	/**
	 * 두개의 자료가 일치하는지 체크
	 *
	 * @param a1 첫번째 바이트 배열
	 * @param offs1 첫번째 offset
	 * @param a2 두번째 바이트 배열
	 * @param offs2 두번째 offset
	 * @param num 일치하는지 확인할 바이트수
	 * @return 일치하면 true, 그렇지 않으면 false
	 */
    private boolean equals(byte[] a1, int offs1, byte[] a2, int offs2, int num) 
	{
        while (num-- > 0) {
            if (a1[offs1++] != a2[offs2++]) {
                return false;
            }
        }
        return true;
    }
    
	private int getIntBigEndian(byte[] a, int offs) 
	{
        return
            (a[offs] & 0xff) << 24 | 
            (a[offs + 1] & 0xff) << 16 | 
            (a[offs + 2] & 0xff) << 8 | 
            a[offs + 3] & 0xff;
    }
    private int getIntLittleEndian(byte[] a, int offs) 
	{
        return
            (a[offs + 3] & 0xff) << 24 | 
            (a[offs + 2] & 0xff) << 16 | 
            (a[offs + 1] & 0xff) << 8 | 
            a[offs] & 0xff;
    }
    private int getShortBigEndian(byte[] a, int offs) 
	{
        return
            (a[offs] & 0xff) << 8 | 
            (a[offs + 1] & 0xff);
    }
    private int getShortLittleEndian(byte[] a, int offs) 
	{
        return (a[offs] & 0xff) | (a[offs + 1] & 0xff) << 8;
    }
    private int read() throws IOException 
	{
        return in.read();
    }
    private int read(byte[] a) throws IOException 
	{
        if (in != null) {
            return in.read(a);
        }
        return 0;
    }
    private int read(byte[] a, int offset, int num) throws IOException 
	{
        if (in != null) {
            return in.read(a, offset, num);
        }
        return 0;
    }
    private long readUBits( int numBits ) throws IOException
    {
        if (numBits == 0) {
            return 0;
        }
        int bitsLeft = numBits;
        long result = 0;
        if (bitPos == 0) { 
            bitBuf = in.read();
            bitPos = 8;
        }
        
        while(true){
            int shift = bitsLeft - bitPos;
            if(shift > 0){
                result |= bitBuf << shift;
                bitsLeft -= bitPos;

                bitBuf = in.read();
                bitPos = 8;
            }
            else{
                result |= bitBuf >> -shift;
                bitPos -= bitsLeft;
                bitBuf &= 0xff >> (8 - bitPos);    

                return result;
            }
        }        
    }
    
    private int readSBits( int numBits ) throws IOException
	{
        long uBits = readUBits( numBits );
        if(( uBits & (1L << (numBits - 1))) != 0 ){
            uBits |= -1L << numBits;
        }
        return (int)uBits;        
    }  
    private void skip(int num) throws IOException 
	{
        in.skip(num);
    }

	/**
	 * 이미지 파일의 높이를 반환
	 *
	 * @return 이미지 파일의 높이 (pixel)
	 */
    public int getHeight() 
	{
        return height;
    }
	
	/**
	 * 이미지 파일의 넓이를 반환
	 *
	 * @return 이미지 파일의 넓이 (pixel)
	 */
    public int getWidth() 
	{
        return width;
    }

	/**
	 * 이미지 파일 세팅
	 *
	 * @param imagepath 이미지 파일의 경로 (절대경로)
	 * @return 파일이 유효하면 true, 존재하지 않거나 유효하지 않으면 false
	 */
    public boolean setImage(String imagepath)
	{
        try{
            in = new FileInputStream(imagepath);    
            return check();
        }
        catch(Exception e){
            return false;
        }
        finally{
            try{if(in != null) in.close();}catch(Exception ie){}
        }
    }
}
%>
