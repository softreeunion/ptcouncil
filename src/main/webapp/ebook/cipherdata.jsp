<%@ page import="java.io.*, java.util.*" contentType="text/plain; charset=utf-8" %>
<%@ include file="inc_skin.jsp" %>
<%
	String mykey = request.getParameter("mykey");
	if(mykey == null) mykey = "";

	String myurl = request.getParameter("myurl");
	if(myurl == null) myurl = "";

	String[] aaa = mysplit(myurl, "/");
	String realurl = aaa[0];
	for(int i = 1;i < aaa.length;i++){
		String tstr = aaa[i];
		if(!tstr.equals("") && !tstr.equals(".") && !tstr.equals("..")) continue;
		if(i == aaa.length-1){
			if(check_fname(tstr) == true) realurl += "/" + tstr;
		}
		else{
			if(check_dirname(tstr) == true) realurl += "/" + tstr;
		}
	}

	String realPath = application.getRealPath(request.getRequestURI());
	realPath = realPath.substring(0, realPath.lastIndexOf(System.getProperty("file.separator"))); 
	String filePath = realPath + "/" + realurl;
	//String filePath = "C:"+"\\"+"src"+"\\"+"keycom"+"\\"+"kyorder"+"\\"+"part_list";

	File file = new File(filePath);
	int blen = (int)file.length();

	int[] sBox = new int[256];
	byte[] userkey = mykey.getBytes();

	if(userkey == null) return;

	int alen = userkey.length;
	if(alen == 0) return;

	int i;
	for(i = 0;i < 256;i++){
		sBox[i] = i;
	}

	int i1 = 0, i2 = 0, t, xorIndex;
	for(i = 0;i < 256;i++){
		i2 = ((userkey[i1] & 0xFF) + sBox[i] + i2) & 0xFF;
		t = sBox[i];
		sBox[i] = sBox[i2];
		sBox[i2] = t;

		i1 = (i1 + 1) % alen;
	}


	response.reset();
	response.setHeader("Content-Transfer-Encoding", "binary;");
	response.setHeader("Pragma", "no-cache;");
	response.setHeader("Expires", "-1;");
	response.setContentLength(blen);

	//byte b[] = new byte[4062];
	byte b[] = new byte[blen];

	if(file.exists() && file.isFile()){
		BufferedInputStream fin = new BufferedInputStream(new FileInputStream(file));
		BufferedOutputStream outs = new BufferedOutputStream(response.getOutputStream());

		/*
		int read = 0;
		while ((read = fin.read(b)) != -1){
			outs.write(b, 0, read);
		}
		*/

		// Read in the bytes 
		int offset = 0;
		int numRead = 0;
		while(offset < b.length && (numRead = fin.read(b, offset, b.length-offset)) >= 0){
			offset += numRead;
		}
		fin.close();

		byte[] bout = new byte[blen];

		int x = 0, y = 0;
		int inOffset = 0, outOffset = 0;

		for(i = 0;i < blen;i++){
			x = (x + 1) & 0xFF;
			y = (sBox[x] + y) & 0xFF;

			t = sBox[x];
			sBox[x] = sBox[y];
			sBox[y] = t;

			xorIndex = (sBox[x] + sBox[y]) & 0xFF;
			bout[outOffset++] = (byte)(b[inOffset++] ^ sBox[xorIndex]);
		}

		outs.write(bout);
		outs.close();
	}
%>
