<%@ page import="java.io.*, java.util.*, java.net.*, org.w3c.dom.*, javax.xml.parsers.*, java.util.regex.*, javax.mail.*, javax.mail.internet.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%@ include file="../inc_main.jsp" %>
<%@ include file="inc_cata.jsp" %>
<%@ include file="inc_util.jsp" %>
<%@ include file="../send/mailcont.jsp" %>
<%
	catimage = get_param1(request.getParameter("catimage"),5);
	Dir = get_param1(request.getParameter("Dir"),5);
	if(Dir.equals("")) Dir = "1";

	String start = check_start(request.getParameter("start"));
	String callmode = check_callmode(request.getParameter("callmode"));
	String eclang = check_eclang(request.getParameter("eclang"));
	String userCate = check_cate(request.getParameter("Cate"));
	String um = check_um(request.getParameter("um"),"um");

	pathOfOriCata 	= webSysDir + "/catImage";
	pathOfCata 		= webSysDir + "/catImage" + catimage;
	pathOfDir 		= pathOfCata + "/" + Dir;

	String webCataOriPath	= webServer+"/catImage";
	String webCataPath 	= webServer+"/catImage"+catimage;
	String webDirPath 	= webServer+"/catImage"+catimage+"/"+Dir;
	String webSkinPath 	= webServer+"/skin";
	String selfName		= "emailact.jsp";
	String incView 		= "c";
	String catadocu		= "ecatalog"+um+".jsp";

	String linkwoDirUrl = "callmode="+callmode+"&catimage="+catimage+"&eclang="+eclang+"&um="+um;
	String linkwDirUrl  = linkwoDirUrl+"&Dir="+Dir;
	String linkwPageUrl = linkwDirUrl+"&start="+start;

	File f = new File(pathOfCata + "/catakind.txt");						// check category configuration file
	if(!f.exists()){
		out.print(echo_text("페이지를 표시할 수 없습니다.(catakind)"));
		return;
	}

	if(callmode.equals("normal_unify") || callmode.equals("admin_unify")) unifyCall = "true";

	HashMap CataKind = new HashMap();
	if(!userCate.equals("")){					// must find the first cover page out of the catalogs
		set_cateCond(userCate, CataKind);
		pathOfDir = pathOfCata + "/" + Dir;
		webDirPath = webServer+"/catImage"+catimage+"/"+Dir;
		linkwDirUrl  = linkwoDirUrl+"&Dir="+Dir;
	}
	else{
		String resstr = set_dirCond(CataKind, unifyCall);
		if(!resstr.equals("")){
			out.print(echo_text(resstr));
			return;
		}
	}

	if(!CataKind.get("unifyCond").equals("U")) unifyCall = "false";

	String parentCode = (String)CataKind.get("parentCode");
	if(parentCode.equals("")){										// exists parent category?
		out.print(echo_text("페이지를 표시할 수 없습니다.(parentCode)"));
		return;
	}
	f = new File(pathOfDir + "/ecatalog.txt");
	if(!f.exists()){				// check catalog configuration file
		out.print(echo_text("페이지를 표시할 수 없습니다.(ecatalog)"));
		return;
	}
	if(!callmode.equals("admin") && !callmode.equals("admin_unify") && (CataKind.get("svcCond1").equals("D") || CataKind.get("svcCond2").equals("D"))){
		out.print(echo_text("현재는 서비스가 불가합니다."));
		return;
	}

	HashMap Category = new HashMap();
	set_category(pathOfCata+"/category.txt", (String)CataKind.get("parentCode"), Category);

	// for moving from category to category
	String moveInDist = "";
	f = new File(pathOfOriCata+"/catimage.txt");
	if(unifyCall.equals("true") && f.exists()){
		moveInDist = get_catimage(catimage, "moveInDist");
	}

	// *** ecatalog.txt - check whether the total application of anyone folder's configuration exists
	boolean jbool = false;
	String bascadapt = (String)Category.get("bascadapt");
	if(bascadapt == null) bascadapt = "";
	if(unifyCall.equals("false")){
		if(!bascadapt.equals("")){
			f = new File(pathOfCata + "/" + bascadapt + "/ecatalog.txt");
			if(f.exists()) jbool = true;
		}
	}

	HashMap Ecatalog = new HashMap();
	if(jbool == true){
		set_ecatalog(pathOfCata+"/"+bascadapt+"/ecatalog.txt", Ecatalog);
		Ecatalog.put("pageno", get_ecatalogKey(pathOfDir + "/ecatalog.txt", "pageno"));
	}
	else{
		set_ecatalog(pathOfDir+"/ecatalog.txt", Ecatalog);
	}

	String ini2var = (String)Ecatalog.get("ini2var");
	String xmlExt = (ini2var.length() > 15 && ini2var.charAt(15) == 'T') ? "xml.txt" : ".xml";
	String ini1var = (String)Ecatalog.get("ini1var");

	String cook_cpasswd = "";
	Cookie[] cookies = request.getCookies();
	if(cookies != null){
		for(int i = 0;i < cookies.length;i++){
			Cookie theCookie = cookies[i];
			if(theCookie.getName().equals("cook_cpasswd")) cook_cpasswd = theCookie.getValue();
		}
	}

	String catpasswd = (String)Ecatalog.get("catpasswd");
	if(catpasswd == null) catpasswd = "";
	if(!catpasswd.equals("") && (cook_cpasswd.equals("") || !cook_cpasswd.equals(catpasswd))){				// cat password
		response.sendRedirect("../ecatalog_cpasswd.jsp?"+linkwPageUrl+"&docu=mail");
		return;
	}

	String logoadapt = (String)Category.get("logoadapt");

	// logo
	String logoFile = "";
	if(!logoadapt.equals("")){
		String file = get_logoFile(pathOfCata+"/"+logoadapt, "splogo");
		if(!file.equals("")) logoFile = "catImage"+catimage+"/"+logoadapt+"/"+file;
	}
	if(logoFile.equals("")){
		String file = get_logoFile(pathOfDir,"splogo");
		if(!file.equals("")) logoFile = "catImage"+catimage+"/"+Dir+"/"+file;
	}
	if(logoFile.equals("")){
		String file = get_logoFile(pathOfOriCata,"splogo");
		if(!file.equals("")) logoFile = "catImage/"+file;
	}

	HashMap Skin = new HashMap();
	set_skin(um, Skin, (String)Ecatalog.get("askin"), (String)Ecatalog.get("bskin"), Category);

	String display = (String)Skin.get("display");
	String bodyskin = get_bodyskin(display);

	if(Ecatalog.get("titlebar") == null || Ecatalog.get("titlebar").equals("")) Ecatalog.put("titlebar","Electronic Catalog(전자 카탈로그) - 메일 발송 결과");

	String default_font = "굴림";
	String mainTitle = "메일 발송 결과";
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><%=Ecatalog.get("titlebar")%> </title>
<link rel="stylesheet" type="text/css" href="<%=webServer%>/access/style.css">
</head>

<body class="body<%=display%>">

<%
	String dispvar = "NNNNNNNNNNNNNNNNNNNNN";
	String displayFile = "./inc_display"+display+".jsp";
%>
<jsp:include page="<%=displayFile%>" flush="true">
	<jsp:param name="incview" value="<%=incView%>"/>
	<jsp:param name="callmode" value="<%=callmode%>"/>
	<jsp:param name="webServer" value="<%=webServer%>"/>
	<jsp:param name="Dir" value="<%=Dir%>"/>
	<jsp:param name="catimage" value="<%=catimage%>"/>
	<jsp:param name="selfName" value="<%=selfName%>"/>
	<jsp:param name="xmlExt" value=""/>
	<jsp:param name="combokind" value=""/>
	<jsp:param name="currCode" value=""/>

	<jsp:param name="showPage" value=""/>
	<jsp:param name="cataPages" value="<%=cataPages%>"/>
	<jsp:param name="firstPage" value="<%=firstPage%>"/>
	<jsp:param name="lastPage" value="<%=lastPage%>"/>
	<jsp:param name="currentPage" value="0"/>
	<jsp:param name="showstart" value="<%=showstart%>"/>
	<jsp:param name="ini1var" value="<%=ini1var%>"/>
	<jsp:param name="ini2var" value="<%=ini2var%>"/>
	<jsp:param name="dispvar" value="<%=dispvar%>"/>

	<jsp:param name="dirName" value=""/>
	<jsp:param name="logoFile" value=""/>
	<jsp:param name="logolink" value=""/>
	<jsp:param name="downfile" value=""/>
	<jsp:param name="downtext" value=""/>
	<jsp:param name="linkwPageUrl" value="<%=linkwPageUrl%>"/>
	<jsp:param name="linkwDirUrl" value="<%=linkwDirUrl%>"/>

	<jsp:param name="leftImageName" value=""/>
	<jsp:param name="leftImageFile" value=""/>
	<jsp:param name="leftBigImgUrl" value=""/>
	<jsp:param name="rightImageName" value=""/>
	<jsp:param name="rightImageFile" value=""/>
	<jsp:param name="rightBigImgUrl" value=""/>

	<jsp:param name="mainTitle" value="<%=URLEncoder.encode(mainTitle,\"utf-8\")%>"/>
	<jsp:param name="catadocu" value="<%=catadocu%>"/>
</jsp:include>


<div id="mainsect" class="main001">
<%
	String mailDir = get_param1(request.getParameter("mailDir"),5);
	String logoDir = get_param1(request.getParameter("logoDir"),5);

	String path = "";
	if(!mailDir.equals("")){
		f = new File(pathOfCata + "/" + mailDir + "/mail.txt");
		if(f.exists()) path = pathOfCata + "/" + mailDir + "/mail.txt";
	}
	if(path.equals("")){
		f = new File(pathOfCata + "/" + Dir + "/mail.txt");
		if(f.exists()) path = pathOfCata + "/" + Dir + "/mail.txt";
	}
	if(path.equals("")) path = pathOfOriCata + "/mail.txt";

	int j = 0;
	String adminCont = "";
	String copyCont = "";
	f = new File(path);
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));

		String source = "";
		while((source = bin.readLine()) != null){
			if(source.equals("")) j = 1;
			else if(j == 1) adminCont += source + "<br>\n";
			else if(source.substring(0,10).equals("copyright:")) copyCont = source.substring(11);
		}
		fin.close();
		bin.close();
	}
	else{
		out.println("<p class='pmsg c"+display+"'>메일을 보낼 수 없습니다(기본설정).</p>");
		return;
	}

	String fromwho = get_qtxt1(request.getParameter("fromwho"),100);		// except for ()_-
	String frommail = request.getParameter("frommail");
	if(frommail == null) frommail = "";
	if(frommail.equals("") || check_email(frommail) == false){
		out.println("<p class='pmsg c"+display+"'>'보내는 사람 이메일' 형식이 맞지 않습니다.</p>");
		return;	
	}

	String towho = get_qtxt1(request.getParameter("towho"),100);		// except for ()_-
	String tomail = request.getParameter("tomail");
	if(tomail == null) tomail = "";
	if(tomail.equals("") || check_email(tomail) == false){
		out.println("<p class='pmsg c"+display+"'>'받는 사람 이메일' 형식이 맞지 않습니다.</p>");
		return;	
	}

	String subject = get_qtxt2(request.getParameter("subject"),200);	// strip tags
	if(subject.equals("")){
		out.println("<p class='pmsg c"+display+"'>'제목'이 없습니다.</p>");
		return;
	}

	String content = get_qtxt2(request.getParameter("content"),2000);	// strip tags

	String malogoFile = "";
	f = new File(pathOfCata + "/" + logoDir);
	if(!logoDir.equals("") && f.exists()){
		File[] folders = f.listFiles();
		for(int i = 0;i < folders.length;i++){
			String temp = folders[i].getName();
			if(temp.length() < 6 || temp.substring(0,1).equals(".")) continue;
			if(temp.substring(0,6).equals("malogo")){
				malogoFile = "catImage" + catimage + "/" + logoDir + "/" + temp;
				break;
			}
		}
	}

	f = new File(pathOfCata + "/" + Dir);
	if(malogoFile.equals("") && f.exists()){
		File[] afolders = f.listFiles();
		for(int i = 0;i < afolders.length;i++){
			String temp = afolders[i].getName();
			if(temp.length() < 6 || temp.substring(0,1).equals(".")) continue;
			if(temp.substring(0,6).equals("malogo")){
				malogoFile = "catImage" + catimage + "/" + Dir + "/" + temp;
				break;
			}
		}
	}

	if(malogoFile.equals("")){
		f = new File(pathOfOriCata);
		File[] bfolders = f.listFiles();
		for(int i = 0;i < bfolders.length;i++){
			String temp = bfolders[i].getName();
			if(temp.equals(".") || temp.equals("..")) continue;
			if(temp.length() > 6 && temp.substring(0,6).equals("malogo")){
				malogoFile = "catImage/" + temp;
				break;
			}
		}
	}

	//String smtpHost = "127.0.0.1";
	//String smtpPort = "25";
	String entire_server = (request.getServerPort() == 80) ? request.getServerName() : request.getServerName() + ":" + request.getServerPort();
	String linkDir = "http://" + entire_server + webServer;
	String userCont = content.replaceAll("\n","<br>");
	String linkFactor = Dir + "&catimage=" + catimage;
	String Mcont = get_mailBody(linkFactor, linkDir, malogoFile, userCont, adminCont, copyCont);

	Properties props = new Properties();
	//props.put("mail.smtp.host", smtpHost); 
	//props.put("mail.smtp.port", smtpPort);
	Session msgSession = Session.getDefaultInstance(props, null);
	MimeMessage msg = new MimeMessage(msgSession);

	String fromx = fromwho + " <" + frommail + ">";
	//fromx = new String(fromx.getBytes("utf-8"),"8859_1");
	InternetAddress from = new InternetAddress(fromx);
	msg.setFrom(from);

	String tox = towho + " <" + tomail + ">";
	//tox = new String(tox.getBytes("utf-8"),"8859_1");
	InternetAddress to = new InternetAddress(tox);
	msg.setRecipient(Message.RecipientType.TO, to);

	msg.setSubject(subject);
	msg.setContent(Mcont, "text/html; charset=utf-8");

	Transport.send(msg);
	out.print("<p class='pmsg c"+display+"'>메일 발송을 완료하였습니다.</p>");
%>
</div>

</body>
</html>
