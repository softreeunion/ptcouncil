<%!
	public String get_showString(int page){
		if(onesmc.equals("true")) return get_pageFromShowkind(page);

		if(page == 0) return get_pageFromShowkind(page+1);
		else if(page == cataPages) return get_pageFromShowkind(page);
		return get_pageFromShowkind(page)+"-"+get_pageFromShowkind(page+1);
	}

	public String get_pageFromShowkind(int page) {
		int showpage = 0;
		if(showstart > 0){
			showpage = get_showPageFromReal(page);
			if(showpage < 1) return "";
		}
		else showpage = page;

		if(showpage > cataPages) showpage = cataPages;

		return ""+showpage;
	}

	public int get_realPageFromShow(int page) {
		return (page + showstart - 1);
	}
	public int get_showPageFromReal(int page) {
		int n = page - showstart + 1;
		if(n <= 0) return 0;
		return (page - showstart + 1);
	}
	public String get_showPageFromReal2(int page) {
		if(showstart == 0) return ""+page;
		int n = page - showstart + 1;
		if(n <= 0) return "";
		return ""+n;
	}

	public String get_fileExtension(String s) {			// file extension of server image
		if(s.toLowerCase().equals("j")) return "jpg";
		else if(s.toLowerCase().equals("p")) return "png";
		else if(s.toLowerCase().equals("s")) return "swf";
		else if(s.toLowerCase().equals("g")) return "gif";
		return "jpg";
	}

	public String getFileName(int n) {
		return digit3(n)+"."+fileExt;
	}

	public String digit3(int n) {			// int to 3 characters
		if(n < 10) return "00"+n;
		else if(n < 100) return "0"+n;
		return ""+n;
	}

	// checking
	public boolean check_indexFile(String path){
		File f = new File(path+"/mokcha.txt");
		if(f.exists() && f.length() > 0) return true;
		return false;
	}

	public boolean check_searchFile(String method, String path){
		if(method.equals("F")){
			File f = new File(path+"/search.txt");
			return (f.exists() && f.length() > 0) ? true : false;
		}
		else if(method.equals("A")){
			File f = new File(path+"/search.xml");
			return (f.exists() && f.length() > 0) ? true : false;
		}
		return false;
	}

	public boolean check_downFile(String downfile){
		if(downfile.equals("")) return false;
		File f = new File(pathOfDir+"/"+downfile);
		return f.exists() ? true : false;
	}

	// search
	public String[] getAlterPage(String path, int npage){
		String[] arPage = {""+npage, ""+(npage+1)};
		String[] searchStr = {"",""};

		String source = "";
		File f = new File(path);
		StringBuffer cont = new StringBuffer();
		FileInputStream fin;
		BufferedReader bin;
		try{
			fin = new FileInputStream(f);
		 	bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				//source = strTrimEach(source);
				if(source.equals("")) continue;
				cont.append(source).append("\n");
			}
			fin.close();
			bin.close();
		}
		catch(Exception e) {
			return searchStr;
		}

		Pattern p = Pattern.compile("<no>(.*)</no>([ \t\r\n]+)<search><!\\[CDATA\\[(.*)\\]\\]></search>");
		Matcher m = p.matcher(cont.toString());
		while(m.find()){
			String pageno = m.group(1);
			String qstr = m.group(3);

			if(pageno.equals(arPage[0])) searchStr[0] = qstr;
			else if(pageno.equals(arPage[1])) searchStr[1] = qstr;
		}
		return searchStr;
	}

	public String get_comboString(String currCode){
		int cateStep = get_cateStep(currCode);
		int plen = cateStep*2 - 2;
		String pnCode = currCode.substring(0,plen);

		String source = "";
		File f = new File(pathOfCata + "/catakind.txt");
		StringBuffer sbuf = new StringBuffer();
		FileInputStream fin;
		BufferedReader bin;
		try{
			fin = new FileInputStream(f);
			bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				String[] aaa = mysplit(source, "|");		// cate|setting|name|etc
				String t_cate = aaa[0];

				if(t_cate.substring(0,plen).equals(pnCode) && !t_cate.substring(plen,plen+2).equals("00")){
					if(aaa[1].substring(0,1).equals("A") && !aaa[1].substring(3,4).equals("0")){
						sbuf.append("<option value='" + aaa[3] + "'>" + aaa[2] + "</option>\n");
					}
				}
			}
			fin.close();
			bin.close();
		}
		catch(Exception e) {
			sbuf.append("<option value=''>can't fulfill getComboString()!</option>\n");
		}

		return sbuf.toString();
	}

	public String get_comboStringAll(String currCode){
		File f = new File(pathOfCata + "/catakind.txt");

		String source = "";
		StringBuffer sbuf = new StringBuffer();
		FileInputStream fin;
		BufferedReader bin;
		try{
			Hashtable hash = new Hashtable();

			int alen = 0;
			int i = 0;
			fin = new FileInputStream(f);
			bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				String[] aaa = mysplit(source, "|");		// cate|setting|name|etc

				String t_cate = aaa[0];
				hash.put("cateCond" + t_cate, aaa[1]);
				hash.put("cateName" + t_cate, aaa[2]);
				hash.put("cateDir"  + t_cate, aaa[3]);

				if(t_cate.substring(2,10).equals("00000000")){
					hash.put("mainCate" + i, t_cate);
					i++;
				}
				else{
					String t_cate2 = "";
					if(t_cate.substring(4,10).equals("000000")) t_cate2 = t_cate.substring(0,2) + "00000000";

					String t_temp2 = hash.containsKey("linkCate" + t_cate2) ? (String)hash.get("linkCate" + t_cate2) : null;
					if(t_temp2 == null) hash.put("linkCate" + t_cate2, t_cate);
					else hash.put("linkCate" + t_cate2, t_temp2 + "|" + t_cate);
				}
				alen = i;
			}
			fin.close();
			bin.close();

			for(i = 0;i < alen;i++){
				String t_cate = hash.containsKey("mainCate" + i) ? (String)hash.get("mainCate" + i) : "";
				String t_name = hash.containsKey("cateName" + t_cate) ? (String)hash.get("cateName" + t_cate) : "";
				sbuf.append("<optgroup label='" + t_name + "'>\n");

				String t_temp = hash.containsKey("linkCate" + t_cate) ? (String)hash.get("linkCate" + t_cate) : "";
				if(t_temp == null) continue;

				StringTokenizer st = new StringTokenizer(t_temp, "|");
				Vector v = new Vector();
				while(st.hasMoreTokens()){
					v.addElement(st.nextToken());
				}

				int ken = v.size();
				for(int k = 0;k < ken;k++){
					String t_cate2 = (String)v.elementAt(k);
					String t_cond2 = hash.containsKey("cateCond" + t_cate2) ? (String)hash.get("cateCond" + t_cate2) : "";
					String t_skd2 = "";
					if(!t_cond2.equals("")) t_skd2 = t_cond2.substring(3,4);

					if(t_skd2.equals("0")){
						String t_temp2 = hash.containsKey("linkCate" + t_cate2) ? (String)hash.get("linkCate" + t_cate2) : "";
						if(t_temp2 == null || t_temp2.equals("")) continue;

						String t_name2 = hash.containsKey("cateName" + t_cate2) ? (String)hash.get("cateName" + t_cate2) : "";
						sbuf.append("<optgroup label='" + t_name2 + "'>\n");

						StringTokenizer st1 = new StringTokenizer(t_temp2, "|");
						Vector v1 = new Vector();
						while(st1.hasMoreTokens()){
							v1.addElement(st1.nextToken());
						}

						int hen = v1.size();
						for(int h = 0;h < hen;h++){
							String lesc = "";
							String t_cate3 = (String)v1.elementAt(h);

							String t_temp3 = hash.containsKey("cateCond" + t_cate3) ? (String)hash.get("cateCond" + t_cate3) : "";
							if(t_temp3.substring(0,1).equals("D")) continue;

							t_temp3 = hash.containsKey("cateDir" + t_cate3) ? (String)hash.get("cateDir" + t_cate3) : "";
							sbuf.append("<option value='").append(t_cate3).append("' ").append(lesc).append(">");
							t_temp3 = hash.containsKey("cateName" + t_cate3) ? (String)hash.get("cateName" + t_cate3) : "";
							sbuf.append(t_temp3).append("</option>\n");
						}

						sbuf.append("</optgroup>\n");
					}
				}
				sbuf.append("</optgroup>\n");
			}
		}
		catch(Exception e) {
			sbuf.append("<option value=''>can't fulfill getComboString()!</option>\n");
		}

		return sbuf.toString();
	}

	public String get_indexString(){
		File f = new File(pathOfDir + "/mokcha.txt");

		String source = "";
		StringBuffer sbuf = new StringBuffer("");
		FileInputStream fin;
		BufferedReader bin;

		try{
			fin = new FileInputStream(f);
			bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("") || source.equals("nextline")) continue;

				int in = source.indexOf(":");
				if(in == -1) continue;

				String skey = source.substring(0, in);
				String sval = source.substring(in+1);

				if(skey.substring(0,1).equals("-")) skey = skey.substring(1);			// 1 class
				sbuf.append("<option value='" + skey + "'>" + sval + "</option>\n");
			}
			fin.close();
			bin.close();
		}
		catch(Exception e) {
			return "";
		}

		return sbuf.toString();
	}

	public int arraySearch(String subb, String[] ar){
		int kx = -1;
		for(int i = 0;i < ar.length;i++){
			if(ar[i] == null) continue;
			if(ar[i].equals(subb)){
				kx = i;
				break;
			}
		}
		return kx;
	}

	public int[] db_board_start(int myrow_total, int Punit, String Pg){
		int[] subar = new int[4];
		subar[3] = (int)((myrow_total-1)/Punit) + 1;

		if(Pg == null || Pg.equals("")) subar[2] = 1;
		else subar[2] = Integer.parseInt(Pg);

		subar[0] = (subar[2]-1) * Punit;
		subar[1] = (subar[2] == subar[3]) ? myrow_total : subar[0] + Punit;

		return subar;
	}
%>
