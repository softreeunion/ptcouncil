<%@ page import="java.io.*, java.util.*, java.net.*, org.w3c.dom.*, javax.xml.parsers.*, java.util.regex.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%@ include file="../inc_main.jsp" %>
<%@ include file="inc_cata.jsp" %>
<%@ include file="inc_util.jsp" %>
<%
	catimage = get_param1(request.getParameter("catimage"),5);
	Dir = get_param1(request.getParameter("Dir"),5);
	if(Dir.equals("")) Dir = "1";

	String start = check_start(request.getParameter("start"));
	String callmode = check_callmode(request.getParameter("callmode"));
	String eclang = check_eclang(request.getParameter("eclang"));
	String userCate = check_cate(request.getParameter("Cate"));
	String um = check_um(request.getParameter("um"),"um");

	pathOfOriCata 	= webSysDir + "/catImage";
	pathOfCata 		= webSysDir + "/catImage" + catimage;
	pathOfDir 		= pathOfCata + "/" + Dir;

	String webCataOriPath	= webServer+"/catImage";
	String webCataPath 	= webServer+"/catImage"+catimage;
	String webDirPath 	= webServer+"/catImage"+catimage+"/"+Dir;
	String webSkinPath 	= webServer+"/skin";
	String selfName		= "combo.jsp";
	String incView 		= "c";
	String catadocu		= "ecatalog"+um+".jsp";

	String linkwoDirUrl = "callmode="+callmode+"&catimage="+catimage+"&eclang="+eclang+"&um="+um;
	String linkwDirUrl  = linkwoDirUrl+"&Dir="+Dir;
	String linkwPageUrl = linkwDirUrl+"&start="+start;

	File f = new File(pathOfCata + "/catakind.txt");						// check category configuration file
	if(!f.exists()){
		out.print(echo_text("페이지를 표시할 수 없습니다.(catakind)"));
		return;
	}

	if(callmode.equals("normal_unify") || callmode.equals("admin_unify")) unifyCall = "true";

	HashMap CataKind = new HashMap();
	if(!userCate.equals("")){					// must find the first cover page out of the catalogs
		set_cateCond(userCate, CataKind);
		pathOfDir = pathOfCata + "/" + Dir;
		webDirPath = webServer+"/catImage"+catimage+"/"+Dir;
		linkwDirUrl  = linkwoDirUrl+"&Dir="+Dir;
	}
	else{
		String resstr = set_dirCond(CataKind, unifyCall);
		if(!resstr.equals("")){
			out.print(echo_text(resstr));
			return;
		}
	}

	if(!CataKind.get("unifyCond").equals("U")) unifyCall = "false";

	String parentCode = (String)CataKind.get("parentCode");
	if(parentCode.equals("")){										// exists parent category?
		out.print(echo_text("페이지를 표시할 수 없습니다.(parentCode)"));
		return;
	}
	f = new File(pathOfDir + "/ecatalog.txt");
	if(!f.exists()){				// check catalog configuration file
		out.print(echo_text("페이지를 표시할 수 없습니다.(ecatalog)"));
		return;
	}
	if(!callmode.equals("admin") && !callmode.equals("admin_unify") && (CataKind.get("svcCond1").equals("D") || CataKind.get("svcCond2").equals("D"))){
		out.print(echo_text("현재는 서비스가 불가합니다."));
		return;
	}

	HashMap Category = new HashMap();
	set_category(pathOfCata+"/category.txt", (String)CataKind.get("parentCode"), Category);

	// for moving from category to category
	String moveInDist = "";
	f = new File(pathOfOriCata+"/catimage.txt");
	if(unifyCall.equals("true") && f.exists()){
		moveInDist = get_catimage(catimage, "moveInDist");
	}

	// *** ecatalog.txt - check whether the total application of anyone folder's configuration exists
	boolean jbool = false;
	String bascadapt = (String)Category.get("bascadapt");
	if(bascadapt == null) bascadapt = "";
	if(unifyCall.equals("false")){
		if(!bascadapt.equals("")){
			f = new File(pathOfCata + "/" + bascadapt + "/ecatalog.txt");
			if(f.exists()) jbool = true;
		}
	}

	HashMap Ecatalog = new HashMap();
	if(jbool == true){
		set_ecatalog(pathOfCata+"/"+bascadapt+"/ecatalog.txt", Ecatalog);
		Ecatalog.put("pageno", get_ecatalogKey(pathOfDir + "/ecatalog.txt", "pageno"));
	}
	else{
		set_ecatalog(pathOfDir+"/ecatalog.txt", Ecatalog);
	}

	String ini2var = (String)Ecatalog.get("ini2var");
	String xmlExt = (ini2var.length() > 15 && ini2var.charAt(15) == 'T') ? "xml.txt" : ".xml";
	String ini1var = (String)Ecatalog.get("ini1var");

	String cook_cpasswd = "";
	Cookie[] cookies = request.getCookies();
	if(cookies != null){
		for(int i = 0;i < cookies.length;i++){
			Cookie theCookie = cookies[i];
			if(theCookie.getName().equals("cook_cpasswd")) cook_cpasswd = theCookie.getValue();
		}
	}

	String catpasswd = (String)Ecatalog.get("catpasswd");
	if(catpasswd == null) catpasswd = "";
	if(!catpasswd.equals("") && (cook_cpasswd.equals("") || !cook_cpasswd.equals(catpasswd))){				// cat password
		response.sendRedirect("../ecatalog_cpasswd.jsp?"+linkwPageUrl+"&docu=comb");
		return;
	}

	String logoadapt = (String)Category.get("logoadapt");

	// logo
	String logoFile = "";
	if(!logoadapt.equals("")){
		String file = get_logoFile(pathOfCata+"/"+logoadapt, "splogo");
		if(!file.equals("")) logoFile = "catImage"+catimage+"/"+logoadapt+"/"+file;
	}
	if(logoFile.equals("")){
		String file = get_logoFile(pathOfDir,"splogo");
		if(!file.equals("")) logoFile = "catImage"+catimage+"/"+Dir+"/"+file;
	}
	if(logoFile.equals("")){
		String file = get_logoFile(pathOfOriCata,"splogo");
		if(!file.equals("")) logoFile = "catImage/"+file;
	}

	HashMap Display = new HashMap();
	Display.put("user", get_userSkin((String)Ecatalog.get("askin"), incView));

	HashMap Skin = new HashMap();
	set_skin(um, Skin, (String)Ecatalog.get("askin"), (String)Ecatalog.get("bskin"), Category, Display);

	String display = (String)Skin.get("display");
	String bodyskin = get_bodyskin(display);

	fileExt = get_fileExtension(ini2var.substring(2,3));

	// page variables
	if(!Ecatalog.get("showstart").equals("")) showstart = Integer.parseInt((String)Ecatalog.get("showstart"));
	cataPages = Integer.parseInt((String)Ecatalog.get("pageno"));
	firstPage = 1;
	lastPage = cataPages;

	CataDoc catadoc = new CataDoc();
	catadoc.set_fileseq(ini1var.charAt(11), xmlExt);
	if(unifyCall.equals("true")) catadoc.set_unifyVar((String)Category.get("unif"));

	if(Ecatalog.get("titlebar") == null || Ecatalog.get("titlebar").equals("")) Ecatalog.put("titlebar","Electronic Catalog(전자 카탈로그) - 이동");

	String mainTitle = "다른 카탈로그";
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=10.0, user-scalable=yes">
<title><%=Ecatalog.get("titlebar")%> </title>
<link rel="stylesheet" type="text/css" href="<%=webServer%>/access/style.css">
<style>
	*{font-size:13px;}
</style>
</head>

<body class="body<%=bodyskin%>">

<%
	String dispvar = "NNNNNNNNNNNNNNNNNNNNN";
	String displayFile = "./inc_display"+display+".jsp";
%>
<jsp:include page="<%=displayFile%>" flush="true">
	<jsp:param name="incview" value="<%=incView%>"/>
	<jsp:param name="callmode" value="<%=callmode%>"/>
	<jsp:param name="webServer" value="<%=webServer%>"/>
	<jsp:param name="Dir" value="<%=Dir%>"/>
	<jsp:param name="catimage" value="<%=catimage%>"/>
	<jsp:param name="selfName" value="<%=selfName%>"/>
	<jsp:param name="xmlExt" value=""/>
	<jsp:param name="combokind" value=""/>
	<jsp:param name="currCode" value=""/>

	<jsp:param name="showPage" value=""/>
	<jsp:param name="cataPages" value="<%=cataPages%>"/>
	<jsp:param name="firstPage" value="<%=firstPage%>"/>
	<jsp:param name="lastPage" value="<%=lastPage%>"/>
	<jsp:param name="currentPage" value="0"/>
	<jsp:param name="showstart" value="<%=showstart%>"/>
	<jsp:param name="ini1var" value="<%=ini1var%>"/>
	<jsp:param name="ini2var" value="<%=ini2var%>"/>
	<jsp:param name="dispvar" value="<%=dispvar%>"/>

	<jsp:param name="dirName" value=""/>
	<jsp:param name="logoFile" value=""/>
	<jsp:param name="logolink" value=""/>
	<jsp:param name="downfile" value=""/>
	<jsp:param name="downtext" value=""/>
	<jsp:param name="linkwPageUrl" value="<%=linkwPageUrl%>"/>
	<jsp:param name="linkwDirUrl" value="<%=linkwDirUrl%>"/>

	<jsp:param name="leftImageName" value=""/>
	<jsp:param name="leftImageFile" value=""/>
	<jsp:param name="leftBigImgUrl" value=""/>
	<jsp:param name="rightImageName" value=""/>
	<jsp:param name="rightImageFile" value=""/>
	<jsp:param name="rightBigImgUrl" value=""/>

	<jsp:param name="mainTitle" value="<%=URLEncoder.encode(mainTitle,\"utf-8\")%>"/>
	<jsp:param name="catadocu" value="<%=catadocu%>"/>
</jsp:include>

<div id="mainsect" class="main002">
<ul class="contlist<%=bodyskin%>">
<%
	String currCode = (String)CataKind.get("cateCode");
	int tabindex = 3;
	if(Skin.get("combokind").equals("001")){
		int cateStep = get_cateStep(currCode);
		int plen = cateStep*2 - 2;
		String pnCode = currCode.substring(0,plen);

		StringBuffer sbuf = new StringBuffer();
		try{
			String source = "";
			f = new File(pathOfCata + "/catakind.txt");
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				String[] aaa = mysplit(source, "|");		// cate|setting|name|etc
				String t_cate = aaa[0];

				if(t_cate.substring(0,plen).equals(pnCode) && !t_cate.substring(plen,plen+2).equals("00") && !aaa[1].substring(0,1).equals("D") && !aaa[1].substring(2,3).equals("0")){
					String href = catadocu + "?" + linkwoDirUrl + "&Dir=" + aaa[3];
					out.print("<li><p class='listsubj"+bodyskin+" liststep1'><a href='"+href+"' tabindex='"+tabindex+"' class='c"+display+"'>"+aaa[2]+"</p>\n");
					out.print("<p class='listmark"+bodyskin+"'><img src='"+webServer+"/access/pix/index_list.png'></p></li>");
					tabindex++;
				}
			}
			fin.close();
			bin.close();
		}
		catch(IOException e) {
			out.print("catakind.txt read error!(1)\n");
		}
	}
	else{
		String source = "";
		Hashtable hash = new Hashtable();

		int alen = 0;
		int i = 0;
		f = new File(pathOfCata + "/catakind.txt");
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			String[] aaa = mysplit(source, "|");		// cate|setting|name|etc
			String t_cate = aaa[0];
			hash.put("cateCond" + t_cate, aaa[1]);
			hash.put("cateName" + t_cate, aaa[2]);
			hash.put("cateDir"  + t_cate, aaa[3]);

			if(t_cate.substring(2,10).equals("00000000")){
				hash.put("mainCate" + i, t_cate);
				i++;
			}
			else{
				String t_cate2 = "";
				if(t_cate.substring(4,10).equals("000000")) t_cate2 = t_cate.substring(0,2) + "00000000";
				else if(t_cate.substring(6,10).equals("0000")) t_cate2 = t_cate.substring(0,4) + "000000";

				String t_temp2 = hash.containsKey("linkCate" + t_cate2) ? (String)hash.get("linkCate" + t_cate2) : null;
				if(t_temp2 == null) hash.put("linkCate" + t_cate2, t_cate);
				else hash.put("linkCate" + t_cate2, t_temp2 + "|" + t_cate);
			}
			alen = i;
		}
		fin.close();
		bin.close();

		for(i = 0;i < alen;i++){
			String t_cate = hash.containsKey("mainCate" + i) ? (String)hash.get("mainCate" + i) : "";
			String t_name = hash.containsKey("cateName" + t_cate) ? (String)hash.get("cateName" + t_cate) : "";

			String t_temp = hash.containsKey("linkCate" + t_cate) ? (String)hash.get("linkCate" + t_cate) : "";
			if(t_temp == null) continue;

			out.print("<li><p class='listsubj"+bodyskin+" liststep1'>"+t_name+"</p><p class='listmark"+bodyskin+"'></p></li>\n");
			tabindex++;

			StringTokenizer st = new StringTokenizer(t_temp, "|");
			Vector v = new Vector();
			while(st.hasMoreTokens()){
				v.addElement(st.nextToken());
			}

			int ken = v.size();
			for(int k = 0;k < ken;k++){
				String t_cate2 = (String)v.elementAt(k);
				String t_name2 = hash.containsKey("cateName" + t_cate2) ? (String)hash.get("cateName" + t_cate2) : "";
				String t_cond2 = hash.containsKey("cateCond" + t_cate2) ? (String)hash.get("cateCond" + t_cate2) : "";
				String t_dir2 = hash.containsKey("cateDir" + t_cate2) ? (String)hash.get("cateDir" + t_cate2) : "";
				String t_skd2 = "";
				if(!t_cond2.equals("")) t_skd2 = t_cond2.substring(2,3);

				if(t_skd2.equals("0")){
					String t_temp2 = hash.containsKey("linkCate" + t_cate2) ? (String)hash.get("linkCate" + t_cate2) : "";
					if(t_temp2 == null || t_name2 == null) continue;

					out.print("<li><p class='listsubj"+bodyskin+" liststep2'>"+t_name2+"</p><p class='listmark1'></p></li>\n");
					tabindex++;

					StringTokenizer st1 = new StringTokenizer(t_temp2, "|");
					Vector v1 = new Vector();
					while(st1.hasMoreTokens()){
						v1.addElement(st1.nextToken());
					}

					int jen = v1.size();
					for(int j = 0;j < jen;j++){
						String lesc = "";
						String t_cate3 = (String)v1.elementAt(j);
						String t_name3 = hash.containsKey("cateName" + t_cate3) ? (String)hash.get("cateName" + t_cate3) : "";
						String t_cond3 = hash.containsKey("cateCond" + t_cate3) ? (String)hash.get("cateCond" + t_cate3) : "";
						String t_dir3 = hash.containsKey("cateDir" + t_cate3) ? (String)hash.get("cateDir" + t_cate3) : "";
						String t_del3 = "";
						if(!t_cond2.equals("")) t_del3 = t_cond3.substring(0,1);
						if(t_del3.equals("D")) continue;

						String href = catadocu + "?" + linkwoDirUrl + "&Dir=" + t_dir3;

						out.print("<li><p class='listsubj"+bodyskin+" liststep3'><a href='"+href+"' tabindex='"+tabindex+"' ");
						out.print("class='c"+display+"'>"+t_name3+"</a></p>\n");
						out.print("<p class='listmark"+bodyskin+"'><img src='"+webServer+"/access/pix/index_list.png'></p></li>\n");
						tabindex++;
					}
				}
				else{
					String href = catadocu + "?" + linkwoDirUrl + "&Dir=" + t_dir2;
					out.print("<li><p class='listsubj"+bodyskin+" liststep2'><a href='"+href+"' tabindex='"+tabindex+"' ");
					out.print("class='c"+display+"'>"+t_name2+"</a></p>\n");
					out.print("<p class='listmark"+bodyskin+"'><img src='"+webServer+"/access/pix/index_list.png'></p></li>\n");
					tabindex++;
				}
			}
		}
	}
%>
</ul>
</div>

</body>
</html>
