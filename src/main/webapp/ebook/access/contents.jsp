<%@ page import="java.io.*, java.util.*, java.net.*, org.w3c.dom.*, javax.xml.parsers.*, java.util.regex.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%@ include file="../inc_main.jsp" %>
<%@ include file="inc_cata.jsp" %>
<%@ include file="inc_util.jsp" %>
<%
	catimage = get_param1(request.getParameter("catimage"),5);
	Dir = get_param1(request.getParameter("Dir"),5);
	if(Dir.equals("")) Dir = "1";

	String start = check_start(request.getParameter("start"));
	String callmode = check_callmode(request.getParameter("callmode"));
	String eclang = check_eclang(request.getParameter("eclang"));
	String userCate = check_cate(request.getParameter("Cate"));
	String um = check_um(request.getParameter("um"),"um");

	pathOfOriCata 	= webSysDir + "/catImage";
	pathOfCata 		= webSysDir + "/catImage" + catimage;
	pathOfDir 		= pathOfCata + "/" + Dir;

	String webCataOriPath	= webServer+"/catImage";
	String webCataPath 	= webServer+"/catImage"+catimage;
	String webDirPath 	= webServer+"/catImage"+catimage+"/"+Dir;
	String webSkinPath 	= webServer+"/skin";
	String selfName		= "contents.jsp";
	String incView 		= "c";
	String catadocu		= "ecatalog"+um+".jsp";

	String linkwoDirUrl = "callmode="+callmode+"&catimage="+catimage+"&eclang="+eclang+"&um="+um;
	String linkwDirUrl  = linkwoDirUrl+"&Dir="+Dir;
	String linkwPageUrl = linkwDirUrl+"&start="+start;

	File f = new File(pathOfCata + "/catakind.txt");						// check category configuration file
	if(!f.exists()){
		out.print(echo_text("페이지를 표시할 수 없습니다.(catakind)"));
		return;
	}

	if(callmode.equals("normal_unify") || callmode.equals("admin_unify")) unifyCall = "true";

	HashMap CataKind = new HashMap();
	if(!userCate.equals("")){					// must find the first cover page out of the catalogs
		set_cateCond(userCate, CataKind);
		pathOfDir = pathOfCata + "/" + Dir;
		webDirPath = webServer+"/catImage"+catimage+"/"+Dir;
		linkwDirUrl  = linkwoDirUrl+"&Dir="+Dir;
	}
	else{
		String resstr = set_dirCond(CataKind, unifyCall);
		if(!resstr.equals("")){
			out.print(echo_text(resstr));
			return;
		}
	}

	if(!CataKind.get("unifyCond").equals("U")) unifyCall = "false";

	String parentCode = (String)CataKind.get("parentCode");
	if(parentCode.equals("")){										// exists parent category?
		out.print(echo_text("페이지를 표시할 수 없습니다.(parentCode)"));
		return;
	}
	f = new File(pathOfDir + "/ecatalog.txt");
	if(!f.exists()){				// check catalog configuration file
		out.print(echo_text("페이지를 표시할 수 없습니다.(ecatalog)"));
		return;
	}
	if(!callmode.equals("admin") && !callmode.equals("admin_unify") && (CataKind.get("svcCond1").equals("D") || CataKind.get("svcCond2").equals("D"))){
		out.print(echo_text("현재는 서비스가 불가합니다."));
		return;
	}

	HashMap Category = new HashMap();
	set_category(pathOfCata+"/category.txt", (String)CataKind.get("parentCode"), Category);

	// for moving from category to category
	String moveInDist = "";
	f = new File(pathOfOriCata+"/catimage.txt");
	if(unifyCall.equals("true") && f.exists()){
		moveInDist = get_catimage(catimage, "moveInDist");
	}

	// *** ecatalog.txt - check whether the total application of anyone folder's configuration exists
	boolean jbool = false;
	String bascadapt = (String)Category.get("bascadapt");
	if(bascadapt == null) bascadapt = "";
	if(unifyCall.equals("false")){
		if(!bascadapt.equals("")){
			f = new File(pathOfCata + "/" + bascadapt + "/ecatalog.txt");
			if(f.exists()) jbool = true;
		}
	}

	HashMap Ecatalog = new HashMap();
	if(jbool == true){
		set_ecatalog(pathOfCata+"/"+bascadapt+"/ecatalog.txt", Ecatalog);
		Ecatalog.put("pageno", get_ecatalogKey(pathOfDir + "/ecatalog.txt", "pageno"));
	}
	else{
		set_ecatalog(pathOfDir+"/ecatalog.txt", Ecatalog);
	}

	String ini2var = (String)Ecatalog.get("ini2var");
	String xmlExt = (ini2var.length() > 15 && ini2var.charAt(15) == 'T') ? "xml.txt" : ".xml";
	String ini1var = (String)Ecatalog.get("ini1var");

	String cook_cpasswd = "";
	Cookie[] cookies = request.getCookies();
	if(cookies != null){
		for(int i = 0;i < cookies.length;i++){
			Cookie theCookie = cookies[i];
			if(theCookie.getName().equals("cook_cpasswd")) cook_cpasswd = theCookie.getValue();
		}
	}

	String catpasswd = (String)Ecatalog.get("catpasswd");
	if(catpasswd == null) catpasswd = "";
	if(!catpasswd.equals("") && (cook_cpasswd.equals("") || !cook_cpasswd.equals(catpasswd))){				// cat password
		response.sendRedirect("../ecatalog_cpasswd.jsp?"+linkwPageUrl+"&docu=cont");
		return;
	}

	String logoadapt = (String)Category.get("logoadapt");

	// logo
	String logoFile = "";
	if(!logoadapt.equals("")){
		String file = get_logoFile(pathOfCata+"/"+logoadapt, "splogo");
		if(!file.equals("")) logoFile = "catImage"+catimage+"/"+logoadapt+"/"+file;
	}
	if(logoFile.equals("")){
		String file = get_logoFile(pathOfDir,"splogo");
		if(!file.equals("")) logoFile = "catImage"+catimage+"/"+Dir+"/"+file;
	}
	if(logoFile.equals("")){
		String file = get_logoFile(pathOfOriCata,"splogo");
		if(!file.equals("")) logoFile = "catImage/"+file;
	}

	HashMap Display = new HashMap();
	Display.put("user", get_userSkin((String)Ecatalog.get("askin"), incView));

	HashMap Skin = new HashMap();
	set_skin(um, Skin, (String)Ecatalog.get("askin"), (String)Ecatalog.get("bskin"), Category, Display);

	String display = (String)Skin.get("display");
	String bodyskin = get_bodyskin(display);

	fileExt = get_fileExtension(ini2var.substring(2,3));

	// page variables
	if(!Ecatalog.get("showstart").equals("")) showstart = Integer.parseInt((String)Ecatalog.get("showstart"));
	cataPages = Integer.parseInt((String)Ecatalog.get("pageno"));
	firstPage = 1;
	lastPage = cataPages;

	CataDoc catadoc = new CataDoc();
	catadoc.set_fileseq(ini1var.charAt(11), xmlExt);
	if(unifyCall.equals("true")) catadoc.set_unifyVar((String)Category.get("unif"));

	if(Ecatalog.get("titlebar") == null || Ecatalog.get("titlebar").equals("")) Ecatalog.put("titlebar","Electronic Catalog(전자 카탈로그) - 목차");

	String mainTitle = "목차";
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=10.0, user-scalable=yes">
<title><%=Ecatalog.get("titlebar")%> </title>
<link rel="stylesheet" type="text/css" href="<%=webServer%>/access/style.css">
<style>
	*{font-size:13px;}
</style>
</head>

<body class="body<%=bodyskin%>">

<%
	String dispvar = "NNNNNNNNNNNNNNNNNNNNN";
	String displayFile = "./inc_display"+display+".jsp";
%>
<jsp:include page="<%=displayFile%>" flush="true">
	<jsp:param name="incview" value="<%=incView%>"/>
	<jsp:param name="callmode" value="<%=callmode%>"/>
	<jsp:param name="webServer" value="<%=webServer%>"/>
	<jsp:param name="Dir" value="<%=Dir%>"/>
	<jsp:param name="catimage" value="<%=catimage%>"/>
	<jsp:param name="selfName" value="<%=selfName%>"/>
	<jsp:param name="xmlExt" value=""/>
	<jsp:param name="combokind" value=""/>
	<jsp:param name="currCode" value=""/>

	<jsp:param name="showPage" value=""/>
	<jsp:param name="cataPages" value="<%=cataPages%>"/>
	<jsp:param name="firstPage" value="<%=firstPage%>"/>
	<jsp:param name="lastPage" value="<%=lastPage%>"/>
	<jsp:param name="currentPage" value="0"/>
	<jsp:param name="showstart" value="<%=showstart%>"/>
	<jsp:param name="ini1var" value="<%=ini1var%>"/>
	<jsp:param name="ini2var" value="<%=ini2var%>"/>
	<jsp:param name="dispvar" value="<%=dispvar%>"/>

	<jsp:param name="dirName" value=""/>
	<jsp:param name="logoFile" value=""/>
	<jsp:param name="logolink" value=""/>
	<jsp:param name="downfile" value=""/>
	<jsp:param name="downtext" value=""/>
	<jsp:param name="linkwPageUrl" value="<%=linkwPageUrl%>"/>
	<jsp:param name="linkwDirUrl" value="<%=linkwDirUrl%>"/>

	<jsp:param name="leftImageName" value=""/>
	<jsp:param name="leftImageFile" value=""/>
	<jsp:param name="leftBigImgUrl" value=""/>
	<jsp:param name="rightImageName" value=""/>
	<jsp:param name="rightImageFile" value=""/>
	<jsp:param name="rightBigImgUrl" value=""/>

	<jsp:param name="mainTitle" value="<%=URLEncoder.encode(mainTitle,\"utf-8\")%>"/>
	<jsp:param name="catadocu" value="<%=catadocu%>"/>
</jsp:include>

<div id="mainsect" class="main002">
<ul class="contlist<%=bodyskin%>">
<%
	f = new File(pathOfDir + "/mokcha.txt");
	if(f.exists()){
		int tabindex = 3;
		StringBuffer sbuf = new StringBuffer();
		try{
			String source = "";
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, "utf-8"));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("") || source.equals("nextline")) continue;

				int in = source.indexOf(":");
				if(in == -1) continue;

				String skey = source.substring(0, in);
				if(source.substring(0,2).equals("--")) skey = skey.substring(2);			// 2 class
				else if(source.substring(0,1).equals("-")) skey = skey.substring(1);		// 1 class
				String sval = source.substring(in+1);

				int npage = 1;
				if(!sval.equals("")){
					npage = Integer.parseInt(skey);
					if(showstart > 0) npage = get_realPageFromShow(npage);
				}

				String href = catadocu + "?" + linkwDirUrl + "&start=" + npage;

				out.print("<li><p class='listsubj"+bodyskin+" liststep1'><a href='"+href+"' tabindex='"+tabindex+"' class='c"+bodyskin+"'>"+sval+"</a></p>\n");
				out.print("<p class='listmark"+bodyskin+"'><img src='"+webServer+"/access/pix/index_list.png'></p></li>\n");

				tabindex++;
			}
			fin.close();
			bin.close();
		}
		catch(IOException e) {
			out.print("mokcha read error!\n");
		}
	}
	else{
		out.print("<li><p class='pmsg'>목차 내용이 없습니다.</p></li>\n");
	}
%>
</ul>
</div>

</body>
</html>
