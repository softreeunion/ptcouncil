<%@ page import="java.io.*, java.util.*, java.net.*, org.w3c.dom.*, javax.xml.parsers.*, java.util.regex.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%@ include file="inc_cata.jsp" %>
<%@ include file="inc_util.jsp" %>
<%
	request.setCharacterEncoding("utf-8");

	String incView = request.getParameter("incview");
	String webServer = request.getParameter("webServer");

	String callmode = request.getParameter("callmode");
	String Dir = request.getParameter("Dir");
	String catimage = request.getParameter("catimage");
	String selfName = request.getParameter("selfName");
	String xmlExt = request.getParameter("xmlExt");
	String combokind = request.getParameter("combokind");
	String currCode = request.getParameter("currCode");

	cataPages = Integer.parseInt(request.getParameter("cataPages"));
	firstPage = Integer.parseInt(request.getParameter("firstPage"));
	lastPage = Integer.parseInt(request.getParameter("lastPage"));
	showstart = Integer.parseInt(request.getParameter("showstart"));

	String showPage = request.getParameter("showPage");
	String ini1var = request.getParameter("ini1var");
	String ini2var = request.getParameter("ini2var");
	String dispvar = request.getParameter("dispvar");

	String dirName = URLDecoder.decode(request.getParameter("dirName"),"utf-8");
	String logoFile = request.getParameter("logoFile");
	String logolink = request.getParameter("logolink");
	String downfile = request.getParameter("downfile");
	String downtext = request.getParameter("downtext");
	String linkwPageUrl = request.getParameter("linkwPageUrl");
	String linkwDirUrl = request.getParameter("linkwDirUrl");

	String leftImageName = request.getParameter("leftImageName");
	String leftImageFile = request.getParameter("leftImageFile");
	String leftBigImgUrl = request.getParameter("leftBigImgUrl");
	String rightImageName = request.getParameter("rightImageName");
	String rightImageFile = request.getParameter("rightImageFile");
	String rightBigImgUrl = request.getParameter("rightBigImgUrl");

	onesmc = (dispvar.charAt(3) == 'Y') ? "true" : "false";

	pathOfOriCata 	= webSysDir + "/catImage";
	pathOfCata 		= webSysDir + "/catImage" + catimage;
	pathOfDir 		= pathOfCata + "/" + Dir;

	String webCataOriPath	= webServer+"/catImage";
	String webCataPath 	= webServer+"/catImage"+catimage;
	String webDirPath 	= webServer+"/catImage"+catimage+"/"+Dir;
	String webSkinPath 	= webServer+"/skin";
%>
<!-- logo -->
<% if(!logoFile.equals("")){ %>
<div id="logosect"><% if(!logolink.equals("")){ %><a href="<%=logolink%>" tabindex="2" title="로고 링크" target="_blank"><% } %>
<img src="<%=webServer%>/<%=logoFile%>" alt="로고"><% if(!logolink.equals("")){ %></a><% } %></div>
<% } %>

<% if(incView.equals("s")){ %>
<% int currentPage = Integer.parseInt(request.getParameter("currentPage")); %>
<!-- top menu (inc_display042.jsp) -->
<div id="navisect" class="naviD042">
<div id="turnbtn" class="turnbtnD042"><a href="javascript:linkExpand('e');" title="펼쳐보기"><img id="turnbtnpix" src="<%=webServer%>/skin5/mm042/m_expandbtn.png" alt="펼쳐보기"></a></div>
</div>

<!-- main image -->
<div id="smcsect" class="smc5sD001">

<% if(onesmc.equals("true")){ %>
<div id="smcont1" class="smcont5s smsize">
<a href="javascript:linkBig();" tabindex="7" title="페이지 확대"><img id="smcImgL1" class="smcImg5s smsize"
  src="<%=webCataPath%>/<%=leftImageFile%>" alt="페이지"></a>
<% if(dispvar.charAt(4) == 'Y'){ // enlargeClick %>
<a href="javascript:linkBig();" tabindex="8" title="페이지 확대"><img src="<%=webServer%>/access/pix/enlarge.gif" class="smcenbtn" alt="확대"></a>
<% } %>
</div>

<% } else{ %>
<div id="smcont1" class="smcont5s sm2size">
<% if(leftImageName.equals("")){ %>
<img id="smcImgL1" class="smcImg5s smsize" src="<%=webSkinPath%>/xull.png" alt="">
<% } else{ %>
<a href="<%=leftBigImgUrl%>" tabindex="13" title="페이지 확대"><img id="smcImgL1" class="smcImg5s smsize" src="<%=webCataPath%>/<%=leftImageFile%>" alt="페이지"></a>
<% } %>

<% if(rightImageName.equals("")){ %>
<img id="smcImgR1" class="smcImg5s sm3size" src="<%=webSkinPath%>/xull.png" alt="">
<% } else{ %>
<a href="<%=rightBigImgUrl%>" tabindex="14" title="페이지 확대"><img id="smcImgR1" class="smcImg5s sm3size" src="<%=webCataPath%>/<%=rightImageFile%>" alt="페이지"></a>

<% if(dispvar.charAt(4) == 'Y'){ // enlargeClick %>
<a href="<%=rightBigImgUrl%>" tabindex="15" title="페이지 확대"><img src="<%=webServer%>/access/pix/enlarge.gif" class="smcenbtn" alt="확대"></a>
<% } %>
<% } %>

</div>
<% } %>

<% if(dispvar.charAt(6) == 'Y'){ // dragOver %>
<div id="smcont2" class="smcont5s"></div>
<div id="smcont3" class="smcont5s"></div>
<% } %>

<div id="smclinkL" class="smcImg5s"></div>
<div id="smclinkR" class="smcImg5s sm3size"></div>
</div>

<!-- prev and next button in body -->
<div id="bodyprevbtn" class="bodyprevbtn042" onclick="linkPrev();"><img src="<%=webServer%>/skin5/onsm/onsmimageL042.png" width="40" height="40" alt="이전 페이지"></div>
<div id="bodynextbtn" class="bodynextbtn042" onclick="linkNext();"><img src="<%=webServer%>/skin5/onsm/onsmimageR042.png" width="40" height="40" alt="다음 페이지"></div>

<!-- paging sect -->
<% if(dispvar.charAt(7) == 'Y'){ // pageIcon %>
<div id="pagingiconsect" class="pagingiconD001"></div>
<% } %>

<!-- mmpop menu -->
<div id="popsect" class="popD042" style="visibility:hidden;">
<ul class="pop_ulD042">
<li id="indexbtn" class="indexbtnD042"><a href="contents.jsp?<%=linkwPageUrl%>"" title="목차 보기">
	<img src="<%=webServer%>/skin5/mm042/m_indexbtn.png" width="40" height="10" alt="목차"></a></li>
<li id="spacerbtn" class="spacerbtnD042"><a href="javascript:linkBookmark();" title="책갈피에 추가">
	<img src="<%=webServer%>/skin5/mm042/m_spacerbtn.png" width="42" height="10" alt="책갈피"></a></li>
<li id="expbtn" class="expbtnD042"><a href="explorer.jsp?<%=linkwPageUrl%>" title="탐색기 보기">
	<img src="<%=webServer%>/skin5/mm042/m_expbtn.png" width="35" height="10" alt="탐색기"></a></li>
<% if(dispvar.charAt(9) == 'Y'){ %>
<li id="downbtn" class="downbtnD042"><a href="<%=webDirPath%>/<%=downfile%>" download="<%=webDirPath%>/<%=downfile%>" title="파일 다운로드">
	<img src="<%=webServer%>/skin5/mm042/m_downbtn.png" width="53" height="13" alt="파일 다운로드"></a></li>
<% } %>
</ul>

<!-- sns -->
<ul class="sns_ulD042">
<li id="facebookbtn" class="facebookbtnD042"><a href="javascript:facebookbtnClick();" title="페이스북 공유"><img src="<%=webServer%>/skin5/mm041/m_facebookbtn.png" width="10" height="20" alt="페이스북"></a></li>
<li id="twitterbtn" class="twitterbtnD042"><a href="javascript:twitterbtnClick();" title="트위터 공유"><img src="<%=webServer%>/skin5/mm041/m_twitterbtn.png" width="28" height="20" alt="트위터"></a></li>
<li id="kakaobtn" class="kakaobtnD042"><a href="javascript:kakaobtnClick();" title="카카오 공유"><img src="<%=webServer%>/skin5/mm041/m_kakaobtn.png" width="21" height="20" alt="카카오"></a></li>
</ul>
</div>

<!-- lower menu -->
<nav id="lowersect" class="lowerD042">
<div class="lowernavcnt">
<ul class="mm_ulE042">
<li id="firstbtn" class="firstbtnD042"><a href="javascript:firstbtnClick();" title="처음 페이지로 이동"><img src="<%=webServer%>/skin5/mm042/m_firstbtn.png" 
  width="13" height="13" alt="처음 페이지"></a></li>
<li id="prevbtn" class="prevbtnD042"><a href="javascript:prevbtnClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/mm042/m_prevbtn.png"
  width="10" height="18" alt="이전 페이지"></a></li>
<li id="pageshow" class="pshowD042" contentEditable="true" onfocus="do_pagenoFocusIn2();" onblur="do_pagenoFocusOut2();"></li>
<li id="nextbtn" class="nextbtnD042"><a href="javascript:nextbtnClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/mm042/m_nextbtn.png"
  width="10" height="18" alt="다음 페이지"></a></li>
<li id="lastbtn" class="lastbtnD042"><a href="javascript:lastbtnClick();" title="마지막 페이지로 이동"><img src="<%=webServer%>/skin5/mm042/m_lastbtn.png"
  width="13" height="13" alt="마지막 페이지"></a></li>
</ul>
<div class="lowernavback">
<img class="navidiv001" src="<%=webServer%>/skin5/mm042/m_vertdiv.png">
<img class="navidiv002" src="<%=webServer%>/skin5/mm042/m_vertdiv.png">
</div>
</div>

<div class="lowerbtmcnt">
<ul id="mmgsect" class="mmgD042">
<li id="morebtn" class="morebtnD042"><a href="javascript:popClick();" title="메뉴 더보기"><img src="<%=webServer%>/skin5/mm042/m_morebtn.png"
  width="20" height="20" alt="메뉴 더보기"></a></li>
<li id="combobtn" class="combobtnD042"><a href="combo.jsp?<%=linkwPageUrl%>" title="다른 카탈로그 이동 페이지"><img src="<%=webServer%>/skin5/mm042/m_combobtn.png"
  width="20" height="20" alt="다른 카탈로그"></a></li>
</ul>

<% if(dispvar.charAt(5) == 'Y'){ %>
<div id="searchsect" class="searchD042">
	<form name="searchform" method="get" action="search.jsp" title="페이지 검색">
	<input type="search" class="searchtxtD042" name="qtxt" title="검색어 입력">
	<input type="image" class="searchbtnD042" src="<%=webServer%>/skin5/mm042/m_searchbtn.png" title="검색">
	<input type="hidden" name="Dir" value="<%=Dir%>">
	<input type="hidden" name="catimage" value="<%=catimage%>">
	<input type="hidden" name="callmode" value="<%=callmode%>">
	<input type="hidden" name="start" value="<%=currentPage%>">
	<input type="hidden" name="um" value="s">
	</form>
</div>
<% } %>
</div>
</nav>

<form name="pageshowform" method="get" action="<?php echo $selfName; ?>" title="입력한 페이지로 이동">
<input type="hidden" name="start" value="<?php echo $currPage; ?>">
<input type="hidden" name="Dir" value="<?php echo $Dir; ?>">
<input type="hidden" name="catimage" value="<?php echo $catimage; ?>">
<input type="hidden" name="callmode" value="<?php echo $callmode; ?>">
</form>

<script>
function popClick(){
	if(document.getElementById('popsect').style.visibility == "hidden"){
		document.getElementById('popsect').style.visibility = "visible";
	}
	else{
		document.getElementById('popsect').style.visibility = "hidden";
	}
}
function show_pageNumber(){
	document.getElementById("pageshow").innerHTML = get_showString(currPage);
}
</script>

<% } else if(incView.equals("c")){ %>
<%
	String mainTitle = URLDecoder.decode(request.getParameter("mainTitle"),"utf-8");
	String catadocu = request.getParameter("catadocu");
%>
<div id="navisect" class="naviD042">
	<h1><%=mainTitle%></h1>
	<p class="backbtnD042"><a href="<%=catadocu%>?<%=linkwPageUrl%>"><img src="<%=webServer%>/skin5/mm042/prevbtn.png" 
	width="25" height="25" alt="이전 페이지로" tabindex="2"></a></p>
</div>

<% } else if(incView.equals("e")){ %>
<%
	String smImageWidth = request.getParameter("smImageWidth");
	String smImageHeight = request.getParameter("smImageHeight");
%>
<div id="navisect" class="naviD042">
	<h1>펼쳐보기</h1>
	<div id="turnbtn" class="turnbtnD042"><a href="javascript:linkExpand('');" title="넘겨보기"><img id="turnbtnpix" src="<%=webServer%>/skin5/mm042/m_turnbtn.png" alt="넘겨보기"></a></div>
</div>

<!-- main image -->
<div id="smcsect" class="smc5sD001">
<%
	for(int i = 1;i <= cataPages;i++){
		out.print("<div id='epdiv"+i+"' class=\"expandiv smsize\" data-load='0'></div>\n");
	}
%>
</div>

<% } %>
