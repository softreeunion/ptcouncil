<%@ page import="java.io.*, java.util.*, java.net.*, org.w3c.dom.*, javax.xml.parsers.*, java.util.regex.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%@ include file="../inc_main.jsp" %>
<%@ include file="../ConnLog.jsp" %>
<%@ include file="inc_cata.jsp" %>
<%@ include file="inc_util.jsp" %>
<%
	catimage = get_param1(request.getParameter("catimage"),5);
	Dir = get_param1(request.getParameter("Dir"),5);
	if(Dir.equals("")) Dir = "1";

	String start = check_start(request.getParameter("start"));
	String callmode = check_callmode(request.getParameter("callmode"));
	String eclang = check_eclang(request.getParameter("eclang"));
	String userCate = check_cate(request.getParameter("Cate"));

	pathOfOriCata 	= webSysDir + "/catImage";
	pathOfCata 		= webSysDir + "/catImage" + catimage;
	pathOfDir 		= pathOfCata + "/" + Dir;

	String webCataOriPath	= webServer+"/catImage";
	String webCataPath 	= webServer+"/catImage"+catimage;
	String webDirPath 	= webServer+"/catImage"+catimage+"/"+Dir;
	String webSkinPath 	= webServer+"/skin";
	String selfName		= "ecatalogt.jsp";

	String incView		= "t";
	String queryStr 	= "Dir=" + Dir + "&catimage=" + catimage + "&start=" + start + "&callmode=" + callmode + "&eclang=" + eclang;
	String queryStrLink = queryStr + "&Dir=" + Dir + "&Cate=" + userCate;
	String queryStrDir 	= queryStr + "&Dir=" + Dir;

	String agentstr = request.getHeader("User-Agent");
	if(agentstr == null) agentstr = "";
	else{
		agentstr = agentstr.replaceAll("\n","");
		agentstr = agentstr.replaceAll("\r","");
		agentstr = agentstr.replaceAll("\t","");
	}

	File f = new File(pathOfCata + "/catakind.txt");						// check category configuration file
	if(!f.exists()){
		out.print(echo_text("페이지를 표시할 수 없습니다.(catakind)"));
		return;
	}

	if(callmode.equals("normal_unify") || callmode.equals("admin_unify")) unifyCall = "true";

	HashMap CataKind = new HashMap();
	if(!userCate.equals("")){					// must find the first cover page out of the catalogs
		set_cateCond(userCate, CataKind);
		pathOfDir = pathOfCata + "/" + Dir;
		queryStrLink = queryStr + "&Cate=" + userCate;
		queryStrDir = queryStr + "&Dir=" + Dir;
	}
	else{
		String resstr = set_dirCond(CataKind, unifyCall);
		if(!resstr.equals("")){
			out.print(echo_text(resstr));
			return;
		}
		queryStrLink = queryStrDir;
	}
	if(!CataKind.get("unifyCond").equals("U")) unifyCall = "false";

	String parentCode = (String)CataKind.get("parentCode");
	if(parentCode.equals("")){										// exists parent category?
		out.print(echo_text("페이지를 표시할 수 없습니다.(parentCode)"));
		return;
	}
	f = new File(pathOfDir + "/ecatalog.txt");
	if(!f.exists()){				// check catalog configuration file
		out.print(echo_text("페이지를 표시할 수 없습니다.(ecatalog)"));
		return;
	}
	if(!callmode.equals("admin") && !callmode.equals("admin_unify") && (CataKind.get("svcCond1").equals("D") || CataKind.get("svcCond2").equals("D"))){
		out.print(echo_text("현재는 서비스가 불가합니다."));
		return;
	}

	String[] aaa = get_machine(agentstr);
	String usrmName = aaa[0];
	String usrmKind = aaa[1];

	HashMap Category = new HashMap();
	set_category(pathOfCata+"/category.txt", (String)CataKind.get("parentCode"), Category);

	// for moving from category to category
	String moveInDist = "";
	f = new File(pathOfOriCata+"/catimage.txt");
	if(unifyCall.equals("true") && f.exists()){
		moveInDist = get_catimage(catimage, "moveInDist");
	}

	// *** ecatalog.txt - check whether the total application of anyone folder's configuration exists
	boolean jbool = false;
	String bascadapt = (String)Category.get("bascadapt");
	if(bascadapt == null) bascadapt = "";
	if(unifyCall.equals("false")){
		if(!bascadapt.equals("")){
			f = new File(pathOfCata + "/" + bascadapt + "/ecatalog.txt");
			if(f.exists()) jbool = true;
		}
	}

	HashMap Ecatalog = new HashMap();
	if(jbool == true){
		set_ecatalog(pathOfCata+"/"+bascadapt+"/ecatalog.txt", Ecatalog);
		Ecatalog.put("pageno", get_ecatalogKey(pathOfDir + "/ecatalog.txt", "pageno"));
	}
	else{
		set_ecatalog(pathOfDir+"/ecatalog.txt", Ecatalog);
	}

	String ini2var = (String)Ecatalog.get("ini2var");
	String xmlExt = (ini2var.length() > 15 && ini2var.charAt(15) == 'T') ? "xml.txt" : ".xml";
	String ini1var = (String)Ecatalog.get("ini1var");

	f = new File(pathOfDir+"/bookdata"+xmlExt);
	if(ini1var.charAt(11) == 'X' && (!f.exists() || f.length() == 0)){
		out.print(echo_script("페이지를 표시할 수 없습니다.(bookdata"+xmlExt+")","","window","window.close();\n"));
		return;
	}

	String cook_counter = "";
	String cook_cpasswd = "";
	Cookie[] cookies = request.getCookies();
	if(cookies != null){
		for(int i = 0;i < cookies.length;i++){
			Cookie theCookie = cookies[i];
			if(theCookie.getName().equals("cook_counter")) cook_counter = theCookie.getValue();
			else if(theCookie.getName().equals("cook_cpasswd")) cook_cpasswd = theCookie.getValue();
		}
	}

	String catpasswd = (String)Ecatalog.get("catpasswd");
	if(catpasswd == null) catpasswd = "";
	if(!catpasswd.equals("") && (cook_cpasswd.equals("") || !cook_cpasswd.equals(catpasswd))){				// cat password
		response.sendRedirect("../ecatalog_cpasswd.jsp?"+queryStrLink+"&docu=t");
		return;
	}

	/* counter start */
	f = new File(webSysDir + "/log");
	if(!f.exists()) f.mkdirs();

	ConnLog connlog = new ConnLog(streamCharset);
	connlog.calDate();
	connlog.setMoveFile(webSysDir+"/log", "log"+catimage+"_");

 	if(cook_counter.equals("")){
 		connlog.recordLog(webSysDir+"/log/log"+catimage+"_"+connlog.today+".txt", "0\t"+Dir+"\t"+connlog.toStr+"\t"+request.getRemoteAddr()+"\t"+agentstr+"\n");
		Cookie c_cook_counter = new Cookie("cook_counter", Dir);
		response.addCookie(c_cook_counter);
	}
	else if(connlog.parse_cookie(Dir, cook_counter) == false){
		connlog.recordLog(webSysDir+"/log/log"+catimage+"_"+connlog.today+".txt", "1\t"+Dir+"\t"+connlog.toStr+"\t"+request.getRemoteAddr()+"\t"+agentstr+"\n");
		Cookie c_cook_counter = new Cookie("cook_counter", cook_counter + ":" + Dir);
		response.addCookie(c_cook_counter);
	}
	/* counter end */

	String logoadapt = (String)Category.get("logoadapt");

	// logo
	String logoFile = "";
	if(!logoadapt.equals("")){
		String file = get_logoFile(pathOfCata+"/"+logoadapt, "splogo");
		if(!file.equals("")) logoFile = "catImage"+catimage+"/"+logoadapt+"/"+file;
	}
	if(logoFile.equals("")){
		String file = get_logoFile(pathOfDir,"splogo");
		if(!file.equals("")) logoFile = "catImage"+catimage+"/"+Dir+"/"+file;
	}
	if(logoFile.equals("")){
		String file = get_logoFile(pathOfOriCata,"splogo");
		if(!file.equals("")) logoFile = "catImage/"+file;
	}

	s000File = get_logoFile(pathOfDir, "s000");

	HashMap Display = new HashMap();
	Display.put("user", get_userSkin((String)Ecatalog.get("askin"), incView));

	HashMap Skin = new HashMap();
	set_skin(incView, Skin, (String)Ecatalog.get("askin"), (String)Ecatalog.get("bskin"), Category, Display);

	String display = (String)Skin.get("display");
	String bodyskin = get_bodyskin(display);
	String smc = (String)Skin.get("smc");

	if(ini2var.charAt(5) == 'N') onesmc = "false";

	// 변수 설정
	fileExt = get_fileExtension(ini2var.substring(2,3));
	String enlargeClick = (ini1var.charAt(16) == 'N') ? "false" : "true";
	String dragOver ="false";
	String pageIcon = "false";
	String bigImageOnly = (ini2var.charAt(11) == 'Y') ? "true" : "false";
	if(bigImageOnly.equals("true")) enlargeClick = "false";

	String searchMethod = ini1var.substring(9,10);
	int smImageWidth = Integer.parseInt((String)Ecatalog.get("smwidth"));
	int smImageHeight = Integer.parseInt((String)Ecatalog.get("smheight"));
	int smImageHeightHalf = (int)(smImageHeight/2);

	boolean checkSearchFile = check_searchFile(searchMethod, pathOfDir);
	boolean checkDownFile = check_downFile((String)Ecatalog.get("downfile"));

	// page variables
	if(!Ecatalog.get("showstart").equals("")) showstart = Integer.parseInt((String)Ecatalog.get("showstart"));
	cataPages = Integer.parseInt((String)Ecatalog.get("pageno"));
	if(!Ecatalog.get("frpage").equals("")) firstPage = Integer.parseInt((String)Ecatalog.get("frpage"));

	if(onesmc.equals("true")){
		firstPage = 1;
		lastPage = cataPages;
	}
	else lastPage = ((firstPage+cataPages)%2 == 0) ? cataPages + 1 : cataPages;

	CataDoc catadoc = new CataDoc();
	catadoc.set_fileseq(ini1var.charAt(11), xmlExt);
	if(unifyCall.equals("true")) catadoc.set_unifyVar((String)Category.get("unif"));

	int currentPage = catadoc.get_currentPage(start);
	String showPage = get_showString(currentPage);

	String linkwoDirUrl = "callmode="+callmode+"&catimage="+catimage+"&eclang="+eclang;
	String linkwDirUrl  = linkwoDirUrl+"&Dir="+Dir+"&um=s";
	String linkwPageUrl = linkwDirUrl+"&start="+currentPage;

	String leftImageDir = "";
	String leftImageName = "";
	String leftImageFile = "";
	String leftBigFile = "";
	String leftBigImgUrl = "";

	String rightImageDir = "";
	String rightImageName = "";
	String rightImageFile = "";
	String rightBigFile = "";
	String rightBigImgUrl = "";

	String[] bbb = catadoc.get_leftImageName("", currentPage);
	leftImageDir = bbb[0];
	leftImageName = bbb[1];
	if(!leftImageName.equals("")){
		leftImageFile = leftImageDir+"/s"+leftImageName;
		leftBigFile = leftImageDir+"/"+leftImageName;
		leftBigImgUrl = "bigimage.jsp?"+linkwDirUrl+"&fd="+leftImageDir+"&img="+leftImageName+"&w="+Ecatalog.get("bgwidth")+"&h="+Ecatalog.get("bgheight")+"&start="+currentPage;
		if(bigImageOnly.equals("true")) leftImageFile = leftBigFile;
	}

	if(onesmc.equals("false")){
		String[] ccc = catadoc.get_leftImageName("", currentPage+1);
		rightImageDir = ccc[0];
		rightImageName = ccc[1];
		if(!rightImageName.equals("")){
			rightImageFile = rightImageDir+"/s"+rightImageName;
			rightBigFile = rightImageDir+"/"+rightImageName;
			rightBigImgUrl = "bigimage.jsp?"+linkwDirUrl+"&fd="+rightImageDir+"&img="+rightImageName+"&w="+Ecatalog.get("bgwidth")+"&h="+Ecatalog.get("bgheight")+"&start="+(currentPage+1);
			if(bigImageOnly.equals("true")) rightImageFile = rightBigFile;
		}
	}

	String twitter = (String)Ecatalog.get("twitter");
	String facebook = (String)Ecatalog.get("facebook");
	String kakao = (String)Ecatalog.get("kakao");

	// for SNS Link
	// jump start in euc-kr
	if(charset.toLowerCase().equals("euc-kr")){
		if(!twitter.equals("")) twitter = new String(twitter.getBytes("utf-8"));
		if(!facebook.equals("")) facebook = new String(facebook.getBytes("utf-8"));
		if(!kakao.equals("")) kakao = new String(kakao.getBytes("utf-8"));
	}
	// jump end in euc-kr

	String domainUrl = (request.getServerPort() == 80) ? request.getServerName() : request.getServerName() + ":" + request.getServerPort();
	String selfUrl = "http://" + domainUrl + request.getServletPath();
	String cataRootUrl = request.getServletPath().replaceAll("access/ecatalogs.jsp","ecatalog.jsp");

	if(Ecatalog.get("titlebar") == null || Ecatalog.get("titlebar").equals("")) Ecatalog.put("titlebar","Electronic Catalog(전자 카탈로그)");

	//### options for this site
	boolean turnInJs = true;
	boolean allInJs = (dragOver.equals("true") || turnInJs == true) ? true : false;
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=10.0, user-scalable=yes">
<meta property="og:description" content="<%=facebook%>">
<title><%=Ecatalog.get("titlebar")%> </title>
<link rel="stylesheet" type="text/css" href="style.css">
<style>
	*{font-size:9pt;font-family:굴림;}
	.smsize{width:<%=smImageWidth%>px;height:<%=smImageHeight%>px;}
	.sm2size{width:<%=(smImageWidth*2)%>px;height:<%=smImageHeight%>px;}
	.sm3size{width:<%=smImageWidth%>px;height:<%=smImageHeight%>px;left:<%=smImageWidth%>px;}
</style>
<script>
var display = "<%=display%>";
var oriwidth = <%=smImageWidth%>;
var oriheight = <%=smImageHeight%>;
var bigwidth = <%=Ecatalog.get("bgwidth")%>;
var bigheight = <%=Ecatalog.get("bgheight")%>;
var onesmc = <%=onesmc%>;
var pageval = "";
var catimage = "<%=catimage%>";
var webCataPath = "<%=webCataPath%>";
var webServer = "<%=webServer%>";
var menuHeight = 0;
var showstart = <%=showstart%>;
var applyXMLSeq = <% if(catadoc.fileSeq.equals("X")) out.print("true"); else out.print("false"); %>;
var dragOver = false;
var enlargeClick = <%=enlargeClick%>;
var turnInJs = false;
var pageIcon = <%=pageIcon%>;
var incview = "<%=incView%>";
var bigImageOnly = <%=bigImageOnly%>;

var mainState = "";
var leftImageDir = "<%=leftImageDir%>";
var leftImageName = "<%=leftImageName%>";
var rightImageDir = "<%=rightImageDir%>";
var rightImageName = "<%=rightImageName%>";
var leftLink = new Array();
var rightLink = new Array();
var linkwDirUrl = "<%=selfName%>?<%=linkwDirUrl%>";
var firstPage = <%=firstPage%>;
var currPage = <%=currentPage%>;
var cataPages = <%=cataPages%>;
var lastPage = cataPages;
if(onesmc == false && (firstPage+cataPages)%2 == 0) lastPage = cataPages + 1;

var marginT = 5, marginR = 3, marginB = 5, marginL = 3;
var imgOutLine = true;

<%
	Vector leftLink = catadoc.set_linkData(leftImageDir, leftImageName);
	if(leftLink.size() > 0){
		for(int i = 0;i < leftLink.size();i++){
			out.print("leftLink["+i+"] = \""+(String)leftLink.elementAt(i)+"\";\n");
		}
	}

	Vector rightLink = catadoc.set_linkData(rightImageDir, rightImageName);
	if(rightLink.size() > 0){
		for(int i = 0;i < rightLink.size();i++){
			out.print("rightLink["+i+"] = \""+(String)rightLink.elementAt(i)+"\";\n");
		}
	}
%>

function sns_linkopen(s){
	if(s == "twitter"){
		var s = "<%=URLEncoder.encode(twitter,"utf-8")%>%20<%=URLEncoder.encode(selfUrl+"?"+queryStrLink)%>";
		window.open("https://twitter.com/intent/tweet?url=" + s + "&text=" + s);
	}
	else if(s == "facebook"){
		window.open("http://www.facebook.com/sharer.php?u=<%=URLEncoder.encode(selfUrl+"?"+queryStrLink)%>&t=<%=URLEncoder.encode(facebook,"utf-8")%>");
	}
	else if(s == "kakao"){
		Kakao.init('3f375eea21612332b3fb20d61d232fb5');
		Kakao.Link.sendTalkLink({
			label: "<%=kakao%>\n<%=selfUrl%>?<%=queryStrLink%>"
		});
	}
}
</script>
<script src="<%=webServer%>/access/access-t.js"></script>
<script src="<%=webServer%>/access/link.js"></script>
<script src="https://developers.kakao.com/sdk/js/kakao.min.js" async></script>
</head>

<body class="body<%=bodyskin%>" onload="onload_func();">

<div id="skipToContent"><a href="#b2container" tabindex='1'>메뉴 건너뛰기</a></div>
<div id="divmessage"></div>

<%
	String dispvar = "";
	dispvar += facebook.equals("") ? "N" : "Y";
	dispvar += twitter.equals("") ? "N" : "Y";
	dispvar += kakao.equals("") ? "N" : "Y";
	dispvar += onesmc.equals("true") ? "Y" : "N";
	dispvar += enlargeClick.equals("true") ? "Y" : "N";

	dispvar += (checkSearchFile == true) ? "Y" : "N";
	dispvar += dragOver.equals("true") ? "Y" : "N";
	dispvar += pageIcon.equals("true") ? "Y" : "N";
	dispvar += (check_indexFile(pathOfDir) == true) ? "Y" : "N";
	dispvar += (checkDownFile == true) ? "Y" : "N";

	dispvar += !Skin.get("combokind").equals("000") ? "Y" : "N";
	dispvar += searchMethod;
	dispvar += (String)Category.get("fmove");

	String displayFile = "./inc_display"+display+".jsp";
%>
<jsp:include page="<%=displayFile%>" flush="true">
	<jsp:param name="incview" value="<%=incView%>"/>
	<jsp:param name="callmode" value="<%=callmode%>"/>
	<jsp:param name="webServer" value="<%=webServer%>"/>
	<jsp:param name="Dir" value="<%=Dir%>"/>
	<jsp:param name="catimage" value="<%=catimage%>"/>
	<jsp:param name="selfName" value="<%=selfName%>"/>
	<jsp:param name="xmlExt" value="<%=xmlExt%>"/>
	<jsp:param name="combokind" value="<%=Skin.get(\"combokind\")%>"/>
	<jsp:param name="currCode" value="<%=CataKind.get(\"cateCode\")%>"/>

	<jsp:param name="showPage" value="<%=showPage%>"/>
	<jsp:param name="cataPages" value="<%=cataPages%>"/>
	<jsp:param name="firstPage" value="<%=firstPage%>"/>
	<jsp:param name="lastPage" value="<%=lastPage%>"/>
	<jsp:param name="currentPage" value="<%=currentPage%>"/>
	<jsp:param name="showstart" value="<%=showstart%>"/>
	<jsp:param name="ini1var" value="<%=ini1var%>"/>
	<jsp:param name="ini2var" value="<%=ini2var%>"/>
	<jsp:param name="dispvar" value="<%=dispvar%>"/>

	<jsp:param name="dirName" value="<%=URLEncoder.encode((String)CataKind.get(\"dirName\"),\"utf-8\")%>"/>
	<jsp:param name="logoFile" value="<%=logoFile%>"/>
	<jsp:param name="logolink" value="<%=Ecatalog.get(\"logolink\")%>"/>
	<jsp:param name="downfile" value="<%=Ecatalog.get(\"downfile\")%>"/>
	<jsp:param name="downtext" value="<%=Ecatalog.get(\"downtext\")%>"/>
	<jsp:param name="linkwPageUrl" value="<%=linkwPageUrl%>"/>
	<jsp:param name="linkwDirUrl" value="<%=linkwDirUrl%>"/>

	<jsp:param name="leftImageName" value="<%=leftImageName%>"/>
	<jsp:param name="leftImageFile" value="<%=leftImageFile%>"/>
	<jsp:param name="leftBigImgUrl" value="<%=leftBigImgUrl%>"/>
	<jsp:param name="rightImageName" value="<%=rightImageName%>"/>
	<jsp:param name="rightImageFile" value="<%=rightImageFile%>"/>
	<jsp:param name="rightBigImgUrl" value="<%=rightBigImgUrl%>"/>
</jsp:include>

</body>
</html>
