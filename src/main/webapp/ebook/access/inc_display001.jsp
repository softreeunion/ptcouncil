<%@ page import="java.io.*, java.util.*, java.net.*, org.w3c.dom.*, javax.xml.parsers.*, java.util.regex.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%@ include file="inc_cata.jsp" %>
<%@ include file="inc_util.jsp" %>
<%
	request.setCharacterEncoding("utf-8");

	String incView = request.getParameter("incview");
	String webServer = request.getParameter("webServer");

	String callmode = request.getParameter("callmode");
	String Dir = request.getParameter("Dir");
	String catimage = request.getParameter("catimage");
	String selfName = request.getParameter("selfName");
	String xmlExt = request.getParameter("xmlExt");
	String combokind = request.getParameter("combokind");
	String currCode = request.getParameter("currCode");

	cataPages = Integer.parseInt(request.getParameter("cataPages"));
	firstPage = Integer.parseInt(request.getParameter("firstPage"));
	lastPage = Integer.parseInt(request.getParameter("lastPage"));
	showstart = Integer.parseInt(request.getParameter("showstart"));

	String showPage = request.getParameter("showPage");
	String ini1var = request.getParameter("ini1var");
	String ini2var = request.getParameter("ini2var");
	String dispvar = request.getParameter("dispvar");

	String dirName = URLDecoder.decode(request.getParameter("dirName"),"utf-8");
	String logoFile = request.getParameter("logoFile");
	String logolink = request.getParameter("logolink");
	String downfile = request.getParameter("downfile");
	String downtext = request.getParameter("downtext");
	String linkwPageUrl = request.getParameter("linkwPageUrl");
	String linkwDirUrl = request.getParameter("linkwDirUrl");

	String leftImageName = request.getParameter("leftImageName");
	String leftImageFile = request.getParameter("leftImageFile");
	String leftBigImgUrl = request.getParameter("leftBigImgUrl");
	String rightImageName = request.getParameter("rightImageName");
	String rightImageFile = request.getParameter("rightImageFile");
	String rightBigImgUrl = request.getParameter("rightBigImgUrl");

	onesmc = (dispvar.charAt(3) == 'Y') ? "true" : "false";

	pathOfOriCata 	= webSysDir + "/catImage";
	pathOfCata 		= webSysDir + "/catImage" + catimage;
	pathOfDir 		= pathOfCata + "/" + Dir;

	String webCataOriPath	= webServer+"/catImage";
	String webCataPath 	= webServer+"/catImage"+catimage;
	String webDirPath 	= webServer+"/catImage"+catimage+"/"+Dir;
	String webSkinPath 	= webServer+"/skin";
%>
<!-- logo -->
<% if(!logoFile.equals("")){ %>
<div id="logosect"><% if(!logolink.equals("")){ %><a href="<%=logolink%>" tabindex="2" title="로고 링크" target="_blank"><% } %>
<img src="<%=webServer%>/<%=logoFile%>" alt="로고"><% if(!logolink.equals("")){ %></a><% } %></div>
<% } %>

<% if(incView.equals("s")){ %>
<% int currentPage = Integer.parseInt(request.getParameter("currentPage")); %>
<!-- top menu (display001.jsp) -->
<div id="navisect" class="naviD001">
<div class="navicntD001">
<ul class="navi_ulD001">
<li class="firstbtn001"><a href="javascript:linkFirst();" tabindex="2" title="처음 페이지로 이동"><img
  src="<%=webServer%>/access/pix/firstbtn.gif" width="44" height="28" alt="처음 페이지"></a></li>
<li class="prevbtn001"><a href="javascript:linkPrev();" tabindex="3" title="이전 페이지로 이동"><img
  src="<%=webServer%>/access/pix/prevbtn.gif" width="38" height="28" alt="이전 페이지"></a></li>
<li class="nextbtn001"><a href="javascript:linkNext();" tabindex="4" title="다음 페이지로 이동"><img
  src="<%=webServer%>/access/pix/nextbtn.gif" width="38" height="28" alt="다음 페이지"></a></li>
<li class="lastbtn001"><a href="javascript:linkLast();" tabindex="5" title="끝 페이지로 이동"><img
  src="<%=webServer%>/access/pix/lastbtn.gif" width="44" height="28" alt="끝 페이지"></a></li>
</ul>

<!-- page show in top menu -->
<div id="pageshow" class="pshowD001">
<form name="pageshowform" method="get" action="<%=selfName%>" title="입력한 페이지로 이동">
<input type="text" name="start" value="<%=showPage%>" maxlength="7" title="페이지 입력" tabindex="6"
  onfocus="do_pagenoFocusIn();" onblur="do_pagenoFocusOut();">
<input type="hidden" name="Dir" value="<%=Dir%>">
<input type="hidden" name="catimage" value="<%=catimage%>">
<input type="hidden" name="callmode" value="<%=callmode%>">
<span>/  <%=cataPages%></span>
</form>
</div>

</div>
</div>

<!-- main image -->
<div id="smcsect" class="smc5sD001">

<% if(onesmc.equals("true")){ %>
<div id="smcont1" class="smcont5s smsize">
<a href="javascript:linkBig();" tabindex="7" title="페이지 확대"><img id="smcImgL1" class="smcImg5s smsize" src="<%=webCataPath%>/<%=leftImageFile%>" alt="페이지"></a>
<% if(dispvar.charAt(4) == 'Y'){ // enlargeClick %>
<a href="javascript:linkBig();" tabindex="8" title="페이지 확대"><img src="<%=webServer%>/access/pix/enlarge.gif" class="smcenbtn" alt="확대"></a>
<% } %>
</div>

<% } else{ %>
<div id="smcont1" class="smcont5s sm2size">
<% if(leftImageName.equals("")){ %>
<img id="smcImgL1" class="smcImg5s smsize" src="<%=webSkinPath%>/xull.png" alt="">
<% } else{ %>
<a href="<%=leftBigImgUrl%>" tabindex="13" title="페이지 확대"><img id="smcImgL1" class="smcImg5s smsize" src="<%=webCataPath%>/<%=leftImageFile%>" alt="페이지"></a>
<% } %>

<% if(rightImageName.equals("")){ %>
<img id="smcImgR1" class="smcImg5s sm3size" src="<%=webSkinPath%>/xull.png" alt="">
<% } else{ %>
<a href="<%=rightBigImgUrl%>" tabindex="14" title="페이지 확대"><img id="smcImgR1" class="smcImg5s sm3size" src="<%=webCataPath%>/<%=rightImageFile%>" alt="페이지"></a>

<% if(dispvar.charAt(4) == 'Y'){ // enlargeClick %>
<a href="<%=rightBigImgUrl%>" tabindex="15" title="페이지 확대"><img src="<%=webServer%>/access/pix/enlarge.gif" class="smcenbtn" alt="확대"></a>
<% } %>
<% } %>

</div>
<% } %>

<% if(dispvar.charAt(6) == 'Y'){ // dragOver %>
<div id="smcont2" class="smcont5s"></div>
<div id="smcont3" class="smcont5s"></div>
<% } %>

<div id="smclinkL" class="smcImg5s"></div>
<div id="smclinkR" class="smcImg5s sm3size"></div>
</div>

<!-- prev and next button in body -->
<div id="bodyprevbtn" class="bodyprevbtn001" onclick="linkPrev();"><img src="<%=webServer%>/access/pix/body_prev.png" width="18" height="87" alt="이전 페이지"></div>
<div id="bodynextbtn" class="bodynextbtn001" onclick="linkNext();"><img src="<%=webServer%>/access/pix/body_next.png" width="18" height="87" alt="다음 페이지"></div>

<!-- paging sect -->
<% if(dispvar.charAt(7) == 'Y'){ // pageIcon %>
<div id="pagingiconsect" class="pagingiconD001"></div>
<% } %>

<!-- search and sns -->
<% if(dispvar.charAt(5) == 'Y' || dispvar.charAt(0) == 'Y' || dispvar.charAt(1) == 'Y' || dispvar.charAt(2) == 'Y'){ %>
<div id="sidesect" class="sideD001">
<% if(dispvar.charAt(5) == 'Y'){ %>
<div class="search5sD001">
	<form name="searchform" method="get" action="search.jsp" title="페이지 검색">
	<input type="search" name="qtxt" class="searchtxtD001" tabindex="11" title="검색어 입력">
	<input type="image" src="<%=webServer%>/access/pix/searchbtn.gif" class="searchbtnD001" tabindex="12" title="검색">
	<input type="hidden" name="Dir" value="<%=Dir%>">
	<input type="hidden" name="catimage" value="<%=catimage%>">
	<input type="hidden" name="callmode" value="<%=callmode%>">
	<input type="hidden" name="start" value="<%=currentPage%>">
	<input type="hidden" name="um" value="s">
	</form>
</div>
<% } %>

<!-- sns in b3menu -->
<% if(dispvar.charAt(0) == 'Y' || dispvar.charAt(1) == 'Y' || dispvar.charAt(2) == 'Y'){ %>
<ul id="snssect" class="snssectD001">
<% if(dispvar.charAt(0) == 'Y'){ %>
<li id="facebookbtn"><a href="javascript:sns_linkopen('facebook')" tabindex="14" title="페이스북 공유"><img 
src="<%=webServer%>/access/pix/facebook_btn.png" width="24" height="24" alt="페이스북"></a></li>
<% } %>
<% if(dispvar.charAt(1) == 'Y'){ %>
<li id="twitterbtn"><a href="javascript:sns_linkopen('twitter')" tabindex="15" title="트위터 공유"><img 
src="<%=webServer%>/access/pix/twitter_btn.png" width="24" height="24" alt="트위터"></a></li>
<% } %>
<% if(dispvar.charAt(2) == 'Y'){ %>
<li id="kakaobtn"><a href="javascript:sns_linkopen('kakao')" tabindex="18" title="카카오 공유"><img 
src="<%=webServer%>/access/pix/kakao_btn.png" width="24" height="24" alt="카카오"></a></li>
<% } %>
</ul>
<% } %>
</div>
<% } %>

<div id="lowersect" class="lower5sD001">
<div id="lowercnt" class="lowercnt5sD001">
<a href="explorer.jsp?<%=linkwPageUrl%>" tabindex="20" title="탐색기 보기"><p id="expbtn" class="pbtnE002 c001">탐 색</p></a>
<% if(dispvar.charAt(8) == 'Y'){ %>
<a href="contents.jsp?<%=linkwPageUrl%>" tabindex="21" title="목차 보기"><p id="indexbtn" class="pbtnE002 c001">목 차</p></a>
<% } %>
<a href="javascript:linkBookmark();" tabindex="22" title="책갈피 추가"><p id="spacerbtn" class="pbtnE001 c001">책갈피</p></a>
<% if(ini2var.charAt(4) == 'N'){ %>
<a href="email.jsp?<%=linkwPageUrl%>" tabindex="23" title="메일 발송"><p id="mailbtn" class="pbtnE002 c001">메 일</p></a>
<% } %>
<% if(!combokind.equals("000")){ %>
<a href="combo.jsp?<%=linkwPageUrl%>" tabindex="24" title="다른 이북"><p id="combobtn" class="pbtnE002 c001">이 동</p></a>
<% } %>
<% if(dispvar.charAt(9) == 'Y'){ %>
<a href="<%=webDirPath%>/<%=downfile%>" tabindex="25" title="파일 다운로드"><p id="downbtn" class="pbtnE001 c001">다운로드</p></a>
<% } %>
</div>
</div>

<script>
function show_pageNumber(){
	document.pageshowform.start.value = get_showString(currPage);
}
</script>

<% } else if(incView.equals("t")){ %>
<%
	int currentPage = Integer.parseInt(request.getParameter("currentPage"));
	int prevPage = onesmc.equals("true") ? currentPage - 1 : currentPage - 2;
	int nextPage = onesmc.equals("true") ? currentPage + 1 : currentPage + 2;

	String[] searchStr = {"",""};
	if(dispvar.charAt(11) == 'A' && dispvar.charAt(5) == 'Y'){
		searchStr = getAlterPage(pathOfDir+"/search"+xmlExt, currentPage);
	}

	String combobuf = "";
	if(dispvar.charAt(12) == '0'){
		if(combokind.equals("001")) combobuf = get_comboString(currCode);
		else combobuf = get_comboStringAll(currCode);
	}

%>
<!-- top menu (display001.jsp) -->
<div id="navisect" class="naviD001">
<div class="navicntD001">
<ul class="navi_ulD001">
<li class="firstbtn001"><a href="<%=selfName%>?<%=linkwDirUrl%>&start=<%=firstPage%>" tabindex="3" title="처음 페이지로 이동"><img
  src="<%=webServer%>/access/pix/firstbtn.gif" width="44" height="28" alt="처음 페이지"></a></li>
<li class="prevbtn001"><a href="<%=selfName%>?<%=linkwDirUrl%>&start=<%=prevPage%>" tabindex="4" title="이전 페이지로 이동"><img
  src="<%=webServer%>/access/pix/prevbtn.gif" width="38" height="28" alt="이전 페이지"></a></li>
<li class="nextbtn001"><a href="<%=selfName%>?<%=linkwDirUrl%>&start=<%=nextPage%>" tabindex="5" title="다음 페이지로 이동"><img
  src="<%=webServer%>/access/pix/nextbtn.gif" width="38" height="28" alt="다음 페이지"></a></li>
<li class="lastbtn001"><a href="<%=selfName%>?<%=linkwDirUrl%>&start=<%=lastPage%>" tabindex="6" title="끝 페이지로 이동"><img
  src="<%=webServer%>/access/pix/lastbtn.gif" width="44" height="28" alt="끝 페이지"></a></li>
</ul>

<!-- page show in top menu -->
<div id="pageshow" class="pshowD001">
<form name="pageshowform" method="get" action="<%=selfName%>" title="입력한 페이지로 이동">
<input type="text" name="start" value="<%=showPage%>" maxlength="7" title="페이지 입력" tabindex="7"
  onfocus="do_pagenoFocusIn();" onblur="do_pagenoFocusOut();">
<input type="hidden" name="Dir" value="<%=Dir%>">
<input type="hidden" name="catimage" value="<%=catimage%>">
<input type="hidden" name="callmode" value="<%=callmode%>">
<span>/  <%=cataPages%></span>
</form>
</div>

</div>
</div>

<!-- index -->
<% if(dispvar.charAt(8) == 'Y'){ %>
<div id="indexsect" class="index5tD001">
<form name="indexform" method="get" action="<%=selfName%>" title="목차 페이지로 이동">
<select name="start" class="indexsel5tD001" title="목차 선택" tabindex="8" onchange="goNext(this.value, document.indexform,'user')">
	<option value="">목 차</option>
	<%=get_indexString()%>
</select>
<input type="image" src="<%=webServer%>/access/pix/movebtn.gif" class="indexbtn5tD001" tabindex="9" alt="이동">
<input type="hidden" name="Dir" value="<%=Dir%>">
<input type="hidden" name="catimage" value="<%=catimage%>">
<input type="hidden" name="callmode" value="<%=callmode%>">
</form>
</div>
<% } %>


<!-- main image -->
<div id="smcsect" class="smc5tD001">

<% if(onesmc.equals("true")){ %>

<div id="smcont1" class="smcont5t smsize">
<a href="<%=leftBigImgUrl%>" tabindex="10" title="페이지 확대"><img id="smcImgL1" class="smcImg5s smsize" src="<%=webCataPath%>/<%=leftImageFile%>" alt="페이지"></a>
<% if(dispvar.charAt(4) == 'Y'){ %>
<a href="<%=leftBigImgUrl%>" tabindex="11" title="페이지 확대"><img src="<%=webServer%>/access/pix/enlarge.gif" class="smcenbtn" alt="확대"></a>
<% } %>

<div id="smcaltsL" class="noshow">
<%
	if(searchStr[0].equals("")) out.print("<p>"+currentPage+"페이지 내용 없음</p>");
	else out.print("<p>"+currentPage+"페이지 내용 : "+searchStr[0]+"</p>");
%>
</div>

</div>

<% } else{ %>

<% if(!leftImageName.equals("")){ %>
<a href="bookmark.jsp?<%=linkwPageUrl%>&cpage=<%=currentPage%>" tabindex="12" title="<%=currentPage%>페이지 책갈피 추가"><img
id="splbtnsect" class="splbtn5tD001" src="<%=webServer%>/access/pix/bookmark_left.png" width="47" height="35" alt="책갈피 추가"></a>
<% } else{ %>
<img id="splbtnsect" class="splbtn5tD001" src="<%=webServer%>/access/pix/bookmark_left.png" width="47" height="35" alt="책갈피 추가">
<% } %>

<div id="smcont1" class="smcont5t sm2size">
<% if(leftImageName.equals("")){ %>
<img id="smcImgL1" class="smcImg5s smsize" src="<%=webSkinPath%>/xull.png" alt="">
<% } else{ %>
<a href="<%=leftBigImgUrl%>" tabindex="13" title="페이지 확대"><img id="smcImgL1" class="smcImg5s smsize" src="<%=webCataPath%>/<%=leftImageFile%>" alt="페이지"></a>
<% } %>

<div id="smcaltsL" class="noshow">
<%
	if(searchStr[0].equals("")) out.print("<p>"+currentPage+"페이지 내용 없음</p>");
	else out.print("<p>"+currentPage+"페이지 내용 : "+searchStr[0]+"</p>");
%>
</div>

<% if(rightImageName.equals("")){ %>
<img id="smcImgR1" class="smcImg5s sm3size" src="<%=webSkinPath%>/xull.png" alt="">
<% } else{ %>
<a href="<%=rightBigImgUrl%>" tabindex="14" title="페이지 확대"><img id="smcImgR1" class="smcImg5s sm3size" src="<%=webCataPath%>/<%=rightImageFile%>" alt="페이지"></a>

<% if(dispvar.charAt(4) == 'Y'){ %>
<a href="<%=rightBigImgUrl%>" tabindex="15" title="페이지 확대"><img src="<%=webServer%>/access/pix/enlarge.gif" class="smcenbtn" alt="확대"></a>
<% } %>
<% } %>

</div>

<% if(!rightImageName.equals("")){ %>
<a href="bookmark.jsp?<%=linkwPageUrl%>&cpage=<%=(currentPage+1)%>" tabindex="16" title="<%=(currentPage+1)%>페이지 책갈피 추가"><img
id="sprbtnsect" class="sprbtn5tD001" src="<%=webServer%>/access/pix/bookmark_right.png" width="47" height="35" alt="책갈피 추가"></a>
<% } else{ %>
<img id="sprbtnsect" class="sprbtn5tD001" src="<%=webServer%>/access/pix/bookmark_right.png" width="47" height="35" alt="책갈피 추가">
<% } %>
<% } %>

<div id="smcaltsR" class="noshow">
<%
	if(searchStr[1].equals("")) out.print("<p>"+(currentPage+1)+"페이지 내용 없음</p>");
	else out.print("<p>"+(currentPage+1)+"페이지 내용 : "+searchStr[1]+"</p>");
%>
</div>

<div id="smclinkL" class="smcImg5s"></div>
<div id="smclinkR" class="smcImg5s sm3size"></div>
</div>

<!-- search and download -->
<% if(dispvar.charAt(5) == 'Y' || dispvar.charAt(9) == 'Y'){ %>
<div id="sidesect" class="sideD001">
<% if(dispvar.charAt(5) == 'Y'){ %>
<div class="search5tD001">
	<form name="searchform" method="get" action="search.jsp" title="페이지 검색">
	<input type="search" name="qtxt" class="searchtxtD001" tabindex="17" title="검색어 입력">
	<input type="image" src="<%=webServer%>/access/pix/searchbtn.gif" class="searchbtnD001" tabindex="18" title="검색">
	<input type="hidden" name="Dir" value="<%=Dir%>">
	<input type="hidden" name="catimage" value="<%=catimage%>">
	<input type="hidden" name="callmode" value="<%=callmode%>">
	<input type="hidden" name="start" value="<%=currentPage%>">
	<input type="hidden" name="um" value="t">
	</form>
</div>
<% } %>

<% if(dispvar.charAt(9) == 'Y'){ %>
<p id="downbtn" class="down5tD001">
<%
	String downmoo = "";
	if(downtext.equals("")) downmoo = "다운로드";
	else if(downtext.indexOf("|") < 0) downmoo = downtext;
	else{
		int in = downtext.indexOf("|");
		String skey = downtext.substring(0, in);
		String sval = downtext.substring(in+1);

		if(sval.equals("")) downmoo = downtext;
		else downmoo = "<span style=\"color:#"+sval+"\">"+skey+"</span>";
	}
	out.print("<a href='"+webDirPath+"/"+downfile+"' tabindex='19' class='c001' title='파일 다운로드'>"+downmoo+"</a>");
%>
</p>
<% } %>
</div>
<% } %>

<div class="clearx"></div>

<div id="lowersect" class="lower5tD001">

<% if(!combobuf.equals("")){ %>
<div id="catabox" class="catabox5tD001">
	<form name="moveform" method="get" action="<%=selfName%>" title="다른 카탈로그 이동">
	<select name="Dir" tabindex="20" onchange="goNext(this.value, document.moveform)">
		<option value="">다른 카탈로그 이동</option>
		<%=combobuf%>
	</select>
	<input type="image" src="<%=webServer%>/access/pix/movebtn.gif" tabindex="21" alt="이동">
	<input type="hidden" name="catimage" value="<%=catimage%>">
	<input type="hidden" name="callmode" value="<%=callmode%>">
	</form>
</div>
<% } %>

<div class="lowercnt5tD001E1">
<a href="explorer.jsp?<%=linkwPageUrl%>" tabindex="22" title="탐색"><p id="expbtn" class="pbtnE002 c001">탐 색</p></a>
<% if(ini2var.charAt(4) == 'N'){ %>
<a href="email.jsp?<%=linkwPageUrl%>" tabindex="23" title="메일 발송"><p id="mailbtn" class="pbtnE002 c001">메 일</p></a>
<% } %>
</div>

<div class="lowercnt5tD001E2">
<% if(dispvar.charAt(0) == 'Y'){ %>
<p id="facebookbtn" class="pbtnE003"><a href="javascript:sns_linkopen('facebook')" tabindex="24" title="페이스북 공유"><img 
src="<%=webServer%>/access/pix/facebook_btn.png" width="24" height="24" alt="페이스북"></a></p>
<% } %>
<% if(dispvar.charAt(1) == 'Y'){ %>
<p id="twitterbtn" class="pbtnE003"><a href="javascript:sns_linkopen('twitter')" tabindex="25" title="트위터 공유"><img 
src="<%=webServer%>/access/pix/twitter_btn.png" width="24" height="24" alt="트위터"></a></p>
<% } %>
<% if(dispvar.charAt(2) == 'Y'){ %>
<p id="kakaobtn" class="pbtnE003"><a href="javascript:sns_linkopen('kakao')" tabindex="26" title="카카오 공유"><img 
src="<%=webServer%>/access/pix/kakao_btn.png" width="24" height="24" alt="카카오"></a></p>
<% } %>
</div>
</div>

<% } else if(incView.equals("c")){ %>
<%
	String mainTitle = URLDecoder.decode(request.getParameter("mainTitle"),"utf-8");
	String catadocu = request.getParameter("catadocu");
%>
<div id="navisect" class="naviD001">
	<h1><%=mainTitle%></h1>
	<p class="backbtnD001"><a href="<%=catadocu%>?<%=linkwPageUrl%>"><img src="<%=webServer%>/access/pix/prevbtn.gif" 
	width="38" height="28" alt="이전 페이지로" tabindex="2"></a></p>
</div>

<% } %>
