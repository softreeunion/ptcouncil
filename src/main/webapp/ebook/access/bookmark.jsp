<%@ page import="java.io.*, java.util.*, java.net.*, org.w3c.dom.*, javax.xml.parsers.*, java.util.regex.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%@ include file="../inc_main.jsp" %>
<%@ include file="inc_cata.jsp" %>
<%@ include file="inc_util.jsp" %>
<%
	catimage = get_param1(request.getParameter("catimage"),5);
	Dir = get_param1(request.getParameter("Dir"),5);
	if(Dir.equals("")) Dir = "1";

	String start = check_start(request.getParameter("start"));
	String callmode = check_callmode(request.getParameter("callmode"));
	String eclang = check_eclang(request.getParameter("eclang"));
	String userCate = check_cate(request.getParameter("Cate"));
	String um = check_um(request.getParameter("um"),"um");
	String cpage = check_cpage(request.getParameter("cpage"));

	pathOfOriCata 	= webSysDir + "/catImage";
	pathOfCata 		= webSysDir + "/catImage" + catimage;
	pathOfDir 		= pathOfCata + "/" + Dir;

	String webCataOriPath	= webServer+"/catImage";
	String webCataPath 	= webServer+"/catImage"+catimage;
	String webDirPath 	= webServer+"/catImage"+catimage+"/"+Dir;
	String webSkinPath 	= webServer+"/skin";
	String selfName		= "bookmark.jsp";
	String incView 		= "c";
	String catadocu		= "ecatalog"+um+".jsp";

	String linkwoDirUrl = "callmode="+callmode+"&catimage="+catimage+"&eclang="+eclang+"&um="+um;
	String linkwDirUrl  = linkwoDirUrl+"&Dir="+Dir;
	String linkwPageUrl = linkwDirUrl+"&start="+start;

	File f = new File(pathOfCata + "/catakind.txt");						// check category configuration file
	if(!f.exists()){
		out.print(echo_text("페이지를 표시할 수 없습니다.(catakind)"));
		return;
	}

	if(callmode.equals("normal_unify") || callmode.equals("admin_unify")) unifyCall = "true";

	HashMap CataKind = new HashMap();
	if(!userCate.equals("")){					// must find the first cover page out of the catalogs
		set_cateCond(userCate, CataKind);
		pathOfDir = pathOfCata + "/" + Dir;
		webDirPath = webServer+"/catImage"+catimage+"/"+Dir;
		linkwDirUrl  = linkwoDirUrl+"&Dir="+Dir;
	}
	else{
		String resstr = set_dirCond(CataKind, unifyCall);
		if(!resstr.equals("")){
			out.print(echo_text(resstr));
			return;
		}
	}

	if(!CataKind.get("unifyCond").equals("U")) unifyCall = "false";

	String parentCode = (String)CataKind.get("parentCode");
	if(parentCode.equals("")){										// exists parent category?
		out.print(echo_text("페이지를 표시할 수 없습니다.(parentCode)"));
		return;
	}
	f = new File(pathOfDir + "/ecatalog.txt");
	if(!f.exists()){				// check catalog configuration file
		out.print(echo_text("페이지를 표시할 수 없습니다.(ecatalog)"));
		return;
	}
	if(!callmode.equals("admin") && !callmode.equals("admin_unify") && (CataKind.get("svcCond1").equals("D") || CataKind.get("svcCond2").equals("D"))){
		out.print(echo_text("현재는 서비스가 불가합니다."));
		return;
	}

	HashMap Category = new HashMap();
	set_category(pathOfCata+"/category.txt", (String)CataKind.get("parentCode"), Category);

	// for moving from category to category
	String moveInDist = "";
	f = new File(pathOfOriCata+"/catimage.txt");
	if(unifyCall.equals("true") && f.exists()){
		moveInDist = get_catimage(catimage, "moveInDist");
	}

	// *** ecatalog.txt - check whether the total application of anyone folder's configuration exists
	boolean jbool = false;
	String bascadapt = (String)Category.get("bascadapt");
	if(bascadapt == null) bascadapt = "";
	if(unifyCall.equals("false")){
		if(!bascadapt.equals("")){
			f = new File(pathOfCata + "/" + bascadapt + "/ecatalog.txt");
			if(f.exists()) jbool = true;
		}
	}

	HashMap Ecatalog = new HashMap();
	if(jbool == true){
		set_ecatalog(pathOfCata+"/"+bascadapt+"/ecatalog.txt", Ecatalog);
		Ecatalog.put("pageno", get_ecatalogKey(pathOfDir + "/ecatalog.txt", "pageno"));
	}
	else{
		set_ecatalog(pathOfDir+"/ecatalog.txt", Ecatalog);
	}

	String ini2var = (String)Ecatalog.get("ini2var");
	String xmlExt = (ini2var.length() > 15 && ini2var.charAt(15) == 'T') ? "xml.txt" : ".xml";
	String ini1var = (String)Ecatalog.get("ini1var");

	String cook_cpasswd = "";
	Cookie[] cookies = request.getCookies();
	if(cookies != null){
		for(int i = 0;i < cookies.length;i++){
			Cookie theCookie = cookies[i];
			if(theCookie.getName().equals("cook_cpasswd")) cook_cpasswd = theCookie.getValue();
		}
	}

	String catpasswd = (String)Ecatalog.get("catpasswd");
	if(catpasswd == null) catpasswd = "";
	if(!catpasswd.equals("") && (cook_cpasswd.equals("") || !cook_cpasswd.equals(catpasswd))){				// cat password
		response.sendRedirect("../ecatalog_cpasswd.jsp?"+linkwPageUrl+"&docu=mark");
		return;
	}

	String logoadapt = (String)Category.get("logoadapt");

	// logo
	String logoFile = "";
	if(!logoadapt.equals("")){
		String file = get_logoFile(pathOfCata+"/"+logoadapt, "splogo");
		if(!file.equals("")) logoFile = "catImage"+catimage+"/"+logoadapt+"/"+file;
	}
	if(logoFile.equals("")){
		String file = get_logoFile(pathOfDir,"splogo");
		if(!file.equals("")) logoFile = "catImage"+catimage+"/"+Dir+"/"+file;
	}
	if(logoFile.equals("")){
		String file = get_logoFile(pathOfOriCata,"splogo");
		if(!file.equals("")) logoFile = "catImage/"+file;
	}

	HashMap Display = new HashMap();
	Display.put("user", get_userSkin((String)Ecatalog.get("askin"), incView));

	HashMap Skin = new HashMap();
	set_skin(um, Skin, (String)Ecatalog.get("askin"), (String)Ecatalog.get("bskin"), Category, Display);

	String display = (String)Skin.get("display");
	String bodyskin = get_bodyskin(display);

	fileExt = get_fileExtension(ini2var.substring(2,3));
	int smImageWidth = Integer.parseInt((String)Ecatalog.get("smwidth"));
	int smImageHeight = Integer.parseInt((String)Ecatalog.get("smheight"));

	// page variables
	if(!Ecatalog.get("showstart").equals("")) showstart = Integer.parseInt((String)Ecatalog.get("showstart"));
	cataPages = Integer.parseInt((String)Ecatalog.get("pageno"));
	firstPage = 1;
	lastPage = cataPages;

	CataDoc catadoc = new CataDoc();
	catadoc.set_fileseq(ini1var.charAt(11), xmlExt);
	if(unifyCall.equals("true")) catadoc.set_unifyVar((String)Category.get("unif"));

	int w = um.equals("t") ? 100 : 60;
	int h = 0;
	if(smImageWidth > 0) h = (int)(smImageHeight * w / smImageWidth);

	int lenc = 0;
	StringBuffer sbuf = new StringBuffer();
	Vector vt = new Vector();

	Cookie c_cook_dir;
	String cKey = "cook_bm_"+catimage+"_"+Dir;

	if(cpage.equals("empty")){
		c_cook_dir = new Cookie(cKey, "");
		response.addCookie(c_cook_dir);
	}
	else{
		String cook_bm = "";
		if(cookies != null){
			for(int i = 0;i < cookies.length;i++){
				Cookie theCookie = cookies[i];
				if(theCookie.getName().equals(cKey)) cook_bm = theCookie.getValue();
			}
		}

		if(!cook_bm.equals("")){
			StringTokenizer st = new StringTokenizer(cook_bm, "|");
			while(st.hasMoreTokens()){
				vt.addElement(st.nextToken());
			}
		}

 		if(!cpage.equals("view") && (vt.size() == 0 || !vt.contains(cpage))){
			vt.addElement(cpage);
			c_cook_dir = new Cookie(cKey, cook_bm + "|" + cpage);
			response.addCookie(c_cook_dir);
		}

		lenc = vt.size();
	}

	if(Ecatalog.get("titlebar") == null || Ecatalog.get("titlebar").equals("")) Ecatalog.put("titlebar","Electronic Catalog(전자 카탈로그) - 책갈피");
 
	String mainTitle = "책갈피";
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=10.0, user-scalable=yes">
<title><%=Ecatalog.get("titlebar")%> </title>
<link rel="stylesheet" type="text/css" href="<%=webServer%>/access/style.css">
<style>
	*{font-size:9pt;font-family:<%=default_font%>;}
	.thimg{width:<%=w%>px;height:<%=h%>px;}
	a{text-decoration:none;}
</style>
<script>
var stageWidth, stageHeight;
var thumbWidth = <%=w%>;
var thumbHeight = <%=h%>;
var unitW = thumbWidth + 2;
var unitH = thumbHeight + 2;
var spacingII = 5;

function onload_func(){
	stageWidth = document.documentElement.clientWidth;
	stageHeight = document.documentElement.clientHeight;
	var colQty = Math.floor((stageWidth-10-unitW)/(unitW+spacingII)) + 1;
	var w = colQty*unitW + colQty*spacingII;
	document.getElementById('mainsect').style.width = w + "px";
}

window.addEventListener("orientationchange", onload_func);
window.addEventListener("resize", onload_func);
</script>
</head>

<body class="body<%=bodyskin%>" onload="onload_func();">

<%
	String dispvar = "NNNNNNNNNNNNNNNNNNNNN";
	String displayFile = "./inc_display"+display+".jsp";
%>
<jsp:include page="<%=displayFile%>" flush="true">
	<jsp:param name="incview" value="<%=incView%>"/>
	<jsp:param name="callmode" value="<%=callmode%>"/>
	<jsp:param name="webServer" value="<%=webServer%>"/>
	<jsp:param name="Dir" value="<%=Dir%>"/>
	<jsp:param name="catimage" value="<%=catimage%>"/>
	<jsp:param name="selfName" value="<%=selfName%>"/>
	<jsp:param name="xmlExt" value=""/>
	<jsp:param name="combokind" value=""/>
	<jsp:param name="currCode" value=""/>

	<jsp:param name="showPage" value=""/>
	<jsp:param name="cataPages" value="<%=cataPages%>"/>
	<jsp:param name="firstPage" value="<%=firstPage%>"/>
	<jsp:param name="lastPage" value="<%=lastPage%>"/>
	<jsp:param name="currentPage" value="0"/>
	<jsp:param name="showstart" value="<%=showstart%>"/>
	<jsp:param name="ini1var" value="<%=ini1var%>"/>
	<jsp:param name="ini2var" value="<%=ini2var%>"/>
	<jsp:param name="dispvar" value="<%=dispvar%>"/>

	<jsp:param name="dirName" value=""/>
	<jsp:param name="logoFile" value=""/>
	<jsp:param name="logolink" value=""/>
	<jsp:param name="downfile" value=""/>
	<jsp:param name="downtext" value=""/>
	<jsp:param name="linkwPageUrl" value="<%=linkwPageUrl%>"/>
	<jsp:param name="linkwDirUrl" value="<%=linkwDirUrl%>"/>

	<jsp:param name="leftImageName" value=""/>
	<jsp:param name="leftImageFile" value=""/>
	<jsp:param name="leftBigImgUrl" value=""/>
	<jsp:param name="rightImageName" value=""/>
	<jsp:param name="rightImageFile" value=""/>
	<jsp:param name="rightBigImgUrl" value=""/>

	<jsp:param name="mainTitle" value="<%=URLEncoder.encode(mainTitle,\"utf-8\")%>"/>
	<jsp:param name="catadocu" value="<%=catadocu%>"/>
</jsp:include>

<div id="mainsect" class="main001">
<ul class="thumblist<%=bodyskin%>">
<%
	int tabindex = 3;
	if(cpage.equals("empty")){
		out.print("<li class='nolist2'>책갈피를 비웠습니다.</li>");
	}
	else if(lenc > 0){
		for(int i = 0;i < lenc;i++){
			int npage = Integer.parseInt((String)vt.get(i));
			String shpage = get_showPageFromReal2(npage);

			String[] temp = catadoc.get_expImageName("s", npage);
			f = new File(pathOfCata + "/" + temp[0] + "/" + temp[1]);
			if(!f.exists()) continue;

			String href = catadocu + "?" + linkwDirUrl + "&start=" + npage;

			out.print("<li><a href='"+href+"' tabindex='"+tabindex+"'><img src='"+webCataPath+"/"+temp[0]+"/"+temp[1]+"' ");
			out.print("class='thimg' alt='"+shpage+"페이지'></a><br>");
			out.print("<span>"+shpage+"페이지</span></li>\n");

			tabindex++;
		}
	}
	else{
		out.print("<li class='nolist2'>목록이 없습니다.</li>\n");
	}
%>
</ul>
</div>

<div class="clearx"></div>
<%
	String pagingSkin = bodyskin;
	String pagingPixDir = "";
	if(pagingSkin.equals("002")) pagingPixDir = "2";
	String btnSkin = "006";
	if(display.equals("002")) btnSkin = "007";
%>
<div id="pagingsect" class="pagingsect<%=pagingSkin%>">
<% if(cpage.equals("empty")){ %>
<p class="pbtnE<%=btnSkin%>"><a href="<%=catadocu%>?<%=linkwPageUrl%>" tabindex="<%=tabindex%>" class="c<%=display%>">본문으로 가기</a></p>
<% } else{ %>
<p class="pbtnE<%=btnSkin%>"><a href="bookmark.jsp?<%=linkwPageUrl%>&cpage=empty" tabindex="<%=tabindex%>" class="c<%=display%>">책갈피 비우기</a></p>
<% } %>
<div>

</body>
</html>
