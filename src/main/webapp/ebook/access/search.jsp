<%@ page import="java.io.*, java.util.*, java.net.*, org.w3c.dom.*, javax.xml.parsers.*, java.util.regex.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%@ include file="../inc_main.jsp" %>
<%@ include file="inc_cata.jsp" %>
<%@ include file="inc_util.jsp" %>
<%
	catimage = get_param1(request.getParameter("catimage"),5);
	Dir = get_param1(request.getParameter("Dir"),5);
	if(Dir.equals("")) Dir = "1";

	String start = check_start(request.getParameter("start"));
	String callmode = check_callmode(request.getParameter("callmode"));
	String eclang = check_eclang(request.getParameter("eclang"));
	String userCate = check_cate(request.getParameter("Cate"));
	String um = check_um(request.getParameter("um"),"um");

	pathOfOriCata 	= webSysDir + "/catImage";
	pathOfCata 		= webSysDir + "/catImage" + catimage;
	pathOfDir 		= pathOfCata + "/" + Dir;

	String webCataOriPath	= webServer+"/catImage";
	String webCataPath 	= webServer+"/catImage"+catimage;
	String webDirPath 	= webServer+"/catImage"+catimage+"/"+Dir;
	String webSkinPath 	= webServer+"/skin";
	String selfName		= "search.jsp";
	String incView 		= "c";
	String catadocu		= "ecatalog"+um+".jsp";

	String linkwoDirUrl = "callmode="+callmode+"&catimage="+catimage+"&eclang="+eclang+"&um="+um;
	String linkwDirUrl  = linkwoDirUrl+"&Dir="+Dir;
	String linkwPageUrl = linkwDirUrl+"&start="+start;

	String qtxt = get_qtxt3(request.getParameter("qtxt"),50);
	if(qtxt.equals("")){
		out.print(echo_text("검색어가 없습니다."));
		return;
	}

	File f = new File(pathOfCata + "/catakind.txt");						// check category configuration file
	if(!f.exists()){
		out.print(echo_text("페이지를 표시할 수 없습니다.(catakind)"));
		return;
	}

	if(callmode.equals("normal_unify") || callmode.equals("admin_unify")) unifyCall = "true";

	HashMap CataKind = new HashMap();
	if(!userCate.equals("")){					// must find the first cover page out of the catalogs
		set_cateCond(userCate, CataKind);
		pathOfDir = pathOfCata + "/" + Dir;
		webDirPath = webServer+"/catImage"+catimage+"/"+Dir;
		linkwDirUrl  = linkwoDirUrl+"&Dir="+Dir;
	}
	else{
		String resstr = set_dirCond(CataKind, unifyCall);
		if(!resstr.equals("")){
			out.print(echo_text(resstr));
			return;
		}
	}

	if(!CataKind.get("unifyCond").equals("U")) unifyCall = "false";

	String parentCode = (String)CataKind.get("parentCode");
	if(parentCode.equals("")){										// exists parent category?
		out.print(echo_text("페이지를 표시할 수 없습니다.(parentCode)"));
		return;
	}
	f = new File(pathOfDir + "/ecatalog.txt");
	if(!f.exists()){				// check catalog configuration file
		out.print(echo_text("페이지를 표시할 수 없습니다.(ecatalog)"));
		return;
	}
	if(!callmode.equals("admin") && !callmode.equals("admin_unify") && (CataKind.get("svcCond1").equals("D") || CataKind.get("svcCond2").equals("D"))){
		out.print(echo_text("현재는 서비스가 불가합니다."));
		return;
	}

	HashMap Category = new HashMap();
	set_category(pathOfCata+"/category.txt", (String)CataKind.get("parentCode"), Category);

	// for moving from category to category
	String moveInDist = "";
	f = new File(pathOfOriCata+"/catimage.txt");
	if(unifyCall.equals("true") && f.exists()){
		moveInDist = get_catimage(catimage, "moveInDist");
	}

	// *** ecatalog.txt - check whether the total application of anyone folder's configuration exists
	boolean jbool = false;
	String bascadapt = (String)Category.get("bascadapt");
	if(bascadapt == null) bascadapt = "";
	if(unifyCall.equals("false")){
		if(!bascadapt.equals("")){
			f = new File(pathOfCata + "/" + bascadapt + "/ecatalog.txt");
			if(f.exists()) jbool = true;
		}
	}

	HashMap Ecatalog = new HashMap();
	if(jbool == true){
		set_ecatalog(pathOfCata+"/"+bascadapt+"/ecatalog.txt", Ecatalog);
		Ecatalog.put("pageno", get_ecatalogKey(pathOfDir + "/ecatalog.txt", "pageno"));
	}
	else{
		set_ecatalog(pathOfDir+"/ecatalog.txt", Ecatalog);
	}

	String ini2var = (String)Ecatalog.get("ini2var");
	String xmlExt = (ini2var.length() > 15 && ini2var.charAt(15) == 'T') ? "xml.txt" : ".xml";
	String ini1var = (String)Ecatalog.get("ini1var");

	String cook_cpasswd = "";
	Cookie[] cookies = request.getCookies();
	if(cookies != null){
		for(int i = 0;i < cookies.length;i++){
			Cookie theCookie = cookies[i];
			if(theCookie.getName().equals("cook_cpasswd")) cook_cpasswd = theCookie.getValue();
		}
	}

	String catpasswd = (String)Ecatalog.get("catpasswd");
	if(catpasswd == null) catpasswd = "";
	if(!catpasswd.equals("") && (cook_cpasswd.equals("") || !cook_cpasswd.equals(catpasswd))){				// cat password
		response.sendRedirect("../ecatalog_cpasswd.jsp?"+linkwPageUrl+"&docu=search&qtxt="+qtxt);
		return;
	}

	String logoadapt = (String)Category.get("logoadapt");

	// logo
	String logoFile = "";
	if(!logoadapt.equals("")){
		String file = get_logoFile(pathOfCata+"/"+logoadapt, "splogo");
		if(!file.equals("")) logoFile = "catImage"+catimage+"/"+logoadapt+"/"+file;
	}
	if(logoFile.equals("")){
		String file = get_logoFile(pathOfDir,"splogo");
		if(!file.equals("")) logoFile = "catImage"+catimage+"/"+Dir+"/"+file;
	}
	if(logoFile.equals("")){
		String file = get_logoFile(pathOfOriCata,"splogo");
		if(!file.equals("")) logoFile = "catImage/"+file;
	}

	HashMap Display = new HashMap();
	Display.put("user", get_userSkin((String)Ecatalog.get("askin"), incView));

	HashMap Skin = new HashMap();
	set_skin(um, Skin, (String)Ecatalog.get("askin"), (String)Ecatalog.get("bskin"), Category, Display);

	String display = (String)Skin.get("display");
	String bodyskin = get_bodyskin(display);

	fileExt = get_fileExtension(ini2var.substring(2,3));
	String searchMethod = ini1var.substring(9,10);

	// page variables
	if(!Ecatalog.get("showstart").equals("")) showstart = Integer.parseInt((String)Ecatalog.get("showstart"));
	cataPages = Integer.parseInt((String)Ecatalog.get("pageno"));
	firstPage = 1;
	lastPage = cataPages;

	CataDoc catadoc = new CataDoc();
	catadoc.set_fileseq(ini1var.charAt(11), xmlExt);
	if(unifyCall.equals("true")) catadoc.set_unifyVar((String)Category.get("unif"));

	if(unifyCall.equals("true")){
		for(int i = 0;i < catadoc.dirLen;i++){
			String sdir = catadoc.arDir[i];
			if(catadoc.arSearchMethod[i].equals("F")) catadoc.get_searchTxt(pathOfCata+"/"+sdir+"/search.txt", qtxt);
			else if(catadoc.arSearchMethod[i].equals("A")) catadoc.get_searchXMLSearch(pathOfCata+"/"+sdir+"/search"+xmlExt, qtxt);
			else if(catadoc.arSearchMethod[i].equals("C")) catadoc.get_searchXMLBook(pathOfCata+"/"+sdir+"/bookdata"+xmlExt, i, qtxt);
			// U는 반영되지 않음.
		}
	}	
	else if(catadoc.fileSeq.equals("X")){
		catadoc.get_searchXMLBook(pathOfDir+"/bookdata"+xmlExt, 0, Dir);
	}
	else if(searchMethod.equals("F")){					// search.txt
		f = new File(pathOfDir+"/search.txt");
		if(!f.exists()){
			out.print(echo_text("The 'search.txt' file does not exist."));
			return;
		}
		catadoc.get_searchTxt(pathOfDir+"/search.txt", qtxt);
	}
	else if(searchMethod.equals("A")){
		f = new File(pathOfDir+"/search"+xmlExt);
		if(!f.exists()){
			out.print(echo_text("The 'search.xml' file does not exist."));
			return;
		}
		catadoc.get_searchXMLSearch(pathOfDir+"/search"+xmlExt, qtxt);
	}

	int lenc = catadoc.arKey.size();

	if(Ecatalog.get("titlebar") == null || Ecatalog.get("titlebar").equals("")) Ecatalog.put("titlebar","Electronic Catalog(전자 카탈로그) - 검색 결과");

	String default_font = "굴림";
	String mainTitle = "검색 결과";
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=10.0, user-scalable=yes">
<title><%=Ecatalog.get("titlebar")%> </title>
<link rel="stylesheet" type="text/css" href="<%=webServer%>/access/style.css">
<style type="text/css">
	*{font-size:13px;}
</style>
</head>

<body class="body<%=bodyskin%>">

<%
	String dispvar = "NNNNNNNNNNNNNNNNNNNNN";
	String displayFile = "./inc_display"+display+".jsp";
%>
<jsp:include page="<%=displayFile%>" flush="true">
	<jsp:param name="incview" value="<%=incView%>"/>
	<jsp:param name="callmode" value="<%=callmode%>"/>
	<jsp:param name="webServer" value="<%=webServer%>"/>
	<jsp:param name="Dir" value="<%=Dir%>"/>
	<jsp:param name="catimage" value="<%=catimage%>"/>
	<jsp:param name="selfName" value="<%=selfName%>"/>
	<jsp:param name="xmlExt" value=""/>
	<jsp:param name="combokind" value=""/>
	<jsp:param name="currCode" value=""/>

	<jsp:param name="showPage" value=""/>
	<jsp:param name="cataPages" value="<%=cataPages%>"/>
	<jsp:param name="firstPage" value="<%=firstPage%>"/>
	<jsp:param name="lastPage" value="<%=lastPage%>"/>
	<jsp:param name="currentPage" value="0"/>
	<jsp:param name="showstart" value="<%=showstart%>"/>
	<jsp:param name="ini1var" value="<%=ini1var%>"/>
	<jsp:param name="ini2var" value="<%=ini2var%>"/>
	<jsp:param name="dispvar" value="<%=dispvar%>"/>

	<jsp:param name="dirName" value=""/>
	<jsp:param name="logoFile" value=""/>
	<jsp:param name="logolink" value=""/>
	<jsp:param name="downfile" value=""/>
	<jsp:param name="downtext" value=""/>
	<jsp:param name="linkwPageUrl" value="<%=linkwPageUrl%>"/>
	<jsp:param name="linkwDirUrl" value="<%=linkwDirUrl%>"/>

	<jsp:param name="leftImageName" value=""/>
	<jsp:param name="leftImageFile" value=""/>
	<jsp:param name="leftBigImgUrl" value=""/>
	<jsp:param name="rightImageName" value=""/>
	<jsp:param name="rightImageFile" value=""/>
	<jsp:param name="rightBigImgUrl" value=""/>

	<jsp:param name="mainTitle" value="<%=URLEncoder.encode(mainTitle,\"utf-8\")%>"/>
	<jsp:param name="catadocu" value="<%=catadocu%>"/>
</jsp:include>


<div id="mainsect" class="main002">

<div class="search5cD<%=bodyskin%>">
	<form name="searchform" method="get" action="search.jsp" title="페이지 검색">
	<input type="text" name="qtxt" class="searchtxtD001" tabindex="17" title="검색어 입력">
	<input type="image" src="<%=webServer%>/access/pix/searchbtn.gif" class="searchbtnD001" tabindex="18" title="검색">
	<input type="hidden" name="Dir" value="<%=Dir%>">
	<input type="hidden" name="catimage" value="<%=catimage%>">
	<input type="hidden" name="callmode" value="<%=callmode%>">
	<input type="hidden" name="start" value="<%=start%>">
	<input type="hidden" name="um" value="<%=um%>">
	</form>
</div>

<div class="clearx"></div>

<% if(lenc == 0){ %>
	<p class="searchres c<%=bodyskin%>"><span class="found">'<%=qtxt%>'</span>에 대한 검색결과가 없습니다.</p>
<% } else{ %>
	<p class="searchres c<%=bodyskin%>"><span class="found">'<%=qtxt%>'</span>에 대한 검색결과 : <%=lenc%>개</p>
<% } %>

<ul class="contlist<%=bodyskin%>">
<%
	String AddLink = linkwPageUrl+"&qtxt="+qtxt;
	int Punit = 10;
	String tPg = get_param1(request.getParameter("Pg"),5);
	int[] bvar = db_board_start(lenc, Punit, tPg);
	int bb = bvar[0];
	int bf = bvar[1];
	int Pg = bvar[2];
	int Pnum = bvar[3];

	int tabindex = 3;
	for(int i = 0;i < lenc;i++){
		int counts = i + 1;

		if(bb < counts && counts <= bf){
			String skey = (String)catadoc.arKey.get(i);
			String sval = (String)catadoc.arVal.get(i);
			String href = catadocu + "?" + linkwDirUrl + "&start=" + skey;

			sval = sval.replaceAll(qtxt, "<span class='found'>"+qtxt+"</span>");

			String showpage = skey;
			if(showstart > 0){
				int npage = Integer.parseInt(skey);
				npage = get_showPageFromReal(npage);
				showpage = (npage < 1) ? "" : "" + npage;
			}

			out.print("<li><p class='listsubj"+bodyskin+" liststep1'><a href='"+href+"' tabindex='"+tabindex+"' class='c"+display+"'>");
			out.print("<span class=\"lihead\">["+showpage+"페이지]</span> "+sval+"</a></p>\n");
			out.print("<p class='listmark"+bodyskin+"'><img src='"+webServer+"/access/pix/index_list.png' alt='이동'></p></li>\n");

			tabindex++;
		}
	}
%>
</ul>
</div>

<div class="clearx"></div>

<%
	String pagingSkin = bodyskin;
	String pagingPixDir = "";
	if(pagingSkin.equals("002")) pagingPixDir = "2";
%>
<div id="pagingsect" class="pagingsect<%=pagingSkin%>">

<!--<p class="b2paging_img001"><a href="<%=selfName%>?<%=AddLink%>&Pg=1"><img 
src="<%=webServer%>/access/pix/page_first.gif" width="35" height="23" tabindex="<%=tabindex++%>" alt="처음 페이지"></a></p>-->

<% int prevNum = (Pg > 1) ? Pg - 1 : 1; %>
<p class="pgimg<%=pagingSkin%>"><a href="<%=selfName%>?<%=AddLink%>&Pg=<%=prevNum%>"><img
src="<%=webServer%>/access/pix<%=pagingPixDir%>/page_prev.gif" tabindex="<%=tabindex++%>" alt="이전 페이지"></a></p>

<%
	if(Pnum == 1 || Pg == 0) out.print("");
	if(!AddLink.equals("")) AddLink = AddLink+"&amp;";

	int fn = (int)((Pg-1)/10 + 1) * 10;
	for(int i = fn-9;i <= Pnum;i++){
		if(i == Pg) out.print("<p class='pgtxt"+pagingSkin+"-curr currpage'>"+i+"</p>");
		else out.print("<p class='pgtxt"+pagingSkin+"'><a href='"+selfName+"?"+AddLink+"Pg="+i+"' class='nextpage'>"+i+"</a></p>");
		if(i >= fn) break;
	}
%>

<% int nextNum = (Pg < Pnum) ? Pg + 1 : Pg; %>
<p class="pgimg<%=pagingSkin%>"><a href="<%=selfName%>?<%=AddLink%>&Pg=<%=nextNum%>"><img
src="<%=webServer%>/access/pix<%=pagingPixDir%>/page_next.gif" tabindex="<%=tabindex++%>" alt="다음 페이지"></a></p>

<!--<p class="b2paging_img001"><a href="<%=selfName%>?<%=AddLink%>&Pg=<%=Pnum%>"><img
src="<%=webServer%>/access/pix/page_last.gif" width="35" height="23" tabindex="<%=tabindex++%>" alt="마지막 페이지"></a></p>-->

</div>
</body>
</html>
