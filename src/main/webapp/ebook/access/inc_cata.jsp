<%!
	static int cataPages = 0;
	static int firstPage = 0;
	static int lastPage = 0;
	static int showstart = 0;
	static String onesmc = "true";
	static String s000File = "";
	static String unifyCall = "false";
	static String fileExt = "";

public class CataDoc{
	public String fileSeq = "N";
	public String streamCharset = "utf-8";

	public String[][] arFileConf;
	public String[][] arFileName;
	public String[][] arTitle;
	public String[][] arSearch;
	public String[][] arBodyCont;

	public String[] loadFileData;
	public String[] arDir;
	public String[] arFileseq;
	public String[] arSearchMethod;
	public int[] arPageNo;
	public int[] arPageAccuNo;

	public Vector arKey;
	public Vector arVal;
	public String[] searchStr;

	public int dirLen = 0;
	public int thisPageNo = 0;

	public String realDir = "";
	public int realPageNo = 0;

	public CataDoc(){
		searchStr = new String[2];
		searchStr[0] = "";
		searchStr[1] = "";
		arKey = new Vector();
		arVal = new Vector();
	}

	public void set_fileseq(char fseq, String xmlExt){
		if(fseq == 'F'){
			File f = new File(pathOfDir+"/fileseq.txt");
			if(f.exists() && f.length() > 0) fileSeq = "F";
		}
		else if(fseq == 'X'){
			File f = new File(pathOfDir+"/bookdata"+xmlExt);
			if(f.exists() && f.length() > 0) fileSeq = "X";
		}
	}

	public String set_unifyVar(String unifstr){
		if(unifstr.equals("")) return "there is no unifstr!";
		String[] aaa = mysplit(unifstr,"\t");		// category \t unif \t totalpages \t folder \t pageno \t fileseq \t alink \t pagesound \t pic \t search

		int tpageno = Integer.parseInt(aaa[2]);

		String[] bbb = mysplit(aaa[4], "|");
		String[] ccc = mysplit(aaa[3], "|");
		String[] ddd = mysplit(aaa[5], "|");
		String[] eee = mysplit(aaa[9], "|");

		this.dirLen = bbb.length;
		arPageNo = new int[this.dirLen];
		arPageAccuNo = new int[this.dirLen];

		int accuno = 0;
		for(int j = 0;j < this.dirLen;j++){
			this.arPageNo[j] = Integer.parseInt(bbb[j]);
			accuno += this.arPageNo[j];
			this.arPageAccuNo[j] = accuno;

			this.arDir[j] = ccc[j];
			this.arFileseq[j] = ddd[j];
			this.arSearchMethod[j] = eee[j];
		}

		this.thisPageNo = arPageNo[0];
		cataPages = tpageno;

		if(onesmc.equals("true")) lastPage = cataPages;
		else{
			if((firstPage+cataPages)%2 == 0) lastPage = cataPages + 1;
			else lastPage = cataPages;
		}
		return "";
	}

	public String get_unifyFileseqImageName(int seq, int npage, String seqkind){
		String currDir = arDir[seq];

		if(loadFileData[seq] == null) loadFileData[seq] = "";

		if(loadFileData[seq].equals("X")) return "";
		else if(loadFileData[seq].equals("")){
			if(seqkind.equals("F")){
				File f = new File(pathOfCata+"/"+currDir+"/fileseq.txt");
				if(!f.exists()){
					loadFileData[seq] = "X";
					return "";
				}
				load_fileseqData(pathOfCata+"/"+currDir+"/fileseq.txt", seq);
			}
			else if(seqkind.equals("X")){
				File f = new File(pathOfCata+"/"+currDir+"/bookdata.xml");
				if(!f.exists()){
					loadFileData[seq] = "X";
					return "";
				}
				load_filexmlData(pathOfCata+"/"+currDir+"/bookdata.xml", seq);
			}
		}

		String fileconf = arFileConf[seq][npage-1];
		String filename = arFileName[seq][npage-1];

		if(fileconf.charAt(0) == '2') return "";				// blank
		return filename;
	}

	// link
	public Vector set_linkData(String currDir, String fname) {
		Vector arLink = new Vector();
		File f = new File(pathOfCata+"/"+currDir+"/alink.txt");
		if(!f.exists()) return arLink;

		String source = "";
		FileInputStream fin;
		BufferedReader bin;
		try{
			fin = new FileInputStream(f);
			bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				int in = source.indexOf("\t");
				String skey = source.substring(0, in);
				if(in == -1) continue;

				if(skey.equals(fname)) arLink.addElement(source);
			}
			fin.close();
			bin.close();
		}
		catch(Exception e) {
			return arLink;
		}

		return arLink;
	}

	public Vector set_linkDataAll(){
		Vector arLink = new Vector();
		File f = new File(pathOfDir+"/alink.txt");
		if(!f.exists()) return arLink;

		String source = "";
		FileInputStream fin;
		BufferedReader bin;
		try{
			fin = new FileInputStream(f);
			bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				arLink.add(source);
			}
			fin.close();
			bin.close();
		}
		catch(Exception e) {
			return arLink;
		}

		return arLink;
	}

	public int get_currentPage(String spage){
		if(spage.equals("")) return firstPage;

		int npage = Integer.parseInt(spage);
		if(onesmc.equals("true")) return npage;
		else if(npage < firstPage) return firstPage;
		else if(npage >= lastPage) return lastPage - 1;
		else if(npage%2 != firstPage) return npage - 1;
		return npage;
	}

	public String[] get_leftImageName(String prefix, int npage){
		String[] resStr = {Dir,""};			// folder, filename

		if(npage == 0){
			if(!s000File.equals("")){
				if(prefix.equals("s")) resStr[1] = s000File;
				else resStr[1] = s000File.substring(1);
			}
			if(!resStr[1].equals("")) resStr[1] = prefix+resStr[1];
			return resStr;
		}

		int seq = 0;
		if(unifyCall.equals("true")){
			seq = get_diridxFromPage(npage);

			String realDir = Dir;
			if(!arDir[seq].equals(Dir)){
				realDir = arDir[seq];
				int rpage = get_realPageNo(seq, npage);

				resStr[0] = realDir;
				if(arFileseq[seq].equals("F") || arFileseq[seq].equals("X")) resStr[1] = get_unifyFileseqImageName(seq, rpage, arFileseq[seq]);
				else resStr[1] = getFileName(rpage);

				if(!resStr[1].equals("")) resStr[1] = prefix+resStr[1];
				return resStr;
			}
		}

		// current folder
		if(fileSeq.equals("X")) resStr[1] = get_unifyFileseqImageName(seq, npage, "X");
		else if(fileSeq.equals("F")) resStr[1] = get_unifyFileseqImageName(seq, npage, "F");
		else resStr[1] = getFileName(npage);

		if(!resStr[1].equals("")) resStr[1] = prefix+resStr[1];
		return resStr;
	}

	public String[] get_expImageName(String prefix, int npage){
		String[] resStr = {Dir,""};			// folder, filename
		int seq = 0;
		if(unifyCall.equals("true")){
			seq = get_diridxFromPage(npage);

			String realDir = Dir;
			if(!arDir[seq].equals(Dir)){
				realDir = arDir[seq];
				int rpage = get_realPageNo(seq, npage);

				resStr[0] = realDir;
				if(arFileseq[seq].equals("F") || arFileseq[seq].equals("X")) resStr[1] = get_unifyFileseqImageName(seq, rpage, arFileseq[seq]);
				else resStr[1] = getFileName(rpage);

				if(!resStr[1].equals("")) resStr[1] = prefix+resStr[1];
				return resStr;
			}
		}

		if(fileSeq.equals("X")) resStr[1] = get_unifyFileseqImageName(seq, npage, "X");
		else if(fileSeq.equals("F")) resStr[1] = get_unifyFileseqImageName(seq, npage, "F");
		else resStr[1] = getFileName(npage);

		if(!resStr[1].equals("")) resStr[1] = prefix+resStr[1];
		return resStr;
	}

	// fileseq string
	public void load_fileseqData(String path, int seq){
		loadFileData[seq] = "Y";
		int npage = arPageNo[seq];

		arFileConf[seq] = new String[npage];
		arFileName[seq] = new String[npage];

		String source = "";
		File f = new File(path);
		FileInputStream fin;
		BufferedReader bin;
		try{
			int i = 0;
			fin = new FileInputStream(f);
			bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				int in = source.indexOf(":");
				if(in == -1) continue;

				arFileConf[seq][i] = source.substring(0, in);
				arFileName[seq][i] = source.substring(in+1);
				i++;
			}
			fin.close();
			bin.close();
		}
		catch(Exception e) {
			return;
		}
	}

	public String get_xmlCont(String realDir, int npage){
		int seq = 0;
		int tpage = npage;
		if(unifyCall.equals("true")){
			seq = get_diridxFromPage(npage);
			if(!arDir[seq].equals(realDir)){
				realDir = arDir[seq];
				tpage = get_realPageNo(seq, npage);
			}
		}

		return arBodyCont[seq][tpage];
	}

	// filexml string
	public void load_filexmlData(String path, int seq){
		loadFileData[seq] = "Y";
		int npage = arPageNo[seq];

		arFileConf[seq] = new String[npage];
		arFileName[seq] = new String[npage];
		arTitle[seq] = new String[npage];
		arSearch[seq] = new String[npage];
		arBodyCont[seq] = new String[npage];

		String source = "";
		File f = new File(path);
		StringBuffer cont = new StringBuffer();
		FileInputStream fin;
		BufferedReader bin;
		try{
			fin = new FileInputStream(f);
		 	bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				//source = strTrimEach(source);
				if(source.equals("")) continue;
				cont.append(source).append("\n");
			}
			fin.close();
			bin.close();
		}
		catch(Exception e) {
			return;
		}

		String cont4 = cont.toString();

		try{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(f);

			NodeList nl = document.getElementsByTagName("page");
			int alen = nl.getLength();

			for(int i = 0;i < alen;i++){
				int no = 1;
				String stitle = "";
				String image = "";
				String search = "";

				Node node = nl.item(i);
				NodeList nl2 = node.getChildNodes();

				NamedNodeMap attrs = node.getAttributes();  
				for(int j = 0;j < attrs.getLength();j++) {
					Attr attribute = (Attr)attrs.item(j);     
					String sname = attribute.getName();
					String svalu = attribute.getValue();

					if(sname.equals("no")) no = Integer.parseInt(svalu) - 1;				// pno 기준이므로
					else if(sname.equals("title")) stitle = svalu;
					else if(sname.equals("image")) image = svalu;
					else if(sname.equals("search")) search = svalu;
				}

				String qstr = "";
				int blen = nl2.getLength();
				for(int j = 0;j < blen;j++){
					Node node2 = nl2.item(j);
					if(node2 == null) continue;

					String tagName = node2.getNodeName();
					if(tagName == null) tagName = "";

					if(node2.getFirstChild() == null) continue;

					String tagValue = node2.getFirstChild().getNodeValue();
					if(tagValue == null) tagValue = "";

					if(tagName.equals("content")) qstr = tagValue;
				}

				arFileConf[seq][no] = "10000";
				arFileName[seq][no] = image;
				arTitle[seq][no] = stitle;
				arSearch[seq][no] = search;
				arBodyCont[seq][no] = qstr;
			}
		}
		catch(Exception e){
			return;
		}
	}

	public int get_diridxFromPage(int n){
		for(int i = 0;i < dirLen;i++){
			if(n <= arPageAccuNo[i]){
				return i;
			}
		}
		return 0;
	}

	public int get_realPageNo(int seq, int n){
		if(seq == 0) return n;
		return (n - arPageAccuNo[seq-1]);
	}

	public void get_searchXMLSearch(String path, String qtxt){
		File f = new File(path);
		if(!f.exists()) return;

		String source = "";
		StringBuffer cont = new StringBuffer();
		FileInputStream fin;
		BufferedReader bin;
		try{
			fin = new FileInputStream(f);
		 	bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				//source = strTrimEach(source);
				if(source.equals("")) continue;
				cont.append(source).append("\n");
			}
			fin.close();
			bin.close();
		}
		catch(Exception e) {
			return;
		}

		Pattern p = Pattern.compile("<no>(.*)</no>([ \t\r\n]+)<search><!\\[CDATA\\[(.*)\\]\\]></search>");
		Matcher m = p.matcher(cont.toString());
		while(m.find()){
			String pageno = m.group(1);
			String qstr = m.group(3);

			int pos = qstr.indexOf(qtxt);
			if(pos == -1) continue;

			arKey.addElement(pageno);
			arVal.addElement(set_resstr(qstr, pos));
		}
	}

	// for search string
	public void get_searchXMLBook(String path, int seq, String qtxt){
		File f = new File(path);
		if(!f.exists()) return;

		load_filexmlData(path, seq);

		int alen = arSearch[seq].length;
		for(int i = 0;i < alen;i++){
			String t_search = arSearch[seq][i];
			if(t_search.equals("undefined")) t_search = arBodyCont[seq][i];

			int pos = t_search.indexOf(qtxt);
			if(pos == -1) continue;

			arKey.addElement(Integer.toString(i+1));
			arVal.addElement(set_resstr(t_search,pos));
		}
	}

	public String Replace_cdataTag(String s){
		s = s.replaceAll("<!\\[CDATA\\[", "");
		s = s.replaceAll("\\]\\]>", "");
		return s;
	}

	public void get_searchTxt(String path, String qtxt){
		File f = new File(path);
		if(!f.exists()) return;

		String source = "";
		FileInputStream fin;
		BufferedReader bin;
		try{
			fin = new FileInputStream(f);
		 	bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				int in = source.indexOf(":");
				if(in == -1) continue;

				String skey = source.substring(0, in);
				String sval = source.substring(in+1);

				if(sval.indexOf(qtxt) < 0) continue;

				arKey.addElement(skey);
				arVal.addElement(sval);
			}
			fin.close();
			bin.close();
		}
		catch(Exception e) {
			return;
		}
	}

	public String set_resstr(String s, int pos){
		s = s.replaceAll("<","&lt;");
		int clen = s.length();
		if(clen <= 250) return s;

		int starti = pos - 125;
		if(starti < 0) starti = 0;

		int endi = starti + 250;
		if(endi > clen) endi = clen;

		if(starti == 0) s = s.substring(0, endi);
		else s = s.substring(starti, endi);

		if(endi != clen) return s+"...";

		return s;
	}
}
%>
