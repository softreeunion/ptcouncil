<%@ page import="java.io.*, java.util.*, java.net.*, org.w3c.dom.*, javax.xml.parsers.*, java.util.regex.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%@ include file="../inc_main.jsp" %>
<%@ include file="inc_cata.jsp" %>
<%@ include file="inc_util.jsp" %>
<%
	catimage = get_param1(request.getParameter("catimage"),5);
	Dir = get_param1(request.getParameter("Dir"),5);
	if(Dir.equals("")) Dir = "1";

	String start = check_start(request.getParameter("start"));
	String callmode = check_callmode(request.getParameter("callmode"));
	String eclang = check_eclang(request.getParameter("eclang"));
	String userCate = check_cate(request.getParameter("Cate"));
	String um = check_um(request.getParameter("um"),"um");

	String width = get_param1(request.getParameter("w"),5);
	String height = get_param1(request.getParameter("h"),5);

	String fd = request.getParameter("fd");
	if(fd == null) fd = "";
	if(fd.equals("") || check_dirname(fd) == false){
		out.print(echo_text("데이터 형식이 맞지 않습니다(디렉토리 이름)."));
		return;
	}
	String img = request.getParameter("img");
	if(img == null) img = "";
	if(img.equals("") || check_fname(img) == false){
		out.print(echo_text("데이터 형식이 맞지 않습니다(파일 이름)."));
		return;
	}

	pathOfOriCata 	= webSysDir + "/catImage";
	pathOfCata 		= webSysDir + "/catImage" + catimage;
	pathOfDir 		= pathOfCata + "/" + Dir;

	String webCataOriPath	= webServer+"/catImage";
	String webCataPath 	= webServer+"/catImage"+catimage;
	String webDirPath 	= webServer+"/catImage"+catimage+"/"+Dir;
	String webSkinPath 	= webServer+"/skin";
	String selfName		= "bigimage.jsp";
	String incView 		= "c";
	String catadocu		= "ecatalog"+um+".jsp";

	String linkwoDirUrl = "callmode="+callmode+"&catimage="+catimage+"&eclang="+eclang+"&um="+um;
	String linkwDirUrl  = linkwoDirUrl+"&Dir="+Dir;
	String linkwPageUrl = linkwDirUrl+"&start="+start;

	File f = new File(pathOfCata + "/" + fd + "/" + img);						// check category configuration file
	if(!f.exists()){
		out.print(echo_text("파일이 존재하지 않습니다.(image file)"));
		return;
	}

	String default_font = "굴림";
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=10.0, user-scalable=yes">
<title>Electronic Catalog(전자 카탈로그) - <%=start%>쪽 확대 이미지</title>
<link rel="stylesheet" type="text/css" href="<%=webServer%>/access/style.css">
<style type="text/css">
	*{font-size:9pt;font-family:<%=default_font%>;}
</style>
</head>

<body>

<p style="margin:0;"><a href="<%=catadocu%>?<%=linkwPageUrl%>" title="이전">
<img src="<%=webCataPath%>/<%=fd%>/<%=img%>" width="<%=width%>" height="<%=height%>" 
alt="<%=start%>쪽 확대 이미지"></a></p>

</body>
</html>
