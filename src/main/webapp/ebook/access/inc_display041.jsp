<%@ page import="java.io.*, java.util.*, java.net.*, org.w3c.dom.*, javax.xml.parsers.*, java.util.regex.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%@ include file="inc_cata.jsp" %>
<%@ include file="inc_util.jsp" %>
<%
	request.setCharacterEncoding("utf-8");

	String incView = request.getParameter("incview");
	String webServer = request.getParameter("webServer");

	String callmode = request.getParameter("callmode");
	String Dir = request.getParameter("Dir");
	String catimage = request.getParameter("catimage");
	String selfName = request.getParameter("selfName");
	String xmlExt = request.getParameter("xmlExt");
	String combokind = request.getParameter("combokind");
	String currCode = request.getParameter("currCode");

	cataPages = Integer.parseInt(request.getParameter("cataPages"));
	firstPage = Integer.parseInt(request.getParameter("firstPage"));
	lastPage = Integer.parseInt(request.getParameter("lastPage"));
	showstart = Integer.parseInt(request.getParameter("showstart"));

	String showPage = request.getParameter("showPage");
	String ini1var = request.getParameter("ini1var");
	String ini2var = request.getParameter("ini2var");
	String dispvar = request.getParameter("dispvar");

	String dirName = URLDecoder.decode(request.getParameter("dirName"),"utf-8");
	String logoFile = request.getParameter("logoFile");
	String logolink = request.getParameter("logolink");
	String downfile = request.getParameter("downfile");
	String downtext = request.getParameter("downtext");
	String linkwPageUrl = request.getParameter("linkwPageUrl");
	String linkwDirUrl = request.getParameter("linkwDirUrl");

	String leftImageName = request.getParameter("leftImageName");
	String leftImageFile = request.getParameter("leftImageFile");
	String leftBigImgUrl = request.getParameter("leftBigImgUrl");
	String rightImageName = request.getParameter("rightImageName");
	String rightImageFile = request.getParameter("rightImageFile");
	String rightBigImgUrl = request.getParameter("rightBigImgUrl");

	onesmc = (dispvar.charAt(3) == 'Y') ? "true" : "false";

	pathOfOriCata 	= webSysDir + "/catImage";
	pathOfCata 		= webSysDir + "/catImage" + catimage;
	pathOfDir 		= pathOfCata + "/" + Dir;

	String webCataOriPath	= webServer+"/catImage";
	String webCataPath 	= webServer+"/catImage"+catimage;
	String webDirPath 	= webServer+"/catImage"+catimage+"/"+Dir;
	String webSkinPath 	= webServer+"/skin";
%>
<!-- logo -->
<% if(!logoFile.equals("")){ %>
<div id="logosect"><% if(!logolink.equals("")){ %><a href="<%=logolink%>" tabindex="2" title="로고 링크" target="_blank"><% } %>
<img src="<%=webServer%>/<%=logoFile%>" alt="로고"><% if(!logolink.equals("")){ %></a><% } %></div>
<% } %>

<% if(incView.equals("s")){ %>
<% int currentPage = Integer.parseInt(request.getParameter("currentPage")); %>
<!-- top menu (inc_display041.jsp) -->
<div id="navisect" class="naviD041">
<div id="popbtn" class="popbtnD041"><a href="javascript:popClick();" title="이동메뉴"><img src="<%=webServer%>/skin5/mm041/m_morebtn.png" alt="이동메뉴"></a></div>
<div id="turnbtn" class="turnbtnD041"><a href="javascript:linkExpand('e');" title="펼쳐보기"><img id="turnbtnpix" src="<%=webServer%>/skin5/mm041/m_expandbtn.png" alt="펼쳐보기"></a></div>
</div>

<!-- main image -->
<div id="smcsect" class="smc5sD001">

<% if(onesmc.equals("true")){ %>
<div id="smcont1" class="smcont5s smsize">
<a href="javascript:linkBig();" tabindex="7" title="페이지 확대"><img id="smcImgL1" class="smcImg5s smsize"
  src="<%=webCataPath%>/<%=leftImageFile%>" alt="페이지"></a>
<% if(dispvar.charAt(4) == 'Y'){ // enlargeClick %>
<a href="javascript:linkBig();" tabindex="8" title="페이지 확대"><img src="<%=webServer%>/access/pix/enlarge.gif" class="smcenbtn" alt="확대"></a>
<% } %>
</div>

<% } else{ %>
<div id="smcont1" class="smcont5s sm2size">
<% if(leftImageName.equals("")){ %>
<img id="smcImgL1" class="smcImg5s smsize" src="<%=webSkinPath%>/xull.png" alt="">
<% } else{ %>
<a href="<%=leftBigImgUrl%>" tabindex="13" title="페이지 확대"><img id="smcImgL1" class="smcImg5s smsize" src="<%=webCataPath%>/<%=leftImageFile%>" alt="페이지"></a>
<% } %>

<% if(rightImageName.equals("")){ %>
<img id="smcImgR1" class="smcImg5s sm3size" src="<%=webSkinPath%>/xull.png" alt="">
<% } else{ %>
<a href="<%=rightBigImgUrl%>" tabindex="14" title="페이지 확대"><img id="smcImgR1" class="smcImg5s sm3size" src="<%=webCataPath%>/<%=rightImageFile%>" alt="페이지"></a>

<% if(dispvar.charAt(4) == 'Y'){ // enlargeClick %>
<a href="<%=rightBigImgUrl%>" tabindex="15" title="페이지 확대"><img src="<%=webServer%>/access/pix/enlarge.gif" class="smcenbtn" alt="확대"></a>
<% } %>
<% } %>

</div>
<% } %>

<% if(dispvar.charAt(6) == 'Y'){ // dragOver %>
<div id="smcont2" class="smcont5s"></div>
<div id="smcont3" class="smcont5s"></div>
<% } %>

<div id="smclinkL" class="smcImg5s"></div>
<div id="smclinkR" class="smcImg5s sm3size"></div>
</div>

<!-- paging sect -->
<% if(dispvar.charAt(7) == 'Y'){ // pageIcon %>
<div id="pagingiconsect" class="pagingiconD001"></div>
<% } %>

<!-- prev and next button in body -->
<div id="bodyprevbtn" class="bodyprevbtn001" onclick="linkPrev();"><img src="<%=webServer%>/access/pix/body_prev.png" width="18" height="87" alt="이전 페이지"></div>
<div id="bodynextbtn" class="bodynextbtn001" onclick="linkNext();"><img src="<%=webServer%>/access/pix/body_next.png" width="18" height="87" alt="다음 페이지"></div>

<!-- mmpop menu -->
<div id="popsect" class="popD041" style="visibility:hidden;">
<div id="popclosebtn"><a href="javascript:popClick();" title="닫기"><img src="<%=webServer%>/skin5/mm041/m_popclosebtn.png" width="24" height="24" alt="닫기"></a></div>
<ul class="pop_ulD041">
<li id="indexbtn"><a href="contents.jsp?<%=linkwPageUrl%>" title="목차 보기">
	<img src="<%=webServer%>/skin5/mm041/m_indexbtn.png" width="24" height="24" alt="목차"><p>목차</p></a></li>
<li id="expbtn"><a href="explorer.jsp?<%=linkwPageUrl%>" title="탐색기 보기">
	<img src="<%=webServer%>/skin5/mm041/m_expbtn.png" width="24" height="24" alt="탐색기"><p>썸네일</p></a></li>
<li id="spacerbtn"><a href="javascript:linkBookmark();" title="책갈피에 추가">
	<img src="<%=webServer%>/skin5/mm041/m_spacerbtn.png" width="24" height="24" alt="책갈피"><p>책갈피</p></a></li>
<li id="combobtn"><a href="combo.jsp?<%=linkwPageUrl%>" title="다른 카탈로그 이동 페이지">
	<img src="<%=webServer%>/skin5/mm041/m_combobtn.png" width="24" height="24" alt="다른 카탈로그"><p>다른 eBook 보기</p></a></li>
</ul>

<% if(dispvar.charAt(5) == 'Y'){ %>
<div id="searchsect" class="searchD041">
	<form name="searchform" method="get" action="search.jsp" title="페이지 검색">
	<input type="search" class="searchtxtD041" name="qtxt" title="검색어 입력">
	<input type="image" class="searchbtnD041" src="<%=webServer%>/skin5/mm041/m_searchbtn.png" title="검색">
	<input type="hidden" name="Dir" value="<%=Dir%>">
	<input type="hidden" name="catimage" value="<%=catimage%>">
	<input type="hidden" name="callmode" value="<%=callmode%>">
	<input type="hidden" name="start" value="<%=currentPage%>">
	<input type="hidden" name="um" value="s">
</form>
</div>
<% } %>

<!-- lower more sns group menu -->
<div id="snssect" class="sns_navD041">
<ul class="sns_ulD041">
<li id="facebookbtn" class="facebookbtnD041"><a href="javascript:sns_linkopen('facebook')" title="페이스북 공유"><img src="<%=webServer%>/skin5/mm041/m_facebookbtn.png" width="10" height="20" alt="페이스북"></a></li>
<li id="twitterbtn" class="twitterbtnD041"><a href="javascript:sns_linkopen('twitter')" title="트위터 공유"><img src="<%=webServer%>/skin5/mm041/m_twitterbtn.png" width="28" height="20" alt="트위터"></a></li>
<li id="kakaobtn" class="kakaobtnD041"><a href="javascript:sns_linkopen('kakao')" title="카카오 공유"><img src="<%=webServer%>/skin5/mm041/m_kakaobtn.png" width="21" height="20" alt="카카오"></a></li>
</ul>
<% if(dispvar.charAt(9) == 'Y'){ %>
<p id="downbtn" class="downbtnD041"><a href="<%=webDirPath%>/<%=downfile%>" download="<%=webDirPath%>/<%=downfile%>" 
  title="파일 다운로드"><img src="<%=webServer%>/skin5/mm041/m_downbtn.png" width="21" height="20" alt="파일 다운로드"></a></p>
<% } %>
</div>
</nav>

<script>
function popClick(){
	if(document.getElementById('popsect').style.visibility == "hidden"){
		document.getElementById('popsect').style.visibility = "visible";
	}
	else{
		document.getElementById('popsect').style.visibility = "hidden";
	}
}
function show_pageNumber(){
}
</script>

<% } else if(incView.equals("c")){ %>
<%
	String mainTitle = URLDecoder.decode(request.getParameter("mainTitle"),"utf-8");
	String catadocu = request.getParameter("catadocu");
%>
<div id="navisect" class="naviD041">
	<h1><%=mainTitle%></h1>
	<p class="backbtnD041"><a href="<%=catadocu%>?<%=linkwPageUrl%>"><img src="<%=webServer%>/skin5/mm041/prevbtn.png" 
	width="25" height="25" alt="이전 페이지로" tabindex="2"></a></p>
</div>

<% } else if(incView.equals("e")){ %>
<%
	String smImageWidth = request.getParameter("smImageWidth");
	String smImageHeight = request.getParameter("smImageHeight");
%>
<div id="navisect" class="naviD041">
	<h1>펼쳐보기</h1>
	<div id="turnbtn" class="turnbtnD041"><a href="javascript:linkExpand('');" title="넘겨보기"><img id="turnbtnpix" src="<%=webServer%>/skin5/mm041/m_turnbtn.png" alt="넘겨보기"></a></div>
</div>

<!-- main image -->
<div id="smcsect" class="smc5sD001">
<%
	for(int i = 1;i <= cataPages;i++){
		out.print("<div id='epdiv"+i+"' class=\"expandiv smsize\" data-load='0'></div>\n");
	}
%>
</div>

<% } %>
