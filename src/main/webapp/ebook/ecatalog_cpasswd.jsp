<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%@ include file="inc_skin.jsp" %>
<%
	catimage = get_param1(request.getParameter("catimage"),5);
	Dir = get_param1(request.getParameter("Dir"),5);
	if(Dir.equals("")) Dir = "1";

	String start = check_start(request.getParameter("start"));
	String callmode = check_callmode(request.getParameter("callmode"));
	String eclang = check_eclang(request.getParameter("eclang"));
	String userCate = check_cate(request.getParameter("Cate"));
	String um = check_um(request.getParameter("um"),"um");

	String qtxt = get_qtxt3(request.getParameter("qtxt"),50);
	String docu = get_param3(request.getParameter("docu"),10);
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Electronic Catalog(전자 카탈로그) - 비밀번호 입력</title>
<style>
	html, body{margin:0px;overflow:hidden;width:100%;height:100%;}
	*{font-size:9pt;}
</style>
<script>
function onload_func(){
	var aw = document.body.clientWidth;
	var ah = document.body.clientHeight;

	document.getElementById("div_cpasswd").style.marginTop = (ah/2 - 25) + "px";
	document.cpassform.thpasswd.focus();
}
function check_form(){
	if(document.cpassform.thpasswd.value == ""){
		alert('비밀번호를 입력하십시오!');
		document.cpassform.thpasswd.focus();
		return false;
	}
}
</script>
</head>

<body onload="onload_func();">

<form name="cpassform" method="post" action="cpasswd_act.jsp" onsubmit="return check_form();">
<div id="div_cpasswd" style="margin:0 auto;width:300px;height:50px;line-height:30px;">
	<span style="font-weight:bold;">비밀번호를 입력하십시오.</span><br>
	<input type="password" name="thpasswd" value="" style="width:200px;height:20px;border:2px solid #444444;">
	<input type="submit" name="submit" value="확 인" style="width:80px;height:27px;">
	<input type="hidden" name="Dir" value="<%=Dir%>">
	<input type="hidden" name="start" value="<%=start%>">
	<input type="hidden" name="catimage" value="<%=catimage%>">
	<input type="hidden" name="callmode" value="<%=callmode%>">
	<input type="hidden" name="Cate" value="<%=userCate%>">
	<input type="hidden" name="eclang" value="<%=eclang%>">
	<input type="hidden" name="docu" value="<%=docu%>">
	<input type="hidden" name="um" value="<%=um%>">
	<input type="hidden" name="qtxt" value="<%=qtxt%>">
</div>
</form>

</body>
</html>
