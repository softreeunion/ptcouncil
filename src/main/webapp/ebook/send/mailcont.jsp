<%!
	public String get_mailBody(String linkPath, String queryStr, String logo, String userCont, String adminCont, String copyCont){
		String sendDir = "/send";

		String rsCont = "<html>\n";
		rsCont = rsCont + "<head><title>Electronic Catalog</title>\n";
		rsCont = rsCont + "<meta http-equiv=\"Content-Type\" Content=\"text/html; charset=utf-8\">\n";
		rsCont = rsCont + "<style type=\"text/css\">\n";
		rsCont = rsCont + " td{font-size:9pt;font-family:Gulim;font-height:150%;}\n";
		rsCont = rsCont + "</style>\n";
		rsCont = rsCont + "</head>\n";

		rsCont = rsCont + "<body leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" bgcolor=\"#ffffff\">\n";
		rsCont = rsCont + "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"612\">\n";
		rsCont = rsCont + "	<tr><td colspan=\"4\"><img src='" + linkPath + "/" + logo + "'></td></tr>\n";
		rsCont = rsCont + "	<tr>\n";
		rsCont = rsCont + "		<td width=\"200\" bgcolor=\"#9F9BFD\" height=\"6\"></td>\n";
		rsCont = rsCont + "		<td width=\"200\" bgcolor=\"#33B3FD\"></td>\n";
		rsCont = rsCont + "		<td width=\"200\" bgcolor=\"#3399FF\"></td>\n";
		rsCont = rsCont + "		<td width=\"12\" rowspan=\"2\"><img src=\"" + linkPath + sendDir + "/book-1.gif\" width=\"12\" height=\"7\"></td>\n";
		rsCont = rsCont + "	</tr>\n";
		rsCont = rsCont + "	<tr><td height=\"1\" colspan=\"3\"></td></tr>\n";
		rsCont = rsCont + "	<tr>\n";
		rsCont = rsCont + "		<td height=\"89\"><img src=\"" + linkPath + sendDir + "/sub-1.gif\" width=\"200\" height=\"89\"></td>\n";
		rsCont = rsCont + "		<td><img src=\"" + linkPath + sendDir + "/sub-2.gif\" width=\"200\" height=\"89\"></td>\n";
		rsCont = rsCont + "		<td><img src=\"" + linkPath + sendDir + "/sub-3.gif\" width=\"200\" height=\"89\"></td>\n";
		rsCont = rsCont + "		<td background=\"" + linkPath + sendDir + "/book-2.gif\" rowspan=\"3\">&nbsp;</td>\n";
		rsCont = rsCont + "	</tr>\n";

		rsCont = rsCont + "	<tr><td height=\"1\" colspan=\"3\"></td></tr>\n";
		rsCont = rsCont + "	<tr><td colspan=\"3\" valign=\"top\">\n";
		rsCont = rsCont + "		<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"100%\"" + "><tr><td bgcolor=\"#969696\">\n";
		rsCont = rsCont + "			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#f5f5f5\" width=\"100%\">\n";
		rsCont = rsCont + "				<tr><td height=\"11\" colspan=\"3\"></td></tr>\n";
		rsCont = rsCont + "				<tr><td width=\"11\">&nbsp;</td><td width=\"477\" height=\"133\" bgcolor=\"#d9dada\">\n";
		rsCont = rsCont + "					<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" width=\"100%\"><tr><td>" + userCont + "</td></tr></table>\n";
		rsCont = rsCont + "				</td><td width=\"108\">&nbsp;</td></tr>\n";
		rsCont = rsCont + "				<tr><td colspan=\"3\"><img src=\"" + linkPath + sendDir + "/pline.gif\" width=\"596\" height=\"20\"></td></tr>\n";
		rsCont = rsCont + "			</table>\n";

		rsCont = rsCont + "			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#f5f5f5\" width=\"100%\">\n";
		rsCont = rsCont + "				<tr><td height=\"21\" colspan=\"3\"></td></tr>\n";
		rsCont = rsCont + "				<tr><td width=\"11\">&nbsp;</td><td width=\"574\">\n";
		rsCont = rsCont + "					<font color=\"#7d7d7d\">" + adminCont + "</font>\n";
		rsCont = rsCont + "				</td><td width=\"11\">&nbsp;</td></tr>\n";
		rsCont = rsCont + "				<tr><td height=\"21\" colspan=\"3\"></td></tr>\n";
		rsCont = rsCont + "			</table>\n";

		rsCont = rsCont + "			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#f5f5f5\" width=\"100%\">\n";
		rsCont = rsCont + "				<tr>\n";
		rsCont = rsCont + "					<td height=\"27\" width=\"512\">&nbsp;</td>\n";
		rsCont = rsCont + "					<td width=\"84\" rowspan=\"3\"><a href=\"" + linkPath + "/index.jsp?Dir=" + queryStr + "\" target=\"_blank\"><img src=\"" + linkPath + sendDir + "/button.gif\" width=\"84\" height=\"81\" border=\"0\"></a></td>\n";
		rsCont = rsCont + "				</tr>\n";
		rsCont = rsCont + "				<tr><td bgcolor=\"#E7E7E7\" align=\"right\" height=\"29\"><img src=\"" + linkPath + sendDir + "/click_para.gif\" width=\"314\" height=\"29\"></td></tr>\n";
		rsCont = rsCont + "				<tr><td height=\"25\" bgcolor=\"#2ECECE\"><font color=\"#ffffff\">&nbsp;" + copyCont + "</font></td></tr>\n";
		rsCont = rsCont + "			</table>\n";
		rsCont = rsCont + "		</td></tr></table>\n";
		rsCont = rsCont + "	</td></tr>\n";
		rsCont = rsCont + "	<tr><td colspan=\"4\"><img src=\"" + linkPath + sendDir + "/book-3.gif\" width=\"612\" height=\"12\"></td></tr>\n";
		rsCont = rsCont + "</table>\n";
		rsCont = rsCont + "</body>\n";
		rsCont = rsCont + "</html>";

		return rsCont;
	}

	public String get_mailBody2(String linkPath, String queryStr, String logo, String userCont, String adminCont, String copyCont){
		String sendDir = "/send";

		String rsCont = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">\n";
		rsCont = rsCont + "<html>\n";
		rsCont = rsCont + "<head>\n";
		rsCont = rsCont + "<meta http-equiv=\"Content-Type\" Content=\"text/html; charset=utf-8\">\n";
		rsCont = rsCont + "<style type=\"text/css\">\n";
		rsCont = rsCont + "	*{font-size:9pt;font-family:Dotum,Tahoma;}\n";
		rsCont = rsCont + "	body{background-color:#FFFFFF;margin:0;}\n";
		rsCont = rsCont + "	img{border:0;display:block;}\n";
		rsCont = rsCont + "</style>\n";
		rsCont = rsCont + "</head>\n";

		rsCont = rsCont + "<body>\n";
		rsCont = rsCont + "<div style=\"display:block;width:601px;margin:0 0 0 9px;\">\n";
		rsCont = rsCont + "	<img src='" + linkPath + "/" + logo + "' alt=\"Logo\">\n";
		rsCont = rsCont + "</div>\n";

		rsCont = rsCont + "<div style=\"display:block;width:612px;height:71px;\">\n";
		rsCont = rsCont + "	<img src=\"" + linkPath + sendDir + "/blue_top.gif\" width=\"612\" height=\"71\" alt=\"top\">\n";
		rsCont = rsCont + "</div>\n";

		rsCont = rsCont + "<div style=\"display:block;width:612px;background:url('" + linkPath + sendDir + "/blue_middle.gif');\">\n";
		rsCont = rsCont + "	<div style=\"display:block;padding:30px;min-height:100px;\">" + userCont + "</div>\n";
		rsCont = rsCont + "</div>\n";

		rsCont = rsCont + "<div style=\"display:block;width:612px;height:4px;margin:0;padding:0;\">\n";
		rsCont = rsCont + "	<img src=\"" + linkPath + sendDir + "/blue_middle2.gif\" width=\"612\" height=\"4\" alt=\"parting line\">\n";
		rsCont = rsCont + "</div>\n";

		rsCont = rsCont + "<div style=\"display:block;width:612px;background:url('" + linkPath + sendDir + "/blue_middle3.gif');min-height:230px;\">\n";
		rsCont = rsCont + "	<div style=\"float:right;margin-right:30px;margin-top:30px;\">\n";
		rsCont = rsCont + "		<a href=\"" + linkPath + "/index.jsp?Dir=" + queryStr + "\" target=\"_blank\">";
		rsCont = rsCont + "<img src=\"" + linkPath + sendDir + "/blue_viewbtn.png\" width=\"107\" height=\"109\" alt=\"button\"></a>\n";
		rsCont = rsCont + "	</div>\n";
		rsCont = rsCont + "	<div style=\"display:block;padding:30px 30px;width:357px;min-height:100px;\">" + adminCont + "</div>\n";

		rsCont = rsCont + "	<div style=\"display:block;width:559px;height:49px;line-height:49px;text-align:center;";
		rsCont = rsCont + "background:url('" + linkPath + sendDir + "/blue_copyright.png');margin:0 auto;\">" + copyCont + "</div>\n";
		rsCont = rsCont + "</div>\n";

		rsCont = rsCont + "<div style=\"display:block;width:612px;height:19px;\">\n";
		rsCont = rsCont + "	<img src=\"" + linkPath + sendDir + "/blue_bottom.gif\" width=\"612\" height=\"19\" alt=\"bottom\">\n";
		rsCont = rsCont + "</div>\n";

		rsCont = rsCont + "</body>\n";
		rsCont = rsCont + "</html>\n";

		return rsCont;
	}
%>
