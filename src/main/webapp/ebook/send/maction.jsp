<%@ page import="java.io.*, java.util.*, java.text.SimpleDateFormat, java.awt.image.*, javax.mail.*, javax.mail.internet.*" contentType="text/html;charset=utf-8" %>
<%@ include file="mailcont.jsp" %>
<%@ include file="../inc_skin.jsp" %>
<%!
	public String replStr(String s, String a, String b){
		int la = a.length();
		int lb = b.length();
		int ls = s.length();
		String r = "";

		int i = 0;
		while(i < (ls-la+1)){
			if(s.substring(i,i+la).equals(a)){
				r += b;
				i += la;
			}
			else{
				r += s.charAt(i);
				i++;
			}
		}
		
		r += s.substring(i,ls);
		return r;
	}
%>
<%
	request.setCharacterEncoding("utf-8");

	catimage = get_param1(request.getParameter("catimage"),5);
	String cook_dir = get_param1(request.getParameter("Dir"),5);
	String mailDir = get_param1(request.getParameter("mailDir"),5);
	String logoDir = get_param1(request.getParameter("logoDir"),5);

	String vari = get_param3(request.getParameter("vari"), 10);
	String cook_id = get_param3(request.getParameter("cook_id"), 20);

	String pathOfCata = webSysDir + "/catImage" + catimage;
	String pathOfOriCata = webSysDir + "/catImage";

	File f;
	String path = "";
	if(!mailDir.equals("")){
		f = new File(pathOfCata + "/" + mailDir + "/mail.txt");
		if(f.exists()) path = pathOfCata + "/" + mailDir + "/mail.txt";
	}
	if(path.equals("")){
		f = new File(pathOfCata + "/" + cook_dir + "/mail.txt");
		if(f.exists()) path = pathOfCata + "/" + cook_dir + "/mail.txt";
	}
	if(path.equals("")) path = pathOfOriCata + "/mail.txt";

	int j = 0;
	String mailskin = "";
	String adminCont = "";
	String copyCont = "";
	f = new File(path);
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));

		String source = "";
		while((source = bin.readLine()) != null){
			if(source.equals("")) j = 1;
			else if(j == 1) adminCont += source + "<br>\n";
			else if(source.substring(0,10).equals("copyright:")) copyCont = source.substring(11);
			else if(source.substring(0,9).equals("mailskin:")) mailskin = source.substring(9);
		}
		fin.close();
		bin.close();
	}
	else{
		out.println("메일을 보낼 수 없습니다(기본설정).");
		return;
	}

	String fromwho = get_qtxt1(request.getParameter("fromwho"),100);		// except for ()_-
	String frommail = request.getParameter("frommail");
	if(frommail == null) frommail = "";
	String towho = get_qtxt1(request.getParameter("towho"),100);			// except for ()_-
	String tomail = request.getParameter("tomail");
	if(tomail == null) tomail = "";
	String subject = get_qtxt2(request.getParameter("subject"),200);		// strip tags
	String content = get_qtxt2(request.getParameter("content"),2000);		// strip tags

	String malogoFile = "";
	f = new File(pathOfCata + "/" + logoDir);
	if(!logoDir.equals("") && f.exists()){
		File[] folders = f.listFiles();
		for(int i = 0;i < folders.length;i++){
			String temp = folders[i].getName();
			if(temp.length() < 6 || temp.substring(0,1).equals(".")) continue;
			if(temp.substring(0,6).equals("malogo")){
				malogoFile = "catImage" + catimage + "/" + logoDir + "/" + temp;
				break;
			}
		}
	}

	f = new File(pathOfCata + "/" + cook_dir);
	if(malogoFile.equals("") && f.exists()){
		File[] afolders = f.listFiles();
		for(int i = 0;i < afolders.length;i++){
			String temp = afolders[i].getName();
			if(temp.length() < 6 || temp.substring(0,1).equals(".")) continue;
			if(temp.substring(0,6).equals("malogo")){
				malogoFile = "catImage" + catimage + "/" + cook_dir + "/" + temp;
				break;
			}
		}
	}

	if(malogoFile.equals("")){
		f = new File(pathOfOriCata);
		File[] bfolders = f.listFiles();
		for(int i = 0;i < bfolders.length;i++){
			String temp = bfolders[i].getName();
			if(temp.equals(".") || temp.equals("..")) continue;
			if(temp.length() > 6 && temp.substring(0,6).equals("malogo")){
				malogoFile = "catImage/" + temp;
				break;
			}
		}
	}

	//String smtpHost = "127.0.0.1";
	//String smtpPort = "25";
	if(check_email(frommail) == true && check_email(tomail) == true){
		String entire_server = (request.getServerPort() == 80) ? request.getServerName() : request.getServerName() + ":" + request.getServerPort();
		String linkDir = "http://" + entire_server + webServer;
		String userCont = replStr(content,"\n","<br>");
		String linkFactor = vari.equals("mblog") ? cook_dir + "&tid=" + cook_id : cook_dir;
		String Mcont = "";
		if(mailskin.equals("") || mailskin.equals("001")){
			get_mailBody(linkFactor, linkDir, malogoFile, userCont, adminCont, copyCont);
		}
		else{
			get_mailBody2(linkFactor, linkDir, malogoFile, userCont, adminCont, copyCont);
		}

		Properties props = new Properties();
		//props.put("mail.smtp.host", smtpHost); 
		//props.put("mail.smtp.port", smtpPort);
		Session msgSession = Session.getDefaultInstance(props, null);
		MimeMessage msg = new MimeMessage(msgSession);

		String fromx = fromwho + " <" + frommail + ">";
		//fromx = new String(fromx.getBytes("utf-8"),"8859_1");
		InternetAddress from = new InternetAddress(fromx);
		msg.setFrom(from);

		String tox = towho + " <" + tomail + ">";
		//tox = new String(tox.getBytes("utf-8"),"8859_1");
		InternetAddress to = new InternetAddress(tox);
		msg.setRecipient(Message.RecipientType.TO, to);

		msg.setSubject(subject);
		msg.setContent(Mcont, "text/html; charset=utf-8");

		Transport.send(msg);
		out.print("actresult=yes");
	}
	else out.println("메일형식이 맞지 않습니다.");
%>
