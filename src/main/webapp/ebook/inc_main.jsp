<%!
	public String echo_script(String para_str, String loc, String wind, String addscript){
		String restr = "<!DOCTYPE html>\n"
		+ "<html lang=\"ko\">\n"
		+ "<head>\n"
		+ "<meta charset=\"utf-8\">\n"
		+ "<title>eCatalog - " + para_str + "</title>\n"
		+ "</head>\n"
		+ "<body>\n"
		+ "<script>\n";
		if(!para_str.equals("")) restr += "alert(\"" + para_str + "\");\n";
		if(!loc.equals("")){
			if(wind.equals("")) wind = "window";
			restr += wind + ".location = \"" + loc + "\";\n";
		}
		if(!addscript.equals("")) restr += addscript;
		restr += "</script>\n"
		+ "</body>\n"
		+ "</html>\n";

		return restr;
	}

	public String echo_text(String para_str){
		return "<!DOCTYPE html>\n"
		+ "<html lang=\"ko\">\n"
		+ "<head>\n"
		+ "<meta charset=\"utf-8\">\n"
		+ "<title>eCatalog - " + para_str + "</title>\n"
		+ "</head>\n"
		+ "<body>\n"
		+ "<p>" + para_str + "</p>\n"
		+ "</body>\n"
		+ "</html>\n";
	}

	public void set_cateCond(String cate, HashMap CataKind)
		throws FileNotFoundException, UnsupportedEncodingException, IOException {			// catakind.txt(when cate is supplied)

		String source = "";
		File f = new File(pathOfCata + "/catakind.txt");
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;
		
			String[] aaa = mysplit(source, "|");
			if(cate.equals(aaa[0])){				// belonged category
				Dir = aaa[3];
				CataKind.put("svcCond1", aaa[1].substring(0,1));
				CataKind.put("svcKind1", aaa[1].substring(2,3));
				CataKind.put("cateCode", aaa[0]);
				CataKind.put("dirName", aaa[2]);
				break;
			}
		}
		bin.close();
		fin.close();

		if(CataKind.get("svcKind1").equals("C")){
			String pCode = get_parentCode(cate);
			fin = new FileInputStream(f);
			bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;
		
				String[] aaa = mysplit(source, "|");
				if(pCode.equals(aaa[0])){
					CataKind.put("svcCond2", aaa[1].substring(0,1));
					CataKind.put("unifyCond", aaa[1].substring(1,2));
					CataKind.put("cateName", aaa[2]);
					break;
				}
			}
			bin.close();
			fin.close();

			CataKind.put("parentCode", pCode);
		}
		else{
			int step = get_cateStep(cate);
			int plen = step*2;
			String pnCode = cate.substring(0,plen);
			String pCode = pnCode + "0000000000".substring(0, 10-pnCode.length());
			CataKind.put("parentCode", pCode);

			Vector adir = new Vector();
			fin = new FileInputStream(f);
			bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;
		
				String[] aaa = mysplit(source, "|");
				if(aaa[1].substring(0,1).equals("D") || aaa[1].substring(2,3).equals("0")) continue;
				if(aaa[0].substring(0,plen).equals(pnCode) && !aaa[0].substring(plen,plen+2).equals("00")){
					adir.addElement(aaa[3]);
				}
			}
			bin.close();
			fin.close();

			Dir = get_servFolder(pathOfCata, adir, "");

			fin = new FileInputStream(f);
			bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;
		
				String[] aaa = mysplit(source, "|");
				if(Dir.equals(aaa[3])){
					CataKind.put("svcCond1", aaa[1].substring(0,1));
					CataKind.put("svcKind1", aaa[1].substring(2,3));
					CataKind.put("cateCode", aaa[0]);
					CataKind.put("dirName", aaa[2]);
				}
				else if(pCode.equals(aaa[0])){				// belonged category
					CataKind.put("svcCond2", aaa[1].substring(0,1));
					CataKind.put("unifyCond", aaa[1].substring(1,2));
					CataKind.put("cateName", aaa[2]);
				}
			}
			bin.close();
			fin.close();
		}
	}

	// catakind.txt(when Dir is supplied)
	public String set_dirCond(HashMap CataKind, String unifyCall)
		throws FileNotFoundException, UnsupportedEncodingException, IOException {
		boolean found = false;
		String source = "";
		File f = new File(pathOfCata + "/catakind.txt");
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;
	
			String[] aaa = mysplit(source, "|");
			if(aaa[3].equals(Dir)){
				CataKind.put("svcCond1", aaa[1].substring(0,1));
				CataKind.put("svcKind1", aaa[1].substring(2,3));
				CataKind.put("cateCode", aaa[0]);
				CataKind.put("dirName", aaa[2]);
				found = true;
				break;
			}
		}
		bin.close();
		fin.close();

		if(found == false) return "Dir does not exist in the catakind.txt";

		String cate = (String)CataKind.get("cateCode");
		int step = get_cateStep(cate);
		int plen = step*2;
		String pnCode = cate.substring(0,plen-2);
		String pCode = pnCode + "0000000000".substring(0, 10-pnCode.length());
		CataKind.put("parentCode", pCode);

		fin = new FileInputStream(f);
		bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;
	
			String[] aaa = mysplit(source, "|");
			if(aaa[0].equals(pCode)){
				CataKind.put("svcCond2", aaa[1].substring(0,1));
				CataKind.put("unifyCond", aaa[1].substring(1,2));
				CataKind.put("cateName", aaa[2]);
				break;
			}
		}
		bin.close();
		fin.close();

		// find the first catalog in a category at unified
		if(unifyCall.equals("true") && CataKind.get("unifyCond").equals("U")){
			fin = new FileInputStream(f);
			bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;
		
				String[] aaa = mysplit(source, "|");
				if(aaa[0].substring(0,plen).equals(pnCode) && !aaa[0].substring(plen,plen+2).equals("00")){
					Dir = aaa[3];
					CataKind.put("svcCond1", aaa[1].substring(0,1));
					CataKind.put("svcKind1", aaa[1].substring(2,3));
					CataKind.put("dirName", aaa[2]);
					break;
				}
			}
			bin.close();
			fin.close();

			pathOfDir = pathOfCata +"/" + Dir;
		}
		return "";
	}

	// category.txt
	public void set_category(String path, String pcode, HashMap Category) throws FileNotFoundException, UnsupportedEncodingException, IOException {
		Category.put("fmove", "0");
		Category.put("onemove", "N");
		Category.put("catflash", "N");
		Category.put("bascadapt", "");
		Category.put("logoadapt", "");
		Category.put("mailadapt" ,"");
		Category.put("music","");

		File f = new File(path);
		if(f.exists()){
			String source = "";
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				if(source.substring(0,10).equals(pcode)){
					String[] aaa = mysplit(source, "\t");
					if(aaa[1].equals("cate")){
						Category.put("fmove", aaa[2].substring(0,1));
						Category.put("onemove", aaa[2].substring(1,2));
						Category.put("bascadapt", aaa[3]);
						Category.put("logoadapt", aaa[4]);
						Category.put("mailadapt", aaa[5]);
						Category.put("music", aaa[6]);
					}
					else if(aaa[1].equals("unif")){
						Category.put("unif", source);
					}
				}
			}
			fin.close();
			bin.close();
		}
	}

	// catimage.txt
	public String get_catimage(String catimage, String varkd)
		throws FileNotFoundException, UnsupportedEncodingException, IOException {
		String t = "";
		File f = new File(pathOfOriCata + "/catimage.txt");
		if(f.exists()){
			String source = "";
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				String[] aaa = mysplit(source, "\t");
				if(aaa[0].equals(catimage)){
					t = aaa[1].substring(0,1);
					break;
				}
			}
			fin.close();
			bin.close();
		}

		return t;
	}

	// ecatalog.txt
	public void set_ecatalog(String path, HashMap Ecatalog)
		throws FileNotFoundException, UnsupportedEncodingException, IOException {
		String source = "";
		File f = new File(path);
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);
			if(in == -1) continue;
			String sval = source.substring(in+1);
			Ecatalog.put(skey, sval);
		}
		fin.close();
		bin.close();

		if(Ecatalog.get("bgwidth") == null) Ecatalog.put("bgwidth","0");
		if(Ecatalog.get("bgheight") == null) Ecatalog.put("bgheight","0");
		if(Ecatalog.get("logolink") == null) Ecatalog.put("logolink","");
		if(Ecatalog.get("logopos") == null) Ecatalog.put("logopos","");
		if(Ecatalog.get("slidetime") == null) Ecatalog.put("slidetime","");
		if(Ecatalog.get("bigslidetime") == null) Ecatalog.put("bigslidetime","");
		if(Ecatalog.get("searchurl") == null) Ecatalog.put("searchurl","");
		if(Ecatalog.get("downfile") == null) Ecatalog.put("downfile","");
		if(Ecatalog.get("downtext") == null) Ecatalog.put("downtext","");
		if(Ecatalog.get("catpasswd") == null) Ecatalog.put("catpasswd","");
		if(Ecatalog.get("etcopt") == null) Ecatalog.put("etcopt","");
		if(Ecatalog.get("twitter") == null) Ecatalog.put("twitter","");
		if(Ecatalog.get("facebook") == null) Ecatalog.put("facebook","");
		if(Ecatalog.get("kakao") == null) Ecatalog.put("kakao","");
		if(Ecatalog.get("startdate") == null) Ecatalog.put("startdate","");
		if(Ecatalog.get("enddate") == null) Ecatalog.put("enddate","");
		if(Ecatalog.get("prepage") == null) Ecatalog.put("prepage","");
		if(Ecatalog.get("outpage") == null) Ecatalog.put("outpage","");
	}

	// ecatalog.txt
	public String get_ecatalogKey(String path, String keyVal)
		throws FileNotFoundException, UnsupportedEncodingException, IOException {
		String resVal = "";
		String source = "";
		File f = new File(path);
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);
			if(in == -1) continue;

			if(skey.equals(keyVal)){
				resVal = source.substring(in+1);
				break;
			}
		}
		fin.close();
		bin.close();

		return resVal;
	}

	public String get_logoFile(String path, String fnfrt){
		String t = "";
		int slen = fnfrt.length();
		File f = new File(path);
		File[] folders = f.listFiles();
		for(int i = 0;i < folders.length;i++){
			String temp = folders[i].getName();
			if(temp.length() < slen || temp.charAt(0) == '.') continue;
			if(temp.substring(0,slen).equals(fnfrt)){
				t = temp;
				break;
			}
		}
		return t;
	}

	// reimage
	public String get_reimageFile(String path, String fname){
		File f = new File(path+"/"+fname);
		if(!fname.equals("") && f.exists()) return fname;

		String t = "";
		File[] folders = f.listFiles();
		for(int i = 0;i < folders.length;i++){
			String temp = folders[i].getName();
			if(temp.length() < 7 || temp.charAt(0) == '.') continue;
			if(temp.substring(0,7).equals("reimage")){
				t = temp;
				break;
			}
		}

		if(!t.equals("")) return t;

		for(int i = 0;i < folders.length;i++){
			String temp = folders[i].getName();
			if(temp.length() < 4 || temp.charAt(0) == '.') continue;
			if(temp.substring(0,4).equals("s001")){
				t = temp;
				break;
			}
		}

		return t;
	}

	// background
	public void set_background(String sval, String bpath, HashMap CataBack){
		String[] aaa = mysplit(sval,"-");

		CataBack.put("backType", aaa[0]);
		String scolor = aaa[1];
		String bcolor = aaa[2];

		if(CataBack.get("backType").equals("")) CataBack.put("backType","color");

		String backtype = (String)CataBack.get("backType");
		if(backtype.equals("color")){
			if(!scolor.equals("")){
				if(scolor.substring(0,1).equals("#")) CataBack.put("backColor", scolor);
				else CataBack.put("backColor", "#"+scolor);
			}
		}
		else if(backtype.equals("image")){
			CataBack.put("backAlign", "repeat");
		}
		else if(backtype.equals("left") || backtype.equals("center") || backtype.equals("right")){
			CataBack.put("backAlign", backtype);
			CataBack.put("backType", "image");
		}

		if(CataBack.get("backType").equals("image")){
			if(bpath.equals("user") && !scolor.equals("")){
				File f = new File(scolor);
				if(f.exists()) CataBack.put("backColor", scolor);
				else CataBack.put("backColor", "");
			}
			else{
				File f = new File(bpath);
				if(f.exists()) CataBack.put("backColor", bpath);
				else CataBack.put("backColor", "");
			}
		}

		if(!bcolor.equals("")){
			if(bcolor.substring(0,1).equals("#")) CataBack.put("blankColor", bcolor);
			else CataBack.put("blankColor", "#"+bcolor);
		}
	}

	public boolean check_servDate(String startdate, String enddate, String currdate){
		if(!startdate.equals("") && get_tense(currdate, startdate, "start") == "future") return false;		// startdate is future than Today(service will start later)
		if(!enddate.equals("") && get_tense(currdate, enddate, "end") == "past") return false;				// enddate is past(service is stopped already)
		return true;
	}

	public String get_servFolder(String path, Vector ardir, String currdate)
		throws FileNotFoundException, UnsupportedEncodingException, IOException {			// catakind.txt(when cate is supplied)
		if(currdate.equals("")) currdate = get_currDate();

		String servDir = "";
		String t_startdate = "";

		String sdir, startdate, enddate, source;
		int alen = ardir.size();
		for(int i = 0;i < alen;i++){
			sdir = (String)ardir.elementAt(i);
			startdate = "";
			enddate = "";

			File f = new File(path + "/" + sdir + "/ecatalog.txt");
			if(f.exists()){
				FileInputStream fin = new FileInputStream(f);
				BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
				while((source = bin.readLine()) != null){
					source = strTrimEach(source);
					if(source.equals("")) continue;

					int in = source.indexOf(":");
					String skey = source.substring(0, in);
					if(in == -1) continue;

					if(skey.equals("startdate")) startdate = source.substring(in+1);
					else if(skey.equals("enddate")) enddate = source.substring(in+1);
				}
				fin.close();
				bin.close();
			}

			if(startdate.equals("") && enddate.equals("")){
				if(t_startdate.equals("")) servDir = sdir;
				continue;
			}

			if(check_servDate(startdate, enddate, currdate) == false) continue;

			if(!startdate.equals("")){
				if(t_startdate.equals("")){
					servDir = sdir;
					t_startdate = startdate;
				}
				else{
					if(get_tense(t_startdate, startdate, "start").equals("future")){			// startdate is future than t_startdate
						servDir = sdir;
						t_startdate = startdate;
					}
				}
			}
		}
		return servDir;
	}

	public String get_tense(String currdate, String testdate, String skd){
		currdate = currdate.replaceAll(" ","-");
		currdate = currdate.replaceAll(":","-");

		String[] aaa = mysplit(currdate, "-");
		int curr_year = Integer.parseInt(aaa[0]);
		int curr_month = Integer.parseInt(aaa[1]);
		int curr_day = Integer.parseInt(aaa[2]);
		int curr_hour = 0;
		if(aaa.length > 3 && !aaa[3].equals("")) curr_hour = Integer.parseInt(aaa[3]);
		int curr_minute = 0;
		if(aaa.length > 4 && !aaa[4].equals("")) curr_minute = Integer.parseInt(aaa[4]);

		testdate = testdate.replaceAll(" ","-");
		testdate = testdate.replaceAll(":","-");

		String[] bbb = mysplit(testdate, "-");
		int test_year = Integer.parseInt(bbb[0]);
		int test_month = Integer.parseInt(bbb[1]);
		int test_day = Integer.parseInt(bbb[2]);
		int test_hour = 0;
		int test_minute = 0;
		if(skd.equals("start")){
			if(bbb.length > 3 && !bbb[3].equals("")) test_hour = Integer.parseInt(bbb[3]);
			if(bbb.length > 4 && !bbb[4].equals("")) test_minute = Integer.parseInt(bbb[4]);
		}
		else{
			test_hour = 23;
			test_minute = 59;
			if(bbb.length > 3 && !bbb[3].equals("")){
				test_hour = Integer.parseInt(bbb[3]);
				test_minute = (bbb.length > 4 && !bbb[4].equals("")) ? Integer.parseInt(bbb[4]) : 0;
			}
		}

		if(curr_year == test_year && curr_month == test_month && curr_day == test_day && curr_hour == test_hour && curr_minute == test_minute){
			return "today";
		}

		if(curr_year < test_year) return "future";
		else if(curr_year == test_year){
			if(curr_month < test_month) return "future";
			else if(curr_month == test_month){
				if(curr_day < test_day) return "future";
				else if(curr_day == test_day){
					if(curr_hour < test_hour) return "future";
					else if(curr_hour == test_hour){
						if(curr_minute < test_minute) return "future";
					}
				}
			}
		}
		return "past";
	}

	public String get_currDate(){
		Calendar aCalendar = Calendar.getInstance();
		int to_year = aCalendar.get(Calendar.YEAR);
		int to_month = aCalendar.get(Calendar.MONTH) + 1;
		int to_day = aCalendar.get(Calendar.DAY_OF_MONTH);

		int to_hour = aCalendar.get(Calendar.HOUR_OF_DAY);
		int to_minute = aCalendar.get(Calendar.MINUTE);
		int to_second = aCalendar.get(Calendar.SECOND);

		String currdate = to_year + "-" + to_month + "-" + to_day + " " + to_hour + ":" + to_minute;
		return currdate;		
	}
%>
