<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%@ include file="inc_skin.jsp" %>
<%@ include file="inc_main.jsp" %>
<%@ include file="ConnLog.jsp" %>
<%
	catimage = get_param1(request.getParameter("catimage"),5);
	Dir = get_param1(request.getParameter("Dir"),5);
	if(Dir.equals("")) Dir = "1";

	String start = check_start(request.getParameter("start"));
	String callmode = check_callmode(request.getParameter("callmode"));
	String eclang = check_eclang(request.getParameter("eclang"));
	String userCate = check_cate(request.getParameter("Cate"));

	String cdmakeNo = "";
	boolean cdmake = false;
	String svckd = "";
	if(callmode.equals("cdmake") || callmode.equals("cdmake_unify")){
		if(callmode.equals("cdmake")) callmode = "normal";
		else if(callmode.equals("cdmake_unify")) callmode = "normal_unify";
		cdmake = true;
		cdmakeNo = get_param1(request.getParameter("makeno"),5);
		if(!cdmakeNo.equals("")) cdmakeNo = "_" + cdmakeNo;
		svckd = get_param2(request.getParameter("svckd"),1);
	}

	pathOfOriCata = webSysDir + "/catImage";
	pathOfCata = webSysDir + "/catImage" + catimage;
	pathOfDir = webSysDir + "/catImage" + catimage + "/" + Dir;

	String incView = "m";
	String queryStr = "catimage=" + catimage + "&start=" + start + "&callmode=" + callmode + "&eclang=" + eclang;
	String queryStrLink = queryStr + "&Dir=" + Dir + "&Cate=" + userCate;
	String queryStrDir = queryStr + "&Dir=" + Dir;

	String agentstr = request.getHeader("User-Agent");
	if(agentstr == null) agentstr = "";
	else{
		agentstr = agentstr.replaceAll("\n","");
		agentstr = agentstr.replaceAll("\r","");
		agentstr = agentstr.replaceAll("\t","");
	}

	String[] aaa = get_machine(agentstr);
	String usrmName = aaa[0];
	String usrmKind = aaa[1];

	File f = new File(pathOfCata + "/catakind.txt");						// check category configuration file
	if(!f.exists()){
		out.print(echo_script("페이지를 표시할 수 없습니다.(catakind)","","window","window.close();\n"));
		return;
	}

	String unifyCall = "false";
	if(callmode.equals("normal_unify") || callmode.equals("admin_unify")) unifyCall = "true";

	HashMap CataKind = new HashMap();
	if(!userCate.equals("")){					// must find the first cover page out of the catalogs
		set_cateCond(userCate, CataKind);
		if(Dir.equals("")){
			out.print(echo_script("페이지를 표시할 수 없습니다.(no Dir)","","window","window.close();\n"));
			return;
		}
		pathOfDir = pathOfCata + "/" + Dir;
		queryStrLink = queryStr + "&Cate=" + userCate;
		queryStrDir = queryStr + "&Dir=" + Dir;
	}
	else{
		String resstr = set_dirCond(CataKind, unifyCall);
		if(!resstr.equals("")){
			out.print(echo_text(resstr));
			return;
		}
		queryStrLink = queryStrDir;
	}

	if(!CataKind.get("unifyCond").equals("U")) unifyCall = "false";

	String parentCode = (String)CataKind.get("parentCode");
	if(parentCode.equals("")){										// exists parent category?
		out.print(echo_script("페이지를 표시할 수 없습니다.(parentCode)","","window","window.close();\n"));
		return;
	}

	f = new File(pathOfDir);
	if(!f.exists()){
		out.print(echo_script("페이지를 표시할 수 없습니다.(pathOfDir)","","window","window.close();\n"));
		return;
	}

	f = new File(pathOfDir + "/ecatalog.txt");
	if(!f.exists()){				// check catalog configuration file
		out.print(echo_script("페이지를 표시할 수 없습니다.(ecatalog)","","window","window.close();\n"));
		return;
	}
	if(!callmode.equals("admin") && !callmode.equals("admin_unify") && (CataKind.get("svcCond1").equals("D") || CataKind.get("svcCond2").equals("D"))){
		out.print(echo_script("현재는 서비스가 불가합니다.","","window","window.close();\n"));
		return;
	}

	HashMap Category = new HashMap();
	set_category(pathOfCata+"/category.txt", (String)CataKind.get("parentCode"), Category);

	// for moving from category to category
	String moveInDist = "";
	f = new File(pathOfOriCata+"/catimage.txt");
	if(unifyCall.equals("true") && cdmake == false && f.exists()){
		moveInDist = get_catimage(catimage, "moveInDist");
	}

	// *** ecatalog.txt - check whether the total application of anyone folder's configuration exists
	boolean jbool = false;
	String bascadapt = (String)Category.get("bascadapt");
	if(bascadapt == null) bascadapt = "";
	if(unifyCall.equals("false")){
		if(!bascadapt.equals("")){
			f = new File(pathOfCata + "/" + bascadapt + "/ecatalog.txt");
			if(f.exists()) jbool = true;
		}
	}

	HashMap Ecatalog = new HashMap();
	if(jbool == true){
		set_ecatalog(pathOfCata+"/"+bascadapt+"/ecatalog.txt", Ecatalog);
		Ecatalog.put("pageno", get_ecatalogKey(pathOfDir + "/ecatalog.txt", "pageno"));
	}
	else{
		set_ecatalog(pathOfDir+"/ecatalog.txt", Ecatalog);
	}

	if(!callmode.equals("admin") && !callmode.equals("admin_unify") && check_servDate((String)Ecatalog.get("startdate"),(String)Ecatalog.get("enddate"),get_currDate()) == false){
		out.print(echo_script("서비스 기간이 아닙니다.","","window","window.close();\n"));
		return;
	}

	String ini2var = (String)Ecatalog.get("ini2var");
	String xmlExt = (ini2var.length() > 15 && ini2var.charAt(15) == 'T') ? "xml.txt" : ".xml";
	String ini1var = (String)Ecatalog.get("ini1var");

	f = new File(pathOfDir+"/bookdata"+xmlExt);
	if(ini1var.charAt(11) == 'X' && (!f.exists() || f.length() == 0)){
		out.print(echo_script("페이지를 표시할 수 없습니다.(bookdata"+xmlExt+")","","window","window.close();\n"));
		return;
	}

	HashMap Display = new HashMap();
	Display.put("user", get_userSkin((String)Ecatalog.get("askin"), incView));
	set_userSkinDef(Display);
	if(!Display.get("user").equals("")){
		f = new File(pathOfOriCata+"/display.txt");
		if(!f.exists()){
			out.print(echo_script("페이지를 표시할 수 없습니다.(display.txt)","","window","window.close();\n"));
			return;
		}
		set_userSkin((String)Display.get("user"), Display);
	}

	String cook_counter = "";
	String cook_cpasswd = "";
	if(cdmake == false){
		Cookie[] cookies = request.getCookies();
		if(cookies != null){
			for(int i = 0;i < cookies.length;i++){
				Cookie theCookie = cookies[i];
				if(theCookie.getName().equals("cook_counter")) cook_counter = theCookie.getValue();
				else if(theCookie.getName().equals("cook_cpasswd")) cook_cpasswd = theCookie.getValue();
			}
		}

		String catpasswd = (String)Ecatalog.get("catpasswd");
		if(catpasswd == null) catpasswd = "";
		if(!catpasswd.equals("") && (cook_cpasswd.equals("") || !cook_cpasswd.equals(catpasswd))){				// cat password
			response.sendRedirect("./ecatalog_cpasswd.jsp?"+queryStrLink+"&docu=m");
			return;
		}

		/* counter start */
		f = new File(webSysDir + "/log");
		if(!f.exists()) f.mkdirs();

		ConnLog connlog = new ConnLog(streamCharset);
		connlog.calDate();
		connlog.setMoveFile(webSysDir+"/log", "log"+catimage+"_");

	 	if(cook_counter.equals("")){
	 		connlog.recordLog(webSysDir+"/log/log"+catimage+"_"+connlog.today+".txt", "0\t"+Dir+"\t"+connlog.toStr+"\t"+request.getRemoteAddr()+"\t"+agentstr+"\n");
			Cookie c_cook_counter = new Cookie("cook_counter", Dir);
			response.addCookie(c_cook_counter);
		}
		else if(connlog.parse_cookie(Dir, cook_counter) == false){
			connlog.recordLog(webSysDir+"/log/log"+catimage+"_"+connlog.today+".txt", "1\t"+Dir+"\t"+connlog.toStr+"\t"+request.getRemoteAddr()+"\t"+agentstr+"\n");
			Cookie c_cook_counter = new Cookie("cook_counter", cook_counter + ":" + Dir);
			response.addCookie(c_cook_counter);
		}
		/* counter end */
	}

	String logoadapt = (String)Category.get("logoadapt");
	String dist1var = Category.get("fmove")+moveInDist+"000";
	String distcfg = logoadapt+";"+Category.get("mailadapt")+";"+Category.get("music")+";"+Category.get("bascadapt");

	// *** s000 file
	String s000File = get_logoFile(pathOfDir, "s000");

	String domainUrl = (request.getServerPort() == 80) ? request.getServerName() : request.getServerName() + ":" + request.getServerPort();
	String selfUrl = "http://" + domainUrl + request.getServletPath();
	String cataRootUrl = request.getServletPath().replaceAll("/ecatalogm.jsp","");

	String titlebar = (String)Ecatalog.get("titlebar");
	if(titlebar.equals("")) titlebar = "Electronic Catalog(전자 카탈로그)";

	HashMap Skin = new HashMap();
	set_skin(incView, Skin, (String)Ecatalog.get("askin"), (String)Ecatalog.get("bskin"), Category, Display);
	if(cdmake == true && svckd.equals("C")) Skin.put("combokind","000");

	String smc = (String)Skin.get("smc");
	String onesmc = (ini2var.charAt(5) == 'Y' || smc.equals("008") || smc.charAt(0) == '3') ? "true" : "false";
	if(usrmKind.equals("s")) onesmc = "true";
	String showDescript = (smc.charAt(0) == '3') ? "true" : "false";

	// logo
	String logoFile = "";
	if(!logoadapt.equals("")){
		String file = get_logoFile(pathOfCata+"/"+logoadapt, "splogo");
		if(!file.equals("")) logoFile = webServer+"/catImage"+catimage+"/"+logoadapt+"/"+file;
	}
	if(logoFile.equals("")){
		String file = get_logoFile(pathOfDir,"splogo");
		if(!file.equals("")) logoFile = webServer+"/catImage"+catimage+"/"+Dir+"/"+file;
	}
	if(logoFile.equals("")){
		String file = get_logoFile(pathOfOriCata,"splogo");
		if(!file.equals("")) logoFile = webServer+"/catImage/"+file;
	}

	String logoPosx = "0";
	String logoPosy = "0";

	String logopos = (String)Ecatalog.get("logopos");
	if(!logopos.equals("")){
		int in = logopos.indexOf("x");
		logoPosx = logopos.substring(0, in);
		logoPosy = logopos.substring(in+1);

		if(logoPosx.equals("")) logoPosx = "0";
		if(logoPosy.equals("")) logoPosy = "0";
	}

	// background
	HashMap CataBack = new HashMap();
	CataBack.put("backType", "color");
	CataBack.put("backColor", "#F7F7F7");
	CataBack.put("backAlign", "");
	CataBack.put("blankColor", "#FFFFFF");

	set_background((String)Ecatalog.get("bground"), pathOfDir, CataBack);

	// reimage
	int reImageWidth = 100;
	int reImageHeight = 100;
	String reImage = get_reimageFile(pathOfDir, (String)Ecatalog.get("reimage"));
	if(!reImage.equals("")){
		ImageSize inImage = new ImageSize();
		inImage.setImage(pathOfDir+"/"+reImage);
		reImageWidth = inImage.getWidth();
		reImageHeight = inImage.getHeight();
		reImage = "http://"+domainUrl+cataRootUrl+"/catimage"+catimage+"/"+Dir+"/"+reImage;
	}

	// variable
	String display = (String)Skin.get("display");
	String dClass = (String)Skin.get("skinc");

	String bigImageOnly = (ini2var.charAt(11) == 'Y') ? "true" : "false";
	String spDrag = (ini2var.charAt(10) == 'Y') ? "true" : "false";
	String soundMenuHide = "false";
	String menuAutoHide = "false";
	String mailButtonHide = (cdmake == false) ? ini2var.substring(4,5) : "Y";
	String imgRender = ini2var.substring(17,18);
	String pchZoom = ini2var.substring(18,19);

	String mokchaFile = "false";
	f = new File(pathOfDir+"/mokcha.txt");
	if(f.exists() && f.length() > 0) mokchaFile = "true";

	String indexType = get_indextype(display, ini2var.substring(19,20), incView, Skin);
	int[] arscrPos;
	if(!Display.get("user").equals("")){
		if(Display.get("dim_d") != null && check_skindimension2((String)Display.get("dim_m")) == true) arscrPos = get_skindimension2((String)Display.get("dim_m"),indexType,mokchaFile);
		else arscrPos = get_skindimension(indexType, dClass, incView, mokchaFile);
	}
	else{	
		arscrPos = get_skindimension(indexType, display, incView, mokchaFile);
	}
	String searchword = "";

	String webpath = webServer + "/catImage" + catimage + "/" + Dir;
	int loadto = 2;			// self & license
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<% if(bigImageOnly.equals("true") && pchZoom.equals("N")){ %>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=10.0, user-scalable=yes">
<% } else{ %>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<% } %>
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta property="og:description" content="<%=Ecatalog.get("facebook")%>">
<title><%=titlebar%> </title>
<link rel="stylesheet" type="text/css" href="<%=webServer%>/main.css">
<link rel="stylesheet" type="text/css" href="<%=webServer%>/skin5/mm<%=Skin.get("cssnDir")%>.css">
<style>
	html, body{width:100%;height:100%;margin:0;overflow-x:hidden;}
<%
if(!Display.get("user").equals("") && Display.get("bground") != null){
	set_background((String)Display.get("bground"), "user", CataBack);
	if(CataBack.get("backType").equals("image")){
		out.print("	body{background-image:url('"+CataBack.get("backColor")+"');");
		if(CataBack.get("backAlign").equals("repeat")) out.print("background-repeat:repeat;}\n");
		else out.print("background-repeat:no-repeat;background-position:"+CataBack.get("backAlign")+";}\n");
	}
	else if(CataBack.get("backType").equals("color")){
		out.print("	body{background-color:"+CataBack.get("backColor")+";}\n");
	}
}
else if(display.equals("012")){
	out.print("	body{background-image:url('"+webServer+"/skin5/mm012/mainback.png');background-size:100% 100%;}\n");
}
else if(display.equals("302")){
	out.print("	body{background-color:#1b121d;}\n");
}
else if(dClass.equals("016")){

}
else if(CataBack.get("backType").equals("image")){
	out.print("	body{background-image:url('"+CataBack.get("backColor")+"');");
	if(CataBack.get("backAlign").equals("repeat")) out.print("background-repeat:repeat;}\n");
	else out.print("background-repeat:no-repeat;background-position:"+CataBack.get("backAlign")+";}\n");
}
else if(CataBack.get("backType").equals("color")){
	out.print("	body{background-color:"+CataBack.get("backColor")+";}\n");
}

if(soundMenuHide.equals("true")) out.print("	#lowersect{display:none;}\n");
else out.print("	#lowersect{display:block;}\n");
%>
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=webServer%>/util.js"></script>
<script src="<%=webServer%>/action.js"></script>
<script src="<%=webServer%>/info.js"></script>
<% if(imgRender.equals("C")){ %>
<script src="<%=webServer%>/anic.js"></script>
<script src="<%=webServer%>/smcc.js" async></script>
<% } else{ %>
<script src="<%=webServer%>/ani.js"></script>
<script src="<%=webServer%>/smc.js" async></script>
<% } %>
<script src="<%=webServer%>/link.js" async></script>
<script src="<%=webServer%>/skin5/smc<%=smc%>.js" async></script>
<script src="https://developers.kakao.com/sdk/js/kakao.min.js" async></script>
<script>
set_catainfo(<%
	out.print("'"+CataKind.get("svcKind1")+"','"+catimage+"',");
	if(ini1var.charAt(14) == 'Y') out.print("true,");
	else out.print("false,");			// openingAni
	if(callmode.equals("")) out.print("'normal',");
	else out.print("'"+callmode+"',");
	out.print("'"+charset+"','"+CataBack.get("backType")+"','"+CataBack.get("backColor")+"','"+CataBack.get("backAlign")+"','"+incView+"',");
	if(cdmake == true) out.print("true,");
	else out.print("false,");
	out.print("'N'");			// memberType
%>);
set_action(<%
	if(ini1var.charAt(0) == 'Y') out.print("true,");			// showAltText
	else out.print("false,");
	out.print("false");		// showLinkText(true로 하려면 html내에 childNode를 넣어야 함)
%>);
set_serverinfo(<%
	out.print("'"+webServer+"','jsp','");
	if(cdmake == true) out.print("',");
	else out.print(selfUrl+"?"+queryStrLink+"',");
	out.print("'','',0,");	// id, name, signedIn
	if(ini2var.charAt(6) == 'Y') out.print("true");		// applyCipher
	else out.print("false");
%>);
set_skininfo(<%
	out.print("'"+display+"','"+smc+"','"+Skin.get("onsmimage")+"','"+Skin.get("combokind")+"','"+Skin.get("onbigimage")+"',");
	out.print("'"+Skin.get("newin")+"','"+Skin.get("cpskin")+"','"+Skin.get("spacerbutton")+"','"+dClass+"',");
	out.print("'"+Skin.get("printKindmc")+"','"+Skin.get("helpmc")+"','"+Skin.get("smcdockind")+"',");
	out.print("'"+Skin.get("mmType")+"',"+Skin.get("smGallery")+",'"+Skin.get("indexmc")+"'");
%>);
set_screeninfo(<%
	out.print(Ecatalog.get("smwidth")+","+Ecatalog.get("smheight")+","+Ecatalog.get("bgwidth")+","+Ecatalog.get("bgheight")+",");
	if(Ecatalog.get("midwidth").equals("")) out.print("0,");
	else out.print(Ecatalog.get("midwidth")+",");
	out.print(onesmc+",");

	// margin of synchronization with browser's window
	String m_synchro = smc.equals("008") ? "N" : ini2var.substring(13,14);
	String m_margin = (String)Ecatalog.get("mmargin");

	out.print("'"+m_synchro+"',");						// syncwindow
	if(m_margin.equals("")) out.print("0,");
	else out.print(m_margin+",");
	out.print("'"+CataBack.get("blankColor")+"',"+logoPosy);					// logoTopMargin

	if(smc.equals("004") || display.equals("018") || display.equals("020") || display.equals("021") || display.equals("301")) out.print(",'center',");
	else out.print(",'top',");						// m_alignVert

	out.print("'"+imgRender+"','"+pchZoom+"',");
	if(ini2var.charAt(20) == 'Y') out.print("true");
	else out.print("false");			// smImageDiff
%>);
set_unifyinfo(<%
	if(unifyCall.equals("true")) out.print("true, false");
	else{
		if(!Ecatalog.get("outpage").equals("")) out.print("true, true");
		else out.print("false, false");
	}
%>);
set_pageinfo(<%
	out.print("'"+CataKind.get("cateCode")+"','"+Dir+"',"+Ecatalog.get("frpage")+","+Ecatalog.get("pageno")+",");
	if(start.equals("")) out.print("0,");
	else out.print(start+",");
	if(Ecatalog.get("showstart").equals("")) out.print("0,");
	else out.print(Ecatalog.get("showstart")+",");
	if(ini2var.charAt(9) == 'F') out.print("'filename',");
	else out.print("'number',");		// pageshowkind
	out.print("'"+CataKind.get("dirName")+"','"+Ecatalog.get("prepage")+"'");
%>);
set_guideinfo(80);
set_menuinfo(<%
	out.print("'"+moveInDist+"','"+Category.get("fmove")+"','"+Category.get("onemove")+"',70,");		// thumbHeight
	if(ini1var.charAt(2) == 'Y') out.print("true,");			// openingIndexView
	else out.print("false,");
	out.print(menuAutoHide+",");							// menuAutoHide
	out.print(soundMenuHide+",");							// soundMenuHide
	out.print("'"+ini1var.substring(20,21)+"',");			// applyMax
	out.print("'"+mailButtonHide+"',");						// mailButtonHide
	out.print("'"+indexType+"','A',90,35,0,0,'',");			// indexType, snsButtonHide, m_thumbWidth(exp), m_thumbHeight(thumb), indexWidth, indexHeight
	out.print("'"+svckd+"'");				// cd selected svckind
%>);
set_fileinfo(<%
	String searchFile = "true";
	if(ini1var.charAt(9) == 'F'){
		f = new File(pathOfDir+"/search.txt");
		searchFile = (f.exists() && f.length() > 0) ? "true" : "false";
	}
	else if(ini1var.charAt(9) == 'A'){
		f = new File(pathOfDir+"/search"+xmlExt);
		searchFile = (f.exists() && f.length() > 0) ? "true" : "false";
	}

	f = new File(webSysDir+"/media/media.txt");
	String mediaFile = (f.exists() && f.length() > 0) ? "true" : "false";

	// download
	String downFile = "false";
	String downfile = (String)Ecatalog.get("downfile");
	if(!downfile.equals("")){
		f = new File(pathOfDir+"/"+downfile);
		downFile = f.exists() ? "true" : "false";
	}

	String downexten = "";
	String downtext = "";
	if(downFile.equals("true")){
		downexten = get_extension(downfile);
		f = new File(webSysDir+"/skin5/icon/file_"+downexten+".gif");
		if(!f.exists()) downexten = "non";

		String dtext = (String)Ecatalog.get("downtext");
		int in = dtext.indexOf("|");
		if(in == -1) downtext = "<span>"+dtext+"</span>";
		else downtext = "<span style=\"color:#"+dtext.substring(in+1)+"\">"+dtext.substring(0,in)+"</span>";
	}

	out.print(mokchaFile+","+searchFile+","+mediaFile+","+downFile+",");
	f = new File(pathOfDir+"/scroll.txt");
	if(f.exists() && f.length() > 0) out.print("true,");
	else out.print("false,");

	f = new File(pathOfDir+"/alink.txt");
	if(f.exists() && f.length() > 0) out.print("true,");
	else out.print("false,");

	f = new File(webSysDir+"/flash/flash.txt");
	if(f.exists() && f.length() > 0) out.print("true,");
	else out.print("false,");

	f = new File(pathOfDir+"/picdata.txt");
	if(f.exists() && f.length() > 0) out.print("true,");
	else out.print("false,");

	f = new File(pathOfDir+"/pagesound.txt");
	if(f.exists() && f.length() > 0) out.print("true,");
	else out.print("false,");

	f = new File(pathOfDir+"/coords"+xmlExt);
	if(ini1var.charAt(9) == 'A' && f.exists() && f.length() > 0) out.print("true,");
	else out.print("false,");

	out.print("'"+s000File+"',");
	if(cdmake == true) out.print("'CD',");
	else out.print("'Net',");

	if(ini1var.charAt(11) == 'F'){
		f = new File(pathOfDir+"/fileseq.txt");
		if(f.exists() && f.length() > 0) out.print("'F',");
		else out.print("'N',");
	}
	else if(ini1var.charAt(11) == 'X'){
		f = new File(pathOfDir+"/bookdata"+xmlExt);
		if(f.exists() && f.length() > 0) out.print("'X',");
		else out.print("'N',");
	}
	else out.print("'N',");

	out.print("'"+ini2var.substring(2,3)+"','"+xmlExt+"'");				// file extension, xmlExt
%>);
set_moveinfo(<%
	out.print("'"+ini1var.substring(4,5)+"',");				// pageOverSpeed
	if(ini1var.charAt(12) == 'C') out.print("true,");		// skipLastFirst
	else out.print("false,");
	if(ini1var.charAt(16) == 'N') out.print("false,");		// enlargeClick
	else out.print("true,");
	if(ini2var.charAt(3) == 'Y') out.print("true,");		// enlarge2Step
	else out.print("false,");
	if(ini1var.charAt(5) == 'Y') out.print("true,");		// pairEnlarge
	else out.print("false,");
	if(ini1var.charAt(13) == 'Y') out.print("true,");		// slideEnlarge
	else out.print("false,");
	if(Ecatalog.get("slidetime").equals("")) out.print("800,");			// slidetime
	else out.print(Ecatalog.get("slidetime")+",");
	if(Ecatalog.get("bigslidetime").equals("")) out.print("1600,");		// bigslidetime
	else out.print(Ecatalog.get("bigslidetime")+",");

	out.print(bigImageOnly+",");										// bigImageOnly
	out.print(spDrag+",");												// spDrag
	if(ini2var.charAt(14) == 'Y') out.print("true");					// pageIcon
	else out.print("false");
%>);
set_searchinfo(<%
	out.print("'"+ini1var.substring(9,10)+"',");						// searchMethod
	out.print("'"+Ecatalog.get("searchurl")+"',");
	out.print("'"+searchword+"',");
	if(ini2var.charAt(16) == 'Y') out.print("true,");					// distinctCase
	else out.print("false,");
	out.print("'#ff6600',0.5,false,false");								// searchColor, searchAlpha, coordTeleLink, coordWebLink
%>);
set_soundinfo(<%
	out.print("'"+ini1var.substring(1,2)+"',");							// openingMusicPlay
	out.print("'"+Category.get("music")+"',");
	out.print("'"+ini2var.substring(0,1)+"',");							// soundAppKind
	if(ini2var.charAt(1) == 'Y') out.print("true,");	// pageoverAtSndEnd
	else out.print("false,");
	out.print("'[mp3]',");								// soundLinkHeader
	if(ini2var.charAt(8) == 'Y') out.print("true,");	// sndplayAtPageOpen
	else out.print("false,");
	out.print("true,true,");				// leftRightUnify, markLinkArea
	if(ini1var.charAt(10) == 'Y') out.print("true,");	// sndplayInAlert
	else out.print("false,");
	if(ini1var.charAt(6) == 'Y') out.print("true");		// sndplayInOver
	else out.print("false");
%>);
set_linkinfo(<%
	out.print("'"+Ecatalog.get("shopstr")+"',");
	if(Ecatalog.get("shopcolor").equals("")) out.print("'#FF0000',");
	else out.print("'"+Ecatalog.get("shopcolor")+"',");
	out.print("0.2,false,'','H',");			// displayAlpha, sameIconSize, markingVar, markingKind
	out.print(showDescript+",'");			// showDescript
	out.print(Ecatalog.get("facebook")+"','");				// facebookStr
	out.print(Ecatalog.get("twitter")+"','");				// twitterStr
	out.print(Ecatalog.get("kakao")+"','해석보기',");		// kakaoStr, imgLinkTitle
	out.print("'"+reImage+"',"+reImageWidth+","+reImageHeight);			// reImage
%>);
set_printinfo('fit');				// partPrintMode
set_spacerinfo();
set_wininfo();
set_connman();
set_perman();
set_screenman();
set_debugman();
set_optioninfo(<%
	out.print("'"+start+"','"+userCate+"','"+eclang+"','"+Ecatalog.get("etcopt")+"'");
%>);
set_touchInfo();

function objLoaded(s, obj){			// for opening object
	loaded++;
	if(loaded == loadto){
		if(UnifyInfo.classUnify == true){
			if(UnifyInfo.outpage === true) ConnectMan.load_unifyOutData();
			else ConnectMan.load_unifyData();
		}
		else{
			if(FileInfo.seqFile[PageInfo.cataDir] == "F") ConnectMan.load_fileseqData(PageInfo.cataDir);
			else if(FileInfo.seqFile[PageInfo.cataDir] == "X") ConnectMan.load_bookxmlData(PageInfo.cataDir);
			else onload_func2();
		}
	}
}
function objLoaded2(s, obj, bool){		// for new window object
	//if(bool == true) obj.onload_func2();
}
function objLoaded3(s, obj){			// for link object
	if(s == "linkimg") linkoutObj.push(obj);
	obj.onload_func2();
}

function onload_func(){
	console.log("root : loaded");
	//alert(support_svg());
	linkoutObj = new Array();

	stageWidth = document.documentElement.clientWidth;
	stageHeight = document.documentElement.clientHeight;
	ScreenInfo.innerOriWidth = window.innerWidth;
	console.log("stage : " + stageWidth + " * " + stageHeight);

	if(document.getElementById("mmsect")) menuSide = document.getElementById("mmsect");
	if(document.getElementById("lowersect")) lowerSide = document.getElementById("lowersect");
	smcntDiv = document.getElementById("smcntsect");
	smcDiv = document.getElementById("smcsect1");
	linkoutDiv = document.getElementById("linkoutsect");

	set_cursorObj();
	if(mediaPlayer) set_mediaObj();

	encDiv = document.getElementById("encsect");
	guideDiv = document.getElementById("guidesect");
	newinDiv = document.getElementById("newinsect");
	indexDiv = document.getElementById("indexsect");
	glassesDiv = document.getElementById("glassessect");
	soundDiv = document.getElementById("soundsect");
	combopopDiv = document.getElementById("combopopsect");
	descriptDiv = document.getElementById("descriptsect");
	hideDiv = document.getElementById("hidesect");
	bbwDiv = document.getElementById("bbwsect");
	memoDiv = document.getElementById('memosect');
	ushapeDiv = document.getElementById("usershape");
	pageiconDiv = document.getElementById("pageiconsect");
	alertDiv = document.getElementById("alertsect");
	debugDiv = document.getElementById("debugsect");
	expandDiv = document.getElementById("expandsect");

	if(SkinInfo.smcenter === "000" || SkinInfo.onesmc == true) document.getElementById("smctrsect").style.display = "none";

	set_cataRectm(<%=arrayJoin(",", arscrPos)%>);			// x, y, right, bottom
	load_license();
}
function after_loadLicense(){
	if(licenseSvg.check_domain() == false) return;
	if(PermitMan.get_openingPerm() === false) return;
	console.log("License approved : " + CataInfo.useDomain);
	objLoaded("root", this);
}

function onload_func2(){
	maincnvDiv = document.getElementById("maincnvsect");
	mainCnv = document.getElementById("maincnv");
	if(ScreenInfo.smImageDiff === true) smcntDiv.style.overflow = "visible";

	init_link();
	init_smc();
	init_smc2();
	MenuObj.onload_func2();

<% if(smc.equals("004") || smc.equals("007")){ %>
	smcDiv.style.visibility = "hidden";
	linkoutDiv.style.visibility = "hidden";
<% } else{ %>
	SmcObj.onload_func2();
<% } %>

<% if(indexType.equals("H")){ %>
	//if(document.getElementById("indexbtn")) document.getElementById("indexbtn").style.visibility = "hidden";
	ConnectMan.load_mokchaData("mhori");
<% } %>

	if(MoveInfo.pageIcon === true) CoverObj.show_pageIcon();
	set_location();

	//debugDiv.style.visibility = "visible";
	ConnectMan.load_cataboxData();
}
function after_openingData(){			// from ConnectMan.load_flashData()
	Action.drawCondition = 0;
	MenuObj.show_pageNumber();
	SmcObj.do_afterMainFinish();
	//if(MenuInfo.menuAutoHide == true){
	//	do_menuAnimate('hide');
	//	do_lowerAnimate('hide');
	//}
	if(MenuInfo.openingIndexView == true) indexbtnClick();
	if(SkinInfo.helpmc === "002") CoverObj.show_helpMessage();
	<% if(ini1var.charAt(19) == 'N') out.print("focus();\n"); %>
}

function set_location(){
<% if(display.equals("012")){ %>
	menuSide.style.width = (winRect.width > stageWidth) ? winRect.width + "px" : "calc(100% - 4px)";
	lowerSide.style.top = cataRect.bottom + "px";
<% } else if(display.equals("018")){ %>
	menuSide.style.width = (winRect.width > stageWidth) ? winRect.width + "px" : "100%";
	menuSide.style.top = cataRect.bottom + "px";
	backDiv.style.height = winRect.height + "px";
<% } else if(display.equals("020") || display.equals("021") || display.equals("030") || display.equals("301")){ %>
	menuSide.style.width = (winRect.width > stageWidth) ? winRect.width + "px" : "100%";
	menuSide.style.top = cataRect.bottom + "px";
<% } else if(dClass.equals("041")){ %>
	menuSide.style.width = (winRect.width > stageWidth) ? winRect.width + "px" : "100%";
<% } else{ %>
<% if(dClass.equals("016")){ %>
	backDiv.style.height = winRect.height + "px";
<% } %>
	menuSide.style.width = (winRect.width > stageWidth) ? winRect.width + "px" : "100%";
	lowerSide.style.width = (winRect.width > stageWidth) ? winRect.width + "px" : "100%";
	lowerSide.style.top = cataRect.bottom + "px";
<% } %>

	smcntDiv.style.width = winRect.width + "px";
	smcntDiv.style.height = winRect.height + "px";

	smcX = smRect.x;
	smcDiv.style.left = smRect.x + "px";
	smcDiv.style.top = smRect.y + "px";
	smcDiv.style.width = smRect.width + "px";
	smcDiv.style.height = smRect.height + "px";

	if(Action.drawCondition < 1){
		linkoutDiv.style.left = smRect.x + "px";
		linkoutDiv.style.top = smRect.y + "px";
	}
	else{
		linkoutDiv.style.left = EncObj.encX + "px";
		linkoutDiv.style.top = EncObj.encY + "px";
	}

	if(OnsmObj) OnsmObj.set_location();

	if(MoveInfo.pageIcon === true){
		pageiconDiv.style.top = (cataRect.bottom-40) + "px";
		pageiconDiv.style.left = Math.floor(winRect.centerPt.x-CoverObj.piWidth/2) + "px";
	}

	if(LinkInfo.showDescript == true){
		descriptDiv.style.left = (cataRect.centerPt.x - descriptDiv.clientWidth/2) + "px";
		descriptDiv.style.top = (SkinInfo.smc == "008") ? (smRect.bottom+25) + "px" : (smRect.bottom+5) + "px";
	}
}

function onresize_func(){
	if(Math.abs(window.orientation) == ScreenInfo.orientation) return;
	ScreenInfo.orientation = Math.abs(window.orientation);

	stageWidth = document.documentElement.clientWidth;
	stageHeight = document.documentElement.clientHeight;

	set_cataRectm(<%=arrayJoin(",", arscrPos)%>);			// x, y, right, bottom
	MenuObj.onresize_func();

	if(Action.mainState === "alert") load_alert('', false);
	else if(Action.mainState === "print") load_print(false);
	else if(Action.mainState === "index") load_indexm(false);
	else if(Action.mainState === "combo") load_combo(false);
	else if(Action.mainState === "mail") load_mail(false);
	else if(Action.mainState === "help") load_help(false);
	else if(Action.mainState === "explorer") load_explorer(false);
	else if(Action.mainState === "spacer") load_spacer('', false);

	if(bboardSvg) load_blackboard(false);
	if(soundctrlSvg) loc_soundcontrol();
	if(EncObj){
		encDiv.style.left = winRect.x+"px";
		encDiv.style.top = winRect.y+"px";
		encDiv.style.width = winRect.width+"px";
		encDiv.style.height = winRect.height+"px";
		EncObj.onresize_func();
	}

	SmcObj.onresize_func();
	LinkObj.loc_linkData();
	set_location();
}
function set_galleryProperty(w, h){
	ScreenInfo.set_gallerySmProperty(w, h);
	set_cataRectm(<%=arrayJoin(",", arscrPos)%>);			// x, y, right, bottom
	set_location();
}

if(navigator.userAgent.match(/(iPad|iPhone|iPod)/g)){
	window.onorientationchange = onresize_func;
}
else{
	window.onresize = onresize_func;
}

document.addEventListener("touchstart", function(e){ Action.do_touchStart("root", e); }, false);
//document.addEventListener("touchmove", function(e){}, false);
document.addEventListener("touchend", function(e){ Action.do_touchEnd("root", e); }, false);
</script>
<noscript>
<style> 
	body{margin:0;overflow:hidden;width:100%;height:100%;background-color:#777777;color:#FFFFFF;}
</style>
</noscript>
</head>

<body onload="onload_func();">

<noscript>
<p style="text-align:center;width:100%;">
	브라우저가 자바스크립트를 지원하지 않으면 현재 페이지를 볼 수 없습니다.
<% if(cdmake == false){ %>
	<br>
	지원 가능하도록 설정을 변경하거나 표준화된 링크형 페이지로 이동하십시오.<br>
	<a href="./access/ecatalogt.jsp?<%=queryStr%>&acmode=js">이동하기</a>
<% } %></p>
</noscript>

<% if(cdmake == false){ %>
<a id="disable" class="disable5m" href="./access/ecatalogt.jsp?<%=queryStrDir%>&acmode=disable"
title="스크린 리더(TalkOver, VoiceOver) 사용자를 위한 HTML 기반의 대체 페이지 입니다." tabindex="0">
<p><%=Ecatalog.get("titlebar")%> 대체 페이지 바로가기</p></a>
<% } %>

<!-- logo -->
<% if(!Display.get("logo").equals("none") && !Display.get("logo").equals("none2") && !display.equals("012") && !logoFile.equals("")){ %>
<div id="logosect" style="left:<%=logoPosx%>px;top:<%=logoPosy%>px;">
<% if(!Ecatalog.get("logolink").equals("")){ %>
<a href="<%=Ecatalog.get("logolink")%>" target="_blank"><img src="<%=logoFile%>" alt="로고"></a>
<% } else{ %>
<img src="<%=logoFile%>" alt="로고">
<% } %>
</div>
<% } %>

<!-- main menu -->
<% String displayFile = "skin5/inc_display"+display+".jsp"; %>
<jsp:include page="<%=displayFile%>" flush="true">
	<jsp:param name="incview" value="<%=incView%>"/>
	<jsp:param name="indexType" value="<%=indexType%>"/>
	<jsp:param name="combokind" value="<%=Skin.get(\"combokind\")%>"/>
	<jsp:param name="downFile" value="<%=downFile%>"/>
	<jsp:param name="downFileName" value="<%=Ecatalog.get(\"downfile\")%>"/>
	<jsp:param name="downexten" value="<%=downexten%>"/>
	<jsp:param name="downtext" value="<%=URLEncoder.encode(downtext,\"utf-8\")%>"/>
	<jsp:param name="webServer" value="<%=webServer%>"/>
	<jsp:param name="Dir" value="<%=Dir%>"/>
	<jsp:param name="catimage" value="<%=catimage%>"/>
	<jsp:param name="printKindmc" value="<%=Skin.get(\"printKindmc\")%>"/>
	<jsp:param name="dirName" value="<%=URLEncoder.encode((String)CataKind.get(\"dirName\"),\"utf-8\")%>"/>
	<jsp:param name="searchFile" value="<%=searchFile%>"/>
	<jsp:param name="mediaFile" value="<%=mediaFile%>"/>
	<jsp:param name="mokchaFile" value="<%=mokchaFile%>"/>
	<jsp:param name="skinMedia" value="<%=Skin.get(\"media\")%>"/>
	<jsp:param name="skinSpacer" value="<%=Skin.get(\"spacerbutton\")%>"/>
	<jsp:param name="mailButtonHide" value="<%=mailButtonHide%>"/>
</jsp:include>

<!-- smc -->
<div id="smcntsect">
<div id="smcsect1">
<svg version="1.1" id="smcsvg1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<filter id="approveMatrix">
<feColorMatrix in="SourceGraphic" type="matrix" values="0.1 0 0 0 0  0 0.1 0 0 0  0 0 0.1 0 0  0 0 0 1 0" />
</filter>
<g id="smcntg1">
<g id="smcImgL1"></g>
<g id="smcImgR1"></g>
<g id="smcSearchL1"></g>
<g id="smcSearchR1"></g>
<g id="smcLinkL1"></g>
<g id="smcLinkR1"></g>
</g>
</svg>
<audio id="smcaudio" class="audio_ctrlr_hidden">브라우저가 audio를 지원하지 않습니다.</audio>
</div>

<div id="smcsect0">
<svg version="1.1" id="smcsvg0" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g id="smcntg0">
<g id="smcImgL0"></g>
<g id="smcImgR0"></g>
<g id="smcSearchL0"></g>
<g id="smcSearchR0"></g>
<g id="smcLinkL0"></g>
<g id="smcLinkR0"></g>
</g>
</svg>
</div>

<div id="smcsect2">
<svg version="1.1" id="smcsvg2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g id="smcntg2">
<g id="smcImgL2"></g>
<g id="smcImgR2"></g>
<g id="smcSearchL2"></g>
<g id="smcSearchR2"></g>
<g id="smcLinkL2"></g>
<g id="smcLinkR2"></g>
</g>
</svg>
</div>
</div>

<!-- sync circle -->
<div id="pageiconsect"></div>

<!-- main canvas -->
<div id="maincnvsect"><canvas id="maincnv"></canvas></div>

<!-- smcenter -->
<div id="smctrsect"></div>

<!-- ani canvas -->
<% if(smc.equals("004") || smc.equals("005") || smc.equals("007") || smc.equals("008") || smc.equals("009")){ %>
<div id="anicnvsect"></div>
<% } %>

<div id="linkoutsect"><audio id="linkoutaudio" class="audio_ctrlr_hidden">브라우저가 audio를 지원하지 않습니다.</audio></div>
<div id="descriptsect"></div>
<div id="soundsect"></div>
<div id="combopopsect" class="combopop<%=Skin.get("cpskin")%>"></div>
<div id="glassessect"></div>
<div id="encsect"></div>
<div id="guidesect"></div>
<div id="bbwsect"></div>
<div id="memosect"></div>
<div id="usershape"></div>
<div id="expandsect"></div>

<div id="hidesect"></div>
<div id="licensesect"></div>

<div id="newinsect" class="newinE<%=Skin.get("newin")%>"></div>
<div id="indexsect" class="<%=Skin.get("idxnewin")%>"></div>
<div id="alertsect" class="newinE<%=Skin.get("newin")%>"></div>

<div id="debugsect"></div>
<div id="cursorsect"></div>

<script>
	loadto = <%=loadto%>;
	if(document.getElementById("logosect")) logoDiv = document.getElementById("logosect");
	if(document.getElementById("anicnvsect")) anicnvDiv = document.getElementById("anicnvsect");
</script>

</body>
</html>
