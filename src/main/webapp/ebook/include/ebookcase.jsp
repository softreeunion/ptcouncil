<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%@ include file="../inc_main.jsp" %>
<%@ include file="inc_catalist.jsp" %>
<%
	catimage = get_param1(request.getParameter("catimage"),5);
	Dir = get_param2(request.getParameter("Dir"),10);

	String sort = request.getParameter("sort");
	if(sort == null) sort = "";
	if(!sort.equals("name") && !sort.equals("rname") && !sort.equals("code") && !sort.equals("rcode")) sort = "";

	String qtxt = get_qtxt3(request.getParameter("qtxt"),50);

	String selfName = "catalist5.jsp";
	int thumbWidth = 100;
	int thumbHeight = 130;

	String agentstr = request.getHeader("User-Agent");
	if(agentstr == null) agentstr = "";
	else{
		agentstr = agentstr.replaceAll("\n","");
		agentstr = agentstr.replaceAll("\r","");
		agentstr = agentstr.replaceAll("\t","");
	}

	String[] aaa = get_machine(agentstr);
	String usrmName = aaa[0];
	String usrmKind = aaa[1];

	String source = "";
	Hashtable ahash = new Hashtable();
	File f = new File(webSysDir + "/catImage/bookcase.txt");
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);
			String sval = source.substring(in+1);

			ahash.put(skey, sval);
		}
		fin.close();
		bin.close();
	}

	String bcskin = ahash.containsKey("bcskin") ? (String)ahash.get("bcskin") : "";
	String ini1var = ahash.containsKey("ini1var") ? (String)ahash.get("ini1var") : "";
	String basicnick = ahash.containsKey("basicnick") ? (String)ahash.get("basicnick") : "";

	char ptflash = 'N';
	if(ini1var.length() > 0) ptflash = ini1var.charAt(0);
	char catmove = 'N';
	if(ini1var.length() > 1) catmove = ini1var.charAt(1);

	StringBuffer jsbuf = new StringBuffer("");
	StringBuffer ciselbuf = new StringBuffer("");

	int thmMargin = 5;
	if(catmove == 'Y'){				// 최상위 & 분류 선택 이동
		Vector arcimage = get_catimagelist();

		int alen = arcimage.size();
		for(int i = 0;i < alen;i++){
			source = (String)arcimage.get(i);
			String[] bbb = mysplit(source,"\t");		// cdir \t dist \t name \t date

			ciselbuf.append("<option value=\""+bbb[0]+"\">"+bbb[2]+"</option>\n");

			if(i == 0) jsbuf.append("if(val == \""+bbb[0]+"\"){\n");
			else jsbuf.append("else if(val == \""+bbb[0]+"\"){\n");

			Vector arcate = get_catalist(bbb[0], "", "", 2, "");			// catimage, sdir, sort, catein, qtxt
			int k = 0;
			int blen = arcate.size();
			if(blen == 0){
				jsbuf.append("	obj.options[0] = new Option(\"전체\",\"\");\n");
			}
			else{
				jsbuf.append("	obj.options[0] = new Option(\"전체\",\"\");\n");
				for(int j = 0;j < blen;j++){
					source = (String)arcate.get(i);
					String[] ccc = mysplit(source,"|");		// code, name, catimage, sdir, image, sentence
					jsbuf.append("	obj.options["+(j+1)+"] = new Option(\""+ccc[1]+"\",\""+ccc[0]+"\");\n)");
					k++;
				}
			}
			jsbuf.append("	obj.length = " + (k+1) + ";\n");
			jsbuf.append("}\n");
		}
	}
	else if(catmove == 'C'){			// 분류만 선택 이동
		Vector arcate = get_catalist(catimage, "", "", 2, "");			// catimage, sdir, sort, catein, qtxt
		int blen = arcate.size();
		for(int j = 0;j < blen;j++){
			source = (String)arcate.get(j);
			String[] ccc = mysplit(source,"|");
			ciselbuf.append("<option value=\"" + ccc[0] + "\">" + ccc[1] + "</option>\n");
		}
	}
	else{
		thmMargin = 36;
	}

	String backimage = webServer+"/include/bookcase1.jpg";
	int cataboxMargin = 3;
	if(bcskin.equals("002")){
		backimage = webServer+"/include/bookcase2.jpg";
		cataboxMargin = 10;
	}
	else if(bcskin.equals("003")){
		backimage = webServer+"/include/bookcase3.jpg";
		cataboxMargin = 20;
	}

	int alen = 0;
	Vector arlist = new Vector();
	if(!catimage.equals("")){
		String real_catimage = catimage;
		if(catimage.equals("B")) real_catimage = "";
		arlist = get_catalist(real_catimage, Dir, sort, 0, qtxt);
	}
	else{
		if(!Dir.equals("")){
			arlist = get_catalist("", Dir, sort, 0, qtxt);
		}
		else{
			arlist = get_catalist("", "", sort, 0, qtxt);			// 기본 최상위분류 내의 카탈로그 리스트
			Vector arcimage = get_catimagelist();
			alen = arcimage.size();
			for(int i = 0;i < alen;i++){
				source = (String)arcimage.get(i);
				String[] bbb = mysplit(source, "\t");						// cdir \t dist \t name \t date
				Vector t_arlist = get_catalist(bbb[0], "", sort, 0, qtxt);	// catimage, sdir, sort, catein, qtxt
				int blen = t_arlist.size();
				if(t_arlist.size() > 0){
					for(int j = 0;j < blen;j++){
						arlist.addElement((String)t_arlist.get(j));
					}
				}
			}
		}
	}

	alen = arlist.size();
	String AddLink = "catimage="+catimage+"&Dir="+Dir+"&qtxt="+qtxt+"&sort="+sort;
	int Punit = 15;
	int Pnum = (int)((alen-1)/Punit) + 1;

	int Pg = 1;
	String tPg = get_param1(request.getParameter("Pg"),5);
	if(tPg == null || tPg.equals("")) Pg = 1;
	else Pg = Integer.parseInt(tPg);

	int bb = (Pg-1) * Punit;
	int bf = (Pg == Pnum) ? alen : bb + Punit;

	StringBuffer arrstr = new StringBuffer();
	int k = 0;
	for(int i = 0;i < alen;i++){
		int counts = i + 1;
		if(bb < counts && counts <= bf){
			source = (String)arlist.get(i);
			arrstr.append("arr["+k+"] = \""+source+"\".split(\"|\");\n");			// code|name|catimage|directory|repr. image|repr. text
			k++;
		}
	}
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog Bookcase Page</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
	body{margin:0;background-color:#FFFFFF;}
	td, p, div, select{font-size:12px;font-family:굴림;text-decoration:none;}
	form{display:inline;margin:0;padding:0;}
	img{vertical-align:middle;border:0;}

	.bookcase{display:block;position:relative;width:588px;height:701px;margin:0 auto;padding:0;
		background-image:url('<%=backimage%>');background-size:100% 100%;background-repeat:no-repeat;}

	.catabox{display:block;position:relative;width:auto;height:28px;margin:<%=cataboxMargin%>px 30px 0 0;padding:0;text-align:right;}
	#thumbcont{z-index:2;display:block;position:absolute;}
	#thumbcont p{display:block;position:absolute;margin:0;padding:0;}
	#thumbcont p img{display:block;width:100%;height:100%;}

	#bottomcnt{display:block;position:absolute;bottom:30px;}
	#pagingsect{display:block;width:530px;height:34px;margin:0 0 0 35px;text-align:center;background-image:url('black40.png');}
	#pagingsect p{display:inline-block;width:15px;height:14px;margin:0;padding:0;line-height:14px;font-family:Tahoma;}
	#pagingsect p a{text-decoration:none;color:#ffffff;}

	.currpage{background-color:#666666;}
	.nextpage{background-color:#444444;}
	.nextpage:hover{background-color:#333333;}

	#searchsect{display:block;float:right;margin:5px 0 0 0;}
	.qtxt{font-size:12px;width:120px;height:15px;margin-left:2px;border:2px solid #444444;-webkit-border-radius:0px;-moz-border-radius:0px;}
	.qsubmit{height:20px;font-size:12px;background-color:#555555;color:#FFFFFF;border:1px solid #555555;}
	.qsubmit:hover{background-color:#444444;}

	.nolist{color:#000000;height:30px;}
	.clearx{clear:both;height:0;}
	#divmessage{position:absolute;left:0;top:0;visibility:hidden;z-index:100;border:1px solid #999999;background-color:#FFFFFF;padding:4px;}
	.thtitle{display:block;height:20px;margin:2px 0 0 0;overflow:hidden;text-align:center;color:#000000;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script>
var catimage = "<%=catimage%>";
var Dir = "<%=Dir%>";
var stageWidth;
var caseOriWidth = 588, caseOriHeight = 701;
var caseWidth, caseHeight;

var thumbOriWidth = <%=thumbWidth%>;
var thumbOriHeight = <%=thumbHeight%>;
var thumbWidth, thumbHeight;
var incview = "<%=usrmKind%>";
var supportHTML5 = true;
var ratio = 1.0;
var catmove = "<%=catmove%>";
var spacingH = 10, spacingV = 52;
var colQty = 5;
var rowQty = 3;
var winpad = 20;
var thumbcontTop = 65;
var thumbcontHeight = 0;
var titlePos = 0;

var arr = [];
var ratio = 1.0;

function onload_func(){
	if(support_html5() == false) supportHTML5 = false;

	<%=arrstr.toString()%>

	message = document.getElementById("divmessage");
	stageWidth = document.documentElement.clientWidth;
	rowQty = Math.floor(14/colQty) + 1;

	caseWidth = caseOriWidth;
	caseHeight = caseOriHeight;
	thumbWidth = thumbOriWidth;
	thumbHeight = thumbOriHeight;

	if(caseWidth > stageWidth){
		caseWidth = stageWidth;
		caseHeight = Math.floor(caseWidth * caseOriHeight / caseOriWidth);

		document.getElementById("mainsect").style.width = caseWidth + "px";
		document.getElementById("mainsect").style.height = caseHeight + "px";

		thumbWidth = Math.floor((caseWidth-colQty*spacingH)/(colQty+1));
		thumbHeight = Math.floor(thumbWidth * thumbOriHeight / thumbOriWidth);

		ratio = caseWidth / caseOriWidth;
		//spacingV = spacingV * ratio;
		spacingV = (thumbOriHeight+spacingV)*ratio - thumbHeight;
	}

	var cw = thumbWidth*colQty + spacingH*(colQty-1);
	var mg = Math.floor((caseWidth - cw)/2);

	document.getElementById("thumbcont").style.width = cw+"px";
	document.getElementById("thumbcont").style.left = mg+"px";

	thumbcontTop = (thumbcontTop+thumbOriHeight)*ratio - thumbHeight;
	document.getElementById("thumbcont").style.top = thumbcontTop+"px";
	//document.getElementById("thumbcont").style.top = thumbcontTop+"px";

	var tabidx = 1;
	var s = "", reptext = "", reimage = "";
	var tx, ty;
	for(var i = 0;i < arr.length;i++){
		reptext = (arr[i][5] == "") ? arr[i][1] : arr[i][5];
		reimage = (arr[i][4] == "") ? "<%=webServer%>/include/noimage.gif" : "<%=webServer%>/catImage"+arr[i][2]+"/"+arr[i][3]+"/"+arr[i][4];

		tx = (thumbWidth+spacingH)*(i%colQty);
		ty = (thumbHeight+spacingV)*Math.floor(i/colQty);
		if(titlePos === 1) ty -= 11;

		s += "<p id='thumbp"+i+"' style=\"width:"+thumbWidth+"px;height:"+thumbHeight+"px;left:"+tx+"px;top:"+ty+"px;\">";
		s += "<a href=\"javascript:link_click('"+arr[i][3]+"&amp;catimage="+arr[i][2]+"');\" tabindex='"+tabidx+"' ";
		if(incview === "d"){
			s += "onmouseOver=\"link_over(event,'"+reptext+"');\" onmouseMove=\"link_move(event);\" onMouseOut=\"link_out(event);\">";
			s += "<img src='"+reimage+"' alt='"+reptext+"'></a>";
			if(titlePos === 1) s += "<span class='thtitle' style=\"width:"+thumbWidth+"px;\">"+reptext+"</span>";
		}
		else{
			s += "><img src='"+reimage+"' alt='"+reptext+"'></a>";
		}
		s += "</p>\n";
	}
	document.getElementById("thumbcont").innerHTML = s;
	thumbcontHeight = ty + thumbHeight;
	document.getElementById("thumbcont").style.height = thumbcontHeight+"px";

	var pagingTop = thumbcontTop + thumbcontHeight + 40*ratio;
	document.getElementById("pagingsect").style.width = (caseWidth-58*ratio)+"px";
	document.getElementById("pagingsect").style.height = (34*ratio)+"px";
	document.getElementById("pagingsect").style.marginLeft = (35*ratio)+"px";
	document.getElementById("pagingsect").style.lineHeight = (34*ratio)+"px";

	document.getElementById("bottomcnt").style.bottom = (30*ratio)+"px";
	document.getElementById("searchsect").style.right = (25*ratio)+"px";
}
function show_next(){
	var val = document.selectform.catimage.value;
	var obj = document.selectform.Dir;
	<%=jsbuf.toString()%>
}
function link_over(e, tstr){
	message.innerHTML = "<span>" + tstr + "</span>";
	message.style.visibility = "visible";
	message.style.left = e.clientX + 10 + document.documentElement.scrollLeft + "px";
	message.style.top = e.clientY + 15 + document.documentElement.scrollTop + "px";
}
function link_move(e){
	message.style.left = e.clientX + 10 + document.documentElement.scrollLeft + "px";
	message.style.top = e.clientY + 15 + document.documentElement.scrollTop + "px";
}
function link_out(e){
	message.innerHTML = "";
	message.style.visibility = "hidden";
}
function link_click(param){
	if(incview === "d"){
		if(od_platform == "flash" || od_platform == "" || supportHTML5 == false) ecatalog('..', '', param, '');
		else ecatalog('*..', '', param, '');
	}
	else{
		var docu = "";
		if(od_platform == "flash" || od_platform == ""){
			docu = "../access/ecatalogs.jsp?Dir="+param;
		}
		else{
			docu = "../ecatalogm.jsp?Dir="+param;
		}

		window.location = docu;
		//if(navigator.userAgent.indexOf("KAKAOTALK") != -1) 
		//else window.open(docu);
	}
}
</script>
</head>

<body onload="onload_func();">

<div id="contents">
	<div class="top_area">
		<div class="location"><a href="./">HOME</a> &gt; <a href="#none">자료실</a> &gt; <strong>뷰어프로그램</strong></div><!-- 14.08.29 : 링크수정 -->
		<h1>뷰어프로그램</h1>
	</div>
	<div class="conts">
		<div class="view_pro">

<div id="divmessage"></div>

<div id="mainsect" class="bookcase">

<% if(catmove == 'Y'){ %>
<div class="catabox">
<form name="selectform" method="get" action="<%=selfName%>">
<select name="catimage" onchange="show_next();">
	<option value="">전체</option>
	<option value="B"><%=basicnick%></option>
	<%=ciselbuf.toString()%>
</select>
<select name="Dir">
	<option value="">전체</option>
</select>
<input type="submit" value="이동" class="qsubmit">
<input type="hidden" name="sort" value="<%=sort%>">
</form>
</div>

<% } else if(catmove == 'C'){ %>
<div class="catabox">
<form name="selectform" method="get" action="<%=selfName%>">
<select name="Dir">
	<option value="">전체</option>
	<%=ciselbuf.toString()%>
</select>
<input type="submit" value="이동" class="qsubmit">
<input type="hidden" name="catimage" value="<%=catimage%>>">
<input type="hidden" name="sort" value="<%=sort%>">
</form>
</div>
<% } %>

<div id="thumbcont"></div>

<div class="clearx"></div>

<div id="bottomcnt">
<div id="pagingsect">
<%
	int prevNum = (Pg > 1) ? Pg - 1 : 1;
	if(Pg > 1) out.print("<p class='nextpage'><a href='"+selfName+"?"+AddLink+"&Pg="+prevNum+"'> < </a></p>\n");

	int fn = (int)((Pg-1)/10 + 1) * 10;
	for(int i = fn-9;i <= Pnum;i++){
		if(i == Pg) out.print("<p class='currpage'>"+i+"</p>\n");
		else out.print("<p class='nextpage'><a href='"+selfName+"?"+AddLink+"&Pg="+i+"'>"+i+"</a></p>\n");
		if(i >= fn) break;
	}

	int nextNum = (Pg < Pnum) ? Pg + 1 : Pg;
	if(Pg < Pnum) out.print("<p class='nextpage'><a href='"+selfName+"?"+AddLink+"&Pg="+nextNum+"'> > </a></p>\n");
%>
</div>

<div id="searchsect">
<form name="searchform" method="get" action="<%=selfName%>" title="페이지 검색">
<input type="text" name="qtxt" tabindex="16" title="검색어 입력" value="<%=qtxt%>" class="qtxt">
<input type="submit" value="검 색" class="qsubmit">
<input type="hidden" name="Dir" value="<%=Dir%>">
<input type="hidden" name="catimage" value="<%=catimage%>">
<input type="hidden" name="sort" value="<%=sort%>">
</form>
</div>
</div>

</div>

			
			</div>			
	</div>
</div>
</body>
</html>
