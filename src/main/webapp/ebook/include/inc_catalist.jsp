<%!
	// catein (0 : only item, 1: item and category, 2: only category)

	static String mainParam = "";
	static Vector cateDeleted = new Vector();
	static String currCode = "";
	static String currDir = "";

	public Vector get_catimagelist(){
		Vector vt = new Vector();

		File f = new File(webSysDir + "/catImage/catimage.txt");
		if(!f.exists()){
			return vt;
		}

		try{
			String source = "";	
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;
				vt.addElement(source);
			}
			fin.close();
			bin.close();
		}
		catch(IOException e) {
		}

		return vt;
	}

	public Vector get_catalist(String catimage, String sdir, String sorting, int catein, String qtxt){
		File f = new File(webSysDir + "/catImage" + catimage + "/catakind.txt");
		if(!f.exists()){
			return new Vector();
		}

		currCode = "";
		currDir = "";

		if(sdir.equals("")){
			mainParam = "";
		}
		else if(sdir.length() == 10){
			mainParam = "cate";
			currCode = sdir;
		}
		else{
			mainParam = "dir";
			currDir = sdir;
		}

		if(sorting.equals("name") || sorting.equals("rname")) return get_listArraySortName(catimage, sorting, catein, qtxt);
		else return get_listArrayCode(catimage, sorting, catein, qtxt);
	}


	public Vector get_listArrayCode(String catimage, String sorting, int catein, String qtxt){					// unconditionally all items
		set_curruse(webSysDir + "/catImage" + catimage + "/catakind.txt");

		String pcode = "";
		String source = "";
		Vector vt_ori = new Vector();
		Vector vt_reverse = new Vector();

		int step = 0;
		if(!currCode.equals("")) step = get_cateStep(currCode);

		String t_catimage = catimage;
		if(catimage.equals("")) t_catimage = "";

		// step 1은 분류만 있음.
		// 현재 요청 step이 0이면 전체를 보여주고 1나 2이면 자신을 부모로 둔 자식들만 보여줌
		File f = new File(webSysDir + "/catImage" + catimage + "/catakind.txt");
		if(step == 1){
			pcode = currCode.substring(0,2);
			try{
				FileInputStream fin = new FileInputStream(f);
				BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
				while((source = bin.readLine()) != null){
					source = strTrimEach(source);
					if(source.equals("")) continue;

					String[] aaa = mysplit(source, "|");		// cate|setting|name|folder

					String t_folder	= aaa[3];
					if(aaa[3].equals("")) t_folder = "";

					String t_code = aaa[0];
					String p1code = t_code.substring(0,2) + "00000000";
					String svckind = aaa[1].substring(2,3);

					if(!t_code.substring(0,2).equals(pcode)) continue;
					if(t_code.equals(currCode)) continue;
					if(aaa[1].substring(0,1).equals("D") || cateDeleted.contains(p1code)) continue;
					if(!qtxt.equals("") && aaa[2].indexOf(qtxt) == -1) continue;

					if(svckind.equals("0")){
						if(catein == 1 || catein == 2) vt_ori.addElement(t_code+"|"+aaa[2]+"|"+t_catimage+"|||");
					}
					else{
						if(catein == 0 || catein == 1) vt_ori.addElement(t_code+"|"+aaa[2]+"|"+t_catimage+"|"+t_folder+"|"+get_desAndRep(catimage, aaa[3]));
					}
				}
				fin.close();
				bin.close();
			}
			catch(IOException e) {
				return vt_ori;
			}
		}
		else if(step == 2){
			pcode = currCode.substring(0,4);
			try{
				FileInputStream fin = new FileInputStream(f);
				BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
				while((source = bin.readLine()) != null){
					source = strTrimEach(source);
					if(source.equals("")) continue;

					String[] aaa = mysplit(source, "|");		// cate|setting|name|folder

					String t_folder	= aaa[3];
					if(aaa[3].equals("")) t_folder = "";

					String t_code = aaa[0];
					String p1code = t_code.substring(0,2) + "00000000";
					String p2code = t_code.substring(0,4) + "000000";
					String svckind = aaa[1].substring(2,3);

					if(!t_code.substring(0,4).equals(pcode)) continue;
					if(aaa[1].substring(0,1).equals("D") || cateDeleted.contains(p1code) || cateDeleted.contains(p2code)) continue;
					if(!qtxt.equals("") && aaa[2].indexOf(qtxt) == -1) continue;

					if(svckind.equals("0")){
						if(catein == 1 || catein == 2) vt_ori.addElement(t_code+"|"+aaa[2]+"|"+t_catimage+"|||");
					}
					else{
						if(catein == 0 || catein == 1) vt_ori.addElement(t_code+"|"+aaa[2]+"|"+t_catimage+"|"+t_folder+"|"+get_desAndRep(catimage, aaa[3]));
					}
				}
				fin.close();
				bin.close();
			}
			catch(IOException e) {
				return vt_ori;
			}
		}
		else{
			try{
				FileInputStream fin = new FileInputStream(f);
				BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
				while((source = bin.readLine()) != null){
					source = strTrimEach(source);
					if(source.equals("")) continue;

					String[] aaa = mysplit(source, "|");		// cate|setting|name|folder

					String t_folder	= aaa[3];
					if(aaa[3].equals("")) t_folder = "";

					String t_code = aaa[0];
					int t_step = get_cateStep(t_code);
					String p1code = t_code.substring(0,2) + "00000000";
					String p2code = t_code.substring(0,4) + "000000";
					String svckind = aaa[1].substring(2,3);

					if(aaa[1].substring(0,1).equals("D")) continue;
					if(!qtxt.equals("") && aaa[2].indexOf(qtxt) == -1) continue;

					if(t_step == 2){
						if(cateDeleted.contains(p1code)) continue;
					}
					else if(t_step == 3){
						if(cateDeleted.contains(p1code) || cateDeleted.contains(p2code)) continue;
					}

					if(svckind.equals("0")){
						if(catein == 1 || catein == 2) vt_ori.addElement(t_code+"|"+aaa[2]+"|"+t_catimage+"|||");
					}
					else{
						if(catein == 0 || catein == 1) vt_ori.addElement(t_code+"|"+aaa[2]+"|"+t_catimage+"|"+t_folder+"|"+get_desAndRep(catimage, aaa[3]));
					}
				}
				fin.close();
				bin.close();
			}
			catch(IOException e) {
				return vt_ori;
			}
		}

		if(sorting.equals("rcode")){
			int alen = vt_ori.size();
			for(int i = 0;i < alen;i++){
				vt_reverse.addElement(vt_ori.get(alen-i-1));
			}
			return vt_reverse;
		}
		else{
			return vt_ori;
		}
	}

	public Vector get_listArraySortName(String catimage, String sorting, int catein, String qtxt){					// sort by name
		set_curruse(webSysDir + "/catImage" + catimage + "/catakind.txt");

		String pcode = "";
		String source = "";
		Hashtable linebuf = new Hashtable();
		Vector vt = new Vector();

		int step = 0;
		if(!currCode.equals("")) step = get_cateStep(currCode);

		String t_catimage = catimage;
		if(catimage.equals("")) t_catimage = "";

		File f = new File(webSysDir + "/catImage" + catimage + "/catakind.txt");
		if(step == 1){
			pcode = currCode.substring(0,2);
			try{
				FileInputStream fin = new FileInputStream(f);
				BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
				while((source = bin.readLine()) != null){
					source = strTrimEach(source);
					if(source.equals("")) continue;

					String[] aaa = mysplit(source, "|");		// cate|setting|name|folder

					String t_folder	= aaa[3];
					if(aaa[3].equals("")) t_folder = "";

					String t_code = aaa[0];
					String p1code = t_code.substring(0,2) + "00000000";
					String svckind = aaa[1].substring(2,3);

					if(!t_code.substring(0,2).equals(pcode)) continue;
					if(aaa[1].substring(0,1).equals("D") || cateDeleted.contains(p1code)) continue;
					if(!qtxt.equals("") && aaa[2].indexOf(qtxt) == -1) continue;

					if(svckind.equals("0")){
						if(catein == 1 || catein == 2){
							vt.addElement(aaa[2]+"|"+t_code);
							linebuf.put(t_code, t_code+"|"+aaa[2]+"|"+t_catimage+"|||");
						}
					}
					else{
						if(catein == 0 || catein == 1){
							vt.addElement(aaa[2]+"|"+t_code);
							linebuf.put(t_code, t_code+"|"+aaa[2]+"|"+t_catimage+"|"+t_folder+"|"+get_desAndRep(catimage, aaa[3]));
						}
					}
				}
				fin.close();
				bin.close();
			}
			catch(IOException e) {
				return vt;
			}
		}
		else if(step == 2){
			pcode = currCode.substring(0,4);
			try{
				FileInputStream fin = new FileInputStream(f);
				BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
				while((source = bin.readLine()) != null){
					source = strTrimEach(source);
					if(source.equals("")) continue;

					String[] aaa = mysplit(source, "|");		// cate|setting|name|folder

					String t_folder	= aaa[3];
					if(aaa[3].equals("")) t_folder = "";

					String t_code = aaa[0];
					String p1code = t_code.substring(0,2) + "00000000";
					String p2code = t_code.substring(0,4) + "000000";
					String svckind = aaa[1].substring(2,3);

					if(!t_code.substring(0,4).equals(pcode)) continue;
					if(aaa[1].substring(0,1).equals("D") || cateDeleted.contains(p1code) || cateDeleted.contains(p2code)) continue;
					if(!qtxt.equals("") && aaa[2].indexOf(qtxt) == -1) continue;

					if(svckind.equals("0")){
						if(catein == 1 || catein == 2){
							vt.addElement(aaa[2]+"|"+t_code);
							linebuf.put(t_code, t_code+"|"+aaa[2]+"|"+t_catimage+"|||");
						}
					}
					else{
						if(catein == 0 || catein == 1){
							vt.addElement(aaa[2]+"|"+t_code);
							linebuf.put(t_code, t_code+"|"+aaa[2]+"|"+t_catimage+"|"+t_folder+"|"+get_desAndRep(catimage, aaa[3]));
						}
					}
				}
				fin.close();
				bin.close();
			}
			catch(IOException e) {
				return vt;
			}
		}
		else{
			try{
				FileInputStream fin = new FileInputStream(f);
				BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
				while((source = bin.readLine()) != null){
					source = strTrimEach(source);
					if(source.equals("")) continue;

					String[] aaa = mysplit(source, "|");		// cate|setting|name|folder

					String t_folder	= aaa[3];
					if(aaa[3].equals("")) t_folder = "";

					String t_code = aaa[0];
					int t_step = get_cateStep(t_code);
					String p1code = t_code.substring(0,2) + "00000000";
					String p2code = t_code.substring(0,4) + "000000";
					String svckind = aaa[1].substring(2,3);

					if(aaa[1].substring(0,1).equals("D")) continue;
					if(!qtxt.equals("") && aaa[2].indexOf(qtxt) == -1) continue;

					if(t_step == 2){
						if(cateDeleted.contains(p1code)) continue;
					}
					else if(t_step == 3){
						if(cateDeleted.contains(p1code) || cateDeleted.contains(p2code)) continue;
					}

					if(svckind.equals("0")){
						if(catein == 1 || catein == 2){
							vt.addElement(aaa[2]+"|"+t_code);
							linebuf.put(t_code, t_code+"|"+aaa[2]+"|"+t_catimage+"|||");
						}
					}
					else{
						if(catein == 0 || catein == 1){
							vt.addElement(aaa[2]+"|"+t_code);
							linebuf.put(t_code, t_code+"|"+aaa[2]+"|"+t_catimage+"|"+t_folder+"|"+get_desAndRep(catimage, aaa[3]));
						}
					}
				}
				fin.close();
				bin.close();
			}
			catch(IOException e) {
				return vt;
			}
		}

		Collections.sort(vt);
		if(sorting.equals("rname")) Collections.reverse(vt);

		Vector buf = new Vector();
		int alen = vt.size();
		for(int i = 0;i < alen;i++){
			StringTokenizer st = new StringTokenizer((String)vt.get(i), "|");
			String[] aaa = new String[2];
			aaa[0] = (st.hasMoreTokens()) ? st.nextToken() : "";
			aaa[1] = (st.hasMoreTokens()) ? st.nextToken() : "";

			buf.addElement(linebuf.get(aaa[1]));
		}

		return buf;
	}

	public void set_curruse(String path){
		String source = "";

		File f = new File(path);
		try {
			FileInputStream fin = new FileInputStream(f);
			BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;
	
				String[] aaa = mysplit(source, "|");		// cate|setting|name|folder
				String t_code = aaa[0];

				if(aaa[1].substring(0,1).equals("D")){
					cateDeleted.addElement(t_code);
				}
				else if(mainParam.equals("dir") && currDir.equals(aaa[3])){
					currCode = get_parentCode(t_code);
				}
			}
			fin.close();
			bin.close();
		}
		catch(IOException e) {
		}
	}

	public String get_desAndRep(String catimage, String folder){
		String reimage = "";
		String retext = "";
		String titlebar = "";
		String source = "";

		String path = webSysDir + "/catImage" + catimage + "/" + folder + "/ecatalog.txt";
		File f = new File(path);

		if(f.exists()){
			int i = 0;
			try {
				FileInputStream fin = new FileInputStream(f);
				BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
				while((source = bin.readLine()) != null){
					source = strTrimEach(source);
					if(source.equals("")) continue;

					int in = source.indexOf(":");
					String skey = source.substring(0, in);

					if(skey.equals("reimage")) reimage = source.substring(in+1);
					else if(skey.equals("retext")) retext = source.substring(in+1);
					else if(skey.equals("titlebar")) titlebar = source.substring(in+1);
				}
				fin.close();
				bin.close();
			}
			catch(IOException e) {
				//try{out.println("kkkkkkkk");}catch(IOException e1) { }
				return "|";
			}
		}

		if(reimage.equals("")){
			f = new File(webSysDir + "/catImage" + catimage + "/" + folder + "/reimage.jpg");
			if(f.exists()) reimage = "reimage.jpg";
			else{
				f = new File(webSysDir + "/catImage" + catimage + "/" + folder + "/reimage.gif");
				if(f.exists()) reimage = "reimage.gif";
				else{
					f = new File(webSysDir + "/catImage" + catimage + "/" + folder + "/reimage.png");
					if(f.exists()) reimage = "reimage.png";
				}
			}
		}

		if(retext.equals("")) retext = titlebar;
		if(reimage.equals("")) reimage = "";

		return (reimage + "|" + retext.replaceAll("|",""));
	}
%>
