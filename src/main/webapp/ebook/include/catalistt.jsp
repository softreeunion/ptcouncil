<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%@ include file="../inc_main.jsp" %>
<%@ include file="inc_catalist.jsp" %>
<%
	catimage = get_param1(request.getParameter("catimage"),5);
	Dir = get_param2(request.getParameter("Dir"),10);

	String sort = request.getParameter("sort");
	if(sort == null) sort = "";
	if(!sort.equals("name") && !sort.equals("rname") && !sort.equals("code") && !sort.equals("rcode")) sort = "";

	String qtxt = get_qtxt3(request.getParameter("qtxt"),50);

	String selfName = "catalistt.jsp";
	int thumbWidth = 100;
	int thumbHeight = 130;

	//String ecataFile = (od_platform.equals("flash") || od_platform.equals("both")) ? "ecatalog.jsp" : "ecatalog5.jsp";
	String ecataFile = "../access/ecatalogt.jsp";
	
	String source = "";
	Hashtable ahash = new Hashtable();
	File f = new File(webSysDir + "/catImage/bookcase.txt");
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);
			String sval = source.substring(in+1);

			ahash.put(skey, sval);
		}
		fin.close();
		bin.close();
	}

	String bcskin = ahash.containsKey("bcskin") ? (String)ahash.get("bcskin") : "";
	String ini1var = ahash.containsKey("ini1var") ? (String)ahash.get("ini1var") : "";
	String basicnick = ahash.containsKey("basicnick") ? (String)ahash.get("basicnick") : "";

	char ptflash = 'N';
	if(ini1var.length() > 0) ptflash = ini1var.charAt(0);
	char catmove = 'N';
	if(ini1var.length() > 1) catmove = ini1var.charAt(1);

	StringBuffer jsbuf = new StringBuffer("");
	StringBuffer ciselbuf = new StringBuffer("");

	int thmMargin = 5;
	if(catmove == 'Y'){				// 최상위 & 분류 선택 이동
		Vector arcimage = get_catimagelist();

		int alen = arcimage.size();
		for(int i = 0;i < alen;i++){
			source = (String)arcimage.get(i);
			String[] bbb = mysplit(source,"\t");		// cdir \t dist \t name \t date

			ciselbuf.append("<option value=\""+bbb[0]+"\">"+bbb[2]+"</option>\n");

			if(i == 0) jsbuf.append("if(val == \""+bbb[0]+"\"){\n");
			else jsbuf.append("else if(val == \""+bbb[0]+"\"){\n");

			Vector arcate = get_catalist(bbb[0], "", "", 2, "");			// catimage, sdir, sort, catein, qtxt
			int k = 0;
			int blen = arcate.size();
			if(blen == 0){
				jsbuf.append("	obj.options[0] = new Option(\"전체\",\"\");\n");
			}
			else{
				jsbuf.append("	obj.options[0] = new Option(\"전체\",\"\");\n");
				for(int j = 0;j < blen;j++){
					source = (String)arcate.get(i);
					String[] ccc = mysplit(source,"|");		// code, name, catimage, sdir, image, sentence
					jsbuf.append("	obj.options["+(j+1)+"] = new Option(\""+ccc[1]+"\",\""+ccc[0]+"\");\n)");
					k++;
				}
			}
			jsbuf.append("	obj.length = " + (k+1) + ";\n");
			jsbuf.append("}\n");
		}
	}
	else if(catmove == 'C'){			// 분류만 선택 이동
		Vector arcate = get_catalist(catimage, "", "", 2, "");			// catimage, sdir, sort, catein, qtxt
		int blen = arcate.size();
		for(int j = 0;j < blen;j++){
			source = (String)arcate.get(j);
			String[] ccc = mysplit(source,"|");
			ciselbuf.append("<option value=\"" + ccc[0] + "\">" + ccc[1] + "</option>\n");
		}
	}
	else{
		thmMargin = 36;
	}

	String backimage = webServer+"/include/bookcase1.jpg";
	int cataboxMargin = 3;
	String thumbcontMargin = "20px 0 0 20px";
	if(bcskin.equals("002")){
		backimage = webServer+"/include/bookcase2.jpg";
		cataboxMargin = 10;
		thumbcontMargin = "15px 0 0 20px";
	}
	else if(bcskin.equals("003")){
		backimage = webServer+"/include/bookcase3.jpg";
		cataboxMargin = 20;
		thumbcontMargin = "5px 0 0 20px";
	}
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>Catalog List Page</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
	body{margin:0;background-color:#FFFFFF;}
	td, a, p, div, select{font-size:9pt;font-family:굴림;text-decoration:none;}
	form{display:inline;margin:0;padding:0;}
	img{vertical-align:middle;border:0;}

	.bookcase{display:block;position:relative;width:588px;height:701px;margin:0 auto;padding:0;overflow:hidden;
		background-image:url('<%=backimage%>');background-size:100% 100%;background-repeat:no-repeat;}

	.catabox{display:block;width:auto;height:28px;margin:<%=cataboxMargin%>px 15px 0 0;padding:0;text-align:right;}
	.thumbcont{z-index:2;display:block;margin:<%=thumbcontMargin%>;}
	.thumbcont:after{clear:both;content:"";display:table;}
	.thumblist{margin:0;padding:0;list-style-type:none;}
	.thumblist li{display:block;float:left;width:<%=thumbWidth%>px;height:<%=thumbHeight%>px;margin:15px 0 38px 9px;}

	#bottomcnt{display:block;position:absolute;bottom:30px;}
	.searchsect{display:block;float:right;margin:10px 20px 0 0;}
	.qtxt{font-size:9pt;width:120px;height:15px;margin-left:1px;border:2px solid #444444;-webkit-border-radius:0px;-moz-border-radius:0px;}
	.qsubmit{height:20px;font-size:9pt;background-color:#555555;color:#FFFFFF;border:1px solid #555555;vertical-align:middle;}

	.pagingsect{display:block;width:530px;height:34px;margin:0 0 0 35px;padding:0;text-align:center;line-height:34px;background-image:url('<%=webServer%>/include/black40.png');}
	.pagingsect p{display:inline-block;width:15px;height:13px;margin:7px 2px;padding:2px 4px;line-height:13px;}

	.currpage{text-decoration:none;background-color:#666666;font-family:Tahoma;font-size:8pt;color:#ffffff;border:1px solid #777777;}
	.nextpage{text-decoration:none;background-color:#444444;font-family:Tahoma;font-size:8pt;color:#ffffff;border:1px solid #555555;}

	.nolist{color:#000000;height:30px;}
	.clearx{clear:both;height:0;}
	#divmessage{position:absolute;left:0;top:0;visibility:hidden;z-index:100;border:1px solid #999999;background-color:#FFFFFF;padding:4px;}
</style>
<script>
var catimage = "<%=catimage%>";
var Dir = "<%=Dir%>";
var oriwidth = 588;
var oriheight = 701;
var thumbwidth = <%=thumbWidth%>;
var thumbheight = <%=thumbHeight%>;
var ratio = 1.0;
var catmove = "<%=catmove%>";
var s1pos = 30;

function onload_func(){
	message = document.getElementById("divmessage");
}
function show_next(){
	var val = document.selectform.catimage.value;
	var obj = document.selectform.Dir;
	<%=jsbuf%>
}
function link_over(e, tstr){
	message.innerHTML = "<span>" + tstr + "</span>";
	message.style.visibility = "visible";
	message.style.left = e.clientX + 10 + document.documentElement.scrollLeft + "px";
	message.style.top = e.clientY + 15 + document.documentElement.scrollTop + "px";
}
function link_move(e){
	message.style.left = e.clientX + 10 + document.documentElement.scrollLeft + "px";
	message.style.top = e.clientY + 15 + document.documentElement.scrollTop + "px";
}
function link_out(e){
	message.innerHTML = "";
	message.style.visibility = "hidden";
}
</script>
</head>

<body onload="onload_func();">
<div id="divmessage"></div>

<main id="mainsect" class="bookcase">

<% if(catmove == 'Y'){ %>
<div class="catabox">
<form name="selectform" method="get" action="<%=selfName%>">
<select name="catimage" onchange="show_next();">
	<option value="">전체</option>
	<option value="B"><%=basicnick%></option>
	<%=ciselbuf.toString()%>
</select>
<select name="Dir">
	<option value="">전체</option>
</select>
<input type="submit" value="이동" class="qsubmit">
<input type="hidden" name="sort" value="<%=sort%>">
</form>
</div>

<% } else if(catmove == 'C'){ %>
<div class="catabox">
<form name="selectform" method="get" action="<%=selfName%>">
<select name="Dir">
	<option value="">전체</option>
	<%=ciselbuf%>
</select>
<input type="submit" value="이동" class="qsubmit">
<input type="hidden" name="catimage" value="<%=catimage%>">
<input type="hidden" name="sort" value="<%=sort%>">
</form>
</div>
<% } %>

<div class="thumbcont">
<ul class="thumblist">
<%
	int alen = 0;
	Vector arlist = new Vector();
	if(!catimage.equals("")){
		String real_catimage = catimage;
		if(catimage.equals("B")) real_catimage = "";
		arlist = get_catalist(real_catimage, Dir, sort, 0, qtxt);
	}
	else{
		if(!Dir.equals("")){
			arlist = get_catalist("", Dir, sort, 0, qtxt);
		}
		else{
			arlist = get_catalist("", "", sort, 0, qtxt);			// 기본 최상위분류 내의 카탈로그 리스트
			Vector arcimage = get_catimagelist();
			alen = arcimage.size();
			for(int i = 0;i < alen;i++){
				source = (String)arcimage.get(i);
				String[] bbb = mysplit(source, "\t");						// cdir \t dist \t name \t date
				Vector t_arlist = get_catalist(bbb[0], "", sort, 0, qtxt);	// catimage, sdir, sort, catein, qtxt
				int blen = t_arlist.size();
				if(t_arlist.size() > 0){
					for(int j = 0;j < blen;j++){
						arlist.addElement((String)t_arlist.get(j));
					}
				}
			}
		}
	}
	alen = arlist.size();
	String AddLink = "catimage="+catimage+"&Dir="+Dir+"&qtxt="+qtxt+"&sort="+sort;
	int Punit = 15;
	int Pnum = (int)((alen-1)/Punit) + 1;

	int Pg = 1;
	String tPg = get_param1(request.getParameter("Pg"),5);
	if(tPg == null || tPg.equals("")) Pg = 1;
	else Pg = Integer.parseInt(tPg);

	int bb = (Pg-1) * Punit;
	int bf = (Pg == Pnum) ? alen : bb + Punit;

	int tabindex = 1;
	StringBuffer arrstr = new StringBuffer();
	int k = 0;
	for(int i = 0;i < alen;i++){
		int counts = i + 1;
		if(bb < counts && counts <= bf){
			source = (String)arlist.get(i);
			String ccc[] = mysplit(source,"|");			// code|name|catimage|directory|repr. image|repr. text

			String reptext = ccc[5].equals("") ? ccc[1] : ccc[5];
			String reimage = ccc[4].equals("") ? "noimage.gif" : webServer+"/catImage"+ccc[2]+"/"+ccc[3]+"/"+ccc[4];

			out.print("<li id='thumbli"+k+"'><a href='"+ecataFile+"?Dir="+ccc[3]+"&amp;catimage="+ccc[2]+"' tabindex='"+tabindex+"' target='_blank' ");
			out.print("onmouseOver=\"link_over(event,'"+reptext+"');\" onmouseMove=\"link_move(event);\" onMouseOut=\"link_out(event);\">");
			out.print("<img src='"+reimage+"' id='thumbimg"+k+"' width='"+thumbWidth+"' height='"+thumbHeight+"' alt='"+reptext+"'></a></li>\n");

			k++;
			tabindex++;
		}
	}

	if(k == 0) out.print("<li class='nolist'>목록이 없습니다.</li>");
%>
</ul>
</div>

<div id="bottomcnt">
<div class="pagingsect">
<%
	int prevNum = (Pg > 1) ? Pg - 1 : 1;
	if(Pg > 1) out.print("<p><a href='"+selfName+"?"+AddLink+"&amp;Pg="+prevNum+"' class='nextpage'> < </a></p>\n");

	int fn = (int)((Pg-1)/10 + 1) * 10;
	for(int i = fn-9;i <= Pnum;i++){
		if(i == Pg) out.print("<p class='currpage'>"+i+"</p>" );
		else out.print("<p><a href='"+selfName+"?"+AddLink+"&amp;Pg="+i+"' class='nextpage'>"+i+"</a></p>");
		if(i >= fn) break;
	}

	int nextNum = (Pg < Pnum) ? Pg + 1 : Pg;
	if(Pg < Pnum) out.print("<p><a href='"+selfName+"?"+AddLink+"&amp;Pg="+nextNum+"' class='nextpage'> > </a></p>\n");
%>
</div>

<div class="clearx"></div>

<div class="searchsect">
<form name="searchform" method="get" action="<%=selfName%>" title="페이지 검색">
<input type="text" name="qtxt" tabindex="16" title="검색어 입력" value="<%=qtxt%>" class="qtxt">
<input type="submit" value="검 색" class="qsubmit">
<input type="hidden" name="Dir" value="<%=Dir%>">
<input type="hidden" name="catimage" value="<%=catimage%>">
<input type="hidden" name="sort" value="<%=sort%>">
</form>
</div>
</div>

</main>

</body>
</html>
