<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%@ include file="../inc_main.jsp" %>
<%@ include file="inc_catalist.jsp" %>
<%
	catimage = get_param1(request.getParameter("catimage"),5);
	Dir = get_param2(request.getParameter("Dir"),10);

	String sort = request.getParameter("sort");
	if(sort == null) sort = "";
	if(!sort.equals("name") && !sort.equals("rname") && !sort.equals("code") && !sort.equals("rcode")) sort = "";

	String qtxt = get_qtxt3(request.getParameter("qtxt"),50);

	String lkd = request.getParameter("lkd");
	if(lkd == null) lkd = "";

	if(lkd.equals("catimove")){				// 콤보박스 2개
		if(lolang.equals("english")){
			out.print("cati||All\n");											// cati|cdir|name
			out.print("cati|B|Basic Category\n");
		}
		else{
			out.print("cati||전체\n");											// cati|cdir|name
			out.print("cati|B|기본분류\n");
		}

		String source = "";
		Vector vtlist = get_catalist("", "", sort, 2, qtxt);
		int alen = vtlist.size();
		for(int i = 0;i < alen;i++){
			source = (String)vtlist.get(i);

			String[] bbb = mysplit(source, "\t");					// cdir \t dist \t name \t date
			out.print("cate||"+bbb[0]+"|"+bbb[1]+"\n");				// cate|catimage|code|name
		}

		Vector arcimage = get_catimagelist();
		alen = arcimage.size();
		for(int i = 0;i < alen;i++){
			source = (String)arcimage.get(i);

			String[] bbb = mysplit(source, "\t");							// cdir \t dist \t name \t date
			out.print("cati|"+bbb[0]+"|"+bbb[2]+"\n");						// cati|cdir|name

			Vector arcate = get_catalist(bbb[0], "", sort, 2, "");			// catimage, sdir, sort, catein, qtxt
			int blen = arcate.size();
			for(int j = 0;j < blen;j++){
				source = (String)arcate.get(i);

				String[] ccc = mysplit(source, "|");						// code|name|catimage|sdir|image|sentence
				out.print("cate|"+bbb[0]+"|"+ccc[0]+"|"+ccc[1]+"\n");		// cate|catimage|code|name
			}
		}
	}
	else if(lkd.equals("catemove")){	// 콤보박스 1개
		Vector arcate = get_catalist(catimage, "", sort, 2, "");			// catimage, sdir, sort, catein, qtxt
		String source = "";
		int blen = arcate.size();
		for(int i = 0;i < blen;i++){
			source = (String)arcate.get(i);

			String[] ccc = mysplit(source, "|");							// code|name|catimage|sdir|image|sentence
			out.print("cate|"+catimage+"|"+ccc[0]+"|"+ccc[1]+"\n");			// cate|catimage|code|name
		}
	}
	else{
		if(!catimage.equals("")){
			if(catimage.equals("B")) catimage = "";
			Vector vtlist = get_catalist(catimage, Dir, sort, 0, qtxt);		// 기본 최상위 분류(B)로 catimage값 넘김.
			int alen = vtlist.size();
			for(int i = 0;i < alen;i++){
				out.println(vtlist.get(i));
			}
		}
		else{
			if(!Dir.equals("")){
				Vector vtlist = get_catalist("", Dir, sort, 0, qtxt);			// 기본 최상위분류 내의 카탈로그 리스트
				int alen = vtlist.size();
				for(int i = 0;i < alen;i++){
					out.println(vtlist.get(i));
				}
			}
			else{
				Vector vtlist = get_catalist("", "", sort, 0, qtxt);			// 기본 최상위분류 내의 카탈로그 리스트
				String source = "";
				int alen = vtlist.size();
				for(int i = 0;i < alen;i++){
					out.println(vtlist.get(i));
				}

				Vector arcimage = get_catimagelist();
				alen = arcimage.size();
				for(int i = 0;i < alen;i++){
					source = (String)arcimage.get(i);
					String[] bbb = mysplit(source, "\t");		// cdir \t dist \t name \t date

					Vector arlist = get_catalist(bbb[0], "", sort, 0, qtxt);	// catimage, sdir, sort, catein, qtxt

					int blen = arlist.size();
					for(int j = 0;j < blen;j++){
						out.println(arlist.get(j));
					}
				}
			}
		}
	}
%>
