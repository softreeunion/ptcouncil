<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%@ include file="../inc_main.jsp" %>
<%
	catimage = get_param1(request.getParameter("catimage"),5);
	Dir = get_param2(request.getParameter("Dir"),10);

	String sort = request.getParameter("sort");
	if(sort == null) sort = "";
	if(!sort.equals("name") && !sort.equals("rname") && !sort.equals("code") && !sort.equals("rcode")) sort = "";

	String source = "";
	Hashtable ahash = new Hashtable();
	File f = new File(webSysDir + "/catImage/bookcase.txt");
	if(f.exists()){
		FileInputStream fin = new FileInputStream(f);
		BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
		while((source = bin.readLine()) != null){
			source = strTrimEach(source);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			String skey = source.substring(0, in);
			String sval = source.substring(in+1);

			ahash.put(skey, sval);
		}
		fin.close();
		bin.close();
	}

	String bcskin = ahash.containsKey("bcskin") ? (String)ahash.get("bcskin") : "";
	String ini1var = ahash.containsKey("ini1var") ? (String)ahash.get("ini1var") : "";
	String basicnick = ahash.containsKey("basicnick") ? (String)ahash.get("basicnick") : "";

	char ptflash = 'N';
	if(ini1var.length() > 0) ptflash = ini1var.charAt(0);
	char catmove = 'N';
	if(ini1var.length() > 1) catmove = ini1var.charAt(1);

	String agentstr = request.getHeader("User-Agent");
	if(agentstr == null) agentstr = "";
	else{
		agentstr = agentstr.replaceAll("\n","");
		agentstr = agentstr.replaceAll("\r","");
		agentstr = agentstr.replaceAll("\t","");
	}

	String[] aaa = get_machine(agentstr);
	String usrmName = aaa[0];
	String usrmKind = aaa[1];

	if(!usrmKind.equals("d")){
		response.sendRedirect("./catalist5.jsp?catimage="+catimage+"&Dir="+Dir+"&sort="+sort);
		return;
	}

	String htmparam = "catimage=" + catimage + "&amp;Dir=" + Dir + "&sort=" + sort;
	String swfparam = "charset=utf-8&ext=jsp&search=1&seqload=0&sort=" + sort + "&lani=0&subj=0";
	swfparam += "&lolang="+lolang+"&url="+webServer+"&catimage="+catimage+"&Dir="+Dir+"&catmove="+catmove+"&basicnick="+basicnick;

	if(bcskin.equals("002")){
		swfparam += "&bcskin=002&bgimage=bookcase2.jpg&tw=100&th=130&mp=15&sx=12&sy=35&vp=55&scx=417&scy=595&pw=530&py=625&cby=17";
	}
	else if(bcskin.equals("003")){
		swfparam += "&bcskin=003&bgimage=bookcase3.jpg&tw=100&th=130&mp=15&sx=12&sy=35&vp=55&scx=417&scy=595&pw=530&py=625&cby=20";
	}
	else{
		swfparam += "&bcskin=001&bgimage=bookcase1.jpg&tw=100&th=130&mp=15&sx=12&sy=35&vp=55&scx=417&scy=595&pw=530&py=625";
	}
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>eCatalog Bookcase Page</title>
<style>
	body{margin:0;overflow:hidden;background-color:#FFFFFF;}
	#divflash{margin:0 auto;width:600px;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=webServer%>/detectFlashVer.js"></script>
<script>
function onload_func(){
	// Flash Version start
	// Major version  of Flash required
	var requiredMajorVersion = 10;
	// Minor version of Flash required
	var requiredMinorVersion = 0;
	// Minor version of Flash required
	var requiredRevision = 0;

	var hasReqestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);
	if(!hasReqestedVersion){
		window.location = "./catalistt.jsp?<%=htmparam%>";
		return;
	}	
	// Flash Version end
}
function newCatalog(param){
	if(od_platform == "flash" || od_platform == ""){
		ecatalog('..', '', param, '');
	}
	else{
		ecatalog('*..', '', param, '');
	}
}
</script>
</head>

<body onload="onload_func();">

<div id="divflash">
<object id="listSwf" type="application/x-shockwave-flash" data="<%=webServer%>/include/catalist.swf" standby="bookcase" width="600" height="700">
	<param name="movie" value="<%=webServer%>/include/catalist.swf">
	<param name="flashVars" value="<%=swfparam%>">
	<param name="quality" value="high">
	<param name="bgcolor" value="#FFFFFF">
	<param name="allowScriptAccess" value="always">
	<param name="allowFullScreen" value="true">
<% if(lolang.equals("english")){ %>
	<p>If this browser doesn't support Flash or the screen is not displayed, please click the 'View as HTML' link.<br><br>
	<a href="./catalistt.jsp?<%=htmparam%>">View as HTML<a></p>
<% } else{ %>
	<p>브라우저가 플래시를 지원하지 않거나 플래시 화면이 보이지 않으면<br>'HTML로 보기' 링크를 클릭하여 주십시오.<br><br>
	<a href="./catalistt.jsp?<%=htmparam%>">HTML로 보기<a></p>
<% } %>
</object>
</div>

</body>
</html>
