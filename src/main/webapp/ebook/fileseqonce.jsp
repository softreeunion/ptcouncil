<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%@ include file="inc_skin.jsp" %>
<%
	catimage = get_param1(request.getParameter("catimage"),5);
	String pcode = check_cate(request.getParameter("pcode"));

	pathOfOriCata = webSysDir + "/catImage";
	pathOfCata = webSysDir + "/catImage" + catimage;

	if(pcode.equals("")){
		out.print("pcode값은 반드시 필요합니다.");
		return;
	}

	String dirStr = "";
	String pnoStr = "";
	String source = "";

	File f = new File(pathOfDir + "/ecatalog.txt");
	FileInputStream fin = new FileInputStream(f);
	BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
	while((source = bin.readLine()) != null){
		source = strTrimEach(source);
		if(source.equals("")) continue;

		int in = source.indexOf(":");
		String skey = source.substring(0, in);
		if(in == -1) continue;

		if(skey.equals("adir")){
			dirStr = source.substring(in+1);
		}
		else if(skey.equals("apageno")){
			pnoStr = source.substring(in+1);
		}
	}
	fin.close();
	bin.close();

	String[] a_dir = mysplit(dirStr,"|");
	String[] a_pageno = mysplit(pnoStr,"|");

	int tno = 0;
	int alen = a_dir.length;
	for(int i = 0;i < alen;i++){
		f = new File(pathOfCata+"/"+a_dir[i]+"/fileseq.txt");
		if(f.exists()){
			int k = 1;
			pno = Integer.parseInt(a_pageno[i]);

			out.print("folder:"+a_dir[i]+"\n");
			fin = new FileInputStream(f);
			bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
			while((source = bin.readLine()) != null){
				source = strTrimEach(source);
				if(source.equals("")) continue;

				out.print(source+"\n");
				if(k == pno) break;
				k++;
			}
			fin.close();
			bin.close();
		}
	}
%>
