<%@ page import="java.io.*, java.util.*" contentType="text/html;charset=utf-8" %>
<%@ include file="inc_skin.jsp" %>
<%
	catimage = get_param1(request.getParameter("catimage"),5);
	Dir = get_param1(request.getParameter("Dir"),5);
	if(Dir.equals("")) Dir = "1";

	String start = check_start(request.getParameter("start"));
	String callmode = check_callmode(request.getParameter("callmode"));
	String eclang = check_eclang(request.getParameter("eclang"));
	String userCate = check_cate(request.getParameter("Cate"));

	String param = Dir + "&amp;Cate=" + userCate + "&amp;catimage=" + catimage + "&amp;start=" + start + "&amp;callmode=" + callmode + "&amp;eclang=" + eclang;
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>Electronic Catalog(전자 카탈로그)</title>
<script src="<%=webServer%>/common.js"></script>
<script>
	od_platform = "<%=od_platform%>";
</script>
</head>

<body>
<script>
	ecatalog('.','','<%=param%>','');
</script>

<p style="margin-top:60px;text-align:center;">
카탈로그 별도창이 보이지 않으시면 
<a href="javascript:ecatalog('.','fixed','<%=param%>','no');">여기</a>를 클릭하십시오.
</p>
</body>
</html>
