<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%@ include file="inc_skin.jsp" %>
<%@ include file="inc_main.jsp" %>
<%@ include file="ConnLog.jsp" %>
<%
	catimage = get_param1(request.getParameter("catimage"),5);
	Dir = get_param1(request.getParameter("Dir"),5);
	if(Dir.equals("")) Dir = "1";

	String start = check_start(request.getParameter("start"));
	String callmode = check_callmode(request.getParameter("callmode"));
	String eclang = check_eclang(request.getParameter("eclang"));
	String userCate = check_cate(request.getParameter("Cate"));

	String cdmakeNo = "";
	boolean cdmake = false;
	String svckd = "";
	if(callmode.equals("cdmake") || callmode.equals("cdmake_unify")){
		if(callmode.equals("cdmake")) callmode = "normal";
		else if(callmode.equals("cdmake_unify")) callmode = "normal_unify";
		cdmake = true;
		cdmakeNo = get_param1(request.getParameter("makeno"),5);
		svckd = get_param2(request.getParameter("svckd"),1);
	}

	pathOfOriCata = webSysDir + "/catImage";
	pathOfCata = webSysDir + "/catImage" + catimage;
	pathOfDir = webSysDir + "/catImage" + catimage + "/" + Dir;

	String incView = "f";
	String queryStr = "catimage=" + catimage + "&start=" + start + "&callmode=" + callmode + "&eclang=" + eclang;
	String queryStrLink = queryStr + "&Dir=" + Dir + "&Cate=" + userCate;
	String queryStrDir = queryStr + "&Dir=" + Dir;

	String agentstr = request.getHeader("User-Agent");
	if(agentstr == null) agentstr = "";
	else{
		agentstr = agentstr.replaceAll("\n","");
		agentstr = agentstr.replaceAll("\r","");
		agentstr = agentstr.replaceAll("\t","");
	}

	String[] aaa = get_machine(agentstr);
	String usrmName = aaa[0];
	String usrmKind = aaa[1];

	if(cdmake == false){
		if(!usrmKind.equals("d")){
			response.sendRedirect("./ecatalogm.jsp?"+queryStrLink);
			return;
		}
	}
	else{
		usrmName = "";
		usrmKind = "";
	}

	File f = new File(pathOfCata + "/catakind.txt");						// check category configuration file
	if(!f.exists()){
		out.print(echo_script("페이지를 표시할 수 없습니다.(catakind)","","window","window.close();\n"));
		return;
	}

	String unifyCall = "false";
	if(callmode.equals("normal_unify") || callmode.equals("admin_unify")) unifyCall = "true";

	HashMap CataKind = new HashMap();
	if(!userCate.equals("")){					// must find the first cover page out of the catalogs
		set_cateCond(userCate, CataKind);
		if(Dir.equals("")){
			out.print(echo_script("페이지를 표시할 수 없습니다.(no Dir)","","window","window.close();\n"));
			return;
		}
		pathOfDir = pathOfCata + "/" + Dir;
		queryStrLink = queryStr + "&Cate=" + userCate;
		queryStrDir = queryStr + "&Dir=" + Dir;
	}
	else{
		String resstr = set_dirCond(CataKind, unifyCall);
		if(!resstr.equals("")){
			out.print(echo_text(resstr));
			return;
		}
		queryStrLink = queryStrDir;
	}

	if(!CataKind.get("unifyCond").equals("U")) unifyCall = "false";

	String parentCode = (String)CataKind.get("parentCode");
	if(parentCode.equals("")){										// exists parent category?
		out.print(echo_script("페이지를 표시할 수 없습니다.(parentCode)","","window","window.close();\n"));
		return;
	}

	f = new File(pathOfDir);
	if(!f.exists()){
		out.print(echo_script("페이지를 표시할 수 없습니다.(pathOfDir)","","window","window.close();\n"));
		return;
	}

	f = new File(pathOfDir + "/ecatalog.txt");
	if(!f.exists()){				// check catalog configuration file
		out.print(echo_script("페이지를 표시할 수 없습니다.(ecatalog)","","window","window.close();\n"));
		return;
	}
	if(!callmode.equals("admin") && !callmode.equals("admin_unify") && (CataKind.get("svcCond1").equals("D") || CataKind.get("svcCond2").equals("D"))){
		out.print(echo_script("현재는 서비스가 불가합니다.","","window","window.close();\n"));
		return;
	}

	HashMap Category = new HashMap();
	set_category(pathOfCata+"/category.txt", (String)CataKind.get("parentCode"), Category);

	// for moving from category to category
	String moveInDist = "";
	f = new File(pathOfOriCata+"/catimage.txt");
	if(unifyCall.equals("true") && cdmake == false && f.exists()){
		moveInDist = get_catimage(catimage, "moveInDist");
	}

	// *** ecatalog.txt - check whether the total application of anyone folder's configuration exists
	boolean jbool = false;
	String bascadapt = (String)Category.get("bascadapt");
	if(bascadapt == null) bascadapt = "";
	if(unifyCall.equals("false")){
		if(!bascadapt.equals("")){
			f = new File(pathOfCata + "/" + bascadapt + "/ecatalog.txt");
			if(f.exists()) jbool = true;
		}
	}

	HashMap Ecatalog = new HashMap();
	if(jbool == true){
		set_ecatalog(pathOfCata+"/"+bascadapt+"/ecatalog.txt", Ecatalog);
		Ecatalog.put("pageno", get_ecatalogKey(pathOfDir + "/ecatalog.txt", "pageno"));
	}
	else{
		set_ecatalog(pathOfDir+"/ecatalog.txt", Ecatalog);
	}

	if(!callmode.equals("admin") && !callmode.equals("admin_unify") && check_servDate((String)Ecatalog.get("startdate"),(String)Ecatalog.get("enddate"),get_currDate()) == false){
		out.print(echo_script("서비스 기간이 아닙니다.","","window","window.close();\n"));
		return;
	}

	String ini2var = (String)Ecatalog.get("ini2var");
	String xmlExt = (ini2var.length() > 15 && ini2var.charAt(15) == 'T') ? "xml.txt" : ".xml";
	String ini1var = (String)Ecatalog.get("ini1var");

	f = new File(pathOfDir+"/bookdata"+xmlExt);
	if(ini1var.charAt(11) == 'X' && (!f.exists() || f.length() == 0)){
		out.print(echo_script("페이지를 표시할 수 없습니다.(bookdata"+xmlExt+")","","window","window.close();\n"));
		return;
	}

	String cook_counter = "";
	String cook_cpasswd = "";

	if(cdmake == false){
		Cookie[] cookies = request.getCookies();
		if(cookies != null){
			for(int i = 0;i < cookies.length;i++){
				Cookie theCookie = cookies[i];
				if(theCookie.getName().equals("cook_counter")) cook_counter = theCookie.getValue();
				else if(theCookie.getName().equals("cook_cpasswd")) cook_cpasswd = theCookie.getValue();
			}
		}

		String catpasswd = (String)Ecatalog.get("catpasswd");
		if(catpasswd == null) catpasswd = "";
		if(!catpasswd.equals("") && (cook_cpasswd.equals("") || !cook_cpasswd.equals(catpasswd))){				// cat password
			response.sendRedirect("./ecatalog_cpasswd.jsp?"+queryStrLink+"&docu=");
			return;
		}

		/* counter start */
		f = new File(webSysDir + "/log");
		if(!f.exists()) f.mkdirs();

		ConnLog connlog = new ConnLog(streamCharset);
		connlog.calDate();
		connlog.setMoveFile(webSysDir+"/log", "log"+catimage+"_");

	 	if(cook_counter.equals("")){
	 		connlog.recordLog(webSysDir+"/log/log"+catimage+"_"+connlog.today+".txt", "0\t"+Dir+"\t"+connlog.toStr+"\t"+request.getRemoteAddr()+"\t"+agentstr+"\n");
			Cookie c_cook_counter = new Cookie("cook_counter", Dir);
			response.addCookie(c_cook_counter);
		}
		else if(connlog.parse_cookie(Dir, cook_counter) == false){
			connlog.recordLog(webSysDir+"/log/log"+catimage+"_"+connlog.today+".txt", "1\t"+Dir+"\t"+connlog.toStr+"\t"+request.getRemoteAddr()+"\t"+agentstr+"\n");
			Cookie c_cook_counter = new Cookie("cook_counter", cook_counter + ":" + Dir);
			response.addCookie(c_cook_counter);
		}
		/* counter end */
	}
	else{
		if(svckd.equals("C")){
			String bskins = (String)Ecatalog.get("bskin");
			Ecatalog.put("bskin", "000"+bskins.substring(3));
		}
	}

	String logoadapt = (String)Category.get("logoadapt");
	String dist1var = Category.get("fmove")+moveInDist+"000";
	String distcfg = logoadapt+";"+Category.get("mailadapt")+";"+Category.get("music")+";"+Category.get("bascadapt");

	// s000 file
	String s000File = get_logoFile(pathOfDir, "s000");

	// logo
	String logoFile = "";
	if(!logoadapt.equals("")){
		String file = get_logoFile(pathOfCata+"/"+logoadapt, "logo");
		if(!file.equals("")) logoFile = "catImage"+catimage+"/"+logoadapt+"/"+file;
	}
	if(logoFile.equals("")){
		String file = get_logoFile(pathOfDir,"logo");
		if(!file.equals("")) logoFile = "catImage"+catimage+"/"+Dir+"/"+file;
	}
	if(logoFile.equals("")){
		String file = get_logoFile(pathOfOriCata,"logo");
		if(!file.equals("")) logoFile = "catImage/"+file;
	}

	// parameter
	StringBuffer param = new StringBuffer("Dir="+Dir+"&ccode="+CataKind.get("cateCode")+"&svckind="+CataKind.get("svcKind1"));
	if(!start.equals("")) param.append("&start="+start);

	param.append("&logofile="+logoFile+"&encode="+charset+"&lolang="+eclang+"&dist1var="+dist1var+"&distcfg="+distcfg+"&callmode="+callmode);
	param.append("&ini1var="+Ecatalog.get("ini1var")+"&ini2var="+Ecatalog.get("ini2var")+"&frpage="+Ecatalog.get("frpage")+"&pageno="+Ecatalog.get("pageno")+"&showstart="+Ecatalog.get("showstart"));
	param.append("&smwidth="+Ecatalog.get("smwidth")+"&smheight="+Ecatalog.get("smheight")+"&bgwidth="+Ecatalog.get("bgwidth")+"&bgheight="+Ecatalog.get("bgheight"));
	param.append("&midwidth="+Ecatalog.get("midwidth")+"&idxwidth="+Ecatalog.get("idxwidth")+"&idxheight="+Ecatalog.get("idxheight"));
	param.append("&askin="+Ecatalog.get("askin")+"&bskin="+Ecatalog.get("bskin")+"&margin="+Ecatalog.get("margin")+"&bground="+Ecatalog.get("bground"));
	param.append("&shopcolor="+Ecatalog.get("shopcolor")+"&shopstr="+Ecatalog.get("shopstr")+"&logopos="+Ecatalog.get("logopos")+"&logolink="+Ecatalog.get("logolink"));
	param.append("&slidetime="+Ecatalog.get("slidetime")+"&bigslidetime="+Ecatalog.get("bigslidetime")+"&searchurl="+Ecatalog.get("searchurl"));
	param.append("&downfile="+Ecatalog.get("downfile")+"&downtext="+Ecatalog.get("downtext")+"&prepage="+Ecatalog.get("prepage"));

	// file checking
	f = new File(pathOfDir+"/mokcha.txt");
	String cfg_file_check  = (f.exists() && f.length() > 0) ? "Y" : "N";

	if(ini1var.charAt(9) == 'F'){
		f = new File(pathOfDir+"/search.txt");
		cfg_file_check += (f.exists() && f.length() > 0) ? "Y" : "N";
	}
	else if(ini1var.charAt(9) == 'A'){
		f = new File(pathOfDir+"/search"+xmlExt);
		cfg_file_check += (f.exists() && f.length() > 0) ? "Y" : "N";
	}
	else cfg_file_check += "Y";

	f = new File(webSysDir+"/media/media.txt");
	cfg_file_check += (f.exists() && f.length() > 0) ? "Y" : "N";
	if(s000File.equals("")){
		cfg_file_check += "N";
	}
	else{
		String s1 = s000File.substring(s000File.lastIndexOf(".")+1);
		cfg_file_check += s1.substring(0,1).toUpperCase();
	}
	cfg_file_check += (cdmake == true) ? "F" : "S";			// file or script
	cfg_file_check += (cdmake == true) ? "C" : "N";			// network or cd

	f = new File(pathOfDir+"/fileseq.txt");
	if(ini1var.charAt(11) == 'F' && f.exists() && f.length() > 0) cfg_file_check += "Y";
	else if(ini1var.charAt(11) == 'X') cfg_file_check += "Y";
	else cfg_file_check += "N";

	f = new File(pathOfDir+"/"+Ecatalog.get("downfile"));
	cfg_file_check += (unifyCall.equals("false") && !Ecatalog.get("downfile").equals("") && f.exists()) ? "Y" : "N";
	if(unifyCall.equals("true")) cfg_file_check += "U";
	else if(!Ecatalog.get("outpage").equals("")) cfg_file_check += "P";
	else cfg_file_check += "N";

	f = new File(pathOfDir+"/scroll.txt");
	cfg_file_check += (f.exists() && f.length() > 0) ? "Y" : "N";
	f = new File(pathOfDir+"/alink.txt");
	cfg_file_check += (f.exists() && f.length() > 0) ? "Y" : "N";
	f = new File(webSysDir+"/flash/flash.txt");
	cfg_file_check += (f.exists() && f.length() > 0) ? "Y" : "N";
	f = new File(pathOfDir+"/picdata.txt");
	cfg_file_check += (f.exists() && f.length() > 0) ? "Y" : "N";

	cfg_file_check += Ecatalog.get("twitter").equals("") ? "Y" : "N";						// 트위터
	cfg_file_check += Ecatalog.get("facebook").equals("") ? "Y" : "N";						// 페이스북
	cfg_file_check += xmlExt.equals(".xml") ? "X" : "T";			// xml extension not use so txt
	cfg_file_check += "N";													// 요즘
	cfg_file_check += "N";													// kakao : 모바일에서만 적용

	f = new File(pathOfDir+"/pagesound.txt");
	cfg_file_check += (f.exists() && f.length() > 0) ? "Y" : "N";
	f = new File(pathOfDir+"/coords"+xmlExt);
	cfg_file_check += (ini1var.charAt(9) == 'A' && f.exists() && f.length() > 0) ? "Y" : "N";

	param.append("&filecheck="+cfg_file_check+"&ext=jsp&catimage="+catimage+"&wserver="+webServer+"&apprvd=");

	String etcopt = (String)Ecatalog.get("etcopt");
	if(etcopt == null) etcopt = "";
	if(!etcopt.equals("")){
		if(!etcopt.substring(0,1).equals("&")) param.append("&"+etcopt);
		else param.append(etcopt);
	}

	// for SNS Link
	// jump start in euc-kr
	String twitter = (String)Ecatalog.get("twitter");
	String facebook = (String)Ecatalog.get("facebook");
	if(charset.toLowerCase().equals("euc-kr")){
		if(!twitter.equals("")) twitter = new String(twitter.getBytes("utf-8"));
		if(!facebook.equals("")) facebook = new String(facebook.getBytes("utf-8"));
	}
	// jump end in euc-kr

	String domainUrl = (request.getServerPort() == 80) ? request.getServerName() : request.getServerName() + ":" + request.getServerPort();
	String selfUrl = "http://" + domainUrl + request.getServletPath();

	String titlebar = (String)Ecatalog.get("titlebar");
	if(titlebar.equals("")) titlebar = "Electronic Catalog(전자 카탈로그)";
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="og:description" content="<%=facebook%>">
<title><%=titlebar%> </title>
<style>
	html, body{width:100%;height:100%;margin:0;overflow:hidden;}
	#divflash, #divflash > object{width:100%;height:100%;}
</style>
<script src="<%=webServer%>/common.js"></script>
<script src="<%=webServer%>/detectFlashVer.js"></script>
<script>
function sns_linkopen(s){
	if(s == "twitter"){
		var s = "<%=URLEncoder.encode(twitter,"utf-8")%>%20<%=URLEncoder.encode(selfUrl+"?"+queryStrLink)%>";
		window.open("http://twitter.com/home?status=" + s + "&query=" + s);
	}
	else if(s == "facebook"){
		window.open("http://www.facebook.com/sharer.php?u=<%=URLEncoder.encode(selfUrl+"?"+queryStrLink)%>&t=<%=URLEncoder.encode(facebook,"utf-8")%>");
	}
}
function showPageNo(s){		// from flash(nude)
	parent.document.showform.pageno.value = s;
}
function onload_func(){
	// Flash Version start
	// Major version  of Flash required
	var requiredMajorVersion = 10;
	// Minor version of Flash required
	var requiredMinorVersion = 0;
	// Minor version of Flash required
	var requiredRevision = 0;

<% if(cdmake == false){ %>
	var hasReqestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);
	if(!hasReqestedVersion){
		window.location = "<%=webServer%>/access/ecatalogt.jsp?<%=queryStrDir%>&acmode=fver";
		return;
	}
	// Flash Version end
<% } %>

	focusToFlash();
}
function onload_html(){
	var aw = document.body.clientWidth;
	var ah = document.body.clientHeight;
<% if(cdmake == false){ %>
	var reptext = "<p>브라우저가 플래시를 지원하지 않습니다.</p>";
<% } else{ %>
	var reptext = "<p>브라우저가 플래시를 지원하지 않거나 플래시 화면이 보이지 않으면<br>'이미지로 카탈로그 보기' 링크를 클릭하여 주십시오.<br><br>";
	reptext += "<a href=\"<%=webServer%>/access/ecatalogt.jsp?<%=queryStrDir%>&acmode=fnot\" target=\"_blank\" title=\"새창열림\">이미지로 카탈로그 보기</a></p>";
<% } %>
	document.getElementById("divflash").innerHTML = get_divHtml(aw, ah, '<%=param%>', reptext, '');
	focusToFlash();
}
function focusToFlash(){
	<% if(ini1var.charAt(19) == 'N'){ %>
		document.getElementById("cataSwf").focus();
	<% } %>
}
function get_clientwidth(){
	return document.body.clientWidth;
}
function get_clientheight(){
	return document.body.clientHeight;
}
</script>
</head>

<body onload="onload_func();">
<% if(cdmake == false){ %>
<div id="disable" style="position:absolute;left:0;top:0;width:1px;height:1px;font-size:0;line-height:0;overflow:hidden;">
<p><a href="<%=webServer%>/access/ecatalogt.jsp?<%=queryStrDir%>&acmode=disable" title="스크린 리더 사용자를 위한 HTML 기반의 대체 페이지 입니다." tabindex="0">
<%=Ecatalog.get("titlebar")%> 플래시 대체 페이지 바로가기</a></p></div>
<% } %>
<div id="divflash">
<object id="cataSwf" type="application/x-shockwave-flash" data="<%=webServer%>/main.swf">
	<param name="movie" value="<%=webServer%>/main.swf">
	<param name="flashVars" value="<%=param%>">
	<param name="quality" value="high">
	<param name="bgcolor" value="#FFFFFF">
	<param name="wmode" value="window">
	<param name="allowScriptAccess" value="always">
	<param name="allowFullScreen" value="true">
	<p>브라우저가 플래시를 지원하지 않거나 플래시 화면이 보이지 않으면<br>'이미지로 카탈로그 보기' 링크를 클릭하여 주십시오.<br><br>
	<% if(cdmake == false){ %>
	<a href="<%=webServer%>/access/ecatalogt.jsp?<%=queryStrDir%>&acmode=fnot2" target="_blank" title="새창열림">이미지로 카탈로그 보기</a>
	<% } %></p>
</object>
</div>

</body>
</html>
