<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%
	request.setCharacterEncoding("utf-8");

	String incView = request.getParameter("incview");
	String indexType = request.getParameter("indexType");
	String combokind = request.getParameter("combokind");
	String downFile = request.getParameter("downFile");
	String downFileName = request.getParameter("downFileName");
	String downexten = request.getParameter("downexten");
	String downtext = URLDecoder.decode(request.getParameter("downtext"),"utf-8");
	String mediaFile = request.getParameter("mediaFile");
	String webServer = request.getParameter("webServer");
	String printKindmc = request.getParameter("printKindmc");
	String dirName = URLDecoder.decode(request.getParameter("dirName"),"utf-8");
	String searchFile = request.getParameter("searchFile");
	String mokchaFile = request.getParameter("mokchaFile");
	String skinMedia = request.getParameter("skinMedia");
	String skinSpacer = request.getParameter("skinSpacer");
	String Dir = request.getParameter("Dir");
	String catimage = request.getParameter("catimage");
	String webpath = webServer + "/catImage" + catimage + "/" + Dir;
%>
<% if(incView.equals("m")){ %>
<!-- mm navigation -->
<nav id="mmsect" class="mm5mD041">
<div id="mmpopbtn" class="mmpopbtn5mD041"><a href="javascript:MenuObj.mmpopClick();" title="이동메뉴"><img src="<%=webServer%>/skin5/mm041/m_morebtn.png"
  class="mm_ul_imgE007" alt="이동메뉴"></a></div>
<div id="mmturnbtn" class="mmturnbtn5mD041"><a href="javascript:MenuObj.mmturnClick();" title="펼쳐보기"><img id="mmturnbtnpix" src="<%=webServer%>/skin5/mm041/m_expandbtn.png"
  class="mm_ul_imgE006" alt="펼쳐보기"></a></div>
</nav>

<% if(indexType.equals("H") && mokchaFile.equals("true")){ %>
<!-- horizontal index -->
<div id="horidx" class="horidx5mD041">
<div id="horidx_div" class="horidx_div5mD041"></div>
<a href="javascript:MenuObj.horidxmoreClick();"><div id="idxmorebtn" class="horidx_more5mD041"><img 
 src="<%=webServer%>/skin5/mm041/m_horidxmore.png" id="idxmoreimg" width="23" height="13" alt="목차 더보기"></div></a>
</div>
<% } %>

<!-- mmpop menu -->
<nav id="mmpopsect" class="mmpop5mD041" style="visibility:hidden;">
<div id="mmpopclosebtn"><a href="javascript:MenuObj.mmpopClick();" title="닫기"><img src="<%=webServer%>/skin5/mm041/m_popclosebtn.png" width="24" height="24" alt="닫기"></a></div>
<ul class="mmpop_ul5mD041">
<% if(indexType.equals("R") && mokchaFile.equals("true")){ %>
<li id="indexbtn"><a href="javascript:indexbtnClick();MenuObj.mmpopClick();" title="목차 보기"><img src="<%=webServer%>/skin5/mm041/m_indexbtn.png" 
  width="24" height="24" alt="목차"><p>목차</p></a></li>
<% } %>
<li id="expbtn"><a href="javascript:expbtnClick();MenuObj.mmpopClick();" title="탐색기 보기"><img src="<%=webServer%>/skin5/mm041/m_expbtn.png" 
  width="24" height="24" alt="탐색기"><p>썸네일</p></a></li>
<% if(!skinSpacer.equals("000")){ %>
<li id="spacerbtn"><a href="javascript:spacerbtnClick('left');MenuObj.mmpopClick();" title="책갈피에 추가"><img src="<%=webServer%>/skin5/mm041/m_spacerbtn.png" 
  width="24" height="24" alt="책갈피"><p>책갈피</p></a></li>
<% } %>
<% if(!combokind.equals("000")){ %>
<li id="combobtn"><a href="javascript:combobtnClick();MenuObj.mmpopClick();" title="다른 카탈로그 이동 페이지"><img src="<%=webServer%>/skin5/mm041/m_combobtn.png" 
  width="24" height="24" alt="다른 카탈로그"><p>다른 eBook 보기</p></a></li>
<% } %>
</ul>

<% if(searchFile.equals("true")){ %>
<div id="searchsect" class="search5mD041">
<form name="searchform" method="get" action="#" onsubmit="return MenuObj.searchClick(document.searchform.qtxt.value)" title="페이지 검색">
<input type="search" class="searchtxt5mD041" name="qtxt" title="검색어 입력">
<input type="image" class="searchbtn5mD041" src="<%=webServer%>/skin5/mm041/m_searchbtn.png" title="검색">
</form>
</div>
<% } %>

<!-- lower more sns group menu -->
<div id="snssect" class="sns_nav5mD041">
<ul class="sns_ul5mD041">
<li id="facebookbtn" class="facebookbtn5mD041"><a href="javascript:facebookbtnClick();" title="페이스북 공유"><img src="<%=webServer%>/skin5/mm041/m_facebookbtn.png" width="10" height="20" alt="페이스북"></a></li>
<li id="twitterbtn" class="twitterbtn5mD041"><a href="javascript:twitterbtnClick();" title="트위터 공유"><img src="<%=webServer%>/skin5/mm041/m_twitterbtn.png" width="28" height="20" alt="트위터"></a></li>
<li id="kakaobtn" class="kakaobtn5mD041"><a href="javascript:kakaobtnClick();" title="카카오 공유"><img src="<%=webServer%>/skin5/mm041/m_kakaobtn.png" width="21" height="20" alt="카카오"></a></li>
</ul>
<% if(downFile.equals("true")){ %>
<p id="downbtn" class="downbtn5mD041"><a href="<%=webpath%>/<%=downFileName%>" download="<%=webpath%>/<%=downFileName%>" 
  title="파일 다운로드"><img src="<%=webServer%>/skin5/mm041/m_downbtn.png" width="21" height="20" alt="파일 다운로드"></a></p>
<% } %>
</div>
</nav>

<!-- onsmimage -->
<div id="onsmlsect" class="onsmlE041">
<a href="javascript:leftOnsmClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageL041.png" width="33" height="49" alt="이전 페이지"></a>
</div>
<div id="onsmrsect" class="onsmlE041">
<a href="javascript:rightOnsmClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageR041.png" width="33" height="49" alt="다음 페이지"></a>
</div>

<script>
MenuObj = {storedStr:''};
MenuObj.onload_func2 = function(){
	if(LinkInfo.facebookStr === "" && document.getElementById("facebookbtn")) document.getElementById("facebookbtn").style.opacity = "0.2";
	if(LinkInfo.twitterStr === "" && document.getElementById("twitterbtn")) document.getElementById("twitterbtn").style.opacity = "0.2";
	if(LinkInfo.kakaoStr === "" && document.getElementById("kakaobtn")) document.getElementById("kakaobtn").style.opacity = "0.2";
	if(logoDiv) logoDiv.style.left = Math.floor(stageWidth/2-logoDiv.clientWidth/2) + "px";
	if(MenuInfo.idxmorebtn === false){
		document.getElementById("horidx_div").style.width = "100%";
		document.getElementById("idxmorebtn").style.display = "none";
	}
}
MenuObj.show_pageNumber = function(){
	show_currIndex();
}
MenuObj.onresize_func = function(){
}
MenuObj.mmpopClick = function(){
	if(mmpopDiv.style.visibility == "hidden"){
		mmpopDiv.style.visibility = "visible";
	}
	else{
		mmpopDiv.style.visibility = "hidden";
	}
}
MenuObj.horidxmoreClick = function(){
	if(indexDiv.style.visibility == "visible"){
		document.getElementById('idxmoreimg').src = "<%=webServer%>/skin5/mm041/m_horidxmore.png";
		indexDiv.innerHTML = "";
		indexDiv.style.visibility = "hidden";
	}
	else{
		document.getElementById('idxmoreimg').src = "<%=webServer%>/skin5/mm041/m_horidxmore_over.png";
		indexDiv.style.top = cataRect.y+"px";
		indexDiv.innerHTML = document.getElementById("horidx_div").innerHTML;
		indexDiv.style.visibility = "visible";
	}
}
MenuObj.mmturnClick = function(){
	if(Action.mainState === "expand"){
		document.getElementById('mmturnbtnpix').src = "<%=webServer%>/skin5/mm041/m_expandbtn.png";
		unload_expand();
	}
	else{
		document.getElementById('mmturnbtnpix').src = "<%=webServer%>/skin5/mm041/m_turnbtn.png";
		load_expand();
	}
}
MenuObj.searchClick = function(s){
	if(s === "") return false;
	if(PermitMan.normalPerm(-1) === false) return false;
	if(searchSvg) return false;

	MenuObj.mmpopClick();
	SearchInfo.set_qtxt(s);
	load_search();
	return false;
}

mmpopDiv = document.getElementById('mmpopsect');
snsDiv = document.getElementById("snssect");
if(document.getElementById("searchsect")) searchDiv = document.getElementById("searchsect");

onsmlDiv = document.getElementById("onsmlsect");
onsmrDiv = document.getElementById("onsmrsect");
OnsmObj = {};
OnsmObj.set_location = function(){
	onsmlDiv.style.left = winRect.x + "px";
	onsmlDiv.style.top = (smRect.centerPt.y-25) + "px";
	onsmrDiv.style.left = (winRect.right-33) + "px";
	onsmrDiv.style.top = (smRect.centerPt.y-25) + "px";
}
OnsmObj.set_visible = function(b1, b2){
	onsmlDiv.style.visibility = b1;
	onsmrDiv.style.visibility = b2;
}
</script>
<% } %>
