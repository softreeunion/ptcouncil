<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%
	String tw = get_param1(request.getParameter("w"),5);
	int w = tw.equals("") ? 0 : Integer.parseInt(tw);
	String th = get_param1(request.getParameter("h"),5);
	int h = th.equals("") ? 0 : Integer.parseInt(th);
	String ds = request.getParameter("d");
	if(ds == null) ds = "";

	String[] ar = mysplit(ds,".");
	String n = "";
	if(ar.length > 1) n = get_param2(ar[1],5);			// SkinInfo.nwclass(n)
	String v = "";
	if(ar.length > 2) v = check_um(ar[2],"um");			// CataInfo.incview(v)
	String d = check_mainmenu(ar[0], v);				// SkinInfo.mainmenu(d)
	String r = "alert";

	pathOfOriCata = webSysDir + "/catImage";
	HashMap Display = new HashMap();
	Display.put("user", get_userSkin(d, v));
	if(!Display.get("user").equals("")) set_userSkin((String)Display.get("user"), Display);
	set_newintitle(r, d, webSysDir+"/skin5", Display);

	String newin_titletext = "알 림";
	String svgObj = "alertSvg";
	String plusID = "Alert";
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>ecatalog - alert001</title>
<link rel="stylesheet" type="text/css" href="<%=webServer%>/main.css">
<style>
	html, body{width:100%;height:100%;margin:0;overflow:hidden;}
	*{font-family:굴림;font-size:12px;}
<% if(n.equals("016")){ %>
	body{background-color:none;}
<% } else{ %>
	body{background-color:#f7f7f7;}
<% } %>
	#newin_bodybtn{display:block;z-index:4;position:absolute;cursor:pointer;}
	.newin_bodybtn001{left:123px;top:110px;}
	.newin_bodyclosebtn012{left:163px;top:90px;}
	.newin_bodysendbtn012{left:163px;top:90px;}
	.newin_bodybtn016{left:133px;top:90px;}
	.newin_bodybtn018{left:133px;top:90px;}
	.newin_bodybtn041{left:123px;top:90px;}

	.alert_div_ani{z-index:2;display:block;position:absolute;left:25px;top:35px;width:60px;height:51px;background:url('<%=webServer%>/skin5/etc/alert_back.png');}
	#alert_div_text{z-index:3;display:block;position:absolute;left:95px;top:45px;width:175px;height:50px;overflow:visible;text-align:center;}
</style>
<script>
var selfName = "alert001.jsp", winCnt = "alert";
var svgWidth = <%=w%>, svgHeight = <%=h%>;
var p;
var ws = "";

// for exclamation mark's up/down animation
var cntx, aniTimer, exclamImg1, exclamImg2;
var loaded, loadto, SEQ;
var dragGap = 0;

function onload_func(){
	p = window.parent;
	console.log(selfName+" : loaded");
	p.alertSvg = this;
	window.parent.objLoaded2('alert', this, false);
	dragGap = p.WinInfo.get_dragGap('<%=n%>');

	p.document.getElementById('newin_frame<%=plusID%>').innerHTML = ws;

	document.getElementById("alert_div_text").innerHTML = p.DebugMan.strTitle;
	cntx = document.getElementById("alert_div_cnv").getContext("2d");

	loaded = 0;
	loadto = 2;

	exclamImg1 = new Image();
	exclamImg1.src = '<%=webServer%>/skin5/etc/alert_exclam1.png';
	exclamImg1.onload = exclamImgLoaded;

	exclamImg2 = new Image();
	exclamImg2.src = '<%=webServer%>/skin5/etc/alert_exclam2.png';
	exclamImg2.onload = exclamImgLoaded;

	if(p.SoundInfo.sndplayInAlert == true){
		var audioPlayer = document.getElementById("alert_autoctrl");
		audioPlayer.setAttribute("src", p.FileInfo.skin5FilePath("sound/chord.mp3"));
		audioPlayer.load();
		audioPlayer.play();
	}
}

function exclamImgLoaded(){
	loaded++;
	if(loaded != loadto) return;
	SEQ = 0;
	aniTimer = setInterval(exclam_animate, 30);
}
function exclam_animate(){
	cntx.clearRect(0, 0, 60, 51);

	var seq = SEQ%41;
	if(seq <= 15){
		cntx.drawImage(exclamImg1, 24, 9, 10, 22-seq);
		cntx.drawImage(exclamImg2, 26, 37, 8, 8);
	}
	else if(seq > 15 && seq <= 25){
		cntx.drawImage(exclamImg1, 24, 9, 10, 7);
		seq = seq - 15;
		cntx.drawImage(exclamImg2, 26, 37-seq, 8, 8);
	}
	else if(seq > 25){
		seq = 40 - seq;
		cntx.drawImage(exclamImg1, 24, 9, 10, 22-seq);
		if(seq > 5){
			seq = seq - 5;
			cntx.drawImage(exclamImg2, 26, 37-seq, 8, 8);
		}
		else{
			cntx.drawImage(exclamImg2, 26, 37, 8, 8);
		}
	}

	SEQ++;
}


/// ############################## window ############################## ///
function newin_close(){
	p.unload_alert();
}
function do_mouseDrag(e){
	e.preventDefault();

	var posX = e.clientX - p.clickX + p.alertbefX;
	var posY = e.clientY - p.clickY + p.alertbefY;

	if(posX < p.cataRect.x) posX = p.cataRect.x;
	else if(posX > p.cataRect.right-svgWidth-dragGap) posX = p.cataRect.right - svgWidth - dragGap;

	if(posY < p.cataRect.y) posY = p.cataRect.y;
	else if(posY > p.cataRect.bottom-svgHeight-dragGap) posY = p.cataRect.bottom - svgHeight - dragGap;

	p.alertDiv.style.transform = "translate("+posX+"px,"+posY+"px)";

	return true;
}
function do_mouseUp(e){
	p.alertbefX += e.clientX - p.clickX;
	p.alertbefY += e.clientY - p.clickY;
}
</script>
</head>

<body onload="onload_func();">

<%
	int fullscrbtn = 0;
	int closebtn = 1;
	String skind = "close";
%>
<%@ include file="inc_newin.jsp" %>

<div class="alert_div_ani"><canvas id="alert_div_cnv" width="60" height="51"></canvas></div>
<div id="alert_div_text"></div>

<div class="audio_ctrlr_hidden">
	<audio id="alert_autoctrl">브라우저가 audio를 지원하지 않습니다.</audio>
</div>

</body>
</html>
