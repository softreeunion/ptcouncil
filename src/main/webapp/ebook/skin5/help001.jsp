<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%
	String tw = get_param1(request.getParameter("w"),5);
	int w = tw.equals("") ? 0 : Integer.parseInt(tw);
	String th = get_param1(request.getParameter("h"),5);
	int h = th.equals("") ? 0 : Integer.parseInt(th);
	String ds = request.getParameter("d");
	if(ds == null) ds = "";

	String[] ar = mysplit(ds,".");
	String n = "";
	if(ar.length > 1) n = get_param2(ar[1],5);			// SkinInfo.nwclass(n)
	String v = "";
	if(ar.length > 2) v = check_um(ar[2],"um");			// CataInfo.incview(v)
	String p = "";
	if(ar.length > 3) p = get_param2(ar[3],5);			// SkinInfo.helpmc(p). 001(기본)/002(간편)
	String d = check_mainmenu(ar[0], v);				// SkinInfo.mainmenu(d)
	String r = "help";

	pathOfOriCata = webSysDir + "/catImage";
	HashMap Display = new HashMap();
	Display.put("user", get_userSkin(d, v));
	set_userSkinDef(Display);
	if(!Display.get("user").equals("")) set_userSkin((String)Display.get("user"), Display);
	set_newintitle(r, d, webSysDir+"/skin5", Display);

	String n2 = (n.equals("016") || n.equals("018")) ? n : "001";		// for grouping css grouping
	String newin_titletext = "도움말";
	String svgObj = "helpSvg";
	String plusID = "";

	int fullscrbtn = 0;
	int closebtn = 1;
	String skind = v.equals("m") ? "" : "close";

	String helpImage = "";
	if(!Display.get("help").equals("")){
		String aaa[] = mysplit((String)Display.get("help"), ",");
		helpImage = aaa[0];
		if(aaa[3].equals("false")) skind = "";
	}
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>ecatalog - help001</title>
<link rel="stylesheet" type="text/css" href="<%=webServer%>/main.css">
<style>
	html{margin:0;width:100%;height:100%;overflow:hidden;}
	*{font-family:굴림;font-size:12px;}

<% if(v.equals("d")){ %>
<% if(p.equals("001")){ %>
	body{margin:0;width:100%;height:100%;overflow:hidden;background-color:#ffffff;}
<% if(n.equals("018")){ %>
	#newin_bodybtn{display:block;z-index:4;position:absolute;right:10px;top:376px;cursor:pointer;margin:0;}
<% } else{ %>
	#newin_bodybtn{display:block;z-index:4;position:absolute;right:5px;top:376px;cursor:pointer;margin:0;}
<% } %>
	.help_main001{z-index:2;display:block;position:absolute;left:0px;top:16px;width:495px;height:430px;}
	.help_main016{z-index:2;display:block;position:absolute;left:5px;top:16px;width:495px;height:430px;}
	.help_main018{z-index:2;display:block;position:absolute;left:7px;top:16px;width:495px;height:430px;}

<% } else{ %>
	body{margin:0;width:100%;height:100%;overflow:hidden;background-color:#ffffff;}
	#newin_bodybtn{display:block;z-index:4;position:absolute;right:53px;top:374px;cursor:pointer;}
	.help_main001{z-index:2;display:block;position:absolute;left:3px;top:16px;width:495px;height:430px;}
	.help_main016{z-index:2;display:block;position:absolute;left:5px;top:16px;width:495px;height:430px;}
	.help_main018{z-index:2;display:block;position:absolute;left:7px;top:16px;width:495px;height:430px;}

<% } %>
	.help_g_menu > rect{fill:#FFFFFF;stroke:#989898;stroke-opacity:1;shape-rendering:crispEdges;cursor:pointer;}
	.help_g_menu:hover > rect{fill:#D7FDFF;stroke:#444444;stroke-opacity:1;shape-rendering:crispEdges;}
	.help_rect{fill:#FFFFFF;stroke:#989898;stroke-opacity:1;shape-rendering:crispEdges;}
	.mask1{fill:#FFFFFF;}

<% } else{ %>
<% if(p.equals("001")){ %>
	body{margin:0;width:100%;height:100%;overflow:hidden;background-color:#ffffff;}
	.help_div_main{display:block;position:absolute;left:0;top:16px;width:100%;height:289px;}
	.help_div_main > img{display:block;width:215px;height:289px;margin:0 auto;}

	#help_paging{z-index:4;display:block;position:absolute;left:calc(50% - 20px);top:304px;width:40px;height:12px;height:12px;margin:0 auto;}
	#help_paging img{display:block;float:left;width:8px;height:8px;margin:2px 0 0 5px;}
<% } else{ %>
	body{margin:0;width:100%;height:100%;overflow:hidden;background-color:#000000;opacity:0.8;}
	#help_div_main{display:block;position:relative;margin:0 auto;}
<% } %>
<% } %>
</style>
<% if(v.equals("m")){ %>
<script src="touches.js"></script>
<% } %>
<script>
var selfName = "help001.jsp", winCnt = "help";
var svgWidth = <%=w%>, svgHeight = <%=h%>;
var p;
var ws = "";

var downKind = 0;		// for touches.js
var incview = "<%=v%>";
var dragGap = 0;

function onload_func(){
	p = window.parent;
	console.log(selfName+" : loaded");
	p.helpSvg = this;
	window.parent.objLoaded2('help', this, false);
	dragGap = p.WinInfo.get_dragGap('<%=n%>');

	p.document.getElementById('newin_frame').innerHTML = ws;

<% if(v.equals("m")){ %>
<% if(p.equals("001")){ %>
	touchMoveAry.push(document.getElementById("help_move"));
	touchMoveAry.push(document.getElementById("help_enlarge"));
	touchMoveAry.push(document.getElementById("help_print"));

	touchPagingObj = document.getElementById('help_paging');
	touchPagingPix = "<%=webServer%>/skin5/mm001/m_syncircle.png";
	touchPagingCurPix = "<%=webServer%>/skin5/mm001/m_syncircle-s.png";

	init_touchDrag(0, svgWidth, 0);			// startX, touchMoveWidth, touchidx
<% } else{ %>
	var tw = 113;
	var th = 141;
	var s = "";
	if(p.MoveInfo.spDrag === true){
		s += "<img src='<%=webServer%>/skin5/help/m2_draglr.png' class='helpimg5mE002'>";
		th += 141;
	}
	s += (p.MoveInfo.bigImageOnly === true) ? "<img src='<%=webServer%>/skin5/help/m2_pinchen.png' class='helpimg5mE002'>" : "<img src='<%=webServer%>/skin5/help/m2_clicken.png' class='helpimg5mE002'>";
	document.getElementById('help_div_main').innerHTML = s;
	document.getElementById('help_div_main').style.width = tw + "px";
	document.getElementById('help_div_main').style.top = ((svgHeight-th)/2-10) + "px";
<% } %>
<% } %>
}

function help_menuClick(s){
	if(s == "move") nx = 155;
	else if(s == "enlarge") nx = 240;
	else if(s == "explorer") nx = 325;
	else if(s == "print") nx = 410;

	document.getElementById('help_image').setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", "<%=webServer%>/skin5/help/help001_"+s+".png");
	document.getElementById('help_line_mask').setAttribute("x", nx);
}

/// ############################## window ############################## ///
function newin_close(){
	p.unload_newin(winCnt);
	p.helpSvg = undefined;
}
function do_mouseDrag(e){
	e.preventDefault();

	var posX = e.clientX - p.clickX + p.befX;
	var posY = e.clientY - p.clickY + p.befY;

	if(posX < p.cataRect.x) posX = p.cataRect.x;
	else if(posX > p.cataRect.right-svgWidth-dragGap) posX = p.cataRect.right - svgWidth - dragGap;

	if(posY < p.cataRect.y) posY = p.cataRect.y;
	else if(posY > p.cataRect.bottom-svgHeight-dragGap) posY = p.cataRect.bottom - svgHeight - dragGap;

	p.newinDiv.style.left = posX + "px";
	p.newinDiv.style.top = posY + "px";

	return true;
}
function do_mouseUp(e){
	p.befX += e.clientX - p.clickX;
	p.befY += e.clientY - p.clickY;
}
</script>
</head>

<body onload="onload_func()">

<%@ include file="inc_newin.jsp" %>

<% if(v.equals("d")){ %>
<% if(p.equals("001")){ %>
<!-- help main svg -->
<svg version="1.1" class="help_main<%=n2%>" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g transform="translate(154.5,0.5)" class="help_g_menu" onclick="help_menuClick('move')">
	<title>페이지 이동</title>
	<rect x="0" y="0" width="85" height="26" rx="2" />
	<image x="12" y="5" width="52" height="13" xlink:href="<%=webServer%>/skin5/help/menu1.png" />
</g>

<g transform="translate(239.5,0.5)" class="help_g_menu" onclick="help_menuClick('enlarge')">
	<title>확대/축소</title>
	<rect x="0" y="0" width="85" height="25" rx="2" />
	<image x="12" y="5" width="52" height="13" xlink:href="<%=webServer%>/skin5/help/menu2.png" />
</g>

<g transform="translate(324.5,0.5)" class="help_g_menu" onclick="help_menuClick('explorer')">
	<title>탐색/링크</title>
	<rect x="0" y="0" width="85" height="25" rx="2" />
	<image x="12" y="5" width="52" height="13" xlink:href="<%=webServer%>/skin5/help/menu3.png" />
</g>

<g transform="translate(409.5,0.5)" class="help_g_menu" onclick="help_menuClick('print')">
	<title>인쇄/메일</title>
	<rect x="0" y="0" width="85" height="25" rx="2" />
	<image x="12" y="5" width="52" height="13" xlink:href="<%=webServer%>/skin5/help/menu4.png" />
</g>

<g transform="translate(5,24)">
	<rect class="help_rect" x="0" y="0" width="490" height="327" />
	<image id="help_image" x="1" y="1" width="486" height="322" xlink:href="<%=webServer%>/skin5/help/help001_move.png" />
</g>

<rect id="help_line_mask" class="mask1" x="155" y="23" width="84" height="2" />
</svg>

<% } else{ %>
<% if(!helpImage.equals("")){ %>
<div class="help_main<%=n2%>"><img src="<%=webServer%>/<%=helpImage%>" alt=""></div>
<% } else{ %>
<div class="help_main<%=n2%>"><img src="<%=webServer%>/skin5/help/help0025_content.png" width="684" height="369" alt=""></div>
<% } %>
<% } %>

<% } else{ //(v.equals("m")) %>
<% if(p.equals("001")){ %>
<div id="help_move" class="help_div_main" style="visibility:visible;"><img src="<%=webServer%>/skin5/help/m_help001_move.png" alt="페이지 이동"></div>
<div id="help_enlarge" class="help_div_main" style="visibility:hidden;"><img src="<%=webServer%>/skin5/help/m_help001_enlarge.png" alt="확대/축소"></div>
<div id="help_print" class="help_div_main" style="visibility:hidden;"><img src="<%=webServer%>/skin5/help/m_help001_print.png" alt="인쇄/메일"></div>

<!-- sync circle -->
<div id="help_paging">
<a href="javascript:paging_itemClick(0);"><img src='<%=webServer%>/skin5/mm001/m_syncircle.png' alt="이동"></a>
<a href="javascript:paging_itemClick(1);"><img src='<%=webServer%>/skin5/mm001/m_syncircle.png' alt="이동"></a>
<a href="javascript:paging_itemClick(2);"><img src='<%=webServer%>/skin5/mm001/m_syncircle.png' alt="이동"></a>
</div>
<% } else{ %>
<div id="help_div_main"></div>
<% } %>
<% } %>

</body>
</html>
