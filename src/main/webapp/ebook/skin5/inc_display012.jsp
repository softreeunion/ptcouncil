<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%
	request.setCharacterEncoding("utf-8");

	String incView = request.getParameter("incview");
	String indexType = request.getParameter("indexType");
	String combokind = request.getParameter("combokind");
	String downFile = request.getParameter("downFile");
	String downFileName = request.getParameter("downFileName");
	String downexten = request.getParameter("downexten");
	String downtext = URLDecoder.decode(request.getParameter("downtext"),"utf-8");
	String mediaFile = request.getParameter("mediaFile");
	String mokchaFile = request.getParameter("mokchaFile");
	String webServer = request.getParameter("webServer");
	String printKindmc = request.getParameter("printKindmc");
	String dirName = URLDecoder.decode(request.getParameter("dirName"),"utf-8");
	String searchFile = request.getParameter("searchFile");
	String skinMedia = request.getParameter("skinMedia");
	String skinSpacer = request.getParameter("skinSpacer");
	String Dir = request.getParameter("Dir");
	String catimage = request.getParameter("catimage");
	String webpath = webServer + "/catImage" + catimage + "/" + Dir;
%>
<% if(incView.equals("d")){ %>

<div class="mm_div5dD012">
<div id="pageshow" class="pshow5dD012">
<form name="pageshowform" method="get" action="ecatalog.jsp" onsubmit="return MenuObj.do_pagenoFormSubmit();" title="페이지 이동">
<p class="pshow_p5dD012"><input type="text" name="pageno" value="" onfocus="MenuObj.do_pagenoFocusIn(event);" onblur="MenuObj.do_pagenoFocusOut(event)"
  pattern="[0-9-]+" title="숫자만 입력 가능합니다." required></p>
<p id="pshow_totalpage"></p>
</form>
</div>

<ul class="mm_ulE002">
<li id="indexbtn" class="indexbtn5dD012"><a href="javascript:indexbtnClick();" title="목차 보기"><img src="<%=webServer%>/skin5/mm012/indexbtn.png" width="23" height="39" alt="목차 보기"
  onmouseover="this.src='<%=webServer%>/skin5/mm012/indexbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm012/indexbtn.png'"></a></li>
<li id="expbtn" class="expbtn5dD012"><a href="javascript:expbtnClick();"title="탐색기 보기"><img src="<%=webServer%>/skin5/mm012/expbtn.png" width="34" height="39" alt="탐색기 보기"
  onmouseover="this.src='<%=webServer%>/skin5/mm012/expbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm012/expbtn.png'"></a></li>
<li id="glassesbtn" class="glassesbtn5dD012"><a href="javascript:glassesbtnClick();" title="돋보기로 보기"><img src="<%=webServer%>/skin5/mm012/glassesbtn.png" width="32" height="39" alt="돋보기로 보기"
  onmouseover="this.src='<%=webServer%>/skin5/mm012/glassesbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm012/glassesbtn.png'"></a></li>
<li id="printbtn" class="printbtn5dD012"><a href="javascript:printbtnClick();" title="인쇄하기"><img src="<%=webServer%>/skin5/mm012/printbtn.png" width="33" height="39" alt="인쇄하기"
  onmouseover="this.src='<%=webServer%>/skin5/mm012/printbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm012/printbtn.png'"></a></li>
<li id="slidebtn" class="slidebtn5dD012"><a href="javascript:slidebtnClick();" title="자동 넘김"><img src="<%=webServer%>/skin5/mm012/slidebtn.png" width="41" height="39" alt="자동 넘김"
  onmouseover="this.src='<%=webServer%>/skin5/mm012/slidebtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm012/slidebtn.png'"></a></li>
<li id="helpbtn" class="helpbtn5dD012"><a href="javascript:helpbtnClick();" title="도움말 페이지 보기"><img src="<%=webServer%>/skin5/mm012/helpbtn.png" width="32" height="39" alt="도움말 페이지 보기"
  onmouseover="this.src='<%=webServer%>/skin5/mm012/helpbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm012/helpbtn.png'"></a></li>
</ul>
</div>

<ul class="mm_ulE003">
<li id="fullscrbtn" class="fullscrbtn5dD012"><a href="javascript:fullscrbtnClick();" title="전체화면으로 보기"><img src="<%=webServer%>/skin5/mm012/fullscrbtn.png" width="22" height="22" alt="전체화면으로 보기"
  onmouseover="this.src='<%=webServer%>/skin5/mm012/fullscrbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm012/fullscrbtn.png'"></a></li>
</ul>

<% if(searchFile.equals("true")){ %>
<div id="searchsect" class="search5dD012">
<form name="searchform" method="get" action="#" onsubmit="return do_searchbtnClick(document.searchform.qtxt.value)" title="페이지 검색">
<input type="search" class="searchtxt5dD012" name="qtxt" title="검색어 입력" onfocus="do_qtxtFocusIn(this)">
<input type="image" class="searchbtn5dD012 hover_opac8" src="<%=webServer%>/skin5/mm012/search_btn.png" alt="검색 시작">
</form>
</div>
<% } %>
</nav>

<div id="lowersect" class="lower5dD012">

<!-- download -->
<% if(downFile.equals("true")){ %>
<div id="downsect" class="down5dD012">
<a href="<%=webpath%>/<%=downFileName%>" download="<%=webpath%>/<%=downFileName%>"
title="파일 다운로드"><img src="<%=webServer%>/skin5/icon/file_<%=downexten%>.gif" class="down_img" alt="파일 다운로드"><%=downtext%></a>
</div>
<% } %>

<!-- combobox -->
<% if(!combokind.equals("000")){ %>
<a href="javascript:comboClick();"><div id="combosect" class="cbbox5dD012" title="다른 카탈로그로 이동" 
data-width="display" data-align="right" data-direction="top" data-vpos="-3"><img src="<%=webServer%>/skin5/mm012/combo_txt.png" width="111" height="14" 
alt="다른 카탈로그 이동" onmouseover="this.src='<%=webServer%>/skin5/mm012/combo_txt_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm012/combo_txt.png'"></div></a>
<% } %>

<!-- thumb preview -->
<div id="thumbsect" class="thumb5dD012"></div>

</div>

<script>
MenuObj = {storedStr:'', thumbX:20, thumbMargin:40, thobjWidth:0, thobjHeight:0};

thumbDiv = document.getElementById("thumbsect");
if(document.getElementById("downsect")){
	downDiv = document.getElementById("downsect");
	MenuObj.thumbX += 136;		// 116 + 20
	MenuObj.thumbMargin += 136;
}
if(document.getElementById("combosect")){
	comboDiv = document.getElementById("combosect");
	MenuObj.thumbMargin += 156;
}
if(document.getElementById("searchsect")) searchDiv = document.getElementById("searchsect");
if(document.getElementById("snssect")) snsDiv = document.getElementById("snssect");

MenuObj.onload_func2 = function(){
	var n = (PageInfo.showStartPage == 0) ? PageInfo.cataPages : PageInfo.get_showPageFromReal(PageInfo.cataPages);
	document.getElementById("pshow_totalpage").innerHTML = n;

	if(FileInfo.idxFile === false && document.getElementById("indexbtn")) document.getElementById("indexbtn").style.opacity = "0.3";
	if(SkinInfo.printkind === "000" && document.getElementById("printbtn")) document.getElementById("printbtn").style.opacity = "0.3";

	var tw = stageWidth - MenuObj.thumbMargin;
	var th = 70;
	MenuObj.thobjWidth = tw;
	MenuObj.thobjHeight = th;
	thumbDiv.style.left = MenuObj.thumbX + "px";
	thumbDiv.style.width = tw + "px";
	thumbDiv.innerHTML = (FileInfo.embedTag === "iframe") ? "<iframe src='<%=webServer%>/skin5/thumbpage001.html'></iframe>\n"
		: "<object data='<%=webServer%>/skin5/thumbpage001.html' type='text/html'></object>\n";
}
MenuObj.onresize_func = function(){
	var tw = stageWidth - MenuObj.thumbMargin;
	MenuObj.thobjWidth = tw;
	thumbDiv.style.width = tw + "px";
	thumbSvg.onresize_func(tw);
}
MenuObj.show_pageNumber = function(){
}
</script>

<% } else if(incView.equals("m")){ %>
<!-- mm navigation -->
<nav id="mmsect" class="mm5mD012">
<div id="mm_div" class="mm_divE004">
<ul id="mm_ul" class="mm_ulE004">
<% if(indexType.equals("R")){ %>
<li id="indexbtn" class="indexbtn5mD012"><a href="javascript:indexbtnClick();" title="목차 보기"><img src="<%=webServer%>/skin5/mm012/m_indexbtn.png"
  width="17" height="31" alt="목차"></a></li>
<% } %>
<li id="expbtn" class="expbtn5mD012"><a href="javascript:expbtnClick();" title="탐색기 보기"><img src="<%=webServer%>/skin5/mm012/m_expbtn.png" 
  width="20" height="31" alt="탐색기"></a></li>
<li id="printbtn" class="printbtn5mD012"><a href="javascript:printbtnClick();" title="인쇄하기"><img src="<%=webServer%>/skin5/mm012/m_printbtn.png" 
  width="22" height="31" alt="인쇄"></a></li>
<li id="slidebtn" class="slidebtn5mD012"><a href="javascript:slidebtnClick();" title="자동넘김"><img src="<%=webServer%>/skin5/mm012/m_slidebtn.png" 
  width="31" height="31" alt="자동넘김"></a></li>
<li id="helpbtn" class="helpbtn5mD012"><a href="javascript:helpbtnClick();" title="도움말 보기"><img src="<%=webServer%>/skin5/mm012/m_helpbtn.png" 
  width="24" height="31" alt="도움말"></a></li>
<% if(!skinSpacer.equals("000")){ %>
<li id="spacerbtn" class="spacerbtn5mD012"><a href="javascript:spacerbtnClick('left');" title="북마크"><img src="<%=webServer%>/skin5/mm012/m_spacerbtn.png" 
  width="23" height="31" alt="책갈피"></a></li>
<% } %>
<li id="snsbtn" class="snsbtn5mD012"><a href="javascript:MenuObj.mmgroupClick('sns');" title="sns공유"><img src="<%=webServer%>/skin5/mm012/m_snsbtn.png" 
  width="18" height="31" alt="sns공유"></a></li>
<% if(searchFile.equals("true")){ %>
<li id="searchbtn" class="searchbtn25mD012"><a href="javascript:MenuObj.mmgroupClick('search');" title="검색메뉴"><img src="<%=webServer%>/skin5/mm012/m_mmsearchbtn.png"
  width="19" height="31" alt="검색메뉴"></a></li>
<% } %>
<% if(downFile.equals("true")){ %>
<li id="downbtn" class="downbtn5mD012"><a href="<%=webpath%>/<%=downFileName%>" download="<%=webpath%>/<%=downFileName%>" title="파일 다운로드"><img
  src="<%=webServer%>/skin5/mm012/m_downbtn.png" width="31" height="31" alt="파일 다운로드"></a></li>
<% } %>
<% if(mediaFile.equals("true") && !skinMedia.equals("000")){ %>
<li id="mediabtn" class="mediabtn5mD012"><a href="javascript:MenuObj.mmgroupClick('media');" title="음악메뉴"><img
  src="<%=webServer%>/skin5/mm012/m_mediabtn.png" width="32" height="31" alt="음악메뉴"></a></li>
<% } %>
<% if(!combokind.equals("000")){ %>
<li id="combobtn" class="combobtn5mD012"><a href="javascript:combobtnClick();" title="다른 카탈로그 이동 페이지"><img
  src="<%=webServer%>/skin5/mm012/m_combobtn.png" width="24" height="33" alt="다른 카탈로그 이동 페이지"></a></li>
<% } %>
</ul>
</div>
<div class="mmpopbtn5mD012"><a href="javascript:MenuObj.mmpopClick();" title="메뉴 더보기"><img src="<%=webServer%>/skin5/mm012/m_morebtn.png"
  width="6" height="17" alt="더보기"></a></div>
</nav>

<div id="pageshow" class="pshow5mD012" contentEditable="true" onfocus="do_pagenoFocusIn2(event);" onblur="do_pagenoFocusOut2(event);"></div>

<% if(indexType.equals("H") && mokchaFile.equals("true")){ %>
<!-- horizontal index -->
<div id="horidx" class="horidx5mD012">
<div id="horidx_div" class="horidx_div5mD012"></div>
<a href="javascript:MenuObj.horidxmoreClick();"><div id="idxmorebtn" class="horidx_more5mD012"><img 
 src="<%=webServer%>/skin5/mm012/m_morebtn.png" id="idxmoreimg" width="5" height="15" alt="목차 더보기"></div></a>
</div>
<% } %>

<% if(searchFile.equals("true")){ %>
<!-- popup search menu -->
<div id="searchsect" class="search5mD012" style="visibility:hidden;">
<form name="searchform" method="get" action="#" onsubmit="return do_searchbtnClick(document.searchform.qtxt.value)" title="페이지 검색">
<input type="search" class="searchtxt5mD012" name="qtxt" title="검색어 입력">
<input type="image" class="searchbtn5mD012" width="21" height="20" src="<%=webServer%>/skin5/mm012/m_searchbtn.png" tabindex="12" title="검색">
</form>
<div class="mmpopclosebtn"><a href="javascript:MenuObj.mmgroupClick('search');"><img src="<%=webServer%>/skin5/win/m_closebtn.png" width="15" height="15"></a></div>
</div>
<% } %>

<!-- popup sns menu -->
<div id="snssect" class="sns5mD012" style="visibility:hidden;">
<ul class="mm_ulE005">
<li id="facebookbtn" class="facebookbtn5mD012"><a href="javascript:facebookbtnClick();" title="페이스북 공유"><img src="<%=webServer%>/skin5/mm012/m_facebookbtn.png"
  width="25" height="22" alt="페이스북"></a></li>
<li id="twitterbtn" class="twitterbtn5mD012"><a href="javascript:twitterbtnClick();" title="트위터 공유"><img src="<%=webServer%>/skin5/mm012/m_twitterbtn.png"
  width="25" height="22" alt="트위터"></a></li>
<li id="kakaobtn" class="kakaobtn5mD012"><a href="javascript:kakaobtnClick();" title="카카오 공유"><img src="<%=webServer%>/skin5/mm012/m_kakaotalkbtn.png"
  width="25" height="22" alt="카카오"></li>
<li id="mailbtn" class="mailbtn5mD012"><a href="javascript:mailbtnClick();" title="전자우편 발송"><img src="<%=webServer%>/skin5/mm012/m_mailbtn.png"
  width="25" height="22" alt="전자우편"></a></li>
</ul>
<div class="mmpopclosebtn"><a href="javascript:MenuObj.mmgroupClick('sns');"><img src="<%=webServer%>/skin5/win/m_closebtn.png" width="15" height="15"></a></div>
</div>

<!-- popup media menu -->
<% if(mediaFile.equals("true") && !skinMedia.equals("000")){ %>
<div id="mediasect" class="media5mD012" style="visibility:hidden;">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<defs>
<linearGradient id="buttonGradient" gradientTransform="rotate(90)">
	<stop offset="0%" stop-color="#ffffff" />
	<stop offset="100%" stop-color="#dddddd" />
</linearGradient>
</defs>
<g>
	<rect id="mediarect1" x="0" y="0" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect2" x="6" y="0" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect3" x="12" y="0" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect4" x="0" y="3" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect5" x="6" y="3" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect6" x="12" y="3" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect7" x="0" y="6" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect8" x="6" y="6" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect9" x="12" y="6" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect10" x="0" y="9" width="4" height="2" fill="#74B800" />
	<rect id="mediarect11" x="6" y="9" width="4" height="2" fill="#74B800" />
	<rect id="mediarect12" x="12" y="9" width="4" height="2" fill="#74B800" />
	<rect id="mediarect13" x="0" y="12" width="4" height="2" fill="#748A00" />
	<rect id="mediarect14" x="6" y="12" width="4" height="2" fill="#748A00" />
	<rect id="mediarect15" x="12" y="12" width="4" height="2" fill="#748A00" />
</g>
</svg>
<ul class="mm_ulE005">
	<li id="media_backbtn" class="media_backbtn5mD012"><a href="javascript:MediaObj.backbtnClick();" title="이전 곡">
	<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13" height="13">
	<polygon points="5,7 13,13 13,0" />
	<rect x="0.5" y="0" width="4" height="13" fill="url(#buttonGradient)" />
	</svg></a>
	</li>
	<li id="media_playbtn" class="media_playbtn5mD012"><a href="javascript:MediaObj.playbtnClick();" title="재생">
	<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10" height="13">
	<polygon points="0,0 10,7 0,13" />
	</svg></a>
	</li>
	<li id="media_nextbtn" class="media_nextbtn5mD012"><a href="javascript:MediaObj.nextbtnClick();" title="다음 곡">
	<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13" height="13">
	<polygon points="0,0 8,7 0,13" />
	<rect x="8.5" y="0" width="4" height="13" fill="url(#buttonGradient)" />
	</svg></a>
	</li>
	<li id="media_stopbtn" class="media_stopbtn5mD012"><a href="javascript:MediaObj.stopbtnClick();" title="멈춤">
	<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="9" height="9">
	<polygon points="0,0 9,0 9,9 0,9" fill="url(#buttonGradient)" />
	</svg></a>
	</li>
	<li id="media_listbtn" class="media_listbtn5mD012"><a href="javascript:MediaObj.listbtnClick();" title="재생목록">
	<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13" height="13">
	<polygon points="7,0 13,8 0,8" />
	<rect x="0" y="10.5" width="13" height="4" fill="#dddddd"/>
	</svg></a>
	</li>
</ul>
<audio id="autoctrl">브라우저가 audio를 지원하지 않습니다.</audio>
<div class="mmpopclosebtn"><a href="javascript:MenuObj.mmgroupClick('media');"><img src="<%=webServer%>/skin5/win/m_closebtn.png" width="15" height="15"></a></div>
</div>
<% } %>

<div id="lowersect" class="lower5mD012">
<div id="thumbsect" class="thumb5mD012"></div>
</div>

<script>
MenuObj = {storedStr:'', thobjWidth:0, thobjHeight:0};

thumbDiv = document.getElementById("thumbsect");
if(document.getElementById("searchsect") && document.getElementById("searchsect")) searchDiv = document.getElementById("searchsect");
if(document.getElementById("snssect") && document.getElementById("snssect")) snsDiv = document.getElementById("snssect");
if(document.getElementById("mediasect") && document.getElementById("mediasect")) mediaDiv = document.getElementById("mediasect");

MenuObj.onload_func2 = function(){
	if(LinkInfo.facebookStr === "" && document.getElementById("facebookbtn")) document.getElementById("facebookbtn").style.opacity = "0.2";
	if(LinkInfo.twitterStr === "" && document.getElementById("twitterbtn")) document.getElementById("twitterbtn").style.opacity = "0.2";
	if(LinkInfo.kakaoStr === "" && document.getElementById("kakaobtn")) document.getElementById("kakaobtn").style.opacity = "0.2";
	if(MenuInfo.mailButtonHide === "Y" && document.getElementById("mailbtn")) document.getElementById("mailbtn").style.visibility = "hidden";
	if(FileInfo.idxFile === false && document.getElementById("indexbtn")) document.getElementById("indexbtn").style.opacity = "0.3";
	if(SkinInfo.printkind === "000" && document.getElementById("printbtn")) document.getElementById("printbtn").style.opacity = "0.3";
	if(MenuInfo.idxmorebtn === false){
		document.getElementById("horidx_div").style.width = "100%";
		document.getElementById("idxmorebtn").style.display = "none";
	}

	MenuObj.thobjWidth = stageWidth;
	MenuObj.thobjHeight = 41;
	thumbDiv.innerHTML = "<object data=\"<%=webServer%>/skin5/thumbpage001m.html\" type=\"text/html\"></object>\n";
}
MenuObj.onresize_func = function(){
	MenuObj.thobjWidth = stageWidth;
	thumbSvg.onresize_func(stageWidth);
}
MenuObj.show_pageNumber = function(){
	show_currIndex();
}
MenuObj.horidxmoreClick = function(){
	if(indexDiv.style.visibility == "visible"){
		indexDiv.innerHTML = "";
		indexDiv.style.visibility = "hidden";
	}
	else{
		indexDiv.style.top = cataRect.y+"px";
		indexDiv.innerHTML = document.getElementById("horidx_div").innerHTML;
		indexDiv.style.visibility = "visible";
	}
}
MenuObj.mmpopClick = function(){
	var divobj = document.getElementById("mm_div");
	var ulobj = document.getElementById("mm_ul");
	divobj.scrollLeft = (divobj.scrollLeft === 0) ? ulobj.clientWidth - divobj.clientWidth + 13 : 0;
}
MenuObj.mmgroupClick = function(s){
	if(s === "media"){
		if(mediaDiv.style.visibility == "visible"){
			mediaDiv.style.visibility = "hidden";
		}
		else{
			mediaDiv.style.visibility = "visible";
			searchDiv.style.visibility = "hidden";
			snsDiv.style.visibility = "hidden";
		}
	}
	else if(s === "sns"){
		if(snsDiv.style.visibility == "visible"){
			snsDiv.style.visibility = "hidden";
		}
		else{
			snsDiv.style.visibility = "visible";
			searchDiv.style.visibility = "hidden";
			mediaDiv.style.visibility = "hidden";
		}
	}
	else if(s === "search"){
		if(searchDiv.style.visibility == "visible"){
			searchDiv.style.visibility = "hidden";
		}
		else{
			searchDiv.style.visibility = "visible";
			mediaDiv.style.visibility = "hidden";
			snsDiv.style.visibility = "hidden";
		}
	}
}

OnsmObj = {};
OnsmObj.set_location = function(){
}
OnsmObj.set_visible = function(b1, b2){
}
</script>
<% } %>
