<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%
	request.setCharacterEncoding("utf-8");

	String incView = request.getParameter("incview");
	String indexType = request.getParameter("indexType");
	String combokind = request.getParameter("combokind");
	String downFile = request.getParameter("downFile");
	String downFileName = request.getParameter("downFileName");
	String downexten = request.getParameter("downexten");
	String downtext = URLDecoder.decode(request.getParameter("downtext"),"utf-8");
	String mediaFile = request.getParameter("mediaFile");
	String webServer = request.getParameter("webServer");
	String printKindmc = request.getParameter("printKindmc");
	String dirName = URLDecoder.decode(request.getParameter("dirName"),"utf-8");
	String searchFile = request.getParameter("searchFile");
	String mokchaFile = request.getParameter("mokchaFile");
	String skinMedia = request.getParameter("skinMedia");
	String skinSpacer = request.getParameter("skinSpacer");
	String mailButtonHide = request.getParameter("mailButtonHide");
	String Dir = request.getParameter("Dir");
	String catimage = request.getParameter("catimage");
	String webpath = webServer + "/catImage" + catimage + "/" + Dir;
%>
<% if(incView.equals("d")){ %>
<!-- navigation -->
<nav id="lowersect" class="mm5dD020-idxR">
<ul class="mm_ulE001">
<li id="closebtn"><a href="javascript:closebtnClick();" title="창닫기"><img src="<%=webServer%>/skin5/mm020/closebtn.png" width="15" height="14" alt="창닫기"
  onmouseover="this.src='<%=webServer%>/skin5/mm020/closebtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm020/closebtn.png'"></a></li>
</ul>

<ul class="mm_ulE002">
<li id="glassesbtn" class="glassesbtn5dD020"><a href="javascript:glassesbtnClick();" title="돋보기로 보기"><img src="<%=webServer%>/skin5/mm020/glassesbtn.png" 
width="24" height="23" alt="돋보기" onmouseover="this.src='<%=webServer%>/skin5/mm020/glassesbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm020/glassesbtn.png'"></a></li>
<li id="mailbtn" class="mailbtn5dD020"><a href="javascript:mailbtnClick();" title="전자우편 발송"><img src="<%=webServer%>/skin5/mm020/mailbtn.png"
width="29" height="22" alt="전자우편" onmouseover="this.src='<%=webServer%>/skin5/mm020/mailbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm020/mailbtn.png'"></a></li>
<li id="printbtn" class="printbtn5dD020"><a href="javascript:printbtnClick();" title="인쇄하기"><img src="<%=webServer%>/skin5/mm020/printbtn.png" 
width="28" height="22" alt="인쇄" onmouseover="this.src='<%=webServer%>/skin5/mm020/printbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm020/printbtn.png'"></a></li>
<li id="helpbtn" class="helpbtn5dD020"><a href="javascript:helpbtnClick();" title="도움말 보기"><img src="<%=webServer%>/skin5/mm020/helpbtn.png" 
width="25" height="26" alt="도움말" onmouseover="this.src='<%=webServer%>/skin5/mm020/helpbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm020/helpbtn.png'"></a></li>
</ul>

<ul class="mm_ulE003">
<li id="lastbtn"><a href="javascript:lastbtnClick();" title="마지막 페이지로 이동"><img src="<%=webServer%>/skin5/mm020/lastbtn.png" 
width="16" height="19" alt="마지막 페이지" onmouseover="this.src='<%=webServer%>/skin5/mm020/lastbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm020/lastbtn.png'"></a></li>
</ul>

<ul class="mm_ulE004">
<li id="prevbtn" class="prevbtn5dD020"><a href="javascript:prevbtnClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/mm020/prevbtn.png" 
width="11" height="18" alt="이전 페이지" onmouseover="this.src='<%=webServer%>/skin5/mm020/prevbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm020/prevbtn.png'"></a></li>
<li id="nextbtn" class="nextbtnn5dD020"><a href="javascript:nextbtnClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/mm020/nextbtn.png" 
width="12" height="18" alt="다음 페이지" onmouseover="this.src='<%=webServer%>/skin5/mm020/nextbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm020/nextbtn.png'"></a></li>
<li id="slidebtn" class="slidebtn5dD020"><a href="javascript:slidebtnClick();" title="자동 넘김"><img src="<%=webServer%>/skin5/mm020/slidebtn.png" 
width="36" height="9" alt="자동 넘김" onmouseover="this.src='<%=webServer%>/skin5/mm020/slidebtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm020/slidebtn.png'"></a></li>
</ul>

<ul class="mm_ulE005">
<li id="firstbtn"><a href="javascript:firstbtnClick();" title="처음 페이지로 이동"><img src="<%=webServer%>/skin5/mm020/firstbtn.png" 
width="16" height="19" alt="처음 페이지로 이동" onmouseover="this.src='<%=webServer%>/skin5/mm020/firstbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm020/firstbtn.png'"></a></li>
</ul>

<ul class="mm_ulE006">
<li id="indexbtn" class="indexbtn5dD020"><a href="javascript:indexbtnClick();" title="목차 보기"><img src="<%=webServer%>/skin5/mm020/indexbtn.png" 
width="18" height="9" alt="목차 보기" onmouseover="this.src='<%=webServer%>/skin5/mm020/indexbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm020/indexbtn.png'"></a></li>
<li id="expbtn" class="expbtn5dD020"><a href="javascript:expbtnClick();" title="탐색기 보기"><img src="<%=webServer%>/skin5/mm020/expbtn.png"
width="17" height="9" alt="탐색기 보기" onmouseover="this.src='<%=webServer%>/skin5/mm020/expbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm020/expbtn.png'"></a></li>
<li id="fullscrbtn" class="fullscrbtn5dD020"><a href="javascript:fullscrbtnClick();" title="전체화면으로 보기"><img src="<%=webServer%>/skin5/mm020/fullscrbtn.png"
width="35" height="9" alt="전체화면으로 보기" onmouseover="this.src='<%=webServer%>/skin5/mm020/fullscrbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm020/fullscrbtn.png'"></a></li>
</ul>

<div id="pageshow" class="pshow5dD020">
<form name="pageshowform" method="get" action="ecatalog.php" onsubmit="return MenuObj.do_pagenoFormSubmit();" title="페이지 이동">
<p class="pshow_p5dD020"><input type="text" name="pageno" value="" onfocus="MenuObj.do_pagenoFocusIn(event);" onblur="MenuObj.do_pagenoFocusOut(event)"
	pattern="[0-9-]+" title="숫자만 입력 가능합니다." required></p>
<p id="pshow_totalpage"></p>
</form>
</div>

<!-- search menu -->
<% if(searchFile.equals("true")){ %>
<div id="searchsect" class="search5dD020">
<form name="searchform" method="get" action="#" onsubmit="return do_searchbtnClick(document.searchform.qtxt.value)">
<input type="search" class="searchtxt5dD020" name="qtxt" title="검색어 입력" onfocus="do_qtxtFocusIn(this)">
<input type="image" class="searchbtn5dD020 hover_opac8" src="<%=webServer%>/skin5/mm020/search_btn.png" alt="검색 시작">
</form>
</div>
<% } %>

<!-- combobox -->
<% if(!combokind.equals("000")){ %>
<div id="combosect" class="cbbox5dD020" title="다른 카탈로그로 이동" data-width="display" data-align="left" data-direction="top">
<a href="javascript:comboClick();">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="151" height="18">
<g class="cbbox_g-text">
	<rect x="0.5" y="0.5" width="149" height="17" fill="#f7f7f7" stroke="#555555" />
	<text id="cbbox_text" x="3" y="14"><%=dirName%></text>
</g>
<g class="cbbox_g-btn" transform="translate(132,2)">
	<rect x="0.5" y="0.5" width="15" height="13" fill="#ffffff" stroke="#999999" />
	<polygon points="4,4 12,4 8,10" fill="#000000" />
</g>
</svg></a>
</div>
<% } %>

<!-- download -->
<% if(downFile.equals("true")){ %>
<div id="downsect" class="down5dD020">
<a href="<%=webpath%>/<%=downFileName%>" download="<%=webpath%>/<%=downFileName%>"
  title="파일 다운로드"><img src="<%=webServer%>/skin5/icon/file_<%=downexten%>.gif" class="down_img" alt="파일 다운로드"><%=downtext%></a>
</div>
<% } %>

</nav>

<script>
MenuObj = {storedStr:''};
MenuObj.onload_func2 = function(){
	var n = (PageInfo.showStartPage == 0) ? PageInfo.cataPages : PageInfo.get_showPageFromReal(PageInfo.cataPages);
	document.getElementById("pshow_totalpage").innerHTML = n;

	if(SkinInfo.printkind === "000" && document.getElementById("printbtn")) document.getElementById("printbtn").style.opacity = "0.2";
	if(FileInfo.idxFile === false && document.getElementById("indexbtn")) document.getElementById("indexbtn").style.opacity = "0.3";
	if(MenuInfo.mailButtonHide === "Y" && document.getElementById("mailbtn")) document.getElementById("mailbtn").style.opacity = "0.2";
}
MenuObj.show_pageNumber = function(){
	document.pageshowform.pageno.value = MoveInfo.get_showPageNumber();
}
MenuObj.do_pagenoFocusIn = function(e){
	MenuObj.storedStr = document.pageshowform.pageno.value;
	document.pageshowform.pageno.value = "";
}
MenuObj.do_pagenoFocusOut = function(e){
	document.pageshowform.pageno.value = MenuObj.storedStr;
}
MenuObj.do_pagenoFormSubmit = function(){
	var n = PermitMan.get_directGoPerm("showboard", document.pageshowform.pageno.value, "", "");			// device, spage, pageshow, state
	if(n < 0) return false;
	go_general(n, "user");
	return false;
}
MenuObj.onresize_func = function(){
}

backDiv = document.getElementById("backsect");
if(document.getElementById("combosect")) comboDiv = document.getElementById("combosect");
if(document.getElementById("searchsect")) searchDiv = document.getElementById("searchsect");
if(document.getElementById("snssect")) snsDiv = document.getElementById("snssect");
</script>

<% } else if(incView.equals("m")){ %>
<!-- navigation -->
<nav id="mmsect" class="mm5mD020">
<ul class="mm_ulE007">
<li id="firstbtn"><a href="javascript:firstbtnClick();" title="처음 페이지로 이동"><img src="<%=webServer%>/skin5/mm020/m_firstbtn.png" width="16" height="19" alt="처음 페이지"></a></li>
</ul>

<ul class="mm_ulE008">
<li id="prevbtn" class="prevbtn5mD020"><a href="javascript:prevbtnClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/mm020/m_prevbtn.png" width="11" height="18" alt="이전 페이지"></a></li>
<li id="nextbtn" class="nextbtn5mD020"><a href="javascript:nextbtnClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/mm020/m_nextbtn.png" width="12" height="18" alt="다음 페이지"></a></li>
<li id="slidebtn" class="slidebtn5mD020"><a href="javascript:slidebtnClick();" title="자동 넘김"><img src="<%=webServer%>/skin5/mm020/m_slidebtn.png" width="36" height="9" alt="자동 넘김"></a></li>
</ul>

<ul class="mm_ulE009">
<li id="lastbtn"><a href="javascript:lastbtnClick();" title="마지막 페이지로 이동"><img src="<%=webServer%>/skin5/mm020/m_lastbtn.png" width="16" height="19" alt="마지막 페이지로 이동"></a></li>
</ul>

<ul class="mm_ulE010">
<% if(indexType.equals("R")){ %>
<li id="indexbtn" class="indexbtn5mD020"><a href="javascript:indexbtnClick();" title="목차 보기"><img src="<%=webServer%>/skin5/mm020/m_indexbtn.png" width="18" height="9" alt="목차 보기"></a></li>
<% } %>
<li id="expbtn" class="expbtn5mD020"><a href="javascript:expbtnClick();" title="탐색기 보기"><img src="<%=webServer%>/skin5/mm020/m_expbtn.png" width="18" height="9" alt="탐색기 보기"></a></li>
<li id="combobtn" class="combobtn5mD020"><a href="javascript:combobtnClick();" title="다른 카탈로그 이동 페이지"><img src="<%=webServer%>/skin5/mm020/m_combobtn.png" width="18" height="9" alt="다른 카탈로그 이동 페이지"></a></li>
</ul>

<div id="pageshow" class="pshow5mD020" contentEditable="true" onfocus="do_pagenoFocusIn2(event);" onblur="do_pagenoFocusOut2(event);"></div>

<ul class="mm_ulE011">
<li id="mailbtn" class="mailbtn5mD020"><a href="javascript:mailbtnClick();" title="전자우편 발송"><img src="<%=webServer%>/skin5/mm020/m_mailbtn.png" width="29" height="22" alt="전자우편 발송"></a></li>
<li id="printbtn" class="printbtn5mD020"><a href="javascript:printbtnClick();" title="인쇄하기"><img src="<%=webServer%>/skin5/mm020/m_printbtn.png" width="28" height="22" alt="인쇄하기"></a></li>
<li id="helpbtn" class="helpbtn5mD020"><a href="javascript:helpbtnClick();" title="도움말 보기"><img src="<%=webServer%>/skin5/mm020/m_helpbtn.png" width="25" height="26" alt="도움말 보기"></a></li>
</ul>

<ul class="mm_ulE012">
<li id="facebookbtn" class="facebookbtn5mD020"><a href="javascript:facebookbtnClick();" title="페이스북 공유"><img src="<%=webServer%>/skin5/mm018/m_facebookbtn.png" width="25" height="22" alt="페이스북 공유"></a></li>
<li id="twitterbtn" class="twitterbtn5mD020"><a href="javascript:twitterbtnClick();" title="트위터 공유"><img src="<%=webServer%>/skin5/mm018/m_twitterbtn.png" width="25" height="22" alt="트위터 공유"></a></li>
<li id="kakaobtn" class="kakaobtn5mD020"><a href="javascript:kakaobtnClick();" title="카카오톡 공유"><img src="<%=webServer%>/skin5/mm018/m_kakaobtn.png" width="25" height="22" alt="카카오톡 공유"></a></li>
</ul>

<% if(searchFile.equals("true")){ %>
<div id="searchsect" class="search5mD020">
<form name="searchform" method="get" action="#" onsubmit="return do_searchbtnClick(document.searchform.qtxt.value)">
<input type="search" class="searchtxt5mD020" name="qtxt" title="검색어 입력" onfocus="do_qtxtFocusIn(this)">
<input type="button" class="searchbtn5mD020" value="검색">
</form>
</div>
<% } %>

</nav>

<% if(indexType.equals("H") && mokchaFile.equals("true")){ %>
<!-- horizontal index -->
<div id="horidx" class="horidx5mD020">
<div id="horidx_div" class="horidx_div5mD020"></div>
</div>
<% } %>

<script>
MenuObj = {storedStr:''};
MenuObj.onload_func2 = function(){
	if(LinkInfo.facebookStr === "" && document.getElementById("facebookbtn")) document.getElementById("facebookbtn").style.opacity = "0.2";
	if(LinkInfo.twitterStr === "" && document.getElementById("twitterbtn")) document.getElementById("twitterbtn").style.opacity = "0.2";
	if(LinkInfo.kakaoStr === "" && document.getElementById("kakaobtn")) document.getElementById("kakaobtn").style.opacity = "0.2";

	if(SkinInfo.printkind === "000" && document.getElementById("printbtn")) document.getElementById("printbtn").style.opacity = "0.2";
	if(MenuInfo.mailButtonHide === "Y" && document.getElementById("mailbtn")) document.getElementById("mailbtn").style.opacity = "0.2";
	if(FileInfo.idxFile === false && document.getElementById("indexbtn")) document.getElementById("indexbtn").style.opacity = "0.2";
	if(SkinInfo.combokind === "000" && document.getElementById("combobtn")) document.getElementById("combobtn").style.opacity = "0.2";
}
MenuObj.show_pageNumber = function(){
	var n = (PageInfo.showStartPage == 0) ? PageInfo.cataPages : PageInfo.get_showPageFromReal(PageInfo.cataPages);
	document.getElementById("pageshow").innerHTML = MoveInfo.get_showPageNumber() + "/" + n;
	show_currIndex();
}
MenuObj.do_pagenoFocusIn = function(e){
	MenuObj.storedStr = document.getElementById("pageshow").innerHTML;
	document.getElementById("pageshow").innerHTML = PageInfo.currentPage;
	window.getSelection().selectAllChildren(document.getElementById("pageshow"));
}
MenuObj.do_pagenoFocusOut = function(e){
	do_pagenoFormSubmit();
	document.getElementById("pageshow").innerHTML = MenuObj.storedStr;
}
MenuObj.do_pagenoFormSubmit = function(){
	var spage = document.getElementById("pageshow").innerHTML;
	var n = PermitMan.get_directGoPerm("showboard", spage, "", "");			// device, spage, pageshow, state
	if(n < 0) return false;
	go_general(n, "user");
	return false;
}
MenuObj.onresize_func = function(){
}
if(document.getElementById("searchsect")) searchDiv = document.getElementById("searchsect");

OnsmObj = {};
OnsmObj.set_location = function(){
}
OnsmObj.set_visible = function(b1, b2){
}
</script>
<% } %>
