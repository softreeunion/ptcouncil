<%@ page pageEncoding="utf-8" %>
<!-- bookdeco -->
<% if(Skin.get("bookdeco").equals("001") || Skin.get("bookdeco").equals("018")){ %>
<% if(Skin.get("bookdeco").equals("001")){ %>
<div id="bookdecosect">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<defs>
<pattern id="decobackL" width="13" height="50" patternContentUnits="objectBoundingBox">
	<image width="1" height="1" xlink:href="<%=webServer%>/skin5/bookdeco/leftmid001.png" preserveAspectRatio="xMidYMid slice" />
</pattern>
<pattern id="decobackR" width="100%" height="100%" patternContentUnits="objectBoundingBox">
	<image width="1" height="1" xlink:href="<%=webServer%>/skin5/bookdeco/rightmid001.png" preserveAspectRatio="xMidYMid slice" />
</pattern>
</defs>

<image id="decoImgLT" width="13" height="6" xlink:href="<%=webServer%>/skin5/bookdeco/lefttop001.png" />
<rect id="decoRectLM" y="6" width="13" height="50" fill="url(#decobackL)" />
<image id="decoImgLB" width="13" height="5" xlink:href="<%=webServer%>/skin5/bookdeco/leftbottom001.png" />
<image id="decoImgRT" width="13" height="6" xlink:href="<%=webServer%>/skin5/bookdeco/righttop001.png" />
<rect id="decoRectRM" y="6" width="13" height="50" fill="url(#decobackR)" />
<image id="decoImgRB" width="13" height="5" xlink:href="<%=webServer%>/skin5/bookdeco/rightbottom001.png" />
<rect id="decoRectC" x="0" y="0.5" width="1" height="1" stroke="#777777" />
</svg>
</div>
<% } else if(Skin.get("bookdeco").equals("018")){ %>
<div id="bookdecosect">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<defs>
<pattern id="decobackL" width="100%" height="100%" patternContentUnits="objectBoundingBox">
	<image width="1" height="1" xlink:href="<%=webServer%>/skin5/bookdeco/leftmid616.png" preserveAspectRatio="xMidYMid slice" />
</pattern>
<pattern id="decobackR" width="100%" height="100%" patternContentUnits="objectBoundingBox">
	<image width="1" height="1" xlink:href="<%=webServer%>/skin5/bookdeco/rightmid616.png" preserveAspectRatio="xMidYMid slice" />
</pattern>
</defs>

<image id="decoImgLT" width="13" height="2" xlink:href="<%=webServer%>/skin5/bookdeco/lefttop616.png" />
<rect id="decoRectLM" y="2" width="13" height="50" fill="url(#decobackL)" />
<image id="decoImgLB" width="13" height="1" xlink:href="<%=webServer%>/skin5/bookdeco/leftbottom616.png" />
<image id="decoImgRT" width="13" height="2" xlink:href="<%=webServer%>/skin5/bookdeco/righttop616.png" />
<rect id="decoRectRM" y="2" width="13" height="50" fill="url(#decobackR)" />
<image id="decoImgRB" width="13" height="1" xlink:href="<%=webServer%>/skin5/bookdeco/rightbottom616.png" />
<rect id="decoRectC" x="0" y="0.5" width="1" height="1" stroke="#d5d7d6" />
</svg>
</div>
<% } %>

<script>
bookdecoDiv = document.getElementById("bookdecosect");
DecoObj = {drawC:0};			// drawC : both_show:0, left_hide:1, right_hide:2, onesmc:3, both_hidden:4
DecoObj.onload_func2 = function(){
	if(ScreenInfo.onesmc === true){
		DecoObj.drawC = 3;
	}
	else{
		if(CataInfo.openingAnimation === true){
			if(PageInfo.currentPage === 0){
				DecoObj.drawC = 1;
			}
			else if(PageInfo.blank_imageFromLeft(PageInfo.currentPage) === 1){		// if the last page's right side is blank
				DecoObj.drawC = 2;
			}
		}
	}
	DecoObj.draw_deco(-1);
}
DecoObj.draw_deco = function(n){
	if(n !== -1) DecoObj.drawC = n;

	if(DecoObj.drawC === 3 || DecoObj.drawC === 1 || DecoObj.drawC === 4){
		document.getElementById('decoImgLT').setAttribute('visibility','hidden');
		document.getElementById('decoRectLM').setAttribute('visibility','hidden');
		document.getElementById('decoImgLB').setAttribute('visibility','hidden');
	}
	else{
		document.getElementById('decoImgLT').removeAttribute('visibility');
		document.getElementById('decoRectLM').removeAttribute('visibility');
		document.getElementById('decoImgLB').removeAttribute('visibility');
		DecoObj.loc_leftGrp();
	}

	if(DecoObj.drawC === 2 || DecoObj.drawC === 4){
		document.getElementById('decoImgRT').setAttribute('visibility','hidden');
		document.getElementById('decoRectRM').setAttribute('visibility','hidden');
		document.getElementById('decoImgRB').setAttribute('visibility','hidden');
	}
	else{
		document.getElementById('decoImgRT').removeAttribute('visibility');
		document.getElementById('decoRectRM').removeAttribute('visibility');
		document.getElementById('decoImgRB').removeAttribute('visibility');
		DecoObj.loc_rightGrp();
	}
	if(DecoObj.drawC === 4){
		document.getElementById('decoRectC').setAttribute('visibility','hidden');
	}
	else{
		document.getElementById('decoRectC').removeAttribute('visibility');
	}

	DecoObj.loc_ctrGrp();
}
<% if(Skin.get("bookdeco").equals("001")){ %>
DecoObj.loc_leftGrp = function(){
	document.getElementById('decoRectLM').setAttribute("height", smRect.height-9);
	document.getElementById('decoImgLB').setAttribute("y", smRect.height-3);
}
DecoObj.loc_rightGrp = function(){
	document.getElementById('decoImgRT').setAttribute('x', smRect.width+13);
	document.getElementById('decoRectRM').setAttribute('x', smRect.width+13);
	document.getElementById('decoRectRM').setAttribute("height", smRect.height-9);
	document.getElementById('decoImgRB').setAttribute('x', smRect.width+13);
	document.getElementById('decoImgRB').setAttribute("y", smRect.height-3);
}
<% } else if(Skin.get("bookdeco").equals("018")){ %>
DecoObj.loc_leftGrp = function(){
	document.getElementById('decoRectLM').setAttribute("height", smRect.height-1);
	document.getElementById('decoImgLB').setAttribute("y", smRect.height+1);
}
DecoObj.loc_rightGrp = function(){
	document.getElementById('decoImgRT').setAttribute('x', smRect.width+13);
	document.getElementById('decoRectRM').setAttribute('x', smRect.width+13);
	document.getElementById('decoRectRM').setAttribute("height", smRect.height-1);
	document.getElementById('decoImgRB').setAttribute('x', smRect.width+13);
	document.getElementById('decoImgRB').setAttribute("y", smRect.height+1);
}
<% } %>
DecoObj.loc_ctrGrp = function(){
	if(DecoObj.drawC === 1){
		document.getElementById('decoRectC').setAttribute('x', ScreenInfo.smImageWidth+13-0.5);
		document.getElementById('decoRectC').setAttribute("width", ScreenInfo.smImageWidth);
	}
	else{
		document.getElementById('decoRectC').setAttribute('x',13);
		if(DecoObj.drawC === 2) document.getElementById('decoRectC').setAttribute("width", ScreenInfo.smImageWidth+1);
		else document.getElementById('decoRectC').setAttribute("width", smRect.width);
	}
	document.getElementById('decoRectC').setAttribute("height", smRect.height+2-0.5);
}
DecoObj.onresize_func = function(){
	if(DecoObj.drawC === 0 || DecoObj.drawC === 2) DecoObj.loc_leftGrp();
	if(DecoObj.drawC !== 2) DecoObj.loc_rightGrp();
	DecoObj.loc_ctrGrp();
}
DecoObj.set_location = function(){
	CoverObj.bookdecoX = smRect.x - 13;
	bookdecoDiv.style.left = CoverObj.bookdecoX + "px";
	bookdecoDiv.style.top = (smRect.y-1) + "px";
	bookdecoDiv.style.width = (smRect.width+26) + "px";
	bookdecoDiv.style.height = (smRect.height+2) + "px";
	splbtnX = smRect.x-13, splbtnY = smRect.y+2;
	sprbtnX = smRect.right, sprbtnY = splbtnY;
}
</script>

<% } else if(Skin.get("bookdeco").equals("016")){ %>
<div id="bookdecosect">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<image id="decoImgTL" x="0" y="0" width="14" height="11" xlink:href="<%=webServer%>/skin5/bookdeco/topleft016.png" />
<image id="decoImgTC" x="14" y="0" width="100" height="11" xlink:href="<%=webServer%>/skin5/bookdeco/topcenter016.png" preserveAspectRatio="none" />
<image id="decoImgTR" x="100" y="0" width="15" height="11" xlink:href="<%=webServer%>/skin5/bookdeco/topright016.png" />

<image id="decoImgML" x="0" y="11" width="14" height="50" xlink:href="<%=webServer%>/skin5/bookdeco/midleft016.png" preserveAspectRatio="none" />
<rect id="decoRectMC" x="6" y="6" width="1" height="1" fill="#ffffff" />
<image id="decoImgMR" x="100" y="11" width="15" height="50" xlink:href="<%=webServer%>/skin5/bookdeco/midright016.png" preserveAspectRatio="none" />

<image id="decoImgBL" x="0" y="11" width="14" height="17" xlink:href="<%=webServer%>/skin5/bookdeco/btmleft016.png" />
<image id="decoImgBC" x="14" y="0" width="100" height="17" xlink:href="<%=webServer%>/skin5/bookdeco/btmcenter016.png" preserveAspectRatio="none" />
<image id="decoImgBR" width="15" height="17" xlink:href="<%=webServer%>/skin5/bookdeco/btmright016.png" />
</svg>
</div>
<script>
bookdecoDiv = document.getElementById("bookdecosect");
DecoObj = {drawC:0};			// drawC : both_show:0, left_hide:1, right_hide:2, onesmc:3
DecoObj.onload_func2 = function(){
	DecoObj.drawC = 0;
	if(ScreenInfo.onesmc === true){
		DecoObj.drawC = 3;
	}
	else{
		if(CataInfo.openingAnimation === true){
			if(PageInfo.currentPage === 0){
				DecoObj.drawC = 1;
			}
			else if(PageInfo.blank_imageFromLeft(PageInfo.currentPage) === 1){		// if the last page's right side is blank
				DecoObj.drawC = 2;
			}
		}
	}

	DecoObj.draw_deco(-1);
}
DecoObj.draw_deco = function(n){
	if(n != -1) DecoObj.drawC = n;

	//alert(DecoObj.drawC);
	if(DecoObj.drawC === 0 || DecoObj.drawC === 3){
		document.getElementById('decoImgTL').setAttribute("x", 0);
		document.getElementById('decoImgML').setAttribute("x", 0);
		document.getElementById('decoImgML').setAttribute("height", smRect.height-10);
		document.getElementById('decoImgBL').setAttribute("x", 0);
		document.getElementById('decoImgBL').setAttribute("y", smRect.height+1);

		document.getElementById('decoImgTC').setAttribute("x", 14);
		document.getElementById('decoImgTC').setAttribute("width", smRect.width-11);
		document.getElementById('decoRectMC').setAttribute("x", 6);
		document.getElementById('decoRectMC').setAttribute("width", smRect.width);
		document.getElementById('decoRectMC').setAttribute("height", smRect.height);
		document.getElementById('decoImgBC').setAttribute("x", 14);
		document.getElementById('decoImgBC').setAttribute("y", smRect.height+1);
		document.getElementById('decoImgBC').setAttribute("width", smRect.width-11);

		document.getElementById('decoImgTR').setAttribute('x', smRect.width+3);
		document.getElementById('decoImgMR').setAttribute('x', smRect.width+3);
		document.getElementById('decoImgMR').setAttribute("height", smRect.height-10);
		document.getElementById('decoImgBR').setAttribute("x", smRect.width+3);
		document.getElementById('decoImgBR').setAttribute("y", smRect.height+1);
	}
	else if(DecoObj.drawC === 1){
		document.getElementById('decoImgTL').setAttribute("x", ScreenInfo.smImageWidth);
		document.getElementById('decoImgML').setAttribute("x", ScreenInfo.smImageWidth);
		document.getElementById('decoImgML').setAttribute("height", smRect.height-10);
		document.getElementById('decoImgBL').setAttribute("x", ScreenInfo.smImageWidth);
		document.getElementById('decoImgBL').setAttribute("y", smRect.height+1);

		document.getElementById('decoImgTC').setAttribute("x", ScreenInfo.smImageWidth+14);
		document.getElementById('decoImgTC').setAttribute("width", ScreenInfo.smImageWidth-11);
		document.getElementById('decoRectMC').setAttribute("x", ScreenInfo.smImageWidth+6);
		document.getElementById('decoRectMC').setAttribute("width", ScreenInfo.smImageWidth);
		document.getElementById('decoRectMC').setAttribute("height", smRect.height);
		document.getElementById('decoImgBC').setAttribute("x", ScreenInfo.smImageWidth+14);
		document.getElementById('decoImgBC').setAttribute("y", smRect.height+1);
		document.getElementById('decoImgBC').setAttribute("width", ScreenInfo.smImageWidth-11);

		document.getElementById('decoImgTR').setAttribute('x', smRect.width+3);
		document.getElementById('decoImgMR').setAttribute('x', smRect.width+3);
		document.getElementById('decoImgMR').setAttribute("height", smRect.height-10);
		document.getElementById('decoImgBR').setAttribute("x", smRect.width+3);
		document.getElementById('decoImgBR').setAttribute("y", smRect.height+1);
	}
	else if(DecoObj.drawC === 2){
		document.getElementById('decoImgTL').setAttribute("x", 0);
		document.getElementById('decoImgML').setAttribute("x", 0);
		document.getElementById('decoImgML').setAttribute("height", smRect.height-10);
		document.getElementById('decoImgBL').setAttribute("x", 0);
		document.getElementById('decoImgBL').setAttribute("y", smRect.height+1);

		document.getElementById('decoImgTC').setAttribute("x", 14);
		document.getElementById('decoImgTC').setAttribute("width", ScreenInfo.smImageWidth-11);
		document.getElementById('decoRectMC').setAttribute("x", 6);
		document.getElementById('decoRectMC').setAttribute("width", ScreenInfo.smImageWidth);
		document.getElementById('decoRectMC').setAttribute("height", smRect.height);
		document.getElementById('decoImgBC').setAttribute("x", 14);
		document.getElementById('decoImgBC').setAttribute("y", smRect.height+1);
		document.getElementById('decoImgBC').setAttribute("width", ScreenInfo.smImageWidth-11);

		document.getElementById('decoImgTR').setAttribute('x', ScreenInfo.smImageWidth+3);
		document.getElementById('decoImgMR').setAttribute('x', ScreenInfo.smImageWidth+3);
		document.getElementById('decoImgMR').setAttribute("height", smRect.height-10);
		document.getElementById('decoImgBR').setAttribute("x", ScreenInfo.smImageWidth+3);
		document.getElementById('decoImgBR').setAttribute("y", smRect.height+1);
	}
}
DecoObj.onresize_func = function(){
	DecoObj.draw_deco(-1);
}
DecoObj.set_location = function(){
	CoverObj.bookdecoX = smRect.x - 6;
	bookdecoDiv.style.left = CoverObj.bookdecoX + "px";
	bookdecoDiv.style.top = (smRect.y-6) + "px";
	bookdecoDiv.style.width = (smRect.width+18) + "px";				// 6 + 12
	bookdecoDiv.style.height = (smRect.height+18) + "px";			// 6 + 12
	splbtnX = smRect.x, splbtnY = smRect.y+2;
	sprbtnX = smRect.right-12, sprbtnY = splbtnY;
}
</script>

<% } else if(Skin.get("bookdeco").equals("617")){ %>
<div id="bookdecosect">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<rect id="decoRect1" x="0" y="0" width="13" height="50" fill="#A6A6A6" />
<rect id="decoRect2" x="0" y="0" width="13" height="50" fill="#CFCFCF" />
</svg>
</div>
<script>
bookdecoDiv = document.getElementById("bookdecosect");
DecoObj = {drawC:0};			// drawC : both_show:0, left_hide:1, right_hide:2, onesmc:3
DecoObj.onload_func2 = function(){
	DecoObj.drawC = 0;
	if(ScreenInfo.onesmc === true){
		DecoObj.drawC = 3;
	}
	else{
		if(CataInfo.openingAnimation === true){
			if(PageInfo.currentPage === 0){
				DecoObj.drawC = 1;
			}
			else if(PageInfo.blank_imageFromLeft(PageInfo.currentPage) === 1){		// if the last page's right side is blank
				DecoObj.drawC = 2;
			}
		}
	}

	DecoObj.draw_deco(-1);
}
DecoObj.draw_deco = function(n){
	if(n !== -1) DecoObj.drawC = n;

	if(DecoObj.drawC === 0 || DecoObj.drawC === 3){
		document.getElementById('decoRect1').setAttribute('x', 0);
		document.getElementById('decoRect1').setAttribute('y', 10);
		document.getElementById('decoRect1').setAttribute('width', smRect.width+26);
		document.getElementById('decoRect1').setAttribute('height', smRect.height-44);

		document.getElementById('decoRect2').setAttribute('x', 7);
		document.getElementById('decoRect2').setAttribute('y', 0);
		document.getElementById('decoRect2').setAttribute('width', smRect.width+12);
		document.getElementById('decoRect2').setAttribute('height', smRect.height-24);
	}
	else if(DecoObj.drawC === 1){
		document.getElementById('decoRect1').setAttribute('x', ScreenInfo.smImageWidth+13);
		document.getElementById('decoRect1').setAttribute('y', 10);
		document.getElementById('decoRect1').setAttribute('width', ScreenInfo.smImageWidth+13);
		document.getElementById('decoRect1').setAttribute('height', smRect.height-44);

		document.getElementById('decoRect2').setAttribute('x', ScreenInfo.smImageWidth+13);
		document.getElementById('decoRect2').setAttribute('y', 0);
		document.getElementById('decoRect2').setAttribute('width', ScreenInfo.smImageWidth+6);
		document.getElementById('decoRect2').setAttribute('height', smRect.height-24);
	}
	else if(DecoObj.drawC === 2){
		document.getElementById('decoRect1').setAttribute('x', 0);
		document.getElementById('decoRect1').setAttribute('y', 22);
		document.getElementById('decoRect1').setAttribute('width', ScreenInfo.smImageWidth+13);
		document.getElementById('decoRect1').setAttribute('height', smRect.height-44);

		document.getElementById('decoRect2').setAttribute('x', 7);
		document.getElementById('decoRect2').setAttribute('y', 12);
		document.getElementById('decoRect2').setAttribute('width', ScreenInfo.smImageWidth+6);
		document.getElementById('decoRect2').setAttribute('height', smRect.height-24);
	}
}
DecoObj.onresize_func = function(){
	DecoObj.draw_deco(-1);
}
DecoObj.set_location = function(){
	CoverObj.bookdecoX = smRect.x - 13;
	bookdecoDiv.style.left = CoverObj.bookdecoX + "px";
	bookdecoDiv.style.top = (smRect.y+12) + "px";
	bookdecoDiv.style.width = (smRect.width+26) + "px";				// 6 + 12
	bookdecoDiv.style.height = (smRect.height-24) + "px";			// 6 + 12
	splbtnX = smRect.x, splbtnY = smRect.y+12;
	sprbtnX = smRect.right-12, sprbtnY = splbtnY;
}
</script>
<% } %>

<!-- spacerbutton -->
<% String spacerbutton = (String)Skin.get("spacerbutton"); %>
<% if(spacerbutton.equals("001")){ %>
<div id="splbtnsect">
<img src="<%=webServer%>/skin5/spacer/spacerbuttonL001.png" width="52" height="32" usemap="#spacerMap1" alt="책갈피 버튼">
<map name="spacerMap1">
	<area shape="rect" coords="6,6,14,14" href="javascript:spacerbtnClick('view')" alt="책갈피 보기" title="책갈피 보기">
	<area shape="rect" coords="6,17,14,26" href="javascript:spacerbtnClick('left')" alt="책갈피 추가" title="책갈피 추가">
</map>
</div>
<div id="sprbtnsect">
<img src="<%=webServer%>/skin5/spacer/spacerbuttonR001.png" width="52" height="32" usemap="#spacerMap2" alt="책갈피 버튼">
<map name="spacerMap2">
	<area shape="rect" coords="37,6,45,14" href="javascript:spacerbtnClick('view')" alt="책갈피 보기" title="책갈피 보기">
	<area shape="rect" coords="37,17,45,26" href="javascript:spacerbtnClick('right')" alt="책갈피 추가" title="책갈피 추가">
</map>
</div>

<script>
splbtnDiv = document.getElementById("splbtnsect");
sprbtnDiv = document.getElementById("sprbtnsect");
CoverObj.splbtnX = 0;
CoverObj.sprbtnX = 0;

SpacerObj = {};
SpacerObj.set_location = function(){
	splbtnDiv.style.left = (splbtnX-21) + "px";			// 본래 너비와 많이 틀림(19)
	splbtnDiv.style.top = (splbtnY-3) + "px";
	sprbtnDiv.style.left = (sprbtnX-20) + "px";
	sprbtnDiv.style.top = (splbtnY-3) + "px";
}
</script>

<% } else if(spacerbutton.equals("002") || spacerbutton.equals("004")){ %>
<div id="splbtnsect">
<img src="<%=webServer%>/skin5/spacer/spacerbuttonL<%=spacerbutton%>.png" width="27" height="46" usemap="#spacerMap1" alt="책갈피 버튼">
<map name="spacerMap1">
	<area shape="rect" coords="8,6,19,19" href="javascript:spacerbtnClick('view')" alt="책갈피 보기" title="책갈피 보기">
	<area shape="rect" coords="8,26,19,38" href="javascript:spacerbtnClick('left')" alt="책갈피 추가" title="책갈피 추가">
</map>
</div>
<div id="sprbtnsect">
<img src="<%=webServer%>/skin5/spacer/spacerbuttonR<%=spacerbutton%>.png" width="27" height="46" usemap="#spacerMap2" alt="책갈피 버튼">
<map name="spacerMap2">
	<area shape="rect" coords="8,6,19,19" href="javascript:spacerbtnClick('view')" alt="책갈피 보기" title="책갈피 보기">
	<area shape="rect" coords="8,26,19,38" href="javascript:spacerbtnClick('right')" alt="책갈피 추가" title="책갈피 추가">
</map>
</div>

<script>
splbtnDiv = document.getElementById("splbtnsect");
sprbtnDiv = document.getElementById("sprbtnsect");
CoverObj.splbtnX = 0;
CoverObj.sprbtnX = 0;

SpacerObj = {};
SpacerObj.set_location = function(){
	splbtnDiv.style.left = (splbtnX-26) + "px";				// 너비는 27인데 보기좋게 하기 위하여 1을 안보이게 함.
	splbtnDiv.style.top = splbtnY + "px";
	sprbtnDiv.style.left = (sprbtnX+12) + "px";
	sprbtnDiv.style.top = sprbtnY + "px";
}
</script>

<% } else if(spacerbutton.equals("003")){ %>
<div id="splbtnsect">
<img src="<%=webServer%>/skin5/spacer/spacerbuttonL003.png" width="24" height="150" usemap="#spacerMap1" alt="책갈피 버튼">
<map name="spacerMap1">
	<area shape="rect" coords="1,1,24,74" href="javascript:spacerbtnClick('view')" alt="책갈피 보기" title="책갈피 보기">
	<area shape="rect" coords="1,76,24,150" href="javascript:spacerbtnClick('left')" alt="책갈피 추가" title="책갈피 추가">
</map>
</div>
<div id="sprbtnsect">
<img src="<%=webServer%>/skin5/spacer/spacerbuttonR003.png" width="24" height="150" usemap="#spacerMap2" alt="책갈피 버튼">
<map name="spacerMap2">
	<area shape="rect" coords="1,1,24,74" href="javascript:spacerbtnClick('view')" alt="책갈피 보기" title="책갈피 보기">
	<area shape="rect" coords="1,76,24,150" href="javascript:spacerbtnClick('right')" alt="책갈피 추가" title="책갈피 추가">
</map>
</div>

<script>
splbtnDiv = document.getElementById("splbtnsect");
sprbtnDiv = document.getElementById("sprbtnsect");
CoverObj.splbtnX = 0;
CoverObj.sprbtnX = 0;

SpacerObj = {};
SpacerObj.set_location = function(){
	splbtnDiv.style.left = (splbtnX-23) + "px";
	splbtnDiv.style.top = splbtnY + "px";
	sprbtnDiv.style.left = (sprbtnX+11) + "px";
	sprbtnDiv.style.top = sprbtnY + "px";
}
</script>

<% } else if(spacerbutton.equals("011")){ %>
<div id="splbtnsect" class="splbtn5dD011">
<img src="<%=webServer%>/skin5/spacer/spacerbuttonL011.png" width="27" height="52" usemap="#spacerMap1" alt="책갈피 버튼">
<map name="spacerMap1">
	<area shape="rect" coords="9,9,22,22" href="javascript:spacerbtnClick('view')" alt="책갈피 보기" title="책갈피 보기">
	<area shape="rect" coords="9,28,22,41" href="javascript:spacerbtnClick('left')" alt="책갈피 추가" title="책갈피 추가">
</map>
</div>
<div id="sprbtnsect" class="sprbtn5dD011">
<img src="<%=webServer%>/skin5/spacer/spacerbuttonR011.png" width="27" height="52" usemap="#spacerMap2" alt="책갈피 버튼">
<map name="spacerMap2">
	<area shape="rect" coords="5,10,17,22" href="javascript:spacerbtnClick('view')" alt="책갈피 보기" title="책갈피 보기">
	<area shape="rect" coords="5,28,17,40" href="javascript:spacerbtnClick('right')" alt="책갈피 추가" title="책갈피 추가">
</map>
</div>

<script>
splbtnDiv = document.getElementById("splbtnsect");
sprbtnDiv = document.getElementById("sprbtnsect");
splbtnDiv.style.zIndex = 70;
sprbtnDiv.style.zIndex = 71;
splbtnDiv.style.left = "4px";
splbtnDiv.style.top = "124px";
sprbtnDiv.style.right = "88px";
sprbtnDiv.style.top = "124px";

SpacerObj = {};
SpacerObj.set_location = function(){
}
</script>

<% } else if(spacerbutton.equals("005")){ %>
<div id="splbtnsect" class="splbtn5dE005">
<a href="javascript:SpacerObj.do_splbtnClick();" title="책갈피 추가"><img src="<%=webServer%>/skin5/spacer/spacerbuttonL005.png" width="22" height="62" alt="책갈피 버튼"></a>
<svg version="1.1" id="splsvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="22" height="200"></svg>
</div>
<div id="sprbtnsect" class="splbtn5dE005">
<a href="javascript:SpacerObj.do_sprbtnClick();" title="책갈피 추가"><img src="<%=webServer%>/skin5/spacer/spacerbuttonR005.png" width="22" height="62" alt="책갈피 버튼"></a>
<svg version="1.1" id="sprsvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="22" height="200"></svg>
</div>

<script>
splbtnDiv = document.getElementById("splbtnsect");
splSvg = document.getElementById("splsvg");
sprbtnDiv = document.getElementById("sprbtnsect");
sprSvg = document.getElementById("sprsvg");
CoverObj.splbtnX = 0;
CoverObj.sprbtnX = 0;

SpacerObj = {};
SpacerObj.set_location = function(){
	splbtnDiv.style.left = (splbtnX-22) + "px";
	splbtnDiv.style.top = splbtnY + "px";
	sprbtnDiv.style.left = (sprbtnX+12) + "px";
	sprbtnDiv.style.top = sprbtnY + "px";
}
SpacerObj.onload_func2 = function(){
	SpacerInfo.retrieve_item();
	SpacerObj.show_items();
}
SpacerObj.finish_turnover = function(){
	SpacerObj.show_items();
}
SpacerObj.do_itemClick = function(pno){
	var n = PermitMan.get_directGoPerm("spacer", pno, "", "");			// device, spage, pageshow, state
	if(n < 0) return;

	if(SpacerInfo.delWhenClick === true){
		SpacerInfo.remove_itemValue(""+pno);
	}

	go_general(n, "system");
}
SpacerObj.do_splbtnClick = function(){
	SpacerObj.do_addSpacer(PageInfo.currentPage);
}
SpacerObj.do_sprbtnClick = function(){
	if(ScreenInfo.onesmc === true) SpacerObj.do_addSpacer(PageInfo.currentPage);
	else SpacerObj.do_addSpacer(PageInfo.currentPage+1);
}
SpacerObj.do_addSpacer = function(n){
	if(PermitMan.get_spcerAddPerm(n) == false) return;

	SpacerInfo.add_item(""+n, 0);
	SpacerObj.show_items();
}
SpacerObj.show_items = function(){
	var nodes = splSvg.childNodes;
	for(var i = nodes.length-1;i >= 0;i--){
		splSvg.removeChild(nodes[i]);
	}

	nodes = sprSvg.childNodes;
	for(i = nodes.length-1;i >= 0;i--){
		sprSvg.removeChild(nodes[i]);
	}

	if(SpacerInfo.arItem.length === 0) return;

	for(i = 0;i < SpacerInfo.arItem.length;i++){
		if(SpacerInfo.arItem[i] == "") continue;

		var n = parseInt(SpacerInfo.arItem[i]);
		var rectObj = document.createElementNS("http://www.w3.org/2000/svg","rect");
		rectObj.setAttribute("x", 0);
		rectObj.setAttribute("y", 5+(20+3)*i);
		rectObj.setAttribute("width", 22);
		rectObj.setAttribute("height", 20);
		rectObj.setAttribute("fill", SpacerInfo.arColor[i]);
		rectObj.setAttribute("onclick", "SpacerObj.do_itemClick("+n+")");

		if(n <= PageInfo.currentPage){
			splSvg.appendChild(rectObj);
		}
		else{
			sprSvg.appendChild(rectObj);
		}
	}
}
</script>
<% } %>


<!-- onsmimage -->
<% String onsmimage = (String)Skin.get("onsmimage"); %>
<% if(onsmimage.equals("001")){ %>
<div id="onsmlsect" class="onsmlE001">
<a href="javascript:leftOnsmClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageL001.png" width="36" height="36" alt="이전 페이지"
  onmouseover="this.src='<%=webServer%>/skin5/onsm/onsmimageL001_over.png'" onmouseout="this.src='<%=webServer%>/skin5/onsm/onsmimageL001.png'"></a>
</div>
<div id="onsmrsect" class="onsmrE001">
<a href="javascript:rightOnsmClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageR001.png" width="36" height="36" alt="다음 페이지"
  onmouseover="this.src='<%=webServer%>/skin5/onsm/onsmimageR001_over.png'" onmouseout="this.src='<%=webServer%>/skin5/onsm/onsmimageR001.png'"></a>
</div>
<script>
onsmlDiv = document.getElementById("onsmlsect");
onsmrDiv = document.getElementById("onsmrsect");
OnsmObj = {};
OnsmObj.set_location = function(){
	CoverObj.onsmrX = smRect.right - 36;
	onsmlDiv.style.left = smRect.x + "px";
	onsmlDiv.style.top = (smRect.bottom-36) + "px";
	onsmrDiv.style.left = (smRect.right-36) + "px";
	onsmrDiv.style.top = (smRect.bottom-36) + "px";
}
OnsmObj.set_visible = function(b1, b2){
	onsmlDiv.style.visibility = b1;
	onsmrDiv.style.visibility = b2;
}
OnsmObj.prepare_enlarge = function(){
}
OnsmObj.complete_downsize = function(){
}
</script>

<% } else if(onsmimage.equals("002")){ %>
<div id="onsmlsect" class="onsml002">
<a href="javascript:leftOnsmClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageL002.png" width="27" height="36" alt="이전 페이지"
  onmouseover="this.src='<%=webServer%>/skin5/onsm/onsmimageL002_over.png'" onmouseout="this.src='<%=webServer%>/skin5/onsm/onsmimageL002.png'"></a>
</div>
<div id="onsmrsect" class="onsmr002">
<a href="javascript:rightOnsmClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageR002.png" width="27" height="36" alt="다음 페이지"
  onmouseover="this.src='<%=webServer%>/skin5/onsm/onsmimageR002_over.png'" onmouseout="this.src='<%=webServer%>/skin5/onsm/onsmimageR002.png'"></a>
</div>
<script>
onsmlDiv = document.getElementById("onsmlsect");
onsmrDiv = document.getElementById("onsmrsect");
OnsmObj = {};
OnsmObj.set_location = function(){
	CoverObj.onsmrX = smRect.right - 27;
	onsmlDiv.style.left = smRect.x + "px";
	onsmlDiv.style.top = (smRect.bottom-36) + "px";
	onsmrDiv.style.left = (smRect.right-27) + "px";
	onsmrDiv.style.top = (smRect.bottom-36) + "px";
}
OnsmObj.set_visible = function(b1, b2){
	onsmlDiv.style.visibility = b1;
	onsmrDiv.style.visibility = b2;
}
OnsmObj.prepare_enlarge = function(){
}
OnsmObj.complete_downsize = function(){
}
</script>

<% } else if(onsmimage.equals("003")){ %>
<div id="onsmlsect" class="onsml003">
<a href="javascript:leftOnsmClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageL003.png" width="22" height="25" alt="이전 페이지"
  onmouseover="this.src='<%=webServer%>/skin5/onsm/onsmimageL003_over.png'" onmouseout="this.src='<%=webServer%>/skin5/onsm/onsmimageL003.png'"></a>
</div>
<div id="onsmrsect" class="onsmr003">
<a href="javascript:rightOnsmClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageR003.png" width="22" height="25" alt="다음 페이지"
  onmouseover="this.src='<%=webServer%>/skin5/onsm/onsmimageR003_over.png'" onmouseout="this.src='<%=webServer%>/skin5/onsm/onsmimageR003.png'"></a>
</div>
<script>
onsmlDiv = document.getElementById("onsmlsect");
onsmrDiv = document.getElementById("onsmrsect");
MoveInfo.onsmCoverAni = true;
OnsmObj = {};
OnsmObj.set_location = function(){
	CoverObj.onsmrX = smRect.right - 22;
	onsmlDiv.style.left = smRect.x + "px";
	onsmlDiv.style.top = (smRect.bottom-25) + "px";
	onsmrDiv.style.left = (smRect.right-22) + "px";
	onsmrDiv.style.top = (smRect.bottom-25) + "px";
}
OnsmObj.set_visible = function(b1, b2){
	onsmlDiv.style.visibility = b1;
	onsmrDiv.style.visibility = b2;
}
OnsmObj.prepare_enlarge = function(){
}
OnsmObj.complete_downsize = function(){
}
</script>

<% } else if(onsmimage.equals("004")){ %>
<div id="onsmlsect" class="onsml004">
<a href="javascript:leftOnsmClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageL004.png" width="33" height="33" alt="이전 페이지"
  onmouseover="this.src='<%=webServer%>/skin5/onsm/onsmimageL004_over.png'" onmouseout="this.src='<%=webServer%>/skin5/onsm/onsmimageL004.png'"></a>
</div>
<div id="onsmrsect" class="onsmr004">
<a href="javascript:rightOnsmClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageR004.png" width="33" height="33" alt="다음 페이지"
  onmouseover="this.src='<%=webServer%>/skin5/onsm/onsmimageR004_over.png'" onmouseout="this.src='<%=webServer%>/skin5/onsm/onsmimageR004.png'"></a>
</div>
<script>
onsmlDiv = document.getElementById("onsmlsect");
onsmrDiv = document.getElementById("onsmrsect");
OnsmObj = {};
OnsmObj.set_location = function(){
	CoverObj.onsmrX = smRect.right - 33;
	onsmlDiv.style.left = smRect.x + "px";
	onsmlDiv.style.top = (smRect.bottom-33) + "px";
	onsmrDiv.style.left = (smRect.right-33) + "px";
	onsmrDiv.style.top = (smRect.bottom-33) + "px";
}
OnsmObj.set_visible = function(b1, b2){
	onsmlDiv.style.visibility = b1;
	onsmrDiv.style.visibility = b2;
}
OnsmObj.prepare_enlarge = function(){
}
OnsmObj.complete_downsize = function(){
}
</script>

<% } else if(onsmimage.equals("008")){ %>
<div id="onsmlsect" class="onsml008">
<a href="javascript:leftOnsmClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageL008.png" width="34" height="39" alt="이전 페이지"
  onmouseover="this.src='<%=webServer%>/skin5/onsm/onsmimageL008_over.png'" onmouseout="this.src='<%=webServer%>/skin5/onsm/onsmimageL008.png'"></a>
</div>
<div id="onsmrsect" class="onsmr008">
<a href="javascript:rightOnsmClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageR008.png" width="34" height="39" alt="다음 페이지"
  onmouseover="this.src='<%=webServer%>/skin5/onsm/onsmimageR008_over.png'" onmouseout="this.src='<%=webServer%>/skin5/onsm/onsmimageR008.png'"></a>
</div>
<script>
onsmlDiv = document.getElementById("onsmlsect");
onsmrDiv = document.getElementById("onsmrsect");
OnsmObj = {};
OnsmObj.set_location = function(){
	CoverObj.onsmrX = sprbtnX + 12;
	onsmlDiv.style.left = (splbtnX-34) + "px";
	onsmlDiv.style.top = (smRect.centerPt.y-18) + "px";
	onsmrDiv.style.left = (sprbtnX+12) + "px";
	onsmrDiv.style.top = (smRect.centerPt.y-18) + "px";
}
OnsmObj.set_visible = function(b1, b2){
	onsmlDiv.style.visibility = b1;
	onsmrDiv.style.visibility = b2;
}
OnsmObj.prepare_enlarge = function(){
}
OnsmObj.complete_downsize = function(){
}
</script>

<% } else if(onsmimage.equals("016")){ %>
<div id="onsmlsect" class="onsmlE016">
<a href="javascript:leftOnsmClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageL<%=Skin.get("onsmskin")%>.png" width="60" height="116" alt="이전 페이지"></a>
</div>
<div id="onsmrsect" class="onsmrE016">
<a href="javascript:rightOnsmClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageR<%=Skin.get("onsmskin")%>.png" width="60" height="116" alt="다음 페이지"></a>
</div>
<script>
onsmlDiv = document.getElementById("onsmlsect");
onsmrDiv = document.getElementById("onsmrsect");
OnsmObj = {};
OnsmObj.set_location = function(){
	onsmlDiv.style.left = winRect.x + "px";
	onsmlDiv.style.top = (smRect.centerPt.y-59) + "px";
	onsmrDiv.style.left = (winRect.right-60) + "px";
	onsmrDiv.style.top = (smRect.centerPt.y-59) + "px";
}
OnsmObj.set_visible = function(b1, b2){
	onsmlDiv.style.visibility = b1;
	onsmrDiv.style.visibility = b2;
}
OnsmObj.prepare_enlarge = function(){
}
OnsmObj.complete_downsize = function(){
}
</script>

<% } %>
