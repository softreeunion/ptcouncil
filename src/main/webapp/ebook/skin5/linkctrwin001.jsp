<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%
	String tw = get_param1(request.getParameter("w"),5);
	int w = tw.equals("") ? 0 : Integer.parseInt(tw);
	String th = get_param1(request.getParameter("h"),5);
	int h = th.equals("") ? 0 : Integer.parseInt(th);
	String ds = request.getParameter("d");
	if(ds == null) ds = "";

	String idx = get_param2(request.getParameter("idx"),5);		// arLinkUnit's index

	String[] ar = mysplit(ds,".");
	String n = "";
	if(ar.length > 1) n = get_param2(ar[1],5);			// SkinInfo.nwclass(n)
	String v = "";
	if(ar.length > 2) v = check_um(ar[2],"um");			// CataInfo.incview(v)
	String d = check_mainmenu(ar[0], v);				// SkinInfo.mainmenu(d)
	String r = "ctrwin";	

	pathOfOriCata = webSysDir + "/catImage";
	HashMap Display = new HashMap();
	Display.put("user", get_userSkin(d, v));
	if(!Display.get("user").equals("")) set_userSkin((String)Display.get("user"), Display);
	set_newintitle(r, d, webSysDir+"/skin5", Display);

	String newin_titletext = "도움말";
	String svgObj = "ctrwinSvg";
	String plusID = "";
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>ecatalog - ctrwin001</title>
<style>
	html, body{width:100%;height:100%;margin:0;overflow:hidden;}
	*{font-family:굴림;font-size:12px;}

	#maindiv{display:block;z-index:2;position:absolute;width:100%;height:100%;}
	#maindiv > object{display:block;width:100%;height:100%;}
	#maindiv > video{display:block;width:100%;height:100%;}
</style>
<script>
var selfName = "linkctrwin001.jsp", winCnt = "ctrwin";
var svgWidth = <%=w%>, svgHeight = <%=h%>;
var p;
var ws = "";

var incview = "<%=v%>";
var idx = <%=idx%>;

var downObj, downX, downY;
var linkunit;
var movieObj, newinObj;
var currState = "normal";			// normal, full
var oriwidth, oriheight;

var dragGap = 0;

function onload_func(){
	p = window.parent;
	console.log("ctrwin001 loaded");
	p.ctrwinSvg = this;
	window.parent.objLoaded2('newin', this);
	p.document.getElementById('newin_frame').innerHTML = ws;

	linkunit = p.LinkObj.arLinkUnit[idx];
	var n = linkunit.pointer;
	var stitle = p.LinkInfo.arLinkExpl[n];
	p.document.getElementById("newin_titletext").textContent = stitle;
}

/// ############################## window and events ############################## ///
function newin_close(){
	p.unload_newin(winCnt);
	p.ctrwinSvg = undefined;
}
function newin_fullscr(){
	if(currState === "normal"){
		currState = "full";
		swap_fullscrimage("restore");
		p.LinkObj.go_winToFullscr();
		newin_fullscr2();
	}
	else{
		currState = "normal";
		swap_fullscrimage("fullscr");
		p.LinkObj.go_winToNormalscr();
		newin_restore();
	}
}
function do_mouseDrag(e){
	if(currState === "full") return;
	e.preventDefault();

	var posX = e.clientX - p.clickX + p.befX;
	var posY = e.clientY - p.clickY + p.befY;

	if(posX < p.cataRect.x) posX = p.cataRect.x;
	else if(posX > p.cataRect.right-svgWidth-dragGap) posX = p.cataRect.right - svgWidth - dragGap;

	if(posY < p.cataRect.y) posY = p.cataRect.y;
	else if(posY > p.cataRect.bottom-svgHeight-dragGap) posY = p.cataRect.bottom - svgHeight - dragGap;

	p.newinDiv.style.left = posX + "px";
	p.newinDiv.style.top = posY + "px";

	return true;
}
function do_mouseUp(e){
	p.befX += e.clientX - p.clickX;
	p.befY += e.clientY - p.clickY;
}
</script>
</head>

<body onload="onload_func()">

<%
	int fullscrbtn = 1;
	int closebtn = 1;
	String skind = "";
%>
<%@ include file="inc_newin.jsp" %>

<div id="maindiv"></div>
</body>
</html>
