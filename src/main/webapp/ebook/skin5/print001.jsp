<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%
	String tw = get_param1(request.getParameter("w"),5);
	int w = tw.equals("") ? 0 : Integer.parseInt(tw);
	String th = get_param1(request.getParameter("h"),5);
	int h = th.equals("") ? 0 : Integer.parseInt(th);
	String ds = request.getParameter("d");
	if(ds == null) ds = "";

	String[] ar = mysplit(ds,".");
	String n = "";
	if(ar.length > 1) n = get_param2(ar[1],5);			// SkinInfo.nwclass(n)
	String v = "";
	if(ar.length > 2) v = check_um(ar[2],"um");			// CataInfo.incview(v)
	String d = check_mainmenu(ar[0], v);				// SkinInfo.mainmenu(d)
	String r = "print";

	pathOfOriCata = webSysDir + "/catImage";
	HashMap Display = new HashMap();
	Display.put("user", get_userSkin(d, v));
	if(!Display.get("user").equals("")) set_userSkin((String)Display.get("user"), Display);
	set_newintitle(r, d, webSysDir+"/skin5", Display);

	String newin_titletext = "인 쇄";
	String svgObj = "printSvg";
	String plusID = "";
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>ecatalog - print001</title>
<style>
	html, body{width:100%;height:100%;margin:0;overflow:hidden;}

	*{font-family:굴림;font-size:12px;}
	input{vertical-align:bottom;}
	a{text-decoration:none;}

<% if(n.equals("016")){ %>
	body{background-color:#ffffff;}
<% } else{ %>
	body{background-color:#f7f7f7;}
<% } %>

	#newin_bodybtn{z-index:4;display:block;position:absolute;cursor:pointer;}
	.newin_bodybtn001{left:151px;top:246px;}
	.newin_bodyclosebtn012{right:10px;top:239px;}
	.newin_bodybtn016, .newin_bodybtn018{left:152px;top:245px;}
	.newin_bodybtn041{left:165px;top:140px;}

	.prn_svgback{z-index:2;display:block;position:absolute;width:280px;height:100%;}
	.prn_svgback001, .prn_svgback012, .prn_svgback018{left:6px;top:6px;}
	.prn_svgback016{left:11px;top:2px;}
	.prn_svgback041{left:6px;top:6px;}
	.prn_svgback_rect{fill:none;stroke:#666666;stroke-opacity:1;}

	.prn_secprint{z-index:4;display:block;position:absolute;}
	.prn_secprint001, .prn_secprint012, .prn_secprint016{left:6px;top:6px;}
	.prn_secprint018{left:10px;top:6px;}
	.prn_secprint > h1{display:block;position:relative;margin:5px 0 0 10px;padding:0;font-weight:normal;}
	.prn_secprint > p:nth-of-type(1){display:block;position:relative;margin:5px 0 0 50px;padding:0;}
	.prn_secprint > p:nth-of-type(2){display:block;position:relative;margin:5px 0 0 70px;padding:0;}

	.prn_partprint{z-index:4;display:block;position:absolute;left:6px;top:100px;}
	.prn_partprint > h1{display:block;position:relative;margin:16px 0 0 10px;padding:0;font-weight:normal;}
	.prn_partprint > p:nth-of-type(1){display:block;position:relative;margin:5px 0 0 30px;padding:0;}

	#frpage{display:inline;border:1px solid #000000;width:32px;height:14px;}
	#topage{display:inline;border:1px solid #000000;width:32px;height:14px;margin-left:10px;}

	.prn_button{z-index:5;display:block;width:81px;height:21px;border:1px solid #666666;
		text-align:center;cursor:pointer;background-color:#e5e5e5;color:#444444;}
	.prn_button:hover{background-color:#FCFBBC;}
	.prn_button_secprint{margin:10px 0 0 170px;}
	.prn_button_secprint:active{margin:11px 0 0 171px;}
	.prn_button_partprint{margin:4px 0 0 170px;}
	.prn_button_partprint:active{margin:5px 0 0 171px;}

<% if(v.equals("m")){ %>
	.prn_newincheck{z-index:5;display:block;position:absolute;left:6px;top:120px;width:268px;height:23px;margin:0;}
	.prn_image{z-index:5;display:block;position:absolute;left:20px;top:36px;margin:0;width:26px;height:24px;}
	#prn_anisvg{z-index:5;display:block;position:absolute;left:20px;top:40px;margin:0;width:30px;height:30px;visibility:hidden;}
<% } else{ %>
	.prn_newincheck{z-index:5;display:block;position:absolute;left:6px;top:220px;width:268px;height:23px;margin:0;}
	.prn_image{z-index:5;display:block;position:absolute;left:20px;top:36px;margin:0;width:26px;height:24px;}
	#prn_anisvg{z-index:5;display:block;position:absolute;left:20px;top:40px;margin:0;width:30px;height:30px;visibility:hidden;}
<% } %>

</style>
<script>
var selfName = "print001.jsp", winCnt = "print";
var svgWidth = <%=w%>, svgHeight = <%=h%>;
var p;
var ws = "";
var dragGap = 0;

var prnaniObj = new Array();
var prnaniTimer;
var prnSeq = 0;

function onload_func(){
	p = window.parent;
	console.log(selfName+" : loaded");
	p.printSvg = this;
	window.parent.objLoaded2('print', this, false);
	dragGap = p.WinInfo.get_dragGap('<%=n%>');

	p.document.getElementById('newin_frame').innerHTML = ws;

	if(p.PrintInfo.newinPage === true) document.printform.newin.checked = true;
	if(p.SkinInfo.printkind === "999") encPrint();

	prnaniObj[0] = document.getElementById("prnanicc1");
	prnaniObj[1] = document.getElementById("prnanicc2");
	prnaniObj[2] = document.getElementById("prnanicc3");
	prnaniObj[3] = document.getElementById("prnanicc4");
	prnaniObj[4] = document.getElementById("prnanicc5");
	prnaniObj[5] = document.getElementById("prnanicc6");
	prnaniObj[6] = document.getElementById("prnanicc7");
	prnaniObj[7] = document.getElementById("prnanicc8");
}

function sectionPrint(){
	if(document.printform.frpage.value == ""){
		alert('인쇄하실 시작 페이지를 입력하여 주십시오.');
		document.printform.frpage.focus();
		return false;
	}
	if(document.printform.topage.value == ""){
		alert('인쇄하실 페이지를 입력하여 주십시오.');
		document.printform.topage.focus();
		return false;
	}

	var fr_page = p.PageInfo.get_realFromShowPage(parseInt(document.printform.frpage.value));
	var to_page = p.PageInfo.get_realFromShowPage(parseInt(document.printform.topage.value));
	if(fr_page > to_page){
		alert('인쇄구간의 시작과 끝을 정확하게 입력해주십시오.');
		return false;
	}

	p.PrintInfo.frPageNo = fr_page;
	p.PrintInfo.toPageNo = to_page;
	p.PrintInfo.partPrint = false;

	if(!prnaniTimer) start_ani();
	p.load_printpage();
	return false;
}
function encPrint(){
	if(p.Action.mainState === "alert") return;
	if(p.Action.drawCondition <= 0){
		alert('확대상태에서 인쇄하실 수 있습니다.');
		return;
	}

	var fr_page, to_page;
	if(p.Action.togetherEnlarge === true){
		fr_page = p.PageInfo.currentPage
		to_page = p.PageInfo.currentPage + 1;
	}
	else if(p.Action.drawCondition === 10){
		fr_page = p.PageInfo.currentPage;
		to_page = p.PageInfo.currentPage
	}
	else if(p.Action.drawCondition === 11){
		fr_page = p.PageInfo.currentPage + 1;
		to_page = p.PageInfo.currentPage + 1;
	}

	p.PrintInfo.frPageNo = fr_page;
	p.PrintInfo.toPageNo = to_page;
	p.PrintInfo.partPrint = false;

	if(!prnaniTimer) start_ani();
	p.load_printpage();
}
function partPrint(){
	p.PrintInfo.partPrint = true;
	p.load_partprint();
}
function change_newinOption(){
	p.PrintInfo.newinPage = document.printform.newin.checked ? true : false;	
}

function start_ani(){				// from [ecatalog5.ext]-onload_func2()
	document.getElementById('prn_anisvg').style.visibility = "visible";
	prnaniTimer = setInterval(spool_animate, 120);
}

function spool_animate(){
	var n = prnSeq % 8;
	for(var i = 0;i < 8;i++){
		if(i == n) prnaniObj[i].setAttribute("fill","#aaaaaa");
		else if(i == n-1) prnaniObj[i].setAttribute("fill","#bbbbbb");
		else if(i == n-2) prnaniObj[i].setAttribute("fill","#cccccc");
		else prnaniObj[i].setAttribute("fill","#dddddd");
	}
	prnSeq++;
}

/// ############################## window ############################## ///
function newin_close(){
	p.unload_printpage();
	p.unload_newin(winCnt);
	p.printSvg = undefined;
}
function do_mouseDrag(e){
	e.preventDefault();

	var posX = e.clientX - p.clickX + p.befX;
	var posY = e.clientY - p.clickY + p.befY;

	if(posX < p.cataRect.x) posX = p.cataRect.x;
	else if(posX > p.cataRect.right-svgWidth-dragGap) posX = p.cataRect.right - svgWidth - dragGap;

	if(posY < p.cataRect.y) posY = p.cataRect.y;
	else if(posY > p.cataRect.bottom-svgHeight-dragGap) posY = p.cataRect.bottom - svgHeight - dragGap;

	p.newinDiv.style.left = posX + "px";
	p.newinDiv.style.top = posY + "px";

	return true;
}
function do_mouseUp(e){
	p.befX += e.clientX - p.clickX;
	p.befY += e.clientY - p.clickY;
}
</script>
</head>

<body onload="onload_func()">

<%
	int fullscrbtn = 0;
	int closebtn = 1;
	String skind = v.equals("m") ? "" : "close";
%>
<%@ include file="inc_newin.jsp" %>

<!-- section print background -->
<svg version="1.1" class="prn_svgback prn_svgback<%=n%>" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g transform="translate(0,10)">
<clipPath id="titlemask1">
	<polygon points="0,0 7,0 7,15 63,15 63,0 268,0 268,91 0,91, 0,0" />
</clipPath>
<rect class="prn_svgback_rect" x="0.5" y="0.5" width="267" height="90" rx="5" clip-path="url(#titlemask1)" />
</g>

<% if(v.equals("d")){ %>
<!-- part print background -->
<g transform="translate(0,115)">
<clipPath id="titlemask2">
	<polygon points="0,0 7,0 7,15 63,15 63,0 268,0 268,91 0,91, 0,0" />
</clipPath>
<rect class="prn_svgback_rect" x="0.5" y="0.5" width="267" height="90" rx="5" clip-path="url(#titlemask2)" />
</g>
<% } %>
</svg>

<div class="prn_image"><img src="<%=webServer%>/skin5/etc/printico.png" width="26" height="24" alt="프린터"></div>

<!-- animation -->
<svg version="1.1" id="prn_anisvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g transform="translate(0,29)">
	<circle id="prnanicc1" cx="15" cy="3" r="3" fill="#dddddd" />
	<circle id="prnanicc2" cx="23" cy="6" r="3" fill="#dddddd" />
	<circle id="prnanicc3" cx="26" cy="14" r="3" fill="#dddddd" />
	<circle id="prnanicc4" cx="23" cy="22" r="3" fill="#dddddd" />
	<circle id="prnanicc5" cx="15" cy="26" r="3" fill="#dddddd" />
	<circle id="prnanicc6" cx="6" cy="23" r="3" fill="#dddddd" />
	<circle id="prnanicc7" cx="3" cy="15" r="3" fill="#dddddd" />
	<circle id="prnanicc8" cx="6" cy="6" r="3" fill="#dddddd" />
</g>
</svg>

<!-- section print input form -->
<form name="printform" method="get" action="javascript:void(0);" onsubmit="return sectionPrint();">
<section class="prn_secprint prn_secprint<%=n%>">
<h1>구간인쇄</h1>
<p>인쇄하실 페이지를 입력해주세요.</p>
<p><input type="text" id="frpage" name="frpage"> 부터 ~ <input type="text" id="topage" name="topage"> 까지</p>
<input type="submit" class="prn_button prn_button_secprint" value="인  쇄">
</section>

<div class="prn_newincheck">
<label><input type="checkbox" name="newin" value="1" onclick="change_newinOption();">새창에서 열기 (미리보기로 확인 가능)</label>
</div>

<% if(v.equals("d")){ %>
<!-- part print button -->
<section class="prn_partprint">
<h1>부분인쇄</h1>
<p>인쇄하실 영역을 아래 버튼을 클릭하신 후<br>마우스 드래그하여 선택해주십시오.<br>취소는 ESC 키를 눌러 주십시오.</p>
<input type="button" class="prn_button prn_button_partprint" value="선  택" onclick="partPrint();">
</section>
<% } %>
</form>

</body>
</html>

