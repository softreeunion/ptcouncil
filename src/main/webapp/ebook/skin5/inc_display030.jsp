<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%
	request.setCharacterEncoding("utf-8");

	String incView = request.getParameter("incview");
	String indexType = request.getParameter("indexType");
	String combokind = request.getParameter("combokind");
	String downFile = request.getParameter("downFile");
	String downFileName = request.getParameter("downFileName");
	String downexten = request.getParameter("downexten");
	String downtext = URLDecoder.decode(request.getParameter("downtext"),"utf-8");
	String mediaFile = request.getParameter("mediaFile");
	String webServer = request.getParameter("webServer");
	String printKindmc = request.getParameter("printKindmc");
	String dirName = URLDecoder.decode(request.getParameter("dirName"),"utf-8");
	String searchFile = request.getParameter("searchFile");
	String mokchaFile = request.getParameter("mokchaFile");
	String skinMedia = request.getParameter("skinMedia");
	String skinSpacer = request.getParameter("skinSpacer");
	String mailButtonHide = request.getParameter("mailButtonHide");
	String Dir = request.getParameter("Dir");
	String catimage = request.getParameter("catimage");
	String webpath = webServer + "/catImage" + catimage + "/" + Dir;
%>
<% if(incView.equals("d")){ %>
<!-- navigation -->
<nav id="mmsect" class="mm5dD030-idxR">
<ul class="mm_ulE001">
<li class="closebtn" class="closebtn5dD030"><a href="javascript:closebtnClick();" title="닫기"><img src="<%=webServer%>/skin5/mm030/closebtn.png" width="14" height="14" alt="닫기"
  onmouseover="this.src='<%=webServer%>/skin5/mm030/closebtn_over.png';" onmouseout="this.src='<%=webServer%>/skin5/mm030/closebtn.png'"></a></li>
<li class="fullscrbtn" class="fullscrbtn5dD030"><a href="javascript:fullscrbtnClick();" title="전체화면으로 보기"><img src="<%=webServer%>/skin5/mm030/fullscrbtn.png" width="14" height="14" alt="전체화면으로 보기"
  onmouseover="this.src='<%=webServer%>/skin5/mm030/fullscrbtn_over.png';" onmouseout="this.src='<%=webServer%>/skin5/mm030/fullscrbtn.png'"></a></li>
<li class="movebtn" class="movebtn5dD030"><img src="<%=webServer%>/skin5/mm030/move.png" width="14" height="64" alt="이동"
  onmouseover="this.src='<%=webServer%>/skin5/mm030/move_over.png';MenuObj.show_movepack();" onmouseout="this.src='<%=webServer%>/skin5/mm030/move.png'"></li>
<li class="findbtn" class="findbtn5dD030"><img src="<%=webServer%>/skin5/mm030/find.png" width="14" height="64" alt="이동"
  onmouseover="this.src='<%=webServer%>/skin5/mm030/find_over.png';MenuObj.show_findpack();" onmouseout="this.src='<%=webServer%>/skin5/mm030/find.png'"></li>
<li class="outputbtn" class="outputbtn5dD030"><img src="<%=webServer%>/skin5/mm030/output.png" width="14" height="64" alt="출력"
  onmouseover="this.src='<%=webServer%>/skin5/mm030/output_over.png';MenuObj.show_outputpack();" onmouseout="this.src='<%=webServer%>/skin5/mm030/output.png'"></li>
<li class="sharebtn" class="sharebtn5dD030"><img src="<%=webServer%>/skin5/mm030/share.png" width="14" height="64" alt="공유"
  onmouseover="this.src='<%=webServer%>/skin5/mm030/share_over.png';MenuObj.show_sharepack();" onmouseout="this.src='<%=webServer%>/skin5/mm030/share.png'"></li>
<li id="combosect" class="cbbox5dD030" data-width="98" data-align="left" data-direction="right"><img src="<%=webServer%>/skin5/mm030/link.png" width="14" height="64" alt="링크"
  onmouseover="this.src='<%=webServer%>/skin5/mm030/link_over.png';comboClick('over');" onmouseout="this.src='<%=webServer%>/skin5/mm030/link.png'"></li>
<li class="mediabtn" class="mediabtn5dD030">
<% if(mediaFile.equals("true") && !skinMedia.equals("000")){ %>
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14" height="14" onmouseover="MenuObj.show_mediapack();">
<!-- animation -->
<g transform="translate(2,4)">
	<rect id="mediarect1" x="0" y="0" width="2" height="1" fill="#FF9900" />
	<rect id="mediarect2" x="4" y="0" width="2" height="1" fill="#FF9900" />
	<rect id="mediarect3" x="8" y="0" width="2" height="1" fill="#FF9900" />
	<rect id="mediarect4" x="0" y="2" width="2" height="1" fill="#FF9900" />
	<rect id="mediarect5" x="4" y="2" width="2" height="1" fill="#FF9900" />
	<rect id="mediarect6" x="8" y="2" width="2" height="1" fill="#FF9900" />
	<rect id="mediarect7" x="0" y="4" width="2" height="1" fill="#FF9900" />
	<rect id="mediarect8" x="4" y="4" width="2" height="1" fill="#FF9900" />
	<rect id="mediarect9" x="8" y="4" width="2" height="1" fill="#FF9900" />
	<rect id="mediarect10" x="0" y="6" width="2" height="1" fill="#FF6600" />
	<rect id="mediarect11" x="4" y="6" width="2" height="1" fill="#FF6600" />
	<rect id="mediarect12" x="8" y="6" width="2" height="1" fill="#FF6600" />
	<rect id="mediarect13" x="0" y="8" width="2" height="1" fill="#FF0000" />
	<rect id="mediarect14" x="4" y="8" width="2" height="1" fill="#FF0000" />
	<rect id="mediarect15" x="8" y="8" width="2" height="1" fill="#FF0000" />
</g>
</svg>
<% } %>
</li>
<li class="helpbtn" class="helpbtn5dD030"><a href="javascript:helpbtnClick();" title="도움말 보기"><img src="<%=webServer%>/skin5/mm030/helpbtn.png" width="14" height="64" alt="도움말 보기"
  onmouseover="this.src='<%=webServer%>/skin5/mm030/helpbtn_over.png';" onmouseout="this.src='<%=webServer%>/skin5/mm030/helpbtn.png'"></a></li>
</ul>

<div id="movepack">
<ul class="mm_ulE002">
<li id="firstbtn" class="firstbtn5dD030"><a href="javascript:firstbtnClick();" title="처음 페이지로 이동"><img src="<%=webServer%>/skin5/mm030/firstbtn.png" width="33" height="33" alt="처음 페이지로 이동"
  onmouseover="this.src='<%=webServer%>/skin5/mm030/firstbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm030/firstbtn.png'"></a></li>
<li id="prevbtn" class="prevbtn5dD030"><a href="javascript:prevbtnClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/mm030/prevbtn.png" width="33" height="33" alt="이전 페이지로 이동"
  onmouseover="this.src='<%=webServer%>/skin5/mm030/prevbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm030/prevbtn.png'"></a></li>
<li id="slidebtn" class="slidebtn5dD030"><a href="javascript:slidebtnClick();" title="자동 넘김"><img src="<%=webServer%>/skin5/mm030/slidebtn.png" width="33" height="33" alt="자동 넘김"
  onmouseover="this.src='<%=webServer%>/skin5/mm030/slidebtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm030/slidebtn.png'"></a></li>
<li id="nextbtn" class="nextbtn5dD030"><a href="javascript:nextbtnClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/mm030/nextbtn.png" width="33" height="33" alt="다음 페이지로 이동"
  onmouseover="this.src='<%=webServer%>/skin5/mm030/nextbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm030/nextbtn.png'"></a></li>
<li id="lastbtn" class="lastbtn5dD030"><a href="javascript:lastbtnClick();" title="마지막 페이지로 이동"><img src="<%=webServer%>/skin5/mm030/lastbtn.png" width="33" height="33" alt="마지막 페이지로 이동"
  onmouseover="this.src='<%=webServer%>/skin5/mm030/lastbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm030/lastbtn.png'"></a></li>
</ul>
<div id="pageshow" class="pshow5dD030">
<form name="pageshowform" method="get" action="ecatalog.php" onsubmit="return MenuObj.do_pagenoFormSubmit();" title="페이지 이동">
<p class="pshow_p5dD030"><input type="text" name="pageno" value="" onfocus="MenuObj.do_pagenoFocusIn(event);" onblur="MenuObj.do_pagenoFocusOut(event)"
 pattern="[0-9-]+" title="숫자만 입력 가능합니다." required></p>
<p id="pshow_totalpage"></p>
</form>
</div>
</div>

<div id="findpack">
<ul class="mm_ulE002">
<li id="expbtn" class="expbtn5dD030"><a href="javascript:expbtnClick();" title="탐색기 보기"><img src="<%=webServer%>/skin5/mm030/expbtn.png" width="39" height="20" alt="탐색기 보기"
  onmouseover="this.src='<%=webServer%>/skin5/mm030/expbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm030/expbtn.png'"></a></li>
<li id="totalbtn" class="totalbtn5dD030"><a href="javascript:totalbtnClick();" title="전체목록 보기"><img src="<%=webServer%>/skin5/mm030/totalbtn.png" width="39" height="20" alt="전체목록 보기"
  onmouseover="this.src='<%=webServer%>/skin5/mm030/totalbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm030/totalbtn.png'"></a></li>
<li id="glassesbtn" class="glassesbtn5dD030"><a href="javascript:glassesbtnClick();" title="돋보기로 보기"><img src="<%=webServer%>/skin5/mm030/glassesbtn.png" width="39" height="20" alt="돋보기로 보기"
  onmouseover="this.src='<%=webServer%>/skin5/mm030/glassesbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm030/glassesbtn.png'"></a></li>
<li id="indexbtn" class="indexbtn5dD030"><a href="javascript:indexbtnClick();" title="목차 보기"><img src="<%=webServer%>/skin5/mm030/indexbtn.png" width="39" height="20" alt="목차 보기"
  onmouseover="this.src='<%=webServer%>/skin5/mm030/indexbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm030/indexbtn.png'"></a></li>
<li id="splbtn" class="splbtn5dD030"><a href="javascript:load_spacer('left',true);" title="책갈피 추가"><img src="<%=webServer%>/skin5/mm030/spacerbtnL.png" width="16" height="13" alt="책갈피 추가"
  onmouseover="this.src='<%=webServer%>/skin5/mm030/spacerbtnL_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm030/spacerbtnL.png'"></a></li>
<li id="sprbtn" class="sprbtn5dD030"><a href="javascript:load_spacer('right',true);" title="책갈피 추가"><img src="<%=webServer%>/skin5/mm030/spacerbtnR.png" width="16" height="13" alt="책갈피 추가"
  onmouseover="this.src='<%=webServer%>/skin5/mm030/spacerbtnR_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm030/spacerbtnR.png'"></a></li>
</ul>

<!-- spacer title -->
<div class="splrtitle"><img src="<%=webServer%>/skin5/mm030/spacerbtn.png" width="42" height="17" alt="책갈피"></div>

<!-- search menu -->
<% if(searchFile.equals("true")){ %>
<div id="searchsect" class="search5dD030">
<form name="searchform" method="get" action="#" onsubmit="return do_searchbtnClick(document.searchform.qtxt.value)">
<input type="search" class="searchtxt5dD030" name="qtxt" title="검색어 입력" onfocus="do_qtxtFocusIn(this)">
<input type="image" class="searchbtn5dD030 hover_opac8" src="<%=webServer%>/skin5/mm030/search_btn.png" alt="검색 시작">
</form>
</div>
<% } %>

</div>

<div id="outputpack">
<ul class="mm_ulE002">
<% if(!printKindmc.equals("000")){ %>
<li id="printbtn" class="printbtn5dD030"><a href="javascript:printbtnClick();" title="인쇄하기"><img src="<%=webServer%>/skin5/mm030/printbtn.png" width="52" height="23" alt="인쇄하기"></a></li>
<% } %>
</ul>

<!-- download -->
<% if(downFile.equals("true")){ %>
<div id="downsect" class="down5dD030">
<a href="<%=webpath%>/<%=downFileName%>" download="<%=webpath%>/<%=downFileName%>"
  title="파일 다운로드"><img src="<%=webServer%>/skin5/icon/file_<%=downexten%>.gif" class="down_img" alt="파일 다운로드"><%=downtext%></a>
</div>
<% } %>

</div>

<div id="sharepack">
<ul class="mm_ulE002">
<li id="mailbtn" class="mailbtn5dD030"><a href="javascript:mailbtnClick();" title="전자우편 발송"><img src="<%=webServer%>/skin5/mm030/mailbtn.png" width="60" height="23" alt="전자우편 발송"></a></li>
</ul>

<!-- sns -->
<nav id="snssect" class="sns5dD030">
<ul class="sns_ul5dD030">
<li id="facebookbtn" class="sns_liE010 hover_opac8"><a href="javascript:facebookbtnClick();" 
  title="페이스북 공유"><img src="<%=webServer%>/skin5/mm010/facebookbtn.png" class="sns_liE010" alt="페이스북 공유"></a></li>
<li id="twitterbtn" class="sns_liE010 hover_opac8"><a href="javascript:twitterbtnClick();" 
  title="트위터 공유"><img src="<%=webServer%>/skin5/mm010/twitterbtn.png" class="sns_liE010" alt="트위터 공유"></a></li>
</ul>
</nav>

</div>

<div id="mediapack">
<p class="mediaback5dD030" data-width="content" data-align="left" data-direction="top" data-hpos="0" data-vpos="-4"></p>
<ul class="media_ul5dD030">
	<li id="media_backbtn" class="media_backbtn5dD030"><a href="javascript:MediaObj.backbtnClick();" title="이전 곡"><img width="7" height="7" src="<%=webServer%>/skin5/mm010/media_prevbtn.png" alt="이전 곡"></a></li>
	<li id="media_playbtn" class="media_playbtn5dD030"><a href="javascript:MediaObj.playbtnClick();" title="재생"><img width="4" height="7" src="<%=webServer%>/skin5/mm010/media_nextbtn.png" alt="재생"></a></li>
	<li id="media_nextbtn" class="media_nextbtn5dD030"><a href="javascript:MediaObj.nextbtnClick();" title="다음 곡"><img width="7" height="7" src="<%=webServer%>/skin5/mm010/media_lastbtn.png" alt="다음 곡"></a></li>
	<li id="media_stopbtn" class="media_stopbtn5dD030"><a href="javascript:MediaObj.stopbtnClick();" title="멈춤"><img width="7" height="7" src="<%=webServer%>/skin5/mm030/media_pausebtn.png" alt="멈춤"></a></li>
	<li id="media_listbtn" class="media_listbtn5dD030"><a href="javascript:MediaObj.listbtnClick();" title="곡 목록"><img width="7" height="7" src="<%=webServer%>/skin5/mm030/media_listbtn.png" alt="곡 목록"></a></li>
</ul>
<audio id="mediactrl" class="audio_ctrlr_hidden">브라우저가 audio를 지원하지 않습니다.</audio>
</div>

</nav>

<script>
MenuObj = {storedStr:'', befX:0, gradLen:0, moveSeq:0, findSeq:0, outputSeq:0, shareSeq:0, mediaSeq:0,
	moveTimer:undefined, findTimer:undefined, outputTimer:undefined, shareTimer:undefined, mediaTimer:undefined,
	moveDiv:undefined, findDiv:undefined, outputDiv:undefined, shareDiv:undefined, mediaDiv:undefined};

MenuObj.onload_func2 = function(){
	MenuObj.moveDiv = document.getElementById("movepack");
	MenuObj.findDiv = document.getElementById("findpack");
	MenuObj.outputDiv = document.getElementById("outputpack");
	MenuObj.shareDiv = document.getElementById("sharepack");
	MenuObj.mediaDiv = document.getElementById("mediapack");
	MenuObj.gradLen = AniObj.gradualPercent.length;

	var n = (PageInfo.showStartPage == 0) ? PageInfo.cataPages : PageInfo.get_showPageFromReal(PageInfo.cataPages);
	document.getElementById("pshow_totalpage").innerHTML = n;

	if(LinkInfo.facebookStr === "" && document.getElementById("facebookbtn")) document.getElementById("facebookbtn").style.opacity = "0.2";
	if(LinkInfo.twitterStr === "" && document.getElementById("twitterbtn")) document.getElementById("twitterbtn").style.opacity = "0.2";
	if(MenuInfo.mailButtonHide === "Y" && document.getElementById("mailbtn")) document.getElementById("mailbtn").style.opacity = "0.2";
	if(FileInfo.idxFile === false && document.getElementById("indexbtn")) document.getElementById("indexbtn").style.opacity = "0.3";
	if(SkinInfo.combokind === "000" && document.getElementById("combosect")) document.getElementById("combosect").style.opacity = "0.5";
	if(SkinInfo.spacerbtn === "000"){
		if(document.getElementById("splbtn")) document.getElementById("splbtn").style.opacity = "0.2";
		if(document.getElementById("sprbtn")) document.getElementById("sprbtn").style.opacity = "0.2";
		if(document.getElementById("splrtitle")) document.getElementById("splrtitle").style.opacity = "0.2";
	}
}
MenuObj.show_pageNumber = function(){
	document.pageshowform.pageno.value = MoveInfo.get_showPageNumber();
}
MenuObj.onresize_func = function(){
}
MenuObj.show_movepack = function(){
	if(MenuObj.moveTimer) return;
	MenuObj.moveSeq = 0;
	MenuObj.moveTimer = setInterval(MenuObj.show_animateMove, 15);
}
MenuObj.show_animateMove = function(){
	var tx = Math.floor(-119*(1-AniObj.gradualPercent[MenuObj.moveSeq]));
	MenuObj.moveDiv.style.left = tx+"px";

	MenuObj.moveSeq++;
	if(MenuObj.moveSeq === MenuObj.gradLen){
		clearInterval(MenuObj.moveTimer);
		MenuObj.moveTimer = undefined;
		MenuObj.moveDiv.addEventListener("mouseout", MenuObj.do_mouseOutMove, true);
	}
}
MenuObj.do_mouseOutMove = function(e){
	var obj = e.relatedTarget;
	while(obj){
		if(obj == this) return;
		obj = obj.parentNode;
	}
	MenuObj.hide_movepack();
}
MenuObj.hide_movepack = function(){
	if(MenuObj.moveTimer) return;
	MenuObj.moveSeq = 0;
	MenuObj.moveTimer = setInterval(MenuObj.hide_animateMove, 15);
}
MenuObj.hide_animateMove = function(){
	var tx = Math.floor(-104*AniObj.gradualPercent[MenuObj.moveSeq]);
	MenuObj.moveDiv.style.left = tx+"px";

	MenuObj.moveSeq++;
	if(MenuObj.moveSeq === MenuObj.gradLen){
		clearInterval(MenuObj.moveTimer);
		MenuObj.moveTimer = undefined;
		MenuObj.moveDiv.removeEventListener("mouseout", MenuObj.do_mouseOutMove);
	}
}
// find
MenuObj.show_findpack = function(){
	if(MenuObj.findTimer) return;
	MenuObj.findSeq = 0;
	MenuObj.findTimer = setInterval(MenuObj.show_animateFind, 15);
}
MenuObj.show_animateFind = function(){
	var tx = Math.floor(-104*(1-AniObj.gradualPercent[MenuObj.findSeq]));
	MenuObj.findDiv.style.left = tx+"px";

	MenuObj.findSeq++;
	if(MenuObj.findSeq === MenuObj.gradLen){
		clearInterval(MenuObj.findTimer);
		MenuObj.findTimer = undefined;
		MenuObj.findDiv.addEventListener("mouseout", MenuObj.do_mouseOutFind, true);
	}
}
MenuObj.do_mouseOutFind = function(e){
	var obj = e.relatedTarget;
	while(obj){
		if(obj == this) return;
		obj = obj.parentNode;
	}
	MenuObj.hide_findpack();
}
MenuObj.hide_findpack = function(){
	if(MenuObj.findTimer) return;
	MenuObj.findSeq = 0;
	MenuObj.findTimer = setInterval(MenuObj.hide_animateFind, 15);
}
MenuObj.hide_animateFind = function(){
	var tx = Math.floor(-104*AniObj.gradualPercent[MenuObj.findSeq]);
	MenuObj.findDiv.style.left = tx+"px";

	MenuObj.findSeq++;
	if(MenuObj.findSeq === MenuObj.gradLen){
		clearInterval(MenuObj.findTimer);
		MenuObj.findTimer = undefined;
		MenuObj.findDiv.removeEventListener("mouseout", MenuObj.do_mouseOutFind);
	}
}
// output
MenuObj.show_outputpack = function(){
	if(MenuObj.outputTimer) return;
	MenuObj.outputSeq = 0;
	MenuObj.outputTimer = setInterval(MenuObj.show_animateOutput, 15);
}
MenuObj.show_animateOutput = function(){
	var tx = Math.floor(-104*(1-AniObj.gradualPercent[MenuObj.outputSeq]));
	MenuObj.outputDiv.style.left = tx+"px";

	MenuObj.outputSeq++;
	if(MenuObj.outputSeq === MenuObj.gradLen){
		clearInterval(MenuObj.outputTimer);
		MenuObj.outputTimer = undefined;
		MenuObj.outputDiv.addEventListener("mouseout", MenuObj.do_mouseOutPut, true);
	}
}
MenuObj.do_mouseOutPut = function(e){
	var obj = e.relatedTarget;
	while(obj){
		if(obj == this) return;
		obj = obj.parentNode;
	}
	MenuObj.hide_outputpack();
}
MenuObj.hide_outputpack = function(){
	if(MenuObj.outputTimer) return;
	MenuObj.outputSeq = 0;
	MenuObj.outputTimer = setInterval(MenuObj.hide_animateOutput, 15);
}
MenuObj.hide_animateOutput = function(){
	var tx = Math.floor(-104*AniObj.gradualPercent[MenuObj.outputSeq]);
	MenuObj.outputDiv.style.left = tx+"px";

	MenuObj.outputSeq++;
	if(MenuObj.outputSeq === MenuObj.gradLen){
		clearInterval(MenuObj.outputTimer);
		MenuObj.outputTimer = undefined;
		MenuObj.outputDiv.removeEventListener("mouseout", MenuObj.do_mouseOutPut);
	}
}
// share
MenuObj.show_sharepack = function(){
	if(MenuObj.shareTimer) return;
	MenuObj.shareSeq = 0;
	MenuObj.shareTimer = setInterval(MenuObj.show_animateShare, 15);
}
MenuObj.show_animateShare = function(){
	var tx = Math.floor(-104*(1-AniObj.gradualPercent[MenuObj.shareSeq]));
	MenuObj.shareDiv.style.left = tx+"px";

	MenuObj.shareSeq++;
	if(MenuObj.shareSeq === MenuObj.gradLen){
		clearInterval(MenuObj.shareTimer);
		MenuObj.shareTimer = undefined;
		MenuObj.shareDiv.addEventListener("mouseout", MenuObj.do_mouseOutShare, true);
	}
}
MenuObj.do_mouseOutShare = function(e){
	var obj = e.relatedTarget;
	while(obj){
		if(obj == this) return;
		obj = obj.parentNode;
	}
	MenuObj.hide_sharepack();
}
MenuObj.hide_sharepack = function(){
	if(MenuObj.shareTimer) return;
	MenuObj.shareSeq = 0;
	MenuObj.shareTimer = setInterval(MenuObj.hide_animateShare, 15);
}
MenuObj.hide_animateShare = function(){
	var tx = Math.floor(-104*AniObj.gradualPercent[MenuObj.shareSeq]);
	MenuObj.shareDiv.style.left = tx+"px";

	MenuObj.shareSeq++;
	if(MenuObj.shareSeq === MenuObj.gradLen){
		clearInterval(MenuObj.shareTimer);
		MenuObj.shareTimer = undefined;
		MenuObj.shareDiv.removeEventListener("mouseout", MenuObj.do_mouseOutShare);
	}
}
// media
MenuObj.show_mediapack = function(){
	if(MenuObj.mediaTimer) return;
	MenuObj.mediaSeq = 0;
	MenuObj.mediaTimer = setInterval(MenuObj.show_animateMedia, 15);
}
MenuObj.show_animateMedia = function(){
	var tx = Math.floor(-104*(1-AniObj.gradualPercent[MenuObj.mediaSeq]));
	MenuObj.mediaDiv.style.left = tx+"px";

	MenuObj.mediaSeq++;
	if(MenuObj.mediaSeq === MenuObj.gradLen){
		clearInterval(MenuObj.mediaTimer);
		MenuObj.mediaTimer = undefined;
		MenuObj.mediaDiv.addEventListener("mouseout", MenuObj.do_mouseOutMedia, true);
	}
}
MenuObj.do_mouseOutMedia = function(e){
	var obj = e.relatedTarget;
	while(obj){
		if(obj == this) return;
		obj = obj.parentNode;
	}
	MenuObj.hide_mediapack();
}
MenuObj.hide_mediapack = function(){
	if(MenuObj.mediaTimer) return;
	MenuObj.mediaSeq = 0;
	MenuObj.mediaTimer = setInterval(MenuObj.hide_animateMedia, 15);
}
MenuObj.hide_animateMedia = function(){
	var tx = Math.floor(-104*AniObj.gradualPercent[MenuObj.mediaSeq]);
	MenuObj.mediaDiv.style.left = tx+"px";

	MenuObj.mediaSeq++;
	if(MenuObj.mediaSeq === MenuObj.gradLen){
		clearInterval(MenuObj.mediaTimer);
		MenuObj.mediaTimer = undefined;
		MenuObj.mediaDiv.removeEventListener("mouseout", MenuObj.do_mouseOutMedia);
	}
}

if(document.getElementById("combosect")) comboDiv = document.getElementById("combosect");
if(document.getElementById("mediasect")){
	mediaDiv = document.getElementById("mediasect");
	mediaPlayer = document.getElementById("mediactrl");
}
</script>

<% } else if(incView.equals("m")){ %>
<nav id="mmsect" class="mm5mD030">
	<p><a href="javascript:MenuObj.show_movepack();" title="메뉴 보기">메 뉴</a></p>
<% if(!combokind.equals("000")){ %>
	<p><a href="javascript:combobtnClick();" title="다른 카탈로그 이동 페이지">이 동</a></p>
<% } %>
</nav>

<div id="mobilepack" ontouchstart="MenuObj.do_touchStartMove(event);" ontouchmove="Action.do_mouseDrag(event)" ontouchend="MenuObj.do_touchEnd()">

<div id="pageshow" class="pshow5mD030">
<form name="pageshowform" method="get" action="ecatalog.php" onsubmit="return MenuObj.do_pagenoFormSubmit();" title="페이지 이동">
<p class="pshow_p5mD030"><input type="text" name="pageno" value="" onfocus="MenuObj.do_pagenoFocusIn(event);" onblur="MenuObj.do_pagenoFocusOut(event)"
pattern="[0-9-]+" title="숫자만 입력 가능합니다." required></p>
<p id="pshow_totalpage"></p>
</form>
</div>

<ul class="mm_ulE003">
<li id="firstbtn" class="firstbtn5mD030"><a href="javascript:firstbtnClick();" title="처음 페이지로 이동"><img src="<%=webServer%>/skin5/mm030/m_firstbtn.png" width="33" height="33" alt="처음 페이지"></a></li>
<li id="prevbtn" class="prevbtn5mD030"><a href="javascript:prevbtnClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/mm030/m_prevbtn.png" width="33" height="33" alt="이전 페이지"></a></li>
<li id="slidebtn" class="slidebtn5mD030"><a href="javascript:slidebtnClick();" title="자동 넘김"><img src="<%=webServer%>/skin5/mm030/m_slidebtn.png" width="33" height="33" alt="자동 넘김"></a></li>
<li id="nextbtn" class="nextbtn5mD030"><a href="javascript:nextbtnClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/mm030/m_nextbtn.png" width="33" height="33" alt="다음 페이지"></a></li>
<li id="lastbtn" class="lastbtn5mD030"><a href="javascript:lastbtnClick();" title="마지막 페이지로 이동"><img src="<%=webServer%>/skin5/mm030/m_lastbtn.png" width="33" height="33" alt="마지막 페이지"></a></li>
</ul>

<ul class="mm_ulE004">
<li id="expbtn" class="expbtn5mD030"><a href="javascript:expbtnClick();" title="탐색기 보기"><img src="<%=webServer%>/skin5/mm030/m_expbtn.png" width="39" height="20" alt="탐색기"></a></li>
<li id="indexbtn" class="indexbtn5mD030"><a href="javascript:indexbtnClick();" title="목차 보기"><img src="<%=webServer%>/skin5/mm030/m_indexbtn.png" width="39" height="20" alt="목차"></a></li>
</ul>

<% if(searchFile.equals("true")){ %>
<div id="searchsect" class="search5mD030">
<form name="searchform" method="get" action="#" onsubmit="return do_searchbtnClick(document.searchform.qtxt.value)">
<input type="search" class="searchfnt030 searchtxt5mD030" name="qtxt" title="검색어 입력" onfocus="do_qtxtFocusIn(this)">
<input type="image" class="searchbtn5mD030" src="<%=webServer%>/skin5/mm030/m_searchbtn.png" alt="검색 시작">
</form>
</div>
<% } %>

<ul class="mm_ulE005">
<li id="spacerbtn" class="spacerbtn5mD030"><a href="javascript:spacerbtnClick('left');" title="북마크"><img src="<%=webServer%>/skin5/mm030/m_spacerbtn.png" width="20" height="27" alt="북마크"><p>책갈피</p></a></li>
<li id="printbtn" class="printbtn5mD030"><a href="javascript:printbtnClick();" title="인쇄하기"><img src="<%=webServer%>/skin5/mm030/m_printbtn.png" width="27" height="27" alt="인쇄하기"><p>인 쇄</p></a></li>
<li id="mailbtn" class="mailbtn5mD030"><a href="javascript:mailbtnClick();" title="전자우편 발송"><img src="<%=webServer%>/skin5/mm030/m_mailbtn.png" width="30" height="25" alt="전자우편 발송"><p>메 일</p></a></li>
<li id="helpbtn" class="helpbtn5mD030"><a href="javascript:helpbtnClick();" title="도움말 보기"><img src="<%=webServer%>/skin5/mm030/m_helpbtn.png" width="27" height="27" alt="도움말 보기"><p>도움말</p></a></li>
<% if(downFile.equals("true")){ %>
<li id="downbtn" class="downbtn5mD030"><a href="<%=webpath%>/<%=downFileName%>" download="<%=webpath%>/<%=downFileName%>" 
title="파일 다운로드"><img src="<%=webServer%>/skin5/mm030/m_downbtn.png" width="26" height="25" alt="파일 다운로드"><p>다운로드</p></a></li>
<% } %>
</ul>

<ul class="mm_ulE006">
<li id="facebookbtn" class="facebookbtn5mD030"><a href="javascript:facebookbtnClick();" title="페이스북 공유"><img src="<%=webServer%>/skin5/mm001/m_facebookbtn.png"
  width="25" height="22" alt="페이스북 공유"></a></li>
<li id="twitterbtn" class="twitterbtn5mD030"><a href="javascript:twitterbtnClick();" title="트위터 공유"><img src="<%=webServer%>/skin5/mm001/m_twitterbtn.png"
  width="25" height="22" alt="트위터 공유"></a></li>
<li id="kakaobtn" class="kakaobtn5mD030"><a href="javascript:kakaobtnClick();" title="카카오 공유"><img src="<%=webServer%>/skin5/mm001/m_kakaotalkbtn.png"
  width="25" height="22" alt="카카오 공유"></li>
</ul>

<% if(mediaFile.equals("true") && !skinMedia.equals("000")){ %>
<div id="mediasect" class="media5mD030">
<svg version="1.1" class="media_svg5mD030" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g>
	<rect id="mediarect1" x="0" y="0" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect2" x="6" y="0" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect3" x="12" y="0" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect4" x="0" y="3" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect5" x="6" y="3" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect6" x="12" y="3" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect7" x="0" y="6" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect8" x="6" y="6" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect9" x="12" y="6" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect10" x="0" y="9" width="4" height="2" fill="#74B800" />
	<rect id="mediarect11" x="6" y="9" width="4" height="2" fill="#74B800" />
	<rect id="mediarect12" x="12" y="9" width="4" height="2" fill="#74B800" />
	<rect id="mediarect13" x="0" y="12" width="4" height="2" fill="#748A00" />
	<rect id="mediarect14" x="6" y="12" width="4" height="2" fill="#748A00" />
	<rect id="mediarect15" x="12" y="12" width="4" height="2" fill="#748A00" />
</g>
</svg>

<ul class="media_ul5mD030">
<li id="media_playbtn" class="media_playbtn5mD030"><a href="javascript:MediaObj.backbtnClick();" title="재생">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10" height="13">
	<polygon points="0,0 10,7 0,13" fill="#1378aa" />
</svg></a>
</li>
<li id="media_listbtn" class="media_listbtn5mD030"><a href="javascript:MediaObj.playbtnClick();" title="재생목록">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13" height="13">
	<polygon points="7,0 13,8 0,8" fill="#1378aa" />
	<line x1="0" y1="11.5" x2="13" y2="11.5" stroke="#1378aa" stroke-width="3" />
</svg></a>
</li>
<li id="media_stopbtn" class="media_stopbtn5mD030"><a href="javascript:MediaObj.nextbtnClick();" title="멈춤">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="9" height="9">
	<polygon points="0,0 9,0 9,9 0,9" fill="#1378aa" />
</svg></a>
</li>
<li id="media_backbtn" class="media_backbtn5mD030"><a href="javascript:MediaObj.stopbtnClick();" title="이전 곡">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13" height="13">
	<polygon points="5,7 13,13 13,0" fill="#1378aa" />
	<line x1="0.5" y1="0" x2="0.5" y2="13" stroke="#1378aa" stroke-width="5" />
</svg></a>
</li>
<li id="media_nextbtn" class="media_nextbtn5mD030"><a href="javascript:MediaObj.listbtnClick();" title="다음 곡">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13" height="13">
	<polygon points="0,0 8,7 0,13" fill="#1378aa" />
	<line x1="12.5" y1="0" x2="12.5" y2="13" stroke="#1378aa" stroke-width="5" />
</svg></a>
</li>
</ul>
<audio id="autoctrl">브라우저가 audio를 지원하지 않습니다.</audio>
</div>
<% } %>

</div>

<script>
MenuObj = {storedStr:'', befX:0, gradLen:0,	mheight:0, moveTimer:undefined, moveSeq:0,	moveDiv:undefined};

MenuObj.onload_func2 = function(){
	if(LinkInfo.facebookStr === "" && document.getElementById("facebookbtn")) document.getElementById("facebookbtn").style.opacity = "0.2";
	if(LinkInfo.twitterStr === "" && document.getElementById("twitterbtn")) document.getElementById("twitterbtn").style.opacity = "0.2";
	if(LinkInfo.kakaoStr === "" && document.getElementById("kakaobtn")) document.getElementById("kakaobtn").style.opacity = "0.2";
	if(MenuInfo.mailButtonHide === "Y" && document.getElementById("mailbtn")) document.getElementById("mailbtn").style.opacity = "0.2";
	if(FileInfo.idxFile === false && document.getElementById("indexbtn")) document.getElementById("indexbtn").style.opacity = "0.3";
	if(SkinInfo.spacerbtn === "000" && document.getElementById("spacerbtn")) document.getElementById("spacerbtn").style.opacity = "0.3";
	if(SkinInfo.printkind === "000" && document.getElementById("printbtn")) document.getElementById("printbtn").style.opacity = "0.3";

	MenuObj.moveDiv = document.getElementById("mobilepack");
	MenuObj.gradLen = AniObj.normalPercent.length;

	var mh = 465;
	MenuObj.mheight = mh;

	if(!searchDiv) mh -= 32;
	if(!mediaDiv) mh -= 56;

	MenuObj.moveDiv.style.height = (mh > winRect.height-36) ? (winRect.height-36)+"px" : mh+"px";

	var n = (PageInfo.showStartPage == 0) ? PageInfo.cataPages : PageInfo.get_showPageFromReal(PageInfo.cataPages);
	document.getElementById("pshow_totalpage").innerHTML = n;
}
MenuObj.show_pageNumber = function(){
	document.pageshowform.pageno.value = MoveInfo.get_showPageNumber();
}
MenuObj.onresize_func = function(){
	MenuObj.moveDiv.style.height = (MenuObj.mheight > winRect.height-36) ? (winRect.height-36)+"px" : MenuObj.mheight+"px";
}
MenuObj.show_movepack = function(){
	if(MenuObj.moveTimer) return;
	if(MenuObj.moveDiv.style.left == "0px"){
		MenuObj.hide_movepack();
		return;
	}
	MenuObj.moveSeq = 0;
	MenuObj.moveTimer = setInterval(MenuObj.show_animateMove, 15);
}
MenuObj.show_animateMove = function(){
	var tx = Math.floor(-106*(1-AniObj.normalPercent[MenuObj.moveSeq]));
	MenuObj.moveDiv.style.left = tx+"px";

	MenuObj.moveSeq++;
	if(MenuObj.moveSeq === MenuObj.gradLen){
		clearInterval(MenuObj.moveTimer);
		MenuObj.moveTimer = undefined;
		MenuObj.moveDiv.addEventListener("mouseout", MenuObj.do_mouseOutMove, true);
	}
}
MenuObj.do_mouseOutMove = function(e){
	var obj = e.relatedTarget;
	while(obj){
		if(obj == this) return;
		obj = obj.parentNode;
	}
	MenuObj.hide_movepack();
}
MenuObj.hide_movepack = function(){
	if(MenuObj.moveTimer) return;
	MenuObj.moveSeq = 0;
	MenuObj.moveTimer = setInterval(MenuObj.hide_animateMove, 15);
}
MenuObj.hide_animateMove = function(){
	var tx = Math.floor(-106*AniObj.normalPercent[MenuObj.moveSeq]);
	MenuObj.moveDiv.style.left = tx+"px";

	MenuObj.moveSeq++;
	if(MenuObj.moveSeq === MenuObj.gradLen){
		clearInterval(MenuObj.moveTimer);
		MenuObj.moveTimer = undefined;
		MenuObj.moveDiv.removeEventListener("mouseout", MenuObj.do_mouseOutMove);
	}
}
MenuObj.do_touchStartMove = function(e){
	clickX = e.touches[0].clientX;
	clickY = e.touches[0].clientY;

	var now = new Date();
	Action.touchStartTime = now.getTime();
	Action.mouseDragClip = MenuObj;
}
MenuObj.do_mouseDrag = function(e){
	e.preventDefault();
	MenuObj.befX = e.touches[0].clientX;
}
MenuObj.do_touchEnd = function(){
	var now = new Date();
	var diffTime = now.getTime() - Action.touchStartTime;

	if(TouchInfo.get_flicked(diffTime) === true && MenuObj.befX < clickX){
		MenuObj.hide_movepack();
	}
}
if(document.getElementById("searchsect")) searchDiv = document.getElementById("searchsect");
if(document.getElementById("mediasect")){
	mediaDiv = document.getElementById("mediasect");
	mediaPlayer = document.getElementById("mediactrl");
}

OnsmObj = {};
OnsmObj.set_location = function(){
}
OnsmObj.set_visible = function(b1, b2){
}
</script>
<% } %>
