<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%
	request.setCharacterEncoding("utf-8");

	String incView = request.getParameter("incview");
	String indexType = request.getParameter("indexType");
	String combokind = request.getParameter("combokind");
	String downFile = request.getParameter("downFile");
	String downFileName = request.getParameter("downFileName");
	String downexten = request.getParameter("downexten");
	String downtext = URLDecoder.decode(request.getParameter("downtext"),"utf-8");
	String mediaFile = request.getParameter("mediaFile");
	String webServer = request.getParameter("webServer");
	String printKindmc = request.getParameter("printKindmc");
	String dirName = URLDecoder.decode(request.getParameter("dirName"),"utf-8");
	String searchFile = request.getParameter("searchFile");
	String skinMedia = request.getParameter("skinMedia");
	String Dir = request.getParameter("Dir");
	String catimage = request.getParameter("catimage");
	String webpath = webServer + "/catImage" + catimage + "/" + Dir;
%>
<% if(incView.equals("d")){ %>
<!-- navigation -->
<nav id="mmsect" class="mm5dD302-idxR">
<ul class="mm_ulE001">
<li id="closebtn" class="closebtn5dD302"><a href="javascript:closebtnClick();" title="창닫기"><img src="<%=webServer%>/skin5/mm302/closebtn.png" width="43" height="18" alt="창닫기"
  onmouseover="this.src='<%=webServer%>/skin5/mm302/closebtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm302/closebtn.png'"></a></li>
<li id="fullscrbtn" class="fullscrbtn5dD302"><a href="javascript:fullscrbtnClick();" title="전체화면으로 보기"><img src="<%=webServer%>/skin5/mm302/fullscrbtn.png" width="66" height="23" alt="전체화면"
  onmouseover="this.src='<%=webServer%>/skin5/mm302/fullscrbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm302/fullscrbtn.png'"></a></li>
<li id="slidebtn" class="slidebtn5dD302"><a href="javascript:slidebtnClick();" title="자동 넘김"><img src="<%=webServer%>/skin5/mm302/slidebtn.png" width="66" height="23" alt="자동 넘김"
  onmouseover="this.src='<%=webServer%>/skin5/mm302/slidebtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm302/slidebtn.png'"></a></li>
</ul>
</nav>

<div id="mmleft" class="mmleft5dD302"></div>
<div id="mmright" class="mmright5dD302"></div>

<div id="lowersect" class="lower5dD302">

<div id="pageshow" class="pshow5dD302">
<div id="mmebookname"><%=dirName%></div>
<div class="pshow_form5dD302">
	<form name="pageshowform" method="get" action="ecatalog.php" onsubmit="return MenuObj.do_pagenoFormSubmit();">
	<input type="text" name="pageno" value="" onfocus="MenuObj.do_pagenoFocusIn(event);" onblur="MenuObj.do_pagenoFocusOut(event)"
	pattern="[0-9-]+" title="숫자만 입력 가능합니다." required>
	</form>
</div>
<div id="pshow_bundlepage"></div>
<div id="pshow_totalpage"></div>
</div>

<!-- thumb preview -->
<div id="thumbsect" class="thumb5dD302"></div>

<div id="pageslider" class="pageslider5dD302">
	<div id="pslider_linebar"><img src="<%=webServer%>/skin5/mm302/dragbar.png" width="601" height="5"></div>
	<div id="pslider_btn"><img src="<%=webServer%>/skin5/mm302/dragbutton.png" width="25" height="11" alt="슬라이더 버튼" 
	  onmousedown="this.src='<%=webServer%>/skin5/mm302/dragbutton_down.png';MenuObj.do_mouseDown(event);" onmousemove="Action.do_mouseDrag(event)"
	  onmouseover="this.src='<%=webServer%>/skin5/mm302/dragbutton_over.png';MenuObj.do_psbtnOver(event);" onmouseout="this.src='<%=webServer%>/skin5/mm302/dragbutton.png';MenuObj.do_psbtnOut(event);"></div>
</div>
<p id="pslider_pshow" class="pslider_pshow5dD302"></p>

</div>

<script>
MenuObj = {storedStr:'', linebarobj:undefined, btnobj:undefined, pageshowobj:undefined,	cntRect:undefined,
	dragWidth:0, dragX:0, btnX:0, befX:0, thumbX:20, thumbMargin:0, thobjWidth:0, thobjHeight:0, pc:undefined};

thumbDiv = document.getElementById("thumbsect");

MenuObj.onload_func2 = function(){
	MenuObj.linebarobj = document.getElementById("pslider_linebar");
	MenuObj.btnobj = document.getElementById("pslider_btn");
	MenuObj.pageshowobj = document.getElementById("pslider_pshow");

	MenuObj.dragWidth = 576;		// 362 = 380 - 18(mmpsBtn.width)
	MenuObj.linebarobj.style.width = "586px";		// 4 + 372 + 4 = 380;

	MenuObj.cntRect = document.getElementById("pageslider").getBoundingClientRect();
	MenuObj.set_btnxFromPageno(PageInfo.currentPage);

	MenuObj.thobjWidth = stageWidth;
	MenuObj.thobjHeight = 70;
	thumbDiv.innerHTML = (FileInfo.embedTag === "iframe") ? "<iframe src='<%=webServer%>/skin5/thumbpage001.html'></iframe>\n"
		: "<object data='<%=webServer%>/skin5/thumbpage001.html' type='text/html'></object>\n";
}
MenuObj.show_pageNumber = function(){
	document.pageshowform.pageno.value = MoveInfo.get_showPageNumber();
	MenuObj.set_btnxFromPageno(PageInfo.currentPage);
}
MenuObj.onresize_func = function(){
}
MenuObj.set_btnxFromPageno = function(n){
	var totalpages = PageInfo.cataPages - 1;
	MenuObj.btnX = MenuObj.dragX + ((n-1)/totalpages * MenuObj.dragWidth);

	MenuObj.btnobj.style.left = MenuObj.btnX + "px";
	MenuObj.pageshowobj.style.left = (MenuObj.cntRect.left + MenuObj.btnX - 48) + "px";
	MenuObj.show_pageno();
}

MenuObj.get_pagenoFromBtnx = function(){
	var totalpages = PageInfo.cataPages - 1;
	var currpage = Math.round(totalpages * parseInt(MenuObj.btnobj.style.left.replace("px","") - MenuObj.dragX) / MenuObj.dragWidth);
	return (currpage+1);
}
MenuObj.show_pageno = function(){
	var n = MenuObj.get_pagenoFromBtnx();
	var currPage = PageInfo.get_directPageNo(n);
	MenuObj.pageshowobj.innerHTML = PageInfo.get_pageshowString(currPage) + " / " + PageInfo.cataPages;
}

/// ############################## event ############################## ///
MenuObj.do_mouseDown = function(e){
	e.preventDefault();
	clickX = e.clientX;
	clickY = e.clientY;
	Action.mouseDragClip = MenuObj;
}
MenuObj.do_mouseDrag = function(e){
	e.preventDefault();
	MenuObj.befX = e.clientX;
	var tx = MenuObj.btnX + MenuObj.befX - clickX;
	if(tx < 0) tx = 0;
	else if(tx > MenuObj.dragX+MenuObj.dragWidth) tx = MenuObj.dragX + MenuObj.dragWidth;

	MenuObj.btnobj.style.left = tx + "px";
	MenuObj.pageshowobj.style.left = (MenuObj.cntRect.left + tx - 48) + "px";
	MenuObj.pageshowobj.style.visibility = "visible";

	MenuObj.show_pageno();
}
MenuObj.do_mouseUp = function(e){
	MenuObj.pageshowobj.style.visibility = "hidden";
	MenuObj.btnX += (MenuObj.befX - clickX);
	var n = MenuObj.get_pagenoFromBtnx();
	if(PermitMan.get_directGoPermNumber("pslider", n,"system") == false) return;
	go_general(n, "system");
}
MenuObj.do_psbtnOver = function(e){
	MenuObj.pageshowobj.style.visibility = "visible";
}
MenuObj.do_psbtnOut = function(e){
	MenuObj.pageshowobj.style.visibility = "hidden";
}
</script>

<% } else if(incView.equals("m")){ %>
<!-- navigation -->
<nav id="mmsect" class="mm5dD302-idxR">
<ul class="mm_ulE001">
<li id="slidebtn" class="slidebtn5mD302"><a href="javascript:slidebtnClick();" title="자동 넘김"><img src="<%=webServer%>/skin5/mm302/slidebtn.png" width="66" height="23" alt="자동 넘김"
  onmouseover="this.src='<%=webServer%>/skin5/mm302/slidebtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm302/slidebtn.png'"></a></li>
</ul>
</nav>

<div id="mmleft" class="mmleft5mD302"></div>
<div id="mmright" class="mmright5mD302"></div>

<div id="lowersect" class="lower5mD302">

<div id="pageshow" class="pshow5dD302">
<div id="mmebookname"><%=dirName%></div>
<div id="pshow_page"></div>
</div>

<!-- thumb preview -->
<div id="thumbsect" class="thumb5mD302"></div>

<div id="pageslider" class="pageslider5mD302">
	<div id="pslider_linebar"><img src="<%=webServer%>/skin5/mm302/dragbar.png" width="300" height="5"></div>
	<div id="pslider_btn"><img src="<%=webServer%>/skin5/mm302/dragbutton.png" width="25" height="15" alt="슬라이더 버튼" 
	  ontouchstart="this.src='<%=webServer%>/skin5/mm302/dragbutton_down.png';MenuObj.do_touchDown(event);" ontouchmove="Action.do_mouseDrag(event)"></div>
</div>
<p id="pslider_pshow" class="pslider_pshow5mD302"></p>

</div>


<script>
MenuObj = {storedStr:'', linebarobj:undefined, btnobj:undefined, pageshowobj:undefined,	cntRect:undefined,
	dragWidth:0, dragX:0, btnX:0, befX:0, thumbX:20, thumbMargin:0, thobjWidth:0, thobjHeight:0, pc:undefined};

thumbDiv = document.getElementById("thumbsect");

MenuObj.onload_func2 = function(){
	MenuObj.show_pageNumber();

	MenuObj.linebarobj = document.getElementById("pslider_linebar");
	MenuObj.btnobj = document.getElementById("pslider_btn");
	MenuObj.pageshowobj = document.getElementById("pslider_pshow");

	MenuObj.dragWidth = 275;		// 362 = 380 - 18(mmpsBtn.width)
	MenuObj.linebarobj.style.width = "286px";		// 4 + 372 + 4 = 380;

	MenuObj.cntRect = document.getElementById("pageslider").getBoundingClientRect();
	MenuObj.set_btnxFromPageno(PageInfo.currentPage);

	MenuObj.thobjWidth = stageWidth;
	MenuObj.thobjHeight = 70;
	thumbDiv.innerHTML = "<object data=\"<%=webServer%>/skin5/thumbpage001m.html\" type=\"text/html\"></object>\n";
}
MenuObj.onresize_func = function(){
	MenuObj.thobjWidth = stageWidth;
	thumbSvg.onresize_func(stageWidth);
}
MenuObj.show_pageNumber = function(){
	var n = (PageInfo.showStartPage == 0) ? PageInfo.cataPages : PageInfo.get_showPageFromReal(PageInfo.cataPages);
	document.getElementById("pshow_page").innerHTML = MoveInfo.get_showPageNumber() + " / " + n;
}

MenuObj.set_btnxFromPageno = function(n){
	var totalpages = PageInfo.cataPages - 1;
	MenuObj.btnX = MenuObj.dragX + ((n-1)/totalpages * MenuObj.dragWidth);

	MenuObj.btnobj.style.left = MenuObj.btnX + "px";
	MenuObj.pageshowobj.style.left = (MenuObj.cntRect.left + MenuObj.btnX - 48) + "px";
	MenuObj.show_pageno();
}

MenuObj.get_pagenoFromBtnx = function(){
	var totalpages = PageInfo.cataPages - 1;
	var currpage = Math.round(totalpages * parseInt(MenuObj.btnobj.style.left.replace("px","") - MenuObj.dragX) / MenuObj.dragWidth);
	return (currpage+1);
}
MenuObj.show_pageno = function(){
	var n = MenuObj.get_pagenoFromBtnx();
	var currPage = PageInfo.get_directPageNo(n);
	MenuObj.pageshowobj.innerHTML = PageInfo.get_pageshowString(currPage) + " / " + PageInfo.cataPages;
}

/// ############################## event ############################## ///
MenuObj.do_touchDown = function(e){
	e.preventDefault();
	clickX = e.touches[0].clientX;
	clickY = e.touches[0].clientY;
	Action.mouseDragClip = MenuObj;
}
MenuObj.do_mouseDrag = function(e){
	e.preventDefault();
	MenuObj.befX = e.touches[0].clientX;
	var tx = MenuObj.btnX + MenuObj.befX - clickX;
	if(tx < 0) tx = 0;
	else if(tx > MenuObj.dragX+MenuObj.dragWidth) tx = MenuObj.dragX + MenuObj.dragWidth;

	MenuObj.btnobj.style.left = tx + "px";
	MenuObj.pageshowobj.style.left = (MenuObj.cntRect.left + tx - 48) + "px";
	MenuObj.pageshowobj.style.visibility = "visible";

	MenuObj.show_pageno();
}
MenuObj.do_touchEnd = function(e){
	MenuObj.pageshowobj.style.visibility = "hidden";
	MenuObj.btnX += (MenuObj.befX - clickX);
	var n = MenuObj.get_pagenoFromBtnx();
	if(PermitMan.get_directGoPermNumber("pslider", n,"system") == false) return;
	go_general(n, "system");
}

OnsmObj = {};
OnsmObj.set_location = function(){
}
OnsmObj.set_visible = function(b1, b2){
}
</script>

<% } %>
