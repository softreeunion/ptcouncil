<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%
	request.setCharacterEncoding("utf-8");

	String incView = request.getParameter("incview");
	String indexType = request.getParameter("indexType");
	String combokind = request.getParameter("combokind");
	String downFile = request.getParameter("downFile");
	String downFileName = request.getParameter("downFileName");
	String downexten = request.getParameter("downexten");
	String downtext = URLDecoder.decode(request.getParameter("downtext"),"utf-8");
	String mediaFile = request.getParameter("mediaFile");
	String mokchaFile = request.getParameter("mokchaFile");
	String webServer = request.getParameter("webServer");
	String printKindmc = request.getParameter("printKindmc");
	String dirName = URLDecoder.decode(request.getParameter("dirName"),"utf-8");
	String searchFile = request.getParameter("searchFile");
	String skinMedia = request.getParameter("skinMedia");
	String skinSpacer = request.getParameter("skinSpacer");
	String mailButtonHide = request.getParameter("mailButtonHide");
	String Dir = request.getParameter("Dir");
	String catimage = request.getParameter("catimage");
	String webpath = webServer + "/catImage" + catimage + "/" + Dir;
%>
<% if(incView.equals("d")){ %>
<!-- background -->
<div id="backsect" class="back5d018"></div>

<!-- navigation -->
<nav id="lowersect" class="mm5dD018-idxR">
<ul class="mm_ulE002">
<% if(!printKindmc.equals("000")){ %>
<li id="printbtn" class="printbtn5dD018"><a href="javascript:printbtnClick();" title="인쇄하기">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="45" height="70" 
  onmouseover="MenuObj.printbtnOver();" onmouseout="MenuObj.printbtnOut();">
<defs>
<mask id="printbtn_mask">
	<rect x="6" y="30" width="33" height="38" style="stroke:none;fill:#ffffff" />
</mask>
</defs>
<image x="10" y="45" width="27" height="10" xlink:href="<%=webServer%>/skin5/mm018/printbtn_string.png" />
<image x="6" y="0" id="printbtn_paper1" width="33" height="18" xlink:href="<%=webServer%>/skin5/mm016/printbtn_paper1.png" />
<image x="0" y="18" width="45" height="19" xlink:href="<%=webServer%>/skin5/mm018/printbtn_body.png" />
<image x="33" y="21" width="8" height="8" xlink:href="<%=webServer%>/skin5/mm016/printbtn_lamp.png" />
<image x="33" y="21" id="printbtn_lampon" width="8" height="8" xlink:href="<%=webServer%>/skin5/mm016/printbtn_lampon.png" visibility="hidden" />
<image x="6" y="0" id="printbtn_paper2" width="33" height="38" xlink:href="<%=webServer%>/skin5/mm018/printbtn_paper2.png" mask="url(#printbtn_mask)" />
</svg></a>
</li>
<% } %>

<% if(mokchaFile.equals("true")){ %>
<li id="indexbtn" class="indexbtn5dD018"><a href="javascript:indexbtnClick();" title="목차 보기">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="45" height="70" 
  onmouseover="MenuObj.indexbtnOver();" onmouseout="MenuObj.indexbtnOut();">
<image x="13" y="45" width="19" height="10" xlink:href="<%=webServer%>/skin5/mm018/indexbtn_string.png" />
<image x="0" y="11" width="28" height="28" xlink:href="<%=webServer%>/skin5/mm016/indexbtn_book.png" />
<image x="7" y="5" id="indexbtn_glasses" width="38" height="38" xlink:href="<%=webServer%>/skin5/mm018/indexbtn_glasses.png" />
</svg></a>
</li>
<% } %>

<li id="helpbtn" class="helpbtn5dD018"><a href="javascript:helpbtnClick();" title="도움말 보기">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="41" height="70" 
  onmouseover="MenuObj.helpbtnOver();" onmouseout="MenuObj.helpbtnOut();">
<defs>
<mask id="helpbtn_mask">
	<rect x="0" y="13" width="41" height="24" style="stroke:none;fill:#ffffff" />
</mask>
</defs>
<image x="6" y="45" width="26" height="10" xlink:href="<%=webServer%>/skin5/mm018/helpbtn_string.png" />
<image x="0" y="5" width="41" height="8" xlink:href="<%=webServer%>/skin5/mm018/helpbtn_wintitle.png" />
<image x="0" y="13" id="helpbtn_winbody" width="41" height="24" xlink:href="<%=webServer%>/skin5/mm018/helpbtn_winbody.png" mask="url(#helpbtn_mask)" />
</svg></a>
</li>
<% if(!mailButtonHide.equals("Y")){ %>
<li id="mailbtn" class="mailbtn5dD018"><a href="javascript:mailbtnClick();" title="전자우편 발송">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="39" height="70">
<image x="8" y="45" width="18" height="10" xlink:href="<%=webServer%>/skin5/mm018/mailbtn_string.png" />
<image x="0" y="9" id="mailbtn_body" width="39" height="28" xlink:href="<%=webServer%>/skin5/mm018/mailbtn_body.png" />
</svg></a>
</li>
<% } %>
</ul>

<nav id="snssect" class="sns5dD018">
<ul class="sns_ul5dD018">
<li id="facebookbtn" class="sns_liE018"><a href="javascript:facebookbtnClick();" title="페이스북 공유"><img src="<%=webServer%>/skin5/mm018/facebookbtn.png" class="sns_liE018" alt="페이스북"
onmouseover="this.src='<%=webServer%>/skin5/mm018/facebookbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm018/facebookbtn.png'"></a></li>
<li id="twitterbtn" class="sns_liE018"><a href="javascript:twitterbtnClick();" title="트위터 공유"><img src="<%=webServer%>/skin5/mm018/twitterbtn.png" class="sns_liE018" alt="트위터"
onmouseover="this.src='<%=webServer%>/skin5/mm018/twitterbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm018/twitterbtn.png'"></a></li>
</ul>
</nav>

<ul class="mm_ulE001">
<li id="closebtn" class="fullscrbtn5dD018"><a href="javascript:closebtnClick();" title="창닫기"><img src="<%=webServer%>/skin5/mm018/closebtn.png" width="27" height="28" alt="창닫기"
onmouseover="this.src='<%=webServer%>/skin5/mm018/closebtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm018/closebtn.png'"></a></li>
<li id="fullscrbtn" class="closebtn5dD018"><a href="javascript:fullscrbtnClick();" title="전체화면으로 보기"><img src="<%=webServer%>/skin5/mm018/fullscrbtn.png" width="27" height="28" alt="전체화면"
onmouseover="this.src='<%=webServer%>/skin5/mm018/fullscrbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm018/fullscrbtn.png'"></a></li>
</ul>

<!-- combobox -->
<% if(!combokind.equals("000")){ %>
<a href="javascript:combo_mouseDown();"<div id="combosect" class="cbbox5dD018" title="다른 카탈로그로 이동"
data-width="display" data-align="left" data-direction="top" data-vpos="-3"><img src="<%=webServer%>/skin5/mm018/combo_box.png" width="120" height="28"
onmouseover="this.src='<%=webServer%>/skin5/mm018/combo_box_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm018/combo_box.png'" alt="다른 카탈로그"></div></a>
<% } %>

<!-- media -->
<% if(mediaFile.equals("true") && !skinMedia.equals("000")){ %>
<div id="mediasect" class="media5dD018" data-width="display" data-align="right" data-direction="top" data-hpos="1">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10" height="10">
<!-- animation -->
<g>
	<rect id="mediarect1" x="0" y="0" width="2" height="1" fill="#74CF00" />
	<rect id="mediarect2" x="4" y="0" width="2" height="1" fill="#74CF00" />
	<rect id="mediarect3" x="8" y="0" width="2" height="1" fill="#74CF00" />
	<rect id="mediarect4" x="0" y="2" width="2" height="1" fill="#74CF00" />
	<rect id="mediarect5" x="4" y="2" width="2" height="1" fill="#74CF00" />
	<rect id="mediarect6" x="8" y="2" width="2" height="1" fill="#74CF00" />
	<rect id="mediarect7" x="0" y="4" width="2" height="1" fill="#74CF00" />
	<rect id="mediarect8" x="4" y="4" width="2" height="1" fill="#74CF00" />
	<rect id="mediarect9" x="8" y="4" width="2" height="1" fill="#74CF00" />
	<rect id="mediarect10" x="0" y="6" width="2" height="1" fill="#74B800" />
	<rect id="mediarect11" x="4" y="6" width="2" height="1" fill="#74B800" />
	<rect id="mediarect12" x="8" y="6" width="2" height="1" fill="#74B800" />
	<rect id="mediarect13" x="0" y="8" width="2" height="1" fill="#748A00" />
	<rect id="mediarect14" x="4" y="8" width="2" height="1" fill="#748A00" />
	<rect id="mediarect15" x="8" y="8" width="2" height="1" fill="#748A00" />
</g>
</svg>

<ul class="media_ul5dD018">
<li id="media_backbtn" class="media_backbtn5dD018"><a href="javascript:MediaObj.backbtnClick();" title="이전 곡">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="8" height="8">
	<polygon points="3,4 8,8 8,0" fill="#D1DEE2" />
	<line x1="0.5" y1="0" x2="0.5" y2="8" stroke="#D1DEE2" stroke-width="3" />
</svg></a>
</li>
<li id="media_playbtn" class="media_playbtn5dD018"><a href="javascript:MediaObj.playbtnClick();" title="재생">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6" height="8">
	<polygon points="0,0 6,4 0,8" fill="#D1DEE2" />
</svg></a>
</li>
<li id="media_nextbtn" class="media_nextbtn5dD018"><a href="javascript:MediaObj.nextbtnClick();" title="다음 곡">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6" height="8">
	<polygon points="0,0 5,4 0,8" fill="#D1DEE2" />
	<line x1="7.5" y1="0" x2="7.5" y2="8" stroke="#D1DEE2" stroke-width="3" />
</svg></a>
</li>
<li id="media_stopbtn" class="media_stopbtn5dD018"><a href="javascript:MediaObj.stopbtnClick();" title="멈춤">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6" height="6">
	<polygon points="0,0 6,0 6,6 0,6" fill="#D1DEE2" />
</svg></a>
</li>
<li id="media_listbtn" class="media_listbtn5dD018"><a href="javascript:MediaObj.listbtnClick();" title="재생목록">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="8" height="9">
	<polygon points="4,0 8,5 0,5" fill="#D1DEE2" />
	<line x1="0" y1="7" x2="8" y2="7" stroke="#D1DEE2" stroke-width="2" />
</svg></a>
</li>
</ul>
<audio id="mediactrl" class="audio_ctrlr_hidden">브라우저가 audio를 지원하지 않습니다.</audio>
</div>
<% } %>

<% if(searchFile.equals("true")){ %>
<div id="searchsect" class="search5dD018">
<form name="searchform" method="get" action="#" onsubmit="return do_searchbtnClick(document.searchform.qtxt.value)" title="페이지 검색">
<input type="search" class="searchtxt5dD018" name="qtxt" title="검색어 입력" onfocus="do_qtxtFocusIn(this)">
<input type="image" class="searchbtn5dD018 hover_opac8" src="<%=webServer%>/skin5/mm018/search_btn.png" alt="검색 시작">
</form>
</div>
<% } %>

</nav>

<script>
MenuObj = {storedStr:'', seq:0, btnY:0, btnTimer:undefined, printLamp:undefined, printPaper1:undefined, printPaper2:undefined, indexGlass:undefined, helpBody:undefined};

backDiv = document.getElementById("backsect");
if(document.getElementById("combosect")) comboDiv = document.getElementById("combosect");
if(document.getElementById("searchsect")) searchDiv = document.getElementById("searchsect");
if(document.getElementById("mediasect")) mediaDiv = document.getElementById("mediasect");
if(document.getElementById("snssect")) snsDiv = document.getElementById("snssect");

MenuObj.onload_func2 = function(){
	if(LinkInfo.facebookStr === "") document.getElementById("facebookbtn").style.visibility = "hidden";
	if(LinkInfo.twitterStr === "") document.getElementById("twitterbtn").style.visibility = "hidden";

	// button animation
	if(document.getElementById("printbtn_lampon")) MenuObj.printLamp = document.getElementById("printbtn_lampon");
	if(document.getElementById("printbtn_paper1")) MenuObj.printPaper1 = document.getElementById("printbtn_paper1");
	if(document.getElementById("printbtn_paper2")) MenuObj.printPaper2 = document.getElementById("printbtn_paper2");
	if(document.getElementById("indexbtn_glasses")) MenuObj.indexGlass = document.getElementById("indexbtn_glasses");
	if(document.getElementById("helpbtn_winbody")) MenuObj.helpBody = document.getElementById("helpbtn_winbody");
}
MenuObj.remove_children = function(){
	var nodes = MenuObj.pagingGrpObj.childNodes;
	for(var i = nodes.length-1;i > 0;i--){
		MenuObj.pagingGrpObj.removeChild(nodes[i]);
	}
	MenuObj.arPagingObj.length = 0;
}
// button animation
MenuObj.printbtnOver = function(){
	MenuObj.btnY = 0;
	if(MenuObj.btnTimer == undefined) MenuObj.btnTimer = setInterval(MenuObj.printbtn_animate, 40);
}
MenuObj.printbtn_animate = function(){
	if(MenuObj.seq%10 == 0) MenuObj.printLamp.setAttribute("visibility", "visible");
	else if(MenuObj.seq%10 == 5) MenuObj.printLamp.setAttribute("visibility", "hidden");

	MenuObj.printPaper1.setAttribute("y", MenuObj.btnY);
	MenuObj.printPaper2.setAttribute("y", MenuObj.btnY);
	MenuObj.btnY++;

	MenuObj.seq++;
	if(MenuObj.btnY >= 18) MenuObj.btnY = 18;
}
MenuObj.printbtnOut = function(){
	clearInterval(MenuObj.btnTimer);
	MenuObj.btnTimer = undefined;
	MenuObj.printLamp.setAttribute("visibility", "hidden");
	MenuObj.printPaper1.setAttribute("y", 0);
	MenuObj.printPaper2.setAttribute("y", 0);
	MenuObj.seq = 0;
}

MenuObj.indexbtnOver = function(){
	if(MenuObj.seq == 0) MenuObj.seq = 20;
	if(MenuObj.btnTimer == undefined) MenuObj.btnTimer = setInterval(MenuObj.indexbtn_animate, 30);
}
MenuObj.indexbtn_animate = function(){
	var rad = (MenuObj.seq * 5) * Math.PI / 180;
	MenuObj.indexGlass.setAttribute("x", 2+5*Math.sin(rad));
	MenuObj.indexGlass.setAttribute("y", 5+5*Math.cos(rad));
	MenuObj.seq++;
}
MenuObj.indexbtnOut = function(){
	clearInterval(MenuObj.btnTimer);
	MenuObj.btnTimer = undefined;
}

MenuObj.helpbtnOver = function(){
	if(MenuObj.btnTimer == undefined) MenuObj.btnTimer = setInterval(MenuObj.helpbtn_animate, 30);
}
MenuObj.helpbtn_animate = function(){
	MenuObj.btnY = 13 - MenuObj.seq;
	if(MenuObj.btnY <= 0) MenuObj.btnY = 0;
	MenuObj.helpBody.setAttribute("y", MenuObj.btnY);
	MenuObj.seq++;
}
MenuObj.helpbtnOut = function(){
	clearInterval(MenuObj.btnTimer);
	MenuObj.btnTimer = undefined;
	MenuObj.seq = 0;
	MenuObj.helpBody.setAttribute("y", 13);
}
MenuObj.show_pageNumber = function(){
}
MenuObj.onresize_func = function(){
}
</script>

<% } else if(incView.equals("m")){ %>

<!-- background -->
<div id="backsect" class="back5m018"></div>

<!-- navigation -->
<nav id="mmsect" class="mm5mD018">
<ul class="mm_ulE003">
<li id="printbtn" class="printbtn5mD018"><a href="javascript:printbtnClick();" title="인쇄하기"><img src="<%=webServer%>/skin5/mm018/m_printbtn.png"
  width="36" height="35" alt="인쇄하기"><p>인 쇄</p></a></li>
<% if(indexType.equals("R")){ %>
<li id="indexbtn" class="indexbtn5mD018"><a href="javascript:indexbtnClick();" title="목차 보기"><img src="<%=webServer%>/skin5/mm018/m_indexbtn.png"
  width="37" height="35" alt="목차 보기"><p>목 차</p></a></li>
<% } %>
<li id="helpbtn" class="helpbtn5mD018"><a href="javascript:helpbtnClick();" title="도움말 보기"><img src="<%=webServer%>/skin5/mm018/m_helpbtn.png"
  width="41" height="32" alt="도움말 보기"><p>도움말</p></a></li>
<li id="mailbtn" class="mailbtn5mD018"><a href="javascript:mailbtnClick();" title="전자우편 발송"><img src="<%=webServer%>/skin5/mm018/m_mailbtn.png"
  width="39" height="28" alt="전자우편 발송"><p>메 일</p></a></li>
<li id="facebookbtn" class="facebookbtn5mD018"><a href="javascript:facebookbtnClick();" title="페이스북 공유"><img src="<%=webServer%>/skin5/mm018/m_facebookbtn.png"
  width="32" height="28" alt="페이스북 공유"><p>페이스북</p></a></li>
<li id="twitterbtn" class="twitterbtn5mD018"><a href="javascript:twitterbtnClick();" title="트위터 공유"><img src="<%=webServer%>/skin5/mm018/m_twitterbtn.png"
  width="32" height="28" alt="트위터 공유"><p>트위터</p></a></li>
<li id="kakaobtn" class="kakaobtn5mD018"><a href="javascript:kakaobtnClick();" title="카카오톡 공유"><img src="<%=webServer%>/skin5/mm018/m_kakaobtn.png"
  width="32" height="28" alt="카카오톡 공유"><p>카카오톡</p></a></li>
<li id="combobtn" class="combobtn5mD018"><a href="javascript:combobtnClick();" title="다른 카탈로그 이동 페이지"><img src="<%=webServer%>/skin5/mm018/m_combobtn.png"
  width="30" height="28" alt="다른 카탈로그 이동 페이지"><p>이 동</p></a></li>
<% if(mediaFile.equals("true") && !skinMedia.equals("000")){ %>
<li id="mediabtn" class="mediabtn5mD018"><a href="javascript:MenuObj.mmgroupClick('media');" title="음악메뉴"><img src="<%=webServer%>/skin5/mm018/m_mediabtn.png"
  width="30" height="28" alt="음악메뉴"><p>배경음악</p></a></li>
<% } %>
<% if(searchFile.equals("true")){ %>
<li id="searchsect" class="search5mD018">
<form name="searchform" method="get" action="#" onsubmit="return do_searchbtnClick(document.searchform.qtxt.value)">
<input type="search" class="searchfnt018 searchtxt018m" name="qtxt" title="검색어 입력" onfocus="do_qtxtFocusIn(this)">
<input type="button" class="searchbtn018m" value="검색" src="<%=webServer%>/skin5/mm018/search_btn.png" alt="검색 시작">
</form>
</li>
<% } %>
</ul>
</nav>

<% if(indexType.equals("H") && mokchaFile.equals("true")){ %>
<!-- horizontal index -->
<div id="horidx" class="horidx5mD018">
<div id="horidx_div" class="horidx_div5mD018"></div>
</div>
<% } %>

<!-- onsmimage -->
<div id="onsmlsect">
<a href="javascript:leftOnsmClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageL018.png" width="30" height="58" alt="이전 페이지"></a>
</div>
<div id="onsmrsect">
<a href="javascript:rightOnsmClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageR018.png" width="30" height="58" alt="다음 페이지"></a>
</div>

<!-- popup's media menu -->
<% if(mediaFile.equals("true") && !skinMedia.equals("000")){ %>
<div id="mediasect" class="media5mD018" style="visibility:hidden;">
<svg version="1.1" id="mediasvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="14">
<defs>
<linearGradient id="mediabtnGradient" gradientTransform="rotate(90)">
	<stop offset="0%" stop-color="#ffffff" />
	<stop offset="100%" stop-color="#dddddd" />
</linearGradient>
</defs>
<!-- animation -->
<g>
	<rect id="mediarect1" x="0" y="0" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect2" x="6" y="0" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect3" x="12" y="0" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect4" x="0" y="3" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect5" x="6" y="3" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect6" x="12" y="3" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect7" x="0" y="6" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect8" x="6" y="6" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect9" x="12" y="6" width="4" height="2" fill="#74CF00" />
	<rect id="mediarect10" x="0" y="9" width="4" height="2" fill="#74B800" />
	<rect id="mediarect11" x="6" y="9" width="4" height="2" fill="#74B800" />
	<rect id="mediarect12" x="12" y="9" width="4" height="2" fill="#74B800" />
	<rect id="mediarect13" x="0" y="12" width="4" height="2" fill="#748A00" />
	<rect id="mediarect14" x="6" y="12" width="4" height="2" fill="#748A00" />
	<rect id="mediarect15" x="12" y="12" width="4" height="2" fill="#748A00" />
</g>
</svg>

<ul class="media_ul5mD018">
<li id="media_backbtn" class="media_backbtn5mD018"><a href="javascript:MediaObj.backbtnClick();" title="이전 곡">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13" height="13">
	<polygon points="5,7 13,13 13,0" fill="url(#mediabtnGradient)" />
	<rect x="0.5" y="0" width="4" height="13" fill="url(#mediabtnGradient)" />
</svg></a>
</li>
<li id="media_playbtn" class="media_playbtn5mD018"><a href="javascript:MediaObj.playbtnClick();" title="재생">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10" height="13">
	<polygon points="0,0 10,7 0,13" fill="url(#mediabtnGradient)" />
</svg></a>
</li>
<li id="media_nextbtn" class="media_nextbtn5mD018"><a href="javascript:MediaObj.nextbtnClick();" title="다음 곡">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13" height="13">
	<polygon points="0,0 8,7 0,13" fill="url(#mediabtnGradient)" />
	<rect x="8.5" y="0" width="4" height="13" fill="url(#mediabtnGradient)" />
</svg></a>
</li>
<li id="media_stopbtn" class="media_stopbtn5mD018"><a href="javascript:MediaObj.stopbtnClick();" title="멈춤">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="9" height="9">
	<polygon points="0,0 9,0 9,9 0,9" fill="url(#mediabtnGradient)" />
</svg></a>
</li>
<li id="media_listbtn" class="media_listbtn5mD018"><a href="javascript:MediaObj.listbtnClick();" title="재생목록">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13" height="13">
	<polygon points="7,0 13,8 0,8" fill="url(#mediabtnGradient)" />
	<rect x="0" y="10.5" width="13" height="4" fill="#dddddd"/>
</svg></a>
</li>
</ul>
<audio id="mediactrl" class="audio_ctrlr_hidden">브라우저가 audio를 지원하지 않습니다.</audio>
<div class="mmpopclosebtn"><a href="javascript:MenuObj.mmgroupClick('media');"><img src="<%=webServer%>/skin5/win/m_closebtn.png" width="15" height="15"></a></div>
</div>
<% } %>

<script>
MenuObj = {storedStr:''};

backDiv = document.getElementById("backsect");
if(document.getElementById("searchsect")) searchDiv = document.getElementById("searchsect");
if(document.getElementById("mediasect")) mediaDiv = document.getElementById("mediasect");

MenuObj.onload_func2 = function(){
	if(LinkInfo.facebookStr === "" && document.getElementById("facebookbtn")) document.getElementById("facebookbtn").style.opacity = "0.2";
	if(LinkInfo.twitterStr === "" && document.getElementById("twitterbtn")) document.getElementById("twitterbtn").style.opacity = "0.2";
	if(LinkInfo.kakaoStr === "" && document.getElementById("kakaobtn")) document.getElementById("kakaobtn").style.opacity = "0.2";

	if(SkinInfo.printkind === "000" && document.getElementById("printbtn")) document.getElementById("printbtn").style.opacity = "0.2";
	if(FileInfo.idxFile === false && document.getElementById("indexbtn")) document.getElementById("indexbtn").style.opacity = "0.2";
	if(MenuInfo.mailButtonHide === "Y" && document.getElementById("mailbtn")) document.getElementById("mailbtn").style.opacity = "0.2";
	if(SkinInfo.combokind === "000" && document.getElementById("combobtn")) document.getElementById("combobtn").style.opacity = "0.2";
}
MenuObj.mmgroupClick = function(s){
	if(mediaDiv.style.visibility == "visible"){
		mediaDiv.style.visibility = "hidden";
	}
	else{
		var areaRect = document.getElementById("mediabtn").getBoundingClientRect();
		mediaDiv.style.top = (areaRect.top - 40) + "px";
		mediaDiv.style.left = (areaRect.left-mediaDiv.clientWidth/2) + "px";
		mediaDiv.style.visibility = "visible";
	}
}
MenuObj.show_pageNumber = function(){
	show_currIndex();
}
MenuObj.onresize_func = function(){
}

onsmlDiv = document.getElementById("onsmlsect");
onsmrDiv = document.getElementById("onsmrsect");
OnsmObj = {};
OnsmObj.set_location = function(){
	onsmlDiv.style.left = winRect.x + "px";
	onsmlDiv.style.top = (smRect.centerPt.y-28) + "px";
	onsmrDiv.style.left = (winRect.right-30) + "px";
	onsmrDiv.style.top = (smRect.centerPt.y-28) + "px";
}
OnsmObj.set_visible = function(b1, b2){
	onsmlDiv.style.visibility = b1;
	onsmrDiv.style.visibility = b1;
}
</script>
<% } %>
