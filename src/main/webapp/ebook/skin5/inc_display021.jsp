<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%
	request.setCharacterEncoding("utf-8");

	String incView = request.getParameter("incview");
	String indexType = request.getParameter("indexType");
	String combokind = request.getParameter("combokind");
	String downFile = request.getParameter("downFile");
	String downFileName = request.getParameter("downFileName");
	String downexten = request.getParameter("downexten");
	String downtext = URLDecoder.decode(request.getParameter("downtext"),"utf-8");
	String mediaFile = request.getParameter("mediaFile");
	String webServer = request.getParameter("webServer");
	String printKindmc = request.getParameter("printKindmc");
	String dirName = URLDecoder.decode(request.getParameter("dirName"),"utf-8");
	String searchFile = request.getParameter("searchFile");
	String skinMedia = request.getParameter("skinMedia");
	String Dir = request.getParameter("Dir");
	String catimage = request.getParameter("catimage");
	String webpath = webServer + "/catImage" + catimage + "/" + Dir;
%>
<% if(incView.equals("d")){ %>
<!-- background -->
<div id="backsect" class="back5d021"></div>

<!-- navigation -->
<nav id="lowersect" class="mm5dD021-idxR">
<ul class="mm_ulE001">
<li id="fullscrbtn" class="fullscrbtn5dD021"><a href="javascript:fullscrbtnClick();" title="전체화면으로 보기"><img src="<%=webServer%>/skin5/mm021/fullscrbtn.png" 
width="16" height="16" alt="전체화면" onmouseover="this.src='<%=webServer%>/skin5/mm021/fullscrbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm021/fullscrbtn.png'"></a></li>
<li id="closebtn" class="closebtn5dD021"><a href="javascript:closebtnClick();" title="창닫기"><img src="<%=webServer%>/skin5/mm021/closebtn.png" 
width="16" height="16" alt="창닫기" onmouseover="this.src='<%=webServer%>/skin5/mm021/closebtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm021/closebtn.png'"></a></li>
</ul>

<ul class="mm_ulE002">
<li id="firstbtn" class="firstbtn5dD021"><a href="javascript:firstbtnClick();" title="처음 페이지로 이동"><img src="<%=webServer%>/skin5/mm021/firstbtn.png" 
width="16" height="16" alt="처음 페이지" onmouseover="this.src='<%=webServer%>/skin5/mm021/firstbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm021/firstbtn.png'"></a></li>
<li id="prevbtn" class="prevbtn5dD021"><a href="javascript:prevbtnClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/mm021/prevbtn.png"
width="16" height="16" alt="이전 페이지" onmouseover="this.src='<%=webServer%>/skin5/mm021/prevbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm021/prevbtn.png'"></a></li>
<li id="lastbtn" class="lastbtn5dD021"><a href="javascript:lastbtnClick();" title="마지막 페이지로 이동"><img src="<%=webServer%>/skin5/mm021/lastbtn.png"
width="16" height="16" alt="마지막 페이지" onmouseover="this.src='<%=webServer%>/skin5/mm021/lastbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm021/lastbtn.png'"></a></li>
<li id="nextbtn" class="nextbtn5dD021"><a href="javascript:nextbtnClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/mm021/nextbtn.png"
width="16" height="16" alt="다음 페이지" onmouseover="this.src='<%=webServer%>/skin5/mm021/nextbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm021/nextbtn.png'"></a></li>
</ul>

<p id="pageshow" class="pagefnt021 pageshow5dD021"></p>

<div class="pageslider5dD021">
	<p id="pslider_spage" class="pagefnt021 pslider_spage5dD021"></p>
	<div id="pslider_cnt" class="pslider_cnt5dD021">
		<div id="pslider_linebar" class="pslider_linebar5dD021">
			<img src="<%=webServer%>/skin5/mm021/dragbar1.png" width="4" height="6">
			<img id="pslider_linebar2" src="<%=webServer%>/skin5/mm021/dragbar2.png" width="100" height="6">
			<img src="<%=webServer%>/skin5/mm021/dragbar3.png" width="4" height="6">
		</div>
		<div id="pslider_redbar" class="pslider_redbar5dD021">
			<img src="<%=webServer%>/skin5/mm021/dragbarleft1.png" width="4" height="6">
			<img id="pslider_redbar2" src="<%=webServer%>/skin5/mm021/dragbarleft2.png" width="100" height="6">
			<img src="<%=webServer%>/skin5/mm021/dragbarleft3.png" width="4" height="6">
		</div>
		<div id="pslider_btn" class="pslider_btn5dD021"><img src="<%=webServer%>/skin5/mm021/dragbutton.png" width="18" height="22" alt="슬라이더 버튼" 
		  onmousedown="this.src='<%=webServer%>/skin5/mm021/dragbutton_down.png';MenuObj.do_mouseDown(event);" onmousemove="Action.do_mouseDrag(event)"
		  onmouseover="this.src='<%=webServer%>/skin5/mm021/dragbutton_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm021/dragbutton.png'"></div>
	</div>
	<p id="pslider_fpage" class="pagefnt021 pslider_fpage5dD021"></p>
</div>

<!-- combobox -->
<% if(!combokind.equals("000")){ %>
<div id="combosect" class="cbbox5dD021" data-width="display" data-align="left" data-direction="top" data-vpos="-3">
<a href="javascript:comboClick();" title="다른 카탈로그로 이동"><img src="<%=webServer%>/skin5/mm021/combo_box.png" width="96" height="16"
onmouseover="this.src='<%=webServer%>/skin5/mm021/combo_box_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm021/combo_box.png'" alt="다른 카탈로그로 이동"></a>
</div>
<% } %>

</nav>

<script>
MenuObj = {storedStr:'', linebarobj:undefined, redbarobj:undefined, btnobj:undefined, pageshowobj:undefined,
	cntRect:undefined, dragWidth:0, dragX:0, btnX:0, befX:0};

MenuObj.onload_func2 = function(){
	MenuObj.linebarobj = document.getElementById("pslider_linebar2");
	MenuObj.redbarobj = document.getElementById("pslider_redbar2");
	MenuObj.btnobj = document.getElementById("pslider_btn");
	MenuObj.pageshowobj = document.getElementById("pageshow");

	document.getElementById("pslider_spage").innerHTML = "1";
	document.getElementById("pslider_fpage").innerHTML = PageInfo.cataPages;

	MenuObj.dragWidth = 362;		// 362 = 380 - 18(mmpsBtn.width)
	MenuObj.linebarobj.style.width = "372px";		// 4 + 372 + 4 = 380;

	MenuObj.cntRect = document.getElementById("pslider_cnt").getBoundingClientRect();
	MenuObj.set_btnxFromPageno(PageInfo.currentPage);
}
MenuObj.show_pageNumber = function(){
	MenuObj.set_btnxFromPageno(PageInfo.currentPage);
}
MenuObj.onresize_func = function(){
}
MenuObj.set_btnxFromPageno = function(n){
	var totalpages = PageInfo.cataPages - 1;
	MenuObj.btnX = MenuObj.dragX + ((n-1)/totalpages * MenuObj.dragWidth);

	MenuObj.btnobj.style.left = MenuObj.btnX + "px";
	MenuObj.redbarobj.style.width = MenuObj.btnX + "px";
	MenuObj.pageshowobj.style.left = (MenuObj.cntRect.left + MenuObj.btnX + 9 - 25) + "px";
	MenuObj.show_pageno();
}

MenuObj.get_pagenoFromBtnx = function(){
	var totalpages = PageInfo.cataPages - 1;
	var currpage = Math.round(totalpages * parseInt(MenuObj.btnobj.style.left.replace("px","") - MenuObj.dragX) / MenuObj.dragWidth);
	return (currpage+1);
}
MenuObj.show_pageno = function(){
	var n = MenuObj.get_pagenoFromBtnx();
	var currPage = PageInfo.get_directPageNo(n);
	MenuObj.pageshowobj.innerHTML = PageInfo.get_pageshowString(currPage);
}

/// ############################## event ############################## ///
MenuObj.do_mouseDown = function(e){
	e.preventDefault();
	clickX = e.clientX;
	clickY = e.clientY;
	Action.mouseDragClip = MenuObj;
}
MenuObj.do_mouseDrag = function(e){
	e.preventDefault();
	MenuObj.befX = e.clientX;
	var tx = MenuObj.btnX + MenuObj.befX - clickX;
	if(tx < 0) tx = 0;
	else if(tx > MenuObj.dragX+MenuObj.dragWidth) tx = MenuObj.dragX + MenuObj.dragWidth;

	MenuObj.btnobj.style.left = tx + "px";
	MenuObj.redbarobj.style.width = tx + "px";
	MenuObj.pageshowobj.style.left = (MenuObj.cntRect.left + tx - 25 + 9) + "px";

	MenuObj.show_pageno();
}
MenuObj.do_mouseUp = function(e){
	MenuObj.btnX += (MenuObj.befX - clickX);
	var n = MenuObj.get_pagenoFromBtnx();
	if(PermitMan.get_directGoPermNumber("pslider", n,"system") == false) return;
	go_general(n, "system");
}
if(document.getElementById("combosect")) comboDiv = document.getElementById("combosect");
</script>

<% } else if(incView.equals("m")){ %>

<!-- background -->
<div id="backsect" class="backsect021"></div>

<!-- navigation -->
<nav id="mmsect" class="mm5mD021">

<p id="pageshow" class="pagefnt021 pageshow5dD021"></p>
<div class="pageslider5mD021">
	<p id="pslider_spage" class="pagefnt021 pslider_spage5dD021"></p>
	<div id="pslider_cnt" class="pslider_cnt5mD021">
		<div id="pslider_linebar" class="pslider_linebar5mD021">
			<img src="<%=webServer%>/skin5/mm021/dragbar1.png" width="4" height="6">
			<img id="pslider_linebar2" src="<%=webServer%>/skin5/mm021/dragbar2.png" width="100" height="6">
			<img src="<%=webServer%>/skin5/mm021/dragbar3.png" width="4" height="6">
		</div>
		<div id="pslider_redbar" class="pslider_redbar5mD021">
			<img src="<%=webServer%>/skin5/mm021/dragbarleft1.png" width="4" height="6">
			<img id="pslider_redbar2" src="<%=webServer%>/skin5/mm021/dragbarleft2.png" width="100" height="6">
			<img src="<%=webServer%>/skin5/mm021/dragbarleft3.png" width="4" height="6">
		</div>
		<div id="pslider_btn" class="pslider_btn5mD021"><img src="<%=webServer%>/skin5/mm021/m_dragbutton.png" width="18" height="22" alt="슬라이더 버튼" 
		ontouchstart="MenuObj.do_touchDown(event);" ontouchmove="Action.do_mouseDrag(event)"></div>
	</div>
	<p id="pslider_fpage" class="pagefnt021 pslider_fpage5dD021"></p>
</div>

</nav>

<script>
MenuObj = {storedStr:'', linebarobj:undefined, redbarobj:undefined, btnobj:undefined, pageshowobj:undefined,
	cntRect:undefined, dragWidth:0, dragX:0, btnX:0, befX:0};

MenuObj.onload_func2 = function(){
	MenuObj.linebarobj = document.getElementById("pslider_linebar2");
	MenuObj.redbarobj = document.getElementById("pslider_redbar2");
	MenuObj.btnobj = document.getElementById("pslider_btn");
	MenuObj.pageshowobj = document.getElementById("pageshow");

	document.getElementById("pslider_spage").innerHTML = "1";
	document.getElementById("pslider_fpage").innerHTML = PageInfo.cataPages;

	MenuObj.dragWidth = 262;		// 262 = 280 - 18(mmpsBtn.width)
	MenuObj.linebarobj.style.width = "272px";		// 4 + 272 + 4 = 280;

	MenuObj.cntRect = document.getElementById("pslider_cnt").getBoundingClientRect();
	MenuObj.set_btnxFromPageno(PageInfo.currentPage);
}
MenuObj.show_pageNumber = function(){
	MenuObj.set_btnxFromPageno(PageInfo.currentPage);
}
MenuObj.onresize_func = function(){
}
MenuObj.set_btnxFromPageno = function(n){
	var totalpages = PageInfo.cataPages - 1;
	MenuObj.btnX = MenuObj.dragX + ((n-1)/totalpages * MenuObj.dragWidth);

	MenuObj.btnobj.style.left = MenuObj.btnX + "px";
	MenuObj.redbarobj.style.width = MenuObj.btnX + "px";
	MenuObj.pageshowobj.style.left = (MenuObj.cntRect.left + MenuObj.btnX + 9 - 25) + "px";
	MenuObj.show_pageno();
}

MenuObj.get_pagenoFromBtnx = function(){
	var totalpages = PageInfo.cataPages - 1;
	var currpage = Math.round(totalpages * parseInt(MenuObj.btnobj.style.left.replace("px","") - MenuObj.dragX) / MenuObj.dragWidth);
	return (currpage+1);
}
MenuObj.show_pageno = function(){
	var n = MenuObj.get_pagenoFromBtnx();
	var currPage = PageInfo.get_directPageNo(n);
	MenuObj.pageshowobj.innerHTML = PageInfo.get_pageshowString(currPage);
}

/// ############################## event ############################## ///
MenuObj.do_touchDown = function(e){
	e.preventDefault();
	clickX = e.touches[0].clientX;
	clickY = e.touches[0].clientY;
	//clickX = e.clientX;
	//clickY = e.clientY;
	
	Action.mouseDragClip = MenuObj;
}
MenuObj.do_mouseDrag = function(e){
	e.preventDefault();
	MenuObj.befX = e.touches[0].clientX;
	var tx = MenuObj.btnX + MenuObj.befX - clickX;
	if(tx < 0) tx = 0;
	else if(tx > MenuObj.dragX+MenuObj.dragWidth) tx = MenuObj.dragX + MenuObj.dragWidth;

	MenuObj.btnobj.style.left = tx + "px";
	MenuObj.redbarobj.style.width = tx + "px";
	MenuObj.pageshowobj.style.left = (MenuObj.cntRect.left + tx - 25 + 9) + "px";
	MenuObj.show_pageno();
}
MenuObj.do_touchEnd = function(e){
	MenuObj.btnX += (MenuObj.befX - clickX);
	var n = MenuObj.get_pagenoFromBtnx();
	if(PermitMan.get_directGoPermNumber("pslider", n,"system") == false) return;
	go_general(n, "system");
}

backDiv = document.getElementById("backsect");
if(document.getElementById("combosect")) comboDiv = document.getElementById("combosect");

OnsmObj = {};
OnsmObj.set_location = function(){
}
OnsmObj.set_visible = function(b1, b2){
}
</script>

<% } %>
