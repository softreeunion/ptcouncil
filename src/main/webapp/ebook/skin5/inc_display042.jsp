<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%
	request.setCharacterEncoding("utf-8");

	String incView = request.getParameter("incview");
	String indexType = request.getParameter("indexType");
	String combokind = request.getParameter("combokind");
	String downFile = request.getParameter("downFile");
	String downFileName = request.getParameter("downFileName");
	String downexten = request.getParameter("downexten");
	String downtext = URLDecoder.decode(request.getParameter("downtext"),"utf-8");
	String mediaFile = request.getParameter("mediaFile");
	String webServer = request.getParameter("webServer");
	String printKindmc = request.getParameter("printKindmc");
	String dirName = URLDecoder.decode(request.getParameter("dirName"),"utf-8");
	String searchFile = request.getParameter("searchFile");
	String mokchaFile = request.getParameter("mokchaFile");
	String skinMedia = request.getParameter("skinMedia");
	String skinSpacer = request.getParameter("skinSpacer");
	String Dir = request.getParameter("Dir");
	String catimage = request.getParameter("catimage");
	String webpath = webServer + "/catImage" + catimage + "/" + Dir;
%>
<% if(incView.equals("m")){ %>
<!-- mm navigation -->
<nav id="mmsect" class="mm5mD042">
<div id="mmturnbtn" class="mmturnbtn5mD042"><a href="javascript:MenuObj.mmturnClick();" title="펼쳐보기"><img id="mmturnbtnpix" src="<%=webServer%>/skin5/mm042/m_expandbtn.png"
  class="mm_ul_imgE006" alt="펼쳐보기"></a></div>
</nav>

<% if(indexType.equals("H") && mokchaFile.equals("true")){ %>
<!-- horizontal index -->
<div id="horidx" class="horidx5mD042">
<div id="horidx_div" class="horidx_div5mD042"></div>
<a href="javascript:MenuObj.horidxmoreClick();"><div id="idxmorebtn" class="horidx_more5mD042"><img 
 src="<%=webServer%>/skin5/mm042/m_horidxmore.png" id="idxmoreimg" width="23" height="13" alt="목차 더보기"></div></a>
</div>
<% } %>

<!-- lower menu -->
<nav id="lowersect" class="lower5mD042">
<div class="lowernavcnt">
<ul class="mm_ulE001">
<li id="firstbtn" class="firstbtn5mD042"><a href="javascript:firstbtnClick();" title="처음 페이지로 이동"><img src="<%=webServer%>/skin5/mm042/m_firstbtn.png" 
  width="13" height="13" alt="처음 페이지"></a></li>
<li id="prevbtn" class="prevbtn5mD042"><a href="javascript:prevbtnClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/mm042/m_prevbtn.png"
  width="10" height="18" alt="이전 페이지"></a></li>
<li id="pageshow" class="pshow5mD042" contentEditable="true" onfocus="MenuObj.do_pagenoFocusIn(event);" onblur="MenuObj.do_pagenoFocusOut(event);"></li>
<li id="nextbtn" class="nextbtn5mD042"><a href="javascript:nextbtnClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/mm042/m_nextbtn.png"
  width="10" height="18" alt="다음 페이지"></a></li>
<li id="lastbtn" class="lastbtn5mD042"><a href="javascript:lastbtnClick();" title="마지막 페이지로 이동"><img src="<%=webServer%>/skin5/mm042/m_lastbtn.png"
  width="13" height="13" alt="마지막 페이지"></a></li>
</ul>
<div class="lowernavback">
<img class="navidiv001" src="<%=webServer%>/skin5/mm042/m_vertdiv.png">
<img class="navidiv002" src="<%=webServer%>/skin5/mm042/m_vertdiv.png">
</div>
</div>

<!-- mmpop menu -->
<nav id="mmpopsect" class="mmpop5mD042" style="visibility:hidden;">
<ul class="mmpop_ul5mD042">
<% if(mokchaFile.equals("true")){ %>
<li id="indexbtn" class="indexbtn5mD042"><a href="javascript:indexbtnClick();" title="목차 보기"><img src="<%=webServer%>/skin5/mm042/m_indexbtn.png"
  width="40" height="10" alt="목차"></a></li>
<% } %>
<% if(!skinSpacer.equals("000")){ %>
<li id="spacerbtn" class="spacerbtn5mD042"><a href="javascript:spacerbtnClick('left');" title="책갈피에 추가"><img src="<%=webServer%>/skin5/mm042/m_spacerbtn.png"
  width="42" height="10" alt="책갈피"></a></li>
<% } %>
<li id="expbtn" class="expbtn5mD042"><a href="javascript:expbtnClick();" title="탐색기 보기"><img src="<%=webServer%>/skin5/mm042/m_expbtn.png" 
  width="35" height="10" alt="탐색기"></a></li>
<% if(downFile.equals("true")){ %>
<li id="downbtn" class="downbtn5mD042"><a href="<%=webpath%>/<%=downFileName%>" download="<%=webpath%>/<%=downFileName%>" 
  title="파일 다운로드"><img src="<%=webServer%>/skin5/mm042/m_downbtn.png" width="53" height="13" alt="파일 다운로드"></a></li>
<% } %>
</ul>

<!-- sns -->
<ul class="sns_ul5mD042">
<li id="facebookbtn" class="facebookbtn5mD042"><a href="javascript:facebookbtnClick();" title="페이스북 공유"><img src="<%=webServer%>/skin5/mm041/m_facebookbtn.png" width="10" height="20" alt="페이스북"></a></li>
<li id="twitterbtn" class="twitterbtn5mD042"><a href="javascript:twitterbtnClick();" title="트위터 공유"><img src="<%=webServer%>/skin5/mm041/m_twitterbtn.png" width="28" height="20" alt="트위터"></a></li>
<li id="kakaobtn" class="kakaobtn5mD042"><a href="javascript:kakaobtnClick();" title="카카오 공유"><img src="<%=webServer%>/skin5/mm041/m_kakaobtn.png" width="21" height="20" alt="카카오"></a></li>
</ul>
</nav>

<div class="lowerbtmcnt">
<ul id="mmgsect" class="mmg5mD042">
<li id="morebtn" class="morebtn5mD042"><a href="javascript:MenuObj.mmpopClick();" title="메뉴 더보기"><img src="<%=webServer%>/skin5/mm042/m_morebtn.png"
  width="20" height="20" alt="메뉴 더보기"></a></li>
<% if(!combokind.equals("000")){ %>
<li id="combobtn" class="combobtn5mD042"><a href="javascript:combobtnClick();" title="다른 카탈로그 이동 페이지"><img src="<%=webServer%>/skin5/mm042/m_combobtn.png"
  width="20" height="20" alt="다른 카탈로그"></a></li>
<% } %>
</ul>

<% if(searchFile.equals("true")){ %>
<div id="searchsect" class="search5mD042">
<form name="searchform" method="get" action="#" onsubmit="return MenuObj.searchClick(document.searchform.qtxt.value)" title="페이지 검색">
<input type="search" class="searchtxt5mD042" name="qtxt" title="검색어 입력">
<input type="image" class="searchbtn5mD042" src="<%=webServer%>/skin5/mm042/m_searchbtn.png" title="검색">
</form>
</div>
<% } %>
</div>
</nav>

<!-- onsmimage -->
<div id="onsmlsect" class="onsmlE041">
<a href="javascript:leftOnsmClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageL041.png" width="33" height="49" alt="이전 페이지"></a>
</div>
<div id="onsmrsect" class="onsmlE041">
<a href="javascript:rightOnsmClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageR041.png" width="33" height="49" alt="다음 페이지"></a>
</div>

<script>
MenuObj = {storedStr:''};
MenuObj.onload_func2 = function(){
	if(LinkInfo.facebookStr === "" && document.getElementById("facebookbtn")) document.getElementById("facebookbtn").style.opacity = "0.2";
	if(LinkInfo.twitterStr === "" && document.getElementById("twitterbtn")) document.getElementById("twitterbtn").style.opacity = "0.2";
	if(LinkInfo.kakaoStr === "" && document.getElementById("kakaobtn")) document.getElementById("kakaobtn").style.opacity = "0.2";
	if(MenuInfo.idxmorebtn === false){
		document.getElementById("horidx_div").style.width = "100%";
		document.getElementById("idxmorebtn").style.display = "none";
	}
}
MenuObj.show_pageNumber = function(){
	var n = (PageInfo.showStartPage == 0) ? PageInfo.cataPages : PageInfo.get_showPageFromReal(PageInfo.cataPages);
	document.getElementById("pageshow").innerHTML = MoveInfo.get_showPageNumber() + "/" + n;
	show_currIndex();
}
MenuObj.onresize_func = function(){
}
MenuObj.do_pagenoFocusIn = function(e){
	MenuObj.storedStr = document.getElementById("pageshow").innerHTML;
	document.getElementById("pageshow").innerHTML = PageInfo.currentPage;
	window.getSelection().selectAllChildren(document.getElementById("pageshow"));
}
MenuObj.do_pagenoFocusOut = function(e){
	MenuObj.do_pagenoFormSubmit();
	document.getElementById("pageshow").innerHTML = MenuObj.storedStr;
}
MenuObj.do_pagenoFormSubmit = function(){
	var spage = document.getElementById("pageshow").innerHTML;
	var n = PermitMan.get_directGoPerm("showboard", spage, "", "");			// device, spage, pageshow, state
	if(n < 0) return false;
	go_general(n, "user");
	return false;
}
MenuObj.mmpopClick = function(){
	if(mmpopDiv.style.visibility == "hidden"){
		mmpopDiv.style.visibility = "visible";
	}
	else{
		mmpopDiv.style.visibility = "hidden";
	}
}
MenuObj.horidxmoreClick = function(){
	if(indexDiv.style.visibility == "visible"){
		document.getElementById('idxmoreimg').src = "<%=webServer%>/skin5/mm042/m_horidxmore.png";
		indexDiv.innerHTML = "";
		indexDiv.style.visibility = "hidden";
	}
	else{
		document.getElementById('idxmoreimg').src = "<%=webServer%>/skin5/mm041/m_horidxmore_over.png";
		indexDiv.style.top = cataRect.y+"px";
		indexDiv.innerHTML = document.getElementById("horidx_div").innerHTML;
		indexDiv.style.visibility = "visible";
	}
}
MenuObj.mmturnClick = function(){
	if(Action.mainState === "expand"){
		document.getElementById('mmturnbtnpix').src = "<%=webServer%>/skin5/mm042/m_expandbtn.png";
		unload_expand();
	}
	else{
		document.getElementById('mmturnbtnpix').src = "<%=webServer%>/skin5/mm042/m_turnbtn.png";
		load_expand();
	}
}
MenuObj.searchClick = function(s){
	if(s === "") return false;
	if(PermitMan.normalPerm(-1) === false) return false;
	if(searchSvg) return false;

	MenuObj.mmpopClick();
	SearchInfo.set_qtxt(s);
	load_search();
	return false;
}

mmpopDiv = document.getElementById('mmpopsect');
snsDiv = document.getElementById("snssect");
if(document.getElementById("searchsect")) searchDiv = document.getElementById("searchsect");

onsmlDiv = document.getElementById("onsmlsect");
onsmrDiv = document.getElementById("onsmrsect");
OnsmObj = {};
OnsmObj.set_location = function(){
	onsmlDiv.style.left = winRect.x + "px";
	onsmlDiv.style.top = (smRect.centerPt.y-25) + "px";
	onsmrDiv.style.left = (winRect.right-33) + "px";
	onsmrDiv.style.top = (smRect.centerPt.y-25) + "px";
}
OnsmObj.set_visible = function(b1, b2){
	onsmlDiv.style.visibility = b1;
	onsmrDiv.style.visibility = b2;
}
</script>
<% } %>
