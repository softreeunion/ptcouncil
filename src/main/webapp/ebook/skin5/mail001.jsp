<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%
	String tw = get_param1(request.getParameter("w"),5);
	int w = tw.equals("") ? 0 : Integer.parseInt(tw);
	String th = get_param1(request.getParameter("h"),5);
	int h = th.equals("") ? 0 : Integer.parseInt(th);
	String ds = request.getParameter("d");
	if(ds == null) ds = "";

	String[] ar = mysplit(ds,".");
	String n = "";
	if(ar.length > 1) n = get_param2(ar[1],5);			// SkinInfo.nwclass(n)
	String v = "";
	if(ar.length > 2) v = check_um(ar[2],"um");			// CataInfo.incview(v)
	String d = check_mainmenu(ar[0], v);				// SkinInfo.mainmenu(d)
	String r = "mail";

	pathOfOriCata = webSysDir + "/catImage";
	HashMap Display = new HashMap();
	Display.put("user", get_userSkin(d, v));
	if(!Display.get("user").equals("")) set_userSkin((String)Display.get("user"), Display);
	set_newintitle(r, d, webSysDir+"/skin5", Display);

	String newin_titletext = "메일발송";
	String svgObj = "mailSvg";
	String plusID = "";
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>ecatalog - mail001</title>
<style>
	html, body{margin:0px;width:100%;height:100%;overflow:hidden;}
	*{font-family:굴림;font-size:12px;}
<% if(n.equals("016")){ %>
	body{background-color:none;}
<% } else{ %>
	body{background-color:#f7f7f7;}
<% } %>

	#newin_bodybtn{display:block;z-index:4;position:absolute;right:23px;top:272px;cursor:pointer;}

	.mail_div{display:block;z-index:2;position:absolute;left:0px;top:5px;width:100%;}
	dl{display:block;width:100%;}
	dl > dt{display:block;float:left;margin:0;width:70px;height:22px;clear:both;
		text-align:right;padding-right:10px;line-height:22px;}
	dl > dd{display:block;float:left;margin:0;width:calc(100% - 80px);height:22px;}
<% if(v.equals("m")){ %>
	.twho{width:80px;height:14px;border:1px solid #000000;-webkit-border-radius:0px;}
	.tmail{width:90%;height:14px;border:1px solid #000000;-webkit-border-radius:0px;}
	.tsubj{width:90%;height:14px;border:1px solid #000000;-webkit-border-radius:0px;}
	textarea{width:90%;height:130px;border:1px solid #000000;-webkit-border-radius:0px;}
<% } else{ %>
	.twho{width:90px;height:14px;border:1px solid #000000;}
	.tmail{width:240px;height:14px;border:1px solid #000000;}
	.tsubj{width:290px;height:14px;border:1px solid #000000;}
	textarea{width:290px;height:130px;border:1px solid #000000;}
<% } %>
</style>
<script>
var selfName = "mail001.jsp", winCnt = "mail";
var svgWidth = <%=w%>, svgHeight = <%=h%>;
var p;
var ws = "";

var downKind = 0;
var dragGap = 0;

function onload_func(){
	p = window.parent;
	console.log(selfName+" : loaded");
	p.mailSvg = this;
	window.parent.objLoaded2('mail', this, false);
	dragGap = p.WinInfo.get_dragGap('<%=n%>');

	p.document.getElementById('newin_frame').innerHTML = ws;
}
function mailSend(){
	var dm = document.mailform;	
	if(dm.fromwho.value == ""){
		alert("보내는이를 입력하여 주십시오.");
		dm.fromwho.focus();
		return;
	}
	if(dm.frommail.value == ""){
		alert("보내는 메일을 입력하여 주십시오.");
		dm.formmail.focus();
		return;
	}
	if(dm.towho.value == ""){
		alert("받는이를 입력하여 주십시오.");
		dm.towho.focus();
		return;
	}
	if(dm.frommail.value == ""){
		alert("받는 메일을 입력하여 주십시오.");
		dm.frommail.focus();
		return;
	}
	if(dm.subject.value == ""){
		alert("제목을 입력하여 주십시오.");
		dm.subject.focus();
		return;
	}
	if(dm.cont.value == ""){
		alert("내용을 입력하여 주십시오.");
		dm.cont.focus();
		return;
	}

	var formdata = new FormData();
	formdata.append("fromwho", dm.fromwho.value);
	formdata.append("frommail", dm.frommail.value);
	formdata.append("towho", dm.towho.value);
	formdata.append("tomail", dm.tomail.value);
	formdata.append("subject", dm.subject.value);
	formdata.append("content", dm.cont.value);

	var maildir = (p.PageInfo.mailDir === "") ? p.PageInfo.cataDir : p.PageInfo.mailDir;
	var logodir = (p.PageInfo.logoDir === "") ? p.PageInfo.cataDir : p.PageInfo.logoDir;

	formdata.append("catimage", p.FileInfo.catimage);
	formdata.append("Dir", p.PageInfo.cataDir);
	formdata.append("mailDir", maildir);
	formdata.append("logoDir", logodir);
	formdata.append("cook_id", p.ServerInfo.memberID);
	formdata.append("vari", p.CataInfo.cataVariant);

	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "../send/maction.jsp");
	xmlHttp.onreadystatechange = function(){
		if(xmlHttp.readyState != 4) return;			// 0:unset, 1:opened, 2:headers_received, 3:loading, 4:done
		if(xmlHttp.status == 200){
			var restr = xmlHttp.responseText.replace(/^\s*|\s*$/g, '');				// front and rear's white space
			//alert(currMode);
			if(restr.length > 5 && restr.substring(0,5) == "Error"){
				alert(restr);
				return;
			}
		}
		alert('메일 발송이 완료 되었습니다.');
		newin_close();
	}
	xmlHttp.send(formdata);
}

/// ############################## window ############################## ///
function newin_close(){
	p.unload_newin(winCnt);
	p.mailSvg = undefined;
}
function do_mouseDrag(e){
	e.preventDefault();

	var posX = e.clientX - p.clickX + p.befX;
	var posY = e.clientY - p.clickY + p.befY;

	if(posX < p.cataRect.x) posX = p.cataRect.x;
	else if(posX > p.cataRect.right-svgWidth-dragGap) posX = p.cataRect.right - svgWidth - dragGap;

	if(posY < p.cataRect.y) posY = p.cataRect.y;
	else if(posY > p.cataRect.bottom-svgHeight-dragGap) posY = p.cataRect.bottom - svgHeight - dragGap;

	p.newinDiv.style.left = posX+"px";
	p.newinDiv.style.top = posY+"px";

	return true;
}
function do_mouseUp(e){
	p.befX += e.clientX - p.clickX;
	p.befY += e.clientY - p.clickY;
}
</script>
</head>

<body onload="onload_func()">

<%
	int fullscrbtn = 0;
	int closebtn = 1;
	String skind = "send";
%>
<%@ include file="inc_newin.jsp" %>

<div class="mail_div">
<form name="mailform">
<dl>
	<dt>보내는 이</dt>
	<dd><input type="text" class="twho" name="fromwho" required></dd>
	<dt>보내는 메일</dt>
	<dd><input type="email" class="tmail" name="frommail" required></dd>
	<dt>받는이</dt>
	<dd><input type="text" class="twho" name="towho" required></dd>
	<dt>받는 메일</dt>
	<dd><input type="email" class="tmail" name="tomail" required></dd>
	<dt>제목</dt>
	<dd><input type="text" class="tsubj" name="subject" required></dd>
	<dt>내용</dt>
	<dd><textarea class="who" name="cont"></textarea></dd>
</dl>
</form>
</section>

</body>
</html>
