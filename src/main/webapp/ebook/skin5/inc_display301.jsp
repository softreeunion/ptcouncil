<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%
	request.setCharacterEncoding("utf-8");

	String incView = request.getParameter("incview");
	String indexType = request.getParameter("indexType");
	String combokind = request.getParameter("combokind");
	String downFile = request.getParameter("downFile");
	String downFileName = request.getParameter("downFileName");
	String downexten = request.getParameter("downexten");
	String downtext = URLDecoder.decode(request.getParameter("downtext"),"utf-8");
	String mediaFile = request.getParameter("mediaFile");
	String webServer = request.getParameter("webServer");
	String printKindmc = request.getParameter("printKindmc");
	String dirName = URLDecoder.decode(request.getParameter("dirName"),"utf-8");
	String searchFile = request.getParameter("searchFile");
	String skinMedia = request.getParameter("skinMedia");
	String Dir = request.getParameter("Dir");
	String catimage = request.getParameter("catimage");
	String webpath = webServer + "/catImage" + catimage + "/" + Dir;
%>
<% if(incView.equals("d")){ %>
<!-- navigation -->
<nav id="mmsect" class="mm5dD301-idxR">

<!-- thumb preview -->
<div id="thumbsect" class="thumb5dD301"></div>

<div id="pageshow" class="pshow5dD301">
<form name="pageshowform" method="get" action="ecatalog.php" onsubmit="return MenuObj.do_pagenoFormSubmit();" title="페이지 이동">
<p class="pshow_p5dD301"><input type="text" name="pageno" value="1" onfocus="MenuObj.do_pagenoFocusIn(event);" onblur="MenuObj.do_pagenoFocusOut(event)"
 pattern="[0-9-]+" title="숫자만 입력 가능합니다." required></p>
<p id="pshow_totalpage">/ 332</p>
</form>
</div>

<ul class="mm_ulE001">
<li id="prevbtn" class="prevbtn5dD301"><a href="javascript:prevbtnClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/mm301/prevbtn.png" width="16" height="16" alt="이전 페이지"
  onmouseover="this.src='<%=webServer%>/skin5/mm301/prevbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm301/prevbtn.png'"></a></li>
<li id="nextbtn" class="nextbtn5dD301"><a href="javascript:nextbtnClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/mm301/nextbtn.png" width="16" height="16" alt="다음 페이지"
  onmouseover="this.src='<%=webServer%>/skin5/mm301/nextbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm301/nextbtn.png'"></a></li>
<li id="slidebtn" class="slidebtn5dD301"><a href="javascript:slidebtnClick();" title="자동 넘김"><img src="<%=webServer%>/skin5/mm301/slidebtn.png" width="16" height="16" alt="자동 넘김"
  onmouseover="this.src='<%=webServer%>/skin5/mm301/slidebtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm301/slidebtn.png'"></a></li>
<% if(!printKindmc.equals("000")){ %>
<li id="printbtn" class="printbtn5dD301"><a href="javascript:printbtnClick();" title="인쇄하기"><img src="<%=webServer%>/skin5/mm301/printbtn.png" width="16" height="17" alt="인쇄"
  onmouseover="this.src='<%=webServer%>/skin5/mm301/printbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm301/printbtn.png'"></a></li>
<% } %>
</ul>

<div id="thumbpaging"></div>

</nav>

<script>
MenuObj = {storedStr:'', thumbX:20, thumbMargin:0, thobjWidth:0, thobjHeight:0,	pc:undefined};

thumbDiv = document.getElementById("thumbsect");
pgthumbDiv = document.getElementById("thumbpaging");

MenuObj.onload_func2 = function(){
	MenuObj.thobjWidth = stageWidth;
	MenuObj.thobjHeight = 70;
	thumbDiv.innerHTML = (FileInfo.embedTag === "iframe") ? "<iframe src='<%=webServer%>/skin5/thumbpage001.html'></iframe>\n"
		: "<object data='<%=webServer%>/skin5/thumbpage001.html' type='text/html'></object>\n";
}
MenuObj.onresize_func = function(){
	MenuObj.thobjWidth = stageWidth;
	thumbSvg.onresize_func(stageWidth);
}
MenuObj.show_pageNumber = function(){
}

MenuObj.make_paging = function(colQty, ntotal){
	MenuObj.pc = new PagingCont();
	MenuObj.pc.totalArticles = ntotal;
	MenuObj.pc.set_articlesOnPage(colQty);
	MenuObj.pc.set_pagesOnSheet(10);
	MenuObj.pc.set_currentPage(1);
	MenuObj.make_pagingList();
}
MenuObj.make_pagingList = function(){
	var res = "";
	if(MenuObj.pc.prevSheet != -1) res += "<a href=\"javascript:MenuObj.prevSheetClick();\">&lt;</a>";

	for(var i = MenuObj.pc.startPage;i <= MenuObj.pc.finalPage;i++){
		if(i == MenuObj.pc.currentPage) res += "<a class=\"currpage\">" + i + "</a>";
		else res += "<a href=\"javascript:MenuObj.do_pageNoClick("+i+");\">" + i + "</a>";
	}

	if(MenuObj.pc.nextSheet != -1) res += "<a href=\"javascript:MenuObj.nextSheetClick();\">&gt;</a>";

	pgthumbDiv.innerHTML = res;
}
MenuObj.do_pageNoClick = function(n){
	if(Action.aniLocking === true) return;
	MenuObj.refresh_pagingList(n);
	thumbSvg.move_clickPage(n);
}
MenuObj.do_prevSheetClick = function(){
	if(Action.aniLocking === true) return;
	MenuObj.refresh_pagingList(MenuObj.pc.prevSheet);
}
MenuObj.do_nextSheetClick = function(){
	if(Action.aniLocking === true) return;
	MenuObj.refresh_pagingList(MenuObj.pc.nextSheet);
}
MenuObj.refresh_pagingList = function(n){
	if(n == MenuObj.pc.currentPage) return;

	pgthumbDiv.innerHTML = "";
	if(n == 0) return;

	MenuObj.pc.set_currentPage(n);
	MenuObj.make_pagingList();
}
MenuObj.remake_pagingList = function(n){
	pgthumbDiv.innerHTML = "";
	MenuObj.pc.set_articlesOnPage(n);
	MenuObj.pc.set_pagesOnSheet(10);
	MenuObj.pc.set_currentPage(1);
	MenuObj.make_pagingList();
}
</script>

<% } else if(incView.equals("m")){ %>
<nav id="mmsect" class="mm5mD301">

<div id="pageshow" class="pshow5mD301" contentEditable="true" onfocus="MenuObj.do_pagenoFocusIn(event);" onblur="MenuObj.do_pagenoFocusOut(event);"></div>

<ul class="mm_ulE002">
<li id="prevbtn" class="prevbtn5mD301"><a href="javascript:prevbtnClick();" title="이전 페이지로 이동"><img 
src="<%=webServer%>/skin5/mm301/m_prevbtn.png" width="18" height="18" alt="이전 페이지로 이동"></a></li>
<li id="nextbtn" class="nextbtn5mD301"><a href="javascript:nextbtnClick();" title="다음 페이지로 이동"><img 
src="<%=webServer%>/skin5/mm301/m_nextbtn.png" width="18" height="18" alt="다음 페이지로 이동"></a></li>
<li id="slidebtn" class="slidebtn5mD301"><a href="javascript:slidebtnClick();" title="자동 넘김"><img 
src="<%=webServer%>/skin5/mm301/m_slidebtn.png" width="18" height="18" alt="자동 넘김"></a></li>
<% if(!printKindmc.equals("000")){ %>
<li id="printbtn" class="printbtn5mD301"><a href="javascript:printbtnClick();" title="인쇄하기"><img 
src="<%=webServer%>/skin5/mm301/m_printbtn.png" width="17" height="18" alt="인쇄하기"></a></li>
<% } %>
</ul>

<!-- thumb preview -->
<div id="thumbsect" class="thumb5mD301"></div>

</nav>

<script>
MenuObj = {storedStr:'', thumbX:20, thumbMargin:0, thobjWidth:0, thobjHeight:0,	pc:undefined};

thumbDiv = document.getElementById("thumbsect");

MenuObj.onload_func2 = function(){
	var tw = stageWidth;
	var th = 70;
	MenuObj.thobjWidth = tw;
	MenuObj.thobjHeight = th;

	thumbDiv.innerHTML = "<object data=\"<%=webServer%>/skin5/thumbpage001m.html\" type=\"text/html\"></object>\n";
}
MenuObj.onresize_func = function(){
	MenuObj.thobjWidth = stageWidth;
	thumbSvg.onresize_func(stageWidth);
}
MenuObj.show_pageNumber = function(){
	var n = (PageInfo.showStartPage == 0) ? PageInfo.cataPages : PageInfo.get_showPageFromReal(PageInfo.cataPages);
	document.getElementById("pageshow").innerHTML = MoveInfo.get_showPageNumber() + "/" + n;
}
MenuObj.do_pagenoFocusIn = function(e){
	MenuObj.storedStr = document.getElementById("pageshow").innerHTML;
	document.getElementById("pageshow").innerHTML = PageInfo.currentPage;
	window.getSelection().selectAllChildren(document.getElementById("pageshow"));
}
MenuObj.do_pagenoFocusOut = function(e){
	do_pagenoFormSubmit();
	document.getElementById("pageshow").innerHTML = MenuObj.storedStr;
}

OnsmObj = {};
OnsmObj.set_location = function(){
}
OnsmObj.set_visible = function(b1, b2){
}
</script>
<% } %>
