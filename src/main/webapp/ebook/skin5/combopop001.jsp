<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%
	String tw = get_param1(request.getParameter("w"),5);
	int w = tw.equals("") ? 0 : Integer.parseInt(tw);
	String th = get_param1(request.getParameter("h"),5);
	int h = th.equals("") ? 0 : Integer.parseInt(th);
	String ds = request.getParameter("d");
	if(ds == null) ds = "";

	String[] ar = mysplit(ds,".");
	String n = "";
	if(ar.length > 1) n = get_param2(ar[1],5);			// SkinInfo.nwclass(n)
	String v = "";
	if(ar.length > 2) v = check_um(ar[2],"um");			// CataInfo.incview(v)
	String p = "";
	if(ar.length > 3) p = get_param2(ar[3],5);			// SkinInfo.cbpopskin(p). 001/012/016
	String c = "";
	if(ar.length > 4) c = get_param1(ar[4],2);			// data-custom. 0/1/...
	String d = check_mainmenu(ar[0], v);				// SkinInfo.mainmenu(d)
	String r = "cbpop";

	pathOfOriCata = webSysDir + "/catImage";
	HashMap Display = new HashMap();
	Display.put("user", get_userSkin(d, v));
	if(!Display.get("user").equals("")) set_userSkin((String)Display.get("user"), Display);

	String gra1Color = "#265a00";
	String gra2Color = "#acc852";
	String textColor = "#ffffff";
	String textColorOver = "#eeeeee";
	String rectColor = "#555555";

	String t = "";
	if(p.equals("001")){
		if(d.equals("011")) t = "E001";
		else if(d.equals("030")) t = "E002";
		else t = "E003";
	}
	else if(p.equals("012")){
		t = "E012";
		if(!Display.get("user").equals("") && !strNullObject(Display.get("cpcolor_d")).equals("")){
			String[] aaa = mysplit((String)Display.get("cpcolor_d"),",");
			gra1Color = aaa[0];
			gra2Color = aaa[1];
			textColor = aaa[2];
			textColorOver = aaa[3];
			rectColor = aaa[4];
		}
	}
	else if(p.equals("016")){
		if(d.equals("021")) t = "E021";
		else if(d.equals("017")) t = "E017";
		else if(d.equals("018")) t = "E018";
		else t = "E016";
	}
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>ecatalog - combopop001</title>
<style>
	html{width:100%;height:100%;margin:0;}
	body{width:100%;height:100%;margin:0;overflow-x:hidden;overflow-y:auto;}
	#mainsvg{display:block;z-index:2;position:absolute;width:100%;height:100%;}
	#contsvg{z-index:3;display:block;position:absolute;width:100%;height:100%;}
	#contdiv{z-index:3;display:block;position:absolute;left:5px;top:5px;width:calc(100% - 15px);height:calc(100% - 17px);
		overflow-x:hidden;overflow-y:auto;}

<% if(p.equals("001")){ %>
	*{font-family:굴림;font-size:12px;}

	.bodyE001{background-color:#D4EBC3;}
	.gitemE001 > rect{fill:#D4EBC3;stroke:none;}
	.gitemE001:hover > rect{fill:#41830F;}
	.gitemE001 > text{fill:#000000;}
	.gitemE001:hover > text{fill:#ffffff;}

	.bodyE002{background-color:#e8f4f8;}
	.gitemE002 > rect{fill:#e8f4f8;stroke:none;}
	.gitemE002:hover > rect{fill:#2492C6;}
	.gitemE002 > text{fill:#000000;}
	.gitemE002:hover > text{fill:#ffffff;}

	.bodyE003{background-color:#ededed;}
	.gitemE003 > rect{fill:#ededed;stroke:none;}
	.gitemE003:hover > rect{fill:#404040;}
	.gitemE003 > text{fill:#000000;}
	.gitemE003:hover > text{fill:#ffffff;}

<% } else if(p.equals("012")){ %>
	*{font-family:굴림;font-size:11px;}

	.bodyE012{background-color:<%=textColor%>;}
	.gitemE012 > text{fill:<%=textColor%>;cursor:pointer;}
	.gitemE012:hover > text{fill:<%=textColorOver%>;}
	.gitemE012 > rect{fill:<%=rectColor%>;cursor:pointer;opacity:0;}
	.gitemE012:hover > rect{opacity:1;}

<% } else if(p.equals("016")){ %>
	*{font-family:굴림;font-size:12px;}

	.bodyE016{background-color:transparent;}
	.gitemE016 > rect{fill:#152e0a;cursor:pointer;opacity:1;}
	.gitemE016:hover > rect{fill:#3A750A;opacity:1;}
	.gitemE016 > text{fill:#ffffff;cursor:pointer;}
	.gitemE016:hover > text{fill:#eeeeee;}
	.rectbackE016{fill:#152e0a;stroke:#a4c37b;stroke-width:1;}
	.arrowpolyE016{fill:#152e0a;stroke:none;}
	.arrowlineE016{fill:none;stroke:#a4c37b;stroke-width:1;}

	.bodyE017{background-color:transparent;}
	.gitemE017 > rect{fill:#393939;cursor:pointer;opacity:1;}
	.gitemE017:hover > rect{fill:#3A750A;opacity:1;}
	.gitemE017 > text{fill:#ffffff;cursor:pointer;}
	.gitemE017:hover > text{fill:#eeeeee;}
	.rectbackE017{fill:#393939;stroke:#cccccc;stroke-width:1;}
	.arrowpolyE017{fill:#393939;stroke:none;}
	.arrowlineE017{fill:none;stroke:#cccccc;stroke-width:1;}

	.bodyE018{background-color:transparent;}
	.gitemE018 > rect{fill:#393939;cursor:pointer;opacity:1;}
	.gitemE018:hover > rect{fill:#2875A8;opacity:1;}
	.gitemE018 > text{fill:#ffffff;cursor:pointer;}
	.gitemE018:hover > text{fill:#eeeeee;}
	.rectbackE018{fill:#393939;stroke:#cccccc;stroke-width:1;}
	.arrowpolyE018{fill:#393939;stroke:none;}
	.arrowlineE018{fill:none;stroke:#cccccc;stroke-width:1;}

	.bodyE021{background-color:transparent;}
	.gitemE021 > rect{fill:none;cursor:pointer;opacity:1;}
	.gitemE021:hover > rect{fill:#444444;opacity:1;}
	.gitemE021 > text{fill:#ffffff;cursor:pointer;}
	.gitemE021:hover > text{fill:#eeeeee;}
	.rectbackE021{fill:url(#mainGradient);stroke:#9C9C9C;stroke-width:1;}
	.arrowpolyE021{fill:#0A5081;stroke:none;}
	.arrowlineE021{fill:none;stroke:#9C9C9C;stroke-width:1;}
<% } %>
</style>
<script>
var selfName = "combopop001.jsp", winCnt = "cbpop";
var svgWidth = <%=w%>, svgHeight = <%=h%>;
var p;

var realHeight;
var uheight = 15, uwidth = 0;
var mainGrpObj;
var maxWidth = 0;
var cbkd;

var startY = 0;

function onload_func(){
	p = window.parent;
	console.log(selfName+" : loaded");
	p.combopopSvg = this;
	p.objLoaded2('cbpop', this, false);

	cbkd = p.ComboObj.skd;
	mainGrpObj = document.getElementById('mainGrp');
	document.body.className = "body<%=t%>";

<% if(p.equals("001")){ %>
	uwidth = svgWidth;
<% } else if(p.equals("012")){ %>
	uwidth = svgWidth - 4;		// shadow
<% } else if(p.equals("016")){ %>
	uwidth = svgWidth - 8;		// bezel
<% } %>

	if(cbkd == "catakind") init_combopop_catakind();
	else if(cbkd == "media") init_combopop_media();
}

function init_combopop_catakind(){
	//for testing
	//var arCataTitle = ["새카탈로그","새갤러리","새글책","새카탈로그","새갤러리새갤러리새갤러리새갤러리","새글책","새카탈로그","새갤러리","새글책","새카탈로그","새갤러리","새글책"]
	//var alen = arCataTitle.length;
	var alen = p.cataBoxData.arCataTitle.length;

	var ty;
	var arRectObj = new Array();
	for(var i = 0;i < alen;i++){
		ty = uheight*i + startY;

		var gObj = document.createElementNS("http://www.w3.org/2000/svg","g");
		gObj.setAttribute("class", "gitem<%=t%>");

		var rectObj = document.createElementNS("http://www.w3.org/2000/svg","rect");		// background
		rectObj.setAttribute("x", 0);
		rectObj.setAttribute("y", ty);
		rectObj.setAttribute("width", uwidth);
		rectObj.setAttribute("height", uheight);
		rectObj.setAttribute("onmousedown", "do_mousedownCatakind("+i+")");
		gObj.appendChild(rectObj);
		arRectObj.push(rectObj);

		var textObj = document.createElementNS("http://www.w3.org/2000/svg","text");		// text
		textObj.setAttribute("x", 1);
		textObj.setAttribute("y", ty+12);
		textNode = document.createTextNode(p.cataBoxData.arCataTitle[i]);
		//textNode = document.createTextNode(arCataTitle[i]);							// for testing
		textObj.appendChild(textNode);
		textObj.setAttribute("onmousedown", "do_mousedownCatakind("+i+")");
		gObj.appendChild(textObj);

		mainGrpObj.appendChild(gObj);

		var bbox = textObj.getBBox();
		if(bbox.width > maxWidth) maxWidth = bbox.width;
	}

	var comboWidth = p.ComboObj.callobj.getAttribute("data-width");
	if(comboWidth === "content" && maxWidth > svgWidth){
		p.combopopObj.width = maxWidth;
		for(i = 0;i < alen;i++){
			arRectObj[i].setAttribute("width", maxWidth);
		}
	}
	else maxWidth = svgWidth;

	do_afterInit(alen, maxWidth);
}

function init_combopop_media(){
	var alen = p.MediaObj.arMediaTitle.length;
	var ty;
	var arRectObj = [];
	for(var i = 0;i < alen;i++){
		ty = uheight*i + startY;

		var gObj = document.createElementNS("http://www.w3.org/2000/svg","g");
		gObj.setAttribute("class", "gitem<%=t%>");

		var rectObj = document.createElementNS("http://www.w3.org/2000/svg","rect");
		rectObj.setAttribute("x", 0);
		rectObj.setAttribute("y", ty);
		rectObj.setAttribute("width", uwidth);
		rectObj.setAttribute("height", uheight);
		rectObj.setAttribute("onmousedown", "do_mousedownMedia("+i+")");
		gObj.appendChild(rectObj);
		arRectObj.push(rectObj);

		var textObj = document.createElementNS("http://www.w3.org/2000/svg","text");
		textObj.setAttribute("x", 1);
		textObj.setAttribute("y", ty+12);
		textNode = document.createTextNode(p.MediaObj.arMediaTitle[i]);
		textObj.setAttribute("onmousedown", "do_mousedownMedia("+i+")");
		textObj.appendChild(textNode);
		gObj.appendChild(textObj);

		mainGrpObj.appendChild(gObj);

		var bbox = textObj.getBBox();
		if(bbox.width > maxWidth) maxWidth = bbox.width;
	}

	var comboWidth = p.ComboObj.callobj.getAttribute("data-width");
	if(comboWidth == "content" && maxWidth > svgWidth){
		p.combopopObj.width = maxWidth;
		for(i = 0;i < alen;i++){
			arRectObj[i].setAttribute("width", maxWidth);
		}
	}
	else maxWidth = svgWidth;

	do_afterInit(alen, maxWidth);
}

<% if(p.equals("001")){ %>
function do_afterInit(alen, awidth){
	realHeight = uheight*alen;
	document.getElementById('mainsvg').style.height = realHeight + "px";
	p.combopopObj.height = realHeight;

	if(realHeight < svgHeight) p.ComboObj.after_combopopLoad(cbkd, awidth, realHeight);
	else p.ComboObj.after_combopopLoad(cbkd, awidth, svgHeight+2);
}
<% } else if(p.equals("012")){ %>
function do_afterInit(alen, awidth){
	realHeight = uheight*alen;
	document.getElementById('mainsvg').style.height = realHeight + "px";
	p.combopopObj.height = realHeight+4;
	document.getElementById('rectback').setAttribute("height", realHeight+4);

	if(realHeight < svgHeight) p.ComboObj.after_combopopLoad(cbkd, awidth, realHeight+4);
	else p.ComboObj.after_combopopLoad(cbkd, awidth, svgHeight+6);
}
<% } else if(p.equals("016")){ %>
function do_afterInit(alen, awidth){
	document.body.style.overflowY = "hidden";
	realHeight = uheight*alen;
	if(realHeight < svgHeight){
		document.getElementById('mainsvg').style.height = realHeight + "px";
		p.combopopObj.height = realHeight+15;

		document.getElementById('rectshadow').setAttribute("width", awidth-1);
		document.getElementById('rectshadow').setAttribute("height", realHeight+8);
		document.getElementById('rectback').setAttribute("width", awidth-1);
		document.getElementById('rectback').setAttribute("height", realHeight+8);
		if(p.ComboObj.direction === "top"){
			document.getElementById('arrowGrp').setAttribute('transform','translate('+(awidth/2-6)+','+(realHeight+8)+')');
		}
		else if(p.ComboObj.direction === "bottom"){
			document.getElementById('rectshadow').setAttribute("y", 5.5);
			document.getElementById('rectback').setAttribute("y", 5.5);
			document.getElementById('arrowGrp').setAttribute('transform','translate('+(awidth/2-6)+',0)');
			document.getElementById('arrowGon').setAttribute('points','0,6 6,0 12,6');
			document.getElementById('arrowLine1').setAttribute('x1','0');
			document.getElementById('arrowLine1').setAttribute('y1','6');
			document.getElementById('arrowLine1').setAttribute('x2','6');
			document.getElementById('arrowLine1').setAttribute('y2','0');
			document.getElementById('arrowLine2').setAttribute('x1','6');
			document.getElementById('arrowLine2').setAttribute('y1','0');
			document.getElementById('arrowLine2').setAttribute('x2','12');
			document.getElementById('arrowLine2').setAttribute('y2','6');
			document.getElementById('contdiv').style.top = "10px";
		}

		document.getElementById('contdiv').style.overflowY = "hidden";
		p.ComboObj.after_combopopLoad(cbkd, awidth+4, realHeight+15);
	}
	else{
		document.getElementById('mainsvg').style.height = svgHeight + "px";
		document.getElementById('contsvg').style.height = realHeight + "px";
		document.getElementById('contdiv').style.overflowY = "auto";
		p.combopopObj.height = svgHeight+9;

		document.getElementById('rectshadow').setAttribute("width", awidth-1);
		document.getElementById('rectshadow').setAttribute("height", svgHeight+8);
		document.getElementById('rectback').setAttribute("width", awidth-1);
		document.getElementById('rectback').setAttribute("height", svgHeight+8);
		document.getElementById('arrowGrp').setAttribute('transform','translate('+(awidth/2-6)+','+(svgHeight+8)+')');

		p.ComboObj.after_combopopLoad(cbkd, awidth+4, svgHeight+17);
	}
}
<% } %>

function do_mousedownCatakind(n){
	var sdir = p.cataBoxData.arCataDir[n];
	if(sdir === p.PageInfo.cataDir) return;
	p.unload_combopop();
	p.location = p.ServerInfo.get_cataDoculink("ecatalog5."+p.ServerInfo.progExt,sdir,"","","","","");
}
function do_mouseoverCatakindBmp(obj, s){
	obj.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", s);
}

function do_mousedownMedia(n){
	p.MediaObj.playFromList(n);
	p.unload_combopop();
}
</script>
</head>

<body onload="onload_func()">

<% if(p.equals("001")){ %>
<svg version="1.1" id="mainsvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g id="mainGrp"></g>
</svg>

<% } else if(p.equals("012")){ %>
<svg version="1.1" id="mainsvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<defs>
<linearGradient id="mainGradient" x1="0%" y1="0%" x2="0%" y2="100%">
	<stop offset="0%" stop-color="<%=gra1Color%>" />
	<stop offset="100%" stop-color="<%=gra1Color%>" />
</linearGradient>
</defs>

<rect id="rectback" x="0" y="0" width="100%" height="200" fill="url(#mainGradient)" />
<g id="mainGrp" transform="translate(2,2)"></g>
</svg>

<% } else if(p.equals("016")){ %>
<svg version="1.1" id="mainsvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<defs>
<filter id="blurMe">
	<feGaussianBlur in="SourceAlpha" stdDeviation="2" />
</filter>
</defs>

<!-- shadow & background rectangle & arrow -->
<rect id="rectshadow" x="0.5" y="0.5" rx="7" width="100%" height="250" fill="#222222" filter="url(#blurMe)" />
<rect id="rectback" class="rectback<%=t%>" x="0.5" y="0.5" rx="7" width="100%" height="250" />
<g id="arrowGrp">
	<polygon class="arrowpoly<%=t%>" id="arrowGon" points="0,0 6,6 12,0" />
	<line class="arrowline<%=t%>" id="arrowLine1" x1="0" y1="0" x2="6" y2="6" />
	<line class="arrowline<%=t%>" id="arrowLine2" x1="6" y1="6" x2="12" y2="0" />
</g>
</svg>

<div id="contdiv">
<svg version="1.1" id="contsvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g id="mainGrp"></g>
</svg>
</div>
<% } %>

</body>
</html>
