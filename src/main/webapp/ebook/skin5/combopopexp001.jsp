<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%
	String tw = get_param1(request.getParameter("w"),5);
	int w = tw.equals("") ? 0 : Integer.parseInt(tw);
	String th = get_param1(request.getParameter("h"),5);
	int h = th.equals("") ? 0 : Integer.parseInt(th);
	String ds = request.getParameter("d");
	if(ds == null) ds = "";

	String[] ar = mysplit(ds,".");
	String n = "";
	if(ar.length > 1) n = get_param2(ar[1],5);			// SkinInfo.nwclass(n)
	String v = "";
	if(ar.length > 2) v = check_um(ar[2],"um");			// CataInfo.incview(v)
	String p = "";
	if(ar.length > 3) p = get_param2(ar[3],5);			// SkinInfo.cbpopskin(p). 001/012/016
	String c = "";
	if(ar.length > 4) c = get_param1(ar[4],2);			// data-custom. 0/1/...
	String d = check_mainmenu(ar[0], v);				// SkinInfo.mainmenu(d)
	String r = "cbpop";

	pathOfOriCata = webSysDir + "/catImage";
	HashMap Display = new HashMap();
	Display.put("user", get_userSkin(d, v));
	if(!Display.get("user").equals("")) set_userSkin((String)Display.get("user"), Display);

	String gra1Color = "#265a00";
	String gra2Color = "#acc852";
	String textColor = "#ffffff";
	String textColorOver = "#eeeeee";
	String rectColor = "#555555";

	if(!Display.get("user").equals("") && !strNullObject(Display.get("cpcolor_d")).equals("")){
		String[] aaa = mysplit((String)Display.get("cpcolor_d"),",");
		gra1Color = aaa[0];
		gra2Color = aaa[1];
		textColor = aaa[2];
		textColorOver = aaa[3];
		rectColor = aaa[4];
	}
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>ecatalog - combopopexp001</title>
<link rel="stylesheet" type="text/css" href="<%=webServer%>/main.css">
<style>
	html, body{width:100%;height:100%;margin:0;overflow:hidden;}
	#mainsvg{display:block;z-index:2;position:absolute;width:100%;height:100%;}

<% if(p.equals("001")){ %>
	*{font-family:굴림;font-size:12px;}
	#sclass{z-index:3;position:absolute;left:0;top:0;}

<% if(d.equals("011")){ %>
	body{background-color:#D4EBC3;}
	.exp_div_span5d{background-color:#D4EBC3;color:#000000;cursor:pointer;}
	.exp_div_span5d:hover{background-color:#41830F;color:#ffffff;}
<% } else if(d.equals("030")){ %>
	body{background-color:#e8f4f8;}
	.exp_div_span5d{background-color:#e8f4f8;color:#000000;cursor:pointer;}
	.exp_div_span5d:hover{background-color:#2492C6;color:#ffffff;}
<% } else{ %>
	body{background-color:#ededed;}
	.exp_div_span5d{background-color:#ededed;color:#000000;cursor:pointer;}
	.exp_div_span5d:hover{background-color:#404040;color:#ffffff;}
<% } %>

<% } else if(p.equals("012")){ %>
	*{font-family:굴림;font-size:11px;}
	#sclass{z-index:3;position:absolute;left:2px;top:2px;}
	.exp_div_span5d{background:none;color:<%=textColor%>;cursor:pointer;}
	.exp_div_span5d:hover{background-color:<%=rectColor%>;color:<%=textColorOver%>;}

<% } else if(p.equals("016")){ %>
	*{font-family:굴림;font-size:12px;}
	#sclass{z-index:3;position:absolute;left:5px;top:5px;}

<% if(d.equals("021")){ %>
	.exp_div_span5d{background:none;color:#ffffff;cursor:pointer;}
	.exp_div_span5d:hover{background-color:#444444;color:#eeeeee;}
	.rectback{fill:url(#mainGradient);stroke:#9C9C9C;stroke-width:1;}
	.arrowpoly{fill:#0A5081;stroke:none;}
	.arrowline{fill:none;stroke:#9C9C9C;stroke-width:1;}
<% } else if(d.equals("017")){ %>
	.exp_div_span5d{background:none;color:#ffffff;cursor:pointer;}
	.exp_div_span5d:hover{background-color:#3A750A;color:#eeeeee;}
	.rectback{fill:#393939;stroke:#cccccc;stroke-width:1;}
	.arrowpoly{fill:#393939;stroke:none;}
	.arrowline{fill:none;stroke:#cccccc;stroke-width:1;}
<% } else if(d.equals("018")){ %>
	.exp_div_span5d{background:none;color:#ffffff;cursor:pointer;}
	.exp_div_span5d:hover{background-color:#2875A8;color:#eeeeee;}
	.rectback{fill:#393939;stroke:#cccccc;stroke-width:1;}
	.arrowpoly{fill:#393939;stroke:none;}
	.arrowline{fill:none;stroke:#cccccc;stroke-width:1;}
<% } else{ %>
	.exp_div_span5d{background:none;color:#ffffff;cursor:pointer;}
	.exp_div_span5d:hover{background-color:#3A750A;color:#eeeeee;}
	.rectback{fill:#152e0a;stroke:#a4c37b;stroke-width:1;}
	.arrowpoly{fill:#152e0a;stroke:none;}
	.arrowline{fill:none;stroke:#a4c37b;stroke-width:1;}
<% } %>

<% } %>
</style>
<script src="<%=webServer%>/skin5/explorer.js"></script>
<script>
var selfName = "combopopexp001.jsp", winCnt = "cbpop";
var svgWidth = <%=w%>, svgHeight = <%=h%>;
var p;

var incview = "<%=v%>";
var downKind = 0;

function onload_func(){
	p = window.parent;
	console.log(selfName+" : loaded");
	p.combopopSvg = this;
	p.objLoaded2('combo', this, false);

	skin5Server = "<%=webServer%>/skin5";
	expDiv = document.getElementById('sclass');
	exp_initVar("d2");

<% if(p.equals("016")){ %>
	expDiv.style.width = (svgWidth-4) + "px";
<% } %>

	expSplit = 1;
	expCurrCode = p.PageInfo.cateCode;
	expChild = p.cataBoxData.linkCate;
	expData = p.cataBoxData.expData;
	expView = p.cataBoxData.expView;

	exp_showOpening(true);

<% if(p.equals("001")){ %>
	p.ComboObj.after_combopopLoad("catakind", svgWidth, svgHeight+2);

<% } else if(p.equals("012")){ %>
	document.getElementById('rectback').setAttribute("height", svgHeight);
	p.ComboObj.after_combopopLoad("catakind", svgWidth, svgHeight+6);

<% } else if(p.equals("016")){ %>
	document.getElementById('rectshadow').setAttribute("width", svgWidth-1);
	document.getElementById('rectshadow').setAttribute("height", svgHeight+8);
	document.getElementById('rectback').setAttribute("width", svgWidth-1);
	document.getElementById('rectback').setAttribute("height", svgHeight+8);
	document.getElementById('arrowGrp').setAttribute('transform','translate('+(svgWidth/2-6)+','+(svgHeight+8)+')');

	p.ComboObj.after_combopopLoad("catakind", svgWidth+4, svgHeight+17);
<% } %>
}

function exp_itemClick(pno, obj){
	var aaa = expData[pno].split("|");
	var sdir = aaa[3];
	if(sdir == "" || sdir == p.PageInfo.cataDir) return;

	p.unload_combopop();
	p.location = p.ServerInfo.get_cataDoculink("ecatalog5."+p.ServerInfo.progExt,sdir,"","","","","");
}
</script>
</head>

<body onload="onload_func()">

<% if(p.equals("001")){ %>
<svg version="1.1" id="mainsvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g id="mainGrp"></g>
</svg>

<% } else if(p.equals("012")){ %>
<svg version="1.1" id="mainsvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<defs>
<linearGradient id="mainGradient" x1="0%" y1="0%" x2="0%" y2="100%">
	<stop offset="0%" stop-color="<%=gra1Color%>" />
	<stop offset="100%" stop-color="<%=gra2Color%>" />
</linearGradient>
</defs>

<rect id="rectback" x="0" y="0" width="100%" height="200" fill="url(#mainGradient)" />
<g id="mainGrp" transform="translate(2,2)"></g>
</svg>

<% } else if(p.equals("016")){ %>
<svg version="1.1" id="mainsvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<defs>
<filter id="blurMe">
	<feGaussianBlur in="SourceAlpha" stdDeviation="2" />
</filter>
</defs>

<!-- shadow & background rectangle & arrow -->
<rect id="rectshadow" x="0.5" y="0.5" rx="7" width="100%" height="250" fill="#222222" filter="url(#blurMe)" />
<rect id="rectback" class="rectback" x="0.5" y="0.5" rx="7" width="100%" height="250" />
<g id="mainGrp" transform="translate(5,5)"></g>
<g id="arrowGrp">
	<polygon class="arrowpoly" points="0,0 6,6 12,0" />
	<polyline class="arrowline" points="0,0 6,6 12,0" />
</g>
</svg>
<% } %>

<div id="sclass"></div>

</body>
</html>
