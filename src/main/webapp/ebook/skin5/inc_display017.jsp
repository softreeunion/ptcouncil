<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%
	request.setCharacterEncoding("utf-8");

	String incView = request.getParameter("incview");
	String indexType = request.getParameter("indexType");
	String combokind = request.getParameter("combokind");
	String downFile = request.getParameter("downFile");
	String downFileName = request.getParameter("downFileName");
	String downexten = request.getParameter("downexten");
	String downtext = URLDecoder.decode(request.getParameter("downtext"),"utf-8");
	String mediaFile = request.getParameter("mediaFile");
	String webServer = request.getParameter("webServer");
	String printKindmc = request.getParameter("printKindmc");
	String dirName = URLDecoder.decode(request.getParameter("dirName"),"utf-8");
	String searchFile = request.getParameter("searchFile");
	String mokchaFile = request.getParameter("mokchaFile");
	String skinMedia = request.getParameter("skinMedia");
	String skinSpacer = request.getParameter("skinSpacer");
	String mailButtonHide = request.getParameter("mailButtonHide");
	String Dir = request.getParameter("Dir");
	String catimage = request.getParameter("catimage");
	String webpath = webServer + "/catImage" + catimage + "/" + Dir;
%>
<% if(incView.equals("d")){ %>
<!-- background -->
<div id="backsect" class="back5d017"></div>

<!-- navigation -->
<nav id="mmsect" class="mm5dD017-idxR">

<!-- thumb preview -->
<div id="thumbsect" class="thumb5dD017">
<div id="thumb_firstbtn"><a href="javascript:MenuObj.firstbtnClick();" title="처음 페이지로 이동"><img src="<%=webServer%>/skin5/mm016/paging_first.png" width="13" height="11" alt="처음 페이지"
  onmouseover="this.src='<%=webServer%>/skin5/mm016/paging_first_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm016/paging_first.png'"></a></div>
<div id="thumb_prevbtn"><a href="javascript:MenuObj.prevbtnClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/mm017/paging_prev.png" width="12" height="17" alt="이전 페이지"
  onmouseover="this.src='<%=webServer%>/skin5/mm017/paging_prev_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm017/paging_prev.png'"></a></div>
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g id="thumb_pagingGrp"></g>
</svg>
<div id="thumb_nextbtn"><a href="javascript:MenuObj.nextbtnClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/mm017/paging_next.png" width="12" height="17" alt="다음 페이지"
  onmouseover="this.src='<%=webServer%>/skin5/mm017/paging_next_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm017/paging_next.png'"></a></div>
<div id="thumb_lastbtn"><a href="javascript:MenuObj.lastbtnClick();" title="마지막 페이지로 이동"><img src="<%=webServer%>/skin5/mm016/paging_last.png" width="13" height="11" alt="마지막 페이지"
onmouseover="this.src='<%=webServer%>/skin5/mm016/paging_last_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm016/paging_last.png'"></a></div>
</div>

<ul class="mm_ulE001">
<li id="closebtn" class="closebtn5dD017"><a href="javascript:closebtnClick();" title="창닫기"><img src="<%=webServer%>/skin5/mm017/closebtn.png" width="23" height="24" alt="창닫기"
  onmouseover="this.src='<%=webServer%>/skin5/mm017/closebtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm017/closebtn.png'"></a></li>
<li id="fullscrbtn" class="fullscrbtn5dD017"><a href="javascript:fullscrbtnClick();" title="전체화면으로 보기"><img src="<%=webServer%>/skin5/mm017/fullscrbtn.png" width="23" height="24" alt="전체화면"
  onmouseover="this.src='<%=webServer%>/skin5/mm017/fullscrbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm017/fullscrbtn.png'"></a></li>
</ul>

<% if(searchFile.equals("true")){ %>
<div id="searchsect" class="search5dD017">
<form name="searchform" method="get" action="#" onsubmit="return do_searchbtnClick(document.searchform.qtxt.value)" title="페이지 검색">
<input type="search" class="searchtxt5dD017" name="qtxt" title="검색어 입력" onfocus="do_qtxtFocusIn(this)">
<input type="image" class="searchbtn5dD017 hover_opac8" src="<%=webServer%>/skin5/mm017/search_btn.png" alt="검색 시작">
</form>
</div>
<% } %>

</nav>

<!-- lowersect -->
<div id="lowersect" class="lower5dD017">
<ul class="mm_ulE002">
<% if(!printKindmc.equals("000")){ %>
<li id="printbtn" class="printbtn5dD017"><a href="javascript:printbtnClick();" title="인쇄하기">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="45" height="70" 
  onmouseover="MenuObj.printbtnOver();" onmouseout="MenuObj.printbtnOut();">
<defs>
<mask id="printbtn_mask">
	<rect x="6" y="30" width="33" height="38" style="stroke:none;fill:#ffffff" />
</mask>
</defs>
<image x="10" y="45" width="27" height="10" xlink:href="<%=webServer%>/skin5/mm016/printbtn_string.png" />
<image x="6" y="0" id="printbtn_paper1" width="33" height="18" xlink:href="<%=webServer%>/skin5/mm016/printbtn_paper1.png" />
<image x="0" y="18" width="45" height="19" xlink:href="<%=webServer%>/skin5/mm016/printbtn_body.png" />
<image x="33" y="21" width="8" height="8" xlink:href="<%=webServer%>/skin5/mm016/printbtn_lamp.png" />
<image x="33" y="21" id="printbtn_lampon" width="8" height="8" xlink:href="<%=webServer%>/skin5/mm016/printbtn_lampon.png" visibility="hidden" />
<image x="6" y="0" id="printbtn_paper2" width="33" height="38" xlink:href="<%=webServer%>/skin5/mm016/printbtn_paper2.png" mask="url(#printbtn_mask)" />
</svg></a>
</li>
<% } %>

<% if(mokchaFile.equals("true")){ %>
<li id="indexbtn" class="indexbtn5dD017"><a href="javascript:indexbtnClick();" title="목차 보기">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="45" height="70" 
   onmouseover="MenuObj.indexbtnOver();" onmouseout="MenuObj.indexbtnOut();">
<image x="13" y="45" width="19" height="10" xlink:href="<%=webServer%>/skin5/mm016/indexbtn_string.png" />
<image x="0" y="11" width="28" height="28" xlink:href="<%=webServer%>/skin5/mm016/indexbtn_book.png" />
<image x="7" y="5" id="indexbtn_glasses" width="38" height="38" xlink:href="<%=webServer%>/skin5/mm016/indexbtn_glasses.png" />
</svg></a>
</li>
<% } %>

<li id="helpbtn" class="helpbtn5dD017"><a href="javascript:helpbtnClick();" title="도움말 보기">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="41" height="70" 
  onmouseover="MenuObj.helpbtnOver();" onmouseout="MenuObj.helpbtnOut();">
<defs>
<mask id="helpbtn_mask">
	<rect x="0" y="13" width="41" height="24" style="stroke:none;fill:#ffffff" />
</mask>
</defs>
<image x="6" y="45" width="26" height="10" xlink:href="<%=webServer%>/skin5/mm016/helpbtn_string.png" />
<image x="0" y="5" width="41" height="8" xlink:href="<%=webServer%>/skin5/mm016/helpbtn_wintitle.png" />
<image x="0" y="13" id="helpbtn_winbody" width="41" height="24" xlink:href="<%=webServer%>/skin5/mm016/helpbtn_winbody.png" mask="url(#helpbtn_mask)" />
</svg></a>
</li>

<% if(!mailButtonHide.equals("Y")){ %>
<li id="mailbtn" class="mailbtn5dD017"><a href="javascript:mailbtnClick();" title="전자우편 발송">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="39" height="70">
<image x="8" y="45" width="18" height="10" xlink:href="<%=webServer%>/skin5/mm016/mailbtn_string.png" />
<image x="0" y="9" id="mailbtn_body" width="39" height="28" xlink:href="<%=webServer%>/skin5/mm016/mailbtn_body.png" />
</svg></a>
</li>
<% } %>
</ul>

<!-- combobox -->
<% if(!combokind.equals("000")){ %>
<a href="javascript:comboClick();" title="다른 카탈로그로 이동"><div id="combosect" class="cbbox5dD017" width="116" height="24"
data-width="display" data-align="right" data-direction="top" data-vpos="-3"><img src="<%=webServer%>/skin5/mm017/combo_box.png"
onmouseover="this.src='<%=webServer%>/skin5/mm017/combo_box_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm017/combo_box.png'" alt="다른 카탈로그로 이동"></a></div>
<% } %>


<!-- download -->
<% if(downFile.equals("true")){ %>
<div id="downsect" class="down5dD017">
<a href="<%=webpath%>/<%=downFileName%>" download="<%=webpath%>/<%=downFileName%>"
title="파일 다운로드"><img src="<%=webServer%>/skin5/icon/file_<%=downexten%>.gif" class="down_img" alt="파일 다운로드"><%=downtext%></a>
</div>
<% } %>

<!-- media -->
<% if(mediaFile.equals("true") && !skinMedia.equals("000")){ %>
<div id="mediasect" class="media5dD017" data-width="display" data-align="right" data-direction="top" data-hpos="1">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10" height="10">
<!-- animation -->
<g>
	<rect id="mediarect1" x="0" y="0" width="2" height="1" fill="#74CF00" />
	<rect id="mediarect2" x="4" y="0" width="2" height="1" fill="#74CF00" />
	<rect id="mediarect3" x="8" y="0" width="2" height="1" fill="#74CF00" />
	<rect id="mediarect4" x="0" y="2" width="2" height="1" fill="#74CF00" />
	<rect id="mediarect5" x="4" y="2" width="2" height="1" fill="#74CF00" />
	<rect id="mediarect6" x="8" y="2" width="2" height="1" fill="#74CF00" />
	<rect id="mediarect7" x="0" y="4" width="2" height="1" fill="#74CF00" />
	<rect id="mediarect8" x="4" y="4" width="2" height="1" fill="#74CF00" />
	<rect id="mediarect9" x="8" y="4" width="2" height="1" fill="#74CF00" />
	<rect id="mediarect10" x="0" y="6" width="2" height="1" fill="#74B800" />
	<rect id="mediarect11" x="4" y="6" width="2" height="1" fill="#74B800" />
	<rect id="mediarect12" x="8" y="6" width="2" height="1" fill="#74B800" />
	<rect id="mediarect13" x="0" y="8" width="2" height="1" fill="#748A00" />
	<rect id="mediarect14" x="4" y="8" width="2" height="1" fill="#748A00" />
	<rect id="mediarect15" x="8" y="8" width="2" height="1" fill="#748A00" />
</g>
</svg>

<ul class="media_ul5dD017">
<li id="media_backbtn" class="media_backbtn5dD017"><a href="javascript:MediaObj.backbtnClick();" title="이전 곡">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="8" height="8">
	<polygon points="3,4 8,8 8,0" fill="#D1DEE2" />
	<line x1="0.5" y1="0" x2="0.5" y2="8" stroke="#D1DEE2" stroke-width="3" />
</svg></a>
</li>
<li id="media_playbtn" class="media_playbtn5dD017"><a href="javascript:MediaObj.playbtnClick();" title="재생">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6" height="8">
	<polygon points="0,0 6,4 0,8" fill="#D1DEE2" />
</svg></a>
</li>
<li id="media_nextbtn" class="media_nextbtn5dD017"><a href="javascript:MediaObj.nextbtnClick();" title="다음 곡">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6" height="8">
	<polygon points="0,0 5,4 0,8" fill="#D1DEE2" />
	<line x1="7.5" y1="0" x2="7.5" y2="8" stroke="#D1DEE2" stroke-width="3" />
</svg></a>
</li>
<li id="media_stopbtn" class="media_stopbtn5dD017"><a href="javascript:MediaObj.stopbtnClick();" title="멈춤">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6" height="6">
	<polygon points="0,0 6,0 6,6 0,6" fill="#D1DEE2" />
</svg></a>
</li>
<li id="media_listbtn" class="media_listbtn5dD017"><a href="javascript:MediaObj.listbtnClick();" title="재생목록">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="8" height="9">
	<polygon points="4,0 8,5 0,5" fill="#D1DEE2" />
	<line x1="0" y1="7" x2="8" y2="7" stroke="#D1DEE2" stroke-width="2" />
</svg></a>
</li>
</ul>
<audio id="mediactrl" class="audio_ctrlr_hidden">브라우저가 audio를 지원하지 않습니다.</audio>
</div>
<% } %>

<!-- sns -->
<nav id="snssect" class="sns5dD017">
<ul class="sns_ul5dD017">
<li id="facebookbtn" class="sns_liE010 hover_opac8"><a href="javascript:facebookbtnClick();" 
  title="페이스북 공유"><img src="<%=webServer%>/skin5/mm010/facebookbtn.png" class="sns_liE010" alt="페이스북"></a></li>
<li id="twitterbtn" class="sns_liE010 hover_opac8"><a href="javascript:twitterbtnClick();" 
  title="트위터 공유"><img src="<%=webServer%>/skin5/mm010/twitterbtn.png" class="sns_liE010" alt="트위터"></a></li>
</ul>
</nav>

</div>

<!-- thumb pop -->
<div id="popthumbsect" class="popthumb5dD017">
<svg version="1.1" id="popthumbsvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<defs>
	<clipPath id="pthumbclip" clipPathUnits="userSpaceOnUse">
		<rect id="pthumbcliprect" x="10" y="10" width="50" height="50" rx="4" />
	</clipPath>
</defs>
<g>
	<image id="pthumbrect1" x="0" y="3" width="8" height="7" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb1.png" />
	<image id="pthumbrect2" x="8" y="3" width="113" height="7" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb2.png" preserveAspectRatio="none" />
	<image id="pthumbrect3" x="121" y="3" width="10" height="7" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb3.png" />
	<image id="pthumbrect4" x="0" y="10" width="8" height="82" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb4.png" preserveAspectRatio="none" />
	<image id="pthumbrect5" x="8" y="10" width="113" height="82" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb5.png" preserveAspectRatio="none" />
	<image id="pthumbrect6" x="121" y="10" width="10" height="82" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb6.png" preserveAspectRatio="none" />
	<image id="pthumbrect7" x="0" y="92" width="8" height="11" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb7.png" />
	<image id="pthumbrect8" x="8" y="92" width="113" height="11" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb8.png" preserveAspectRatio="none" />
	<image id="pthumbrect9" x="121" y="92" width="10" height="11" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb9.png" />
	<image id="pthumbarrow" x="65" y="0" width="9" height="5" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb0.png" />
</g>
<g id="pthumbimgGrp"></g>
</svg>
</div>

<script>
MenuObj = {storedStr:'', totalWidth:0, thumbWidth:0, thumbHeight:0, pgdivWidth:0, pgdivHeight:0, arThumbNode:[],
	pthumbL:undefined, pthumbR:undefined, pthumbImgGrp:undefined, pagingGrpObj:undefined, backDo:0, pc:undefined, firstPageApply:true,
	arPagingObj:[], arPageNo:[], arLeftRight:[], actPair:true, seq:0, btnY:0, btnTimer:undefined, printLamp:undefined, printPaper1:undefined, printPaper2:undefined,
	indexGlass:undefined, helpBody:undefined};

backDiv = document.getElementById("backsect");
thumbDiv = document.getElementById("thumbsect");
popthumbDiv = document.getElementById("popthumbsect");
if(document.getElementById("downsect")) downDiv = document.getElementById("downsect");
if(document.getElementById("combosect")) comboDiv = document.getElementById("combosect");
if(document.getElementById("searchsect")) searchDiv = document.getElementById("searchsect");
if(document.getElementById("mediasect")) mediaDiv = document.getElementById("mediasect");
if(document.getElementById("snssect")) snsDiv = document.getElementById("snssect");

MenuObj.onload_func2 = function(){
	if(ScreenInfo.onesmc === true) MenuObj.actPair = false;
	if(LinkInfo.facebookStr === "" && document.getElementById("facebookbtn")) document.getElementById("facebookbtn").style.visibility = "hidden";
	if(LinkInfo.twitterStr === "" && document.getElementById("twitterbtn")) document.getElementById("twitterbtn").style.visibility = "hidden";

	// button animation
	if(document.getElementById("printbtn_lampon")) MenuObj.printLamp = document.getElementById("printbtn_lampon");
	if(document.getElementById("printbtn_paper1")) MenuObj.printPaper1 = document.getElementById("printbtn_paper1");
	if(document.getElementById("printbtn_paper2")) MenuObj.printPaper2 = document.getElementById("printbtn_paper2");
	if(document.getElementById("indexbtn_glasses")) MenuObj.indexGlass = document.getElementById("indexbtn_glasses");
	if(document.getElementById("helpbtn_winbody")) MenuObj.helpBody = document.getElementById("helpbtn_winbody");

	var h = 60;
	MenuObj.thumbHeight = h;
	MenuObj.thumbWidth = Math.round(h*ScreenInfo.ratioWH);
	var w = (ScreenInfo.onesmc === true) ? MenuObj.thumbWidth : MenuObj.thumbWidth*2;
	MenuObj.totalWidth = w + 18;

	MenuObj.pthumbImgGrp = document.getElementById("pthumbimgGrp");

	document.getElementById("pthumbarrow").setAttribute("x", 4+w/2);		//8-4=4(left margin - arrow width)
	document.getElementById("pthumbrect2").setAttribute("width", w+0.5);
	document.getElementById("pthumbrect3").setAttribute("x", 8+w);

	document.getElementById("pthumbrect4").setAttribute("height", h+1);
	document.getElementById("pthumbrect5").setAttribute("width", w+3);			// 사이가 벌어짐(원래는 w)
	document.getElementById("pthumbrect5").setAttribute("height", h+1);
	document.getElementById("pthumbrect6").setAttribute("x", 8+w);
	document.getElementById("pthumbrect6").setAttribute("height", h+1);

	document.getElementById("pthumbrect7").setAttribute("y", 11+h);
	document.getElementById("pthumbrect8").setAttribute("width", w+0.5);
	document.getElementById("pthumbrect8").setAttribute("y", 11+h);
	document.getElementById("pthumbrect9").setAttribute("x", 8+w);
	document.getElementById("pthumbrect9").setAttribute("y", 11+h);

	document.getElementById("pthumbcliprect").setAttribute("width", w);
	document.getElementById("pthumbcliprect").setAttribute("height", h);

	MenuObj.pgdivWidth = 348;
	MenuObj.pgdivHeight = 24;

	MenuObj.pagingGrpObj = document.getElementById('thumb_pagingGrp');

	if(MenuObj.firstPageApply === true && PageInfo.firstPage === 0) MenuObj.backDo = 1;

	MenuObj.pc = new PagingCont();
	MenuObj.pc.totalArticles = PageInfo.cataPages + MenuObj.backDo;
	MenuObj.pc.set_articlesOnPage(10);
	MenuObj.pc.set_pagesOnSheet(10);
	MenuObj.pc.set_currentPage(1);
	MenuObj.make_paging();
	//MenuObj.show_currPageItem();
}
MenuObj.make_paging = function(){
	var k = 0;
	var tx = 0;
	for(var i = MenuObj.pc.startArticle;i <= MenuObj.pc.finalArticle;i++){
		if(k%2 == 0){
			MenuObj.make_pagingitem(tx, PageInfo.get_showPage(i+1-MenuObj.backDo), i+1-MenuObj.backDo, true);
			tx += 24;
		}
		else{
			MenuObj.make_pagingitem(tx, PageInfo.get_showPage(i+1-MenuObj.backDo), i+1-MenuObj.backDo, false);
			tx += 28;
		}
		k++;
	}
}
MenuObj.make_pagingitem = function(tx, stitle, pno, leftRight){
	if(pno === 0) return;

	var gObj = document.createElementNS("http://www.w3.org/2000/svg","g");
	gObj.setAttribute('transform','translate('+tx+','+0+')');
	gObj.setAttribute("onclick", "MenuObj.do_itemClick("+pno+")");
	gObj.setAttribute("cursor", "pointer");

	var imgsrc = (leftRight === true) ? "<%=webServer%>/skin5/mm017/paging_left.png" : "<%=webServer%>/skin5/mm017/paging_right.png";
	var backObj = document.createElementNS("http://www.w3.org/2000/svg","image");
	backObj.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", imgsrc);
	backObj.setAttribute("width", 23);
	backObj.setAttribute("height", 24);
	gObj.appendChild(backObj);

	var textObj = document.createElementNS("http://www.w3.org/2000/svg","text");
	textObj.setAttribute("class", "thumb_pagenum5dD017");
	textObj.setAttribute("x", 12);
	textObj.setAttribute("y", 16);
	textObj.setAttribute("text-anchor", "middle");
	textNode = document.createTextNode(stitle);
	textObj.appendChild(textNode);
	gObj.appendChild(textObj);

	//textObj.setAttribute("textLength", 5);
	//textObj.setAttribute("lengthAdjust", "spacingAndGlyphs");

	MenuObj.arPageNo.push(pno);
	MenuObj.arLeftRight.push(leftRight);
	var idx = MenuObj.arPagingObj.push(gObj) - 1;

	if(pno === 0) gObj.setAttribute("visibility", "hidden");
	else{
		gObj.setAttribute("onmouseover", "MenuObj.do_mouseOver(evt,"+idx+","+pno+","+leftRight+")");
		gObj.setAttribute("onmouseout", "MenuObj.do_mouseOut(evt,"+idx+","+pno+","+leftRight+")");
	}

	MenuObj.pagingGrpObj.appendChild(gObj);
}
MenuObj.make_thumb = function(tx, url){
	var popThumb = document.createElementNS("http://www.w3.org/2000/svg","image");
	popThumb.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", url);
	popThumb.setAttribute("stroke", "#ff6600");
	popThumb.setAttribute("stroke-width", "1");
	popThumb.setAttribute("x", tx);
	popThumb.setAttribute("y", 10);
	popThumb.setAttribute("width", MenuObj.thumbWidth+0.5);
	popThumb.setAttribute("height", MenuObj.thumbHeight);
	popThumb.setAttribute("clip-path", "url(#pthumbclip)");
	//popThumb.setAttribute("visibility", "hidden");
	return popThumb;
}
MenuObj.do_mouseOver = function(e, idx, pno, lr){
	var areaRect;
	if(lr === true) areaRect = MenuObj.arPagingObj[idx+1].getBoundingClientRect();
	else areaRect = MenuObj.arPagingObj[idx].getBoundingClientRect();

	MenuObj.change_pagingBack(lr, idx, "_over");
	if(popthumbDiv.style.visibility == "visible") return;

	popthumbDiv.style.left = (areaRect.left-MenuObj.totalWidth/2)+"px";
	popthumbDiv.style.visibility = "visible";

	if(lr === true){
		var aaa = FileInfo.thumbFilePath(pno,"s","","*");
		MenuObj.pthumbL = MenuObj.make_thumb(8, aaa[0]);
		MenuObj.pthumbImgGrp.appendChild(MenuObj.pthumbL);

		var bbb = FileInfo.thumbFilePath(pno+1,"s","","*");
		if(bbb[0] != ""){
			MenuObj.pthumbR = MenuObj.make_thumb(8+MenuObj.thumbWidth, bbb[0]);
			MenuObj.pthumbImgGrp.appendChild(MenuObj.pthumbR);
		}
	}
	else{
		var aaa = FileInfo.thumbFilePath(pno,"s","","*");
		MenuObj.pthumbR = MenuObj.make_thumb(8+MenuObj.thumbWidth, aaa[0]);
		MenuObj.pthumbImgGrp.appendChild(MenuObj.pthumbR);

		var bbb = FileInfo.thumbFilePath(pno-1,"s","","*");
		if(bbb[0] != ""){
			MenuObj.pthumbL = MenuObj.make_thumb(8, bbb[0]);
			MenuObj.pthumbImgGrp.appendChild(MenuObj.pthumbL);
		}
	}
}
MenuObj.do_mouseOut = function(e, idx, pno, lr){
	if(pno != PageInfo.currentPage) MenuObj.change_pagingBack(lr, idx, "");

	if(MenuObj.pthumbL) MenuObj.pthumbImgGrp.removeChild(MenuObj.pthumbL);
	MenuObj.pthumbL = undefined;

	if(MenuObj.pthumbR) MenuObj.pthumbImgGrp.removeChild(MenuObj.pthumbR);
	MenuObj.pthumbR = undefined;

	popthumbDiv.style.visibility = "hidden";
}
MenuObj.change_pagingBack = function(lr, idx, sover){
	if(lr === true){
		MenuObj.arPagingObj[idx].childNodes[0].setAttribute("xlink:href","<%=webServer%>/skin5/mm017/paging_left"+sover+".png");
		if(MenuObj.actPair === true && MenuObj.arPagingObj[idx+1]){
			MenuObj.arPagingObj[idx+1].childNodes[0].setAttribute("xlink:href","<%=webServer%>/skin5/mm017/paging_right"+sover+".png");
		}
	}
	else{
		MenuObj.arPagingObj[idx].childNodes[0].setAttribute("xlink:href","<%=webServer%>/skin5/mm017/paging_right"+sover+".png");
		if(MenuObj.actPair === true && MenuObj.arPagingObj[idx-1]){
			MenuObj.arPagingObj[idx-1].childNodes[0].setAttribute("xlink:href","<%=webServer%>/skin5/mm017/paging_left"+sover+".png");
		}
	}
}
MenuObj.show_currPageItem = function(){
	var alen = MenuObj.arPageNo.length;
	for(var i = 0;i < alen;i++){
		if(MenuObj.arPageNo[i] == PageInfo.currentPage){
			MenuObj.change_pagingBack(MenuObj.arLeftRight[i], i, "_over");
		}
		else{
			MenuObj.change_pagingBack(MenuObj.arLeftRight[i], i, "");
		}
		if(MenuObj.actPair === true) i++;
	}
}

/// ############################## button ############################## ///
MenuObj.do_itemClick = function(pno){
	var n = PermitMan.get_directGoPerm("thumbpaging", pno, "", "");			// device, spage, pageshow, state
	if(n < 0) return;
	go_general(n, "system");
}
MenuObj.firstbtnClick = function(){				// 처음 시트로
	if(MenuObj.pc.currentPage === 1) return;
	MenuObj.remove_children();
	MenuObj.pc.set_currentPage(1);
	MenuObj.make_paging();
}
MenuObj.prevbtnClick = function(){				// 이전 시트로
	if(MenuObj.pc.currentPage === 1) return;
	MenuObj.remove_children();
	MenuObj.pc.set_currentPage(MenuObj.pc.currentPage-1);
	MenuObj.make_paging();
}
MenuObj.nextbtnClick = function(){				// 다음 시트로
	if(MenuObj.pc.currentPage === MenuObj.pc.totalPages) return;
	MenuObj.remove_children();
	MenuObj.pc.set_currentPage(MenuObj.pc.currentPage+1);
	MenuObj.make_paging();
}
MenuObj.lastbtnClick = function(){				// 끝 시트로
	if(MenuObj.pc.currentPage === MenuObj.pc.totalPages) return;
	MenuObj.remove_children();
	MenuObj.pc.set_currentPage(MenuObj.pc.totalPages);
	MenuObj.make_paging();
}
MenuObj.remove_children = function(){
	var nodes = MenuObj.pagingGrpObj.childNodes;
	for(var i = nodes.length-1;i > 0;i--){
		MenuObj.pagingGrpObj.removeChild(nodes[i]);
	}
	MenuObj.arPagingObj.length = 0;
	MenuObj.arPageNo.length = 0;
	MenuObj.arLeftRight.length = 0;
}
// button animation
MenuObj.printbtnOver = function(){
	MenuObj.btnY = 0;
	if(MenuObj.btnTimer == undefined) MenuObj.btnTimer = setInterval(MenuObj.printbtn_animate, 40);
}
MenuObj.printbtn_animate = function(){
	if(MenuObj.seq%10 == 0) MenuObj.printLamp.setAttribute("visibility", "visible");
	else if(MenuObj.seq%10 == 5) MenuObj.printLamp.setAttribute("visibility", "hidden");

	MenuObj.printPaper1.setAttribute("y", MenuObj.btnY);
	MenuObj.printPaper2.setAttribute("y", MenuObj.btnY);
	MenuObj.btnY++;

	MenuObj.seq++;
	if(MenuObj.btnY >= 18) MenuObj.btnY = 18;
}
MenuObj.printbtnOut = function(){
	clearInterval(MenuObj.btnTimer);
	MenuObj.btnTimer = undefined;
	MenuObj.printLamp.setAttribute("visibility", "hidden");
	MenuObj.printPaper1.setAttribute("y", 0);
	MenuObj.printPaper2.setAttribute("y", 0);
	MenuObj.seq = 0;
}

MenuObj.indexbtnOver = function(){
	if(MenuObj.seq == 0) MenuObj.seq = 20;
	if(MenuObj.btnTimer == undefined) MenuObj.btnTimer = setInterval(MenuObj.indexbtn_animate, 30);
}
MenuObj.indexbtn_animate = function(){
	var rad = (MenuObj.seq * 5) * Math.PI / 180;
	MenuObj.indexGlass.setAttribute("x", 2+5*Math.sin(rad));
	MenuObj.indexGlass.setAttribute("y", 5+5*Math.cos(rad));
	MenuObj.seq++;
}
MenuObj.indexbtnOut = function(){
	clearInterval(MenuObj.btnTimer);
	MenuObj.btnTimer = undefined;
}

MenuObj.helpbtnOver = function(){
	if(MenuObj.btnTimer == undefined) MenuObj.btnTimer = setInterval(MenuObj.helpbtn_animate, 30);
}
MenuObj.helpbtn_animate = function(){
	MenuObj.btnY = 13 - MenuObj.seq;
	if(MenuObj.btnY <= 0) MenuObj.btnY = 0;
	MenuObj.helpBody.setAttribute("y", MenuObj.btnY);
	MenuObj.seq++;
}
MenuObj.helpbtnOut = function(){
	clearInterval(MenuObj.btnTimer);
	MenuObj.btnTimer = undefined;
	MenuObj.seq = 0;
	MenuObj.helpBody.setAttribute("y", 13);
}
MenuObj.show_pageNumber = function(){
	if(MenuObj.pc.contain_article(PageInfo.currentPage+MenuObj.backDo)) MenuObj.show_currPageItem();
	else{
		MenuObj.remove_children();
		MenuObj.pc.set_currentPage(MenuObj.pc.get_pageFromArticle(PageInfo.currentPage+MenuObj.backDo));
		MenuObj.make_paging();
		MenuObj.show_currPageItem();
	}
}
MenuObj.onresize_func = function(){
}
</script>


<% } else if(incView.equals("m")){ %>
<!-- navigation -->
<nav id="mmsect" class="mm5mD017">

<!-- thumb preview -->
<div id="thumbsect" class="thumb5mD017">
<svg version="1.1" id="thumbsvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g id="thumb_pagingGrp"></g>
</svg>
</div>

</nav>

<!-- background -->
<div id="backsect" class="back5m017"></div>

<!-- lowersect -->
<div id="lowersect" class="lower5mD017">
<ul class="mm_ulE002">
<% if(!printKindmc.equals("000")){ %>
<li id="printbtn" class="printbtn5mD017"><a href="javascript:printbtnClick();" title="인쇄하기"><img src="<%=webServer%>/skin5/mm016/m_printbtn.png"
  width="36" height="35" alt="인쇄"><p>인 쇄</p></a></li>
<% } %>
<% if(searchFile.equals("true")){ %>
<li id="searchbtn" class="indexbtn5mD016"><a href="javascript:MenuObj.mmgroupClick('search');" title="검색메뉴 보기"><img src="<%=webServer%>/skin5/mm016/m_indexbtn.png"
  width="37" height="35" alt="검색"><p>검 색</p></a></li>
<% } else if(mokchaFile.equals("true")){ %>
<li id="indexbtn" class="indexbtn5mD016"><a href="javascript:indexbtnClick();" title="목차 보기"><img src="<%=webServer%>/skin5/mm016/m_indexbtn.png"
  width="37" height="35" alt="목차"><p>목 차</p></a></li>
<% } %>

<li id="helpbtn" class="helpbtn5mD017"><a href="javascript:helpbtnClick();" title="도움말 보기"><img src="<%=webServer%>/skin5/mm016/m_helpbtn.png"
  width="41" height="32" alt="도움말"><p>도움말</p></a></li>
<% if(!mailButtonHide.equals("Y")){ %>
<li id="mailbtn" class="mailbtn5mD017"><a href="javascript:mailbtnClick();" title="전자우편 발송"><img src="<%=webServer%>/skin5/mm016/m_mailbtn.png"
  width="39" height="28" alt="전자우편"><p>메 일</p></a></li>
<% } %>
<li id="snsbtn" class="snsbtn5mD017"><a href="javascript:MenuObj.mmgroupClick('sns');" title="sns메뉴"><img src="<%=webServer%>/skin5/mm016/m_snsbtn.png"
  width="32" height="35" alt="sns메뉴"><p>SNS</p></a></li>
<% if(!combokind.equals("000")){ %>
<li id="combobtn" class="combobtn5mD017"><a href="javascript:combobtnClick();" title="다른 카탈로그 이동 페이지"><img src="<%=webServer%>/skin5/mm016/m_combobtn.png"
  width="30" height="28" alt="다른 카탈로그 이동"><p>이 동</p></a></li>
<% } %>
</ul>
</div>

<!-- popup's sns menu -->
<div id="snssect" class="sns5mD017" style="visibility:hidden;">
<ul class="sns_ul5mD017">
<li id="facebookbtn" class="facebookbtn5mD017"><a href="javascript:facebookbtnClick();" title="페이스북 공유"><img src="<%=webServer%>/skin5/mm012/m_facebookbtn.png"
  width="25" height="22" alt="페이스북"></a></li>
<li id="twitterbtn" class="twitterbtn5mD017"><a href="javascript:twitterbtnClick();" title="트위터 공유"><img src="<%=webServer%>/skin5/mm012/m_twitterbtn.png"
  width="25" height="22" alt="트위터"></a></li>
<li id="kakaobtn" class="kakaobtn5mD017"><a href="javascript:kakaobtnClick();" title="카카오 공유"><img src="<%=webServer%>/skin5/mm012/m_kakaotalkbtn.png"
  width="25" height="22" alt="카카오"></li>
</ul>
<div class="mmpopclosebtn"><a href="javascript:MenuObj.mmgroupClick('sns');"><img src="<%=webServer%>/skin5/win/m_closebtn.png" width="15" height="15"></a></div>
</div>

<!-- popup's search menu -->
<div id="searchsect" class="search5mD017" style="visibility:hidden;">
<form name="searchform" method="get" action="#" onsubmit="return do_searchbtnClick(document.searchform.qtxt.value)" title="페이지 검색">
<input type="search" class="searchtxt5dD017" name="qtxt" title="검색어 입력" onfocus="do_qtxtFocusIn(this)">
<input type="image" class="searchbtn5dD017 hover_opac8" src="<%=webServer%>/skin5/mm017/search_btn.png" alt="검색 시작">
</form>
</div>

<!-- thumb pop -->
<div id="popthumbsect" class="popthumb5mD017">
<svg version="1.1" id="pthumbsvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100" height="100">
<defs>
<!-- 이미지의 모서리를 동그랗게 하기 위함 -->
<clipPath id="pthumbclip" clipPathUnits="userSpaceOnUse" transform="translate(0,0)">
	<rect id="pthumbcliprect" x="10" y="10" width="50" height="50" rx="4" />
</clipPath>
</defs>
<g>
	<image id="pthumbrect1" x="0" y="3" width="8" height="7" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb1.png" />
	<image id="pthumbrect2" x="8" y="3" width="113" height="7" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb2.png" preserveAspectRatio="none" />
	<image id="pthumbrect3" x="121" y="3" width="10" height="7" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb3.png" />
	<image id="pthumbrect4" x="0" y="10" width="8" height="82" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb4.png" preserveAspectRatio="none" />
	<image id="pthumbrect5" x="8" y="10" width="113" height="82" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb5.png" preserveAspectRatio="none" />
	<image id="pthumbrect6" x="121" y="10" width="10" height="82" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb6.png" preserveAspectRatio="none" />
	<image id="pthumbrect7" x="0" y="92" width="8" height="11" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb7.png" />
	<image id="pthumbrect8" x="8" y="92" width="113" height="11" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb8.png" preserveAspectRatio="none" />
	<image id="pthumbrect9" x="121" y="92" width="10" height="11" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb9.png" />
	<image id="pthumbarrow" x="65" y="0" width="9" height="5" xlink:href="<%=webServer%>/skin5/mm016/paging_thumb0.png" />
</g>
<g id="pthumbimgGrp"></g>
</svg>
</div>

<!-- onsmimage -->
<div id="onsmlsect">
<a href="javascript:leftOnsmClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageL017.png" width="30" height="59" alt="이전 페이지"></a>
</div>
<div id="onsmrsect">
<a href="javascript:rightOnsmClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/onsm/onsmimageR017.png" width="30" height="59" alt="다음 페이지"></a>
</div>

<script>
MenuObj = {storedStr:'', totalWidth:0, totalHeight:0, thumbWidth:0, thumbHeight:0, arThumbNode:[],
	currPage:0, curridx:0, pthumbL:undefined, pthumbImgGrp:undefined, pagingGrpObj:undefined, arPagingObj:[]};

backDiv = document.getElementById("backsect");
thumbDiv = document.getElementById("thumbsect");
popthumbDiv = document.getElementById("popthumbsect");
if(document.getElementById("searchsect")) searchDiv = document.getElementById("searchsect");
if(document.getElementById("snssect")) snsDiv = document.getElementById("snssect");

MenuObj.onload_func2 = function(){
	if(LinkInfo.facebookStr === "" && document.getElementById("facebookbtn")) document.getElementById("facebookbtn").style.opacity = "0.2";
	if(LinkInfo.twitterStr === "" && document.getElementById("twitterbtn")) document.getElementById("twitterbtn").style.opacity = "0.2";
	if(LinkInfo.kakaoStr === "" && document.getElementById("kakaobtn")) document.getElementById("kakaobtn").style.opacity = "0.2";

	var h = 60;
	MenuObj.thumbHeight = h;
	MenuObj.thumbWidth = Math.round(h*ScreenInfo.ratioWH);
	var w = MenuObj.thumbWidth;
	MenuObj.totalWidth = w + 18;
	MenuObj.totalHeight = h + 16;

	MenuObj.pthumbImgGrp = document.getElementById("pthumbimgGrp");

	document.getElementById("pthumbarrow").setAttribute("x", 4+w/2);		//8-4=4(left margin - arrow width)
	document.getElementById("pthumbrect2").setAttribute("width", w+0.5);
	document.getElementById("pthumbrect3").setAttribute("x", 8+w);

	document.getElementById("pthumbrect4").setAttribute("height", h+1);
	document.getElementById("pthumbrect5").setAttribute("width", w+3);			// 사이가 벌어짐(원래는 w)
	document.getElementById("pthumbrect5").setAttribute("height", h+1);
	document.getElementById("pthumbrect6").setAttribute("x", 8+w);
	document.getElementById("pthumbrect6").setAttribute("height", h+1);

	document.getElementById("pthumbrect7").setAttribute("y", 11+h);
	document.getElementById("pthumbrect8").setAttribute("width", w+0.5);
	document.getElementById("pthumbrect8").setAttribute("y", 11+h);
	document.getElementById("pthumbrect9").setAttribute("x", 8+w);
	document.getElementById("pthumbrect9").setAttribute("y", 11+h);

	document.getElementById("pthumbcliprect").setAttribute("width", w);
	document.getElementById("pthumbcliprect").setAttribute("height", h);

	document.getElementById("pthumbsvg").style.width = MenuObj.totalWidth+"px";
	document.getElementById("pthumbsvg").style.height = MenuObj.totalHeight+"px";
	popthumbDiv.style.width = MenuObj.totalWidth+"px";
	popthumbDiv.style.height = MenuObj.totalHeight+"px";

	MenuObj.pagingGrpObj = document.getElementById('thumb_pagingGrp');
	MenuObj.make_paging();
}
MenuObj.make_paging = function(){
	var k = 0;
	var tx = 2;
	for(var i = 0;i < PageInfo.cataPages;i++){
		if(k%2 == 0){
			MenuObj.make_pagingitem(tx, PageInfo.get_showPage(i+1), i+1, true);
			tx += 24;
		}
		else{
			MenuObj.make_pagingitem(tx, PageInfo.get_showPage(i+1), i+1, false);
			tx += 28;
		}
		k++;
	}
	document.getElementById("thumbsvg").style.width = tx+"px";
}
MenuObj.make_pagingitem = function(tx, stitle, pno, leftRight){
	if(pno === 0) return;

	var gObj = document.createElementNS("http://www.w3.org/2000/svg","g");
	gObj.setAttribute('transform','translate('+tx+','+0+')');
	gObj.setAttribute("cursor", "pointer");

	var imgsrc = "<%=webServer%>/skin5/mm017/paging_both.png";
	var backObj = document.createElementNS("http://www.w3.org/2000/svg","image");
	backObj.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", imgsrc);
	backObj.setAttribute("width", 23);
	backObj.setAttribute("height", 24);
	gObj.appendChild(backObj);

	var textObj = document.createElementNS("http://www.w3.org/2000/svg","text");
	textObj.setAttribute("class", "thumb_pagenum5dD017");
	textObj.setAttribute("x", 12);
	textObj.setAttribute("y", 16);
	textObj.setAttribute("text-anchor", "middle");
	textNode = document.createTextNode(stitle);
	textObj.appendChild(textNode);
	gObj.appendChild(textObj);

	//textObj.setAttribute("textLength", 5);
	//textObj.setAttribute("lengthAdjust", "spacingAndGlyphs");

	var idx = MenuObj.arPagingObj.push(gObj) - 1;
	if(pno === 0) gObj.setAttribute("visibility", "hidden");
	else gObj.setAttribute("onclick", "MenuObj.do_itemClick(evt,"+idx+","+pno+","+leftRight+")");

	MenuObj.pagingGrpObj.appendChild(gObj);
}
MenuObj.make_thumb = function(tx, url, pno){
	var popThumb = document.createElementNS("http://www.w3.org/2000/svg","image");
	popThumb.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", url);
	popThumb.setAttribute("stroke", "#ff6600");
	popThumb.setAttribute("stroke-width", "1");
	popThumb.setAttribute("x", tx);
	popThumb.setAttribute("y", 10);
	popThumb.setAttribute("width", MenuObj.thumbWidth+0.5);
	popThumb.setAttribute("height", MenuObj.thumbHeight);
	popThumb.setAttribute("clip-path", "url(#pthumbclip)");
	popThumb.setAttribute("onclick", "MenuObj.do_thumbClick("+pno+")");
	//popThumb.setAttribute("visibility", "hidden");
	return popThumb;
}
MenuObj.do_itemClick = function(e, idx, pno, lr){
	var imgReplace = 0;
	if(popthumbDiv.style.visibility == "visible"){
		if(MenuObj.currPage == pno){
			imgReplace = 1;
			MenuObj.do_mouseOut();
			return;
		}
		else{
			imgReplace = 2;
			MenuObj.arPagingObj[MenuObj.curridx].childNodes[0].setAttribute("xlink:href","<%=webServer%>/skin5/mm017/paging_both.png");
		}
	}

	var areaRect = MenuObj.arPagingObj[idx].getBoundingClientRect();
	MenuObj.arPagingObj[idx].childNodes[0].setAttribute("xlink:href","<%=webServer%>/skin5/mm017/paging_both_over.png");

	var tx = areaRect.left + (areaRect.right-areaRect.left)/2 - MenuObj.totalWidth/2;
	if(tx+MenuObj.totalWidth > cataRect.right) tx = cataRect.right - MenuObj.totalWidth - 1;
	else if(tx < cataRect.x) tx = cataRect.x + 1;

	popthumbDiv.style.left = tx+"px";
	popthumbDiv.style.visibility = "visible";

	MenuObj.currPage = pno;
	MenuObj.curridx = idx;
	var aaa = FileInfo.thumbFilePath(pno,"s","","*");
	if(imgReplace === 2){
		MenuObj.pthumbL.setAttribute("xlink:href", aaa[0]);
		MenuObj.pthumbL.setAttribute("onclick", "MenuObj.do_thumbClick("+pno+")");
	}
	else{
		MenuObj.pthumbL = MenuObj.make_thumb(8, aaa[0], pno);
		MenuObj.pthumbImgGrp.appendChild(MenuObj.pthumbL);
	}
}
MenuObj.do_mouseOut = function(){
	MenuObj.arPagingObj[MenuObj.curridx].childNodes[0].setAttribute("xlink:href","<%=webServer%>/skin5/mm017/paging_both.png");

	if(MenuObj.pthumbL) MenuObj.pthumbImgGrp.removeChild(MenuObj.pthumbL);
	MenuObj.pthumbL = undefined;

	popthumbDiv.style.visibility = "hidden";
}

/// ############################## button ############################## ///
MenuObj.do_thumbClick = function(pno){
	var n = PermitMan.get_directGoPerm("thumbpaging", pno, "", "");			// device, spage, pageshow, state
	if(n < 0) return;
	go_general(n, "system");
}
MenuObj.remove_children = function(){
	var nodes = MenuObj.pagingGrpObj.childNodes;
	for(var i = nodes.length-1;i > 0;i--){
		MenuObj.pagingGrpObj.removeChild(nodes[i]);
	}
	MenuObj.arPagingObj.length = 0;
}
MenuObj.mmgroupClick = function(s){
	if(s === "sns"){
		if(searchDiv.style.visibility == "visible") searchDiv.style.visibility = "hidden";
		if(snsDiv.style.visibility == "visible") snsDiv.style.visibility = "hidden";
		else{
			var areaRect = document.getElementById("snsbtn").getBoundingClientRect();
			snsDiv.style.top = (areaRect.top - 40) + "px";
			snsDiv.style.left = (areaRect.left-snsDiv.clientWidth/2) + "px";
			snsDiv.style.visibility = "visible";
		}
	}
	else if(s === "search"){
		if(snsDiv.style.visibility == "visible") snsDiv.style.visibility = "hidden";
		if(searchDiv.style.visibility == "visible") searchDiv.style.visibility = "hidden";
		else{
			var areaRect = document.getElementById("searchbtn").getBoundingClientRect();
			searchDiv.style.top = (areaRect.top - 35) + "px";
			searchDiv.style.left = "15px";
			searchDiv.style.visibility = "visible";
		}
	}
}
MenuObj.show_pageNumber = function(){
}
MenuObj.onresize_func = function(){
}

onsmlDiv = document.getElementById("onsmlsect");
onsmrDiv = document.getElementById("onsmrsect");
OnsmObj = {};
OnsmObj.set_location = function(){
	onsmlDiv.style.left = winRect.x + "px";
	onsmlDiv.style.top = (smRect.centerPt.y-28) + "px";
	onsmrDiv.style.left = (winRect.right-30) + "px";
	onsmrDiv.style.top = (smRect.centerPt.y-28) + "px";
}
OnsmObj.set_visible = function(b1, b2){
	onsmlDiv.style.visibility = b1;
	onsmrDiv.style.visibility = b1;
}
</script>

<% } %>
