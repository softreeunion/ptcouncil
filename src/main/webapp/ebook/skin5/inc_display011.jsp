<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%
	request.setCharacterEncoding("utf-8");

	String incView = request.getParameter("incview");
	String indexType = request.getParameter("indexType");
	String combokind = request.getParameter("combokind");
	String downFile = request.getParameter("downFile");
	String downFileName = request.getParameter("downFileName");
	String downexten = request.getParameter("downexten");
	String downtext = URLDecoder.decode(request.getParameter("downtext"),"utf-8");
	String mediaFile = request.getParameter("mediaFile");
	String mokchaFile = request.getParameter("mokchaFile");
	String webServer = request.getParameter("webServer");
	String printKindmc = request.getParameter("printKindmc");
	String dirName = URLDecoder.decode(request.getParameter("dirName"),"utf-8");
	String searchFile = request.getParameter("searchFile");
	String skinMedia = request.getParameter("skinMedia");
	String skinSpacer = request.getParameter("skinSpacer");
	String Dir = request.getParameter("Dir");
	String catimage = request.getParameter("catimage");
	String webpath = webServer + "/catImage" + catimage + "/" + Dir;
%>
<% if(incView.equals("d")){ %>
<!-- navigation -->
<nav id="mmsect" class="mm5dD011-idx<%=indexType%>">
<ul class="mm_ulE001">
<li id="firstbtn" class="firstbtn5dD011"><a href="javascript:firstbtnClick();" title="처음 페이지로 이동"><img src="<%=webServer%>/skin5/mm011/firstbtn.png" width="61" height="32" alt="처음 페이지"
  onmouseover="this.src='<%=webServer%>/skin5/mm011/firstbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm011/firstbtn.png'"></a></li>
<li id="prevbtn" class="prevbtn5dD011"><a href="javascript:prevbtnClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/mm011/prevbtn.png" width="61" height="32" alt="이전 페이지"
  onmouseover="this.src='<%=webServer%>/skin5/mm011/prevbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm011/prevbtn.png'"></a></li>
<li id="nextbtn" class="nextbtn5dD011"><a href="javascript:nextbtnClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/mm011/nextbtn.png" width="61" height="32" alt="다음 페이지"
  onmouseover="this.src='<%=webServer%>/skin5/mm011/nextbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm011/nextbtn.png'"></a></li>
<li id="lastbtn" class="lastbtn5dD011"><a href="javascript:lastbtnClick();" title="마지막 페이지로 이동"><img src="<%=webServer%>/skin5/mm011/lastbtn.png" width="61" height="32" alt="마지막 페이지"
  onmouseover="this.src='<%=webServer%>/skin5/mm011/lastbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm011/lastbtn.png'"></a></li>
<li id="closebtn" class="closebtn5dD011"><a href="javascript:closebtnClick();" title="창닫기"><img src="<%=webServer%>/skin5/mm011/closebtn.png" width="23" height="56" alt="창닫기"
  onmouseover="this.src='<%=webServer%>/skin5/mm011/closebtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm011/closebtn.png'"></a></li>
</ul>

<% if(indexType.equals("H")){ %>
<div id="horidxa" class="horidx5dD011">
<div class="horidxctn">
	<a href="javascript:go_indexUpper();" title="목차 한줄 위로"><img src="<%=webServer%>/skin5/mm011/horidx_upper.png" width="7" height="4" alt="목차 한줄 위로"></a>
	<a href="javascript:go_indexLower();" title="목차 한줄 아래로"><img src="<%=webServer%>/skin5/mm011/horidx_lower.png" width="7" height="4" alt="목차 한줄 아래로"></a>
	<p id="horidx_txt"></p>
</div>
</div>
<% } %>

<div id="pageshow" class="pshow5dD011">
<form name="pageshowform" method="get" action="ecatalog.jsp" onsubmit="return do_pagenoFormSubmit();" title="페이지 이동">
<p class="pshow_p5dD011"><input type="text" name="pageno" value="" onfocus="MenuObj.do_pagenoFocusIn(event);" onblur="MenuObj.do_pagenoFocusOut(event)"
  pattern="[0-9-]+" title="숫자만 입력 가능합니다." required></p>
<p id="pshow_totalpage"></p>
</form>
</div>

<ul class="mm_ulE002">
<% if(indexType.equals("R")){ %>
<li id="indexbtn" class="indexbtn5dD011"><a href="javascript:indexbtnClick();" title="목차 보기"><img src="<%=webServer%>/skin5/mm011/indexbtn.png" width="31" height="16" alt="목차"
  onmouseover="this.src='<%=webServer%>/skin5/mm011/indexbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm011/indexbtn.png'"></a></li>
<% } %>
<li id="expbtn" class="expbtn5dD011"><a href="javascript:expbtnClick();" title="탐색기 보기"><img src="<%=webServer%>/skin5/mm011/expbtn.png" width="31" height="16" alt="탐색"
  onmouseover="this.src='<%=webServer%>/skin5/mm011/expbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm011/expbtn.png'"></a></li>
<li id="glassesbtn" class="glassesbtn5dD011"><a href="javascript:glassesbtnClick();" title="돋보기로 보기"><img src="<%=webServer%>/skin5/mm011/glassesbtn.png" width="40" height="16" alt="돋보기"
  onmouseover="this.src='<%=webServer%>/skin5/mm011/glassesbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm011/glassesbtn.png'"></a></li>
<li id="slidebtn" class="slidebtn5dD011"><a href="javascript:slidebtnClick();" title="자동 넘김"><img src="<%=webServer%>/skin5/mm011/slidebtn.png" width="64" height="16" alt="자동 넘김"
  onmouseover="this.src='<%=webServer%>/skin5/mm011/slidebtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm011/slidebtn.png'"></a></li>
</ul>
</nav>

<div id="mmleft" class="mmleft011">
	<div><img src="<%=webServer%>/skin5/mm011/menu_lefttop.png" width="45" height="3" alt=""></div>
	<div><img src="<%=webServer%>/skin5/mm011/menu_leftbottom.png" width="45" height="595" alt=""></div>
</div>

<div id="mmright" class="mmright011">
<div><img src="<%=webServer%>/skin5/mm011/menu_righttop.png" width="129" height="3" alt=""></div>
<div><img src="<%=webServer%>/skin5/mm011/menu_rightbottom.png" width="129" height="595" alt=""></div>
<div><img src="<%=webServer%>/skin5/mm011/menu_rightboard.png" width="107" height="166" alt=""></div>

<ul class="mm_ulE003">
<li id="mailbtn"><a href="javascript:mailbtnClick();" title="전자우편 발송"><img src="<%=webServer%>/skin5/mm011/mailbtn.png" width="39" height="14" alt="전자우편 발송"
  onmouseover="this.src='<%=webServer%>/skin5/mm011/mailbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm011/mailbtn.png'"></a></li>
<li id="printbtn"><a href="javascript:printbtnClick();" title="인쇄하기"><img src="<%=webServer%>/skin5/mm011/printbtn.png" width="39" height="14" alt="인쇄하기"
  onmouseover="this.src='<%=webServer%>/skin5/mm011/printbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm011/printbtn.png'"></a></li>
<li id="helpbtn"><a href="javascript:helpbtnClick();" title="도움말 보기"><img src="<%=webServer%>/skin5/mm011/helpbtn.png" width="39" height="14" alt="도움말 보기"
  onmouseover="this.src='<%=webServer%>/skin5/mm011/helpbtn_over.png'" onmouseout="this.src='<%=webServer%>/skin5/mm011/helpbtn.png'"></a></li>
</ul>
</div>

<div id="lowersect" class="lower5dD011">
<!-- combobox -->
<% if(!combokind.equals("000")){ %>
<div id="combosect" class="cbbox5dD011" title="다른 카탈로그로 이동" data-width="display" data-align="left" data-direction="top">
<a href="javascript:comboClick();">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="181" height="18">
<g class="cbbox_g-text">
	<rect x="0.5" y="0.5" width="179" height="17" fill="#D4EBC3" stroke="#D4EBC3" />
	<text id="cbbox_text" x="3" y="14"><%=dirName%></text>
</g>
<g class="cbbox_g-btn" transform="translate(161,2)">
	<rect x="0.5" y="0.5" width="15" height="13" fill="#41830F" stroke="#41830F" />
	<polygon points="4,4 12,4 8,10" fill="#ffffff" />
</g>
</svg></a>
</div>
<% } %>

<!-- download -->
<% if(downFile.equals("true")){ %>
<div id="downsect" class="down5dD011">
<a href="<%=webpath%>/<%=downFileName%>" download="<%=webpath%>/<%=downFileName%>"
  title="파일 다운로드"><img src="<%=webServer%>/skin5/icon/file_<%=downexten%>.gif" class="down_img" alt="파일 다운로드"><%=downtext%></a>
</div>
<% } %>

<!-- search menu -->
<% if(searchFile.equals("true")){ %>
<div id="searchsect" class="search5dD011">
<form name="searchform" method="get" action="#" onsubmit="return do_searchbtnClick(document.searchform.qtxt.value)">
<input type="search" class="searchtxt5dD011" name="qtxt" title="검색어 입력" onfocus="do_qtxtFocusIn(this)">
<input type="image" class="searchbtn5dD011 hover_opac8" src="<%=webServer%>/skin5/mm011/search_btn.png" alt="검색 시작">
</form>
</div>
<% } %>

<!-- media -->
<% if(mediaFile.equals("true") && !skinMedia.equals("000")){ %>
<nav id="mediasect" class="media5dD011" data-width="display" data-align="right" data-direction="top" data-hpos="1">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="90" height="17">

<!-- background -->
<rect x="0" y="0" rx="1" width="90" height="17" fill="#1f390c" />

<!-- animation -->
<g transform="translate(8,4)">
	<rect id="mediarect1" x="0" y="0" width="2" height="1" fill="#FFF501" />
	<rect id="mediarect2" x="4" y="0" width="2" height="1" fill="#FFF501" />
	<rect id="mediarect3" x="8" y="0" width="2" height="1" fill="#FFF501" />
	<rect id="mediarect4" x="0" y="2" width="2" height="1" fill="#FFF501" />
	<rect id="mediarect5" x="4" y="2" width="2" height="1" fill="#FFF501" />
	<rect id="mediarect6" x="8" y="2" width="2" height="1" fill="#FFF501" />
	<rect id="mediarect7" x="0" y="4" width="2" height="1" fill="#FFF501" />
	<rect id="mediarect8" x="4" y="4" width="2" height="1" fill="#FFF501" />
	<rect id="mediarect9" x="8" y="4" width="2" height="1" fill="#FFF501" />
	<rect id="mediarect10" x="0" y="6" width="2" height="1" fill="#FFA602" />
	<rect id="mediarect11" x="4" y="6" width="2" height="1" fill="#FFA602" />
	<rect id="mediarect12" x="8" y="6" width="2" height="1" fill="#FFA602" />
	<rect id="mediarect13" x="0" y="8" width="2" height="1" fill="#FF8801" />
	<rect id="mediarect14" x="4" y="8" width="2" height="1" fill="#FF8801" />
	<rect id="mediarect15" x="8" y="8" width="2" height="1" fill="#FF8801" />
</g>
</svg>

<ul class="media_ul5dD011">
<li id="media_backbtn" class="media_backbtn5dD011"><a href="javascript:MediaObj.backbtnClick();" title="이전곡"><img width="7" height="7" 
  src="<%=webServer%>/skin5/mm010/media_prevbtn.png" alt="이전곡"></a></li>
<li id="media_playbtn" class="media_playbtn5dD011"><a href="javascript:MediaObj.playbtnClick();" title="재생"><img width="4" height="7"
  src="<%=webServer%>/skin5/mm010/media_nextbtn.png" alt="재생"></a></li>
<li id="media_nextbtn" class="media_nextbtn5dD011"><a href="javascript:MediaObj.nextbtnClick();" title="다음곡"><img width="7" height="7"
  src="<%=webServer%>/skin5/mm010/media_lastbtn.png" alt="다음곡"></a></li>
<li id="media_stopbtn" class="media_stopbtn5dD011"><a href="javascript:MediaObj.stopbtnClick();" title="멈춤"><img width="7" height="7"
  src="<%=webServer%>/skin5/mm010/media_pausebtn.png" alt="멈춤"></a></li>
<li id="media_listbtn" class="media_listbtn5dD011"><a href="javascript:MediaObj.listbtnClick();" title="재생목록"><img width="7" height="7"
  src="<%=webServer%>/skin5/mm010/media_listbtn.png" alt="곡 목록"></a></li>
</ul>
<audio id="mediactrl" class="audio_ctrlr_hidden">브라우저가 audio를 지원하지 않습니다.</audio>
</nav>
<% } %>

<!-- sns -->
<nav id="snssect" class="sns_nav5dD011">
<ul class="sns_ul5dD011">
<li id="facebookbtn" class="sns_liE010 hover_opac8"><a href="javascript:facebookbtnClick();" 
  title="페이스북 공유"><img src="<%=webServer%>/skin5/mm010/facebookbtn.png" class="sns_liE010" alt="페이스북 공유"></a></li>
<li id="twitterbtn" class="sns_liE010 hover_opac8"><a href="javascript:twitterbtnClick();" 
  title="트위터 공유"><img src="<%=webServer%>/skin5/mm010/twitterbtn.png" class="sns_liE010" alt="트위터 공유"></a></li>
</ul>
</nav>

</div>

<script>
MenuObj = {storedStr:''};
MenuObj.onload_func2 = function(){
	var n = (PageInfo.showStartPage == 0) ? PageInfo.cataPages : PageInfo.get_showPageFromReal(PageInfo.cataPages);
	document.getElementById("pshow_totalpage").innerHTML = n;

	if(LinkInfo.facebookStr === "" && document.getElementById("facebookbtn")) document.getElementById("facebookbtn").style.visibility = "hidden";
	if(LinkInfo.twitterStr === "" && document.getElementById("twitterbtn")) document.getElementById("twitterbtn").style.visibility = "hidden";
	if(MenuInfo.mailButtonHide === "Y" && document.getElementById("mailbtn")) document.getElementById("mailbtn").style.opacity = "0.2";
	if((MenuInfo.indexType === "H" || FileInfo.idxFile === false) && document.getElementById("indexbtn")) document.getElementById("indexbtn").style.visibility = "hidden";
	if(SkinInfo.printkind === "000" && document.getElementById("printbtn")) document.getElementById("printbtn").style.opacity = "0.3";
}
MenuObj.show_pageNumber = function(){
	document.pageshowform.pageno.value = MoveInfo.get_showPageNumber();
}
MenuObj.do_pagenoFocusIn = function(e){
	MenuObj.storedStr = document.pageshowform.pageno.value;
	document.pageshowform.pageno.value = "";
}
MenuObj.do_pagenoFocusOut = function(e){
	document.pageshowform.pageno.value = MenuObj.storedStr;
}
MenuObj.do_pagenoFormSubmit = function(){
	var n = PermitMan.get_directGoPerm("showboard", document.pageshowform.pageno.value, "", "");			// device, spage, pageshow, state
	if(n < 0) return false;
	go_general(n, "user");
	return false;
}
MenuObj.onresize_func = function(){
}

if(document.getElementById("combosect")) comboDiv = document.getElementById("combosect");
if(document.getElementById("downsect")) downDiv = document.getElementById("downsect");
if(document.getElementById("searchsect")) searchDiv = document.getElementById("searchsect");
if(document.getElementById("mediasect")){
	mediaDiv = document.getElementById("mediasect");
	mediaPlayer = document.getElementById("mediactrl");
}
if(document.getElementById("snssect")) snsDiv = document.getElementById("snssect");
</script>

<% } else if(incView.equals("m")){ %>
<!-- mm navigation -->
<nav id="mmsect" class="mm5mD011">
<ul class="mm_ulE004">
<li id="firstbtn" class="firstbtn5mD011"><a href="javascript:firstbtnClick();" title="처음 페이지로 이동"><img src="<%=webServer%>/skin5/mm011/firstbtn.png" 
  width="45" height="24" alt="처음 페이지"></a></li>
<li id="prevbtn" class="prevbtn5mD011"><a href="javascript:prevbtnClick();" title="이전 페이지로 이동"><img src="<%=webServer%>/skin5/mm011/prevbtn.png"
  width="45" height="24" alt="이전 페이지"></a></li>
<li id="nextbtn" class="nextbtn5mD011"><a href="javascript:nextbtnClick();" title="다음 페이지로 이동"><img src="<%=webServer%>/skin5/mm011/nextbtn.png"
  width="45" height="24" alt="다음 페이지"></a></li>
<li id="lastbtn" class="lastbtn5mD011"><a href="javascript:lastbtnClick();" title="마지막 페이지로 이동"><img src="<%=webServer%>/skin5/mm011/lastbtn.png"
  width="45" height="24" alt="마지막 페이지"></a></li>
<li id="mmpopbtn" class="mmpopbtn5mD011"><a href="javascript:MenuObj.mmpopClick();" title="이동메뉴"><img src="<%=webServer%>/skin5/mm011/m_movebtn.png"
  width="5" height="17" alt="이동메뉴"></a></li>
</ul>
<div id="pageshow" class="pshow5mD011" contentEditable="true" onfocus="MenuObj.do_pagenoFocusIn(event);" onblur="MenuObj.do_pagenoFocusOut(event);"></div>
</nav>

<!-- mmpop menu -->
<div id="mmpopsect" class="mmpop5mD011" style="visibility:hidden;">
<ul class="mmpop_ul5mD011">
<li id="slidebtn"><a href="javascript:slidebtnClick();MenuObj.mmpopClick();" title="자동넘김"><img src="<%=webServer%>/skin5/mm010/m_slidebtn.png"
  class="slidebtn_img5mD011" alt="자동넘김"></a></li>
<% if(indexType.equals("R")){ %>
<li id="indexbtn"><a href="javascript:indexbtnClick();MenuObj.mmpopClick();" title="목차 보기"><img src="<%=webServer%>/skin5/mm001/m_indexbtn.png"
   class="indexbtn_img5mD011" alt="목차"></a></li>
<% } %>
<li id="expbtn"><a href="javascript:expbtnClick();MenuObj.mmpopClick();" title="탐색기 보기"><img src="<%=webServer%>/skin5/mm001/m_expbtn.png"
   class="expbtn_img5mD011" alt="탐색"></a></li>
<% if(!combokind.equals("000")){ %>
<li id="combobtn"><a href="javascript:combobtnClick();MenuObj.mmpopClick();" title="다른 카탈로그 이동 페이지"><img src="<%=webServer%>/skin5/mm001/m_combobtn.png"
   class="combobtn_img5mD011" alt="다른 카탈로그"></a></li>
<% } %>
</ul>
</div>

<% if(indexType.equals("H") && mokchaFile.equals("true")){ %>
<!-- horizontal index -->
<div id="horidx" class="horidx5mD011">
<div id="horidx_div" class="horidx_div5mD011"></div>
<a href="javascript:MenuObj.horidxmoreClick();"><div id="idxmorebtn" class="horidx_more5mD011"><img src="<%=webServer%>/skin5/mm011/m_horidxmore.png" id="idxmoreimg" width="12" height="6" alt="목차 더보기"></div></a>
</div>
<% } %>

<!-- lower menu -->
<div id="lowersect" class="lower5mD011">
<a href="javascript:MenuObj.groupBackClick();" title="이전메뉴로 돌아가기"><div id="mmgbackbtn"><img src="<%=webServer%>/skin5/mm011/m_assembackbtn.png"
  width="8" height="15" alt="이전메뉴로 돌아가기"></div></a>

<ul id="mmgsect" class="mmg5mD011">
<li id="ggenbtn" class="ggenbtn5mD011"><a href="javascript:MenuObj.groupClick('mmggensect');" title="메뉴 더보기"><img src="<%=webServer%>/skin5/mm011/m_morebtn.png"
  width="32" height="24" alt="메뉴 더보기"></a></li>
<li id="gsnsbtn" class="gsnsbtn5mD011"><a href="javascript:MenuObj.groupClick('snssect');" title="sns메뉴"><img src="<%=webServer%>/skin5/mm011/m_snsbtn.png"
  width="32" height="24" alt="sns메뉴"></a></li>
<% if(mediaFile.equals("true") && !skinMedia.equals("000")){ %>
<li id="gmediabtn" class="gmediabtn5mD011"><a href="javascript:MenuObj.groupClick('mediasect');" title="음악메뉴"><img src="<%=webServer%>/skin5/mm011/m_mediabtn.png"
  width="32" height="24" alt="음악메뉴"></a></li>
<% } %>
</ul>

<% if(searchFile.equals("true")){ %>
<div id="searchsect" class="search5mD011">
<form name="searchform" method="get" action="#" onsubmit="return do_searchbtnClick(document.searchform.qtxt.value)" title="페이지 검색">
<input type="search" class="searchtxt5mD011" name="qtxt" title="검색어 입력">
<input type="image" class="searchbtn5mD011" src="<%=webServer%>/skin5/mm011/m_searchbtn.png" title="검색">
</form>
</div>
<% } %>

<!-- lower more general group menu -->
<ul id="mmggensect" class="mmggen5mD011">
<% if(!skinSpacer.equals("000")){ %>
<li id="spacerbtn" class="spacerbtn5mD011"><a href="javascript:spacerbtnClick('left');" title="즐겨찾기">책갈피</a></li>
<% } %>
<% if(!printKindmc.equals("000")){ %>
<li id="printbtn" class="printbtn5mD011"><a href="javascript:printbtnClick();" title="인쇄하기">인 쇄</a></li>
<% } %>
<li id="helpbtn" class="helpbtn5mD011"><a href="javascript:helpbtnClick();" title="도움말 보기">도움말</a></li>
<% if(downFile.equals("true")){ %>
<li id="downbtn" class="downbtn5mD011"><a href="<%=webpath%>/skinMedia%>" download="<%=webpath%>/<%=skinMedia%>" 
  title="파일 다운로드">다운로드</a></li>
<% } %>
</ul>

<!-- lower more sns group menu -->
<nav id="snssect" class="sns_nav5mD011">
<ul class="sns_ul5mD011">
<li id="facebookbtn" class="sns_liE041"><a href="javascript:facebookbtnClick();" title="페이스북 공유"><img src="<%=webServer%>/skin5/mm011/m_facebookbtn.png"
  class="sns_liE041" alt="페이스북"></a></li>
<li id="twitterbtn" class="sns_liE041"><a href="javascript:twitterbtnClick();" title="트위터 공유"><img src="<%=webServer%>/skin5/mm011/m_twitterbtn.png"
  class="sns_liE041" alt="트위터"></a></li>
<li id="kakaobtn" class="sns_liE041"><a href="javascript:kakaobtnClick();" title="카카오 공유"><img src="<%=webServer%>/skin5/mm011/m_kakaotalkbtn.png"
  class="sns_liE041" alt="카카오"></li>
<li id="mmmailbtn" class="sns_liE041"><a href="javascript:mailbtnClick();" title="전자우편 발송"><img src="<%=webServer%>/skin5/mm011/m_mailbtn.png"
  class="sns_liE041" alt="전자우편 발송"></a></li>
</ul>
</nav>

<!-- lower more media group menu -->
<div id="mediasect" class="media5mD011">
<% if(mediaFile.equals("true") && !skinMedia.equals("000")){ %>
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="14">
<g>
	<rect id="mdanirect1" x="0" y="0" width="4" height="2" fill="#74CF00" />
	<rect id="mdanirect2" x="6" y="0" width="4" height="2" fill="#74CF00" />
	<rect id="mdanirect3" x="12" y="0" width="4" height="2" fill="#74CF00" />
	<rect id="mdanirect4" x="0" y="3" width="4" height="2" fill="#74CF00" />
	<rect id="mdanirect5" x="6" y="3" width="4" height="2" fill="#74CF00" />
	<rect id="mdanirect6" x="12" y="3" width="4" height="2" fill="#74CF00" />
	<rect id="mdanirect7" x="0" y="6" width="4" height="2" fill="#74CF00" />
	<rect id="mdanirect8" x="6" y="6" width="4" height="2" fill="#74CF00" />
	<rect id="mdanirect9" x="12" y="6" width="4" height="2" fill="#74CF00" />
	<rect id="mdanirect10" x="0" y="9" width="4" height="2" fill="#74B800" />
	<rect id="mdanirect11" x="6" y="9" width="4" height="2" fill="#74B800" />
	<rect id="mdanirect12" x="12" y="9" width="4" height="2" fill="#74B800" />
	<rect id="mdanirect13" x="0" y="12" width="4" height="2" fill="#748A00" />
	<rect id="mdanirect14" x="6" y="12" width="4" height="2" fill="#748A00" />
	<rect id="mdanirect15" x="12" y="12" width="4" height="2" fill="#748A00" />
</g>
</svg>

<ul class="media_ul5mD011">
	<li id="media_backbtn" class="media_backbtn5mD011"><a href="javascript:MediaObj.backbtnClick();" title="이전 곡">
	<svg version="1.1" class="media_svgE003" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13" height="13">
	<polygon points="5,7 13,13 13,0" />
	<line x1="0.5" y1="0" x2="0.5" y2="13" stroke-width="5" />
	</svg></a>
	</li>
	<li id="media_playbtn" class="media_playbtn5mD011"><a href="javascript:MediaObj.playbtnClick();" title="재생">
	<svg version="1.1" class="media_svgE003" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10" height="13">
	<polygon points="0,0 10,7 0,13" />
	</svg></a>
	</li>
	<li id="media_nextbtn" class="media_nextbtn5mD011"><a href="javascript:MediaObj.nextbtnClick();" title="다음 곡">
	<svg version="1.1" class="media_svgE003" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13" height="13">
	<polygon points="0,0 8,7 0,13" />
	<line x1="12.5" y1="0" x2="12.5" y2="13" stroke-width="5" />
	</svg></a>
	</li>
	<li id="media_stopbtn" class="media_stopbtn5mD011"><a href="javascript:MediaObj.stopbtnClick();" title="멈춤">
	<svg version="1.1" class="media_svgE003" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="9" height="9">
	<polygon points="0,0 9,0 9,9 0,9" />
	</svg></a>
	</li>
	<li id="media_listbtn" class="media_listbtn5mD011"><a href="javascript:MediaObj.listbtnClick();" title="재생목록">
	<svg version="1.1" class="media_svgE003" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13" height="13">
	<polygon points="7,0 13,8 0,8" />
	<line x1="0" y1="11.5" x2="13" y2="11.5" stroke-width="3" />
	</svg></a>
	</li>
</ul>
<audio id="autoctrl">브라우저가 audio를 지원하지 않습니다.</audio>
<% } %>
</div>
</div>

<script>
MenuObj = {storedStr:'', armenu:["mmggensect","snssect","mediasect"]};
MenuObj.onload_func2 = function(){
	if(LinkInfo.facebookStr === "" && document.getElementById("facebookbtn")) document.getElementById("facebookbtn").style.opacity = "0.2";
	if(LinkInfo.twitterStr === "" && document.getElementById("twitterbtn")) document.getElementById("twitterbtn").style.opacity = "0.2";
	if(LinkInfo.kakaoStr === "" && document.getElementById("kakaobtn")) document.getElementById("kakaobtn").style.opacity = "0.2";
	if(MenuInfo.mailButtonHide === "Y" && document.getElementById("mailbtn")) document.getElementById("mailbtn").style.visibility = "hidden";
	if(FileInfo.idxFile === false && document.getElementById("indexbtn")) document.getElementById("indexbtn").style.opacity = "0.2";
	if(MenuInfo.idxmorebtn === false){
		document.getElementById("horidx_div").style.width = "100%";
		document.getElementById("idxmorebtn").style.display = "none";
	}
}
MenuObj.show_pageNumber = function(){
	var n = (PageInfo.showStartPage == 0) ? PageInfo.cataPages : PageInfo.get_showPageFromReal(PageInfo.cataPages);
	document.getElementById("pageshow").innerHTML = MoveInfo.get_showPageNumber() + "/" + n;
	show_currIndex();
}
MenuObj.do_pagenoFocusIn = function(e){
	MenuObj.storedStr = document.getElementById("pageshow").innerHTML;
	document.getElementById("pageshow").innerHTML = PageInfo.currentPage;
	window.getSelection().selectAllChildren(document.getElementById("pageshow"));
}
MenuObj.do_pagenoFocusOut = function(e){
	do_pagenoFormSubmit();
	document.getElementById("pageshow").innerHTML = MenuObj.storedStr;
}
MenuObj.do_pagenoFormSubmit = function(){
	var spage = document.getElementById("pageshow").innerHTML;
	var n = PermitMan.get_directGoPerm("showboard", spage, "", "");			// device, spage, pageshow, state
	if(n < 0) return false;
	go_general(n, "user");
	return false;
}
MenuObj.horidxmoreClick = function(){
	if(indexDiv.style.visibility == "visible"){
		document.getElementById('idxmoreimg').src = "<%=webServer%>/skin5/mm011/m_horidxmore.png";
		indexDiv.innerHTML = "";
		indexDiv.style.visibility = "hidden";
	}
	else{
		document.getElementById('idxmoreimg').src = "<%=webServer%>/skin5/mm011/m_horidxmore_over.png";
		indexDiv.style.top = cataRect.y+"px";
		indexDiv.innerHTML = document.getElementById("horidx_div").innerHTML;
		indexDiv.style.visibility = "visible";
	}
}
MenuObj.groupClick = function(s){
	var sline;
	for(var i = 0;i < MenuObj.armenu.length;i++){
		sline = MenuObj.armenu[i];
		if(sline === s) document.getElementById(sline).style.visibility = "visible";
		else document.getElementById(sline).style.visibility = "hidden";
	}
	document.getElementById('mmgbackbtn').style.visibility = "visible";
	document.getElementById('mmgsect').style.visibility = "hidden";
	if(searchDiv) searchDiv.style.visibility = "hidden";
}
MenuObj.groupBackClick = function(){
	var sline;
	for(var i = 0;i < MenuObj.armenu.length;i++){
		sline = MenuObj.armenu[i];
		document.getElementById(sline).style.visibility = "hidden";
	}
	document.getElementById('mmgbackbtn').style.visibility = "hidden";
	document.getElementById('mmgsect').style.visibility = "visible";
	if(searchDiv) searchDiv.style.visibility = "visible";
}
MenuObj.mmpopClick = function(){
	if(mmpopDiv.style.visibility == "hidden") mmpopDiv.style.visibility = "visible";
	else mmpopDiv.style.visibility = "hidden";
}
MenuObj.onresize_func = function(){
}

if(document.getElementById("mmpopsect")) mmpopDiv = document.getElementById('mmpopsect');
if(document.getElementById("snssect")) snsDiv = document.getElementById("snssect");
if(document.getElementById("mediasect")) mediaDiv = document.getElementById("mediasect");
if(document.getElementById("searchsect")) searchDiv = document.getElementById("searchsect");
if(document.getElementById("mediactrl")) mediaPlayer = document.getElementById("mediactrl");

OnsmObj = {};
OnsmObj.set_location = function(){
}
OnsmObj.set_visible = function(b1, b2){
}
</script>
<% } %>
