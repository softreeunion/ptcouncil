<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%
	request.setCharacterEncoding("utf-8");

	String incView = request.getParameter("incview");
	String indexType = request.getParameter("indexType");
	String combokind = request.getParameter("combokind");
	String downFile = request.getParameter("downFile");
	String downFileName = request.getParameter("downFileName");
	String downexten = request.getParameter("downexten");
	String downtext = URLDecoder.decode(request.getParameter("downtext"),"utf-8");
	String mediaFile = request.getParameter("mediaFile");
	String webServer = request.getParameter("webServer");
	String printKindmc = request.getParameter("printKindmc");
	String dirName = URLDecoder.decode(request.getParameter("dirName"),"utf-8");
	String searchFile = request.getParameter("searchFile");
	String mokchaFile = request.getParameter("mokchaFile");
	String skinMedia = request.getParameter("skinMedia");
	String skinSpacer = request.getParameter("skinSpacer");
	String mailButtonHide = request.getParameter("mailButtonHide");
	String Dir = request.getParameter("Dir");
	String catimage = request.getParameter("catimage");
	String webpath = webServer + "/catImage" + catimage + "/" + Dir;
%>
<% if(incView.equals("d")){ %>

<!-- background -->
<div id="backsect" class="back5d472a"></div>

<!-- navigation -->
<nav id="mmsect" class="mm5dD472a-idxR">
<div class="mm_div5dD472a">

<ul class="mm_ulE001">
</ul>
</div>

<ul class="mm_ulE002">
<div id="searchsect" class="search5dD472a">
<% if(searchFile.equals("true")){ %>
<form name="searchform" method="get" action="#" onsubmit="return do_searchbtnClick(document.searchform.qtxt.value)" title="페이지 검색">
<input type="search" class="searchtxt5dD472a" name="qtxt" title="검색어 입력" onfocus="do_qtxtFocusIn(this)">
<input type="image" class="searchbtn5dD472a hover_opac8" src="./skin5/mm472a/search_btn.png" alt="검색 시작">
</form>
<% } %>
</div>
<li id="indexbtn" class="indexbtn5dD472a"><a href="javascript:indexbtnClick();" title="목차 보기"><img src="./skin5/mm472a/indexbtn.png" width="73" height="43" class="hover_opac8" alt="목차 보기"></a></li>
<li id="printbtn" class="printbtn5dD472a"><a href="javascript:printbtnClick();" title="인쇄하기"><img src="./skin5/mm472a/printbtn.png" width="75" height="43" class="hover_opac8" alt="인쇄하기"></a></li>
<li id="expbtn" class="expbtn5dD472a"><a href="javascript:expbtnClick();"title="탐색기 보기"><img src="./skin5/mm472a/expbtn.png" width="70" height="43" class="hover_opac8" alt="탐색기 보기"></a></li>
<li id="glassesbtn" class="glassesbtn5dD472a"><a href="javascript:glassesbtnClick();" title="돋보기로 보기"><img src="./skin5/mm472a/glassesbtn.png" width="74" height="43" class="hover_opac8" alt="돋보기로 보기"></a></li>
<li id="slidebtn" class="slidebtn5dD472a"><a href="javascript:slidebtnClick();" title="자동넘김"><img src="./skin5/mm472a/slidebtn.png" width="77" height="43" class="hover_opac8" alt="자동넘김"></a></li>
<li id="fullscrbtn" class="fullscrbtn5dD472a"><a href="javascript:fullscrbtnClick();" title="전체화면으로 보기"><img src="./skin5/mm472a/fullscrbtn.png" width="100" height="43" class="hover_opac8" alt="전체화면으로 보기"></a></li>
</ul>
</nav>

<div id="lowersect" class="lower5dD472a">

<!-- sns -->
<nav id="snssect" class="sns_nav5dD472a">
<ul class="sns_ul5dD472a">
<li id="facebookbtn" class="sns_liE472a hover_opac8"><a href="javascript:facebookbtnClick();" 
  title="페이스북 공유"><img src="./skin5/mm472a/facebookbtn.png" class="sns_liE472a" alt="페이스북"></a></li>
<li id="twitterbtn" class="sns_liE472a hover_opac8"><a href="javascript:twitterbtnClick();" 
  title="트위터 공유"><img src="./skin5/mm472a/twitterbtn.png" class="sns_liE472a" alt="트위터"></a></li>
</ul>
<!-- download -->
<% if(downFile.equals("true")){ %>
<div id="downsect" class="down5dD472a">
<a href="<%=webpath%>/<%=downFileName%>" download="<%=webpath%>/<%=downFileName%>"
  title="파일 다운로드"><img src="<%=webServer%>/skin5/icon/file_<%=downexten%>.gif" class="down_img" alt="파일 다운로드"><%=downtext%></a>
</div>
<% } %>
</nav>

<!-- search -->
<ul class="mm_ulE003">
<li id="firstbtn" class="firstbtn5dD472a"><a href="javascript:firstbtnClick();" title="처음 페이지로 이동"><img src="./skin5/mm472a/firstbtn.png" width="65" height="43" class="hover_opac8" alt="처음 페이지"></a></li>
<li id="prevbtn" class="prevbtn5dD472a"><a href="javascript:prevbtnClick();" title="이전 페이지로 이동"><img src="./skin5/mm472a/prevbtn.png" width="57" height="43" class="hover_opac8" alt="이전 페이지"></a></li>
<li id="pageshow" class="pshow5dD472a">
<form name="pageshowform" method="get" action="ecatalog.php" onsubmit="return MenuObj.do_pagenoFormSubmit();" title="페이지 이동">
<p class="pshow_p5dD472a"><input type="text" name="pageno" value="" onfocus="MenuObj.do_pagenoFocusIn(event);" onblur="MenuObj.do_pagenoFocusOut(event)"
  pattern="[0-9-]+" title="숫자만 입력 가능합니다." required></p>
<p id="pshow_totalpage"></p>
</form>
</li>
<li id="nextbtn" class="nextbtn5dD472a"><a href="javascript:nextbtnClick();" title="다음 페이지로 이동"><img src="./skin5/mm472a/nextbtn.png" width="57" height="43" class="hover_opac8" alt="다음 페이지"></a></li>
<li id="lastbtn" class="lastbtn5dD472a"><a href="javascript:lastbtnClick();" title="마지막 페이지로 이동"><img src="./skin5/mm472a/lastbtn.png" width="65" height="43" class="hover_opac8" alt="마지막 페이지"></a></li>
</ul>

<!-- combobox -->
<% if(!combokind.equals("000")){ %>
<a href="javascript:comboClick();"><div id="combosect" class="cbbox5dD472a" title="다른 카탈로그로 이동" 
data-width="display" data-align="left" data-direction="top" data-vpos="-3"><img src="./skin5/mm472a/combo_txt.png" width="143" height="30" 
class="hover_opac8" alt="다른 카탈로그 이동"></div></a>
<% } %>
</div>

<script>
MenuObj = {storedStr:''};
MenuObj.onload_func2 = function(){
	var n = (PageInfo.showStartPage == 0) ? PageInfo.cataPages : PageInfo.get_showPageFromReal(PageInfo.cataPages);
	document.getElementById("pshow_totalpage").innerHTML = n;

	if(document.getElementById("horidx")){
		indexDiv = document.getElementById("horidx");
		AniObj.horidxY = 47;
	}

	if(LinkInfo.facebookStr === "" && document.getElementById("facebookbtn")) document.getElementById("facebookbtn").style.display = "none";
	if(LinkInfo.twitterStr === "" && document.getElementById("twitterbtn")) document.getElementById("twitterbtn").style.display = "none";
	if(MenuInfo.mailButtonHide === "Y" && document.getElementById("mailbtn")) document.getElementById("mailbtn").style.display = "none";
	if((MenuInfo.indexType === "H" || FileInfo.idxFile === false) && document.getElementById("indexbtn")) document.getElementById("indexbtn").style.display = "none";
	if(SkinInfo.printkind === "000" && document.getElementById("printbtn")) document.getElementById("printbtn").style.display = "none";
}
MenuObj.show_pageNumber = function(){
	document.pageshowform.pageno.value = MoveInfo.get_showPageNumber();
}
MenuObj.do_pagenoFocusIn = function(e){
	MenuObj.storedStr = document.pageshowform.pageno.value;
	document.pageshowform.pageno.value = "";
}
MenuObj.do_pagenoFocusOut = function(e){
	document.pageshowform.pageno.value = MenuObj.storedStr;
}
MenuObj.do_pagenoFormSubmit = function(){
	var n = PermitMan.get_directGoPerm("showboard", document.pageshowform.pageno.value, "", "");			// device, spage, pageshow, state
	if(n < 0) return false;
	go_general(n, "user");
	return false;
}
MenuObj.onresize_func = function(){
}

if(document.getElementById("combosect")) comboDiv = document.getElementById("combosect");
if(document.getElementById("downsect")) downDiv = document.getElementById("downsect");
if(document.getElementById("searchsect")) searchDiv = document.getElementById("searchsect");
if(document.getElementById("mediasect")){
	mediaDiv = document.getElementById("mediasect");
	mediaPlayer = document.getElementById("mediactrl");
}
if(document.getElementById("snssect")) snsDiv = document.getElementById("snssect");
</script>

<!-- onsmimage -->
<div id="onsmlsect" class="onsmlE472a">
<a href="javascript:leftOnsmClick();" title="이전 페이지로 이동"><img src="./skin5/mm472a/onsmimageL472a.png" width="33" height="64" alt="이전 페이지"></a>
</div>
<div id="onsmrsect" class="onsmrE472a">
<a href="javascript:rightOnsmClick();" title="다음 페이지로 이동"><img src="./skin5/mm472a/onsmimageR472a.png" width="33" height="64" alt="다음 페이지"></a>
</div>
<script>
onsmlDiv = document.getElementById("onsmlsect");
onsmrDiv = document.getElementById("onsmrsect");
OnsmObj = {};
OnsmObj.set_location = function(){
	onsmlDiv.style.left = winRect.x + "px";
	onsmlDiv.style.top = (smRect.centerPt.y-33) + "px";
	onsmrDiv.style.left = (winRect.right-54) + "px";
	onsmrDiv.style.top = (smRect.centerPt.y-33) + "px";
}
OnsmObj.set_visible = function(b1, b2){
	onsmlDiv.style.visibility = b1;
	onsmrDiv.style.visibility = b2;
}
OnsmObj.prepare_enlarge = function(){
}
OnsmObj.complete_downsize = function(){
}
</script>

<% } else if(incView.equals("m")){ %>

<div id="backsect" class="back5m472a"></div>

<!-- mm navigation -->
<nav id="mmsect" class="mm5mD472a">
<div id="mmpopbtn" class="mmpopbtn5mD472a"><a href="javascript:MenuObj.mmpopClick();" title="이동메뉴"><img src="./skin5/mm041/m_morebtn.png"
  class="mm_ul_imgE007" alt="이동메뉴"></a></div>
<div id="mmturnbtn" class="mmturnbtn5mD472a"><a href="javascript:MenuObj.mmturnClick();" title="펼쳐보기"><img id="mmturnbtnpix" src="./skin5/mm041/m_expandbtn.png"
  class="mm_ul_imgE006" alt="펼쳐보기"></a></div>
<!--<div class="mmlogo5mD472a"><img id="mmlogopix" src="./skin5/mm472a/m_logo.png"  class="mm_ul_imgE008" alt="로고"></div>-->
</nav>

<!-- mmpop menu -->
<nav id="mmpopsect" class="mmpop5mD472a" style="visibility:hidden;">
<ul class="mmpop_ul5mD472a">
<% if(mokchaFile.equals("true")){ %>
<li id="indexbtn"><a href="javascript:indexbtnClick();MenuObj.mmpopClick();" title="목차 보기">
	<img src="<%=webServer%>/skin5/mm041/m_indexbtn.png" width="24" height="24" alt="목차"><p>목차</p>
	<span><img src="<%=webServer%>/skin5/mm472a/m_poplimark.png" class="mm_ul_imgE009"></span></a></li>
<% } %>
<li id="expbtn"><a href="javascript:expbtnClick();MenuObj.mmpopClick();" title="탐색기 보기">
	<img src="<%=webServer%>/skin5/mm041/m_expbtn.png" width="24" height="24" alt="탐색기"><p>썸네일</p>
	<span><img src="<%=webServer%>/skin5/mm472a/m_poplimark.png" class="mm_ul_imgE009"></span></a></li>
<% if(!skinSpacer.equals("000")){ %>
<li id="spacerbtn"><a href="javascript:spacerbtnClick('left');MenuObj.mmpopClick();" title="책갈피에 추가">
	<img src="<%=webServer%>/skin5/mm041/m_spacerbtn.png" width="24" height="24" alt="책갈피"><p>책갈피</p>
	<span><img src="<%=webServer%>/skin5/mm472a/m_poplimark.png" class="mm_ul_imgE009"></span></a></li>
<% } %>
<% if(!combokind.equals("000")){ %>
<li id="combobtn"><a href="javascript:combobtnClick();MenuObj.mmpopClick();" title="다른 카탈로그 이동 페이지">
	<img src="<%=webServer%>/skin5/mm041/m_combobtn.png" width="24" height="24" alt="다른 카탈로그"><p>다른 eBook 보기</p>
	<span><img src="<%=webServer%>/skin5/mm472a/m_poplimark.png" class="mm_ul_imgE009"></span></a></li>
<% } %>
</ul>

<% if(searchFile.equals("true")){ %>
<div id="searchsect" class="search5mD472a">
<form name="searchform" method="get" action="#" onsubmit="return MenuObj.searchClick(document.searchform.qtxt.value)" title="페이지 검색">
<input type="search" class="searchtxt5mD472a" name="qtxt" title="검색어 입력">
<input type="image" class="searchbtn5mD472a" src="./skin5/mm041/m_searchbtn.png" title="검색">
</form>
</div>
<% } %>

<!-- lower more sns group menu -->
<div id="snssect" class="sns_nav5mD472a">
<ul class="sns_ul5mD472a">
<li id="facebookbtn" class="facebookbtn5mD472a"><a href="javascript:facebookbtnClick();" title="페이스북 공유"><img src="./skin5/mm041/m_facebookbtn.png" width="10" height="20" alt="페이스북"></a></li>
<li id="twitterbtn" class="twitterbtn5mD472a"><a href="javascript:twitterbtnClick();" title="트위터 공유"><img src="./skin5/mm041/m_twitterbtn.png" width="28" height="20" alt="트위터"></a></li>
<li id="kakaobtn" class="kakaobtn5mD472a"><a href="javascript:kakaobtnClick();" title="카카오 공유"><img src="./skin5/mm041/m_kakaobtn.png" width="21" height="20" alt="카카오"></a></li>
</ul>

<!-- download -->
<% if(downFile.equals("true")){ %>
<p id="downbtn" class="downbtn5mD472a"><a href="<%=webpath%>/<%=downFileName%>" download="<%=webpath%>/<%=downFileName%>" 
  title="파일 다운로드"><img src="<%=webServer%>/skin5/mm041/m_downbtn.png" width="21" height="20" alt="파일 다운로드"></a></p>
</div>
<% } %>
</nav>

<!-- onsmimage -->
<div id="onsmlsect" class="onsmlE041">
<a href="javascript:leftOnsmClick();" title="이전 페이지로 이동"><img src="./skin5/onsm/onsmimageL041.png" width="33" height="49" alt="이전 페이지"></a>
</div>
<div id="onsmrsect" class="onsmlE041">
<a href="javascript:rightOnsmClick();" title="다음 페이지로 이동"><img src="./skin5/onsm/onsmimageR041.png" width="33" height="49" alt="다음 페이지"></a>
</div>

<script>
MenuObj = {storedStr:''};
MenuObj.onload_func2 = function(){
	if(LinkInfo.facebookStr === "" && document.getElementById("facebookbtn")) document.getElementById("facebookbtn").style.opacity = "0.2";
	if(LinkInfo.twitterStr === "" && document.getElementById("twitterbtn")) document.getElementById("twitterbtn").style.opacity = "0.2";
	if(LinkInfo.kakaoStr === "" && document.getElementById("kakaobtn")) document.getElementById("kakaobtn").style.opacity = "0.2";

	if(logoDiv) logoDiv.style.left = Math.floor(stageWidth/2-logoDiv.clientWidth/2) + "px";
}
MenuObj.show_pageNumber = function(){
}
MenuObj.onresize_func = function(){
}
MenuObj.mmpopClick = function(){
	if(mmpopDiv.style.visibility == "hidden"){
		mmpopDiv.style.visibility = "visible";
	}
	else{
		mmpopDiv.style.visibility = "hidden";
	}
}
MenuObj.mmturnClick = function(){
	if(Action.mainState === "expand"){
		document.getElementById('mmturnbtnpix').src = "./skin5/mm041/m_expandbtn.png";
		unload_expand();
	}
	else{
		document.getElementById('mmturnbtnpix').src = "./skin5/mm041/m_turnbtn.png";
		load_expand();
	}
}
MenuObj.searchClick = function(s){
	if(s === "") return false;
	if(PermitMan.normalPerm(-1) === false) return false;
	if(searchSvg) return false;

	MenuObj.mmpopClick();
	SearchInfo.set_qtxt(s);
	load_search();
	return false;
}

mmpopDiv = document.getElementById('mmpopsect');
snsDiv = document.getElementById("snssect");
if(document.getElementById("searchsect")) searchDiv = document.getElementById("searchsect");

onsmlDiv = document.getElementById("onsmlsect");
onsmrDiv = document.getElementById("onsmrsect");
OnsmObj = {};
OnsmObj.set_location = function(){
	onsmlDiv.style.left = winRect.x + "px";
	onsmlDiv.style.top = (smRect.centerPt.y-25) + "px";
	onsmrDiv.style.left = (winRect.right-33) + "px";
	onsmrDiv.style.top = (smRect.centerPt.y-25) + "px";
}
OnsmObj.set_visible = function(b1, b2){
	onsmlDiv.style.visibility = b1;
	onsmrDiv.style.visibility = b2;
}
</script>
<% } %>