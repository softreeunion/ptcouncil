<%@ page pageEncoding="utf-8" %>
<% if(n.equals("001")){ %>
<!-- normal window(001) -->
<script>
	ws = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">'
	+ '<defs>'
	+ '<linearGradient id="newin_titleGradient<%=plusID%>">';
<% if(!Display.get("user").equals("")){ %>
	ws += '<stop offset="0%" stop-color="<%=Display.get("nwcolor1_d")%>" />'
	+ '<stop offset="100%" stop-color="<%=Display.get("nwcolor2_d")%>" />';
<% } else if(d.equals("001")){ %>
	ws += '<stop offset="0%" stop-color="#5A7685" />'
	+ '<stop offset="100%" stop-color="#A9B5C2" />';
<% } else if(d.equals("010")){ %>
	ws += '<stop offset="0%" stop-color="#111111" />'
	+ '<stop offset="100%" stop-color="#666666" />';
<% } else if(d.equals("011")){ %>
	ws += '<stop offset="0%" stop-color="#41830F" />'
	+ '<stop offset="100%" stop-color="#394F20" />';
<% } else if(d.equals("020")){ %>
	ws += '<stop offset="0%" stop-color="#555555" />'
	+ '<stop offset="100%" stop-color="#999999" />';
<% } else if(d.equals("030")){ %>
	ws += '<stop offset="0%" stop-color="#329FD0" />'
	+ '<stop offset="100%" stop-color="#96E5ED" />';
<% } else if(d.equals("301")){ %>
	ws += '<stop offset="0%" stop-color="#5283c0" />'
	+ '<stop offset="100%" stop-color="#96c8ff" />';
<% } else if(d.equals("302")){ %>
	ws += '<stop offset="0%" stop-color="#121212" />'
	+ '<stop offset="100%" stop-color="#3C476F" />';
<% } %>
	ws += '</linearGradient>'
	+ '</defs>';

	// window title section
	ws += '<g>'
	+ '<rect id="newin_titlerect<%=plusID%>" x="0" y="0" width="<%=w%>" height="21" onmousedown="NewinObj.do_mousedownTitle(evt, <%=svgObj%>);" fill="url(#newin_titleGradient<%=plusID%>)" />';

<% if(Display.get("titlepix").equals("")){ %>
	ws += '<text id="newin_titletext<%=plusID%>" class="newin_titletext5d" x="3" y="14" fill="#ffffff"><%=newin_titletext%></text>';
<% } else{ %>
	ws += '<image id="newin_titleimage<%=plusID%>" x="10" y="6" width="<%=Display.get("titlepix_w")%>"'
	+ ' height="<%=Display.get("titlepix_h")%>" xlink:href="<%=webServer%>/skin5/<%=Display.get("titlepix")%>">'
	+ '<title><%=newin_titletext%></title>'
	+ '</image></a>';
<% } %>

<% if(fullscrbtn == 1){ %>
	ws += '<a xlink:href="javascript:<%=svgObj%>.newin_fullscr();"><image id="newin_titlefullscrbtn" class="hover_opac6" cursor="pointer" '
  	+ 'x="<%=(w-30)%>" y="5" width="11" height="11" xlink:href="<%=webServer%>/skin5/win/fullscr_11.png">'
	+ '<title>전체화면으로 보기</title>'
	+ '</image></a>';
<% } %>
<% if(closebtn == 1){ %>
	ws += '<a xlink:href="javascript:<%=svgObj%>.newin_close();"><image id="newin_titleclosebtn<%=plusID%>" class="hover_opac6" cursor="pointer" '
  	+ 'x="<%=(w-15)%>" y="5" width="11" height="11" xlink:href="<%=webServer%>/skin5/win/close_11.png">'
	+ '<title>창닫기</title>'
	+ '</image></a>';
<% } %>
	ws += '</g>';

	// window body section
	ws += '<line id="newin_bodyTline<%=plusID%>" x1="0.5" y1="22.5" x2="<%=w%>" y2="22.5" stroke="#666666" />'
	+ '<line id="newin_bodyLline<%=plusID%>" x1="0.5" y1="22.5" x2="0.5" y2="<%=h%>" stroke="#666666" />'
	+ '<line id="newin_bodyRline<%=plusID%>" x1="<%=(w-0.5)%>" y1="22.5" x2="<%=(w-0.5)%>" y2="<%=(h-0.5)%>" stroke="#bbbbbb" />'
	+ '<line id="newin_bodyBline<%=plusID%>" x1="0.5" y1="<%=(h-0.5)%>" x2="<%=(w-0.5)%>" y2="<%=(h-0.5)%>" stroke="#bbbbbb" />'
	+ '</svg>'
	+ '</div>';

// from spacer/explorer (after deciding the window size in the object's html)
function newin_size(){
	p.document.getElementById('newin_titlerect<%=plusID%>').setAttribute("width", svgWidth+0.5);
<% if(fullscrbtn == 1){ %>
	p.document.getElementById('newin_titlefullscrbtn').setAttribute("x", svgWidth-30);
<% } %>
<% if(closebtn == 1){ %>
	p.document.getElementById('newin_titleclosebtn<%=plusID%>').setAttribute("x", svgWidth-15);
<% } %>

	p.document.getElementById('newin_bodyTline<%=plusID%>').setAttribute("x2", svgWidth);
	p.document.getElementById('newin_bodyLline<%=plusID%>').setAttribute("y2", svgHeight);

	p.document.getElementById('newin_bodyRline<%=plusID%>').setAttribute("x1", svgWidth-0.5);
	p.document.getElementById('newin_bodyRline<%=plusID%>').setAttribute("x2", svgWidth-0.5);
	p.document.getElementById('newin_bodyRline<%=plusID%>').setAttribute("y2", svgHeight-0.5);

	p.document.getElementById('newin_bodyBline<%=plusID%>').setAttribute("y1", svgHeight-0.5);
	p.document.getElementById('newin_bodyBline<%=plusID%>').setAttribute("y2", svgHeight-0.5);
	p.document.getElementById('newin_bodyBline<%=plusID%>').setAttribute("x2", svgWidth-0.5);
}
function newin_fullscr2(){
<% if(fullscrbtn == 1){ %>
	p.document.getElementById('newin_titlefullscrbtn').setAttribute("x", p.stageWidth-36);
<% } %>
<% if(closebtn == 1){ %>
	p.document.getElementById('newin_titleclosebtn<%=plusID%>').setAttribute("x", p.stageWidth-21);
<% } %>
	p.document.getElementById('newin_titlerect<%=plusID%>').setAttribute("width", p.stageWidth-6);
	p.document.getElementById('newin_bodyTline<%=plusID%>').setAttribute("visibility", "hidden");
	p.document.getElementById('newin_bodyRline<%=plusID%>').setAttribute("visibility", "hidden");
	p.document.getElementById('newin_bodyBline<%=plusID%>').setAttribute("visibility", "hidden");
	p.document.getElementById('newin_bodyLline<%=plusID%>').setAttribute("visibility", "hidden");
}
function newin_restore(){
<% if(fullscrbtn == 1){ %>
	p.document.getElementById('newin_titlefullscrbtn').setAttribute("x", svgWidth-30);
<% } %>
<% if(closebtn == 1){ %>
	p.document.getElementById('newin_titleclosebtn<%=plusID%>').setAttribute("x", svgWidth-15);
<% } %>
	p.document.getElementById('newin_titlerect<%=plusID%>').setAttribute("width", svgWidth);
	p.document.getElementById('newin_bodyTline<%=plusID%>').setAttribute("visibility", "visible");
	p.document.getElementById('newin_bodyRline<%=plusID%>').setAttribute("visibility", "visible");
	p.document.getElementById('newin_bodyBline<%=plusID%>').setAttribute("visibility", "visible");
	p.document.getElementById('newin_bodyLline<%=plusID%>').setAttribute("visibility", "visible");
}
function swap_fullscrimage(s){
	p.document.getElementById('newin_titlefullscrbtn').setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", "<%=webServer%>/skin5/win/"+s+"_11.png");
}
</script>

<!-- window body button section -->
<% if(skind.equals("close")){ %>
<a href="javascript:newin_close();" title="창닫기">
<svg version="1.1" id="newin_bodybtn" class="newin_bodybtn001" width="123" height="25" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g onmousedown="this.setAttribute('transform','translate(1,1)');" onmouseup="this.setAttribute('transform','translate(0,0)');">
	<image x="0.5" y="0.5" width="121" height="23" xlink:href="<%=webServer%>/skin5/win/nw_bodyclose.png" />
	<text x="35" y="16">닫<tspan dx="30">기</tspan></text>
</g>
</svg></a>
<% } else if(skind.equals("send")){ %>
<a href="javascript:mailSend();" title="메일 보내기">
<svg version="1.1" id="newin_bodybtn" class="newin_bodybtn001" width="123" height="25" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g onmousedown="this.setAttribute('transform','translate(1,1)');" onmouseup="this.setAttribute('transform','translate(0,0)');">
	<image x="0.5" y="0.5" width="121" height="23" xlink:href="<%=webServer%>/skin5/win/nw_bodyclose.png" />
	<text x="28" y="16">보<tspan dx="13">내</tspan><tspan dx="13">기</tspan></text>
</g>
</svg></a>
<% } %>

<% } else if(n.equals("012")){ // korea window %>
<%
	String n_icon = "";
	if(d.equals("013")) n_icon = "013";
%>
<!-- korea window(012) -->
<script>
	ws = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">'
	+ '<g onmousedown="NewinObj.do_mousedownTitle(evt, <%=svgObj%>);">';

<% if(d.equals("013")){ %>
	ws += '<rect x="0" y="0" width="<%=(w+0.5)%>" height="30" fill="#565656" />';
<% } else{ %>
	ws += '<image id="newin_titleback1<%=plusID%>" x="0" y="0" width="<%=(w+0.5)%>" height="30" xlink:href="<%=webServer%>/skin5/win/kw_titleback.png" preserveAspectRatio="none" />'
	+ '<image id="newin_titleback2<%=plusID%>" x="<%=(w-238)%>" y="0" width="238" height="30" xlink:href="<%=webServer%>/skin5/win/kw_titlewave.png" />';
<% } %>

<% if(Display.get("titlepix").equals("")){ %>
	ws += '<text id="newin_titletext<%=plusID%>" class="newin_titletext5d" x="5" y="19" fill="#ffffff"><%=newin_titletext%></text>';
<% } else{ %>
	ws += '<image id="newin_titleimage<%=plusID%>" x="10" y="6" width="<%=Display.get("titlepix_w")%>"'
	+ ' height="<%=Display.get("titlepix_h")%>" xlink:href="<%=webServer%>/skin5/<%=Display.get("titlepix")%>">'
	+ '<title><%=newin_titletext%></title>'
	+ '</image></a>';
<% } %>

<% if(fullscrbtn == 1){ %>
	ws += '<a xlink:href="javascript:javascript:<%=svgObj%>.newin_fullscr();"><image id="newin_titlefullscrbtn" class="hover_opac6" cursor="pointer" '
  	+ 'x="<%=(w-40)%>" y="8" width="15" height="14" xlink:href="<%=webServer%>/skin5/win/kw_fullscr<%=n_icon%>.png">'
	+ '<title>전체화면으로 보기</title>'
	+ '</image></a>';
<% } %>
<% if(closebtn == 1){ %>
	ws += '<a xlink:href="javascript:<%=svgObj%>.newin_close();"><image id="newin_titleclosebtn<%=plusID%>" class="hover_opac6" cursor="pointer" '
  	+ 'x="<%=(w-21)%>" y="8" width="15" height="14" xlink:href="<%=webServer%>/skin5/win/kw_titleclose<%=n_icon%>.png">'
	+ '<title>창닫기</title>'
	+ '</image></a>';
<% } %>
	ws += '</g></svg>';
</script>

<% if(skind.equals("close")){ %>
<a href="javascript:newin_close();" title="창닫기">
<svg version="1.1" id="newin_bodybtn" class="newin_bodyclosebtn012" width="38" height="21" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g class="hover_image1hide" onmousedown="this.setAttribute('transform','translate(1,1)');" onmouseup="this.setAttribute('transform','translate(0,0)');">
	<image x="0.5" y="0.5" width="36" height="19" xlink:href="<%=webServer%>/skin5/win/kw_bodyclose_over.png" />
	<image x="0.5" y="0.5" width="36" height="19" xlink:href="<%=webServer%>/skin5/win/kw_bodyclose.png" />
</g>
</svg></a>
<% } else if(skind.equals("send")){ %>
<a href="javascript:mailSend();" title="메일 보내기">
<svg version="1.1" id="newin_bodybtn" class="newin_bodysendbtn012" width="46" height="21" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g class="hover_image1hide" onmousedown="this.setAttribute('transform','translate(1,1)');" onmouseup="this.setAttribute('transform','translate(0,0)');">
	<image x="0.5" y="0.5" width="44" height="19" xlink:href="<%=webServer%>/skin5/win/kw_bodysend_over.png" />
	<image x="0.5" y="0.5" width="44" height="19" xlink:href="<%=webServer%>/skin5/win/kw_bodysend.png" />
</g>
</svg></a>
<% } %>

<script>
function newin_size(){		// from spacer/explorer
	p.document.getElementById('newin_titleback1<%=plusID%>').setAttribute("width", svgWidth+0.5);
	p.document.getElementById('newin_titleback2<%=plusID%>').setAttribute("x", svgWidth-238);

<% if(fullscrbtn == 1){ %>
	p.document.getElementById('newin_titlefullscrbtn').setAttribute("x", svgWidth-40);
<% } %>
<% if(closebtn == 1){ %>
	p.document.getElementById('newin_titleclosebtn<%=plusID%>').setAttribute("x", svgWidth-21);
<% } %>
}

function newin_fullscr2(){
<% if(fullscrbtn == 1){ %>
	p.document.getElementById('newin_titlefullscrbtn').setAttribute("x", svgWidth-30);
<% } %>
<% if(closebtn == 1){ %>
	p.document.getElementById('newin_titleclosebtn<%=plusID%>').setAttribute("x", svgWidth-15);
<% } %>
}
function newin_restore(){
<% if(fullscrbtn == 1){ %>
	p.document.getElementById('newin_titlefullscrbtn').setAttribute("x", svgWidth-30);
<% } %>
<% if(closebtn == 1){ %>
	p.document.getElementById('newin_titleclosebtn<%=plusID%>').setAttribute("x", svgWidth-15);
<% } %>
}
function swap_fullscrimage(s){
	p.document.getElementById('newin_titlefullscrbtn').setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", "<%=webServer%>/skin5/win/kw_"+s+"<%=n_icon%>.png");
}
</script>

<% } else if(n.equals("016")){ %>
<!-- simple window(016) -->
<script>
	ws = '<svg version="1.1" width="<%=w%>" height="<%=h%>" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">'
	+ '<g onmousedown="NewinObj.do_mousedownTitle(evt, <%=svgObj%>);">'
	+ '<image id="newin_titleback1<%=plusID%>" x="0" y="0" width="6" height="31" xlink:href="<%=webServer%>/skin5/win/sw_body<%=d%>1.png" />'
	+ '<image id="newin_titleback2<%=plusID%>" x="6" y="0" width="<%=(w-14)%>" height="31" xlink:href="<%=webServer%>/skin5/win/sw_body<%=d%>2.png" preserveAspectRatio="none" />'
	+ '<image id="newin_titleback3<%=plusID%>" x="<%=(w-8)%>" y="0" width="8" height="31" xlink:href="<%=webServer%>/skin5/win/sw_body<%=d%>3.png" />';

<% if(Display.get("titlepix").equals("")){ %>
	ws += '<text id="newin_titletext<%=plusID%>" class="newin_titletext5d" x="8" y="20" fill="#ffffff"><%=newin_titletext%></text>';
<% } else{ %>
	ws += '<image id="newin_titleimage<%=plusID%>" x="10" y="6" width="<%=Display.get("titlepix_w")%>"'
	+ ' height="<%=Display.get("titlepix_h")%>" xlink:href="<%=webServer%>/skin5/<%=Display.get("titlepix")%>">'
	+ '<title><%=newin_titletext%></title>'
	+ '</image></a>';
<% } %>

<% if(fullscrbtn == 1){ %>
	ws += '<a xlink:href="javascript:<%=svgObj%>.newin_fullscr();"><image id="newin_titlefullscrbtn" class="hover_opac6" cursor="pointer" '
	+ 'x="<%=(w-40)%>" y="8" width="16" height="15" xlink:href="<%=webServer%>/skin5/win/sw_fullscr.png">'
	+ '<title>전체화면으로 보기</title>'
	+ '</image></a>';
<% } %>
<% if(closebtn == 1){ %>
	ws += '<a xlink:href="javascript:<%=svgObj%>.newin_close();"><image id="newin_titleclosebtn<%=plusID%>" class="hover_opac6" cursor="pointer" '
	+ 'x="<%=(w-25)%>" y="8" width="16" height="15" xlink:href="<%=webServer%>/skin5/win/sw_titleclose<%=d%>.png">'
	+ '<title>창닫기</title>'
	+ '</image></a>';
<% } %>
	ws += '</g>';
<% int h1 = h - 31 - 9; %>
	ws += '<image id="newin_bodyback1<%=plusID%>" x="0" y="31" width="6" height="<%=h1%>" xlink:href="<%=webServer%>/skin5/win/sw_body4.png" preserveAspectRatio="none" />'
	+ '<image id="newin_bodyback2<%=plusID%>" x="6" y="31" width="<%=(w-14)%>" height="<%=h1%>" xlink:href="<%=webServer%>/skin5/win/sw_body5.png" preserveAspectRatio="none" />'
	+ '<image id="newin_bodyback3<%=plusID%>" x="<%=(w-8)%>" y="31" width="8" height="<%=h1%>" xlink:href="<%=webServer%>/skin5/win/sw_body6.png" preserveAspectRatio="none" />';

<% int y1 = h - 9; %>
	ws += '<image id="newin_bodyback4<%=plusID%>" x="0" y="<%=y1%>" width="6" height="9" xlink:href="<%=webServer%>/skin5/win/sw_body7.png" />'
	+ '<image id="newin_bodyback5<%=plusID%>" x="6" y="<%=y1%>" width="<%=(w-14)%>" height="9" xlink:href="<%=webServer%>/skin5/win/sw_body8.png" preserveAspectRatio="none" />'
	+ '<image id="newin_bodyback6<%=plusID%>" x="<%=(w-8)%>" y="<%=y1%>" width="8" height="9" xlink:href="<%=webServer%>/skin5/win/sw_body9.png" />'
	+ '</svg>';

function newin_size(){		// from spacer/explorer
	p.document.getElementById('newin_titleback2<%=plusID%>').setAttribute("width", svgWidth-14);
	p.document.getElementById('newin_titleback3<%=plusID%>').setAttribute("x", svgWidth-8);

	var h1 = svgHeight - 31 - 19;
	p.document.getElementById('newin_bodyback1<%=plusID%>').setAttribute("height", h1);
	p.document.getElementById('newin_bodyback2<%=plusID%>').setAttribute("width", svgWidth-14);
	p.document.getElementById('newin_bodyback2<%=plusID%>').setAttribute("height", h1);
	p.document.getElementById('newin_bodyback3<%=plusID%>').setAttribute("x", svgWidth-8);
	p.document.getElementById('newin_bodyback3<%=plusID%>').setAttribute("height", h1);

	var y1 = svgHeight - 19;
	p.document.getElementById('newin_bodyback4<%=plusID%>').setAttribute("y", y1);
	p.document.getElementById('newin_bodyback5<%=plusID%>').setAttribute("y", y1);
	p.document.getElementById('newin_bodyback5<%=plusID%>').setAttribute("width", svgWidth-14);
	p.document.getElementById('newin_bodyback6<%=plusID%>').setAttribute("x", svgWidth-8);
	p.document.getElementById('newin_bodyback6<%=plusID%>').setAttribute("y", y1);

<% if(fullscrbtn == 1){ %>
	p.document.getElementById('newin_titlefullscrbtn').setAttribute("x", svgWidth-40);
<% } %>
<% if(closebtn == 1){ %>
	p.document.getElementById('newin_titleclosebtn<%=plusID%>').setAttribute("x", svgWidth-21);
<% } %>
}
function newin_fullscr2(){
	p.document.getElementById('newin_titleback2<%=plusID%>').setAttribute("width", p.stageWidth-14);
	p.document.getElementById('newin_titleback3<%=plusID%>').setAttribute("x", p.stageWidth-8);

<% if(fullscrbtn == 1){ %>
	p.document.getElementById('newin_titlefullscrbtn').setAttribute("x", svgWidth-43);
<% } %>
<% if(closebtn == 1){ %>
	p.document.getElementById('newin_titleclosebtn<%=plusID%>').setAttribute("x", svgWidth-25);
<% } %>

	p.document.getElementById('newin_bodyback1<%=plusID%>').setAttribute("visibility", "hidden");
	p.document.getElementById('newin_bodyback2<%=plusID%>').setAttribute("visibility", "hidden");
	p.document.getElementById('newin_bodyback3<%=plusID%>').setAttribute("visibility", "hidden");
	p.document.getElementById('newin_bodyback4<%=plusID%>').setAttribute("visibility", "hidden");
	p.document.getElementById('newin_bodyback5<%=plusID%>').setAttribute("visibility", "hidden");
	p.document.getElementById('newin_bodyback6<%=plusID%>').setAttribute("visibility", "hidden");
}
function newin_restore(){
	p.document.getElementById('newin_titleback2<%=plusID%>').setAttribute("width", svgWidth-14);
	p.document.getElementById('newin_titleback3<%=plusID%>').setAttribute("x", svgWidth-8);

<% if(fullscrbtn == 1){ %>
	p.document.getElementById('newin_titlefullscrbtn').setAttribute("x", svgWidth-43);
<% } %>
<% if(closebtn == 1){ %>
	p.document.getElementById('newin_titleclosebtn<%=plusID%>').setAttribute("x", svgWidth-25);
<% } %>

	p.document.getElementById('newin_bodyback1<%=plusID%>').setAttribute("visibility", "visible");
	p.document.getElementById('newin_bodyback2<%=plusID%>').setAttribute("visibility", "visible");
	p.document.getElementById('newin_bodyback3<%=plusID%>').setAttribute("visibility", "visible");
	p.document.getElementById('newin_bodyback4<%=plusID%>').setAttribute("visibility", "visible");
	p.document.getElementById('newin_bodyback5<%=plusID%>').setAttribute("visibility", "visible");
	p.document.getElementById('newin_bodyback6<%=plusID%>').setAttribute("visibility", "visible");
}
function swap_fullscrimage(s){
	p.document.getElementById('newin_titlefullscrbtn').setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", "<%=webServer%>/skin5/win/sw_"+s+".png");
}
</script>

<% if(skind.equals("close")){ %>
<a href="javascript:newin_close();" title="창닫기">
<svg version="1.1" id="newin_bodybtn" class="newin_bodybtn016" width="123" height="27" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g class="hover_image1hide" onmousedown="this.setAttribute('transform','translate(1,1)');" onmouseup="this.setAttribute('transform','translate(0,0)');">
	<image x="0" y="0" width="122" height="26" xlink:href="<%=webServer%>/skin5/win/sw_bodyclose_over.png" />
	<image x="0" y="0" width="122" height="26" xlink:href="<%=webServer%>/skin5/win/sw_bodyclose.png" />
	<text x="35" y="16">닫<tspan dx="30">기</tspan></text>
</g>
</svg></a>
<% } else if(skind.equals("send")){ %>
<a href="javascript:mailSend();" title="메일 보내기">
<svg version="1.1" id="newin_bodybtn" class="newin_bodybtn016" width="124" height="28" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g class="hover_image1hide" onmousedown="this.setAttribute('transform','translate(1,1)');" onmouseup="this.setAttribute('transform','translate(0,0)');">
	<image x="0" y="0" width="122" height="26" xlink:href="<%=webServer%>/skin5/win/sw_bodyclose_over.png" />
	<image x="0" y="0" width="122" height="26" xlink:href="<%=webServer%>/skin5/win/sw_bodyclose.png" />
	<text x="28" y="18">보 <tspan dx="10"> 내</tspan><tspan dx="10">기</tspan></text>
</g>
</svg></a>
<% } %>

<% } else if(n.equals("041")){ %>
<%
	String tback = "#415c6a";			// 001
	if(!Display.get("user").equals("")) tback = (String)Display.get("nwcolor_m");
	else if(d.equals("010")) tback = "#111111";
	else if(d.equals("011")) tback = "#23451b";
	else if(d.equals("012")) tback = "#23451b";
	else if(d.equals("016")) tback = "#538C2C";
	else if(d.equals("017")) tback = "#444444";
	else if(d.equals("020")) tback = "#555555";
	else if(d.equals("030")) tback = "#329FD0";
	else if(d.equals("301")) tback = "#5283c0";
	else if(d.equals("302")) tback = "#121212";
	else if(d.equals("041")) tback = "#484848";
%>
<!-- shadow window(041) -->
<script>
	ws = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">'
	+ '<rect x="0" y="0" width="100%" height="30" fill="<%=tback%>" />';

<% if(Display.get("titlepix").equals("")){ %>
	ws += '<text id="newin_titletext<%=plusID%>" class="newin_titletext5m" x="10" y="19" fill="#ffffff"><%=newin_titletext%></text>';
<% } else{ %>
	ws += '<image id="newin_titleimage<%=plusID%>" x="10" y="6" width="<%=Display.get("titlepix_w")%>"'
	+ ' height="<%=Display.get("titlepix_h")%>" xlink:href="<%=webServer%>/skin5/<%=Display.get("titlepix")%>">'
	+ '<title><%=newin_titletext%></title>'
	+ '</image></a>';
<% } %>

<% if(closebtn == 1){ %>
	ws += '<a xlink:href="javascript:<%=svgObj%>.newin_close();"><image id="newin_titleclosebtn<%=plusID%>" class="hover_opac6" cursor="pointer" '
  	+ 'x="<%=(w-25)%>" y="6" width="15" height="15" xlink:href="<%=webServer%>/skin5/win/m_closebtn.png">'
	+ '<title>창닫기</title>'
	+ '</image></a>';
<% } %>
	ws += '</svg>';

function newin_size(){		// from load_explorer mobile only
<% if(closebtn == 1){ %>
	p.document.getElementById('newin_titleclosebtn<%=plusID%>').setAttribute("x", svgWidth-21);
<% } %>
}
</script>

<!-- window body button section -->
<% if(skind.equals("close")){ %>
<a href="javascript:newin_close();" title="창닫기">
<svg version="1.1" id="newin_bodybtn" class="newin_bodybtn041" width="123" height="25" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<rect x="0.5" y="0.5" width="121" height="23" fill="#dddddd" stroke="#cccccc" stroke-width="1" />
<text x="35" y="16">닫<tspan dx="30">기</tspan></text>
</svg></a>
<% } else if(skind.equals("send")){ %>
<a href="javascript:mailSend();" title="메일 보내기">
<svg version="1.1" id="newin_bodybtn" class="newin_bodybtn041" width="123" height="25" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<rect x="0.5" y="0.5" width="121" height="23" fill="#dddddd" stroke="#cccccc" stroke-width="1" />
<text x="28" y="16">보<tspan dx="13">내</tspan><tspan dx="13">기</tspan></text>
</svg></a>
<% } %>

<% } else if(n.equals("018")){ %>
<%
	String tback = "#415c6a";			// 001
	if(!Display.get("user").equals("")) tback = (String)Display.get("nwcolor1_d");
	else if(d.equals("010")) tback = "#111111";
	else if(d.equals("011")) tback = "#23451b";
	else if(d.equals("012")) tback = "#23451b";
	else if(d.equals("016")) tback = "#538C2C";
	else if(d.equals("017")) tback = "#444444";
	else if(d.equals("020")) tback = "#555555";
	else if(d.equals("030")) tback = "#329FD0";
	else if(d.equals("301")) tback = "#5283c0";
	else if(d.equals("302")) tback = "#121212";
	else if(d.equals("041")) tback = "#484848";
%>
<!-- hyundai window(018) -->
<script>
	ws = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">'
	+ '<rect x="0" y="0" width="100%" height="30" fill="<%=tback%>" />';
<% if(Display.get("titlepix").equals("")){ %>
	ws += '<text id="newin_titletext<%=plusID%>" class="newin_titletext5m" x="10" y="19" fill="#ffffff"><%=newin_titletext%></text>';
<% } else{ %>
	ws += '<image id="newin_titleimage<%=plusID%>" x="10" y="6" width="<%=Display.get("titlepix_w")%>"'
	+ ' height="<%=Display.get("titlepix_h")%>" xlink:href="<%=webServer%>/skin5/<%=Display.get("titlepix")%>">'
	+ '<title><%=newin_titletext%></title>'
	+ '</image></a>';
<% } %>

<% if(closebtn == 1){ %>
	ws += '<a xlink:href="javascript:<%=svgObj%>.newin_close();"><image id="newin_titleclosebtn<%=plusID%>" class="hover_opac6" cursor="pointer" '
  	+ 'x="<%=(w-25)%>" y="6" width="15" height="15" xlink:href="<%=webServer%>/skin5/win/m_closebtn.png">'
	+ '<title>창닫기</title>'
	+ '</image></a>';
<% } %>
	ws += '</svg>';

function newin_size(){		// from load_explorer mobile only
<% if(closebtn == 1){ %>
	p.document.getElementById('newin_titleclosebtn<%=plusID%>').setAttribute("x", svgWidth-21);
<% } %>
}
</script>

<!-- window body button section -->
<% if(skind.equals("close")){ %>
<a href="javascript:newin_close();" title="창닫기">
<svg version="1.1" id="newin_bodybtn" class="newin_bodybtn018 hover_opac9" width="123" height="25" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g onmousedown="this.setAttribute('transform','translate(1,1)');" onmouseup="this.setAttribute('transform','translate(0,0)');">
<rect x="0.5" y="0.5" width="121" height="23" fill="<%=tback%>" />
<text x="35" y="16" fill="#ffffff">닫<tspan dx="30">기</tspan></text>
</g>
</svg></a>
<% } else if(skind.equals("send")){ %>
<a href="javascript:mailSend();" title="메일 보내기">
<svg version="1.1" id="newin_bodybtn" class="newin_bodybtn018 hover_opac9" width="123" height="25" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g onmousedown="this.setAttribute('transform','translate(1,1)');" onmouseup="this.setAttribute('transform','translate(0,0)');">
<rect x="0.5" y="0.5" width="121" height="23" fill="<%=tback%>" />
<text x="28" y="16" fill="#ffffff">보<tspan dx="13">내</tspan><tspan dx="13">기</tspan></text>
</g>
</svg></a>
<% } %>

<% } %>
