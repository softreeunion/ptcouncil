<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%
	String tw = get_param1(request.getParameter("w"),5);
	int w = tw.equals("") ? 0 : Integer.parseInt(tw);
	String th = get_param1(request.getParameter("h"),5);
	int h = th.equals("") ? 0 : Integer.parseInt(th);
	String ds = request.getParameter("d");
	if(ds == null) ds = "";

	String[] ar = mysplit(ds,".");
	String n = "";
	if(ar.length > 1) n = get_param2(ar[1],5);			// SkinInfo.nwclass(n)
	String v = "";
	if(ar.length > 2) v = check_um(ar[2],"um");			// CataInfo.incview(v)
	String b = "";
	if(ar.length > 3) b = ar[3];			// MenuInfo.applyMax(b)
	if(!b.equals("Y") && !b.equals("N")) b = "Y";
	String d = check_mainmenu(ar[0], v);				// SkinInfo.mainmenu(d)
	String r = "explorer";

	pathOfOriCata = webSysDir + "/catImage";
	HashMap Display = new HashMap();
	Display.put("user", get_userSkin(d, v));
	if(!Display.get("user").equals("")) set_userSkin((String)Display.get("user"), Display);
	set_newintitle(r, d, webSysDir+"/skin5", Display);

	String newin_titletext = "탐 색";
	String svgObj = "expSvg";
	String plusID = "";
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>ecatalog - explorer001</title>
<link rel="stylesheet" type="text/css" href="<%=webServer%>/main.css">
<style>
	html{width:100%;height:100%;margin:0;}
	body{width:100%;height:100%;margin:0;background-color:#f7f7f7;}
	*{font-family:굴림;font-size:12px;}

<% if(v.equals("m")){ %>
	#maincnt{display:block;position:absolute;width:100%;height:100%;overflow-y:auto;}
<% } else{ %>
	#maincnt{display:block;position:absolute;width:100%;height:100%;overflow-x:hidden;overflow-y:auto;}
<% } %>

	#mainsvg{display:block;position:absolute;width:100%;height:100%;}

	.thumb_g{cursor:pointer;}
	.thumb_g:hover > rect:first-child{fill:#777777;}
	.thumb_g > text{font-family:Tahoma;font-size:8pt;fill:#666666;fill-opacity:1;text-anchor:middle;text-rendering:optimizeLegibility;}

	.thumb_back{fill:#000000;}
	.thumb_back-curr{fill:#ff6600;}

	.pg_g{cursor:pointer;}
	.pg_g > rect{fill:#F5F5F5;stroke:#333333;}
	.pg_g:hover > rect{fill:#FFFFD9;stroke:#aaaaaa;}
	.pg_g:active > rect{fill:#CCCCCC;stroke:#999999;}
	.pg_g > text{font-family:Tahoma;font-size:8pt;fill:#666666;fill-opacity:1;text-anchor:middle;text-rendering:optimizeLegibility;}

	.pg_g-curr > rect{fill:#777777;stroke:#555555;}
	.pg_g-curr > text{font-family:Tahoma;font-size:8pt;fill:#ffffff;fill-opacity:1;text-anchor:middle;text-rendering:optimizeLegibility;}
</style>
<script>
var selfName = "explorer001.jsp", winCnt = "explorer";
var svgWidth = <%=w%>, svgHeight = <%=h%>;
var p;
var ws = "";
var incview = "<%=v%>";
var dragGap = 0;
var pc, thumbWidth, thumbHeight;
var thumbGrpObj, pagingGrpObj;
var arPagingObj = new Array();

var firstLastBtn = true;
var spacingPP = 5;				// between paging and paging
var spacingPS = 5;				// between prev/first button and paging
var pgitemWidth = 19;			// paging item width
var bef_articlesOnPage = 0;
var bef_currPage = 1;

function onload_func(){
	p = window.parent;
	console.log(selfName+" : loaded");
	p.expSvg = this;
	window.parent.objLoaded2('explorer', this, false);
	dragGap = p.WinInfo.get_dragGap('<%=n%>');
	p.document.getElementById('newin_frame').innerHTML = ws;

	thumbGrpObj = document.getElementById('thumbGrp');
	pagingGrpObj = document.getElementById('pagingGrp');

	thumbWidth = p.newinTmp.thumbWidth;
	thumbHeight = p.newinTmp.thumbHeight

	pc = new p.PagingCont();
	onload_func2();
}

function refresh_size(aw, ah){			// only mobile by orientation change
	svgWidth = aw;
	svgHeight = ah;
	newin_size();

	pc.set_articlesOnPage(p.newinTmp.colQty*p.newinTmp.rowQty);
	pc.set_pagesOnSheet(10);
	
	if(bef_currPage > pc.totalPages) bef_currPage = pc.totalPages;
	pc.set_currentPage(bef_currPage);

	if(pc.articlesOnPage >= pc.totalArticles) p.MenuInfo.applyMax = "N";

	var nodes = thumbGrpObj.childNodes;
	for(var i = 0;i < nodes.length;i++){
		thumbGrpObj.removeChild(nodes[i]);
	}

	thumbGrpObj.setAttribute("transform",'translate('+p.newinTmp.winpadL+','+p.newinTmp.winpadT+')');
	var tx, ty, pno, k = 0;
	for(var i = pc.startArticle;i <= pc.finalArticle;i++){
		pno = i + 1;
		tx = (thumbWidth+p.newinTmp.spacingII)*(k%p.newinTmp.colQty);
		ty = (thumbHeight+p.newinTmp.spacingII)*Math.floor(k/p.newinTmp.colQty);
		if(document.getElementById("thumb_g"+pno)){
			document.getElementById("thumb_g"+pno).setAttribute('transform','translate('+(tx-1)+','+(ty-1)+')');
		}
		else{
			make_thumbItem(pno, tx, ty);
		}
		k++;
	}

	if(pc.articlesOnPage < pc.totalArticles) refresh_paging(true);
	else refresh_paging(false);
}

function onload_func2(){
	pc.totalArticles = p.PageInfo.cataPages;
	pc.set_articlesOnPage(p.newinTmp.colQty*p.newinTmp.rowQty);
	pc.set_pagesOnSheet(10);
	pc.set_currentPage(1);
	bef_currPage = 1;

	if(pc.articlesOnPage >= pc.totalArticles) p.MenuInfo.applyMax = "N";

	// 모바일의 total에서는 maincnt의 스크롤을 이용한다. body를 이용하면 타이틀 영역까지 같이 움직인다.
	if(incview === "m"){
		document.getElementById('mainsvg').style.height = (svgHeight - p.NewinObj.titleHeight) + "px";
		thumbGrpObj.setAttribute("transform",'translate('+p.newinTmp.winpadL+','+p.newinTmp.winpadT+')');
	}
	if(incview === "m"){
		make_thumbs5m();
		if(pc.articlesOnPage < pc.totalArticles) make_paging();			// calculate the height
	}
	else if(p.MenuInfo.currShowMenu === "explorer"){
		make_thumbs5dexp();
		if(pc.articlesOnPage < pc.totalArticles) make_paging();			// calculate the height
	}
	else{
		make_thumbs5dtot();
	}
}
function make_thumbs5dexp(){				// explorer
	var tx, k = 0;
	for(var i = pc.startArticle;i <= pc.finalArticle;i++){
		tx = (thumbWidth+p.newinTmp.spacingII)*k;
		make_thumbItem(i+1, tx, 0);
		k++;
	}
}
function make_thumbs5dtot(){				// total
	var tx, ty;
	for(var i = 0;i < pc.totalArticles;i++){
		tx = (thumbWidth+p.newinTmp.spacingII)*(i%p.newinTmp.colQty);
		ty = (thumbHeight+p.newinTmp.spacingII)*Math.floor(i/p.newinTmp.colQty);
		make_thumbItem(i+1, tx, ty);
	}
}
function make_thumbs5m(){					// explorer(mobile)
	var tx, ty, k = 0;
	for(var i = pc.startArticle;i <= pc.finalArticle;i++){
		tx = (thumbWidth+p.newinTmp.spacingII)*(k%p.newinTmp.colQty);
		ty = (thumbHeight+p.newinTmp.spacingII)*Math.floor(k/p.newinTmp.colQty);
		make_thumbItem(i+1, tx, ty);
		k++;
	}
}
function make_thumbItem(pno, tx, ty){
	var gObj = document.createElementNS("http://www.w3.org/2000/svg","g");
	gObj.setAttribute("id", "thumb_g"+pno);
	gObj.setAttribute("class", "thumb_g");
	gObj.setAttribute('transform','translate('+(tx-1)+','+(ty-1)+')');
	gObj.setAttribute('onclick',"do_itemClick("+pno+")");

	// a rectangle for shadow effect
	var rectObj = document.createElementNS("http://www.w3.org/2000/svg","rect");
	rectObj.setAttribute("fill", "#BBBBBB");
	rectObj.setAttribute("x", 2);
	rectObj.setAttribute("y", 2);
	rectObj.setAttribute("width", thumbWidth+2);
	rectObj.setAttribute("height", thumbHeight+2);
	gObj.appendChild(rectObj);

	// a rectangle for black outline
	rectObj = document.createElementNS("http://www.w3.org/2000/svg","rect");
	if(p.ScreenInfo.onesmc === true){
		if(p.PageInfo.currentPage === pno) rectObj.setAttribute("class", "thumb_back-curr");
		else rectObj.setAttribute("class", "thumb_back");
	}
	else{
		if(p.PageInfo.currentPage === pno) rectObj.setAttribute("class", "thumb_back-curr");
		else if(p.PageInfo.currentPage+1 === pno) rectObj.setAttribute("class", "thumb_back-curr");
		else rectObj.setAttribute("class", "thumb_back");
	}
	rectObj.setAttribute("x", 0);
	rectObj.setAttribute("y", 0);
	rectObj.setAttribute("width", thumbWidth+2);
	rectObj.setAttribute("height", thumbHeight+2);
	gObj.appendChild(rectObj);

	var aaa = p.FileInfo.thumbFilePath(pno,"s","","*");

	var imageObj = document.createElementNS("http://www.w3.org/2000/svg","image");
	if(p.ServerInfo.applyCipher === true) p.load_cataImageCipher2(aaa[0], pno, -1, imageObj);
	else imageObj.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", aaa[0]);
	imageObj.setAttribute("x", 1);
	imageObj.setAttribute("y", 1);
	imageObj.setAttribute("width", thumbWidth);
	imageObj.setAttribute("height", thumbHeight);
	gObj.appendChild(imageObj);

	// circle for page number
	var circleObj = document.createElementNS("http://www.w3.org/2000/svg","circle");
	circleObj.setAttribute("fill", "#FFFFFF");
	circleObj.setAttribute("fill-opacity", "0.6");
	circleObj.setAttribute("cx", 10);
	circleObj.setAttribute("cy", thumbHeight-10);
	circleObj.setAttribute("r", 9);
	gObj.appendChild(circleObj);

	var textObj = document.createElementNS("http://www.w3.org/2000/svg","text");
	textObj.setAttribute("class", "btntext");
	textObj.setAttribute("x", 10);
	textObj.setAttribute("y", thumbHeight-5);
	textNode = document.createTextNode(pno);
	textObj.appendChild(textNode);
	gObj.appendChild(textObj);
	if(p.PageInfo.is_previewPage(pno) === false) gObj.setAttribute("filter","url(#approveMatrix)");

	thumbGrpObj.appendChild(gObj);
}
function refresh_thumbs(n){
	if(n == pc.currentPage) return;

	var nodes = thumbGrpObj.childNodes;
	for(var i = nodes.length-1;i > 0;i--){
		thumbGrpObj.removeChild(nodes[i]);
	}

	if(n === 0) return;

	pc.set_currentPage(n);
	bef_currPage = n;

	if(incview === "m"){
		make_thumbs5m();
	}
	else if(p.MenuInfo.currShowMenu === "explorer"){
		make_thumbs5dexp();
	}
	else{
		make_thumbs5dtot();
	}
}
function do_itemClick(pno){
	if(p.PermitMan.get_directGoPermNumber("explorer", pno, "system") === false) return;			// device, spage, pageshow
	p.go_general(pno, "system");
	if(incview === "m") newin_close();
}

/// ############################## paging ############################## ///
function make_paging(){
	var gObj;
	var accum = 0;
	if(pc.prevSheet !== -1){
		if(firstLastBtn === true){
			gObj = make_pagingitem(accum, "<<", "pg_g");
			gObj.setAttribute("onclick", "do_firstSheetClick()");
			accum += pgitemWidth + spacingPS;
		}

		gObj = make_pagingitem(accum, "<", "pg_g");
		gObj.setAttribute("onclick", "do_prevSheetClick()");
		accum += pgitemWidth + spacingPS;
	}

	for(var i = pc.startPage;i <= pc.finalPage;i++){
		gObj = (i === pc.currentPage) ? make_pagingitem(accum, i, "pg_g-curr") : make_pagingitem(accum, i, "pg_g");
		gObj.setAttribute("onclick", "do_pageNoClick("+i+")");
		arPagingObj.push(gObj);
		accum += pgitemWidth + spacingPP;
	}

	accum += (spacingPS - spacingPP);

	if(pc.nextSheet !== -1){
		gObj = make_pagingitem(accum, ">", "pg_g");
		gObj.setAttribute("onclick", "do_nextSheetClick()");
		accum += pgitemWidth + spacingPP;

		if(firstLastBtn === true){
			gObj = make_pagingitem(accum, ">>", "pg_g");
			gObj.setAttribute("onclick", "do_nextSheetClick()");
			accum += pgitemWidth + spacingPP;
		}
	}
	//alert(svgHeight+":"+p.newinTmp.pgY);
	pagingGrpObj.setAttribute('transform','translate('+(svgWidth-accum)/2+','+p.newinTmp.pgY+')');
}
function make_pagingitem(tx, stitle, clname){
	var gObj = document.createElementNS("http://www.w3.org/2000/svg","g");
	gObj.setAttribute("class", clname);
	gObj.setAttribute('transform','translate('+tx+','+0+')');

	var rectObj = document.createElementNS("http://www.w3.org/2000/svg","rect");
	rectObj.setAttribute("x", 0.5);
	rectObj.setAttribute("y", 0.5);
	rectObj.setAttribute("width", 19);
	rectObj.setAttribute("height", 14);
	gObj.appendChild(rectObj);

	var textObj = document.createElementNS("http://www.w3.org/2000/svg","text");
	textObj.setAttribute("x", 10);
	textObj.setAttribute("y", 11);
	textNode = document.createTextNode(stitle);
	textObj.appendChild(textNode);
	gObj.appendChild(textObj);

	if(stitle == ">" || stitle == "<"){
		textObj.setAttribute("textLength", 5);
		textObj.setAttribute("lengthAdjust", "spacingAndGlyphs");
	}
	else if(stitle == ">>" || stitle == "<<"){
		textObj.setAttribute("textLength", 10);
		textObj.setAttribute("lengthAdjust", "spacingAndGlyphs");
	}

	pagingGrpObj.appendChild(gObj);
	return gObj;
}
function set_currPaging(){
	var k = 0;
	for(var i = pc.startPage;i <= pc.finalPage;i++){
		if(i === pc.currentPage) arPagingObj[k].setAttribute("class", "pg_g-curr");
		else arPagingObj[k].setAttribute("class", "pg_g");
		k++;
	}
}
function refresh_paging(bool){
	var nodes = pagingGrpObj.childNodes;
	for(var i = nodes.length-1;i >= 0;i--){
		pagingGrpObj.removeChild(nodes[i]);
	}
	if(bool === true) make_paging();
}

/// ############################## paging event ############################## ///
function do_pageNoClick(n){
	if(get_perm() === false) return;
	refresh_thumbs(n);
	set_currPaging();
}
function do_firstSheetClick(){
	if(get_perm() === false) return;
	refresh_thumbs(1);
	refresh_paging(true);
}
function do_prevSheetClick(){
	if(get_perm() === false) return;
	refresh_thumbs(pc.prevSheet);
	refresh_paging(true);
}
function do_nextSheetClick(){
	if(get_perm() === false) return;
	refresh_thumbs(pc.nextSheet);
	refresh_paging(true);
}
function do_lastSheetClick(){
	if(get_perm() === false) return;
	refresh_thumbs(pc.totalPages);
	refresh_paging(true);
}
function get_perm(){
	if(p.Action.aniLocking === true || p.Action.drawCondition < -1) return false;
	return true;
}

/// ############################## window ############################## ///
function newin_close(){
	p.unload_newin(winCnt);
	p.expSvg = undefined;
}
function do_mouseDrag(e){
	e.preventDefault();

	var posX = e.clientX - p.clickX + p.befX;
	var posY = e.clientY - p.clickY + p.befY;

	if(posX < p.cataRect.x) posX = p.cataRect.x;
	else if(posX > p.cataRect.right-svgWidth-dragGap) posX = p.cataRect.right - svgWidth - dragGap;

	if(posY < p.cataRect.y) posY = p.cataRect.y;
	else if(posY > p.cataRect.bottom-svgHeight-dragGap) posY = p.cataRect.bottom - svgHeight - dragGap;

	p.newinDiv.style.left = posX+"px";
	p.newinDiv.style.top = posY+"px";

	return true;
}
function do_mouseUp(e){
	p.befX += e.clientX - p.clickX;
	p.befY += e.clientY - p.clickY;
}
function newin_fullscr(){
	if(p.MenuInfo.currShowMenu === "explorer"){
		swap_fullscrimage("restore");
		p.MenuInfo.currShowMenu = "total";
		p.calc_totalwindow();

		refresh_thumbs(0);
		onload_func2();
		refresh_paging(false);
	}
	else{
		swap_fullscrimage("fullscr");
		p.MenuInfo.currShowMenu = "explorer";
		p.calc_expwindow();

		refresh_thumbs(0);
		onload_func2();
		refresh_paging(true);
	}
}
</script>
</head>

<body onload="onload_func()">

<%
	int fullscrbtn = b.equals("Y") ? 1 : 0;
	int closebtn = 1;
	String skind = "";
%>
<%@ include file="inc_newin.jsp" %>

<div id="maincnt">
<svg version="1.1" id="mainsvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<filter id="approveMatrix">
<feColorMatrix in="SourceGraphic" type="matrix" values="0.1 0 0 0 0  0 0.1 0 0 0  0 0 0.1 0 0  0 0 0 1 0" />
</filter>
<g id="thumbGrp" transform="translate(8,8)"></g>
<g id="pagingGrp" transform="translate(8,8)"></g>
</svg>
</div>

</body>
</html>
