<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%
	String tw = get_param1(request.getParameter("w"),5);
	int w = tw.equals("") ? 0 : Integer.parseInt(tw);
	String th = get_param1(request.getParameter("h"),5);
	int h = th.equals("") ? 0 : Integer.parseInt(th);
	String ds = request.getParameter("d");
	if(ds == null) ds = "";

	String[] ar = mysplit(ds,".");
	String n = "";
	if(ar.length > 1) n = get_param2(ar[1],5);			// SkinInfo.nwclass(n)
	String v = "";
	if(ar.length > 2) v = check_um(ar[2],"um");			// CataInfo.incview(v)
	String d = check_mainmenu(ar[0], v);				// SkinInfo.mainmenu(d)
	String r = "index";

	pathOfOriCata = webSysDir + "/catImage";
	HashMap Display = new HashMap();
	Display.put("user", get_userSkin(d, v));
	if(!Display.get("user").equals("")) set_userSkin((String)Display.get("user"), Display);
	set_newintitle(r, d, webSysDir+"/skin5", Display);

	String newin_titletext = "목 차";
	String svgObj = "indexSvg";
	String plusID = "Index";
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>ecatalog - index001</title>
<link rel="stylesheet" type="text/css" href="<%=webServer%>/main.css">
<style>
	html, body{width:100%;height:100%;margin:0;overflow:hidden;}
	#sclass{z-index:2;display:block;position:absolute;width:100%;height:100%;overflow:auto;}
<% if(v.equals("d")){ %>
	body{background-color:#ffffff;}
	*{font-family:굴림;font-size:12px;}
<% } else{ %>
	body{background-color:#ededed;}
	*{font-size:13px;color:#444444;}
	a{text-decoration:none;color:#444444;}
<% } %>
</style>
<script src="<%=webServer%>/skin5/explorer.js"></script>
<script>
var selfName = "index001.jsp", winCnt = "index";
var svgWidth = <%=w%>, svgHeight = <%=h%>;
var p;
var ws = "";

var incview = "<%=v%>";
var dragGap = 0;

function onload_func() {
	p = window.parent;
	console.log(selfName+" : loaded");
	p.indexSvg = this;
	window.parent.objLoaded2('index', this, false);
	dragGap = p.WinInfo.get_dragGap('<%=n%>');
	p.document.getElementById('newin_frame<%=plusID%>').innerHTML = ws;

	skin5Server = "<%=webServer%>/skin5";
	expDiv = document.getElementById('sclass');

	exp_initVar(incview);
	load_mokchaData();
}
function load_mokchaData(){
	var urlstr = p.FileInfo.dirFilePath("mokcha.txt","","*")
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("GET", urlstr);
	if(xmlHttp.overrideMimeType) xmlHttp.overrideMimeType("text/plain; charset=utf-8");
	xmlHttp.onreadystatechange = function(){
		if(xmlHttp.readyState != 4) return;			// 0:unset, 1:opened, 2:headers_received, 3:loading, 4:done
		if(xmlHttp.status == 200){
			var restr = xmlHttp.responseText.replace(/^\s*|\s*$/g, '');				// front and rear's white space
			if(restr.length < 5 || (restr.length > 5 && restr.substring(0,5) == "Error")){
				alert(restr);
				return;
			}

			var lines = restr.split("\n");
			var alen = lines.length;
			var idxs, tstr, t2code, t3code;
			var k = 0;
			for(var i = 0;i < alen;i++){
				tstr = lines[i];
				if(tstr == "" || tstr == "nextline") continue;

				idxs = "" + k;
				if(tstr.substr(0,2) == "--"){		// third category
					//3차 아래는 없으므로
					if(expChild[t3code] == undefined) expChild[t3code] = new Array();
					expChild[t3code].push(idxs);
					expData[idxs] = tstr.substr(2);
				}
				else if(tstr.substr(0,1) == "-"){		// second category
					if(expChild[t2code] == undefined) expChild[t2code] = new Array();
					expChild[t2code].push(idxs);
					expData[idxs] = tstr.substr(1);
					t3code = idxs;
				}
				else{
					if(expChild[exp_rootidx] == undefined) expChild[exp_rootidx] = new Array();
					expChild[exp_rootidx].push(idxs);
					expView[idxs] = false;		// First, it's subcategory is not visible
					expData[idxs] = tstr;
					t2code = idxs;
				}
				k++;
			}
			exp_showOpening(false);
			console.log(selfName+" : "+urlstr+" : "+xmlHttp.readyState);
		}
		else{
			console.log("[Error] "+selfName+" : "+urlstr+" : "+xmlHttp.status);
		}
	}
	xmlHttp.send();
}

function exp_itemClick(pno, obj){
	// device, spage, pageshow, state
	var n = (incview === "m") ? p.PermitMan.get_directGoPerm("index",pno,"","index") : p.PermitMan.get_directGoPerm("index",pno,"","");
	if(n < 0) return;
	p.go_general(n, "user");
	if(incview === "m") newin_close();
}

/// ############################## window ############################## ///
function newin_close(){
	p.unload_index();
}
function refresh_size(ah){			// mobile only
	expDiv.style.height = (ah - 30) + "px";
}
function do_mouseDrag(e){
	e.preventDefault();

	var posX = e.clientX - p.clickX + p.idxbefX;
	var posY = e.clientY - p.clickY + p.idxbefY;

	if(posX < p.cataRect.x) posX = p.cataRect.x;
	else if(posX > p.cataRect.right-svgWidth-dragGap) posX = p.cataRect.right - svgWidth - dragGap;

	if(posY < p.cataRect.y) posY = p.cataRect.y;
	else if(posY > p.cataRect.bottom-svgHeight-dragGap) posY = p.cataRect.bottom - svgHeight - dragGap;

	p.indexDiv.style.left = posX + "px";
	p.indexDiv.style.top = posY + "px";

	return true;
}
function do_mouseUp(e){
	p.idxbefX += e.clientX - p.clickX;
	p.idxbefY += e.clientY - p.clickY;
}
</script>
</head>

<body onload="onload_func();">

<%
	int fullscrbtn = 0;
	int closebtn = 1;
	String skind = "";
%>
<%@ include file="inc_newin.jsp" %>

<div id="sclass"></div>

</body>
</html>
