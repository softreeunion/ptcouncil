<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%@ include file="../inc_skin.jsp" %>
<%
	// 모바일에서만 사용됨
	String tw = get_param1(request.getParameter("w"),5);
	int w = tw.equals("") ? 0 : Integer.parseInt(tw);
	String th = get_param1(request.getParameter("h"),5);
	int h = th.equals("") ? 0 : Integer.parseInt(th);
	String ds = request.getParameter("d");
	if(ds == null) ds = "";

	String[] ar = mysplit(ds,".");
	String n = "";
	if(ar.length > 1) n = get_param2(ar[1],5);			// SkinInfo.nwclass(n)
	String v = "";
	if(ar.length > 2) v = check_um(ar[2],"um");			// CataInfo.incview(v)
	String c = "";
	if(ar.length > 3) c = get_param2(ar[3],5);			// SkinInfo.combokind(c)
	String m = "";
	if(ar.length > 4) m = get_param3(ar[4],1);			// catakind(c)/media(m)
	if(!m.equals("c") && !m.equals("m")) m = "c";
	String d = check_mainmenu(ar[0], v);				// SkinInfo.mainmenu(d)
	String r = "combo";

	pathOfOriCata = webSysDir + "/catImage";
	HashMap Display = new HashMap();
	Display.put("user", get_userSkin(d, v));
	if(!Display.get("user").equals("")) set_userSkin((String)Display.get("user"), Display);
	set_newintitle(r, d, webSysDir+"/skin5", Display);

	String newin_titletext = m.equals("c") ? "이 동" : "목 록";
	String svgObj = "combopopSvg";
	String plusID = "";
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>ecatalog - combo001</title>
<link rel="stylesheet" type="text/css" href="<%=webServer%>/main.css">
<style>
	html{width:100%;height:100%;margin:0;overflow-x:hidden;overflow-y:auto}
	body{width:100%;height:100%;margin:0;overflow-x:hidden;overflow-y:auto;}

	body{background-color:#ededed;}
	*{font-family:굴림;font-size:13px;color:#444444;}
	a{text-decoration:none;color:#444444;}
	#newin_frame{display:block;z-index:1;position:absolute;width:100%;height:<%=h%>px;}
</style>
<script src="<%=webServer%>/skin5/explorer.js"></script>
<script>
var selfName = "combo001.jsp", winCnt = "combo";
var svgWidth = <%=w%>, svgHeight = <%=h%>;
var p;
var ws = "";
var incview = "<%=v%>";

function onload_func() {
	p = window.parent;
	console.log(selfName+" : loaded");
	p.combopopSvg = this;
	window.parent.objLoaded2('combo', this, false);
	p.document.getElementById('newin_frame<%=plusID%>').innerHTML = ws;

	skin5Server = "<%=webServer%>/skin5";
	expDiv = document.getElementById('sclass');

<% if(m.equals("m")){ %>
	expData = p.MediaObj.arMediaFile;
	expView = p.MediaObj.arMediaTitle;
	explist_showOpening();
}
function exp_itemClick(fname, obj){			// media
	p.MediaObj.setSeqfromFile(fname);
	p.MediaObj.playSound();
}
<% } else{ // c %>
<% if(c.equals("001")){ %>
	if(p.DataMan.cataboxData == 4){
		p.ComboObj.set_comboData();
		p.DataMan.cataboxData = 5;
	}

	expCurrCode = p.PageInfo.cataDir;
	expData = p.cataBoxData.arCataDir;
	expView = p.cataBoxData.arCataTitle;
	explist_showOpening();
}
function exp_itemClick(sdir, obj){
	if(sdir == "" || sdir == p.PageInfo.cataDir) return;
	// docu, sdir, cimage, start, cate, callmode, eclang
	p.location = p.ServerInfo.get_cataDoculink("ecatalogm."+p.ServerInfo.progExt,sdir,"","","","","");
}
<% } else{ %>
	exp_initVar(incview);

	if(p.DataMan.cataboxData == 4){
		p.ComboObj.set_expComboData();
		p.DataMan.cataboxData = 5;
	}

	expSplit = 1;
	expCurrCode = p.PageInfo.cateCode;
	expChild = p.cataBoxData.linkCate;
	expData = p.cataBoxData.expData;
	expView = p.cataBoxData.expView;

	exp_showOpening(true);
}
function exp_itemClick(pno, obj){
	var aaa = expData[pno].split("|");
	var sdir = aaa[3];
	if(sdir == "" || sdir == p.PageInfo.cataDir) return;
	// docu, sdir, cimage, start, cate, callmode, eclang
	p.location = p.ServerInfo.get_cataDoculink("ecatalogm."+p.ServerInfo.progExt,sdir,"","","","","");
}
<% } %>
<% } %>

/// ############################## window ############################## ///
function newin_close(){
	p.combopopSvg = undefined;
	p.unload_newin('combo');
}

function newintitle_mousedown(e){
	p.clickX = e.clientX;
	p.clickY = e.clientY;
	downKind = 1;
}
function refresh_size(ah){
	expDiv.style.height = (ah - 30) + "px";
}
</script>
</head>

<body onload="onload_func();">

<%
	int fullscrbtn = 0;
	int closebtn = 1;
	String skind = "close";
%>
<%@ include file="inc_newin.jsp" %>

<div id="sclass" class="sclassE<=n%>"></div>

</body>
</html>
