﻿<%@ page import="java.io.*, java.util.*, java.net.*" contentType="text/html;charset=utf-8" %>
<%@ include file="inc_skin.jsp" %>
<%@ include file="inc_main.jsp" %>
<%!
	private String str_join(String strsum, String s){
		if(strsum.equals("")) strsum = s;
		else strsum = strsum + "|" + s;
		return strsum;
	}
%>
<%
	catimage = get_param1(request.getParameter("catimage"),5);
	Dir = get_param1(request.getParameter("Dir"),5);

	pathOfOriCata = webSysDir + "/catImage";
	pathOfCata = webSysDir + "/catImage" + catimage;
	pathOfDir = webSysDir + "/catImage" + catimage + "/" + Dir;

	File f = new File(pathOfDir + "/ecatalog.txt");						// check category configuration file
	if(!f.exists()){
		out.print("ecatalog.txt does not exists");
		return;
	}

	HashMap Ecatalog = new HashMap();
	set_ecatalog(pathOfDir+"/ecatalog.txt", Ecatalog);

	Vector arDir = new Vector();
	Vector arPage = new Vector();

	arDir.addElement(Dir);
	arPage.addElement((String)Ecatalog.get("pageno"));

	int alen = 0;
	String source = "";
	String outpage = strTrimEach((String)Ecatalog.get("outpage"));
	if(!outpage.equals("")){
		String[] aroutpage = mysplit(outpage, ",");
		alen = aroutpage.length;
		for(int i = 0;i < alen;i++){
			source = strTrimEach(aroutpage[i]);
			if(source.equals("")) continue;

			int in = source.indexOf(":");
			if(in == -1) continue;

			String tpage = source.substring(0, in);
			String tdir = source.substring(in+1);

			arPage.addElement(tpage);
			arDir.addElement(tdir);
		}
	}

	int catapages = 0;
	String apage_str = "";
	String adir_str = "";
	String apageno_str = "";
	String afileseq_str = "";
	String alink_str = "";
	String asound_str = "";
	String apic_str = "";
	String asearch_str = "";

	if(arDir.size() > 0){
		alen = arDir.size();
		for(int i = 0;i < alen;i++){
			String t_dir = (String)arDir.elementAt(i);
			f = new File(pathOfCata + "/" + t_dir + "/ecatalog.txt");
			if(!f.exists()) continue;

			apage_str = str_join(apage_str, (String)arPage.elementAt(i));
			adir_str = str_join(adir_str, t_dir);

			int t_pageno = 0;
			String t_ini1var = "";
			String t_ini2var = "";
			try{
				FileInputStream fin = new FileInputStream(f);
				BufferedReader bin = new BufferedReader(new InputStreamReader(fin, streamCharset));
				while((source = bin.readLine()) != null){
					source = strTrimEach(source);
					if(source.equals("")) continue;
			
					int in = source.indexOf(":");
					if(in == -1) continue;

					String skey = source.substring(0, in);
					String sval = source.substring(in+1);
			
					if(skey.equals("pageno")){
						t_pageno = Integer.parseInt(sval);
					}
					else if(skey.equals("ini1var")){
						t_ini1var = sval;
					}
					else if(skey.equals("ini2var")){
						t_ini2var = sval;
					}
				}
				fin.close();
				bin.close();
			}
			catch(Exception e){
	       	}

			catapages += t_pageno;
			apageno_str = str_join(apageno_str, Integer.toString(t_pageno));

			// fileseq
			String t_fileseq = t_ini1var.substring(11,12);
			if(t_fileseq.equals("F")){
				f = new File(pathOfCata+"/"+t_dir+"/fileseq.txt");
				afileseq_str = f.exists() ? str_join(afileseq_str,"F") : str_join(afileseq_str, "N");
			}
			else if(t_fileseq.equals("X")){
				afileseq_str = str_join(afileseq_str, "X");
			}
			else{
				afileseq_str = str_join(afileseq_str, "N");
			}

			// search, U: url, A: auto, F: file, C:content
			String t_var = t_ini1var.substring(9,10);
			if(t_var.equals("F")){
				f = new File(pathOfCata+"/"+t_dir+"/search.txt");
				asearch_str = f.exists() ? str_join(asearch_str,"F") : str_join(asearch_str, "N");
			}
			else if(t_var.equals("A")){
				f = new File(pathOfCata+"/"+t_dir+"/search.xml");
				asearch_str = f.exists() ? str_join(asearch_str,"A") : str_join(asearch_str, "N");
			}
			else{
				asearch_str = str_join(asearch_str, t_var);			// U/C
			}

			// alink
			f = new File(pathOfCata + "/" + t_dir + "/alink.txt");
			alink_str = f.exists() ? str_join(alink_str,"Y") : str_join(alink_str,"N");

			// pagesound, N:Basis, Y: page sound, P: link
			String t_sound = t_ini2var.substring(0,1);
			f = new File(pathOfCata + "/" + t_dir + "/pagesound.txt");
			t_var = "N";
			if(t_sound.equals("P")) t_var = "P";
			else if(t_sound.equals("Y") && f.exists()) t_var = "Y";
			asound_str = str_join(asound_str, t_var);

			// picdata
			f = new File(pathOfCata + "/" + t_dir + "/picdata.txt");
			t_var = f.exists() ? "Y" : "N";
			apic_str = str_join(apic_str, t_var);
		}
		out.print("outpage\t"+apage_str+"\t"+catapages+"\t"+adir_str+"\t"+apageno_str+"\t"+afileseq_str+"\t"+alink_str+"\t"+asound_str+"\t"+apic_str+"\t"+asearch_str+"\n");
	}

%>
