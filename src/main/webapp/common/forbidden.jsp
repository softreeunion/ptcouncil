<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page info="" isErrorPage="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%	response.setHeader("pragma", "no-cache"); response.setHeader("cache-control", "no-cache"); response.setHeader("expires", "0"); %>
<%	response.setStatus(HttpServletResponse.SC_NOT_FOUND);
	out.clearBuffer(); %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns='http://www.w3.org/1999/xhtml' lang='ko' xml:lang='ko'>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
	<link rel='stylesheet' type='text/css' href='<c:url value='/css/egovframework/sample.css'/>'/>
	<title>Basic Sample</title>
</head>

<body>
<table cellspacing='0' cellpadding='0' style='width:100%;height:100%;border:0;'>
	<tr><td align='center' valign='middle' style='width:100%;height:100%;padding-top:150px;'>
		<table cellspacing='0' cellpadding='0' style='border:0;'>
			<tr><td class='<spring:message code='image.errorBg'/>'><span style='font-family:Tahoma;font-weight:bold;color:#000;line-height:150%;width:440px;height:70px;'>error</span></td></tr>
		</table>
	</td></tr>
</table>
</body>

</html>
<%	out.flush(); %>