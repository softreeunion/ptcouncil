$(document).ready(function(){
	if($("input:text[name='condValue']")>0){$("input:text[name='condValue']").keypress(function(e){if(e.which==13){fnActRetrieve();return false;}});}
	if($("input:text[name='searchText']")>0){$("input:text[name='searchText']").keypress(function(e){if(e.which==13){fnActRetrieve();return false;}});}
	if($("input:text[name='searchMembText']")>0){$("input:text[name='searchMembText']").keypress(function(e){if(e.which==13){fnMembFinder();return false;}});}
	if($("input:text[name='searchTopsText']")>0){$("input:text[name='searchTopsText']").keypress(function(e){if(e.which==13){fnTopsFinder();return false;}});}
	if($("input:password[name='rePass']")>0){$("input:password[name='rePass']").keypress(function(e){if(e.which==13){fnActDetailN();return false;}});}
});
jQuery.fn.center    = function(){this.css("position","absolute").css("top" ,Math.max(0,(($(window).height()-$(this).outerHeight())/2)+$(window).scrollTop()) +"px").css("left",Math.max(0,(($(window).width() -$(this).outerWidth() )/2)+$(window).scrollLeft())+"px");return this;};var doOldLoadShow   = function(){
	var mH = $(document).height();
	var mW = $(window).width();
	$("#mask").css({"width":mW,"height":mH});
	$("#mask").fadeTo("slow",0.8);
	$("#loadLayer").show();
	$("#loadLayer").center();
};
jQuery.ajaxSetup({
	error:function(xhr,exception){
		if(xhr.status==  0){alert("Not connect.\n Verify Network.");}else 
		if(xhr.status==400){alert("Server understood the request, but request content was invalid. [400]");}else 
		if(xhr.status==401){alert("Unauthorized access. [401]");}else 
		if(xhr.status==403){alert("Forbidden resource can not be accessed. [403]");}else 
		if(xhr.status==404){alert("Requested page not found. [404]");}else 
		if(xhr.status==500){alert("Internal server error. [500]");}else 
		if(xhr.status==503){alert("Service unavailable. [503]");}else 
		if(exception=="parsererror"){alert("Requested JSON parse failed. [Failed]");}else 
		if(exception=="timeout"){alert("Time out error. [Timeout]");}else 
		if(exception=="abort"){alert("Ajax request aborted. [Aborted]");}else 
		{alert("Uncaught Error.\n"+xhr.responseText);}
	}
});
var setCookieMobile = function(cname,cvalue,exdays){if(exdays){var now=new Date();now.setDate(now.getDate()+exdays);now.setHours(0);now.setMinutes(0);now.setSeconds(0);var expires=";expires="+now.toGMTString();}else{var expires="";}document.cookie=cname+"="+escape(cvalue)+";"+expires+";";};
var delCookieMobile = function(cname){var now=new Date();now.setDate(now.getDate()-1);var expires=";expires="+now.toGMTString();document.cookie=cname+"="+expires+";";};
var getCookieMobile = function(cname){cname=cname+"=";var start=document.cookie.indexOf(cname);var cvalue="";if(start!=-1){start+=cname.length;var end=document.cookie.indexOf(";",start);if(end==-1){end=document.cookie.length;}cvalue=document.cookie.substring(start,end);}return unescape(cvalue);};
var doOldLoadHide   = function(){$("#mask").css({"width":0,"height":0});$("#mask").hide();$("#loadLayer").hide();};
var fnActScreen     = function(view){$("#popMovie").find("iframe").attr("src",view);};
var fnActScreenN    = function(view){$("#popVideo").show();$("#popVideo").find("iframe").attr("src",view);};
var fnPadding       = function(val,len){val=val+"";return val.length>=len?val:new Array(len-val.length+1).join("0")+val;};
var fnValidUri      = function(val){
	var pattern = new RegExp("^(https?:\\/\\/)?"+ // protocol
							"((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|"+ // domain name
							"((\\d{1,3}\\.){3}\\d{1,3}))"+ // OR ip (v4) address
							"(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*"+ // port and path
							"(\\?[;&a-z\\d%_.~+=-]*)?"+ // query string
							"(\\#[-a-z\\d_]*)?$","i"); // fragment locator
	return pattern.test(val);
};
var fnValidMail     = function(val){
	var pattern = new RegExp("([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$");
	return pattern.test(val);
};
var fnValidPhone    = function(val){
	var pattern = new RegExp("^((01[1|6|7|8|9])[1-9]+[0-9]{6,7})|(010[1-9][0-9]{7})$");
	return pattern.test(val);
};
var fnValidMail2    = function(val){
	var pattern = new RegExp("^(([^<>()[\\]\\.,;:\\s@\"]+(\.[^<>()[\\]\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$");
	return pattern.test(val);
};
var fnTimeColon     = function(obj){
	var tim = fnPadding(obj.val().replace(/[^0-9]/gi,""),6);
	var thh = tim.substring(0,2);
	var tmm = tim.substring(2,4);
	var tss = tim.substring(4,6);
	if(isFinite(thh+tmm+tss)==false){alert("숫자만 입력가능합니다.");return false;}
	if(thh>23){alert("시는 24시을 넘을 수 없습니다.");return false;}
	if(tmm>59){alert("분은 60분을 넘을 수 없습니다.");return false;}
	if(tss>59){alert("초는 60초을 넘을 수 없습니다.");return false;}
	obj.val(thh+":"+tmm+":"+tss);
};
var pCheckLen08     = function(val){return val.length>=8;};
var pCheckLen20     = function(val){return val.length<=20;};
var pCheckLCase     = function(val){return /[a-z]/.test(val);};
var pCheckUCase     = function(val){return /[A-Z]/.test(val);};
var pCheckBNumb     = function(val){return /[0-9]/.test(val);};
var pCheckENumb     = function(val){return /([0-9])\1{2,}/.test(val);};
var pCheckCNumb     = function(val){return /012|123|234|345|456|567|678|789|890/.test(val);};
var pCheckSChar     = function(val){return (/[!#$%&\'\(\)*+,\-./:;\\\<\=>?@\[\]^_`\{|\}~]/gi).test(val);};
var pCheckKChar     = function(val){return (/[가-힣ㄱ-ㅎㅏ-ㅣ]/gi).test(val);};
var pCheckBlank     = function(val){return (/[\s]/gi).test(val);};
var pCombination    = function(val){
	var chkNVal = false;
	var chkCVal = false;
	var chkSVal = false;
	if((/[^\x00-\xff]/g).test(val)){
		return false;
	}else{
		chkSVal = (/[!#$%&\'\(\)*+,\-./:;\\\<\=>?@\[\]^_`\{|\}~]/g).test(val);
		chkCVal = (/[a-zA-Z]/gi).test(val);
		chkNVal = (/[0-9]/g).test(val);
		return chkSVal&&chkCVal&&chkNVal;
	}
};
var fnValidBases    = function(val){
	var $obj = $("#"+val);
	if(!$obj.val()&&$obj.length>0) { 
		var ext = $obj.val().split(".").pop().toLowerCase();
		if($.inArray(ext,["hwp","pdf","doc","docx","ppt","pptx","xls","xlsx","jpg","jpeg","gif","tif","tiff","bmp","png"])==-1) { 
			alert("hwp, pdf, doc, docx, ppt, pptx, xls, xlsx, jpg, jpeg, gif, tif, tiff, bmp, png 파일만 등록가능합니다.");
			$obj.focus();
			return false;
		}
	}
	return true;
};
var fnValidImage    = function(val){
	var $obj = $("#"+val);
	if(!$obj.val()&&$obj.length>0) { 
		var ext = $obj.val().split(".").pop().toLowerCase();
		if($.inArray(ext,["jpg","jpeg","gif","tif","tiff","bmp","png"])==-1) { 
			alert("jpg, jpeg, gif, tif, tiff, bmp, png 파일만 등록가능합니다.");
			$obj.focus();
			return false;
		}
	}
	return true;
};
var fnValidVideo    = function(val){
	var $obj = $("#"+val);
	if(!$obj.val()&&$obj.length>0) { 
		var ext = $obj.val().split(".").pop().toLowerCase();
		if($.inArray(ext,["mp4","webm","ogg"])==-1) { 
			alert("mp4, webm, ogg 파일만 등록가능합니다.");
			$obj.focus();
			return false;
		}
	}
	return true;
};
var fnValidMovie     = function(val){
	var $obj = $("#"+val);
	if(!$obj.val()&&$obj.length>0) { 
		var ext = $obj.val().split(".").pop().toLowerCase();
		if($.inArray(ext,["asf","avi","mpg","mpeg","wmv","mp4"])==-1) { 
			alert("asf, avi, mpg, mpeg, wmv, mp4 파일만 등록가능합니다.");
			$obj.focus();
			return false;
		}
	}
	return true;
};
var getContextPath1 = function(){
	var host = location.href.indexOf(location.host)+location.host.length;
	return location.href.substring(host,location.href.indexOf("/",host+1));
};
var getContextPath2 = function(){
	return window.location.pathname.substring(0,window.location.pathname.indexOf("/",2));
};
var isEmpty         = function(val){
	return val==""||val==null||val==undefined||(val!=null&&typeof val=="object"&&!Object.keys(val).length);
};
var iSecond; //초단위로 환산
var timerchecker    = null;
var fncClearTime    = function(){iSecond=60;};
var fncClearTimer   = function(){$("#timer").empty();};
var fncLPad         = function(str,len){str=str+"";while(str.length<len){str="0"+str;}return str;};
var initTimer       = function(){
	var $obj=$("#timer");
	if(!(typeof($obj)=="undefined")){
		rHour = parseInt(iSecond/3600);
		rHour = rHour%60;
		rMinute = parseInt(iSecond/60);
		rMinute = rMinute%60;
		rSecond = iSecond%60;
		if(iSecond>0){
//			$obj.html("&nbsp;"+fncLPad(rHour,2)+"시간 "+fncLPad(rMinute,2)+"분 "+fncLPad(rSecond,2)+"초 ");
			$obj.html("&nbsp;"+fncLPad(rMinute,2)+"분 "+fncLPad(rSecond,2)+"초 ");
			iSecond--;
			timerchecker=setTimeout("initTimer()",1000);// 1초 간격으로 체크
		}else{
			logoutUser();
		}
	}
};
var refreshTimer    = function(){
	var xhr = initAjax();
//	xhr.open("POST",getContextPath2()+"/view/window_reload.do",false);
	xhr.send();
	fncClearTime();
};
var logoutUser      = function(){
	clearTimeout(timerchecker);
	var xhr = initAjax();
	xhr.open("POST",getContextPath2()+"/coun/iuout_proc.do",false);
	xhr.send();
	alert("최초 본인인증 후 10분이 경과 되어 본인인증이 만료되었습니다.");
	top.location.replace(getContextPath2()+"/");
};
var initAjax        = function(){// 브라우저에 따른 AjaxObject 인스턴스 분기 처리
	var xmlhttp;
	if(window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}else{// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xmlhttp;
};
var shareFacebook   = function(strUrl){window.open("http://www.facebook.com/share.php?u="+strUrl,"_blank","width=400,height=380");};
var shareTwitter    = function(strUrl,title){window.open("https://twitter.com/intent/tweet?url="+strUrl+"&text="+title,"_blank","width=720,height=520");};
var shareBand       = function(strUrl){window.open("http://www.band.us/plugin/share?body="+strUrl+"&route="+strUrl,"_blank","width=420,height=700");};
var shareKakaoStory = function(strUrl){window.open("https://accounts.kakao.com/login/kakaostory?continue=https%3A%2F%2Fstory.kakao.com%2Fshare%3Furl%3D"+strUrl+"&display=popup&referrer","_blank","width=500,height=580");};
var shareTrackback  = function(strUrl){prompt("이 글의 트랙백 주소입니다. Ctrl+C를 눌러 클립보드로 복사하세요.",strUrl);};
var open_viewer     = function(strUrl){
	var strTitle = "회의록보기";
	var width = "950";
	var height = "600";
	window.open(strUrl,"open","width=950,height=600,resizable=yes,scrollbars=yes");
	var viewer_win = window.open(strUrl,"open","width="+width+",height="+height+",resizable=yes,scrollbars=yes");
	viewer_win.focus();
};