﻿$(document).ready(function() {
	//상단- 상임위원회 선택시 
	$(".top_link .inner > ul > li > a").click(function(){
		if( $(this).next().is(".top_link_sub") ){
			if( $(this).hasClass("open") ){
				$(this).removeClass("open").next(".top_link_sub").slideUp("fast");
			} else{
				$(this).addClass("open").next(".top_link_sub").slideDown("fast");
			}
			return false;
		}
	});
	//gnb
	$("#header .gnb_wrap .gnb .inner > ul > li").mouseenter(function(){
		$("#header .gnb_wrap .gnb .inner > ul > li > .gnb_sub").hide();
		$(this).find("> a").addClass("on").next(".gnb_sub").show();
		$("#header .bg_gnb_sub").show();
	});
	$("#header").mouseleave(function(){
		$("#header .gnb_wrap .gnb .inner > ul > li > .gnb_sub").hide();
		$("#header .bg_gnb_sub").hide();
	});
	$("#header .gnb_wrap .gnb .inner > ul > li > a").focus(function(){
		$("#header .gnb_wrap .gnb .inner > ul > li > .gnb_sub").hide();
		$(this).addClass("on").next(".gnb_sub").show();
		$("#header .bg_gnb_sub").show();
	});
	$("#header .gnb_wrap .gnb .inner > ul > li:last-child > .gnb_sub > ul > li:last-child > a").blur(function(){
		$("#header .gnb_wrap .gnb .inner > ul > li > .gnb_sub").hide();
		$("#header .bg_gnb_sub").hide();
	});
	//전체메뉴
	$(".open_fullmenu").click(function(){
		if( $(this).hasClass("on") ){
			$(this).removeClass("on").next(".fullmenu_list").slideUp("fast");
		} else{
			$(this).addClass("on").next(".fullmenu_list").slideDown("fast");
		}
		return false;
	});
	//모바일 fullmenu
	$(".open_mobile_menu").click(function(){
		$(".mobile_menu").slideDown("fast");
		return false;
	});
	$(".close_mobile_menu").click(function(){
		$(".mobile_menu").slideUp("fast");
		return false;
	});
	$(".mobile_menu_list > ul > li > a").click(function(){
		if( $(this).next().is(".mobile_menu_list_sub") ){
			if( $(this).hasClass("on") ){
				$(this).removeClass("on").next(".mobile_menu_list_sub").slideUp("fast");
			} else{
				$(".mobile_menu_list > ul > li > a").removeClass("on");
				$(".mobile_menu_list > ul > li > .mobile_menu_list_sub").slideUp("fast");
				$(this).addClass("on").next(".mobile_menu_list_sub").slideDown("fast");
			}
			return false;
		}
	});
	//접근성메뉴 focus
	$("#skipNavi a").click(function(event){
		var skipTo="#"+this.href.split("#")[1];
		$(skipTo).attr("tabindex", -1).on("blur focusout", function () {
			$(this).removeAttr("tabindex");
		}).focus(); // focus on the content container
	});
	//따라다니는 퀵메뉴
	$(window).scroll(function () { //브라우저에 스크롤이 발생하는 순간부터
		var curpos = $(window).scrollTop()+200; //스크롤바의 상단 위치값+20 보관
		$(".right_quick").stop().animate({"top":curpos}); //스카이메뉴의 상단위치값 애니
	});
	$("#tab_area > ul > li > a").click(function(){
		var Index = $(this).parent().index();
		$("#tab_area > ul > li > a").removeClass("on");
		$(this).addClass("on");
		$("#cont_area > .content_table").hide();
		$("#cont_area > .content_table").eq(Index).show();
		return false;
	});
	$(".go_site > ul > li > a").attr("title","링크 열기");
	$(".go_site > ul > li > a").on("click",function(e){
		e.preventDefault();
		$(".go_site > ul > li > a").attr("title","링크 열기");
		var index=$(".go_site > ul > li > a").index(this);
		var check=$(this).attr("class");
		if(check!="on"){
			$(".go_site > ul> li > div").each(function(){
				 $(this).slideUp("fast");
			});
			$(".go_site > ul > li > a").each(function(){
				 $(this).removeClass("on");
			});
			$(".go_site > ul > li > div").eq(index).slideDown("fast");
			$(".go_site > ul > li > a").eq(index).addClass("on");
			$(".go_site > ul > li").eq(index).addClass("on");
			$(this).attr("title","링크 닫기");
		}else{
			$(".go_site > ul > li > div").eq(index).slideUp("fast");
			$(".go_site > ul > li > a").removeClass("on");
			$(".go_site > ul > li").removeClass("on");
			$(this).attr("title","링크 열기");
		}
	});
	
	$("#coun_drop").focusin(function(){
		alert("11");
	});
	
});
$(window).load(function(){
	//메인비주얼
	$(".main_visual > ul").bxSlider({
		auto : true,
		autoControls: true,
		autoControlsCombine: true,
		pause : 3000
		//autoHover : true
	});
	
	$(".photo_area .photo_viewer > ul").bxSlider({
		auto: true,	// 이미지 회전을 자동
		pagerCustom: '.photo_viewer_pager',
		controls: true, // 이전다음
		autoControls:false, // 시작, 중지
		autoControlsCombine:true,
		pager:true,
		speed: 1000,  // 이미지가 다음 이미지로 바뀌는데 걸리는 시간
		pause: 4500, // 하나의 이미지가 멈춰서 보여지는 시간
	//autoHover : true
	});
	
	//메인 - 탭
	$(".tab_area > ul > li > a").click(function(){
		var Index = $(this).parent().index();
		$(this).parent().parent().find("li > a").removeClass("on");
		$(this).addClass("on");
		$(this).parent().parent().parent().next(".tab_cont").find(".item").hide();
		$(this).parent().parent().parent().next(".tab_cont").find(".item").eq(Index).show();
		return false;
	});
	//메인 - 현역의원
	$(".main_section04 > .list > ul").bxSlider({
		auto: true,
		pager : false,
		controls : true,
		minSlides : 2,
		maxSlides : 8,
		moveSlides : 8,
		slideWidth : 125,
		speed: 1000,
		pause: 5000,
		slideMargin : 28
	});
	
	//메인 - 평택시의회 핫이슈
	$("#container .main_section03 .hotissue .hotissue_list ul").bxSlider({
		pager : false,
		auto : true,
		autoControls: true,
		autoControlsCombine: true
		//autoHover : true
	});
	
	//메인 - 사이트배너
	$(".main_section05 > ul").bxSlider({
		auto : true,
		pager : false,
		controls : false,
		minSlides : 3,
		maxSlides : 6,
		moveSlides : 1,
		slideWidth : 198,
		slideMargin : 2
	});
	/*
	$(".main_section05 > ul").simplyScroll();
	*/
});
//zoom
var nowZoom = 100;
var nowZoom_f = 1;
var zoomcontrol = {
	zoomout : function(){
		nowZoom = nowZoom - 10;
		nowZoom_f /=1.2;
		if(nowZoom <= 70) nowZoom = 70;
		zoomcontrol.zooms();
		return false;
	},
	zoomin : function(){
		nowZoom = nowZoom + 10;
		nowZoom_f *=1.2;
		if(nowZoom >= 500) nowZoom = 500;
		zoomcontrol.zooms();
		return false;
	},
	zoomreset : function(){
		nowZoom = 100; 
		nowZoom_f =1;
		zoomcontrol.zooms();
		return false;
	},
	zooms : function(){
		document.body.style.zoom = nowZoom + "%";
		document.body.style.MozTransform = "scale("+nowZoom_f+")";  // Mozilla(firefox)
		document.body.style.MozTransformOrigin = "0 0";
		document.body.style.OTransform = "scale("+nowZoom_f+")";  // Opera
		document.body.style.OTransformOrigin = "0 0";
		if(nowZoom==70) alert ("30% 축소 되었습니다. 더 이상 축소할 수 없습니다.");
		if(nowZoom==500) alert ("500% 확대 되었습니다. 더 이상 확대할 수 없습니다.");
	}
}