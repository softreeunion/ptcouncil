/**
menu script 
*/

 $(function() {
    // Top Menu 초기 설정
    topMenu();

    $('#topmenu .menu').hide();
    $('#menuArea').css('height', '59px').removeClass("on");

    // Top Menu 활성화 및 드롭다운 애니메이션
    $('#topmenu > li > a').each(function() {
        $(this).on("mouseover focusin", function() {
            $('#topmenu .menu').show().stop(true, false).delay(150);
            var ulHeight = $('#topmenu .menu').outerHeight();
            var gnbHeight = ulHeight + 59;

            $('#menuArea').show().stop(true, false).animate({ height: gnbHeight }, {
                duration: 500,
                easing: "easeOutExpo"
            }).addClass("on");
        });
    });

    // 메뉴 영역을 벗어날 때
    $('#menuArea').mouseleave(function() {
        $('#topmenu .menu').hide();
        $('#menuArea').stop(true, false).animate({ height: '59px' }, {
            duration: 500,
            easing: "easeOutExpo"
        }).removeClass("on");
    });

    // 포커스가 다른 곳으로 이동할 때 메뉴 닫기
    $("a").filter(":not(#topmenu > li a)").focus(function() {
        $('#topmenu .menu').hide();
        $('#menuArea').stop(true, false).animate({ height: '59px' }, {
            duration: 500,
            easing: "easeOutExpo"
        }).removeClass("on");
    });

    // m_menu 초기 상태 설정
    if (!$(".m_menu > ul > li").hasClass("current_on")) {
        $(".m_menu > ul > li:first").addClass("current_on");
    }

    // m_menu 클릭 이벤트
    $(".m_menu > ul > li > a").click(function(e) {
        e.preventDefault();
        $(".m_menu > ul > li").removeClass("current_on");

        var index = $(".m_menu > ul > li > a").index(this);
        var isOn = $(this).hasClass("on");

        if (!isOn) {
            $(".m_menu > ul > li > div > ul").hide();
            $(".m_menu > ul > li > a").removeClass("on");

            $(".m_menu > ul > li > div > ul").eq(index).show();
            $(".m_menu > ul > li > a").eq(index).addClass("on");
        }
    });

    // Sidebar Menu (snb) 클릭 이벤트
    $(".snb > li > a").click(function(e) {
        e.preventDefault();

        var index = $(".snb > li > a").index(this);
        var isOn = $(this).hasClass("on");

        if (!isOn) {
            // 모든 서브메뉴 닫기
            $(".snb > li > ul").slideUp("fast");
            $(".snb > li > a").removeClass("on").removeAttr("title");

            // 선택된 서브메뉴 열기
            $(".snb > li > ul").eq(index).slideDown("fast");
            $(this).addClass("on").attr("title", "현재 선택된 메뉴입니다");
        } else {
            // 이미 활성화된 메뉴 닫기
            $(".snb > li > ul").eq(index).slideUp("fast");
            $(this).removeClass("on").removeAttr("title");
        }
    });

    // submenu 초기 설정
    $('#submenu .depth2 > li.current_on > ul').show();

    // submenu hover 및 focus 이벤트
    $('#submenu .depth2 > li > a').each(function() {
        if ($(this).parent().find("ul").length >= 1) {
            $(this).addClass("hasUl");
        }

        $(this).on("mouseover focusin", function() {
            $('#submenu .depth2 > li > ul').stop().slideUp("fast");
            $('#submenu .depth2 > li.current_on > ul').stop().slideDown("fast");
            $(this).next("ul").stop().slideDown("fast");
        });
    });

    // submenu 영역을 벗어날 때
    $('#submenu').mouseleave(function() {
        $('#submenu .depth2 > li > ul').stop().slideUp("fast");
        $('#submenu .depth2 > li.current_on > ul').stop().slideDown("fast");
    });

    // submenu 외부 포커스 처리
    $("a").filter(":not(#submenu .depth2 > li a)").focus(function() {
        $('#submenu .depth2 > li > ul').stop().slideUp("fast");
        $('#submenu .depth2 > li.current_on > ul').stop().slideDown("fast");
    });
});

// Top Menu 활성화 애니메이션 함수
function topMenu() {
    $("#topmenu > li").on('mouseenter focusin', function() {
        var t = $(this);
        t.addClass("active").siblings().removeClass("active");

        var tabActive = $("#topmenu > li.active").position().left + 30;
        var width = $("#topmenu > li.active").width();
        $("#menuArea .bar").css({
            "opacity": "1",
            "transform": "translateX(" + tabActive + "px)",
            "width": (width - 60) + "px"
        });
    });

    $("#menuArea").on("mouseleave", function() {
        $("#topmenu > li").removeClass('active');
        $("#menuArea .bar").css("opacity", "0");
    });
}
