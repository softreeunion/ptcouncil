// JavaScript Document
$(document).ready(function() {
    // 상단 - 상임위원회 선택시
    $(".top_link > li > a").click(function(event) {
        // 클릭한 a 태그가 이미 열린 상태인지 확인
        if ($(this).next().is(".top_link_sub")) {
            // 이미 열린 경우 -> 메뉴를 닫기
            if ($(this).hasClass("open")) {
                $(this).removeClass("open").next(".top_link_sub").slideUp("fast");
            } else {
                // 다른 열린 메뉴가 있으면 닫기
                $(".top_link > li > a.open").removeClass("open").next(".top_link_sub").slideUp("fast");

                // 클릭한 메뉴 열기
                $(this).addClass("open").next(".top_link_sub").slideDown("fast");
            }
            event.preventDefault(); // 링크 기본 동작 방지
        }
    });
});
//linksite
$(function() {
	
	$(".linksite .inner > li > a").attr("title", "링크열기");
	$(".linksite .inner > li:not(:eq(3)) > a").on("click", function(e){
		e.preventDefault();
		
		$(".linksite .inner > li > a").attr("title", "링크열기");
		var index = $(".linksite .inner > li > a").index(this);
		var check = $(this).parent().attr("class");
		
		if(check != "on"){
			 $(".linksite .inner > li > div").each(function(){
				 $(this).slideUp("300");
			});
			$(".linksite .inner > li > a").each(function(){
				 $(this).parent().removeClass("on");
			});
			
			$(".linksite .inner > li > div").eq(index).slideDown("300");
			$(".linksite .inner > li > a").eq(index).parent().addClass("on");
			$(this).attr("title", "링크열기");
		} else {			
			$(".linksite .inner > li > div").eq(index).slideUp("300");
			$(".linksite .inner > li > a").parent().removeClass("on");
			$(this).attr("title", "링크닫기");
		}
	});
	
	$('.linksite').mouseleave(function() { 
		$('.linksite .inner > li').removeClass("on");
		$('.linksite .inner > li > div').slideUp("300");
		$(".linksite  .inner > ul > li > a").attr("title", "링크닫기");
	}); 
	 
	$("a").filter(":not(.linksite .inner > li a)").focus(function(){
		$('.linksite .inner > li > a').removeClass("on");
		$('.linksite .inner > li > div').slideUp("300");
		$(".linksite .inner > li > a").attr("title", "링크닫기");
	});
});



/* zoom */
let seemSize = 1; // 초기 확대 배율

function zoomIn() {
    if (seemSize >= 1.3) {
        alert("최대 화면 크기입니다.");
        return;
    }
    seemSize = Math.min(seemSize * 1.1, 1.3); // 최대값 제한
    zoom();
}

function zoomReset() {
    seemSize = 1; // 기본 크기로 복원
    zoom();
}

function zoomOut() {
    if (seemSize <= 0.8) {
        alert("최소 화면 크기입니다.");
        return;
    }
    seemSize = Math.max(seemSize / 1.1, 0.8); // 최소값 제한
    zoom();
}

function zoom() {
    document.body.style.transform = `scale(${seemSize})`; // 배율 조정
    document.body.style.transformOrigin = '50% 0'; // 확대 중심
}



 /**
menu script 
*/

 $(function() {
    // Top Menu 초기 설정
    topMenu();

    $('#topmenu .menu').hide();
    $('#menuArea').css('height', '59px').removeClass("on");

    // Top Menu 활성화 및 드롭다운 애니메이션
    $('#topmenu > li > a').each(function() {
        $(this).on("mouseover focusin", function() {
            $('#topmenu .menu').show().stop(true, false).delay(150);
            var ulHeight = $('#topmenu .menu').outerHeight();
            var gnbHeight = ulHeight + 59;

            $('#menuArea').show().stop(true, false).animate({ height: gnbHeight }, {
                duration: 500,
                easing: "easeOutExpo"
            }).addClass("on");
        });
    });

    // 메뉴 영역을 벗어날 때
    $('#menuArea').mouseleave(function() {
        $('#topmenu .menu').hide();
        $('#menuArea').stop(true, false).animate({ height: '59px' }, {
            duration: 500,
            easing: "easeOutExpo"
        }).removeClass("on");
    });

    // 포커스가 다른 곳으로 이동할 때 메뉴 닫기
    $("a").filter(":not(#topmenu > li a)").focus(function() {
        $('#topmenu .menu').hide();
        $('#menuArea').stop(true, false).animate({ height: '59px' }, {
            duration: 500,
            easing: "easeOutExpo"
        }).removeClass("on");
    });

    // m_menu 초기 상태 설정
    if (!$(".m_menu > ul > li").hasClass("current_on")) {
        $(".m_menu > ul > li:first").addClass("current_on");
    }

    // m_menu 클릭 이벤트
    $(".m_menu > ul > li > a").click(function(e) {
        e.preventDefault();
        $(".m_menu > ul > li").removeClass("current_on");

        var index = $(".m_menu > ul > li > a").index(this);
        var isOn = $(this).hasClass("on");

        if (!isOn) {
            $(".m_menu > ul > li > div > ul").hide();
            $(".m_menu > ul > li > a").removeClass("on");

            $(".m_menu > ul > li > div > ul").eq(index).show();
            $(".m_menu > ul > li > a").eq(index).addClass("on");
        }
    });

    // Sidebar Menu (snb) 클릭 이벤트
    $(".snb > li > a").click(function(e) {
        e.preventDefault();

        var index = $(".snb > li > a").index(this);
        var isOn = $(this).hasClass("on");

        if (!isOn) {
            // 모든 서브메뉴 닫기
            $(".snb > li > ul").slideUp("fast");
            $(".snb > li > a").removeClass("on").removeAttr("title");

            // 선택된 서브메뉴 열기
            $(".snb > li > ul").eq(index).slideDown("fast");
            $(this).addClass("on").attr("title", "현재 선택된 메뉴입니다");
        } else {
            // 이미 활성화된 메뉴 닫기
            $(".snb > li > ul").eq(index).slideUp("fast");
            $(this).removeClass("on").removeAttr("title");
        }
    });

    // submenuArea 초기 설정
    $('#submenuArea .depth2 > li.current_on > ul').show();

    // submenuArea hover 및 focus 이벤트
    $('#submenuArea .depth2 > li > a').each(function() {
        if ($(this).parent().find("ul").length >= 1) {
            $(this).addClass("hasUl");
        }

        $(this).on("mouseover focusin", function() {
            $('#submenuArea .depth2 > li > ul').stop().slideUp("fast");
            $('#submenuArea .depth2 > li.current_on > ul').stop().slideDown("fast");
            $(this).next("ul").stop().slideDown("fast");
        });
    });

    // submenuArea 영역을 벗어날 때
    $('#submenuArea').mouseleave(function() {
        $('#submenuArea .depth2 > li > ul').stop().slideUp("fast");
        $('#submenuArea .depth2 > li.current_on > ul').stop().slideDown("fast");
    });

    // submenuArea 외부 포커스 처리
    $("a").filter(":not(#submenuArea .depth2 > li a)").focus(function() {
        $('#submenuArea .depth2 > li > ul').stop().slideUp("fast");
        $('#submenuArea .depth2 > li.current_on > ul').stop().slideDown("fast");
    });
});



// Top Menu 활성화 애니메이션 함수
function topMenu() {
    $("#topmenu > li").on('mouseenter focusin', function() {
        var t = $(this);
        t.addClass("active").siblings().removeClass("active");

        var tabActive = $("#topmenu > li.active").position().left + 30;
        var width = $("#topmenu > li.active").width();
        $("#menuArea .bar").css({
            "opacity": "1",
            "transform": "translateX(" + tabActive + "px)",
            "width": (width - 60) + "px"
        });
    });

    $("#menuArea").on("mouseleave", function() {
        $("#topmenu > li").removeClass('active');
        $("#menuArea .bar").css("opacity", "0");
    });
}



//mobile menu

$(function () {
    // 상단으로 스크롤
    $(".btn_top").click(function (e) {
        e.preventDefault();
        $("html, body").stop().animate({ scrollTop: 0 }, 500);
    });

    // 메뉴 닫기
    $(".btn_menu_close, .mask").click(function () {
        $(".m_menu").removeClass("on");
        $("body").removeClass("on");
        return false;
    });
	
	// search
	$(document).ready(function () {
    // 화면 크기에 따라 높이 계산
    function calculateHeight() {
        const screenHeight = $(document).height();
        if ($(window).width() > 768) {
            return screenHeight - 105 + "px";
        } else if ($(window).width() > 380) {
            return screenHeight - 85 + "px";
        }
        return "0px";
    }

    // 검색 열기/닫기 토글
    function toggleSearch(button) {
        const parent = button.parent();
        const isActive = parent.hasClass("on");

        if (isActive) {
            button.text("검색 닫기");
            parent.removeClass("on").stop().animate({ height: "0px" });
        } else {
            button.text("검색 열기");
            parent.addClass("on").css({ height: calculateHeight() });
        }
    }

    // 게시판 검색 열기/닫기 토글
    function toggleBoardSearch(button) {
        const boardSearch = $(".board_sch");
        const isActive = boardSearch.hasClass("on");

        boardSearch.toggleClass("on");
        button.text(isActive ? "검색 열기" : "검색 닫기");
    }

    // 검색 버튼 클릭 이벤트
    $("#search").on("click", ".btn_search", function (e) {
        e.preventDefault();
        toggleSearch($(this));
    });

    // 게시판 검색 버튼 클릭 이벤트
    $(".board_sch").on("click", ".btn_sch", function (e) {
        e.preventDefault();
        toggleBoardSearch($(this));
    });
});

	

   
      // 모바일 메뉴 관리
   function mMenu() {
    const w = $(window).width();
    const x = 1024; // Breakpoint
    const mHeight = $(document).height();

    // 메뉴 버튼 클릭 이벤트
    $(".btn_menu").off("click").on("click", function (e) {
        if (w <= x) { // 모바일 뷰
            e.preventDefault(); // 링크 기본 동작 방지
            $(".mask").addClass("on");
            $(".m_menu").css({ height: `${mHeight}px` }).addClass("on");
        } else { // 데스크탑 뷰
            $(".mask").removeClass("on");
            // 기본 링크로 이동
            window.location.href = "/coun/base/sitemap.do";
        }
    });

    // 데스크탑 뷰일 경우, 마스크 초기화
    if (w > x) {
        $(".mask").removeClass("on");
    }
}

// 초기화 및 리사이즈 이벤트 핸들링
$(document).ready(function () {
    mMenu(); // 처음 로드 시 실행
});

$(window).on("resize", function () {
    mMenu(); // 화면 크기 변경 시 실행
});
});
