
// ptcuncil_img 슬라이드
    const swiper = new Swiper('#ptcuncil_img', {
      loop: true,  // 무한 반복
      autoplay: {
        delay: 3000,  // 3초마다 자동으로 이동
        disableOnInteraction: false,  // 슬라이드가 인터랙션 후에도 자동 재시작
      },
      effect: 'fade',  // 페이드 효과
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });

    // 정지 버튼 기능
    document.getElementById('stopButton').addEventListener('click', () => {
      swiper.autoplay.stop();  // 자동 슬라이딩 멈추기
      document.getElementById('stopButton').style.display = 'none';  // 정지 버튼 숨기기
      document.getElementById('resumeButton').style.display = 'inline-block';  // 재개 버튼 보이기
    });

    // 재개 버튼 기능
    document.getElementById('resumeButton').addEventListener('click', () => {
      swiper.autoplay.start();  // 자동 슬라이딩 재시작
      document.getElementById('resumeButton').style.display = 'none';  // 재개 버튼 숨기기
      document.getElementById('stopButton').style.display = 'inline-block';  // 정지 버튼 보이기
    });

    // 이전 버튼 기능
    document.getElementById('prevButton').addEventListener('click', () => {
      swiper.slidePrev();  // 이전 슬라이드로 이동
    });

    // 다음 버튼 기능
    document.getElementById('nextButton').addEventListener('click', () => {
      swiper.slideNext();  // 다음 슬라이드로 이동
    });

//board
$(function() {
    var $tab_board=$("#boardArea h5 a");
	$("#boardArea > ul > li:first").addClass("on");
	$("#boardArea > ul").addClass("all");

	$tab_board.on("click focus", function(e){
		e.preventDefault();
		$("#boardArea > ul > li").removeClass("on");
		$(this).parent().parent("li").addClass("on");
	});  
	
});

 //member

$(function () {
    // 멤버 섹션 슬라이더 초기화
    const $bigList = $("#member .big_wrap .big_list");
    const $stopButton = $("#member .big_wrap .control .btn_stop");
    const $smallList = $("#member .small_list .mem_s");

    // 큰 슬라이더 설정
    $bigList.slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        autoplay: true,
        nextArrow: "#member .big_wrap .control .btn_next",
        prevArrow: "#member .big_wrap .control .btn_prev",
    });

    // 슬라이더 재생/중지 버튼
    $stopButton.click(function () {
        const isPaused = $(this).hasClass("on");
        $(this).toggleClass("on");
        $bigList.slick(isPaused ? "slickPlay" : "slickPause");
        return false;
    });

    // 작은 썸네일 클릭 이벤트
    $smallList.click(function (e) {
        e.preventDefault();

        // 슬라이더 이동
        const slideIndex = $(this).index();
        $bigList.slick("slickGoTo", slideIndex);

        // 썸네일 활성화 상태 업데이트
        $smallList.removeClass("active");
        $(this).addClass("active");
    });

    // 처음 썸네일 활성화
    $smallList.first().addClass("active");

    // 슬라이더 변경 시 썸네일 활성화 처리
    $bigList.on("afterChange", function (event, slick, currentSlide) {
        $smallList.removeClass("active").eq(currentSlide).addClass("active");
    });
});





