/*******************************************************************************
  Program ID  : BaseUtility
  Description : 각종 function들의 공통모듈
  Author      : HexaMedia new0man
  Write Date  : 2019.06.04
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.06.04  최초 생성
  v1.0    HexaMedia new0man      2019.06.04  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.com.cmm.util;

import java.io.*;
import java.text.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.util.regex.*;
import java.math.*;
import java.net.*;

import javax.servlet.http.*;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;

import net.minidev.json.*;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.*;

public class BaseUtility { 

	private static final Logger			log          = LoggerFactory.getLogger(BaseUtility.class);
	private static boolean				retB         = false;
	private static String					retS         = "";
	private static boolean				defB         = false;
	private static String					defS         = "";
	private static int					defI         = 0;
	private static int					retI         = 0;
	private static long					retL         = 0;


	/**
	 * <pre>
	 * 시스템 성격에 따른 물리적인 경로을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @return String
	 */
	public static String getRealBase ( 
		) { 

		return (System.getProperty("os.name").indexOf("Windows") <= -1 ? getKeyProperty( "egovframework.egovProps.globals", "file.Unix" ) : getKeyProperty( "egovframework.egovProps.globals", "file.Winf" ));
	}	// end of getRealBase

	/**
	 * <pre>
	 * 시스템 성격에 따른 물리적인 경로을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @return String
	 */
	public static String getTempBase ( 
		) { 

		return (System.getProperty("os.name").indexOf("Windows") <= -1 ? getKeyProperty( "egovframework.egovProps.globals", "temp.Unix" ) : getKeyProperty( "egovframework.egovProps.globals", "temp.Winf" ));
	}	// end of getTempBase

	/**
	 * <pre>
	 * 시스템 성격에 따른 물리적인 경로을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @return String
	 */
	public static String getFileBase ( 
		) { 

		return getRealBase() +"/"+ getKeyProperty( "egovframework.egovProps.globals", "file.Base" );
	}	// end of getFileBase

	/**
	 * <pre>
	 * Property 파일을 읽어 해당 정보을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  String					// 
	 * @return String
	 */
	@SuppressWarnings({"rawtypes","unchecked"})
	public static String getKeyProperty ( 
			String argValue1, 
			String argValue2 
		) { 

		HashMap acts				= new HashMap();
//		String getS					= "";

		retS		= "";

		if( isValidNull( argValue1 ) || isValidNull( argValue2 ) ) return retS;

		try { 
			ResourceBundle rb	= ResourceBundle.getBundle(argValue1);
			Enumeration rbKeys	= rb.getKeys();

			while( rbKeys.hasMoreElements() ) { 
				String bKey	= (String)rbKeys.nextElement();
				acts.put(bKey, rb.getString(bKey));
			}

			retS = (String)acts.get(argValue2);

			return retS;
		} catch( Exception E ) { 
			return "";
		}
	}	// end of getKeyProperty


	/**
	 * <pre>
	 * 접속 PC의 물리적 주소(IP)를 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  HttpServletRequest		// 
	 * @return String
	 */
	public static String getIPv4Address ( 
			HttpServletRequest req 
		) { 

		defS		= "";
		retS		= "";

		try { 
			for( Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) { 
				NetworkInterface intf = en.nextElement();
				for( Enumeration<InetAddress> enumAddr = intf.getInetAddresses(); enumAddr.hasMoreElements(); ) { 
					InetAddress inetAddr = enumAddr.nextElement();
					if( !inetAddr.isLoopbackAddress() && !inetAddr.isLinkLocalAddress() && inetAddr.isSiteLocalAddress() ) { 
						retS = inetAddr.getHostAddress().toString();
					}
				}
			}
		} catch( SocketException SE ) { 
			return reqIPv4Address( req );
		} catch( Exception E ) { 
			return reqIPv4Address( req );
		}

		return null2Char( retS, reqIPv4Address( req ) );
	}	// end of getIPv4Address

	/**
	 * <pre>
	 * 접속 PC의 물리적 주소(IP)를 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  HttpServletRequest		// 
	 * @return String
	 */
	public static String reqIPv4Address ( 
			HttpServletRequest req 
		) { 

		defS		= "";
		retS		= "";

		try { 
			if( isValidNull( retS ) || "unknown".equalsIgnoreCase(retS) ) retS = req.getHeader("X-FORWARDED-FOR");
			if( isValidNull( retS ) || "unknown".equalsIgnoreCase(retS) ) retS = req.getRemoteAddr();
		} catch( Exception E ) { 
			return defS;
		}

		return retS;
	}	// end of reqIPv4Address

	/**
	 * <pre>
	 * 웹주소의 Parameter 중 특수문자 제거 처리
	 * </pre>
	 * 
	 * @param  String					// 
	 * @return String
	 */
	public static String getUriParam ( 
			String argValue 
		) { 

		defS		= "";
		retS		= "";

		if( isValidNull( argValue ) ) return defS;

		try { 
			retS = argValue;
			retS = retS.replaceAll("\""    , "&quot;");		// &#34;
			retS = retS.replaceAll("\'"    , "&#39;");		// &#39;
			retS = retS.replaceAll("'"     , "&#39;");		// &#39;
			retS = retS.replaceAll("<"     , "&lt;");		// &#60;
			retS = retS.replaceAll(">"     , "&gt;");		// &#62;
			retS = retS.replaceAll("@"     , "&#64;");		// &#64;
		} catch( Exception E ) { 
			return defS;
		}

		return retS;
	}	// end of getUriParam

	/**
	 * <pre>
	 * 웹주소의 Parameter 중 특수문자 제거후 정상주소인지 여부확인
	 * </pre>
	 * 
	 * @param  String					// 
	 * @return Boolean
	 */
	public static boolean getUriCheck ( 
			String argValue 
		) { 

		String regEx		= "^(https?):\\/\\/([^:\\/\\s]+)(:([^\\/]*))?((\\/[^\\s/\\/]+)*)?\\/?([^#\\s\\?]*)(\\?([^#\\s]*))?(#(\\w*))?$";

		defB		= false;
		retB		= false;

		if( isValidNull( argValue ) ) return defB;

		try { 
			retB = ((Pattern)Pattern.compile(regEx)).matcher(getUriParam( argValue )).matches();
		} catch( Exception E ) { 
			return defB;
		}

		return retB;
	}	// end of getUriCheck

	/**
	 * <pre>
	 * Tag를 CData 형태로 변경해주는 메소드( DB -> Web )
	 * </pre>
	 * 
	 * @param  Stirng					// 
	 * @return String
	 */
	public static String tag2Sym ( 
			String argValue 
		) { 

		defS		= "";
		retS		= "";

		if( isValidNull( argValue ) ) return defS;

		try { 
			retS = argValue;

//			retS = retS.replaceAll("&"     , "&amp;");		// &#38;
			retS = retS.replaceAll(" "     , "&nbsp;");		// &nbsp;
			retS = retS.replaceAll("\""    , "&quot;");		// &#34;
//			retS = retS.replaceAll("\'"    , "&#39;");		// &#39;
//			retS = retS.replaceAll("'"     , "&#39;");		// &#39;
//			retS = retS.replaceAll("<"     , "&lt;");		// &#60;
//			retS = retS.replaceAll(">"     , "&gt;");		// &#62;

			retS = retS.replaceAll("\r\r"  , "<br/>");
			retS = retS.replaceAll("\n\n"  , "<br/>");
			retS = retS.replaceAll("\r\n"  , "<br/>");
			retS = retS.replaceAll("\r"    , "<br/>");
			retS = retS.replaceAll("\n"    , "<br/>");
		} catch( Exception E ) { 
			return defS;
		}

		return retS; 
	}	// end of tag2Sym

	/**
	 * <pre>
	 * CData를 Tag 형태로 변경해주는 메소드( Web -> DB )
	 * </pre>
	 * 
	 * @param  Stirng					// 
	 * @return String
	 */
	public static String sym2Tag ( 
			String argValue 
		) { 

		defS		= "";
		retS		= "";

		if( isValidNull( argValue ) ) return defS;

		try { 
			retS = argValue;

			retS = retS.replaceAll("<BR>"  , "\n");
			retS = retS.replaceAll("<br>"  , "\n");
			retS = retS.replaceAll("<BR/>" , "\n");
			retS = retS.replaceAll("<br/>" , "\n");
			retS = retS.replaceAll("<BR />", "\n");
			retS = retS.replaceAll("<br />", "\n");
			retS = retS.replaceAll("</p>"  , "\n");

//			retS = retS.replaceAll("&gt;"  , ">");
//			retS = retS.replaceAll("&lt;"  , "<");
			retS = retS.replaceAll("&quot;", "\"");
			retS = retS.replaceAll("&nbsp;", " ");

//			retS = retS.replaceAll("&#62;" , ">");
//			retS = retS.replaceAll("&#60;" , "<");
			retS = retS.replaceAll("&#39;" , "\'");
			retS = retS.replaceAll("&#34;" , "\"");
			retS = retS.replaceAll("&#38;" , "&");
			retS = retS.replaceAll("&amp;" , "&");
		} catch( Exception E ) { 
			return defS;
		}

		return retS; 
	}	// end of sym2Tag

	/**
	 * <pre>
	 * 모든 태그를 삭제해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 입력 내용
	 * @param  String					// 제거하지 않을 태그지정
	 * @return String
	 */
	public static String removeAllTags ( 
			String argValue 
		) { 

		String regex		= "<(/)?([a-zA-Z]*)(\\s[a-zA-Z]*=[^>]*)?(\\s)*(/)?>";

		defS		= "";
		retS		= "";

		if( isValidNull( argValue ) ) return defS;

		try { 
			retS = argValue.replaceAll(regex, "");
		} catch( Exception E ) { 
			return defS;
		}

		return retS.length() == retS.lastIndexOf("`") + 1 ? retS.substring(0,retS.lastIndexOf("`")) : retS;
	}	// end of removeAllTags

	/**
	 * <pre>
	 * 인자로 받은 문자를 정규식 패턴으로 검사해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// [숫자:num, 영문:engS, 한글:korS, 영숫자:engN, 한숫자:korN, 메일:mail. 모바일:mobi, 전화:tele, 주민번호:ssn, IP:inet, 비밀번호:pass]
	 * @param  String					// 
	 * @return Boolean
	 */
	public static boolean checkParam ( 
			String argType, 
			String argValue 
		) { 

		if( isValidNull( argType ) || isValidNull( argValue ) ) return defB;

		argType = argType.trim().toLowerCase();

//		if( "numc".equalsIgnoreCase(argType) )	argType = "^(\\w)\\1{3,}$";
		if( "numc".equalsIgnoreCase(argType) )	argType = "^([0-9])\\1{3,}$";
		if( "num".equalsIgnoreCase(argType) )	argType = "^[0-9]*$";
		if( "engs".equalsIgnoreCase(argType) )	argType = "^[a-zA-Z]*$";
		if( "kors".equalsIgnoreCase(argType) )	argType = "^[ㄱ-ㅎ가-힣]*$";
		if( "engn".equalsIgnoreCase(argType) )	argType = "^[a-zA-Z0-9]*$";
		if( "korn".equalsIgnoreCase(argType) )	argType = "^[ㄱ-ㅎ가-힣0-9]*$";
		if( "mail".equalsIgnoreCase(argType) )	argType = "^[_a-zA-Z0-9-.]+@[a-zA-Z0-9]+(.[_a-zA-Z0-9-]+)*$";
		if( "mobi".equalsIgnoreCase(argType) )	argType = "^01(?:0|1|[6-9])-(?:\\d{3}|d{4})-\\d{4}$";
		if( "tele".equalsIgnoreCase(argType) )	argType = "^\\d{2,3}-\\d{3,4}-\\d{4}$";
		if( "ssn".equalsIgnoreCase(argType) )	argType = "^\\d{6}-[1-4]\\d{6}";
		if( "inet".equalsIgnoreCase(argType) )	argType = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\$";
		if( "pass".equalsIgnoreCase(argType) )	argType = "[a-zA-Z]{1}[a-zA-Z0-9]{5,20}$";

		return !Pattern.matches(argType, argValue);
	}	// end of checkParam


	/**
	 * <pre>
	 * Map을 JsonString으로 변환해주는 메소드
	 * </pre>
	 *
	 * @param  Map<String, Object>
	 * @return JSONObject
	 */
	public static JSONObject map2JsonString ( 
			Map<String, Object> argObject 
		) { 

		JSONObject jsonObject		= new JSONObject();
		String key 					= new String();
		Object value				= new Object();

		for( Map.Entry<String, Object> entry : argObject.entrySet() ) {
			key = entry.getKey();
			value = entry.getValue();
			jsonObject.put(key, value);
		}

		return jsonObject;
	}	// end of map2JsonString


	/**
	 * <pre>
	 * List<Map>을 JsonArray으로 변환해주는 메소드
	 * </pre>
	 *
	 * @param  List<Map<String, Object>>
	 * @return JSONArray
	 */
	public static JSONArray list2JsonArray ( 
			List<Map<String, Object>> argObject 
		) {

		JSONArray jsonArray			= new JSONArray();

		for( Map<String, Object> map : argObject ) { 
			jsonArray.add(map2JsonString( map ));
		}

		return jsonArray;
	}	// end of list2JsonArray


	/**
	 * <pre>
	 * List<Map>을 JsonString으로 변환해주는 메소드
	 * </pre>
	 *
	 * @param  List<Map<String, Object>>
	 * @return String
	 */
	public static String list2JsonString ( 
			List<Map<String, Object>> argObject 
		) { 

		JSONArray jsonArray = list2JsonArray( argObject );

		return jsonArray.toJSONString();
	}	// end of list2JsonString


	/**
	 * <pre>
	 * JsonObject를 Map<String, String>으로 변환해주는 메소드
	 * </pre>
	 *
	 * @param  JSONObject
	 * @return Map<String, Object>
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> jsonObject2Map ( 
			JSONObject argObject 
		) { 

		Map<String, Object> map		= null;

		try { 
			map = new ObjectMapper().readValue(argObject.toJSONString(), Map.class);
		} catch( JsonParseException JPE ) { 
			return null;
		} catch( JsonMappingException JMP ) { 
			return null;
		} catch( IOException IOE ) { 
			return null;
		}

		return map;
	}	// end of jsonObject2Map


	/**
	 * <pre>
	 * JsonArray를 List<Map<String, String>>으로 변환해주는 메소드
	 * </pre>
	 *
	 * @param  JSONArray
	 * @return List<Map<String, Object>>
	 */
	public static List<Map<String, Object>> jsonArray2ListMap ( 
			JSONArray argObject 
		) { 

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> map		= null;

		if( argObject != null ) { 
			for( int i = 0; i < argObject.size(); i++ ) { 
				map = jsonObject2Map( (JSONObject)argObject.get(i) );
				list.add( map );
			}
		}

		return list;
	}	// end of jsonArray2ListMap


	/**
	 * <pre>
	 * Null을 공백으로 변환시키는 메소드
	 * </pre>
	 * 
	 * @param  Object					// 
	 * @return String
	 */
	public static String null2Blank ( 
			Object argObject 
		) { 

		return isValidNull( argObject ) ? "" : argObject.toString();
	}	// end of null2Blank

	/**
	 * <pre>
	 * Null을 공백으로 변환시키는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @return String
	 */
	public static String null2Blank ( 
			String argValue 
		) { 

		return isValidNull( argValue ) ? "" : argValue;
	}	// end of null2Blank

	/**
	 * <pre>
	 * Null을 0으로 변환시키는 메소드
	 * </pre>
	 * 
	 * @param  Object					// 
	 * @return String
	 */
	public static String null2Zero ( 
			Object argObject 
		) { 

		return isValidNull( argObject ) ? "0" : argObject.toString();
	}	// end of null2Zero

	/**
	 * <pre>
	 * Null을 0으로 변환시키는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @return String
	 */
	public static String null2Zero ( 
			String argValue 
		) { 

		return isValidNull( argValue ) ? "0" : argValue;
	}	// end of null2Zero

	/**
	 * <pre>
	 * Null값이나 공백값이 들어왔을때 이를 '&nbsp'값으로 변환시키는 메소드
	 * </pre>
	 * 
	 * @param  Object					// 
	 * @return String
	 */
	public static String null2Nbsp ( 
			Object argObject 
		) { 

		return isValidNull( argObject ) ? "&nbsp;" : argObject.toString();
	}	// end of null2Nbsp

	/**
	 * <pre>
	 * Null값이나 공백값이 들어왔을때 이를 '&nbsp'값으로 변환시키는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @return String
	 */
	public static String null2Nbsp ( 
			String argString 
		) { 

		return isValidNull( argString ) ? "&nbsp;" : argString;
	}	// end of null2Nbsp

	/**
	 * <pre>
	 * Null을 Char로 변환시키는 메소드
	 * </pre>
	 * 
	 * @param  Object					// 
	 * @param  String					// 공백일경우 변환할 값
	 * @return String
	 */
	public static String null2Char ( 
			Object argObject, 
			String chgChar 
		) { 

		return isValidNull( argObject ) ? chgChar : argObject.toString();
	}	// end of null2Char

	/**
	 * <pre>
	 * Null을 Char로 변환시키는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  String					// 공백일경우 변환할 값
	 * @return String
	 */
	public static String null2Char ( 
			String argValue, 
			String chgChar 
		) { 

		return isValidNull( argValue ) ? chgChar : argValue;
	}	// end of null2Char

	/**
	 * <pre>
	 * Null을 Char로 변환시키는 메소드
	 * </pre>
	 * 
	 * @param  Object					// 
	 * @param  String					// 공백일경우 변환할 값
	 * @param  String					// 공백이 아닐경우 변환할 값
	 * @return String
	 */
	public static String null2Char ( 
			Object argObject, 
			String chgChar, 
			String defChar 
		) { 

		return isValidNull( argObject ) ? chgChar : defChar;
	}	// end of null2Char

	/**
	 * <pre>
	 * Null을 Char로 변환시키는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  String					// 공백일경우 변환할 값
	 * @param  String					// 공백이 아닐경우 변환할 값
	 * @return String
	 */
	public static String null2Char ( 
			String argValue, 
			String chgChar, 
			String defChar 
		) { 

		return isValidNull( argValue ) ? chgChar : defChar;
	}	// end of null2Char

	/**
	 * <pre>
	 * Null 유무를 확인해주는 메소드
	 * </pre>
	 * 
	 * @param  Object					// 
	 * @return Boolean
	 */
	public static boolean isValidNull ( 
			Object argObject 
		) { 

		return argObject == null || argObject.equals(null) || "".equals(argObject.toString().replaceAll("[ ]", ""));
	}	// end of isValidNull

	/**
	 * <pre>
	 * Null 유무를 확인해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @return Boolean
	 */
	public static boolean isValidNull ( 
			String argValue 
		) { 

		return argValue == null || argValue.length() == 0 || "".equalsIgnoreCase(argValue.replaceAll("[ ]", ""));
	}	// end of isValidNull


	/**
	 * <pre>
	 * 일자 유효성를 확인해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 일자
	 * @return Boolean					// [정상:True, 비정상:False]
	 */
	public static boolean isValidDate ( 
			String argValue  
		) { 

		return isValidDate( argValue, "yyyyMMdd" );
	}	// end of isValidDate

	/**
	 * <pre>
	 * 일자 유효성를 확인해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 일자
	 * @param  String					// 형식
	 * @return Boolean					// [정상:True, 비정상:False]
	 */
	public static boolean isValidDate ( 
			String argValue, 
			String argFormat 
		) { 

		SimpleDateFormat df			= null;

		retB		= false;

		if( isValidNull( argValue ) ) return retB;

		argFormat	= null2Char( argFormat, "yyyyMMddHHmmss" );
		df			= new SimpleDateFormat(argFormat, Locale.KOREA);
		df.setLenient(false);	// False로 설정해야 엄밀한 해석을 함.

		try { 
			df.parse(argValue.replaceAll("[^0-9]", ""));
			retB = true;
		} catch( ParseException PE ) { 
			retB = false;
		} catch( IllegalArgumentException IAE ) { 
			retB = false;
		}

		return retB;
	}	// end of isValidDate

	/**
	 * <pre>
	 * 이메일주소 유효성를 확인해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @return Boolean					// [정상:True, 비정상:False]
	 */
	public static boolean isValidMail ( 
			String argValue 
		) { 

		String regex				= "^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$";
		Pattern pattern				= Pattern.compile(regex);
		Matcher matcher				= pattern.matcher(argValue);

		retB		= false;

		if( matcher.matches() ) { 
			retB = true;
		}
		
		return retB;
	}	// end of isValidMail


	/**
	 * <pre>
	 * '0'이 존재하면 제거해주는 메소드
	 * ex) 000001 -> 1
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  Integer					// 
	 * @return String
	 */
	public static String allZero2Null ( 
			String argValue 
		) throws Exception { 

		defS		= "";

		if( isValidNull( argValue ) ) return defS;

		try { 
			return	null2Char( argValue.replaceAll("[^0-9]", ""), argValue, String.valueOf(Integer.parseInt(argValue)) );
		} catch( Exception E ) { 
			return	argValue;
		}
	}	// end of allZero2Null

	/**
	 * <pre>
	 * 0을 Null로 변환시키는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @return String
	 */
	public static String zero2Null ( 
			String argValue 
		) { 

		return isValidNull( argValue ) ? null : ("0".equals(argValue) ? null : argValue);
	}	// end of zero2Null

	/**
	 * <pre>
	 * 0을 Char로 변환시키는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  String					// 0일경우 변환할 값
	 * @return String
	 */
	public static String zero2Char ( 
			String argValue, 
			String chgChar 
		) { 

		return isValidNull( argValue ) ? "" : ("0".equals(argValue) ? chgChar : argValue);
	}	// end of zero2Char

	/**
	 * <pre>
	 * 0을 Char로 변환시키는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  String					// 0일경우 변환할 값
	 * @param  String					// 0가 아닐경우 변환할 값
	 * @return String
	 */
	public static String zero2Char ( 
			String argValue, 
			String chgChar, 
			String defChar 
		) { 

		return isValidNull( argValue ) ? "" : ("0".equals(argValue) ? chgChar : defChar);
	}	// end of zero2Char

	/**
	 * <pre>
	 * 원하는 크기만큼 '0'으로 채워주는 메소드
	 * ex) 1 -> 000001, 1 -> 100000
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  Integer					// 
	 * @return String
	 */
	public static String zeroFill ( 
			int argValue, 
			int argCount 
		) throws Exception { 

		return	zeroFill( String.valueOf(argValue), argCount, "L" );
	}	// end of zeroFill 

	/**
	 * <pre>
	 * 원하는 크기만큼 '0'으로 채워주는 메소드
	 * ex) 1 -> 000001, 1 -> 100000
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  Integer					// 
	 * @return String
	 */
	public static String zeroFill ( 
			String argValue, 
			int argCount 
		) throws Exception { 

		return	zeroFill( argValue, argCount, "L" );
	}	// end of zeroFill 

	/**
	 * <pre>
	 * 원하는 크기만큼 '0'으로 채워주는 메소드
	 * ex) 1 -> 000001, 1 -> 100000
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  Integer					// 표현할 자리수
	 * @param  String					// 표현 형식 [L:왼쪽 표시, R:오른쪽 표시]
	 * @return String
	 */
	public static String zeroFill ( 
			String argValue, 
			int argCount, 
			String	argAlign 
		) throws Exception { 

		try { 
			// 입력값이 자리수가 초과되면
			if( argCount <= argValue.length() ) return argValue;

			return "L".equalsIgnoreCase(argAlign) ? StringUtils.leftPad(argValue, argCount, "0") : StringUtils.rightPad(argValue, argCount, "0");
		} catch( Exception E ) { 
			return argValue;
		}
	}	// end of zeroFill 

	/**
	 * <pre>
	 * 원하는 크기만큼 '0'으로 채워주는 메소드
	 * ex) 1 -> 000001, 1 -> 100000
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  Integer					// 표현할 자리수
	 * @param  String					// 표현 형식 [L:왼쪽 표시, R:오른쪽 표시]
	 * @return String
	 */
	public static String zeroFill ( 
			int argValue, 
			int argCount, 
			String	argAlign 
		) throws Exception { 

		return zeroFill( String.valueOf(argValue), argCount, argAlign );
	}	// end of zeroFill

	/**
	 * <pre>
	 * 원하는 크기만큼 공백으로 채워주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  Integer					// 표현할 자리수
	 * @return String
	 */
	public static String spaceFill ( 
			String argValue, 
			int argCount 
		) throws Exception { 

		return spaceFill( argValue, argCount, "L" );
	}	// end of spaceFill

	/**
	 * <pre>
	 * 원하는 크기만큼 공백으로 채워주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  Integer					// 표현할 자리수
	 * @return String
	 */
	public static String spaceFill ( 
			int argValue, 
			int argCount 
		) throws Exception { 

		return spaceFill( argValue, argCount, "L" );
	}	// end of spaceFill

	/**
	 * <pre>
	 * 원하는 크기만큼 공백으로 채워주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  Integer					// 표현할 자리수
	 * @param  String					// 표현 형식 [L:왼쪽 표시, R:오른쪽 표시]
	 * @return String
	 */
	public static String spaceFill ( 
			String argValue, 
			int argCount, 
			String argAlign 
		) throws Exception { 

		try { 
			// 입력값이 자리수가 초과되면
			if( argCount <= argValue.length() || "0".equals(argValue) ) return argValue;

			return "L".equalsIgnoreCase(argAlign) ? StringUtils.leftPad(argValue, argCount, " ") : StringUtils.rightPad(argValue, argCount, " ");
		} catch( Exception E ) { 
			return argValue;
		}
	}	// end of spaceFill

	/**
	 * <pre>
	 * 원하는 크기만큼 공으로 채워주는 메소드
	 * </pre>
	 * 
	 * @param  Integer					// 
	 * @param  Integer					// 표현할 자리수
	 * @param  String					// 표현 형식 [L:왼쪽 표시, R:오른쪽 표시]
	 * @return String
	 */
	public static String spaceFill ( 
			int argValue, 
			int argCount, 
			String argAlign 
		) throws Exception { 

		return spaceFill( String.valueOf(argValue), argCount, argAlign );
	}	// end of spaceFill


	/**
	 * 
	 * @param  String					// 
	 * @param  String					// 제거할 마지막 문자
	 * @return String
	 */
	public static String lastOfRemove( 
			String argValue, 
			String argLast 
		) throws Exception { 

		retS	= "";

		if( isValidNull( argValue ) ) return retS;

		if( argValue.lastIndexOf(argLast) <= 0 ) 
			retS = argValue;
		else 
			retS = argValue.substring(0,argValue.lastIndexOf(argLast));

		return retS;
	}


	/**
	 * <pre>
	 * 더블 값을 받아서 scale + 1에서 반올림한다.
	 * 다만 소수점 아래에 맨뒷자리의 0들은 사라지므로 주의하여야 한다.
	 * integer 형은 소수점 한자리에 0을 부쳐준다.
	 *
	 * ex1) BaseUtility.round(2.70003, 3) == 2.7
	 * ex2) BaseUtility.round(2.70203, 3) == 2.702
	 * ex3) BaseUtility.round(2.70263, 3) == 2.703
	 * ex4) BaseUtility.round(2, 3) == 2.0
	 * </pre>
	 *
	 * @param  Double					// 
	 * @param  Integer					// 소수점 자리 위치
	 * @return Double
	 */
	public static double round ( 
			double argValue, 
			int argScale 
		) { 

		BigDecimal temp1 = new BigDecimal(argValue);
		BigDecimal temp2 = new BigDecimal(Math.round(temp1.movePointRight(argScale).doubleValue()));

		return temp2.movePointLeft(argScale).doubleValue();
	}	// end of round

	/**
	 * <pre>
	 * 더블 값을 받아서 소수둘째짜리에서 반올림한다.
	 * ex) BaseUtility.round(2.70003) == 2.7
	 * ex) BaseUtility.round(2.75003) == 2.8
	 * </pre>
	 *
	 * @param  Double					// 
	 * @return Double
	 */
	public static double round ( 
			double argValue 
		) { 

		return round( argValue, 1 );
	}	// end of round


	/**
	 * <pre>
	 * 인자로 받은 숫자를 표시 형식으로 바꿔주는 메소드
	 * ex) 1 -> 일, Sun
	 * </pre>
	 * 
	 * @param  Integer					// 
	 * @return String
	 */
	public static String week2Name ( 
			int argValue 
		) { 

		return week2Name( argValue, "" );
	}	// end of week2Name

	/**
	 * <pre>
	 * 인자로 받은 숫자를 표시 형식으로 바꿔주는 메소드
	 * ex) 1 -> 일, Sun
	 * </pre>
	 * 
	 * @param  Integer					// 
	 * @param  String					// 선택 [K:한글, 그외:영문]
	 * @return String
	 */
	public static String week2Name ( 
			int argValue, 
			String argType 
		) { 

		retS		= "";

		if( isValidNull( argValue ) && isValidNull( argType ) ) return retS;

		switch( argValue ) { 
			case 1: { retS = "K".equalsIgnoreCase(argType) ? "일" : "SUN"; break; }
			case 2: { retS = "K".equalsIgnoreCase(argType) ? "월" : "MON"; break; }
			case 3: { retS = "K".equalsIgnoreCase(argType) ? "화" : "TUE"; break; }
			case 4: { retS = "K".equalsIgnoreCase(argType) ? "수" : "WED"; break; }
			case 5: { retS = "K".equalsIgnoreCase(argType) ? "목" : "THU"; break; }
			case 6: { retS = "K".equalsIgnoreCase(argType) ? "금" : "FRI"; break; }
			case 7: { retS = "K".equalsIgnoreCase(argType) ? "토" : "SAT"; break; }
			default : { retS = ""; break; }
		}

		return retS;
	}	// end of week2Name

	/**
	 * <pre>
	 * 해당일자로 요일을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @return String
	 */
	public static String date2Week ( 
			String argValue 
		) { 

		String dateFmt				= "yyyyMMdd";
		Date newDate				= null;
		SimpleDateFormat df			= null;

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if(  isValidNull( argValue ) ) return retS;
		if( !isValidDate( argValue, dateFmt ) ) return retS;

		df			= new SimpleDateFormat(dateFmt, Locale.KOREA);
		df.setLenient(false);	// False로 설정해야 엄밀한 해석을 함.

		try { 
			newDate	= df.parse(argValue.replaceAll("[^0-9]", ""));
			retS	= week2Name( newDate.getDay() + 1, "K" );
		} catch( ParseException PE ) { 
			retS = "";
		} catch( IllegalArgumentException IAE ) { 
			retS = "";
		}

		return retS;
	}	// end of date2Week

	/**
	 * <pre>
	 * 해당일자로 요일을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @return Integer					// 1:일요일 ~ 7:토요일
	 */
	public static int dayOfWeek ( 
			String argValue 
		) { 

		String dateFmt				= "yyyyMMdd";
		Date newDate				= null;
		SimpleDateFormat df			= null;

		retI		= 0;
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if(  isValidNull( argValue ) ) return retI;
		if( !isValidDate( argValue, dateFmt ) ) return retI;

		df			= new SimpleDateFormat(dateFmt, Locale.KOREA);
		df.setLenient(false);	// False로 설정해야 엄밀한 해석을 함.

		try { 
			newDate	= df.parse(argValue.replaceAll("[^0-9]", ""));
			retI	= newDate.getDay() + 1;
		} catch( ParseException PE ) { 
			retI = 0;
		} catch( IllegalArgumentException IAE ) { 
			retI = 0;
		}

		return retI;
	}	// end of date2Week


	/**
	 * <pre>
	 * 인자로 받은 날짜를 표시 형식으로 바꿔주는 메소드(자리수 강제조정)
	 * ex) YYYYMMDD -> YYYY[-MM[-DD][ HH:MM]]]
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  String					// 일자 구분자
	 * @param  String					// 자리수에 따른 결과표현 [14:일자시분초, 12:일자시분, 8:일자, 6:년월, 4:월일, 2:년도]
	 * @return String
	 */
	public static String toDateFormat ( 
			String argValue, 
			String argSepa, 
			int argSize 
		) { 

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");
		argSepa		= null2Char( argSepa, "-" );

		if( isValidNull( argValue ) || "00000000".equals(argValue) ) return retS;

		try { 
			if( argSize == 14 ) 
				retS = argValue.substring(0,4) + argSepa + argValue.substring(4,6) + argSepa + argValue.substring(6,8) +"  "+ argValue.substring(8,10) +":"+ argValue.substring(10,12) +":"+ argValue.substring(12,14);
			else if( argSize == 12 ) 
				retS = argValue.substring(0,4) + argSepa + argValue.substring(4,6) + argSepa + argValue.substring(6,8) +"  "+ argValue.substring(8,10) +":"+ argValue.substring(10,12);
			else if( argSize == 8 ) 
				retS = argValue.substring(0,4) + argSepa + argValue.substring(4,6) + argSepa + argValue.substring(6,8);
			else if( argSize == 6 ) 
				retS = argValue.substring(0,4) + argSepa + argValue.substring(4,6);
			else if( argSize == 4 ) 
				retS = argValue.length() <= 4 ? argValue.substring(0,2) + argSepa + argValue.substring(2,4) : argValue.substring(4,6) + argSepa + argValue.substring(6,8);
			else if( argSize == 2 ) 
				retS = argValue.substring(0,4);
			else
				retS = argValue;
		} catch( Exception E ) { 
			return argValue;
		}

		return retS;
	}	// end of toDateFormat

	/**
	 * <pre>
	 * 인자로 받은 날짜를 표시 형식으로 바꿔주는 메소드
	 * ex) YYYYMMDD -> YYYY[-MM[-DD]]
	 * </pre>
	 * 
	 * @param  String					// 
	 * @return String					// (Format: YYYY[-MM[-DD]])
	 */
	public static String toDateFormat ( 
			String argValue 
		) { 

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if( isValidNull( argValue ) || "00000000".equals(argValue) ) return retS;

		return toDateFormat( argValue, "", argValue.length() );
	}	// end of toDateFormat

	/**
	 * <pre>
	 * 인자로 받은 날짜를 표시 형식으로 바꿔주는 메소드
	 * ex) YYYYMMDD -> YYYY[-MM[-DD]]
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  String					// 일자 구분자
	 * @return String					// (Format: YYYY[-MM[-DD]])
	 */
	public static String toDateFormat ( 
			String argValue, 
			String argSepa 
		) { 

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if( isValidNull( argValue ) || "00000000".equals(argValue) ) return retS;

		return toDateFormat( argValue, argSepa, argValue.length() );
	}	// end of toDateFormat

	/**
	 * <pre>
	 * 인자로 받은 날짜를 표시 형식으로 바꿔주는 메소드(자리수 강제조정)
	 * ex) YYYYMMDD -> YYYY[년 MM[월 DD일]]
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  Integer					// 자리수에 따른 결과표현 [8:일자, 6:년월, 4:월일, 년도]
	 * @param  String					// 타입에 따른 결과표현 [YM:년월, MD:월일, YY:년도]
	 * @return String
	 */
	public static String toDateFormat ( 
			String argValue, 
			int argSize, 
			String argType 
		) { 

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if( isValidNull( argValue ) || "00000000".equals(argValue) ) return retS;

		try { 
			if( argSize == 8 ) 
				retS = argValue.substring(0,4) +"년 " + argValue.substring(4,6) +"월 " + argValue.substring(6,8) +"일";
			else if( argSize == 6 && "YM".equalsIgnoreCase(argType) ) 
				retS = argValue.substring(0,4) +"년 " + argValue.substring(4,6) +"월";
			else if( argSize == 4 && "MD".equalsIgnoreCase(argType) ) 
				retS = argValue.substring(4,6) +"월 " + argValue.substring(6,8) +"일";
			else if( argSize == 4 && "YY".equalsIgnoreCase(argType) )
				retS = argValue.substring(0,4) +"년";
			else
				retS = argValue;
		} catch( Exception E ) { 
			return argValue;
		}

		return retS;
	}	// end of toDateFormat

	/**
	 * <pre>
	 * 인자로 받은 날짜를 표시 형식으로 바꿔주는 메소드(자리수 강제조정) - 월,일의 1~9 사이 값 1자리수로 표현
	 * ex) YYYYMMDD -> YYYY[-MM[-DD][ HH:MM]]]
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  String					// 일자 구분자
	 * @param  String					// 자리수에 따른 결과표현 [14:일자시분초, 12:일자시분, 8:일자, 6:년월, 4:월일, 2:년도]
	 * @return String
	 */
	public static String toDateFormatN ( 
			String argValue, 
			String argSepa, 
			int argSize 
		) { 

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");
		argSepa		= null2Char( argSepa, "-" );

		if( isValidNull( argValue ) || "00000000".equals(argValue) ) return retS;

		try { 
			if( argSize == 14 ) 
				retS = spaceFill( allZero2Null( argValue.substring(0,4) ), 4, "L" ) + argSepa + spaceFill( allZero2Null( argValue.substring(4,6) ), 2, "L" ) + argSepa + spaceFill( allZero2Null( argValue.substring(6,8) ), 2, "L" ) +"  "+ argValue.substring(8,10) +":"+ argValue.substring(10,12) +":"+ argValue.substring(12,14);
			else if( argSize == 12 ) 
				retS = spaceFill( allZero2Null( argValue.substring(0,4) ), 4, "L" ) + argSepa + spaceFill( allZero2Null( argValue.substring(4,6) ), 2, "L" ) + argSepa + spaceFill( allZero2Null( argValue.substring(6,8) ), 2, "L" ) +"  "+ argValue.substring(8,10) +":"+ argValue.substring(10,12);
			else if( argSize == 8 ) 
				retS = spaceFill( allZero2Null( argValue.substring(0,4) ), 4, "L" ) + argSepa + spaceFill( allZero2Null( argValue.substring(4,6) ), 2, "L" ) + argSepa + spaceFill( allZero2Null( argValue.substring(6,8) ), 2, "L" );
			else if( argSize == 6 ) 
				retS = spaceFill( allZero2Null( argValue.substring(0,4) ), 4, "L" ) + argSepa + spaceFill( allZero2Null( argValue.substring(4,6) ), 2, "L" );
			else if( argSize == 4 ) 
				retS = argValue.length() <= 4 ? spaceFill( allZero2Null( argValue.substring(0,2) ), 2, "L" ) + argSepa + spaceFill( allZero2Null( argValue.substring(2,4) ), 2, "L" ) : spaceFill( allZero2Null( argValue.substring(4,6) ), 2, "L" ) + argSepa + spaceFill( allZero2Null( argValue.substring(6,8) ), 2, "L" );
			else if( argSize == 2 ) 
				retS = spaceFill( allZero2Null( argValue.substring(0,4) ), 4, "L" );
			else
				retS = argValue;
		} catch( Exception E ) { 
			return argValue;
		}

		return retS;
	}	// end of toDateFormatN

	/**
	 * <pre>
	 * 인자로 받은 날짜를 표시 형식으로 바꿔주는 메소드
	 * ex) YYYYMMDD -> YYYY[-MM[-DD]]
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  String					// 일자 구분자
	 * @return String					// (Format: YYYY[-MM[-DD]])
	 */
	public static String toDateFormatN ( 
			String argValue, 
			String argSepa 
		) { 

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if( isValidNull( argValue ) || "00000000".equals(argValue) ) return retS;

		return toDateFormatN( argValue, argSepa, argValue.length() );
	}	// end of toDateFormatN

	/**
	 * <pre>
	 * 인자로 받은 날짜를 표시 형식으로 바꿔주는 메소드(자리수 강제조정) - 월,일의 1~9 사이 값 1자리수로 표현
	 * ex) YYYYMMDD -> YYYY[년 MM[월 DD일]]
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  Integer					// 자리수에 따른 결과표현 [8:일자, 6:년월, 4:월일, 년도]
	 * @param  String					// 타입에 따른 결과표현 [YM:년월, MD:월일, YY:년도]
	 * @return String
	 */
	public static String toDateFormatN ( 
			String argValue, 
			int argSize, 
			String argType 
		) { 

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if( isValidNull( argValue ) || "00000000".equals(argValue) ) return retS;

		try { 
			if( argSize == 8 ) 
				retS = spaceFill( allZero2Null( argValue.substring(0,4) ), 4, "L" ) +"년 " + spaceFill( allZero2Null( argValue.substring(4,6) ), 2, "L" ) +"월 " + spaceFill( allZero2Null( argValue.substring(6,8) ), 2, "L" ) +"일";
			else if( argSize == 6 && "YM".equalsIgnoreCase(argType) ) 
				retS = spaceFill( allZero2Null( argValue.substring(0,4) ), 4, "L" ) +"년 " + spaceFill( allZero2Null( argValue.substring(4,6) ), 2, "L" ) +"월";
			else if( argSize == 4 && "MD".equalsIgnoreCase(argType) ) 
				retS = spaceFill( allZero2Null( argValue.substring(4,6) ), 2, "L" ) +"월 " + spaceFill( allZero2Null( argValue.substring(6,8) ), 2, "L" ) +"일";
			else if( argSize == 4 && "YY".equalsIgnoreCase(argType) )
				retS = spaceFill( allZero2Null( argValue.substring(0,4) ), 4, "L" ) +"년";
			else if( argSize == 2 && "MM".equalsIgnoreCase(argType) )
				retS = spaceFill( allZero2Null( argValue.substring(4,6) ), 2, "L" ) +"월";
			else if( argSize == 2 && "DD".equalsIgnoreCase(argType) )
				retS = spaceFill( allZero2Null( argValue.substring(6,8) ), 2, "L" ) +"일";
			else
				retS = argValue;
		} catch( Exception E ) { 
			return argValue;
		}

		return retS;
	}	// end of toDateFormatN

	/**
	 * <pre>
	 * 인자로 받은 시간을 표시 형식으로 바꿔주는 메소드
	 * ex) HHMMSS -> HH:MM[:SS]
	 * </pre>
	 * 
	 * @param  String					// 
	 * @return String
	 */
	public static String toTimeFormat ( 
			String argValue 
		) { 

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if( isValidNull( argValue ) || "000000".equals(argValue) || "0000".equals(argValue) ) return retS;

		try { 
			if( argValue.length() == 6 || argValue.length() == 4 ) 
				retS = toTimeFormat( argValue, argValue.length() );
			else 
				retS = argValue;
		} catch( Exception E ) { 
			return argValue;
		}

		return retS;
	}	// end of toTimeFormat

	/**
	 * <pre>
	 * 인자로 받은 시간을 표시 형식으로 바꿔주는 메소드
	 * ex) HHMMSS -> HH:MM[:SS]
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  Integer					// 자리수에 따른 결과표현 [6:시분초, 4:시분]
	 * @return String
	 */
	public static String toTimeFormat ( 
			String argValue, 
			int argSize 
		) { 

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if( isValidNull( argValue ) || "000000".equals(argValue) || "0000".equals(argValue) ) return retS;

		try { 
			if( argSize == 6 ) 
				retS = argValue.substring(0,2) +":"+ argValue.substring(2,4) +":"+ argValue.substring(4,6);
			else if( argSize == 4 ) 
				retS = argValue.substring(0,2) +":"+ argValue.substring(2,4);
			else 
				retS = argValue;
		} catch( Exception E ) { 
			return argValue;
		}

		return retS;
	}	// end of toTimeFormat

	/**
	 * <pre>
	 * 인자로 받은 시간을 표시 형식으로 바꿔주는 메소드
	 * ex) HHMMSS -> HH:MM[:SS]
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  String					// 타입에 따른 결과표현 [HM:시분, HH:시, MM:분]
	 * @return String
	 */
	public static String toTimeFormat ( 
			String argValue, 
			String argType 
		) { 

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if( isValidNull( argValue ) || "000000".equals(argValue) ) return retS;

		try { 
			if( argValue.length() == 6 ) 
				retS = argValue.substring(0,2) +"시 "+ argValue.substring(2,4) +"분 "+ argValue.substring(4,6) +"초";
			else if( (argValue.length() == 4 || argValue.length() == 6) && "HM".equalsIgnoreCase(argType)  ) 
				retS = argValue.substring(0,2) +"시 "+ argValue.substring(2,4) +"분";
			else if( (argValue.length() == 4 || argValue.length() == 6) && "HH".equalsIgnoreCase(argType)  ) 
				retS = argValue.substring(0,2) +"시";
			else if( (argValue.length() == 4 || argValue.length() == 6) && "MM".equalsIgnoreCase(argType)  ) 
				retS = argValue.substring(2,4) +"분";
			else 
				retS = argValue;
		} catch( Exception E ) { 
			return argValue;
		}

		return retS;
	}	// end of toTimeFormat

	/**
	 * <pre>
	 * 인자로 받은 문자열을 표시 형식으로 바꿔주는 메소드
	 * ex) 999999-9999999
	 * </pre>
	 * 
	 * @param	String					// 
	 * @param	String					// 일자 구분자
	 * @param	String					// 타입에 따른 결과표현 [999999-9999999, 9999-99-99]
	 * @return	String
	 */
	public static String toSSNFormat ( 
			String argValue, 
			String argSepa, 
			String argType 
		) { 

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if( isValidNull( argValue ) || "0000000000000".equals(argValue) ) return retS;

		try { 
			if( argValue.length() == 13 || "SSN".equalsIgnoreCase(argType) ) 
				retS = argValue.substring(0,6) +"-"+ argValue.substring(6);
			else if( argValue.length() == 8 || "BIR".equalsIgnoreCase(argType) ) { 
				retS = argValue.substring(0,1) == "0" || argValue.substring(0,1) == "1" || argValue.substring(0,1) == "2" ? "20" : "19";
				retS = retS + argValue.substring(0,2) + argSepa + argValue.substring(2,4) + argSepa + argValue.substring(4,6);
			} else 
				retS = argValue;
		} catch( Exception E ) { 
			return argValue;
		}

		return retS;
	}	// end of toSSNFormat

	/**
	 * <pre>
	 * 인자로 받은 문자열을 전화번호 형식으로 바꿔주는 메소드
	 * ex) 010-000-0000, 02-000-0000, 010-0000-0000
	 * </pre>
	 * 
	 * @param  String					// 
	 * @return String
	 */
	public static String toTeleFormat ( 
			String argValue 
		) { 

		String regEx				= "(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})";

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if( isValidNull( argValue ) ) return retS;

		try { 
			retS = argValue.replaceAll(regEx, "$1-$2-$3");
		} catch( Exception E ) { 
			return argValue;
		}

		return retS;
	}	// end of toTeleFormat

	/**
	 * <pre>
	 * 인자로 받은 문자열을 사업자번호 형식으로 바꿔주는 메소드
	 * ex) 000-00-00000
	 * </pre>
	 * 
	 * @param  String					// 
	 * @return String
	 */
	public static String toCorpFormat ( 
			String argValue 
		) { 

		String regEx				= "([0-9]{3})([0-9]+)([0-9]{5})";

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if( isValidNull( argValue ) ) return retS;

		try { 
			retS = argValue.replaceAll(regEx, "$1-$2-$3");
		} catch( Exception E ) { 
			return argValue;
		}

		return retS;
	}	// end of toCorpFormat

	/**
	 * <pre>
	 * 인자로 받은 문자열을 우편번호 형식으로 바꿔주는 메소드
	 * ex) 000-00-00000
	 * </pre>
	 * 
	 * @param  String					// 
	 * @return String
	 */
	public static String toPostFormat ( 
			String argValue 
		) { 

		String regEx				= "([0-9]+)([0-9]{3})";

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if( isValidNull( argValue ) ) return retS;

		try { 
			retS = argValue.replaceAll(regEx, argValue.length() <= 5 ? "$1$2" : "$1-$2");
		} catch( Exception E ) { 
			return argValue;
		}

		return retS;
	}	// end of toPostFormat

	/**
	 * <pre>
	 * 숫자를 천단위에 컴마(,)를 찍어 변환해주는 메소드(소수점 자리이하는 3자리까지 처리한다)
	 * </pre>
	 * 
	 * @param  String					// 
	 * @return String
	 */ 
	public static String toNumberFormat ( 
			String argValue 
		) {

		DecimalFormat nf			= new DecimalFormat("#,###.###");

		retS		= "0";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9.-]", "");

		if( isValidNull( argValue ) ) return retS;

		try { 
			return nf.format(round( Double.parseDouble(argValue), 3 ));
		} catch( NumberFormatException NFE ) { 
			return "0";
		}
	}	// end of toNumberFormat

	/**
	 * <pre>
	 * 숫자를 천단위에 컴마(,)를 찍어 변환해주는 메소드(소수점 자리이하는 3자리까지 처리한다)
	 * </pre>
	 * 
	 * @param  Integer					// 
	 * @return String
	 */ 
	public static String toNumberFormat ( 
			int argValue 
		) {

		DecimalFormat nf			= new DecimalFormat("#,###.###");

		retS		= "0";

		try { 
			return nf.format(round( Double.parseDouble(String.valueOf(argValue)), 3));
		} catch( NumberFormatException NFE ) { 
			return retS;
		}
	}	// end of toNumberFormat

	/**
	 * <pre>
	 * 숫자를 천단위에 컴마(,)를 찍어 변환해주는 메소드(소수점 자리이하는 3자리까지 처리한다)
	 * </pre>
	 * 
	 * @param  Integer					// 
	 * @return String
	 */ 
	public static String toNumberFormat ( 
			double argValue 
		) {

		DecimalFormat nf			= new DecimalFormat("#,###.###");

		retS		= "0";

		try { 
			return nf.format(round( Double.parseDouble(String.valueOf(argValue)), 3));
		} catch( NumberFormatException NFE ) { 
			return retS;
		}
	}	// end of toNumberFormat

	/**
	 * <pre>
	 * 숫자를 천단위에 컴마(,)를 찍어 변환(소수점 이하 자리수는 지정해준다)
	 * </pre>
	 * 
	 * @param  Double					// 
	 * @param  Integer					// 
	 * @return String
	 */
	public static String toNumberFormat ( 
			double argValue, 
			int floatNo 
		) {

		try { 
			return toNumberFormat( String.valueOf(argValue), floatNo );
		} catch( NumberFormatException NFE ) { 
			return toNumberFormat( String.valueOf(argValue) );
		}
	}	// end of toNumberFormat

	/**
	 * <pre>
	 * 숫자를 천단위에 컴마(,)를 찍어 변환해주는 메소드(소수점 이하 자리수는 지정해준다)
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  String					// 
	 * @return String
	 */
	public static String toNumberFormat ( 
			String argValue, 
			String floatNo 
		) {

		retS		= "0";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9.-]", "");

		if( isValidNull( argValue ) ) return retS;

		try { 
			int val = Integer.parseInt(floatNo);
			return toNumberFormat( argValue, val );
		} catch( NumberFormatException NFE ) { 
			return toNumberFormat( argValue );
		}
	}	// end of toNumberFormat

	/**
	 * <pre>
	 * 숫자를 천단위에 컴마(,)를 찍어 변환해주는 메소드(소수점 이하 자리수는 지정해준다)
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  Integer					// 
	 * @return String
	 */ 
	public static String toNumberFormat ( 
			String argValue, 
			int floatNo 
		) {

		retS		= "0";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9.-]", "");

		if( isValidNull( argValue ) ) return retS;

		try { 
			int rVal = argValue.indexOf(".");

			if( rVal != -1 && floatNo > 0 ) { 
				String temp = "";

				for( int i = 0; i < floatNo; i++ )	temp += "0";
				DecimalFormat nf = new DecimalFormat("#,##0." + temp);
				retS = nf.format(Double.parseDouble(argValue));
			} else { 
				DecimalFormat nf = new DecimalFormat("#,###"+ (floatNo == 0 ? "" : ".###"));
				retS	= nf.format(round( Double.parseDouble(argValue), (floatNo == 0 ? 0 : floatNo) ));
			}

			return retS;
		} catch( NumberFormatException NFE ) { 
			return argValue;
		}
	}	// end of toNumberFormat

	/**
	 * <pre>
	 * 숫자를 천단위에 컴마(,)를 찍어 변환해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  Integer					// 
	 * @param  Boolean					// 
	 * @return String
	 */ 
	public static String toNumberFormat ( 
			String argValue, 
			int floatNo, 
			boolean constraint 
		) {

		retS		= "0";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9.-]", "");

		if( isValidNull( argValue ) ) return retS;

		try { 
			if( argValue != null && constraint ) { 
				int rVal = argValue.indexOf(".");
				String temp = "";

				for( int i = 0; i < floatNo; i++ )	temp += "0";
				if( rVal == -1 )	argValue += ".";
				DecimalFormat nf = new DecimalFormat("#,##0." + temp);
				retS = nf.format(Double.parseDouble(argValue));
			} else {
				retS = toNumberFormat( argValue, floatNo );
			}

			return retS;
		} catch( NumberFormatException NFE ) { 
			return argValue;
		}
	}	// end of toNumberFormat

	/**
	 * <pre>
	 * 첫글자만 대문자로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @return String
	 * 
	 */
	public static String toCapitalize ( 
			String argValue 
		) { 

		String[] words				= argValue.split(" ");
		StringBuilder retVal		= new StringBuilder();

		if( isValidNull( argValue ) ) return defS;

		for( int c = 0; c < words.length; c++ ) { 
			retVal.append(Character.toUpperCase(words[c].charAt(0)));
			retVal.append(words[c].substring(1));
			if( c < words.length - 1 ) retVal.append(" ");
		}

		return retVal.toString();
	}	// end of toCapitalize


	/**
	 * <pre>
	 * 
	 * </pre>
	 * 
	 * @param  Byte[]					// 
	 * @return String
	 */
	public static byte[] hex2ByteArray ( 
			String argValue 
		) { 

		if( isValidNull( argValue ) ) return null;

		byte[] nData				= new byte[argValue.length() / 2];
		for( int k = 0; k < nData.length; k++ ) 
			nData[k] = (byte)Integer.parseInt(argValue.substring(2*k,2*k+2), 16);

		return nData;
	}	// end of hex2ByteArray

	/**
	 * <pre>
	 * 
	 * </pre>
	 * 
	 * @param  String					// 
	 * @return Byte[]
	 */
	public static String byteArray2Hex ( 
			byte[] argValue 
		) { 

		if( argValue == null || argValue.length <= 0 ) return null;

		StringBuffer retSB			= new StringBuffer(argValue.length * 2);
		String hexNumber			= "";

		for( int k = 0; k < argValue.length; k++ ) { 
			hexNumber = "0"+ Integer.toHexString(0xff & argValue[k]);
			retSB.append(hexNumber.substring(hexNumber.length() - 2));
		}

		return retSB == null || retSB.length() <= 0 ? "" : retSB.toString();
	}	// end of byteArray2Hex


	/**
	 * <pre>
	 * TimeStamp를 이용하여 유일값을 취득해주는 메소드
	 * </pre>
	 * 
	 * @return String
	 * @throws Exception
	 * @see
	 */
	public static String getTimeStamp() { 

		String dateFmt				= "yyyyMMddHHmmss";
		SimpleDateFormat sdf		= new SimpleDateFormat(dateFmt, Locale.KOREA);
		sdf.setLenient(false);		// False로 설정해야 엄밀한 해석을 함.
		Timestamp ts				= null;

		retS		= "";

		try { 
			ts = new Timestamp(System.currentTimeMillis());

			retS = sdf.format(ts.getTime());
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		}

		return retS;
	}	// end of getTimeStamp

	/**
	 * <pre>
	 * 시간을 원하는만큼 '더하거나 혹은 빼거나' 하여 새로운 날짜로 변환해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  Integer					// 
	 * @return String
	 */ 
	public static String getTimeOfAdder ( 
			String argValue, 
			int argIndex 
		) { 

		String dateFmt				= "yyyyMMddHHmmss";
		Calendar cal				= Calendar.getInstance();
		SimpleDateFormat sdf		= new SimpleDateFormat(dateFmt, Locale.KOREA);
		sdf.setLenient(false);		// False로 설정해야 엄밀한 해석을 함.

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if(  isValidNull( argValue ) ) return retS;
		if( !isValidDate( argValue, dateFmt ) ) return retS;

		try { 
			if( argValue.length() == 2 ) { 
				cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(argValue.substring(0,2)));
				cal.add(Calendar.HOUR_OF_DAY, argIndex);
				sdf.applyPattern("HH");
			} else if( argValue.length() == 4 ) { 
				cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(argValue.substring(0,2)));
				cal.set(Calendar.MINUTE      , Integer.parseInt(argValue.substring(2,4)));
				cal.add(Calendar.MINUTE      , argIndex);
				sdf.applyPattern("HHmm");
			} else if( argValue.length() == 6 ) { 
				cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(argValue.substring(0,2)));
				cal.set(Calendar.MINUTE      , Integer.parseInt(argValue.substring(2,4)));
				cal.set(Calendar.SECOND      , Integer.parseInt(argValue.substring(4,6)));
				cal.add(Calendar.SECOND      , argIndex);
				sdf.applyPattern("HHmmss");
			}

			return sdf.format(cal.getTime());
		} catch( Exception E ) { 
			return argValue;
		}
	}	// end of getTimeOfAdder

	/**
	 * <pre>
	 * 시스템의 현재일자를 가져오는 메소드
	 * </pre>
	 * 
	 * @return String
	 */
	public static String getDateOfNow ( 
		) { 

		return getDateOfNow( "" );
	}	// end of getDateOfNow

	/**
	 * <pre>
	 * 시스템의 현재일자를 가져오는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @return String
	 */
	public static String getDateOfNow ( 
			String dateFmt 
		) { 

		dateFmt		= null2Char( dateFmt, "yyyyMMddHHmmss" );

		SimpleDateFormat df			= new SimpleDateFormat(dateFmt, Locale.KOREA);
		df.setLenient(false);		// False로 설정해야 엄밀한 해석을 함.

		return (String)df.format(new java.util.Date()).trim();
	}	// end of getDateOfNow

	/**
	 * <pre>
	 * 해당년월[YYYYMM]이 처음(마지막) 일자를 일자형식[YYYYMMDD]으로 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  String					// 선택 [F:월의 처음일자, 그외:월의 마지막일자]
	 * @return String
	 */
	public static String getDateOfLast ( 
			String argValue, 
			String argType 
		) { 

		int[] iDate					= new int[3];
		String[] lastDay			= {"0", "31", "28", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"};

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if( isValidNull( argValue ) ) return retS;

		try { 
			if( "F".equalsIgnoreCase(argType) ) 
				retS = (argValue.length() <= 4 ? "" : argValue.substring(0,6)) +"01";
			else { 
				iDate[0] = Integer.parseInt(argValue.substring(0,4));
				iDate[1] = Integer.parseInt(argValue.substring(4,6));
				lastDay[2] = iDate[0] % 4 == 0 || iDate[0] % 100 != 0 && iDate[0] % 400 == 0 ? "29" : "28";
				retS = argValue.substring(0,6) + lastDay[iDate[1]];
			}
		} catch( Exception E ) { 
			return argValue;
		}

		return retS;
	}	// end of getDateOfLast

	/**
	 * <pre>
	 * 날짜를 원하는만큼 '더하거나 혹은 빼거나' 하여 새로운 날짜로 변환해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  Integer					// 
	 * @return String
	 */ 
	public static String getDateOfAdder ( 
			String argValue, 
			int argIndex 
		) { 

		String dateFmt				= "yyyyMMdd";
		Calendar cal				= Calendar.getInstance();
		SimpleDateFormat sdf		= new SimpleDateFormat(dateFmt, Locale.KOREA);
		sdf.setLenient(false);		// False로 설정해야 엄밀한 해석을 함.

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if(  isValidNull( argValue ) ) return retS;

		try { 
			if( argValue.length() == 4 ) { 
				dateFmt = "yyyy";
				if( !isValidDate( argValue, dateFmt ) ) return retS;
				cal.set(Calendar.YEAR       , Integer.parseInt(argValue.substring(0,4)));
				cal.add(Calendar.YEAR       , argIndex);
				sdf.applyPattern(dateFmt);
			} else if( argValue.length() == 6 ) { 
				dateFmt = "yyyyMM";
				if( !isValidDate( argValue, dateFmt ) ) return retS;
				cal.set(Calendar.YEAR       , Integer.parseInt(argValue.substring(0,4)));
				cal.set(Calendar.MONTH      , Integer.parseInt(argValue.substring(4,6)) - 1);
				cal.add(Calendar.MONTH      , argIndex);
				sdf.applyPattern(dateFmt);
			} else if( argValue.length() == 8 ) { 
				dateFmt = "yyyyMMdd";
				if( !isValidDate( argValue, dateFmt ) ) return retS;
				cal.set(Calendar.YEAR       , Integer.parseInt(argValue.substring(0,4)));
				cal.set(Calendar.MONTH      , Integer.parseInt(argValue.substring(4,6)) - 1);
				cal.set(Calendar.DATE       , Integer.parseInt(argValue.substring(6,8)));
				cal.add(Calendar.DATE       , argIndex);
				sdf.applyPattern(dateFmt);
			}

			return sdf.format(cal.getTime());
		} catch( Exception E ) { 
			return argValue;
		}
	}	// end of getDateOfAdder

	/**
	 * <pre>
	 * 시작일자와 종료일자의 해당월 수을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 시작 일자
	 * @param  String					// 종료 일자
	 * @return String
	 */
	public static int getDateOfSubtract ( 
			String argValue1, 
			String argValue2 
		) { 

		retI		= 0;
		argValue1	= null2Blank( argValue1 ).replaceAll("[^0-9]", "");
		argValue2	= null2Blank( argValue2 ).replaceAll("[^0-9]", "");

		if( isValidNull( argValue1 ) || isValidNull( argValue2 ) ) return retI;

		try { 
			retI = (Integer.parseInt(argValue2.substring(0,4)) - Integer.parseInt(argValue1.substring(0,4))) * 12 + Integer.parseInt(argValue2.substring(4,6)) - Integer.parseInt(argValue1.substring(4,6));
		} catch( Exception E ) { 
			return 0;
		}

		return retI;
	}	// end of getDateOfSubtract

	/**
	 * <pre>
	 * 시작일시와 종료일시를 비교하여 사이인지 아닌지 여부를 확인해주는 메소드
	 * </pre>
	 * 
	 * @param  Boolean					// 조건 해제 값 [true: 시작일시 비교금지, false: 시작일시 비교처리]
	 * @param  String					// 기준 형식
	 * @param  String					// 기준 일시
	 * @param  String					// 시작 일시
	 * @param  String					// 종료 일시
	 * @return Boolean
	 */
	public static boolean getDateOfCompare ( 
			boolean argType, 
			String dateFmt, 
			String argValue1, 
			String argValue2, 
			String argValue3 
		) { 

		dateFmt		= null2Char( dateFmt, "yyyyMMddHHmm" );

		SimpleDateFormat sdf		= new SimpleDateFormat(dateFmt, Locale.KOREA);
		sdf.setLenient(false);		// False로 설정해야 엄밀한 해석을 함.

		Date stdDate				= new Date();
		Date bgnDate				= new Date();
		Date endDate				= new Date();

		retB		= false;
		argValue1	= null2Blank( argValue1 ).replaceAll("[^0-9]", "");
		argValue2	= null2Blank( argValue2 ).replaceAll("[^0-9]", "");
		argValue3	= null2Blank( argValue3 ).replaceAll("[^0-9]", "");

		if( isValidNull( argValue1 ) || isValidNull( argValue2 ) || isValidNull( argValue3 ) ) return retB;

		try { 
			stdDate = sdf.parse(argValue1);
			bgnDate = sdf.parse(argValue2);
			endDate = sdf.parse(argValue3);

			retB = ((argType || stdDate.compareTo(bgnDate) >= 0) && stdDate.compareTo(endDate) <= 0) && ((argType || bgnDate.before(stdDate)) && endDate.after(stdDate));
		} catch( Exception E ) { 
			return false;
		}

		return retB;
	}	// end of getDateOfCompare

	/**
	 * <pre>
	 * 해당일자의 분기 내역을 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 반기/분기 구분 [H:반기,Q:분기]
	 * @param  String					// 기준 일자
	 * @return 
	 */
	public static String getDateOfQuarter ( 
			String argType, 
			String argValue 
		) { 

		int gMonth	= 0;

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if( isValidNull( argValue ) ) return retS;
		gMonth		= Integer.parseInt(argValue.substring(4,6));

		if( "Q".equalsIgnoreCase(argType) )  
			retS = String.valueOf((int)Math.ceil(gMonth / 3));
		else if( "H".equalsIgnoreCase(argType) )  
			retS = String.valueOf((int)Math.ceil(gMonth / 7) + 1);
		else 
			retS = String.valueOf((int)Math.ceil(gMonth / 1));

		return retS;
	}	// end of getDateOfQuarter

	/**
	 * <pre>
	 * 해당일자의 해당월(주) 처음(마직막)일자를 일자형식[YYYYMMDD]으로 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  String					// 선택 [MF:월의 처음일자, ML:월의 마지막일자, WF:주의 처음일자, WL:주의 마지막일자]
	 * @return String
	 */
	public static String getWeekInMonths ( 
			String argValue, 
			String argType 
		) { 

		String dateFmt				= "yyyyMMdd";
		Calendar cal				= Calendar.getInstance();
		SimpleDateFormat sdf		= new SimpleDateFormat(dateFmt, Locale.KOREA);
		sdf.setLenient(false);		// False로 설정해야 엄밀한 해석을 함.
		int dayOfWeek				= 0;

		retS		= "";
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if(  isValidNull( argValue ) ) return retS;
		if( !isValidDate( argValue, dateFmt ) ) return retS;

		try { 
			cal.set(Calendar.YEAR       , Integer.parseInt(argValue.substring(0,4)));
			cal.set(Calendar.MONTH      , Integer.parseInt(argValue.substring(4,6)) - 1);
			cal.set(Calendar.DATE       , Integer.parseInt(argValue.substring(6,8)));
			if( "MF".equals(argType) || "WF".equals(argType) ) { 
				if( "MF".equals(argType) ) cal.set(Calendar.DATE       , Integer.parseInt(getDateOfLast( argValue, "F" ).substring(6,8)));
				cal.setFirstDayOfWeek(Calendar.SUNDAY);
				dayOfWeek = cal.get(Calendar.DAY_OF_WEEK) - cal.getFirstDayOfWeek();
				cal.add(Calendar.DAY_OF_MONTH, 0-dayOfWeek);
				retS = sdf.format(cal.getTime());
			}
			if( "ML".equals(argType) || "WL".equals(argType) ) { 
				if( "ML".equals(argType) ) cal.set(Calendar.DATE       , Integer.parseInt(getDateOfLast( argValue, "L" ).substring(6,8)));
				cal.setFirstDayOfWeek(Calendar.SUNDAY);
				dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
				cal.add(Calendar.DAY_OF_MONTH, 7-dayOfWeek);
				retS = sdf.format(cal.getTime());
			}
		} catch( Exception E ) { 
			retS = "";
		}

		return retS;
	}	// end of getWeekInMonths

	/**
	 * <pre>
	 * 해당일자의 주수를 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  String					// 선택 [C:현재일자의 주수, M:현재월의 최대주수]
	 * @return Integer
	 */
	public static int getWeekOfMonths ( 
			String argValue, 
			String argType 
		) { 

		String dateFmt				= "yyyyMMdd";
		Calendar cal				= Calendar.getInstance();
		SimpleDateFormat sdf		= new SimpleDateFormat(dateFmt, Locale.KOREA);
		sdf.setLenient(false);		// False로 설정해야 엄밀한 해석을 함.
		int dayOfWeek				= 0;

		retI		= 0;
		argValue	= null2Blank( argValue ).replaceAll("[^0-9]", "");

		if(  isValidNull( argValue ) ) return retI;
		if( !isValidDate( argValue, dateFmt ) ) return retI;

		try { 
			cal.set(Calendar.YEAR       , Integer.parseInt(argValue.substring(0,4)));
			cal.set(Calendar.MONTH      , Integer.parseInt(argValue.substring(4,6)) - 1);
			cal.set(Calendar.DATE       , "C".equals(argType) ? Integer.parseInt(argValue.substring(6,8)) : cal.getActualMaximum(Calendar.DATE));

			retI = cal.get(Calendar.WEEK_OF_MONTH);
		} catch( Exception E ) { 
			retI = 0;
		}

		return retI;
	}	// end of getWeekCountOfMonth

	/**
	 * <pre>
	 * 지정되지 않은 파일을 확인해주는 메소드
	 * </pre>
	 * 
	 * @param	String					// 
	 * @return	boolean					// 
	 */
	public static boolean getFileAllowOfBase ( 
			String argValue 
		) { 

		retB		= false;

		if( isValidNull( argValue ) ) return retB;

		try { 
			retB  = argValue.toLowerCase().endsWith(".ppt") || argValue.toLowerCase().endsWith(".pptx") || 
					argValue.toLowerCase().endsWith(".xls") || argValue.toLowerCase().endsWith(".xlsx") || 
					argValue.toLowerCase().endsWith(".doc") || argValue.toLowerCase().endsWith(".docx") || 
					argValue.toLowerCase().endsWith(".hwp") || argValue.toLowerCase().endsWith(".pdf")  || 
					argValue.toLowerCase().endsWith(".zip") || argValue.toLowerCase().endsWith(".zipx") || 
					argValue.toLowerCase().endsWith(".alz") || 
					argValue.toLowerCase().endsWith(".jpg") || argValue.toLowerCase().endsWith(".jpeg") || 
					argValue.toLowerCase().endsWith(".tif") || argValue.toLowerCase().endsWith(".tiff") || 
					argValue.toLowerCase().endsWith(".gif") || argValue.toLowerCase().endsWith(".png")  || 
					argValue.toLowerCase().endsWith(".bmp") || 
					argValue.toLowerCase().endsWith(".asf") || argValue.toLowerCase().endsWith(".avi")  || 
					argValue.toLowerCase().endsWith(".mpg") || argValue.toLowerCase().endsWith(".mpeg") || 
					argValue.toLowerCase().endsWith(".wmv") || argValue.toLowerCase().endsWith(".mp4");
		} catch( Exception E ) { 
			return false;
		}

		return retB;
	}	// end of getFileAllowOfBase

	/**
	 * <pre>
	 * 지정되지 않은 파일(문서)을 확인해주는 메소드
	 * </pre>
	 * 
	 * @param	String					// 
	 * @return	boolean					// 
	 */
	public static boolean getFileAllowOfDocu ( 
			String argValue 
		) { 

		retB		= false;

		if( isValidNull( argValue ) ) return retB;

		try { 
			retB  = argValue.toLowerCase().endsWith(".ppt") || argValue.toLowerCase().endsWith(".pptx") || 
					argValue.toLowerCase().endsWith(".xls") || argValue.toLowerCase().endsWith(".xlsx") || 
					argValue.toLowerCase().endsWith(".doc") || argValue.toLowerCase().endsWith(".docx") || 
					argValue.toLowerCase().endsWith(".hwp") || argValue.toLowerCase().endsWith(".pdf")  || 
					argValue.toLowerCase().endsWith(".zip") || argValue.toLowerCase().endsWith(".zipx");
		} catch( Exception E ) { 
			return false;
		}

		return retB;
	}	// end of getFileAllowOfDocu

	/**
	 * <pre>
	 * 지정되지 않은 파일(그림)을 확인해주는 메소드
	 * </pre>
	 * 
	 * @param	String					// 
	 * @return	boolean					// 
	 */
	public static boolean getFileAllowOfImage ( 
			String argValue 
		) { 

		retB		= false;

		if( isValidNull( argValue ) ) return retB;

		try { 
			retB  = argValue.toLowerCase().endsWith(".jpg") || argValue.toLowerCase().endsWith(".jpeg") || 
					argValue.toLowerCase().endsWith(".tif") || argValue.toLowerCase().endsWith(".tiff") || 
					argValue.toLowerCase().endsWith(".gif") || argValue.toLowerCase().endsWith(".png")  || 
					argValue.toLowerCase().endsWith(".bmp");
		} catch( Exception E ) { 
			return false;
		}

		return retB;
	}	// end of getFileAllowOfImage

	/**
	 * <pre>
	 * 지정되지 않은 파일을 확인해주는 메소드
	 * </pre>
	 * 
	 * @param	String					// 
	 * @return	boolean					// 
	 */
	public static boolean getFileAllowOfDocuImage ( 
			String argValue 
		) { 

		retB		= false;

		if( isValidNull( argValue ) ) return retB;

		try { 
			retB  = argValue.toLowerCase().endsWith(".ppt") || argValue.toLowerCase().endsWith(".pptx") || 
					argValue.toLowerCase().endsWith(".xls") || argValue.toLowerCase().endsWith(".xlsx") || 
					argValue.toLowerCase().endsWith(".doc") || argValue.toLowerCase().endsWith(".docx") || 
					argValue.toLowerCase().endsWith(".hwp") || argValue.toLowerCase().endsWith(".pdf")  || 
					argValue.toLowerCase().endsWith(".zip") || argValue.toLowerCase().endsWith(".zipx") || 
					argValue.toLowerCase().endsWith(".jpg") || argValue.toLowerCase().endsWith(".jpeg") || 
					argValue.toLowerCase().endsWith(".tif") || argValue.toLowerCase().endsWith(".tiff") || 
					argValue.toLowerCase().endsWith(".gif") || argValue.toLowerCase().endsWith(".png")  || 
					argValue.toLowerCase().endsWith(".bmp");
		} catch( Exception E ) { 
			return false;
		}

		return retB;
	}	// end of getFileAllowOfBase

	/**
	 * <pre>
	 * 지정되지 않은 파일(영상)을 확인해주는 메소드
	 * </pre>
	 * 
	 * @param	String					// 
	 * @return	boolean					// 
	 */
	public static boolean getFileAllowOfMovie ( 
			String argValue 
		) { 

		retB		= false;

		if( isValidNull( argValue ) ) return retB;

		try { 
			retB  = argValue.toLowerCase().endsWith(".mp4") || argValue.toLowerCase().endsWith(".webm")  || 
					argValue.toLowerCase().endsWith(".ogg");
		} catch( Exception E ) { 
			return false;
		}

		return retB;
	}	// end of getFileAllowOfMovie

	/**
	 * <pre>
	 * 지정되지 않은 파일(영상)을 확인해주는 메소드
	 * </pre>
	 * 
	 * @param	String					// 
	 * @return	boolean					// 
	 */
	public static boolean getFileAllowOfMovie2 ( 
			String argValue 
		) { 

		retB		= false;

		if( isValidNull( argValue ) ) return retB;

		try { 
			retB  = argValue.toLowerCase().endsWith(".asf") || argValue.toLowerCase().endsWith(".avi")  || 
					argValue.toLowerCase().endsWith(".mpg") || argValue.toLowerCase().endsWith(".mpeg") || 
					argValue.toLowerCase().endsWith(".wmv") || argValue.toLowerCase().endsWith(".mp4");
		} catch( Exception E ) { 
			return false;
		}

		return retB;
	}	// end of getFileAllowOfMovie

	/**
	 * <pre>
	 * 지정되지 않은 파일(Excel)을 확인해주는 메소드
	 * </pre>
	 * 
	 * @param	String					// 
	 * @return	boolean					// 
	 */
	public static boolean getFileAllowOfExcel ( 
			String argValue 
		) { 

		retB		= false;

		if( isValidNull( argValue ) ) return retB;

		try { 
			retB  = argValue.toLowerCase().endsWith(".xls") || argValue.toLowerCase().endsWith(".xlsx");
		} catch( Exception E ) { 
			return false;
		}

		return retB;
	}	// end of getFileAllowOfExcel

	/**
	 * <pre>
	 * 지정되지 않은 파일(PDF)을 확인해주는 메소드
	 * </pre>
	 * 
	 * @param	String					// 
	 * @return	boolean					// 
	 */
	public static boolean getFileAllowOfPDF ( 
			String argValue 
		) { 

		retB		= false;

		if( isValidNull( argValue ) ) return retB;

		try { 
			retB  = argValue.toLowerCase().endsWith(".pdf");
		} catch( Exception E ) { 
			return false;
		}

		return retB;
	}	// end of getFileAllowOfPDF


}	// end of class