package egovframework.com.cmm;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.*;

import org.slf4j.*;
import org.springframework.util.*;
import org.springframework.web.multipart.*;
import org.springframework.web.multipart.commons.*;

import egovframework.com.cmm.util.BaseUtility;
import egovframework.ptcc.cmmn.util.*;

public class EgovComMultiPartHndlr extends CommonsMultipartResolver implements MultipartResolver {

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());

	public EgovComMultiPartHndlr(){super();}
	public EgovComMultiPartHndlr(HttpServletRequest request){super();}
	public EgovComMultiPartHndlr(ServletContext servletContext){this();setServletContext(servletContext);}

	@Override
	public MultipartHttpServletRequest resolveMultipart (
			HttpServletRequest request 
		) throws MultipartException {

		long					maxFileSize  = 20971520;	// 20MB
		int						maxMemsSize  = 52428800;	// 50MB
		boolean[]				requestURI   = new boolean[6];

		requestURI[0] = request.getRequestURI().contains("/coun/hope");
		requestURI[1] = request.getRequestURI().contains("/coun/base/noticeHope");
		requestURI[2] = request.getRequestURI().contains("/coun/visit");
		requestURI[3] = request.getRequestURI().contains("/coun/acts/visitIntro");
		requestURI[4] = request.getRequestURI().contains("/coun/tip");
		requestURI[5] = request.getRequestURI().contains("/coun/base/noticeTip");

		if( UserDetailsHelper.isAuthenticatedAdmin() ) { 
//			maxFileSize = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "file.upAdm" ));
			maxFileSize = Long.parseLong(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "file.upAdm" ));
		} else if( UserDetailsHelper.isAuthenticatedUser() ) { 
//			maxFileSize = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "file.upInt" ));
			maxFileSize = Long.parseLong(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "file.upInt" ));
		} else if( requestURI[0] || requestURI[1] || requestURI[2] || requestURI[3] || requestURI[4] || requestURI[5] ) {
//			maxFileSize = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "file.upMax" ));
			maxFileSize = Long.parseLong(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "file.upMax" ));
		} else { 
			throw new MultipartException("예상하지 않은 파일업로드가 시도됨.");
		}

//		maxMemsSize = maxFileSize < maxMemsSize ? maxFileSize-1 : maxMemsSize;

		setMaxUploadSize(maxFileSize);
		setMaxInMemorySize(maxMemsSize);
log.info("maxFileSize: "+ maxFileSize +" |maxMemsSize:"+ maxMemsSize);

		try {
			return super.resolveMultipart(request);
		} catch (MaxUploadSizeExceededException MUSEE) {
			request.setAttribute("exception", MUSEE);
			return super.resolveMultipart(request);
//			return new DefaultMultipartHttpServletRequest(request);
		}
	}

	public MultipartHttpServletRequest resolveMultipart (
			HttpServletRequest request, 
			MultiValueMap<String, MultipartFile> mpFiles, 
			Map<String, String[]> mpParams, 
			Map<String, String> mpParamContentTypes 
		) {

		long					maxFileSize  = 20971520;	// 20MB
		int						maxMemsSize  = 52428800;	// 50MB
		boolean[]				requestURI   = new boolean[6];

		requestURI[0] = request.getRequestURI().contains("/coun/hope");
		requestURI[1] = request.getRequestURI().contains("/coun/base/noticeHope");
		requestURI[2] = request.getRequestURI().contains("/coun/visit");
		requestURI[3] = request.getRequestURI().contains("/coun/acts/visitIntro");
		requestURI[4] = request.getRequestURI().contains("/coun/tip");
		requestURI[5] = request.getRequestURI().contains("/coun/base/noticeTip");

		if( UserDetailsHelper.isAuthenticatedAdmin() ) { 
//			maxFileSize = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "file.upAdm" ));
			maxFileSize = Long.parseLong(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "file.upAdm" ));
		} else if( UserDetailsHelper.isAuthenticatedUser() ) { 
//			maxFileSize = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "file.upInt" ));
			maxFileSize = Long.parseLong(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "file.upInt" ));
		} else if( requestURI[0] || requestURI[1] || requestURI[2] || requestURI[3] || requestURI[4] || requestURI[5] ) {
//			maxFileSize = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "file.upMax" ));
			maxFileSize = Long.parseLong(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "file.upMax" ));
		} else { 
			throw new MultipartException("예상하지 않은 파일업로드가 시도됨.");
		}

//		maxMemsSize = maxFileSize<maxMemsSize?maxFileSize-1:maxMemsSize;

		setMaxUploadSize(maxFileSize);
		setMaxInMemorySize(maxMemsSize);
log.info("maxFileSize: "+ maxFileSize +" |maxMemsSize:"+ maxMemsSize);

		try {
			return super.resolveMultipart(request);
		} catch (MaxUploadSizeExceededException MUSEE) {
			request.setAttribute("exception", MUSEE);
			return super.resolveMultipart(request);
//			return new DefaultMultipartHttpServletRequest(request);
		}
	}

}