/*******************************************************************************
  Program ID  : MainCounController
  Description : 클라이언트의 요청(공통)을 처리할 메서드를 구현
  Author      : HexaMedia new0man
  Write Date  : 2019.06.04
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v1.0    (주)헥사미디어 개발자  2014.00.00  최초 생성
  v2.0    HexaMedia new0man      2019.06.04  평택시의회 Site Renewal
  v2.1    HexaMedia new0man      2020.03.17  비공개 목록 표시 및 검색조건 추가
 *******************************************************************************/
package egovframework.ptcc.coun.main.web;

import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.util.*;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.board.service.*;
import egovframework.ptcc.mngr.agenda.service.*;

@Controller
public class MainCounController { 

	@Autowired
	private WebApplicationContext			webContext;

	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="boardMngrService")
	protected BoardMngrService				boardMngrService;
	@Resource(name="agendaMngrService")
	protected AgendaMngrService			agendaMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected String						hideMark     = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.HideMark" );
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));
	protected boolean						isGallery    = "Gallery.do".equalsIgnoreCase(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.ViewPage" ));
	protected String						replyImage   = "<img src='@@/images/icon/ico_reply.png' alt=''/>";


	/**
	 * <pre>
	 * 시의회 메인화면을(를) 조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/index.do")
	public String scrInitOfCounMain ( 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseCommonVO			searchVO     = new BaseCommonVO();

		List<String>			bbsTypes     = new ArrayList<String>();
		List<BaseBoardsVO>		mainImage    = new ArrayList<BaseBoardsVO>();
		List<BaseBoardsVO>		mainPopup    = new ArrayList<BaseBoardsVO>();
		List<BaseBoardsVO>		mainBanner   = new ArrayList<BaseBoardsVO>();

		List<BaseAgendaVO>		mainMemb     = new ArrayList<BaseAgendaVO>();

		List<BaseBoardsVO>		mainNoti     = new ArrayList<BaseBoardsVO>();
		List<BaseBoardsVO>		mainRaws     = new ArrayList<BaseBoardsVO>();
		List<BaseBoardsVO>		mainSchd     = new ArrayList<BaseBoardsVO>();
		List<BaseBoardsVO>		mainPress    = new ArrayList<BaseBoardsVO>();
		List<BaseBoardsVO>		mainMovie    = new ArrayList<BaseBoardsVO>();
		List<BaseBoardsVO>		mainPhoto    = new ArrayList<BaseBoardsVO>();
		List<BaseAgendaVO>		mainRecv     = new ArrayList<BaseAgendaVO>();
		List<BaseAgendaVO>		mainProc     = new ArrayList<BaseAgendaVO>();
		int[]					tempCnt      = new int[5];
		int						listCnt      = 0;

		try { 
//			// 방문자내역 갱신
//			BaseVisitsVO visitVO = new BaseVisitsVO();
//			visitVO.setVsnYear(systemVO.getToYear());
//			visitVO = (BaseVisitsVO)baseCmmnService.getInfoOfVisitSeq( visitVO );
//			visitVO.setVsnIPv4(BaseUtility.reqIPv4Address( request ));
//			visitVO.setVsnSite(request.getRequestURL().toString());
//			visitVO.setVsnReferer(request.getHeader("REFERER"));
//			visitVO.setVsnAgent(request.getHeader("User-Agent"));
//			visitVO.setCreUser("USR");
//			if( baseCmmnService.getCountOfVisitDay( visitVO.getVsnIPv4() ) <= 0 ) baseCmmnService.doRtnOfCreateVisit( visitVO );
//			visitVO = (BaseVisitsVO)baseCmmnService.getStatOfVisit(null);
//			session.setAttribute("totVisit",BaseUtility.toNumberFormat(visitVO.getTotsCnt()));
//			session.setAttribute("dayVisit",BaseUtility.toNumberFormat(visitVO.getDaysCnt()));

			// mainImage : 메인이미지 내역 취득
			searchVO = new BaseCommonVO();
			searchVO.setBbsType("10210010");
			searchVO.setFirDate(firDate);
			searchVO.setReCreHide("N");
			mainImage = (List<BaseBoardsVO>)boardMngrService.getListOfBoardImgs( searchVO );
			// Data Adjustment(Boards)
			if( !(mainImage == null || mainImage.size() <= 0) ) { 
				for( int d = 0; d < mainImage.size(); d++ ) { 
					mainImage.get(d).setViewNo(mainImage.get(d).getBbsUUID() + BaseUtility.null2Char( mainImage.get(d).getRowSeq(), String.valueOf(d+1) ));
					mainImage.get(d).setContLinkC(BaseUtility.getUriCheck( mainImage.get(d).getBbsSiteUri() ) ? "T" : "F");
					mainImage.get(d).setFileUUID(BaseUtility.null2Char(mainImage.get(d).getFileUUID(), "", "0"+ mainImage.get(d).getFileUUID() ));
					if( !mainImage.get(d).getCreUser().equals(mainImage.get(d).getCreName()) ) mainImage.get(d).setCreName(propertiesService.getString("creName"));
				}
			}

			// mainPopup : 메인팝업 내역 취득
			searchVO = new BaseCommonVO();
			searchVO.setBbsType("10210040");
			searchVO.setFirDate(firDate);
			searchVO.setReCreHide("N");
			mainPopup = (List<BaseBoardsVO>)boardMngrService.getListOfBoardImgs( searchVO );
			// Data Adjustment(Boards)
			if( !(mainPopup == null || mainPopup.size() <= 0) ) { 
				for( int d = 0; d < mainPopup.size(); d++ ) { 
					mainPopup.get(d).setViewNo(mainPopup.get(d).getBbsUUID() + BaseUtility.null2Char( mainPopup.get(d).getRowSeq(), String.valueOf(d+1) ));
					mainPopup.get(d).setContLinkC(BaseUtility.getUriCheck( mainPopup.get(d).getBbsSiteUri() ) ? "T" : "F");
					mainPopup.get(d).setFileUUID(BaseUtility.null2Char(mainPopup.get(d).getFileUUID(), "", "2"+ mainPopup.get(d).getFileUUID() ));
					if( !mainPopup.get(d).getCreUser().equals(mainPopup.get(d).getCreName()) ) mainPopup.get(d).setCreName(propertiesService.getString("creName"));
				}
			}

			// mainBanner : 메인배너 내역 취득
			searchVO = new BaseCommonVO();
			searchVO.setBbsType("10210070");
			searchVO.setFirDate(firDate);
			searchVO.setReCreHide("N");
			mainBanner = (List<BaseBoardsVO>)boardMngrService.getListOfBoardImgs( searchVO );
			// Data Adjustment(Boards)
			if( !(mainBanner == null || mainBanner.size() <= 0) ) { 
				for( int d = 0; d < mainBanner.size(); d++ ) { 
					mainBanner.get(d).setViewNo(mainBanner.get(d).getBbsUUID() + BaseUtility.null2Char( mainBanner.get(d).getRowSeq(), String.valueOf(d+1) ));
					mainBanner.get(d).setContLinkC(BaseUtility.getUriCheck( mainBanner.get(d).getBbsSiteUri() ) ? "T" : "F");
					mainBanner.get(d).setFileUUID(BaseUtility.null2Char(mainBanner.get(d).getFileUUID(), "", "1"+ mainBanner.get(d).getFileUUID() ));
					if( !mainBanner.get(d).getCreUser().equals(mainBanner.get(d).getCreName()) ) mainBanner.get(d).setCreName(propertiesService.getString("creName"));
				}
			}

			// mainMemb : 현역의원 내역 취득
			searchVO = new BaseCommonVO();
			searchVO.setFirDate(firDate);
			mainMemb = (List<BaseAgendaVO>)agendaMngrService.getMainOfAgendaMemb( searchVO );
			// Data Adjustment(Agendas)
			if( !(mainMemb == null || mainMemb.size() <= 0) ) { 
				for( int d = 0; d < mainMemb.size(); d++ ) { 
					mainMemb.get(d).setComName(BaseUtility.null2Char( mainMemb.get(d).getComName(), "의원" ));
				}
			}

			// mainNoti : 공지사항/입법예고 내역 취득
			bbsTypes = new ArrayList<String>();
			bbsTypes.add("10150010"); // 공지사항
			bbsTypes.add("10110070"); // 입법예고
			searchVO = new BaseCommonVO();
			searchVO.setBbsType("");
			searchVO.setBbsTypes(bbsTypes);
			searchVO.setFirDate(firDate);
			searchVO.setRowCnt("5");
			searchVO.setReCreHide("N");
			mainNoti = (List<BaseBoardsVO>)boardMngrService.getListOfBoardMain( searchVO );
			// Data Adjustment(Boards)
			if( !(mainNoti == null || mainNoti.size() <= 0) ) { 
				for( int d = 0; d < mainNoti.size(); d++ ) { 
					if( "10150010".equals(mainNoti.get(d).getBbsType()) ) {tempCnt[0]=d!=0?tempCnt[0]+1:1;listCnt=tempCnt[0];};
					if( "10110070".equals(mainNoti.get(d).getBbsType()) ) {tempCnt[1]=d!=0?tempCnt[1]+1:1;listCnt=tempCnt[1];};
					mainNoti.get(d).setViewNo(mainNoti.get(d).getBbsUUID() + BaseUtility.null2Char( mainNoti.get(d).getRowSeq(), String.valueOf(listCnt) ));
					mainNoti.get(d).setHitCount(BaseUtility.toNumberFormat( mainNoti.get(d).getHitCount() ));
					mainNoti.get(d).setCreDate(BaseUtility.toDateFormat( mainNoti.get(d).getCreDate(), dtSep, 8 ));
					mainNoti.get(d).setModDate(BaseUtility.toDateFormat( mainNoti.get(d).getModDate(), dtSep, 8 ));
					mainNoti.get(d).setDelDate(BaseUtility.toDateFormat( mainNoti.get(d).getDelDate(), dtSep, 8 ));

					mainNoti.get(d).setContDesc(mainNoti.get(d).getContDesc());
					mainNoti.get(d).setBbsTitle(("Y".equals(mainNoti.get(d).getCreHide())?hideMark:"") + mainNoti.get(d).getBbsTitle());
					mainNoti.get(d).setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( mainNoti.get(d).getAnsPos() )) <= 0 ? mainNoti.get(d).getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ mainNoti.get(d).getBbsTitle());
					mainNoti.get(d).setContLinkC(BaseUtility.getUriCheck( mainNoti.get(d).getBbsSiteUri() ) ? "T" : "F");
					mainNoti.get(d).setDownExt(FileProcess.getFileLowerExt( mainNoti.get(d).getDownRel() ));
					mainNoti.get(d).setFileUUID(BaseUtility.null2Char(mainNoti.get(d).getFileUUID(), "", "2"+ mainNoti.get(d).getFileUUID() ));
					mainNoti.get(d).setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( mainNoti.get(d).getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
					mainNoti.get(d).setActMode("10150010".equals(mainNoti.get(d).getBbsType()) ? "base/noticeView" : "laws/reportView");
					if( !mainNoti.get(d).getCreUser().equals(mainNoti.get(d).getCreName()) ) mainNoti.get(d).setCreName(propertiesService.getString("creName"));
				}
			}

			// mainRaws : 입법예고 내역 취득
			searchVO = new BaseCommonVO();
			searchVO.setBbsType("10110070");
			searchVO.setFirDate(firDate);
			searchVO.setRowCnt("5");
			searchVO.setReCreHide("N");
			mainRaws = (List<BaseBoardsVO>)boardMngrService.getListOfBoardMain( searchVO );
			// Data Adjustment(Boards)
			if( !(mainRaws == null || mainRaws.size() <= 0) ) {
				for( int d = 0; d < mainRaws.size(); d++ ) {
					mainRaws.get(d).setViewNo(mainRaws.get(d).getBbsUUID() + BaseUtility.null2Char( mainRaws.get(d).getRowSeq(), String.valueOf(d+1) ));
					mainRaws.get(d).setHitCount(BaseUtility.toNumberFormat( mainRaws.get(d).getHitCount() ));
					mainRaws.get(d).setCreDate(BaseUtility.toDateFormat( mainRaws.get(d).getCreDate(), dtSep, 8 ));
					mainRaws.get(d).setModDate(BaseUtility.toDateFormat( mainRaws.get(d).getModDate(), dtSep, 8 ));
					mainRaws.get(d).setDelDate(BaseUtility.toDateFormat( mainRaws.get(d).getDelDate(), dtSep, 8 ));

					mainRaws.get(d).setContDesc(mainRaws.get(d).getContDesc());
					mainRaws.get(d).setBbsTitle(("Y".equals(mainRaws.get(d).getCreHide())?hideMark:"") + mainRaws.get(d).getBbsTitle());
					mainRaws.get(d).setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( mainRaws.get(d).getAnsPos() )) <= 0 ? mainRaws.get(d).getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ mainRaws.get(d).getBbsTitle());
					mainRaws.get(d).setContLinkC(BaseUtility.getUriCheck( mainRaws.get(d).getBbsSiteUri() ) ? "T" : "F");
					mainRaws.get(d).setDownExt(FileProcess.getFileLowerExt( mainRaws.get(d).getDownRel() ));
					mainRaws.get(d).setFileUUID(BaseUtility.null2Char(mainRaws.get(d).getFileUUID(), "", "2"+ mainRaws.get(d).getFileUUID() ));
					mainRaws.get(d).setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( mainRaws.get(d).getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
					mainRaws.get(d).setActMode("laws/reportView");
					if( !mainRaws.get(d).getCreUser().equals(mainRaws.get(d).getCreName()) ) mainRaws.get(d).setCreName(propertiesService.getString("creName"));
				}
			}

//			// mainSchd : 의사일정 내역 취득
//			searchVO = new BaseCommonVO();
//			searchVO.setBbsType("10110110");
//			searchVO.setFirDate(firDate);
//			searchVO.setRowCnt("5");
//			searchVO.setReCreHide("N");
//			mainSchd = (List<BaseBoardsVO>)boardMngrService.getListOfBoardMain( searchVO );
//			// Data Adjustment(Boards)
//			if( !(mainSchd == null || mainSchd.size() <= 0) ) { 
//				for( int d = 0; d < mainSchd.size(); d++ ) { 
//					mainSchd.get(d).setViewNo(mainSchd.get(d).getBbsUUID() + BaseUtility.null2Char( mainSchd.get(d).getRowSeq(), String.valueOf(d+1) ));
//					mainSchd.get(d).setHitCount(BaseUtility.toNumberFormat( mainSchd.get(d).getHitCount() ));
//					mainSchd.get(d).setCreDate(BaseUtility.toDateFormat( mainSchd.get(d).getCreDate(), dtSep, 8 ));
//					mainSchd.get(d).setModDate(BaseUtility.toDateFormat( mainSchd.get(d).getModDate(), dtSep, 8 ));
//					mainSchd.get(d).setDelDate(BaseUtility.toDateFormat( mainSchd.get(d).getDelDate(), dtSep, 8 ));
//
//					mainSchd.get(d).setBbsTitle(("Y".equals(mainSchd.get(d).getCreHide())?hideMark:"") + mainSchd.get(d).getBbsTitle());
//					mainSchd.get(d).setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( mainSchd.get(d).getAnsPos() )) <= 0 ? mainSchd.get(d).getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ mainSchd.get(d).getBbsTitle());
//					mainSchd.get(d).setContLinkC(BaseUtility.getUriCheck( mainSchd.get(d).getBbsSiteUri() ) ? "T" : "F");
//					mainSchd.get(d).setDownExt(FileProcess.getFileLowerExt( mainSchd.get(d).getDownRel() ));
//					mainSchd.get(d).setFileUUID(BaseUtility.null2Char(mainSchd.get(d).getFileUUID(), "", "2"+ mainSchd.get(d).getFileUUID() ));
//					mainSchd.get(d).setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( mainSchd.get(d).getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
//					mainSchd.get(d).setActMode("schd/reportView");
//					if( !mainSchd.get(d).getCreUser().equals(mainSchd.get(d).getCreName()) ) mainSchd.get(d).setCreName(propertiesService.getString("creName"));
//				}
//			}

			// mainPress : 언론보도 내역 취득
			searchVO = new BaseCommonVO();
			searchVO.setBbsType("10110100");
			searchVO.setFirDate(firDate);
			searchVO.setRowCnt("5");
			searchVO.setReCreHide("N");
			mainPress = (List<BaseBoardsVO>)boardMngrService.getListOfBoardMain( searchVO );
			// Data Adjustment(Boards)
			if( !(mainPress == null || mainPress.size() <= 0) ) { 
				for( int d = 0; d < mainPress.size(); d++ ) { 
					mainPress.get(d).setViewNo(mainPress.get(d).getBbsUUID() + BaseUtility.null2Char( mainPress.get(d).getRowSeq(), String.valueOf(d+1) ));
					mainPress.get(d).setHitCount(BaseUtility.toNumberFormat( mainPress.get(d).getHitCount() ));
					mainPress.get(d).setCreDate(BaseUtility.toDateFormat( mainPress.get(d).getCreDate(), dtSep, 8 ));
					mainPress.get(d).setModDate(BaseUtility.toDateFormat( mainPress.get(d).getModDate(), dtSep, 8 ));
					mainPress.get(d).setDelDate(BaseUtility.toDateFormat( mainPress.get(d).getDelDate(), dtSep, 8 ));

					mainPress.get(d).setContDesc(mainPress.get(d).getContDesc());
					mainPress.get(d).setBbsTitle(("Y".equals(mainPress.get(d).getCreHide())?hideMark:"") + mainPress.get(d).getBbsTitle());
					mainPress.get(d).setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( mainPress.get(d).getAnsPos() )) <= 0 ? mainPress.get(d).getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ mainPress.get(d).getBbsTitle());
					mainPress.get(d).setContLinkC(BaseUtility.getUriCheck( mainPress.get(d).getBbsSiteUri() ) ? "T" : "F");
					mainPress.get(d).setDownExt(FileProcess.getFileLowerExt( mainPress.get(d).getDownRel() ));
					mainPress.get(d).setFileUUID(BaseUtility.null2Char(mainPress.get(d).getFileUUID(), "", "2"+ mainPress.get(d).getFileUUID() ));
					mainPress.get(d).setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( mainPress.get(d).getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
					mainPress.get(d).setActMode("press/reportView");
					if( !mainPress.get(d).getCreUser().equals(mainPress.get(d).getCreName()) ) mainPress.get(d).setCreName(propertiesService.getString("creName"));
				}
			}

			// mainMovie : 영상의정활동 내역 취득
			searchVO = new BaseCommonVO();
			searchVO.setBbsType("10110040");
			searchVO.setFirDate(firDate);
			searchVO.setRowCnt("1");
			searchVO.setReCreHide("N");
			mainMovie = (List<BaseBoardsVO>)boardMngrService.getListOfBoardMain( searchVO );
			// Data Adjustment(Boards)
			if( !(mainMovie == null || mainMovie.size() <= 0) ) { 
				for( int d = 0; d < mainMovie.size(); d++ ) { 
					mainMovie.get(d).setViewNo(mainMovie.get(d).getBbsUUID() + BaseUtility.null2Char( mainMovie.get(d).getRowSeq(), String.valueOf(d+1) ));
					mainMovie.get(d).setHitCount(BaseUtility.toNumberFormat( mainMovie.get(d).getHitCount() ));
					mainMovie.get(d).setCreDate(BaseUtility.toDateFormat( mainMovie.get(d).getCreDate(), dtSep, 8 ));
					mainMovie.get(d).setModDate(BaseUtility.toDateFormat( mainMovie.get(d).getModDate(), dtSep, 8 ));
					mainMovie.get(d).setDelDate(BaseUtility.toDateFormat( mainMovie.get(d).getDelDate(), dtSep, 8 ));

					mainMovie.get(d).setBbsTitle(("Y".equals(mainMovie.get(d).getCreHide())?hideMark:"") + mainMovie.get(d).getBbsTitle());
					mainMovie.get(d).setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( mainMovie.get(d).getAnsPos() )) <= 0 ? mainMovie.get(d).getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ mainMovie.get(d).getBbsTitle());
					mainMovie.get(d).setContLinkC(BaseUtility.getUriCheck( mainMovie.get(d).getBbsSiteUri() ) ? "T" : "F");
					mainMovie.get(d).setDownAbs(BaseUtility.null2Blank( mainMovie.get(d).getDownAbs() ).replace("/movie_","/movie/"));
					mainMovie.get(d).setDownExt(FileProcess.getFileLowerExt( mainMovie.get(d).getDownRel() ));
					mainMovie.get(d).setFileUUID(BaseUtility.null2Char(mainMovie.get(d).getFileUUID(), "", "2"+ mainMovie.get(d).getFileUUID() ));
					mainMovie.get(d).setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( mainMovie.get(d).getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
					mainMovie.get(d).setActMode("base/movieList");
					if( !mainMovie.get(d).getCreUser().equals(mainMovie.get(d).getCreName()) ) mainMovie.get(d).setCreName(propertiesService.getString("creName"));
				}
			}

			// mainRecv : 접수의안 내역 취득
			searchVO = new BaseCommonVO();
			searchVO.setRowCnt("5");
			mainRecv = (List<BaseAgendaVO>)agendaMngrService.getMainOfAgendaRecv( searchVO );
			// Data Adjustment(Agendas)
			if( !(mainRecv == null || mainRecv.size() <= 0) ) { 
				for( int d = 0; d < mainRecv.size(); d++ ) { 
					mainRecv.get(d).setViewNo(mainRecv.get(d).getSugUUID() + BaseUtility.null2Char( mainRecv.get(d).getRowSeq(), String.valueOf(d+1) ));
					mainRecv.get(d).setSugDate(BaseUtility.toDateFormat( mainRecv.get(d).getSugDate(), dtSep, 8 ));
					mainRecv.get(d).setActMode("find/agendaView");
				}
			}

			// mainProc : 처리의안 내역 취득
			searchVO = new BaseCommonVO();
			searchVO.setRowCnt("5");
			mainProc = (List<BaseAgendaVO>)agendaMngrService.getMainOfAgendaProc( searchVO );
			// Data Adjustment(Agendas)
			if( !(mainProc == null || mainProc.size() <= 0) ) { 
				for( int d = 0; d < mainProc.size(); d++ ) { 
					mainProc.get(d).setViewNo(mainProc.get(d).getSugUUID() + BaseUtility.null2Char( mainProc.get(d).getRowSeq(), String.valueOf(d+1) ));
					mainProc.get(d).setSugDate(BaseUtility.toDateFormat( mainProc.get(d).getSugDate(), dtSep, 8 ));
					mainProc.get(d).setActMode("find/agendaView");
				}
			}

			// mainPhoto : 포토의정활동 내역 취득
			searchVO = new BaseCommonVO();
			if( isGallery ) { 
				searchVO.setBbsType("1");
				searchVO.setRowCnt("2");
				mainPhoto = (List<BaseBoardsVO>)boardMngrService.getListOfPhotoMain( searchVO );
			} else { 
				searchVO.setBbsType("10110010");
				searchVO.setFirDate(firDate);
				searchVO.setRowCnt("2");
				searchVO.setReCreHide("N");
				mainPhoto = (List<BaseBoardsVO>)boardMngrService.getListOfBoardMain( searchVO );
			}
			// Data Adjustment(Boards)
			if( !(mainPhoto == null || mainPhoto.size() <= 0) ) { 
				for( int d = 0; d < mainPhoto.size(); d++ ) { 
					mainPhoto.get(d).setViewNo(mainPhoto.get(d).getBbsUUID() + BaseUtility.null2Char( mainPhoto.get(d).getRowSeq(), String.valueOf(d+1) ));
					mainPhoto.get(d).setHitCount(BaseUtility.toNumberFormat( mainPhoto.get(d).getHitCount() ));
					mainPhoto.get(d).setCreDate(BaseUtility.toDateFormat( mainPhoto.get(d).getCreDate(), dtSep, 8 ));
					mainPhoto.get(d).setModDate(BaseUtility.toDateFormat( mainPhoto.get(d).getModDate(), dtSep, 8 ));
					mainPhoto.get(d).setDelDate(BaseUtility.toDateFormat( mainPhoto.get(d).getDelDate(), dtSep, 8 ));
					mainPhoto.get(d).setTakeDate(BaseUtility.toDateFormat( mainPhoto.get(d).getTakeDate(), dtSep, 8 ));

					mainPhoto.get(d).setBbsTitle(("Y".equals(mainPhoto.get(d).getCreHide())?hideMark:"") + mainPhoto.get(d).getBbsTitle());
					mainPhoto.get(d).setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( mainPhoto.get(d).getAnsPos() )) <= 0 ? mainPhoto.get(d).getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ mainPhoto.get(d).getBbsTitle());
					mainPhoto.get(d).setContLinkC(BaseUtility.getUriCheck( mainPhoto.get(d).getBbsSiteUri() ) ? "T" : "F");
					mainPhoto.get(d).setDownExt(FileProcess.getFileLowerExt( mainPhoto.get(d).getDownRel() ));
					mainPhoto.get(d).setFileUUID(BaseUtility.null2Char(mainPhoto.get(d).getFileUUID(), "", "2"+ mainPhoto.get(d).getFileUUID() ));
					mainPhoto.get(d).setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( mainPhoto.get(d).getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
					mainPhoto.get(d).setActMode("base/photoView");
					if( !mainPhoto.get(d).getCreUser().equals(mainPhoto.get(d).getCreName()) ) mainPhoto.get(d).setCreName(propertiesService.getString("creName"));
				}
			}

			model.addAttribute("mainImage"     , mainImage);
			model.addAttribute("mainPopup"     , mainPopup);
			model.addAttribute("mainBanner"    , mainBanner);
			model.addAttribute("mainMemb"      , mainMemb);
			model.addAttribute("mainNoti"      , mainNoti);
			model.addAttribute("mainRaws"      , mainRaws);
			model.addAttribute("mainSchd"      , mainSchd);
			model.addAttribute("mainPress"     , mainPress);
			model.addAttribute("mainMovie"     , mainMovie);
			model.addAttribute("mainRecv"      , mainRecv);
			model.addAttribute("mainProc"      , mainProc);
			model.addAttribute("mainPhoto"     , mainPhoto);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/coun/main";
	}	// end of scrInitOfCounMain

}	// end of class