/*******************************************************************************
  Program ID  : ReportCounController
  Description : 클라이언트의 요청(공통)을 처리할 메서드를 구현
  Author      : HexaMedia new0man
  Write Date  : 2019.06.04
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v1.0    (주)헥사미디어 개발자  2014.00.00  최초 생성
  v2.0    HexaMedia new0man      2019.06.04  평택시의회 Site Renewal
  v2.1    HexaMedia new0man      2020.03.17  비공개 목록 표시 및 검색조건 추가
  v2.2    HexaMedia new0man      2020.04.01  일정관리에서 일요일 및 토요일 포함여부
 *******************************************************************************/
package egovframework.ptcc.coun.content.web;

import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.dao.*;
import org.springframework.jdbc.*;
import org.springframework.jdbc.datasource.*;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.*;
import org.springframework.transaction.support.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.*;
import org.springframework.web.servlet.*;
import org.springframework.web.util.WebUtils;
import org.springmodules.validation.commons.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.util.*;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.agenda.service.*;
import egovframework.ptcc.mngr.board.service.*;

@Controller
@RequestMapping(value="/coun")
public class ReportCounController implements HandlerExceptionResolver {

	@Autowired
	private WebApplicationContext			webContext;
	@Autowired
	private DataSourceTransactionManager	txManager;

	@Resource(name="beanValidator")
	protected DefaultBeanValidator			beanValidator;
	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="boardMngrService")
	protected BoardMngrService				boardMngrService;
	@Resource(name="agendaMngrService")
	protected AgendaMngrService			agendaMngrService;
	@Resource(name="reschdMngrService")
	protected ReschdMngrService			reschdMngrService;
	@Resource(name="attendMngrService")
	protected AttendMngrService			attendMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected String						hideMark     = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.HideMark" );
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));
	protected boolean						isGallery    = "Gallery.do".equalsIgnoreCase(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.ViewPage" ));
	protected String						replyImage   = "<img src='@@/images/icon/ico_reply.png' alt=''/>";
	protected String						lnkName      = "PT_BOARD";
	protected String						bbsType      = "10150010";
	protected boolean						notiItems    = false;
	protected boolean						moveItems    = true;
	protected boolean						fileUP       = false;
	protected boolean						tempUP       = false;


	/**
	 * <pre>
	 * 통합검색 결과을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/base/search.do")
	public String scrListOfSearch ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();

		List<String>			bbsTypes     = new ArrayList<String>();
		int[]					totsCnt      = new int[4];
		int						listCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseBoardsVO>		notiList     = new ArrayList<BaseBoardsVO>();
		List<BaseBoardsVO>		dataList     = new ArrayList<BaseBoardsVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		tempList     = new ArrayList<BaseCommonVO>();
		BaseCommonVO			tempView     = new BaseCommonVO();
log.debug(" act: "+ BaseUtility.spaceFill( "Search", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		// 기본 조회조건 취득
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(propertiesService.getInt("pageSize"));
		pageInfo.setTotalRecordCount(1);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getSearchText());
		model.addAttribute("searchText"    , searchVO.getSearchText());

		// 추가 조회조건 취득
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getSearchText(), "%", "%"+ searchVO.getSearchText() +"%" ));

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue() +" |searchText: "+ searchVO.getSearchText());
		try { 
			if( !BaseUtility.isValidNull( searchVO.getSearchText() ) ) { 
				if( BaseUtility.isValidNull( searchVO.getReEraCode() ) ) { 
					// 분류구분 취득 - 현재 회의대수 구분
					tempView = (BaseCommonVO)baseCmmnService.getInfoOfClassNOW( "" );
					searchVO.setReEraCode(tempView == null ? BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsCate" ) : tempView.getEraCode());
					model.addAttribute("reEraCode"     , searchVO.getReEraCode());
log.debug("new eraCode: "+ searchVO.getReEraCode());
				}

//				// 의사일정 [시의회:10110110, 상임위:10110111]
////				bbsTypes = new ArrayList<String>();
////				bbsTypes.add("10110110");
////				bbsTypes.add("10110111");
//				searchVO.setCondName("BASE");
//				searchVO.setBbsType("10110110");
//				searchVO.setBbsTypes(null);
//				searchVO.setReCreHide("N");
//				listCnt = (int)boardMngrService.getCountOfBoard( searchVO );
//				totsCnt[1] += listCnt;
//				model.addAttribute("schdCnt"       , BaseUtility.toNumberFormat( listCnt ));

				// 포토의정활동 [시의회:10110010, 상임위:10110020, 청소년:10110030]
				if( isGallery ) { 
					searchVO.setBbsType("1");
					listCnt = (int)boardMngrService.getCountOfPhoto( searchVO );
				} else { 
//					bbsTypes = new ArrayList<String>();
//					bbsTypes.add("10110010");
//					bbsTypes.add("10110020");
//					bbsTypes.add("10110030");
					searchVO.setCondName("BASE");
					searchVO.setBbsType("10110010");
					searchVO.setBbsTypes(null);
					searchVO.setReCreHide("N");
					listCnt = (int)boardMngrService.getCountOfBoard( searchVO );
				}
				totsCnt[1] += listCnt;
				model.addAttribute("photoCnt"      , BaseUtility.toNumberFormat( listCnt ));

				// 입법예고 [10110070]
				searchVO.setCondName("BASE");
				searchVO.setBbsType("10110070");
				searchVO.setBbsTypes(null);
				searchVO.setReCreHide("N");
				listCnt  = (int)boardMngrService.getCountOfBoard( searchVO );
				totsCnt[1] += listCnt;
				model.addAttribute("lawsCnt"       , BaseUtility.toNumberFormat( listCnt ));

				// 공지사항 [10150010]
				searchVO.setCondName("BASE");
				searchVO.setBbsType("10150010");
				searchVO.setBbsTypes(null);
				searchVO.setReCreHide("N");
				listCnt = (int)boardMngrService.getCountOfBoard( searchVO );
				totsCnt[1] += listCnt;
				model.addAttribute("notiCnt"       , BaseUtility.toNumberFormat( listCnt ));

				// 보도자료 [시의회:10110100, 상임위:10110101]
//				bbsTypes = new ArrayList<String>();
//				bbsTypes.add("10110100");
//				bbsTypes.add("10110101");
				searchVO.setCondName("BASE");
				searchVO.setBbsType("10110100");
				searchVO.setBbsTypes(null);
				searchVO.setReCreHide("N");
				listCnt = (int)boardMngrService.getCountOfBoard( searchVO );
				totsCnt[1] += listCnt;
				model.addAttribute("pressCnt"      , BaseUtility.toNumberFormat( listCnt ));

				// 의회에바란다 [10150060]
				searchVO.setCondName("IMGS");
				searchVO.setBbsType("10150060");
				searchVO.setBbsTypes(null);
				searchVO.setReCreHide("");
				listCnt = (int)boardMngrService.getCountOfBoard( searchVO );
				totsCnt[1] += listCnt;
				model.addAttribute("hopeCnt"       , BaseUtility.toNumberFormat( listCnt ));

				// 회의록
				totsCnt[2] = (int)agendaMngrService.getCountOfConfBase( searchVO );

				// 의안
				totsCnt[3] = (int)agendaMngrService.getCountOfAgendaFind( searchVO );
			}

			model.addAttribute("totsCnt"       , BaseUtility.toNumberFormat( totsCnt[1] + totsCnt[2] + totsCnt[3] ));
			model.addAttribute("boardCnt"      , BaseUtility.toNumberFormat( totsCnt[1] ));
			model.addAttribute("minutesCnt"    , BaseUtility.toNumberFormat( totsCnt[2] ));
			model.addAttribute("agendaCnt"     , BaseUtility.toNumberFormat( totsCnt[3] ));
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return "/coun/base/search_view";
	}	// end of scrListOfSearch


	/**
	 * <pre>
	 * 의정활동을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath1}/{paramPath2}List.do")
	public String scrListOfActivity ( 
			@PathVariable String paramPath1, 
			@PathVariable String paramPath2, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception {

		String webBase = webContext.getServletContext().getContextPath();
		HttpSession session = request == null ? null : request.getSession();
		BaseCommonVO systemVO = (BaseCommonVO) baseCmmnService.getSystemOfDate();
		BaseMemberVO memberVO = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO) UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String retPath = "";

		int listCnt = 0;
		int idxFir = 0;
		int idxLst = 0;
		PaginationInfo pageInfo = new PaginationInfo();
		List<BaseBoardsVO> notiList = new ArrayList<BaseBoardsVO>();
		List<BaseBoardsVO> dataList = new ArrayList<BaseBoardsVO>();
		List<BaseAttendVO> atndList = new ArrayList<BaseAttendVO>();
		List<BaseCommonVO> cateList = new ArrayList<BaseCommonVO>();
		BaseBoardsVO dataView = new BaseBoardsVO();


		String cateNum = request.getParameter("ct");
		System.out.println(">>>>>>>>>>>> paramPath1 : " + paramPath1);
		System.out.println(">>>>>>>>>>>> paramPath2 : " + paramPath2);
		System.out.println(">>>>>>>>>>>> cateNum : " + cateNum);

		// 인터넷방송 게시판별구분
		if ("1".equals(cateNum)) searchVO.setCondCate("MAIN"); // 본회의
		else if ("2".equals(cateNum)) searchVO.setCondCate("OPER"); // 의회운영위원회
		else if ("3".equals(cateNum)) searchVO.setCondCate("GVADM"); // 기획행정위원회
		else if ("4".equals(cateNum)) searchVO.setCondCate("WELFARE"); // 복지환경위원회
		else if ("5".equals(cateNum)) searchVO.setCondCate("BUILD"); // 산업건설위원회
		else if ("6".equals(cateNum)) searchVO.setCondCate("SPEC"); // 특별위원회
		else if ("7".equals(cateNum)) searchVO.setCondCate("GOVER"); // 시정질문
		else if ("8".equals(cateNum)) searchVO.setCondCate("SPCH"); // 7분자유발언

		// 방청신청만 확인
		BaseMemberVO authInfo = new BaseMemberVO();
		if ("visit".equalsIgnoreCase(paramPath2)) {
			// 인증내역 확인
			authInfo.setCertName(BaseUtility.null2Blank((String) session.getAttribute("NMinto")));
			authInfo.setCertInfo(BaseUtility.null2Blank((String) session.getAttribute("DIinto")));
//			// 인증내역이 없으면 인증화면으로 이동
//			if( BaseUtility.isValidNull( authInfo.getCertName() ) && BaseUtility.isValidNull( authInfo.getCertInfo() ) ) { 
//log.debug(" act: "+ BaseUtility.spaceFill( BaseUtility.toCapitalize( paramPath2 ) + BaseUtility.toCapitalize( paramPath1 ) +"List", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
//				WebUtils.setSessionAttribute(request, "searchSess", searchVO);
////				model.addAttribute("message"       , "본인인증 후 사용 가능합니다.\\n\\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
//				model.addAttribute("returnUrl"     , webBase+"/coun/intro/visitAgre.do");
//				return "/cmmn/exe_script";
//			}
			model.addAttribute("authInfo", authInfo);
			log.debug(" act: " + BaseUtility.spaceFill(BaseUtility.toCapitalize(paramPath2) + BaseUtility.toCapitalize(paramPath1) + "List", 24, "R") + " | net: " + BaseUtility.spaceFill(BaseUtility.reqIPv4Address(request), 16, "R") + " |user: " + authInfo.getCertName());
		} else {
			log.debug(" act: " + BaseUtility.spaceFill(BaseUtility.toCapitalize(paramPath2) + BaseUtility.toCapitalize(paramPath1) + "List", 24, "R") + " | net: " + BaseUtility.spaceFill(BaseUtility.reqIPv4Address(request), 16, "R") + " |user: " + memberVO.getUserPID());
		}

		if( "stand".equalsIgnoreCase(paramPath1) && "movie".equalsIgnoreCase(paramPath2) ) {
			retPath = "acts/" + paramPath2.toLowerCase() + BaseUtility.toCapitalize(paramPath1) + "_list";
		}else if( "base".equalsIgnoreCase(paramPath1) && !"".equals(paramPath2) ) {
			retPath  = "acts/"+ paramPath2.toLowerCase() + BaseUtility.toCapitalize( paramPath1 ) +"_list";
		} else if( "intro".equalsIgnoreCase(paramPath1) && !"".equals(paramPath2) ) { 
			retPath  = "acts/"+ paramPath2.toLowerCase() + BaseUtility.toCapitalize( paramPath1 ) +"_list";
		} else if( "coun".equalsIgnoreCase(paramPath1) && !"".equals(paramPath2) ) {
			retPath = "acts/" + paramPath2.toLowerCase() + BaseUtility.toCapitalize(paramPath1) + "_list";
		}
		if( "stand".equalsIgnoreCase(paramPath1) && "movie".equalsIgnoreCase(paramPath2) ) {
			bbsType = "10110050";
		} else if( "base".equalsIgnoreCase(paramPath2) ) {
			bbsType = "";
		} else if( "photo".equalsIgnoreCase(paramPath2) ) {
			if( isGallery ) { 
				if( "base".equalsIgnoreCase(paramPath1) ) 
					bbsType  = "1";
				else if( "intro".equalsIgnoreCase(paramPath1) ) 
					bbsType  = "47";
			} else { 
				bbsType  = "10110010";
			}
		} else if( "movie".equalsIgnoreCase(paramPath2) ) { 
			bbsType  = "10110040";
		} else if( "former".equalsIgnoreCase(paramPath2) ) { 
			bbsType  = "10110041";
		} else if( "visit".equalsIgnoreCase(paramPath2) ) { 
			bbsType  = "";
		}
		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(propertiesService.getInt("pageSize"));
		pageInfo.setTotalRecordCount(1);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), "photo".equalsIgnoreCase(paramPath2) ? propertiesService.getString("imgsUnit") : propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));
		searchVO.setCondCate("visit".equalsIgnoreCase(paramPath2)?"OPEN":searchVO.getCondCate());

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("bgnScDate"     , searchVO.getBgnScDate());
		model.addAttribute("endScDate"     , searchVO.getEndScDate());
		model.addAttribute("reRndCode"     , searchVO.getReRndCode());

		// 추가 - 21.02.02
		if(searchVO.getBgnScDate() != null) searchVO.setBgnScDate(searchVO.getBgnScDate().replaceAll("[.]", ""));
		if(searchVO.getEndScDate() != null) searchVO.setEndScDate(searchVO.getEndScDate().replaceAll("[.]", ""));

		System.out.println(">>>>>>>>>>>> getBgnScDate : " + searchVO.getBgnScDate());
		System.out.println(">>>>>>>>>>>> getEndScDate : " + searchVO.getEndScDate());

		// 추가 조회조건 취득
		if( isGallery && "photo".equalsIgnoreCase(paramPath2) ) searchVO.setSearchText(BaseUtility.null2Blank( searchVO.getCondValue() ));
		if( "visit".equalsIgnoreCase(paramPath2) ) { 
			searchVO.setRultCode("COUN");
			searchVO.setCondCate("OPEN");
			if( BaseUtility.isValidNull( authInfo.getCertName() ) && BaseUtility.isValidNull( authInfo.getCertInfo() ) ) { 
				searchVO.setCondName("BASE");
			} else { 
				searchVO.setCondName("CERT");
				searchVO.setCreName(authInfo.getCertName());
				searchVO.setCreCert(authInfo.getCertInfo());
			}
		} else { 
			searchVO.setCondName("BASE");
		}
		searchVO.setBbsType(BaseUtility.null2Char( searchVO.getBbsType(), bbsType ));
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReCreHide("N");

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("bbsType: "+ searchVO.getBbsType());
		try { 
			if( isGallery && "photo".equalsIgnoreCase(paramPath2) ) { 
				listCnt = (int)boardMngrService.getCountOfPhoto( searchVO );
			} else { 
				if( "visit".equalsIgnoreCase(paramPath2) ) { 
					listCnt = (int)attendMngrService.getCountOfAttend( searchVO );
				} else { 
					listCnt = (int)boardMngrService.getCountOfBoard( searchVO );
				}
			}

			// 조회 건수를 전체선택인 경우
			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
			}
			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
			searchVO.setPageFirNo(Integer.toString(idxFir));
			searchVO.setPageLstNo(Integer.toString(idxLst));
			searchVO.setListCnt(Integer.toString(listCnt));

			if( listCnt > 0 ) { 
				if( isGallery && "photo".equalsIgnoreCase(paramPath2) ) { 
					dataList = (List<BaseBoardsVO>)boardMngrService.getListOfPhoto( searchVO );
//					dataList = (List<BaseBoardsVO>)boardMngrService.getListOfPhoto( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				} else { 
					if( "visit".equalsIgnoreCase(paramPath2) ) { 
						atndList = (List<BaseAttendVO>)attendMngrService.getListOfAttend( searchVO );
//						atndList = (List<BaseAttendVO>)attendMngrService.getListOfAttend( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
					} else { 
						dataList = (List<BaseBoardsVO>)boardMngrService.getListOfBoard( searchVO );
//						dataList = (List<BaseBoardsVO>)boardMngrService.getListOfBoard( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
					}
				}
				// Data Adjustment(Boards)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					for( int d = 0; d < dataList.size(); d++ ) { 
						dataList.get(d).setViewNo(dataList.get(d).getBbsUUID() + dataList.get(d).getRowSeq());

						dataList.get(d).setBbsFiles(BaseUtility.toNumberFormat( dataList.get(d).getBbsFiles() ));
						dataList.get(d).setBgnDate(BaseUtility.toDateFormat( dataList.get(d).getBgnDate(), dtSep, 8 ));
						dataList.get(d).setEndDate(BaseUtility.toDateFormat( dataList.get(d).getEndDate(), dtSep, 8 ));
						dataList.get(d).setHitCount(BaseUtility.toNumberFormat( dataList.get(d).getHitCount() ));
						dataList.get(d).setCreDate(BaseUtility.toDateFormat( dataList.get(d).getCreDate(), dtSep, 8 ));
						dataList.get(d).setModDate(BaseUtility.toDateFormat( dataList.get(d).getModDate(), dtSep, 8 ));
						dataList.get(d).setDelDate(BaseUtility.toDateFormat( dataList.get(d).getDelDate(), dtSep, 8 ));
						dataList.get(d).setTakeDate(BaseUtility.toDateFormat( dataList.get(d).getTakeDate(), dtSep, 8 ));

						dataList.get(d).setBbsTitle(("Y".equals(dataList.get(d).getCreHide())?hideMark:"") + dataList.get(d).getBbsTitle());
						dataList.get(d).setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( dataList.get(d).getAnsPos() )) <= 0 ? dataList.get(d).getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ dataList.get(d).getBbsTitle());
						dataList.get(d).setContDescBR(BaseUtility.tag2Sym( dataList.get(d).getContDesc() ));
						dataList.get(d).setContDescCR(BaseUtility.sym2Tag( dataList.get(d).getContDesc() ));
						dataList.get(d).setContLinkC(BaseUtility.getUriCheck( dataList.get(d).getBbsSiteUri() ) ? "T" : "F");
						dataList.get(d).setCreLock(BaseUtility.null2Char( dataList.get(d).getCrePasswd(), "", "Y" ));
						dataList.get(d).setDownAbs(BaseUtility.null2Blank( dataList.get(d).getDownAbs() ).replace("/movie_","/movie/"));
						dataList.get(d).setDownExt(FileProcess.getFileLowerExt( dataList.get(d).getDownRel() ));
						dataList.get(d).setFileUUID(BaseUtility.null2Char(dataList.get(d).getFileUUID(), "", "2"+ dataList.get(d).getFileUUID() ));
						dataList.get(d).setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( dataList.get(d).getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
						dataList.get(d).setRegiDate(BaseUtility.toDateFormat( BaseUtility.null2Char( dataList.get(d).getRegiDate(), dataList.get(d).getCreDate() ), dtSep, 8 ));
						if( !dataList.get(d).getCreUser().equals(dataList.get(d).getCreName()) ) dataList.get(d).setCreName(propertiesService.getString("creName"));

						// 상임위원회 구분
						if( "stand".equalsIgnoreCase(paramPath2) && BaseUtility.isValidNull( dataList.get(d).getBbsCateName() ) ) { 
							if( "OPER".equals(dataList.get(d).getBbsCate())  ) dataList.get(d).setBbsCateName("의회운영위원회");
							if( "GVADM".equals(dataList.get(d).getBbsCate()) ) dataList.get(d).setBbsCateName("기획행정위원회");
							if( "WELFARE".equals(dataList.get(d).getBbsCate()) ) dataList.get(d).setBbsCateName("복지환경위원회");
							if( "BUILD".equals(dataList.get(d).getBbsCate()) ) dataList.get(d).setBbsCateName("산업건설위원회");

							if( "MAIN".equals(dataList.get(d).getBbsCate()) ) dataList.get(d).setBbsCateName("본회의");
							if( "SPEC".equals(dataList.get(d).getBbsCate()) ) dataList.get(d).setBbsCateName("특별위원회");
							if( "GOVER".equals(dataList.get(d).getBbsCate()) ) dataList.get(d).setBbsCateName("시정질문");
							if( "SPCH".equals(dataList.get(d).getBbsCate()) ) dataList.get(d).setBbsCateName("7분자유발언");
						}

						// 영상인경우만 처리
						if( "movie".equalsIgnoreCase(paramPath2) ) { 
							if( BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
//								if( d == 0 ) {
//									dataView = dataList.get(d);
//									boardMngrService.doRtnOfModifyBoardHit( dataView );
//									dataList.get(d).setHitCount(BaseUtility.toNumberFormat( Integer.parseInt(BaseUtility.null2Zero( dataList.get(d).getHitCount() )) + 1 ));
//								}
							} else { 
								if( dataList.get(d).getViewNo().equals(searchVO.getViewNo()) ) { 
									dataView = dataList.get(d);
									boardMngrService.doRtnOfModifyBoardHit( dataView );
									dataList.get(d).setHitCount(BaseUtility.toNumberFormat( Integer.parseInt(BaseUtility.null2Zero( dataList.get(d).getHitCount() )) + 1 ));
								}
							}
						}
					}
				}
				// Data Adjustment(Attends)
				if( !(atndList == null || atndList.size() <= 0) ) { 
					for( int d = 0; d < atndList.size(); d++ ) { 
						atndList.get(d).setViewNo(atndList.get(d).getReqUUID() + atndList.get(d).getRowSeq());

						atndList.get(d).setReqDate(BaseUtility.toDateFormat( atndList.get(d).getReqDate(), dtSep, 8 ));
						atndList.get(d).setVstDate(BaseUtility.toDateFormat( atndList.get(d).getVstDate(), dtSep, 12 ));
						atndList.get(d).setCreDate(BaseUtility.toDateFormat( atndList.get(d).getCreDate(), dtSep, 8 ));
						atndList.get(d).setModDate(BaseUtility.toDateFormat( atndList.get(d).getModDate(), dtSep, 8 ));
						atndList.get(d).setDelDate(BaseUtility.toDateFormat( atndList.get(d).getDelDate(), dtSep, 8 ));

						atndList.get(d).setVstDescBR(BaseUtility.tag2Sym( atndList.get(d).getVstDesc() ));
						atndList.get(d).setVstDescCR(BaseUtility.sym2Tag( atndList.get(d).getVstDesc() ));
						atndList.get(d).setDownExt(FileProcess.getFileLowerExt( atndList.get(d).getDownRel() ));
						atndList.get(d).setFileUUID(BaseUtility.null2Char(atndList.get(d).getFileUUID(), "", "2"+ atndList.get(d).getFileUUID() ));
						atndList.get(d).setFontColor("G".equals(atndList.get(d).getUseAccept())?" style='color:rgb(64,64,64);'":atndList.get(d).getFontColor());	// 접수
						atndList.get(d).setFontColor("Y".equals(atndList.get(d).getUseAccept())?" style='color:rgb(0,153,0);'":atndList.get(d).getFontColor());	// 접수대기
						atndList.get(d).setFontColor("B".equals(atndList.get(d).getUseAccept())?" style='color:rgb(0,0,255);'":atndList.get(d).getFontColor());	// 승인
						atndList.get(d).setFontColor("R".equals(atndList.get(d).getUseAccept())?" style='color:rgb(255,0,0);'":atndList.get(d).getFontColor());	// 승인불가
						atndList.get(d).setFontColor("C".equals(atndList.get(d).getUseAccept())?"":atndList.get(d).getFontColor());	// 취소
						atndList.get(d).setReqTypeName(BaseUtility.null2Blank( atndList.get(d).getReqTypeName() ).split("[_]",2)[1]);
						if( !"CERT".equals(searchVO.getCondName()) ) 
							atndList.get(d).setCreName(BaseUtility.null2Blank( atndList.get(d).getCreName() ).substring(0,1)+"○○");
					}
				}

				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
				pageInfo.setTotalRecordCount(listCnt);
			}

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			if( "visit".equalsIgnoreCase(paramPath2) ) { 
				model.addAttribute("dataList"      , atndList);
				model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (atndList == null ? 0 : atndList.size()) : listCnt ));
			} else { 
				model.addAttribute("dataList"      , dataList);
				model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (dataList == null ? 0 : dataList.size()) : listCnt ));
			}
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("notiList"      , notiList);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("dataView"      , dataView);
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/coun/"+retPath;
	}	// end of scrListOfActivity

	/**
	 * <pre>
	 * 의정활동을(를) 상세조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath1}/{paramPath2}View.do")
	public String scrViewOfActivity ( 
			@PathVariable String paramPath1, 
			@PathVariable String paramPath2, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					retPath      = "";

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		int						chldIndx     = 0;
		BaseBoardsVO			dataView     = new BaseBoardsVO();
		BaseBoardsVO			prevView     = new BaseBoardsVO();
		BaseBoardsVO			nextView     = new BaseBoardsVO();
		BaseCommonVO			findFile     = new BaseCommonVO();
		List<BaseFilesVO>		dataFile     = new ArrayList<BaseFilesVO>();
		List<BaseFilesVO>		thmbFile     = new ArrayList<BaseFilesVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		dataSupp     = new ArrayList<BaseCommonVO>();
		List<BaseBoardsVO>		chldFile     = new ArrayList<BaseBoardsVO>();
		BaseBoardsVO			tempSupp     = new BaseBoardsVO();
		BaseBoardsVO			tempView     = new BaseBoardsVO();
		String[][]				tempList     = new String[4][10];
log.debug(" act: "+ BaseUtility.spaceFill( BaseUtility.toCapitalize( paramPath2 ) + BaseUtility.toCapitalize( paramPath1 ) +"View", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "base".equalsIgnoreCase(paramPath1) && !"".equals(paramPath2) ) { 
			retPath  = "acts/"+ paramPath2.toLowerCase() + BaseUtility.toCapitalize( paramPath1 ) +"_view";
		} else if( "intro".equalsIgnoreCase(paramPath1) && !"".equals(paramPath2) ) { 
			retPath  = "acts/"+ paramPath2.toLowerCase() + BaseUtility.toCapitalize( paramPath1 ) +"_view";
		} else if( "coun".equalsIgnoreCase(paramPath1) && !"".equals(paramPath2) ) { 
			retPath  = "acts/"+ paramPath2.toLowerCase() + BaseUtility.toCapitalize( paramPath1 ) +"_view";
		}
		if( "base".equalsIgnoreCase(paramPath2) ) { 
			bbsType  = "10150010";
		} else if( "photo".equalsIgnoreCase(paramPath2) ) { 
			if( isGallery ) { 
				if( "base".equalsIgnoreCase(paramPath1) ) 
					bbsType  = "1";
				else if( "intro".equalsIgnoreCase(paramPath1) ) 
					bbsType  = "47";
			} else { 
				bbsType  = "10110010";
			}
		} else if( "movie".equalsIgnoreCase(paramPath2) ) { 
			bbsType  = "10110040";
		} else if( "former".equalsIgnoreCase(paramPath2) ) { 
			bbsType  = "10110041";
		}
		if( "".equals(retPath) || BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), "photo".equalsIgnoreCase(paramPath2) ? propertiesService.getString("imgsUnit") : propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		if( isGallery && "photo".equalsIgnoreCase(paramPath2) ) searchVO.setSearchText(BaseUtility.null2Blank( searchVO.getCondValue() ));
		searchVO.setCondName("BASE");
		searchVO.setBbsType(BaseUtility.null2Char( searchVO.getBbsType(), bbsType ));
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));
		searchVO.setReCreHide("N");

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				if( isGallery && "photo".equalsIgnoreCase(paramPath2) ) { 
					viewNo = BaseUtility.null2Char( (String)boardMngrService.getInfoOfPhotoUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);
				} else { 
					viewNo = BaseUtility.null2Char( (String)boardMngrService.getInfoOfBoardUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);
				}

				// 입력내역 취득(게시판)
				searchVO.setBbsType(viewNo[0]);
				searchVO.setBbsSeq(viewNo[1]);
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());

				if( isGallery && "photo".equalsIgnoreCase(paramPath2) ) { 
					dataView = (BaseBoardsVO)boardMngrService.getDetailOfPhoto( searchVO );
				} else { 
					dataView = (BaseBoardsVO)boardMngrService.getDetailOfBoard( searchVO );
				}
				if( dataView != null ) { 
					if( isGallery && "photo".equalsIgnoreCase(paramPath2) ) { 
						boardMngrService.doRtnOfModifyPhotoHit( dataView );
					} else { 
						boardMngrService.doRtnOfModifyBoardHit( dataView );

						findFile.setLnkName(lnkName);
						findFile.setLnkKey(viewNo[0] + viewNo[1]);
						// Data Adjustment(Data Files)
						dataFile = (List<BaseFilesVO>)baseCmmnService.getListOfDataFile( findFile );
						// Data Adjustment(Thumb Files)
						thmbFile = (List<BaseFilesVO>)baseCmmnService.getListOfThmbFile( findFile );
						if( !(thmbFile == null || thmbFile.size() <= 0) ) { 
							for( int f = 0; f < thmbFile.size(); f++ ) { 
								thmbFile.get(f).setFileImage(BaseUtility.getFileAllowOfImage( thmbFile.get(f).getFileAbs() ) ? "T" : null);
							}
						}
					}

					// Data Adjustment(Boards)
					dataView.setActMode("Update");
					dataView.setViewNo(dataView.getBbsUUID());

					dataView.setBbsFiles(BaseUtility.toNumberFormat( dataView.getBbsFiles() ));
					dataView.setBgnDate(BaseUtility.toDateFormat( dataView.getBgnDate(), dtSep, 8 ));
					dataView.setEndDate(BaseUtility.toDateFormat( dataView.getEndDate(), dtSep, 8 ));
					dataView.setHitCount(BaseUtility.toNumberFormat( Integer.parseInt(BaseUtility.null2Zero( dataView.getHitCount() )) + 1 ));
					dataView.setCreDate(BaseUtility.toDateFormat( dataView.getCreDate(), dtSep, 8 ));
					dataView.setModDate(BaseUtility.toDateFormat( dataView.getModDate(), dtSep, 8 ));
					dataView.setDelDate(BaseUtility.toDateFormat( dataView.getDelDate(), dtSep, 8 ));
					dataView.setTakeDate(BaseUtility.toDateFormat( dataView.getTakeDate(), dtSep, 8 ));

					dataView.setBbsTitle(("Y".equals(dataView.getCreHide())?hideMark:"") + dataView.getBbsTitle());
					dataView.setContDescBR(BaseUtility.tag2Sym( dataView.getContDesc() ));
					dataView.setContDescCR(BaseUtility.sym2Tag( dataView.getContDesc() ));
					dataView.setContLinkC(BaseUtility.getUriCheck( dataView.getBbsSiteUri() ) ? "T" : "F");
					dataView.setCreLock(BaseUtility.null2Char( dataView.getCrePasswd(), "", "Y" ));
					dataView.setDownAbs(BaseUtility.null2Blank( dataView.getDownAbs() ).replace("/movie_","/movie/"));
					dataView.setDownExt(FileProcess.getFileLowerExt( dataView.getDownRel() ));
					dataView.setFileUUID(BaseUtility.null2Char(dataView.getFileUUID(), "", "6"+ dataView.getFileUUID() ));
					dataView.setFileLinkS(FileProcess.getInfoOfFileBBS( request, response, dataFile, "S" ));
					dataView.setFileLinkM(FileProcess.getInfoOfFileBBS( request, response, dataFile, "M" ));
					dataView.setRegiDate(BaseUtility.toDateFormat( BaseUtility.null2Char( dataView.getRegiDate(), dataView.getCreDate() ), dtSep, 8 ));
					if( !dataView.getCreUser().equals(dataView.getCreName()) ) dataView.setCreName(propertiesService.getString("creName"));

					// 추가게시판 내역
					if( isGallery && "photo".equalsIgnoreCase(paramPath2) ) { 
						chldFile = (List<BaseBoardsVO>)boardMngrService.getChldOfPhoto( searchVO );
						if( !(chldFile == null || chldFile.size() <= 0) ) for( int f = 0; f < chldFile.size(); f++ ) if( dataView.getBbsUUID().equals(chldFile.get(f).getBbsUUID()) ) chldIndx = Integer.parseInt(BaseUtility.null2Zero( chldFile.get(f).getRowSeq() ))-1;
					} else { 
						tempSupp = (BaseBoardsVO)boardMngrService.getDetailOfAdder( searchVO );
						if( tempSupp != null ) { 
							dataView.setConfCode1(tempSupp.getConfCode1());
							dataView.setConfUUID1(BaseCategory.encryptSHA256( tempSupp.getConfCode1() ));
							dataView.setConfName1((String)baseCmmnService.getInfoOfConfName( tempSupp.getConfCode1() ));
							dataView.setRegiDate1(BaseUtility.toDateFormat( tempSupp.getRegDate1(), dtSep, 8 ));

							tempList[0] = BaseUtility.isValidNull( tempSupp.getConfCode2() ) ? null : BaseUtility.null2Blank( tempSupp.getConfCode2() ).split("[◆]");
							tempList[1] = BaseUtility.isValidNull( tempSupp.getRegDate2()  ) ? null : BaseUtility.null2Blank( tempSupp.getRegDate2() ).split("[◆]");
							tempList[2] = BaseUtility.isValidNull( tempSupp.getRegName2()  ) ? null : BaseUtility.null2Blank( tempSupp.getRegName2() ).split("[◆]");
							tempList[3] = BaseUtility.isValidNull( tempSupp.getContent2()  ) ? null : BaseUtility.null2Blank( tempSupp.getContent2() ).split("[◆]");

							if( !(tempList[2] == null || tempList[2].length <= 0) ) { 
								tempList[0] = tempList[0] == null ? new String[tempList[2].length] : tempList[0];
								tempList[1] = tempList[1] == null ? new String[tempList[2].length] : tempList[1];
								tempList[3] = tempList[3] == null ? new String[tempList[2].length] : tempList[3];
								for( int i = 0; i < tempList[2].length; i++ ) { 
									tempView = new BaseBoardsVO();
									tempView.setConfCode2(BaseUtility.null2Blank( tempList[0][i] ));
									tempView.setConfUUID2(BaseCategory.encryptSHA256( tempList[0][i] ));
									tempView.setConfName2((String)baseCmmnService.getInfoOfConfName( tempList[0][i] ));
									tempView.setRegiDate2(BaseUtility.toDateFormat( tempList[1][i], dtSep, 8 ));
									tempView.setRegiName2(BaseUtility.null2Blank( tempList[2][i] ));
									tempView.setContents2(BaseUtility.null2Blank( tempList[3][i] ));
									tempView.setContents2BR(BaseUtility.tag2Sym( tempList[3][i] ));
									tempView.setContents2CR(BaseUtility.sym2Tag( tempList[3][i] ));
									dataSupp.add(tempView);
								}
							}
						}
					}

					// 상임위원회 구분
					if( "stand".equalsIgnoreCase(paramPath2) && BaseUtility.isValidNull( dataView.getBbsCateName() ) ) { 
						if( "OPER".equals(dataView.getBbsCate())  ) dataView.setBbsCateName("의회운영위원회");
						if( "GVADM".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("기획행정위원회");
						if( "WELFARE".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("복지환경위원회");
						if( "BUILD".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("산업건설위원회");

						if( "MAIN".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("본회의");
						if( "SPEC".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("특별위원회");
						if( "GOVER".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("시정질문");
						if( "SPCH".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("7분자유발언");
					}

					// Data Adjustment(Move Items)
					if( moveItems && !BaseUtility.isValidNull( searchVO.getRowSeq() ) ) { 
						searchVO.setRowSeq(String.valueOf(Integer.parseInt(searchVO.getRowSeq()) + 1));
						if( isGallery && "photo".equalsIgnoreCase(paramPath2) ) { 
							searchVO.setBbsType(bbsType);
							prevView = (BaseBoardsVO)boardMngrService.getMovingOfPhoto( searchVO );
						} else { 
							prevView = (BaseBoardsVO)boardMngrService.getMovingOfBoard( searchVO );
						}
						if( prevView != null ) { 
							prevView.setViewNo(prevView.getBbsUUID() + prevView.getRowSeq());
							prevView.setBbsTitle(("Y".equals(prevView.getCreHide())?hideMark:"") + prevView.getBbsTitle());
							prevView.setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( prevView.getAnsPos() )) <= 0 ? prevView.getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ prevView.getBbsTitle());
							prevView.setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( prevView.getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
							if( !prevView.getCreUser().equals(prevView.getCreName()) ) prevView.setCreName(propertiesService.getString("creName"));
						}
						searchVO.setRowSeq(String.valueOf(Integer.parseInt(searchVO.getRowSeq()) - 2));
						if( isGallery && "photo".equalsIgnoreCase(paramPath2) ) { 
							searchVO.setBbsType(bbsType);
							nextView = (BaseBoardsVO)boardMngrService.getMovingOfPhoto( searchVO );
						} else { 
							nextView = (BaseBoardsVO)boardMngrService.getMovingOfBoard( searchVO );
						}
						if( nextView != null ) { 
							nextView.setViewNo(nextView.getBbsUUID() + nextView.getRowSeq());
							nextView.setBbsTitle(("Y".equals(nextView.getCreHide())?hideMark:"") + nextView.getBbsTitle());
							nextView.setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( nextView.getAnsPos() )) <= 0 ? nextView.getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ nextView.getBbsTitle());
							nextView.setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( nextView.getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
							if( !nextView.getCreUser().equals(nextView.getCreName()) ) nextView.setCreName(propertiesService.getString("creName"));
						}
					}
				}
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("dataFile"      , dataFile);
			model.addAttribute("thmbFile"      , thmbFile);
			model.addAttribute("moveItem"      , moveItems?"T":"F");
			model.addAttribute("prevView"      , prevView);
			model.addAttribute("nextView"      , nextView);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("dataSupp"      , dataSupp);
			model.addAttribute("chldFile"      , chldFile);
			model.addAttribute("chldShow"      , chldIndx<0?0:chldIndx);
			model.addAttribute("viewLink"      , webBase +"/player/"+ searchVO.getRowUUID() + bbsType +".do");
//			model.addAttribute("viewLink"      , webBase +"/player/"+ searchVO.getRowUUID() +"c.do");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/coun/"+retPath;
	}	// end of scrViewOfActivity

	/**
	 * <pre>
	 * 의정활동을(를) 상세조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/mediaView.do")
	public String scrAjaxOfActivity ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		boolean				vodMode      = "Direct.do".equalsIgnoreCase(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.ViewMode" ));
		String					viewPort     = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.ViewPort" );

		boolean				actMode      = "true".equals(BaseUtility.null2Blank( searchVO.getActMode() ));
		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		BaseBoardsVO			dataView     = new BaseBoardsVO();
		StringBuffer			dataRult     = new StringBuffer();
log.debug(" act: "+ BaseUtility.spaceFill( "MediaCounView", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

//		if( "base".equalsIgnoreCase(paramPath) ) { 
//			bbsType  = "10150010";
//		} else if( "photo".equalsIgnoreCase(paramPath) ) { 
//			bbsType  = "10110010";
//		} else if( "movie".equalsIgnoreCase(paramPath) ) { 
//			bbsType  = "10110040";
//		} else if( "former".equalsIgnoreCase(paramPath) ) { 
//			bbsType  = "10110041";
//		}

		if( BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 추가 조회조건 취득
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)boardMngrService.getInfoOfBoardUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(게시판)
				searchVO.setBbsType(viewNo[0]);
				searchVO.setBbsSeq(viewNo[1]);
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());

				dataView = (BaseBoardsVO)boardMngrService.getDetailOfBoard( searchVO );
				if( dataView != null ) { 
					boardMngrService.doRtnOfModifyBoardHit( dataView );

					// Data Adjustment(Boards)
					dataView.setActMode("Update");
					dataView.setViewNo(dataView.getBbsUUID());

					dataView.setCreDate(BaseUtility.toDateFormat( dataView.getCreDate(), dtSep, 8 ));
					dataView.setModDate(BaseUtility.toDateFormat( dataView.getModDate(), dtSep, 8 ));
					dataView.setDelDate(BaseUtility.toDateFormat( dataView.getDelDate(), dtSep, 8 ));
					dataView.setTakeDate(BaseUtility.toDateFormat( dataView.getTakeDate(), dtSep, 8 ));

					dataView.setDownAbs(BaseUtility.null2Blank( dataView.getDownAbs() ).replace("/movie_","/movie/"));
					dataView.setDownExt(FileProcess.getFileLowerExt( dataView.getDownRel() ));
					dataView.setFileUUID(BaseUtility.null2Char(dataView.getFileUUID(), "", "6"+ dataView.getFileUUID() ));
					dataView.setRegiDate(BaseUtility.toDateFormat( BaseUtility.null2Char( dataView.getRegiDate(), dataView.getCreDate() ), dtSep, 8 ));
					if( !dataView.getCreUser().equals(dataView.getCreName()) ) dataView.setCreName(propertiesService.getString("creName"));

					dataRult.append("<h1>"+ dataView.getBbsTitle() +"</h1>");
					dataRult.append("<div class='layer_media'>");
					dataRult.append("<video id='vod' oncontextmenu='return false;' width='100%' controls autoplay preload='auto'>");
					if( vodMode || actMode ) 
						dataRult.append("<source src='"+ webBase +"/GetMovie.do?key="+ dataView.getDownUUID() +"' type='video/"+ dataView.getDownExt() +"'/>");
					else 
						dataRult.append("<source src='"+ viewPort + webBase + dataView.getDownAbs() +"' type='video/"+ dataView.getDownExt() +"'/>");
					dataRult.append("</video>");
					dataRult.append("</div>");
					dataRult.append("<div class='close_btn'><a href='javascript:void(0);' class='btn_c'><img src='"+ webBase +"/images/coun/popup_close.png' alt='닫기'/></a></div>");
//				} else { 
//					dataRult.append("<h1></h1>");
//					dataRult.append("<div class='layer_media'>");
//					dataRult.append("<video id='vod' oncontextmenu='return false;' width='100%' controls autoplay preload='auto'>");
//					dataRult.append("<source src='' type=''/>");
//					dataRult.append("</video>");
//					dataRult.append("</div>");
//					dataRult.append("<div class='close_btn'><a href='javascript:void(0);' class='btn_c'><img src='"+ webBase +"/images/coun/popup_close.png' alt='닫기'/></a></div>");
				}
			}

log.debug(dataRult.toString());
			response.setContentType("text/html;charset=utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(dataRult.toString());
			response.getWriter().flush();
			response.getWriter().close();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return null;
	}	// end of scrAjaxOfActivity


	/**
	 * <pre>
	 * 방청신청 - 개인정보 수집화면으로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/intro/visitAgre.do")
	public String scrAgreOfAttend ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		BaseCommonVO			searchSess   = (BaseCommonVO)WebUtils.getSessionAttribute(request, "searchSess");
		String					retPath      = "acts/visitIntro_agre";

		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
log.debug(" act: "+ BaseUtility.spaceFill( "VisitIntroAgre", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 세션의 기본 조회조건 취득
		if( !BaseUtility.isValidNull( searchSess ) ) { 
			searchVO = searchSess;
			session.removeAttribute("searchSess");
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reStdStat"     , BaseUtility.null2Char( searchVO.getReStdStat(), "C" ));
		model.addAttribute("reStdDate"     , BaseUtility.toDateFormat( searchVO.getReStdDate(), dtSep ));
		model.addAttribute("reCurrDate"    , BaseUtility.toDateFormat( systemVO.getToYear() + systemVO.getToMonth(), dtSep ));
		model.addAttribute("reqDate"       , BaseUtility.toDateFormat( searchVO.getReqDate(), dtSep ));
		if( BaseUtility.isValidNull( searchVO.getNextMode() ) ) { 
log.debug("REFERER :"+ request.getHeader("REFERER"));
			if( request.getHeader("REFERER").contains("List") ) { 
				model.addAttribute("nextMode"      , "List");
			} else if( request.getHeader("REFERER").contains("View") ) { 
				model.addAttribute("nextMode"      , "View");
			} else if( request.getHeader("REFERER").contains("Inst") ) { 
				model.addAttribute("nextMode"      , "Inst");
			} else if( request.getHeader("REFERER").contains("Edit") ) { 
				model.addAttribute("nextMode"      , "Edit");
			} else if( request.getHeader("REFERER").contains("Requ") ) { 
				model.addAttribute("nextMode"      , "Requ");
			} else if( request.getHeader("REFERER").contains("Create") ) { 
				model.addAttribute("nextMode"      , "List");
			} else if( request.getHeader("REFERER").contains("Update") ) { 
				model.addAttribute("nextMode"      , "List");
			} else if( request.getHeader("REFERER").contains("Delete") ) { 
				model.addAttribute("nextMode"      , "View");
			} else if( request.getHeader("REFERER").contains("Modify") ) { 
				model.addAttribute("nextMode"      , "View");
			} else { 
				model.addAttribute("nextMode"      , "List");
			}
		} else { 
			model.addAttribute("nextMode"      , searchVO.getNextMode());
		}
log.debug("nextMode: "+ (String)model.get("nextMode"));
log.debug("retPath: " + retPath);

		model.addAttribute("certTest"      , BaseUtility.null2Char( propertiesService.getString("certTest"), "F" ));
		if( "T".equals(propertiesService.getString("certTest")) ) { 
			session.setAttribute("NMinto", propertiesService.getString("certName"));
			session.setAttribute("DIinto", propertiesService.getString("certInfo"));
		}

		return "/coun/"+retPath;
	}	// end of scrAgreOfAttend

	/**
	 * <pre>
	 * 방청신청을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/intro/visitRequ.do")
	public String scrRequOfAttend ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					retPath      = "acts/visitIntro_requ";

		int						listCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseReschdVO>		schdList     = new ArrayList<BaseReschdVO>();
		List<BaseReschdVO>		schdChld     = new ArrayList<BaseReschdVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		String[]				schdType     = new String[12];
		StringBuffer			schdDocu     = new StringBuffer();
log.debug(" act: "+ BaseUtility.spaceFill( "VisitIntroRequ", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(propertiesService.getInt("pageSize"));
		pageInfo.setTotalRecordCount(1);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
//		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), "photo".equalsIgnoreCase(paramPath2) ? propertiesService.getString("imgsUnit") : propertiesService.getString("pageUnit") ));
		searchVO.setPagePerNo("all");
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReStdStat(BaseUtility.null2Char( searchVO.getReStdStat(), "C" ));
		searchVO.setReStdDate(BaseUtility.null2Char( searchVO.getReStdDate(), systemVO.getToYear() + systemVO.getToMonth() ));
		searchVO.setReStdDate("PY".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(), -12 ) : searchVO.getReStdDate());
		searchVO.setReStdDate("PM".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(),  -1 ) : searchVO.getReStdDate());
		searchVO.setReStdDate("NM".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(),   1 ) : searchVO.getReStdDate());
		searchVO.setReStdDate("NY".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(),  12 ) : searchVO.getReStdDate());
		searchVO.setReLstType("CW");

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("stdDate: "+ searchVO.getReStdDate() +" |stdStat: "+ searchVO.getReStdStat());
		try { 
			searchVO.setReBgnDate(BaseUtility.getWeekInMonths( BaseUtility.getDateOfLast( searchVO.getReStdDate(), "F" ), "MF" ));
			searchVO.setReEndDate(BaseUtility.getWeekInMonths( BaseUtility.getDateOfLast( searchVO.getReStdDate(), "L" ), "ML" ));
log.debug("bgnDate: "+ searchVO.getReBgnDate() +" |endDate: "+ searchVO.getReEndDate());

			listCnt = (int)reschdMngrService.getCountOfReschdCal( searchVO );

			// 조회 건수를 전체선택인 경우
			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
			}
			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
			searchVO.setPageFirNo(Integer.toString(idxFir));
			searchVO.setPageLstNo(Integer.toString(idxLst));
			searchVO.setListCnt(Integer.toString(listCnt));

			if( listCnt > 0 ) { 
				schdList = (List<BaseReschdVO>)reschdMngrService.getListOfReschdCal( searchVO );
//				schdList = (List<BaseReschdVO>)reschdMngrService.getListOfReschdCal( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				// Data Adjustment(Reschds)
				if( !(schdList == null || schdList.size() <= 0) ) { 
					schdDocu.setLength(0);
					for( int d = 0; d < schdList.size(); d++ ) { 
						schdList.get(d).setViewNo(schdList.get(d).getStdUUID() + schdList.get(d).getRowSeq());

//						schdList.get(d).setStdDate(BaseUtility.toDateFormat( schdList.get(d).getStdDate(), dtSep, 8 ));

						schdChld = (List<BaseReschdVO>)schdList.get(d).getChildItem();

						if( d % 7 == 0 ) schdDocu.append("<tr>");
						schdDocu.append("\n\t\t\t\t\t\t\t").append("<td class='day'"+ (schdList.get(d).getStdDate().equals(systemVO.getToDate())?" style='background-color:#83d3ca;'":"") +">");
						schdDocu.append("\n\t\t\t\t\t\t\t\t").append("<div class='date"+ (d%7==0?" sun":(d%7==6?" sat":"")) +"'><span class='d_number'");
						schdDocu.append(schdList.get(d).getStdDate().substring(0,6).equals(searchVO.getReStdDate())?"":" style='opacity:0.3;'");
						schdDocu.append(">"+ Integer.parseInt(schdList.get(d).getStdDay()) +"</span></div>");
						schdDocu.append("\n\t\t\t\t\t\t\t\t").append("<div class='day-content'>");
						if( systemVO.getToDate().compareTo(schdList.get(d).getStdDate()) < 0 && !(("F".equals(propertiesService.getString("viewSun")) && BaseUtility.dayOfWeek( schdList.get(d).getStdDate() ) == 1) || ("F".equals(propertiesService.getString("viewSat")) && BaseUtility.dayOfWeek( schdList.get(d).getStdDate() ) == 7)) ) { 
							if( !(schdChld == null || schdChld.size() <= 0) && "CW".equals(BaseUtility.null2Blank( schdChld.get(0).getSchdType() ).split("[`]", schdType.length)[10]) )
								schdDocu.append("<p><a href='javascript:void(0);' class='reserve_01' onclick='fnActCreate(\""+ schdList.get(d).getStdDate() +"\",\"at\")'>방청신청</a></p>");
							schdDocu.append("<p><a href='javascript:void(0);' class='reserve' onclick='fnActCreate(\""+ schdList.get(d).getStdDate() +"\",\"vt\")'>방문신청</a></p>");
						}
						schdDocu.append("</div>").append("\n\t\t\t\t\t\t\t").append("</td>");
						if( d % 7 == 6 ) schdDocu.append("\n\t\t\t\t\t\t").append("</tr>");
					}
				}

				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
				pageInfo.setTotalRecordCount(listCnt);
			}

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			model.addAttribute("dataList"      , schdList);
			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (schdList == null ? 0 : schdList.size()) : listCnt ));
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("dispDocu"      , schdDocu.toString());
			model.addAttribute("dispDate"      , BaseUtility.toDateFormat( searchVO.getReStdDate(), dtSep ));
			model.addAttribute("reStdDate"     , searchVO.getReStdDate());
			model.addAttribute("reStdStat"     , searchVO.getReStdStat());
			model.addAttribute("reCurrDate"    , systemVO.getToYear() + systemVO.getToMonth());
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/coun/"+retPath;
	}	// end of scrRequOfAttend

	/**
	 * <pre>
	 * 방청신청 등록화면으로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/intro/visitInst.do")
	public String scrInstOfAttend ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					retPath      = "acts/visitIntro_inst";

		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();

		// 인증내역 확인
		BaseMemberVO			authInfo     = new BaseMemberVO();
		authInfo.setCertName(BaseUtility.null2Blank( (String)session.getAttribute("NMinto") ));
		authInfo.setCertInfo(BaseUtility.null2Blank( (String)session.getAttribute("DIinto") ));
		// 인증내역이 없으면 인증화면으로 이동
		if( BaseUtility.isValidNull( authInfo.getCertName() ) && BaseUtility.isValidNull( authInfo.getCertInfo() ) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "VisitIntroInst", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			WebUtils.setSessionAttribute(request, "searchSess", searchVO);
//			model.addAttribute("message"       , "본인인증 후 사용 가능합니다.\\n\\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
			model.addAttribute("returnUrl"     , webBase+"/coun/intro/visitAgre.do");
			return "/cmmn/exe_script";
		}
		model.addAttribute("authInfo"      , authInfo);
log.debug(" act: "+ BaseUtility.spaceFill( "VisitIntroInst", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + authInfo.getCertName());

		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reStdDate"     , searchVO.getReStdDate());
		model.addAttribute("reStdStat"     , searchVO.getReStdStat());
		model.addAttribute("reCurrDate"    , systemVO.getToYear() + systemVO.getToMonth());
		model.addAttribute("reqDate"       , BaseUtility.toDateFormat( searchVO.getReqDate(), dtSep ));

		model.addAttribute("cateList"      , cateList);

		return "/coun/"+retPath;
	}	// end of scrInstOfAttend

	/**
	 * <pre>
	 * 방청신청 수정화면으로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/intro/visitEdit.do")
	public String scrEditOfAttend ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					retPath      = "acts/visitIntro_edit";

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		BaseAttendVO			dataView     = new BaseAttendVO();
		List<BaseFilesVO>		dataFile     = new ArrayList<BaseFilesVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();

		// 인증내역 확인
		BaseMemberVO			authInfo     = new BaseMemberVO();
		authInfo.setCertName(BaseUtility.null2Blank( (String)session.getAttribute("NMinto") ));
		authInfo.setCertInfo(BaseUtility.null2Blank( (String)session.getAttribute("DIinto") ));
		// 인증내역이 없으면 인증화면으로 이동
		if( BaseUtility.isValidNull( authInfo.getCertName() ) && BaseUtility.isValidNull( authInfo.getCertInfo() ) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "VisitIntroEdit", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			WebUtils.setSessionAttribute(request, "searchSess", searchVO);
//			model.addAttribute("message"       , "본인인증 후 사용 가능합니다.\\n\\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
			model.addAttribute("returnUrl"     , webBase+"/coun/intro/visitAgre.do");
			return "/cmmn/exe_script";
		}
		model.addAttribute("authInfo"      , authInfo);
log.debug(" act: "+ BaseUtility.spaceFill( "VisitIntroEdit", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + authInfo.getCertName());

		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));
		searchVO.setCondCate("OPEN");

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondName("CERT");
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			viewNo = BaseUtility.null2Char( (String)attendMngrService.getInfoOfAttendUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

			// 입력내역 취득(방청신청)
			searchVO.setReqDate(viewNo[0]);
			searchVO.setReqSeq(viewNo[1]);
log.debug("viewNo: "+ searchVO.getReqDate() +" | "+ searchVO.getReqSeq());

			dataView = (BaseAttendVO)attendMngrService.getDetailOfAttend( searchVO );
			if( dataView != null ) { 
				// Data Adjustment(Attends)
				dataView.setActMode("Update");
				dataView.setViewNo(dataView.getReqUUID());

				dataView.setReqDate(BaseUtility.toDateFormat( dataView.getReqDate(), dtSep, 8 ));
				dataView.setVstTime(BaseUtility.toDateFormat( dataView.getVstDate(), dtSep, 8 ));
				dataView.setVstTimeH(BaseUtility.null2Blank( dataView.getVstDate() ).substring(8,10));
				dataView.setVstTimeM(BaseUtility.null2Blank( dataView.getVstDate() ).substring(10,12));
				dataView.setCreDate(BaseUtility.toDateFormat( dataView.getCreDate(), dtSep, 8 ));
				dataView.setModDate(BaseUtility.toDateFormat( dataView.getModDate(), dtSep, 8 ));
				dataView.setDelDate(BaseUtility.toDateFormat( dataView.getDelDate(), dtSep, 8 ));

				dataView.setVstDescBR(BaseUtility.tag2Sym( dataView.getVstDesc() ));
				dataView.setVstDescCR(BaseUtility.sym2Tag( dataView.getVstDesc() ));
				dataView.setDownExt(FileProcess.getFileLowerExt( dataView.getDownRel() ));
				dataView.setFileUUID(BaseUtility.null2Char(dataView.getFileUUID(), "", "6"+ dataView.getFileUUID() ));
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("cateList"      , cateList);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/coun/"+retPath;
	}	// end of scrEditOfAttend

	/**
	 * <pre>
	 * 방청신청 내역을 등록처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/intro/visitCreate.do")
	public ModelAndView doActOfCreateAttend ( 
			@ModelAttribute("searchVO")BaseAttendVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

//		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
//		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseAttendVO			dataView     = new BaseAttendVO();
		int						fileCnt      = 0;
		int						thmbCnt      = 0;

		// 인증내역 확인
		BaseMemberVO			authInfo     = new BaseMemberVO();
		authInfo.setCertName(BaseUtility.null2Blank( (String)session.getAttribute("NMinto") ));
		authInfo.setCertInfo(BaseUtility.null2Blank( (String)session.getAttribute("DIinto") ));
		// 인증내역이 없으면 인증화면으로 이동
		if( BaseUtility.isValidNull( authInfo.getCertName() ) && BaseUtility.isValidNull( authInfo.getCertInfo() ) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "VisitIntroCreate", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			WebUtils.setSessionAttribute(request, "searchSess", searchVO);
//			model.addAttribute("message"       , "본인인증 후 사용 가능합니다.\\n\\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
//			model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeAgre.do");
//			return "/cmmn/exe_script";
			mav.addObject("isError"       , "C");
//			mav.addObject("message"       , "본인인증 후 사용 가능합니다.\n\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/coun/intro/visitAgre.do");
			return mav;
		}
		model.addAttribute("authInfo"      , authInfo);
log.debug(" act: "+ BaseUtility.spaceFill( "VisitIntroCreate", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + authInfo.getCertName());

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reStdStat"     , searchVO.getReStdStat());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setReqDate(BaseUtility.null2Char( searchVO.getReqDate(), systemVO.getToDate() ));

//log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/coun/atcs/visitIntro_inst";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			// 입력내역 취득(방청신청)
			dataView = (BaseAttendVO)attendMngrService.getInfoOfAttendSeq( searchVO );
			searchVO.setReqSeq(dataView == null ? "1" : dataView.getReqSeq());
			searchVO.setReqUUID(BaseCategory.encryptSHA256( searchVO.getReqDate() + searchVO.getReqSeq() ).toUpperCase());
			searchVO.setReqType("at".equalsIgnoreCase(searchVO.getReStdStat())?"CE":searchVO.getReqType());
			searchVO.setReqType("vt".equalsIgnoreCase(searchVO.getReStdStat())?"CV":searchVO.getReqType());
			searchVO.setVstDate(BaseUtility.null2Char( searchVO.getVstDate(), systemVO.getToDate() + systemVO.getToTime() ).replaceAll("[^0-9]", ""));
			searchVO.setVstTimeH(BaseUtility.null2Char( searchVO.getVstTimeH(), "09" ));
			searchVO.setVstTimeM(BaseUtility.null2Char( searchVO.getVstTimeM(), "00" ));
			searchVO.setVstDate(searchVO.getVstDate() + searchVO.getVstTimeH() + searchVO.getVstTimeM());
			searchVO.setVstPerson(BaseUtility.null2Char( searchVO.getVstPerson(), "1" ).replaceAll("[^0-9]", ""));
			searchVO.setUseDele("N");
			searchVO.setCreIPv4(BaseUtility.reqIPv4Address( request ));
			searchVO.setModIPv4(BaseUtility.reqIPv4Address( request ));
			searchVO.setCreUser(BaseUtility.null2Char( searchVO.getCreUser(), authInfo.getCertName() ));
			searchVO.setCreName(BaseUtility.null2Char( searchVO.getCreName(), authInfo.getCertName() ));
			searchVO.setCreCert(authInfo.getCertInfo());
			searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), authInfo.getCertName() ));
			searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), authInfo.getCertName() ));
			searchVO.setViewNo(searchVO.getBbsUUID() +"1");
log.debug("viewNo: "+ searchVO.getReqDate() +" | "+ searchVO.getReqSeq());

			// 방청신청 내역 갱신
			attendMngrService.doRtnOfCreateAttend( searchVO );
			model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReBgnDate((String)model.get("reBgnDate"));
			searchVO.setReEndDate((String)model.get("reEndDate"));
			searchVO.setReStdStat((String)model.get("reStdStat"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reBgnDate"     , (String)model.get("reBgnDate"));
			mav.addObject("reEndDate"     , (String)model.get("reEndDate"));
			mav.addObject("reStdStat"     , (String)model.get("reStdStat"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//-		return "redirect:/coun/intro/visitView.do";
//		return "redirect:/coun/intro/visitRequ.do";
		return mav;
	}	// end of doActOfCreateAttend

	/**
	 * <pre>
	 * 방청신청 내역을 수정처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  MultipartHttpServletRequest		// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/intro/visitUpdate.do")
	public ModelAndView doActOfUpdateAttend ( 
			@ModelAttribute("searchVO")BaseAttendVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			final MultipartHttpServletRequest mpRequest, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseAttendVO			dataView     = new BaseAttendVO();
		Map<String, MultipartFile> fileMap   = mpRequest == null ? null : mpRequest.getFileMap();
		int						fileCnt      = 0;
		int						thmbCnt      = 0;
		String[]				deleList     = null;
		BaseFilesVO				fileDele     = new BaseFilesVO();

		// 인증내역 확인
		BaseMemberVO			authInfo     = new BaseMemberVO();
		authInfo.setCertName(BaseUtility.null2Blank( (String)session.getAttribute("NMinto") ));
		authInfo.setCertInfo(BaseUtility.null2Blank( (String)session.getAttribute("DIinto") ));
		// 인증내역이 없으면 인증화면으로 이동
		if( BaseUtility.isValidNull( authInfo.getCertName() ) && BaseUtility.isValidNull( authInfo.getCertInfo() ) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "VisitIntroUpdate", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			WebUtils.setSessionAttribute(request, "searchSess", searchVO);
//			model.addAttribute("message"       , "본인인증 후 사용 가능합니다.\\n\\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
//			model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeAgre.do");
//			return "/cmmn/exe_script";
			mav.addObject("isError"       , "C");
//			mav.addObject("message"       , "본인인증 후 사용 가능합니다.\n\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/coun/intro/visitAgre.do");
			return mav;
		}
		model.addAttribute("authInfo"      , authInfo);
log.debug(" act: "+ BaseUtility.spaceFill( "VisitIntroUpdate", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + authInfo.getCertName());

		// 불량 접속 확인
		if( BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
//			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n이전화면으로 전환합니다.");
//			model.addAttribute("returnUrl"     , webBase+"/coun/intro/visitList.do");
//			return "/cmmn/exe_script";
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "잘못된 경로로 접속하였습니다.\n\n이전화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/coun/intro/visitList.do");
			return mav;
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reStdStat"     , searchVO.getReStdStat());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/coun/acts/visitIntro_edit";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)attendMngrService.getInfoOfAttendUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(방청신청)
				searchVO.setReqDate(viewNo[0]);
				searchVO.setReqSeq(viewNo[1]);
				searchVO.setVstDate(BaseUtility.null2Char( searchVO.getVstDate(), systemVO.getToDate() + systemVO.getToTime() ).replaceAll("[^0-9]", ""));
				searchVO.setVstTimeH(BaseUtility.null2Char( searchVO.getVstTimeH(), "09" ));
				searchVO.setVstTimeM(BaseUtility.null2Char( searchVO.getVstTimeM(), "00" ));
				searchVO.setVstDate(searchVO.getVstDate() + searchVO.getVstTimeH() + searchVO.getVstTimeM());
				searchVO.setVstPerson(BaseUtility.null2Char( searchVO.getVstPerson(), "1" ).replaceAll("[^0-9]", ""));
				searchVO.setModIPv4(BaseUtility.reqIPv4Address( request ));
				searchVO.setCreName(BaseUtility.null2Char( searchVO.getCreName(), authInfo.getCertName() ));
				searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), authInfo.getCertName() ));
				searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), authInfo.getCertName() ));
log.debug("viewNo: "+ searchVO.getReqDate() +" | "+ searchVO.getReqSeq() +" | "+ searchVO.getCondDele());

				// 방청신청 내역 갱신
				attendMngrService.doRtnOfUpdateAttend( searchVO );
				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReBgnDate((String)model.get("reBgnDate"));
			searchVO.setReEndDate((String)model.get("reEndDate"));
			searchVO.setReStdStat((String)model.get("reStdStat"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reBgnDate"     , (String)model.get("reBgnDate"));
			mav.addObject("reEndDate"     , (String)model.get("reEndDate"));
			mav.addObject("reStdStat"     , (String)model.get("reStdStat"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//-		return "redirect:/coun/intro/visitView.do";
//		return "redirect:/coun/intro/visitList.do";
		return mav;
	}	// end of doActOfUpdateAttend

	/**
	 * <pre>
	 * 방청신청 내역을 삭제처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return String
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/intro/visitDelete.do")
	public ModelAndView doActOfDeleteAttend ( 
			@ModelAttribute("searchVO")BaseAttendVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseAttendVO			dataView     = new BaseAttendVO();

		// 인증내역 확인
		BaseMemberVO			authInfo     = new BaseMemberVO();
		authInfo.setCertName(BaseUtility.null2Blank( (String)session.getAttribute("NMinto") ));
		authInfo.setCertInfo(BaseUtility.null2Blank( (String)session.getAttribute("DIinto") ));
		// 인증내역이 없으면 인증화면으로 이동
		if( BaseUtility.isValidNull( authInfo.getCertName() ) && BaseUtility.isValidNull( authInfo.getCertInfo() ) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "VisitIntroDelete", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			WebUtils.setSessionAttribute(request, "searchSess", searchVO);
//			model.addAttribute("message"       , "본인인증 후 사용 가능합니다.\\n\\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
//			model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeAgre.do");
//			return "/cmmn/exe_script";
			mav.addObject("isError"       , "C");
//			mav.addObject("message"       , "본인인증 후 사용 가능합니다.\n\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/coun/intro/visitAgre.do");
			return mav;
		}
		model.addAttribute("authInfo"      , authInfo);
log.debug(" act: "+ BaseUtility.spaceFill( "VisitIntroDelete", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + authInfo.getCertName());

		// 불량 접속 확인
		if( BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
//			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n이전화면으로 전환합니다.");
//			model.addAttribute("returnUrl"     , webBase+"/coun/intro/visitList.do");
//			return "/cmmn/exe_script";
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "잘못된 경로로 접속하였습니다.\n\n이전화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/coun/intro/visitList.do");
			return mav;
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reStdStat"     , searchVO.getReStdStat());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)attendMngrService.getInfoOfAttendUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(방청신청)
				searchVO.setReqDate(viewNo[0]);
				searchVO.setReqSeq(viewNo[1]);
				searchVO.setDelIPv4(BaseUtility.reqIPv4Address( request ));
				searchVO.setDelUser(BaseUtility.null2Char( searchVO.getDelUser(), authInfo.getCertName() ));
				searchVO.setDelName(BaseUtility.null2Char( searchVO.getDelName(), authInfo.getCertName() ));
log.debug("viewNo: "+ searchVO.getReqDate() +" | "+ searchVO.getReqSeq());

				// 방청신청 내역 삭제
				attendMngrService.doRtnOfDeleteAttend( searchVO );
				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReBgnDate((String)model.get("reBgnDate"));
			searchVO.setReEndDate((String)model.get("reEndDate"));
			searchVO.setReStdStat((String)model.get("reStdStat"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reBgnDate"     , (String)model.get("reBgnDate"));
			mav.addObject("reEndDate"     , (String)model.get("reEndDate"));
			mav.addObject("reStdStat"     , (String)model.get("reStdStat"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/coun/intro/visitList.do";
		return mav;
	}	// end of doActOfDeleteAttend


	/**
	 * <pre>
	 * 의정보고서을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath}/reportList.do")
	public String scrListOfReport ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					retPath      = "";

		int						listCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseBoardsVO>		notiList     = new ArrayList<BaseBoardsVO>();
		List<BaseBoardsVO>		dataList     = new ArrayList<BaseBoardsVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();

		BaseCommonVO			findFile     = new BaseCommonVO();
		List<BaseFilesVO>		dataFile     = new ArrayList<BaseFilesVO>();

log.debug(" act: "+ BaseUtility.spaceFill( "Report"+ BaseUtility.toCapitalize( paramPath ) +"List", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			retPath  = "base/report"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		} else if( "cost".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110120";
			retPath  = "acts/report"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		} else if( "brief".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110130";
			retPath  = "acts/report"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		} else if( "resr".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110140";
			retPath  = "acts/report"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		} else if( "schd".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110110";
			retPath  = "acts/report"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		} else if( "gover".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110090";
			retPath  = "conf/report"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		} else if( "spch".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110080";
			retPath  = "conf/report"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		} else if( "laws".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110070";
			retPath  = "base/report"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		} else if( "press".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110100";
			retPath  = "base/report"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		} else if( "interview".equalsIgnoreCase(paramPath) ) {
			bbsType  = "10110150";
			retPath  = "base/report"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		} else if( "hold".equalsIgnoreCase(paramPath) ) {
			bbsType  = "10110160";
			retPath  = "acts/report"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		}
		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(propertiesService.getInt("pageSize"));
		pageInfo.setTotalRecordCount(1);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setBbsType(BaseUtility.null2Char( searchVO.getBbsType(), bbsType ));
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReCreHide("N");

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("bbsType: "+ searchVO.getBbsType());
		try { 
			listCnt = (int)boardMngrService.getCountOfBoard( searchVO );

			// 조회 건수를 전체선택인 경우
			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
			}
			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
			searchVO.setPageFirNo(Integer.toString(idxFir));
			searchVO.setPageLstNo(Integer.toString(idxLst));
			searchVO.setListCnt(Integer.toString(listCnt));

			if( listCnt > 0 ) { 
				dataList = (List<BaseBoardsVO>)boardMngrService.getListOfBoard( searchVO );
//				dataList = (List<BaseBoardsVO>)boardMngrService.getListOfBoard( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				// Data Adjustment(Boards)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					for( int d = 0; d < dataList.size(); d++ ) { 
						dataList.get(d).setViewNo(dataList.get(d).getBbsUUID() + dataList.get(d).getRowSeq());

						dataList.get(d).setBbsFiles(BaseUtility.toNumberFormat( dataList.get(d).getBbsFiles() ));
						dataList.get(d).setBgnDate(BaseUtility.toDateFormat( dataList.get(d).getBgnDate(), dtSep, 8 ));
						dataList.get(d).setEndDate(BaseUtility.toDateFormat( dataList.get(d).getEndDate(), dtSep, 8 ));
						dataList.get(d).setHitCount(BaseUtility.toNumberFormat( dataList.get(d).getHitCount() ));
						dataList.get(d).setCreDate(BaseUtility.toDateFormat( dataList.get(d).getCreDate(), dtSep, 8 ));
						dataList.get(d).setModDate(BaseUtility.toDateFormat( dataList.get(d).getModDate(), dtSep, 8 ));
						dataList.get(d).setDelDate(BaseUtility.toDateFormat( dataList.get(d).getDelDate(), dtSep, 8 ));

						dataList.get(d).setBbsTitle(("Y".equals(dataList.get(d).getCreHide())?hideMark:"") + dataList.get(d).getBbsTitle());
						dataList.get(d).setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( dataList.get(d).getAnsPos() )) <= 0 ? dataList.get(d).getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ dataList.get(d).getBbsTitle());
						dataList.get(d).setContDescBR(BaseUtility.tag2Sym( dataList.get(d).getContDesc() ));
						dataList.get(d).setContDescCR(BaseUtility.sym2Tag( dataList.get(d).getContDesc() ));
						dataList.get(d).setContLinkC(BaseUtility.getUriCheck( dataList.get(d).getBbsSiteUri() ) ? "T" : "F");
						dataList.get(d).setCreLock(BaseUtility.null2Char( dataList.get(d).getCrePasswd(), "", "Y" ));
						dataList.get(d).setDownExt(FileProcess.getFileLowerExt( dataList.get(d).getDownRel() ));
						dataList.get(d).setFileUUID(BaseUtility.null2Char(dataList.get(d).getFileUUID(), "", "2"+ dataList.get(d).getFileUUID() ));
						dataList.get(d).setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( dataList.get(d).getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
						dataList.get(d).setRegiDate(BaseUtility.toDateFormat( BaseUtility.null2Char( dataList.get(d).getRegiDate(), dataList.get(d).getCreDate() ), dtSep, 8 ));
						if( "gover".equalsIgnoreCase(paramPath) || "spch".equalsIgnoreCase(paramPath) ) dataList.get(d).setCreUser(dataList.get(d).getCreName());
						if( !dataList.get(d).getCreUser().equals(dataList.get(d).getCreName()) ) dataList.get(d).setCreName(propertiesService.getString("creName"));

						// 보도자료는 미리보기사진 한장을 보여주도록 셋팅
						if( "press".equalsIgnoreCase(paramPath) ) {
							findFile.setLnkName(lnkName);
							findFile.setLnkKey(bbsType + dataList.get(d).getBbsSeq() + "");
							dataFile = (List<BaseFilesVO>)baseCmmnService.getListOfDataFile( findFile );
							dataList.get(d).setFileViewImg(FileProcess.getInfoOfFileViewImg( request, response, dataFile, "S" ));
						}

						// 상임위원회 구분
						if( "stand".equalsIgnoreCase(paramPath) && BaseUtility.isValidNull( dataList.get(d).getBbsCateName() ) ) { 
							if( "OPER".equals(dataList.get(d).getBbsCate())  ) dataList.get(d).setBbsCateName("의회운영위원회");
							if( "GVADM".equals(dataList.get(d).getBbsCate()) ) dataList.get(d).setBbsCateName("기획행정위원회");
							if( "WELFARE".equals(dataList.get(d).getBbsCate()) ) dataList.get(d).setBbsCateName("복지환경위원회");
							if( "BUILD".equals(dataList.get(d).getBbsCate()) ) dataList.get(d).setBbsCateName("산업건설위원회");
						}
					}
				}

				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
				pageInfo.setTotalRecordCount(listCnt);
			}

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			model.addAttribute("dataList"      , dataList);
			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (dataList == null ? 0 : dataList.size()) : listCnt ));
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("notiList"      , notiList);
			model.addAttribute("cateList"      , cateList);
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/coun/"+retPath;
	}	// end of scrListOfReport

	/**
	 * <pre>
	 * 의정보고서을(를) 상세조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath}/reportView.do")
	public String scrViewOfReport ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					retPath      = "";

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		BaseBoardsVO			dataView     = new BaseBoardsVO();
		BaseBoardsVO			prevView     = new BaseBoardsVO();
		BaseBoardsVO			nextView     = new BaseBoardsVO();
		BaseCommonVO			findFile     = new BaseCommonVO();
		List<BaseFilesVO>		dataFile     = new ArrayList<BaseFilesVO>();
		List<BaseFilesVO>		thmbFile     = new ArrayList<BaseFilesVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		dataSupp     = new ArrayList<BaseCommonVO>();
		BaseBoardsVO			tempSupp     = new BaseBoardsVO();
		BaseBoardsVO			tempView     = new BaseBoardsVO();
		String[][]				tempList     = new String[4][10];
log.debug(" act: "+ BaseUtility.spaceFill( "Report"+ BaseUtility.toCapitalize( paramPath ) +"View", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			retPath  = "base/report"+ BaseUtility.toCapitalize( paramPath ) +"_view";
		} else if( "cost".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110120";
			retPath  = "acts/report"+ BaseUtility.toCapitalize( paramPath ) +"_view";
		} else if( "brief".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110130";
			retPath  = "acts/report"+ BaseUtility.toCapitalize( paramPath ) +"_view";
		} else if( "resr".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110140";
			retPath  = "acts/report"+ BaseUtility.toCapitalize( paramPath ) +"_view";
		} else if( "schd".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110110";
			retPath  = "acts/report"+ BaseUtility.toCapitalize( paramPath ) +"_view";
		} else if( "gover".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110090";
			retPath  = "conf/report"+ BaseUtility.toCapitalize( paramPath ) +"_view";
		} else if( "spch".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110080";
			retPath  = "conf/report"+ BaseUtility.toCapitalize( paramPath ) +"_view";
		} else if( "laws".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110070";
			retPath  = "base/report"+ BaseUtility.toCapitalize( paramPath ) +"_view";
		} else if( "press".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110100";
			retPath  = "base/report"+ BaseUtility.toCapitalize( paramPath ) +"_view";
		} else if( "interview".equalsIgnoreCase(paramPath) ) {
			bbsType  = "10110150";
			retPath  = "base/report"+ BaseUtility.toCapitalize( paramPath ) +"_view";
		} else if( "hold".equalsIgnoreCase(paramPath) ) {
			bbsType  = "10110160";
			retPath  = "acts/report"+ BaseUtility.toCapitalize( paramPath ) +"_view";
		}
		if( "".equals(retPath) || BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setBbsType(BaseUtility.null2Char( searchVO.getBbsType(), bbsType ));
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));
		searchVO.setReCreHide("N");

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)boardMngrService.getInfoOfBoardUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(게시판)
				searchVO.setBbsType(viewNo[0]);
				searchVO.setBbsSeq(viewNo[1]);
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());

				dataView = (BaseBoardsVO)boardMngrService.getDetailOfBoard( searchVO );
				if( dataView != null ) { 
					boardMngrService.doRtnOfModifyBoardHit( dataView );

					findFile.setLnkName(lnkName);
					findFile.setLnkKey(viewNo[0] + viewNo[1]);
					// Data Adjustment(Data Files)
					dataFile = (List<BaseFilesVO>)baseCmmnService.getListOfDataFile( findFile );
					// Data Adjustment(Thumb Files)
					thmbFile = (List<BaseFilesVO>)baseCmmnService.getListOfThmbFile( findFile );
					if( !(thmbFile == null || thmbFile.size() <= 0) ) { 
						for( int f = 0; f < thmbFile.size(); f++ ) { 
							thmbFile.get(f).setFileImage(BaseUtility.getFileAllowOfImage( thmbFile.get(f).getFileAbs() ) ? "T" : null);
						}
					}

					// Data Adjustment(Boards)
					dataView.setActMode("Update");
					dataView.setViewNo(dataView.getBbsUUID());

					dataView.setBbsFiles(BaseUtility.toNumberFormat( dataView.getBbsFiles() ));
					dataView.setBgnDate(BaseUtility.toDateFormat( dataView.getBgnDate(), dtSep, 8 ));
					dataView.setEndDate(BaseUtility.toDateFormat( dataView.getEndDate(), dtSep, 8 ));
					dataView.setHitCount(BaseUtility.toNumberFormat( Integer.parseInt(BaseUtility.null2Zero( dataView.getHitCount() )) + 1 ));
					dataView.setCreDate(BaseUtility.toDateFormat( dataView.getCreDate(), dtSep, 8 ));
					dataView.setModDate(BaseUtility.toDateFormat( dataView.getModDate(), dtSep, 8 ));
					dataView.setDelDate(BaseUtility.toDateFormat( dataView.getDelDate(), dtSep, 8 ));

					dataView.setBbsTitle(("Y".equals(dataView.getCreHide())?hideMark:"") + dataView.getBbsTitle());
					dataView.setContDescBR(BaseUtility.tag2Sym( dataView.getContDesc() ));
					dataView.setContDescCR(BaseUtility.sym2Tag( dataView.getContDesc() ));
					dataView.setContLinkC(BaseUtility.getUriCheck( dataView.getBbsSiteUri() ) ? "T" : "F");
					dataView.setCreLock(BaseUtility.null2Char( dataView.getCrePasswd(), "", "Y" ));
					dataView.setDownExt(FileProcess.getFileLowerExt( dataView.getDownRel() ));
					dataView.setFileUUID(BaseUtility.null2Char(dataView.getFileUUID(), "", "6"+ dataView.getFileUUID() ));
					dataView.setFileLinkS(FileProcess.getInfoOfFileBBS( request, response, dataFile, "S" ));
					dataView.setFileLinkM(FileProcess.getInfoOfFileBBS( request, response, dataFile, "M" ));
					dataView.setRegiDate(BaseUtility.toDateFormat( BaseUtility.null2Char( dataView.getRegiDate(), dataView.getCreDate() ), dtSep, 8 ));
					if( "gover".equalsIgnoreCase(paramPath) || "spch".equalsIgnoreCase(paramPath) ) dataView.setCreUser(dataView.getCreName());
					if( !dataView.getCreUser().equals(dataView.getCreName()) ) dataView.setCreName(propertiesService.getString("creName"));

//log.debug("dataFile: " + dataFile);

					// 보도자료는 첨부사진을 보여주도록 셋팅
					if( "press".equalsIgnoreCase(paramPath) ) {
						dataView.setFileViewImg(FileProcess.getInfoOfFileViewImg( request, response, dataFile, "M" ));
					}

					// 추가게시판 내역
					tempSupp = (BaseBoardsVO)boardMngrService.getDetailOfAdder( searchVO );
					if( tempSupp != null ) { 
						dataView.setConfCode1(tempSupp.getConfCode1());
						dataView.setConfUUID1(BaseCategory.encryptSHA256( tempSupp.getConfCode1() ));
						dataView.setConfName1((String)baseCmmnService.getInfoOfConfName( tempSupp.getConfCode1() ));
						dataView.setRegiDate1(BaseUtility.toDateFormat( tempSupp.getRegDate1(), dtSep, 8 ));

						tempList[0] = BaseUtility.isValidNull( tempSupp.getConfCode2() ) ? null : BaseUtility.null2Blank( tempSupp.getConfCode2() ).split("[◆]");
						tempList[1] = BaseUtility.isValidNull( tempSupp.getRegDate2()  ) ? null : BaseUtility.null2Blank( tempSupp.getRegDate2() ).split("[◆]");
						tempList[2] = BaseUtility.isValidNull( tempSupp.getRegName2()  ) ? null : BaseUtility.null2Blank( tempSupp.getRegName2() ).split("[◆]");
						tempList[3] = BaseUtility.isValidNull( tempSupp.getContent2()  ) ? null : BaseUtility.null2Blank( tempSupp.getContent2() ).split("[◆]");

						if( !(tempList[2] == null || tempList[2].length <= 0) ) { 
							tempList[0] = tempList[0] == null ? new String[tempList[2].length] : tempList[0];
							tempList[1] = tempList[1] == null ? new String[tempList[2].length] : tempList[1];
							tempList[3] = tempList[3] == null ? new String[tempList[2].length] : tempList[3];
							for( int i = 0; i < tempList[2].length; i++ ) { 
								tempView = new BaseBoardsVO();
								tempView.setConfCode2(BaseUtility.null2Blank( tempList[0][i] ));
								tempView.setConfUUID2(BaseCategory.encryptSHA256( tempList[0][i] ));
								tempView.setConfName2((String)baseCmmnService.getInfoOfConfName( tempList[0][i] ));
								tempView.setRegiDate2(BaseUtility.toDateFormat( tempList[1][i], dtSep, 8 ));
								tempView.setRegiName2(BaseUtility.null2Blank( tempList[2][i] ));
								tempView.setContents2(BaseUtility.null2Blank( tempList[3][i] ));
								tempView.setContents2BR(BaseUtility.tag2Sym( tempList[3][i] ));
								tempView.setContents2CR(BaseUtility.sym2Tag( tempList[3][i] ));
								dataSupp.add(tempView);
							}
						}
					}

					// 상임위원회 구분
					if( "stand".equalsIgnoreCase(paramPath) && BaseUtility.isValidNull( dataView.getBbsCateName() ) ) { 
						if( "OPER".equals(dataView.getBbsCate())  ) dataView.setBbsCateName("의회운영위원회");
						if( "GVADM".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("기획행정위원회");
						if( "WELFARE".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("복지환경위원회");
						if( "BUILD".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("산업건설위원회");
					}

					// Data Adjustment(Move Items)
					if( moveItems && !BaseUtility.isValidNull( searchVO.getRowSeq() ) ) { 
						searchVO.setRowSeq(String.valueOf(Integer.parseInt(searchVO.getRowSeq()) + 1));
						prevView = (BaseBoardsVO)boardMngrService.getMovingOfBoard( searchVO );
						if( prevView != null ) { 
							prevView.setViewNo(prevView.getBbsUUID() + prevView.getRowSeq());
							prevView.setBbsTitle(("Y".equals(prevView.getCreHide())?hideMark:"") + prevView.getBbsTitle());
							prevView.setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( prevView.getAnsPos() )) <= 0 ? prevView.getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ prevView.getBbsTitle());
							prevView.setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( prevView.getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
							if( !prevView.getCreUser().equals(prevView.getCreName()) ) prevView.setCreName(propertiesService.getString("creName"));
						}
						searchVO.setRowSeq(String.valueOf(Integer.parseInt(searchVO.getRowSeq()) - 2));
						nextView = (BaseBoardsVO)boardMngrService.getMovingOfBoard( searchVO );
						if( nextView != null ) { 
							nextView.setViewNo(nextView.getBbsUUID() + nextView.getRowSeq());
							nextView.setBbsTitle(("Y".equals(nextView.getCreHide())?hideMark:"") + nextView.getBbsTitle());
							nextView.setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( nextView.getAnsPos() )) <= 0 ? nextView.getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ nextView.getBbsTitle());
							nextView.setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( nextView.getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
							if( !nextView.getCreUser().equals(nextView.getCreName()) ) nextView.setCreName(propertiesService.getString("creName"));
						}
					} else { 
						prevView = null;
						nextView = null;
					}
				}
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("dataFile"      , dataFile);
			model.addAttribute("thmbFile"      , thmbFile);
			model.addAttribute("moveItem"      , moveItems?"T":"F");
			model.addAttribute("prevView"      , prevView);
			model.addAttribute("nextView"      , nextView);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("dataSupp"      , dataSupp);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/coun/"+retPath;
	}	// end of scrViewOfReport

//	@ExceptionHandler(MaxUploadSizeExceededException.class)
	public ModelAndView resolveException(HttpServletRequest request,HttpServletResponse response,Object handler,Exception exception){
		ModelAndView			mav          = new ModelAndView("jsonView");
		if(exception instanceof MaxUploadSizeExceededException)
			mav.addObject("message"       , "첨부파일 사이즈는 "+ BaseUtility.toNumberFormat( (((MaxUploadSizeExceededException)exception).getMaxUploadSize()/1024/1024) ) +"MB를 초과할 수 없습니다.").addObject("isError"       , "T");
		return mav;
	}

}	// end of class