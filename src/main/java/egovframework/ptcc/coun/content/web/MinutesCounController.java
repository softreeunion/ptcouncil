/*******************************************************************************
  Program ID  : MinutesCounController
  Description : 클라이언트의 요청(공통)을 처리할 메서드를 구현
  Author      : HexaMedia new0man
  Write Date  : 2019.06.04
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v1.0    (주)헥사미디어 개발자  2014.00.00  최초 생성
  v2.0    HexaMedia new0man      2019.06.04  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.coun.content.web;

import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.*;

import net.sf.json.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.jdbc.datasource.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springmodules.validation.commons.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.util.UserDetailsHelper;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.agenda.service.*;

@Controller
@RequestMapping(value="/coun")
public class MinutesCounController {

	@Autowired
	private WebApplicationContext			webContext;
	@Autowired
	private DataSourceTransactionManager	txManager;

	@Resource(name="beanValidator")
	protected DefaultBeanValidator			beanValidator;
	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="agendaMngrService")
	protected AgendaMngrService			agendaMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));


	/**
	 * <pre>
	 * 회의록 정보을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath}/minutesList.do")
	public String scrListOfMinutes ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception {


log.debug(">>> start");

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					retPath      = "";

		int						listCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseAgendaVO>		dataList     = new ArrayList<BaseAgendaVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		eraList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		kndList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		rndList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		memList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		pntList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		tempList     = new ArrayList<BaseCommonVO>();
		BaseCommonVO			tempView     = new BaseCommonVO();
		JSONObject				jsonObjs     = new JSONObject();
		JSONArray				tempTree     = new JSONArray();
		List<String>			rndCodes     = new ArrayList<String>();
log.debug(" act: "+ BaseUtility.spaceFill( "Minutes"+ BaseUtility.toCapitalize( paramPath ) +"List", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

log.debug(">>> paramPath : " + paramPath);

		if( "base".equals(paramPath.toLowerCase()) ) {
			retPath  = "conf/minutes"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		} else if( "past".equals(paramPath.toLowerCase()) ) { 
			retPath  = "conf/minutes"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		} else if( "sess".equals(paramPath.toLowerCase()) ) { 
			retPath  = "conf/minutes"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		} else if( "meet".equals(paramPath.toLowerCase()) ) { 
			retPath  = "conf/minutes"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		} else if( "year".equals(paramPath.toLowerCase()) ) { 
			retPath  = "conf/minutes"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		} else if( "tail".equals(paramPath.toLowerCase()) ) { 
			retPath  = "conf/minutes"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		} else if( "temp".equals(paramPath.toLowerCase()) ) { 
			retPath  = "conf/minutes"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		}
		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		pageInfo.setTotalRecordCount(1);
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(10);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "N" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reConfType"    , searchVO.getReConfType());
		model.addAttribute("reEraCode"     , searchVO.getReEraCode());
		model.addAttribute("reKndCode"     , searchVO.getReKndCode());
		model.addAttribute("reRndCode"     , searchVO.getReRndCode());
		model.addAttribute("reRndDiv"      , searchVO.getReRndDiv());
		model.addAttribute("reMemID"       , searchVO.getReMemID());
		model.addAttribute("searchText"    , searchVO.getSearchText());

		// 추가 조회조건 취득
//		searchVO.setCondCate(BaseUtility.null2Char( searchVO.getCondCate(), "%" ));
//		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReBgnDate(BaseUtility.null2Blank( searchVO.getReBgnDate() ).replaceAll("[^0-9]", ""));
		searchVO.setReEndDate(BaseUtility.null2Blank( searchVO.getReEndDate() ).replaceAll("[^0-9]", ""));

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
if( "past".equalsIgnoreCase(paramPath) || "tail".equalsIgnoreCase(paramPath) || "temp".equalsIgnoreCase(paramPath) || "days".equalsIgnoreCase(paramPath) || "time".equalsIgnoreCase(paramPath) ) { 
log.debug("bgnDate: "+ searchVO.getReBgnDate() +" |endDate: "+ searchVO.getReEndDate());
log.debug("eraCode: "+ searchVO.getReEraCode() +" |kndCode: "+ searchVO.getReKndCode() +" |rndCode: "+ searchVO.getReRndCode() +" |rndDiv: "+ searchVO.getReRndDiv());
log.debug("confType: "+ searchVO.getReConfType() +" |memID: "+ searchVO.getReMemID() +" |searchText: "+ searchVO.getSearchText());
}
		try { 
			if( BaseUtility.isValidNull( searchVO.getReEraCode() ) ) { 
				// 현재 회의대수 취득 - 최근회의록, 상세검색
				if( "past".equalsIgnoreCase(paramPath) || "tail".equalsIgnoreCase(paramPath) ) { 
					// 분류구분 취득 - 현재 회의대수 구분
					tempView = (BaseCommonVO)baseCmmnService.getInfoOfClassNOW( "" );
					searchVO.setReEraCode(tempView == null ? BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsCate" ) : tempView.getEraCode());
					model.addAttribute("reEraCode"     , searchVO.getReEraCode());
log.debug(">>> new eraCode: "+ searchVO.getReEraCode());
				}
			}

			if( BaseUtility.isValidNull( searchVO.getReRndCode() ) ) {
				// 회의회수 취득(2회) - 최근회의록
				if( "past".equalsIgnoreCase(paramPath) ) {
					// 분류구분 취득 - 전체 회의회수 구분
					tempList = (List<BaseCommonVO>)baseCmmnService.getListOfRoundALL( searchVO.getReEraCode() );
log.debug(">>> tempList.size() : "+tempList.size());
//					if( !(tempList == null || tempList.size() <= 0) ) {
//						rndCodes = new ArrayList<String>();
//						for( int t = 0; t < 2; t++ ) rndCodes.add(tempList.get(t).getRoundCode());
//						searchVO.setReRndCode("");
//						searchVO.setRndCodes(rndCodes);
//					}
//					model.addAttribute("reRndCode"   , searchVO.getReRndCode());
//					log.debug(">>> new rndCode: "+ searchVO.getRndCodes());
				}
			}


			// 임시회의록인경우
			if( "temp".equalsIgnoreCase(paramPath) ) { 
				searchVO.setReConfType("T");
log.debug("new confType: "+ searchVO.getReConfType());
			}
			// 단순검색 - 회기별
			if( "sess".equalsIgnoreCase(paramPath) ) { 
				dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfSess( searchVO );
				// Data Adjustment(Minutes)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					tempTree.clear();
					for( int d = 0; d < dataList.size(); d++ ) { 
						jsonObjs.clear();
						jsonObjs.put("root"          , "PTCC");
						jsonObjs.put("folder"        , "0".equals(dataList.get(d).getRndCount()) ? "false" : "true");
						jsonObjs.put("lazy"          , "true");
						jsonObjs.put("mode"          , "session");
						jsonObjs.put("era"           , dataList.get(d).getEraCode());
						jsonObjs.put("title"         , dataList.get(d).getEraTitle());
						tempTree.add(jsonObjs);
					}
				}
			}
			// 단순검색 - 회의별
			if( "meet".equalsIgnoreCase(paramPath) ) { 
				dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfMeet( searchVO );
				// Data Adjustment(Minutes)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					tempTree.clear();
					for( int d = 0; d < dataList.size(); d++ ) { 
						jsonObjs.clear();
						jsonObjs.put("root"          , "PTCC");
						jsonObjs.put("folder"        , "0".equals(dataList.get(d).getKndCount()) ? "false" : "true");
						jsonObjs.put("lazy"          , "true");
						jsonObjs.put("mode"          , "period");
						jsonObjs.put("knd"           , dataList.get(d).getKndCode());
						jsonObjs.put("title"         , dataList.get(d).getKndTitle());
						tempTree.add(jsonObjs);
					}
				}
			}
			// 단순검색 - 년도별
			if( "year".equalsIgnoreCase(paramPath) ) { 
				dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfYear( searchVO );
				// Data Adjustment(Minutes)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					tempTree.clear();
					for( int d = 0; d < dataList.size(); d++ ) { 
						jsonObjs.clear();
						jsonObjs.put("root"          , "PTCC");
						jsonObjs.put("folder"        , "0".equals(dataList.get(d).getYosCount()) ? "false" : "true");
						jsonObjs.put("lazy"          , "true");
						jsonObjs.put("mode"          , "session");
						jsonObjs.put("yos"           , dataList.get(d).getYosCode());
						jsonObjs.put("title"         , dataList.get(d).getYosTitle());
						tempTree.add(jsonObjs);
					}
				}
			}

log.debug(">>> search");
			// 최근회의록, 상세검색, 임시회의록, 회의통계
			if( "past".equalsIgnoreCase(paramPath) || "tail".equalsIgnoreCase(paramPath) || "temp".equalsIgnoreCase(paramPath) || "days".equalsIgnoreCase(paramPath) || "time".equalsIgnoreCase(paramPath) ) {

				listCnt = (int)agendaMngrService.getCountOfConfBase( searchVO );

log.debug(">>> listCnt : " + listCnt);

				// 조회 건수를 전체선택인 경우
				if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
					searchVO.setPageCurNo("1");
					searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
				}
				idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
				idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
				searchVO.setPageFirNo(Integer.toString(idxFir));
				searchVO.setPageLstNo(Integer.toString(idxLst));
				searchVO.setListCnt(Integer.toString(listCnt));

//log.debug(">>> 222");
				if( listCnt > 0 ) { 
					dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfBase( searchVO );
//					dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfBase( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
					// Data Adjustment(Minutes)
					if( !(dataList == null || dataList.size() <= 0) ) { 
						for( int d = 0; d < dataList.size(); d++ ) { 
							dataList.get(d).setViewNo(dataList.get(d).getConfUUID() + dataList.get(d).getRowSeq());

							dataList.get(d).setConfDate(BaseUtility.toDateFormat( dataList.get(d).getConfDate(), dtSep ));
							dataList.get(d).setConfStartTime(BaseUtility.toTimeFormat( dataList.get(d).getConfStartTime() ));
							dataList.get(d).setConfCloseTime(BaseUtility.toTimeFormat( dataList.get(d).getConfCloseTime() ));
							dataList.get(d).setConfReqTime(BaseUtility.toTimeFormat( dataList.get(d).getConfReqTime() ));
							dataList.get(d).setConfWeek(BaseUtility.date2Week( dataList.get(d).getConfDate() ));
						}
					}

					pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
					pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
					pageInfo.setTotalRecordCount(listCnt);
				}

				// 분류구분 취득 - 전체 회의대수 구분
				eraList = (List<BaseCommonVO>)baseCmmnService.getListOfClassALL( "" );
				if( "tail".equalsIgnoreCase(paramPath) || "item".equalsIgnoreCase(paramPath) ) {
					// 분류구분 취득 - 회의회수 구분
					rndList = (List<BaseCommonVO>)baseCmmnService.getListOfRoundALL( searchVO.getReEraCode() );
					// 분류구분 취득 - 희의분류 구분
					kndList = (List<BaseCommonVO>)baseCmmnService.getListOfGroup( "" );
					// 분류구분 취득 - 위원명
					memList = (List<BaseCommonVO>)baseCmmnService.getListOfMembALL( searchVO.getReEraCode() );
				}

log.debug(">>> idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
				model.addAttribute("dataList"      , dataList);
				model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (dataList == null ? 0 : dataList.size()) : listCnt ));
				model.addAttribute("pageInfo"      , pageInfo);
				model.addAttribute("cateList"      , cateList);
				model.addAttribute("eraList"       , eraList);
				model.addAttribute("kndList"       , kndList);
				model.addAttribute("rndList"       , rndList);
				model.addAttribute("pntList"       , pntList);
				model.addAttribute("memList"       , memList);
				model.remove("searchVO");
			}
			// 단순검색
			if( "sess".equalsIgnoreCase(paramPath) || "meet".equalsIgnoreCase(paramPath) || "year".equalsIgnoreCase(paramPath) ) { 
				model.addAttribute("cateTree"      , tempTree == null ? "" : tempTree.toString());
				model.remove("searchVO");
			}
		} catch( Exception E ) { 
			log.debug("Error >>> " + E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/coun/"+retPath;
	}	// end of scrListOfMinutes

	/**
	 * <pre>
	 * 회의록 통계을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath}/minutesStat.do")
	public String scrStatOfMinutes ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception {

		log.debug("222 >>> ");

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					retPath      = "";

		int						listCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseAgendaVO>		dataList     = new ArrayList<BaseAgendaVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		eraList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		kndList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		tempList     = new ArrayList<BaseCommonVO>();
		BaseCommonVO			tempView     = new BaseCommonVO();
log.debug(" act: "+ BaseUtility.spaceFill( "Minutes"+ BaseUtility.toCapitalize( paramPath ) +"List", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "base".equalsIgnoreCase(paramPath) ) { 
			retPath  = "conf/minutes"+ BaseUtility.toCapitalize( paramPath ) +"_stat";
		} else if( "days".equalsIgnoreCase(paramPath) ) { 
			retPath  = "conf/minutes"+ BaseUtility.toCapitalize( paramPath ) +"_stat";
		} else if( "time".equalsIgnoreCase(paramPath) ) { 
			retPath  = "conf/minutes"+ BaseUtility.toCapitalize( paramPath ) +"_stat";
		}
		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		pageInfo.setTotalRecordCount(1);
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(10);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "N" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reEraCode"     , searchVO.getReEraCode());
		model.addAttribute("reKndCode"     , searchVO.getReKndCode());

		// 추가 조회조건 취득
//		searchVO.setCondCate(BaseUtility.null2Char( searchVO.getCondCate(), "%" ));
//		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("eraCode: "+ searchVO.getReEraCode() +" |kndCode: "+ searchVO.getReKndCode());
		try { 
			if( BaseUtility.isValidNull( searchVO.getReEraCode() ) ) { 
				// 분류구분 취득 - 현재 회의대수 구분
				tempView = (BaseCommonVO)baseCmmnService.getInfoOfClassNOW( "" );
				searchVO.setReEraCode(tempView == null ? BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsCate" ) : tempView.getEraCode());
				model.addAttribute("reEraCode"     , searchVO.getReEraCode());
log.debug("new eraCode: "+ searchVO.getReEraCode());
			}
//			listCnt = (int)boardMngrService.getCountOfBoard( searchVO );
//
//			// 조회 건수를 전체선택인 경우
//			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
//				searchVO.setPageCurNo("1");
//				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
//			}
//			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
//			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
//			searchVO.setPageFirNo(Integer.toString(idxFir));
//			searchVO.setPageLstNo(Integer.toString(idxLst));
//			searchVO.setListCnt(Integer.toString(listCnt));
//
//			if( listCnt > 0 ) { 
				if( "base".equalsIgnoreCase(paramPath) ) { 
					dataList = (List<BaseAgendaVO>)agendaMngrService.getStatOfAgendaDays( searchVO );
				} else if( "days".equalsIgnoreCase(paramPath) ) { 
					dataList = (List<BaseAgendaVO>)agendaMngrService.getStatOfAgendaDays( searchVO );
				} else if( "time".equalsIgnoreCase(paramPath) ) { 
					dataList = (List<BaseAgendaVO>)agendaMngrService.getStatOfAgendaTime( searchVO );
				}
				// Data Adjustment(Agendas)
				if( !(dataList == null || dataList.size() <= 0) && "days".equalsIgnoreCase(paramPath) ) { 
					for( int d = 0; d < dataList.size(); d++ ) { 
						dataList.get(d).setViewNo(dataList.get(d).getConfCode());

						dataList.get(d).setRoundStartDate(BaseUtility.toDateFormat( dataList.get(d).getRoundStartDate(), dtSep ));
						dataList.get(d).setRoundCloseDate(BaseUtility.toDateFormat( dataList.get(d).getRoundCloseDate(), dtSep ));

						dataList.get(d).setRndDiffDate(BaseUtility.toNumberFormat( dataList.get(d).getRndDiffDate() ));
						dataList.get(d).setKnd000(BaseUtility.toNumberFormat( dataList.get(d).getKnd000() ));
						dataList.get(d).setKnd100(BaseUtility.toNumberFormat( dataList.get(d).getKnd100() ));
						dataList.get(d).setKnd200(BaseUtility.toNumberFormat( dataList.get(d).getKnd200() ));
						dataList.get(d).setKnd300(BaseUtility.toNumberFormat( dataList.get(d).getKnd300() ));
						dataList.get(d).setKnd400(BaseUtility.toNumberFormat( dataList.get(d).getKnd400() ));
						dataList.get(d).setKnd500(BaseUtility.toNumberFormat( dataList.get(d).getKnd500() ));
						dataList.get(d).setKnd600(BaseUtility.toNumberFormat( dataList.get(d).getKnd600() ));
						dataList.get(d).setKnd700(BaseUtility.toNumberFormat( dataList.get(d).getKnd700() ));
						dataList.get(d).setKnd800(BaseUtility.toNumberFormat( dataList.get(d).getKnd800() ));
						dataList.get(d).setKnd900(BaseUtility.toNumberFormat( dataList.get(d).getKnd900() ));

						if( "xsum".equals(dataList.get(d).getEraCode()) ) { 
							dataList.get(d).setFontColor(" style='color:#629593;font-weight:500;'");
						} else { 
							dataList.get(d).setFontColor("");
						}
				}
				}
//
//				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
//				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
//				pageInfo.setTotalRecordCount(listCnt);
//			}

			// 분류구분 취득 - 전체 회의대수 구분
			eraList = (List<BaseCommonVO>)baseCmmnService.getListOfClassALL( "" );
			// 분류구분 취득 - 희의분류 구분
			kndList = (List<BaseCommonVO>)baseCmmnService.getListOfGroup( "" );

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			model.addAttribute("dataList"      , dataList);
			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (dataList == null ? 0 : dataList.size()) : listCnt ));
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("eraList"       , eraList);
			model.addAttribute("kndList"       , kndList);
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/coun/"+retPath;
	}	// end of scrStatOfMinutes

}	// end of class