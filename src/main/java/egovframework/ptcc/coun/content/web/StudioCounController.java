/*******************************************************************************
  Program ID  : StudioCounController
  Description : 클라이언트의 요청(공통)을 처리할 메서드를 구현
  Author      : HexaMedia new0man
  Write Date  : 2019.06.04
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.06.04  최초 생성
  v1.0    HexaMedia new0man      2019.06.04  평택시의회 Site Renewal
  v1.1    HexaMedia new0man      2020.04.01  일정관리에서 일요일 및 토요일 포함여부
 *******************************************************************************/
package egovframework.ptcc.coun.content.web;

import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.util.*;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.agenda.service.*;
import egovframework.ptcc.mngr.board.service.*;
import egovframework.ptcc.mngr.member.service.*;

@Controller
@RequestMapping(value="/coun")
public class StudioCounController {

	@Autowired
	private WebApplicationContext			webContext;
	@Autowired
	private DataSourceTransactionManager	txManager;

	@Resource(name="beanValidator")
	protected DefaultBeanValidator			beanValidator;
	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="agendaMngrService")
	protected AgendaMngrService			agendaMngrService;
	@Resource(name="reschdMngrService")
	protected ReschdMngrService			reschdMngrService;
	@Resource(name="membMngrService")
	protected MembMngrService				membMngrService;
	@Resource(name="boardMngrService")
	protected BoardMngrService				boardMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));
	protected boolean						notiItems    = false;
	protected boolean						moveItems    = false;
	protected boolean						fileUP       = false;
	protected boolean						tempUP       = false;


	private String getSchdTypeName ( 
			String argType, 
			String argValue 
		) throws Exception { 

		String					retS         = "";

		try { 
			if( BaseUtility.isValidNull( argValue ) ) { 
				retS = "";
			} else if( "CL".equals(argValue) ) { 
				retS = "S".equals(argType) ? "생방(본)"   : ("N".equals(argType) ? "본회의"   : "생 방 송 (본회의)");
			} else if( "OL".equals(argValue) ) { 
				retS = "S".equals(argType) ? "생방(운영)" : ("N".equals(argType) ? "운영위"   : "생 방 송 (의회운영위원회)");
			} else if( "GL".equals(argValue) ) { 
//				retS = "S".equals(argType) ? "생방(자치)" : ("N".equals(argType) ? "자치위"   : "생 방 송 (자치행정위원회)");
				retS = "S".equals(argType) ? "생방(기획)" : ("N".equals(argType) ? "기획위"   : "생 방 송 (기획행정위원회)");
			} else if( "WL".equals(argValue) ) {
				retS = "S".equals(argType) ? "생방(복지)" : ("N".equals(argType) ? "복지위"   : "생 방 송 (복지환경위원회)");
			} else if( "BL".equals(argValue) ) { 
				retS = "S".equals(argType) ? "생방(산건)" : ("N".equals(argType) ? "산건위"   : "생 방 송 (산업건설위원회)");
			} else if( "CS".equals(argValue) ) { 
				retS = "S".equals(argType) ? "의사(시)"   : ("N".equals(argType) ? "시의회"   : "의사일정 (시의회)");
			} else if( "OS".equals(argValue) ) { 
				retS = "S".equals(argType) ? "의사(운영)" : ("N".equals(argType) ? "운영위"   : "의사일정 (의회운영위원회)");
			} else if( "GS".equals(argValue) ) { 
//				retS = "S".equals(argType) ? "의사(자치)" : ("N".equals(argType) ? "자치위"   : "의사일정 (자치행정위원회)");
				retS = "S".equals(argType) ? "의사(기획)" : ("N".equals(argType) ? "기획위"   : "의사일정 (기획행정위원회)");
			} else if( "WS".equals(argValue) ) {
				retS = "S".equals(argType) ? "의사(복지)" : ("N".equals(argType) ? "복지위"   : "의사일정 (복지환경위원회)");
			} else if( "BS".equals(argValue) ) { 
				retS = "S".equals(argType) ? "의사(산건)" : ("N".equals(argType) ? "산건위"   : "의사일정 (산업건설위원회)");
			} else if( "CW".equals(argValue) ) { 
				retS = "S".equals(argType) ? "방청"       : ("N".equals(argType) ? "방청견학" : "방청견학");
			}
		} catch( Exception E ) { 
			retS = "";
		}

		return retS;
	}	// end of getSchdTypeName

	/**
	 * <pre>
	 * 인터넷방송을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/cast/{paramPath}.do")
	public String scrListOfStudio ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					retPath      = "";
		String					lstType      = "";

		int						listCnt      = 0;
		int						chldCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseAgendaVO>		dataList     = new ArrayList<BaseAgendaVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		eraList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		rndList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		tempList     = new ArrayList<BaseCommonVO>();
		BaseCommonVO			tempView     = new BaseCommonVO();

		List<BaseReschdVO>		schdList     = new ArrayList<BaseReschdVO>();
		List<BaseReschdVO>		schdChld     = new ArrayList<BaseReschdVO>();
		String[]				schdType     = new String[12];
		StringBuffer			schdDocu     = new StringBuffer();
		StringBuffer			liveDocu     = new StringBuffer();
log.debug(" act: "+ BaseUtility.spaceFill( "Studio"+ BaseUtility.toCapitalize( paramPath ) +"List", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "base".equalsIgnoreCase(paramPath) ) { 
			retPath  = "cast/"+ paramPath.toLowerCase() +"_list";
		} else if( "live".equalsIgnoreCase(paramPath) ) { 
			retPath  = "cast/"+ paramPath.toLowerCase() +"_list";
		} else if( "general".equalsIgnoreCase(paramPath) ) { 
//			lstType  = "GEN";
//			retPath  = "cast/"+ paramPath.toLowerCase() +"_list";

			String cateNum = request.getParameter("ct");
			System.out.println(">>>>>>>>>>>> cateNum : " + cateNum);

			if ("1".equals(cateNum)) lstType  = "GEN"; // 본회의공지사항
			else if ("2".equals(cateNum)) lstType  = "SND1"; // 의회운영위원회
			else if ("3".equals(cateNum)) lstType  = "SND2"; // 기획행정위원회
			else if ("4".equals(cateNum)) lstType  = "SND3"; // 복지환경위원회
            else if ("5".equals(cateNum)) lstType  = "SND4"; // 산업건설위원회
            else if ("6".equals(cateNum)) lstType  = "BGT"; // 예산결산특별위원회

			retPath  = "cast/"+ paramPath.toLowerCase() +"_list";

		} else if( "stand".equalsIgnoreCase(paramPath) ) { 
			lstType  = "SND";
			retPath  = "cast/"+ paramPath.toLowerCase() +"_list";
		} else if( "budget".equalsIgnoreCase(paramPath) ) { 
			lstType  = "BGT";
			retPath  = "cast/"+ paramPath.toLowerCase() +"_list";
		} else if( "audit".equalsIgnoreCase(paramPath) ) { 
			lstType  = "ADT";
			retPath  = "cast/"+ paramPath.toLowerCase() +"_list";
		} else if( "gover".equalsIgnoreCase(paramPath) ) { 
			retPath  = "cast/"+ paramPath.toLowerCase() +"_list";
		} else if( "speech".equalsIgnoreCase(paramPath) ) { 
			retPath  = "cast/"+ paramPath.toLowerCase() +"_list";
		}

		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		pageInfo.setTotalRecordCount(1);
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(10);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "N" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reEraCode"     , searchVO.getReEraCode());
		model.addAttribute("reRndCode"     , searchVO.getReRndCode());
		model.addAttribute("reRndDiv"      , searchVO.getReRndDiv());
		model.addAttribute("reKndCode"     , searchVO.getReKndCode());

		// 추가 조회조건 취득
//		searchVO.setCondCate(BaseUtility.null2Char( searchVO.getCondCate(), "%" ));
//		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		if( "live".equalsIgnoreCase(paramPath) ) { 
			searchVO.setReStdStat(BaseUtility.null2Char( searchVO.getReStdStat(), "C" ));
			searchVO.setReStdDate(BaseUtility.null2Char( searchVO.getReStdDate(), systemVO.getToYear() + systemVO.getToMonth() ).replaceAll("[^0-9]", ""));
			searchVO.setReStdDate("PY".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(), -12 ) : searchVO.getReStdDate());
			searchVO.setReStdDate("PM".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(),  -1 ) : searchVO.getReStdDate());
			searchVO.setReStdDate("NM".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(),   1 ) : searchVO.getReStdDate());
			searchVO.setReStdDate("NY".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(),  12 ) : searchVO.getReStdDate());
			searchVO.setReBgnDate(BaseUtility.getWeekInMonths( BaseUtility.getDateOfLast( searchVO.getReStdDate(), "F" ), "MF" ));
			searchVO.setReEndDate(BaseUtility.getWeekInMonths( BaseUtility.getDateOfLast( searchVO.getReStdDate(), "L" ), "ML" ));
			searchVO.setReLstType("LV");
		} else { 
			searchVO.setReBgnDate(BaseUtility.null2Blank( searchVO.getReBgnDate() ).replaceAll("[^0-9]", ""));
			searchVO.setReEndDate(BaseUtility.null2Blank( searchVO.getReEndDate() ).replaceAll("[^0-9]", ""));
			searchVO.setReLstType(lstType);
			searchVO.setReRndUsed(propertiesService.getString("castUnit"));
			searchVO.setReKndCode(BaseUtility.null2Char( searchVO.getReKndCode(), "%" ));
		}

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("bgnDate: "+ searchVO.getReBgnDate() +" |endDate: "+ searchVO.getReEndDate() +" |lstType: "+ searchVO.getReLstType());
log.debug("eraCode: "+ searchVO.getReEraCode() +" |rndCode: "+ searchVO.getReRndCode() +" |rndDiv: "+ searchVO.getReRndDiv());

		try { 
			if( !"live".equalsIgnoreCase(paramPath) && BaseUtility.isValidNull( searchVO.getReEraCode() ) ) { 
				// 분류구분 취득 - 현재 회의대수 구분
				tempView = (BaseCommonVO)baseCmmnService.getInfoOfClassNOW( "" );
				searchVO.setReEraCode(tempView == null ? BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsCate" ) : tempView.getEraCode());
				model.addAttribute("reEraCode"     , searchVO.getReEraCode());
log.debug("new eraCode: "+ searchVO.getReEraCode());
			}
			if( "base".equalsIgnoreCase(paramPath) ) { 
				listCnt = (int)agendaMngrService.getCountOfConfCast( searchVO );
			} else if( "live".equalsIgnoreCase(paramPath) ) { 
				listCnt = (int)reschdMngrService.getCountOfReschdCal( searchVO );
			} else if( "general".equalsIgnoreCase(paramPath) ) { 
				listCnt = (int)agendaMngrService.getCountOfConfCast( searchVO );
			} else if( "stand".equalsIgnoreCase(paramPath) ) { 
				listCnt = (int)agendaMngrService.getCountOfConfCast( searchVO );
			} else if( "budget".equalsIgnoreCase(paramPath) ) { 
				listCnt = (int)agendaMngrService.getCountOfConfCast( searchVO );
			} else if( "audit".equalsIgnoreCase(paramPath) ) { 
				listCnt = (int)agendaMngrService.getCountOfConfCast( searchVO );
			} else if( "gover".equalsIgnoreCase(paramPath) ) { 
				listCnt = (int)membMngrService.getCountOfMemb( searchVO );
			} else if( "speech".equalsIgnoreCase(paramPath) ) { 
				listCnt = (int)membMngrService.getCountOfMemb( searchVO );
			}

			if( "live".equalsIgnoreCase(paramPath) || "gover".equalsIgnoreCase(paramPath) || "speech".equalsIgnoreCase(paramPath) ) { 
				searchVO.setOrdMode("N");
				searchVO.setPagePerNo("all");
			}

			// 조회 건수를 전체선택인 경우
			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
			}
			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
			searchVO.setPageFirNo(Integer.toString(idxFir));
			searchVO.setPageLstNo(Integer.toString(idxLst));
			searchVO.setListCnt(Integer.toString(listCnt));

			BaseLiveVO 	liveVO 	= new BaseLiveVO();
			liveVO = (BaseLiveVO) boardMngrService.getDataLive();

			if( liveVO == null ) {
				liveVO.setUrlMain("");
				liveVO.setUrlOper("");
				liveVO.setUrlGvadm("");
				liveVO.setUrlWelfare("");
				liveVO.setUrlBuild("");
			}

			if( listCnt > 0 ) { 
				if( "base".equalsIgnoreCase(paramPath) ) { 
					dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfCast( searchVO );
//					dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfCast( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				} else if( "live".equalsIgnoreCase(paramPath) ) { 
					schdList = (List<BaseReschdVO>)reschdMngrService.getListOfReschdCal( searchVO );
//					schdList = (List<BaseReschdVO>)reschdMngrService.getListOfReschdCal( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
//log.debug("liveHost : "+ propertiesService.getString("liveHost"));
//log.debug("operHost : "+ propertiesService.getString("operHost"));
//log.debug("gvadmHost: "+ propertiesService.getString("gvadmHost"));
//log.debug("welfareHost: "+ propertiesService.getString("welfareHost"));
//log.debug("buildHost: "+ propertiesService.getString("buildHost"));
log.debug("liveHost(0): "+  liveVO.getUrlMain());
log.debug("operHost(0): "+ liveVO.getUrlOper());
log.debug("gvadmHost(0): "+ liveVO.getUrlGvadm());
log.debug("welfareHost(0): "+ liveVO.getUrlWelfare());
log.debug("buildHost(0): "+ liveVO.getUrlBuild());

					// Data Adjustment(Reschds)
					if( !(schdList == null || schdList.size() <= 0) ) { 
						schdDocu.setLength(0);
//						liveDocu.setLength(0);
						for( int d = 0; d < schdList.size(); d++ ) { 
							schdList.get(d).setViewNo(schdList.get(d).getStdUUID() + schdList.get(d).getRowSeq());

//							schdList.get(d).setStdDate(BaseUtility.toDateFormat( schdList.get(d).getStdDate(), dtSep, 8 ));

							if( d % 7 == 0 ) schdDocu.append("<tr>");
							schdDocu.append("\n\t\t\t\t\t\t\t").append("<td class='day'"+ (schdList.get(d).getStdDate().equals(systemVO.getToDate())?" style='background-color:#83d3ca;'":"") +">");
							schdDocu.append("\n\t\t\t\t\t\t\t\t").append("<div class='date"+ (d%7==0?" sun":(d%7==6?" sat":"")) +"'><span class='d_number'");
							schdDocu.append(schdList.get(d).getStdDate().substring(0,6).equals(searchVO.getReStdDate())?"":" style='opacity:0.3;'");
							schdDocu.append(">"+ Integer.parseInt(schdList.get(d).getStdDay()) +"</span></div>");
							schdDocu.append("\n\t\t\t\t\t\t\t\t").append("<div class='day-content'>");
							schdChld = (List<BaseReschdVO>)schdList.get(d).getChildItem();
							chldCnt  = schdChld == null || schdChld.size() <= 0 ? 0 : schdChld.size();
							if( chldCnt <= 0 || ("F".equals(propertiesService.getString("viewSun")) && BaseUtility.dayOfWeek( schdList.get(d).getStdDate() ) == 1) || ("F".equals(propertiesService.getString("viewSat")) && BaseUtility.dayOfWeek( schdList.get(d).getStdDate() ) == 7) ) { 
							} else { 
								schdDocu.append("<ul class='p_icon'><li>");
								for( int c = 0; c < chldCnt; c++ ) { 
									schdType = BaseUtility.null2Blank( schdChld.get(c).getSchdType() ).split("[`]", schdType.length);
									for( int s = 0; s < schdType.length-1; s ++ ) if( "CL".equals(schdType[s]) || "OL".equals(schdType[s])  || "GL".equals(schdType[s]) || "WL".equals(schdType[s])  || "BL".equals(schdType[s]) ) {
										schdDocu.append("\n\t\t\t\t\t\t\t\t\t").append("<p title='["+ getSchdTypeName( "N", schdType[s] ) +"] "+ schdChld.get(c).getSchdSTitle() +"'><a href='javascript:void(0);' onclick='fnActMonth(\""+ schdChld.get(c).getSchdUUID() +"\",\""+ schdList.get(d).getStdDate() +"\")'><span>");
										schdDocu.append(BaseUtility.toTimeFormat( schdChld.get(c).getSchdBTime() ) +"&nbsp; ");
										schdDocu.append(schdChld.get(c).getSchdSTitle()).append("</span></a></p>");
									}
//									if( schdList.get(d).getStdDate().equals(systemVO.getToDate()) ) { 
//										for( int s = 0; s < schdType.length-1; s ++ ) { 
//											if( "CL".equals(schdType[s]) ) liveDocu.append("<li>").append("<a href='"+ propertiesService.getString("liveHost")  +"' target='_new' title='"+ schdChld.get(c).getSchdTitle() +" "+ getSchdTypeName( "F", schdType[s] ) +" 새창열기'>["+ BaseUtility.toTimeFormat( schdChld.get(c).getSchdBTime() ) +"] "+ schdChld.get(c).getSchdTitle() +"</a>").append("T".equals(schdChld.get(c).getLiveBefore())?"<span class='bef_live'>방송준비중</span>":"").append("T".equals(schdChld.get(c).getLivePresent())?"<span class='live'>생방송중</span>":"").append("T".equals(schdChld.get(c).getLiveAfter())?"<span class='no_live'>방송종료</span>":"").append("</li>");
//											if( "OL".equals(schdType[s]) ) liveDocu.append("<li>").append("<a href='"+ propertiesService.getString("operHost")  +"' target='_new' title='"+ schdChld.get(c).getSchdTitle() +" "+ getSchdTypeName( "F", schdType[s] ) +" 새창열기'>["+ BaseUtility.toTimeFormat( schdChld.get(c).getSchdBTime() ) +"] "+ schdChld.get(c).getSchdTitle() +"</a>").append("T".equals(schdChld.get(c).getLiveBefore())?"<span class='bef_live'>방송준비중</span>":"").append("T".equals(schdChld.get(c).getLivePresent())?"<span class='live'>생방송중</span>":"").append("T".equals(schdChld.get(c).getLiveAfter())?"<span class='no_live'>방송종료</span>":"").append("</li>");
//											if( "GL".equals(schdType[s]) ) liveDocu.append("<li>").append("<a href='"+ propertiesService.getString("gvadmHost") +"' target='_new' title='"+ schdChld.get(c).getSchdTitle() +" "+ getSchdTypeName( "F", schdType[s] ) +" 새창열기'>["+ BaseUtility.toTimeFormat( schdChld.get(c).getSchdBTime() ) +"] "+ schdChld.get(c).getSchdTitle() +"</a>").append("T".equals(schdChld.get(c).getLiveBefore())?"<span class='bef_live'>방송준비중</span>":"").append("T".equals(schdChld.get(c).getLivePresent())?"<span class='live'>생방송중</span>":"").append("T".equals(schdChld.get(c).getLiveAfter())?"<span class='no_live'>방송종료</span>":"").append("</li>");
//											if( "BL".equals(schdType[s]) ) liveDocu.append("<li>").append("<a href='"+ propertiesService.getString("buildHost") +"' target='_new' title='"+ schdChld.get(c).getSchdTitle() +" "+ getSchdTypeName( "F", schdType[s] ) +" 새창열기'>["+ BaseUtility.toTimeFormat( schdChld.get(c).getSchdBTime() ) +"] "+ schdChld.get(c).getSchdTitle() +"</a>").append("T".equals(schdChld.get(c).getLiveBefore())?"<span class='bef_live'>방송준비중</span>":"").append("T".equals(schdChld.get(c).getLivePresent())?"<span class='live'>생방송중</span>":"").append("T".equals(schdChld.get(c).getLiveAfter())?"<span class='no_live'>방송종료</span>":"").append("</li>");
//										}
//									}
								}
								schdDocu.append("\n\t\t\t\t\t\t\t\t").append("</li></ul>");
							}
							schdDocu.append("</div>").append("\n\t\t\t\t\t\t\t").append("</td>");
							if( d % 7 == 6 ) schdDocu.append("\n\t\t\t\t\t\t").append("</tr>");
						}
					}
				} else if( "general".equalsIgnoreCase(paramPath) ) { 
					dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfCast( searchVO );
//					dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfCast( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				} else if( "stand".equalsIgnoreCase(paramPath) ) { 
					dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfCast( searchVO );
//					dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfCast( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				} else if( "budget".equalsIgnoreCase(paramPath) ) { 
					dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfCast( searchVO );
//					dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfCast( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				} else if( "audit".equalsIgnoreCase(paramPath) ) { 
					dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfCast( searchVO );
//					dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfCast( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				} else if( "gover".equalsIgnoreCase(paramPath) ) { 
					dataList = (List<BaseAgendaVO>)membMngrService.getListOfMemb( searchVO );
//					dataList = (List<BaseAgendaVO>)membMngrService.getListOfMemb( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				} else if( "speech".equalsIgnoreCase(paramPath) ) { 
					dataList = (List<BaseAgendaVO>)membMngrService.getListOfMemb( searchVO );
//					dataList = (List<BaseAgendaVO>)membMngrService.getListOfMemb( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				}

				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
				pageInfo.setTotalRecordCount(listCnt);
			}

			if( "live".equalsIgnoreCase(paramPath) ) { 
				tempView = new BaseCommonVO();
				tempView.setReStdDate(systemVO.getToDate());
				tempView.setReLstType("LV");
				schdList = (List<BaseReschdVO>)reschdMngrService.getChldOfReschdCal( tempView );
//				schdList = (List<BaseReschdVO>)reschdMngrService.getChldOfReschdCal( tempView, tempView.getPageCurNo(), tempView.getPagePerNo() );

				// Data Adjustment(Reschds)
				if( !(schdList == null || schdList.size() <= 0) ) { 
//log.debug("liveHost : "+ propertiesService.getString("liveHost"));
//log.debug("operHost : "+ propertiesService.getString("operHost"));
//log.debug("gvadmHost: "+ propertiesService.getString("gvadmHost"));
//log.debug("welfareHost: "+ propertiesService.getString("welfareHost"));
//log.debug("buildHost: "+ propertiesService.getString("buildHost"));
log.debug("liveHost(1): "+  liveVO.getUrlMain());
log.debug("operHost(1): "+ liveVO.getUrlOper());
log.debug("gvadmHost(1): "+ liveVO.getUrlGvadm());
log.debug("welfareHost(1): "+ liveVO.getUrlWelfare());
log.debug("buildHost(1): "+ liveVO.getUrlBuild());

					liveDocu.setLength(0);
					for( int d = 0; d < schdList.size(); d++ ) { 
						schdList.get(d).setViewNo(schdList.get(d).getStdUUID() + schdList.get(d).getRowSeq());

						schdList.get(d).setSchdBDate(BaseUtility.toDateFormat( schdList.get(d).getSchdBDate(), dtSep, 8 ));
						schdList.get(d).setSchdEDate(BaseUtility.toDateFormat( schdList.get(d).getSchdEDate(), dtSep, 8 ));
						schdList.get(d).setSchdBTime(BaseUtility.toTimeFormat( schdList.get(d).getSchdBTime() ));
						schdList.get(d).setSchdETime(BaseUtility.toTimeFormat( schdList.get(d).getSchdETime() ));

						schdType = BaseUtility.null2Blank( schdList.get(d).getSchdType() ).split("[`]", schdType.length);
						for( int s = 0; s < schdType.length-1; s ++ ) { 
//							if( "CL".equals(schdType[s]) ) liveDocu.append("<li>").append("<a href='"+ propertiesService.getString("liveHost")  +"' target='_new' title='"+ schdList.get(d).getSchdTitle() +" "+ getSchdTypeName( "F", schdType[s] ) +" 새창열기'>["+ schdList.get(d).getSchdBTime() +"] "+ schdList.get(d).getSchdTitle() +"</a>").append("T".equals(schdList.get(d).getLiveBefore())?"<span class='bef_live'>방송준비중</span>":"").append("T".equals(schdList.get(d).getLivePresent())?"<span class='live'>생방송중</span>":"").append("T".equals(schdList.get(d).getLiveAfter())?"<span class='no_live'>방송종료</span>":"").append("</li>");
//							if( "OL".equals(schdType[s]) ) liveDocu.append("<li>").append("<a href='"+ propertiesService.getString("operHost")  +"' target='_new' title='"+ schdList.get(d).getSchdTitle() +" "+ getSchdTypeName( "F", schdType[s] ) +" 새창열기'>["+ schdList.get(d).getSchdBTime() +"] "+ schdList.get(d).getSchdTitle() +"</a>").append("T".equals(schdList.get(d).getLiveBefore())?"<span class='bef_live'>방송준비중</span>":"").append("T".equals(schdList.get(d).getLivePresent())?"<span class='live'>생방송중</span>":"").append("T".equals(schdList.get(d).getLiveAfter())?"<span class='no_live'>방송종료</span>":"").append("</li>");
//							if( "GL".equals(schdType[s]) ) liveDocu.append("<li>").append("<a href='"+ propertiesService.getString("gvadmHost") +"' target='_new' title='"+ schdList.get(d).getSchdTitle() +" "+ getSchdTypeName( "F", schdType[s] ) +" 새창열기'>["+ schdList.get(d).getSchdBTime() +"] "+ schdList.get(d).getSchdTitle() +"</a>").append("T".equals(schdList.get(d).getLiveBefore())?"<span class='bef_live'>방송준비중</span>":"").append("T".equals(schdList.get(d).getLivePresent())?"<span class='live'>생방송중</span>":"").append("T".equals(schdList.get(d).getLiveAfter())?"<span class='no_live'>방송종료</span>":"").append("</li>");
//							if( "WL".equals(schdType[s]) ) liveDocu.append("<li>").append("<a href='"+ propertiesService.getString("welfareHost") +"' target='_new' title='"+ schdList.get(d).getSchdTitle() +" "+ getSchdTypeName( "F", schdType[s] ) +" 새창열기'>["+ schdList.get(d).getSchdBTime() +"] "+ schdList.get(d).getSchdTitle() +"</a>").append("T".equals(schdList.get(d).getLiveBefore())?"<span class='bef_live'>방송준비중</span>":"").append("T".equals(schdList.get(d).getLivePresent())?"<span class='live'>생방송중</span>":"").append("T".equals(schdList.get(d).getLiveAfter())?"<span class='no_live'>방송종료</span>":"").append("</li>");
//							if( "BL".equals(schdType[s]) ) liveDocu.append("<li>").append("<a href='"+ propertiesService.getString("buildHost") +"' target='_new' title='"+ schdList.get(d).getSchdTitle() +" "+ getSchdTypeName( "F", schdType[s] ) +" 새창열기'>["+ schdList.get(d).getSchdBTime() +"] "+ schdList.get(d).getSchdTitle() +"</a>").append("T".equals(schdList.get(d).getLiveBefore())?"<span class='bef_live'>방송준비중</span>":"").append("T".equals(schdList.get(d).getLivePresent())?"<span class='live'>생방송중</span>":"").append("T".equals(schdList.get(d).getLiveAfter())?"<span class='no_live'>방송종료</span>":"").append("</li>");
							if( "CL".equals(schdType[s]) ) liveDocu.append("<li>").append("<a href='"+ liveVO.getUrlMain()  +"' target='_new' title='"+ schdList.get(d).getSchdTitle() +" "+ getSchdTypeName( "F", schdType[s] ) +" 새창열기'>["+ schdList.get(d).getSchdBTime() +"] "+ schdList.get(d).getSchdTitle() +"</a>").append("T".equals(schdList.get(d).getLiveBefore())?"<span class='bef_live'>방송준비중</span>":"").append("T".equals(schdList.get(d).getLivePresent())?"<span class='live'>생방송중</span>":"").append("T".equals(schdList.get(d).getLiveAfter())?"<span class='no_live'>방송종료</span>":"").append("</li>");
							if( "OL".equals(schdType[s]) ) liveDocu.append("<li>").append("<a href='"+ liveVO.getUrlOper()  +"' target='_new' title='"+ schdList.get(d).getSchdTitle() +" "+ getSchdTypeName( "F", schdType[s] ) +" 새창열기'>["+ schdList.get(d).getSchdBTime() +"] "+ schdList.get(d).getSchdTitle() +"</a>").append("T".equals(schdList.get(d).getLiveBefore())?"<span class='bef_live'>방송준비중</span>":"").append("T".equals(schdList.get(d).getLivePresent())?"<span class='live'>생방송중</span>":"").append("T".equals(schdList.get(d).getLiveAfter())?"<span class='no_live'>방송종료</span>":"").append("</li>");
							if( "GL".equals(schdType[s]) ) liveDocu.append("<li>").append("<a href='"+ liveVO.getUrlGvadm() +"' target='_new' title='"+ schdList.get(d).getSchdTitle() +" "+ getSchdTypeName( "F", schdType[s] ) +" 새창열기'>["+ schdList.get(d).getSchdBTime() +"] "+ schdList.get(d).getSchdTitle() +"</a>").append("T".equals(schdList.get(d).getLiveBefore())?"<span class='bef_live'>방송준비중</span>":"").append("T".equals(schdList.get(d).getLivePresent())?"<span class='live'>생방송중</span>":"").append("T".equals(schdList.get(d).getLiveAfter())?"<span class='no_live'>방송종료</span>":"").append("</li>");
							if( "WL".equals(schdType[s]) ) liveDocu.append("<li>").append("<a href='"+ liveVO.getUrlWelfare() +"' target='_new' title='"+ schdList.get(d).getSchdTitle() +" "+ getSchdTypeName( "F", schdType[s] ) +" 새창열기'>["+ schdList.get(d).getSchdBTime() +"] "+ schdList.get(d).getSchdTitle() +"</a>").append("T".equals(schdList.get(d).getLiveBefore())?"<span class='bef_live'>방송준비중</span>":"").append("T".equals(schdList.get(d).getLivePresent())?"<span class='live'>생방송중</span>":"").append("T".equals(schdList.get(d).getLiveAfter())?"<span class='no_live'>방송종료</span>":"").append("</li>");
							if( "BL".equals(schdType[s]) ) liveDocu.append("<li>").append("<a href='"+ liveVO.getUrlBuild() +"' target='_new' title='"+ schdList.get(d).getSchdTitle() +" "+ getSchdTypeName( "F", schdType[s] ) +" 새창열기'>["+ schdList.get(d).getSchdBTime() +"] "+ schdList.get(d).getSchdTitle() +"</a>").append("T".equals(schdList.get(d).getLiveBefore())?"<span class='bef_live'>방송준비중</span>":"").append("T".equals(schdList.get(d).getLivePresent())?"<span class='live'>생방송중</span>":"").append("T".equals(schdList.get(d).getLiveAfter())?"<span class='no_live'>방송종료</span>":"").append("</li>");
						}
					}
				}
			}

			// 분류구분 취득 - 전체 회의대수 구분
			eraList = (List<BaseCommonVO>)baseCmmnService.getListOfClassALL( "" );
			// 분류구분 취득 - 회의회수 구분
			rndList = (List<BaseCommonVO>)baseCmmnService.getListOfRoundALL( searchVO.getReEraCode() );

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			model.addAttribute("dataList"      , dataList);
			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (dataList == null ? 0 : dataList.size()) : listCnt ));
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("eraList"       , eraList);
			model.addAttribute("rndList"       , rndList);
			model.addAttribute("liveDocu"      , liveDocu.toString());
			model.addAttribute("dispDocu"      , schdDocu.toString());
			model.addAttribute("dispDate"      , BaseUtility.toDateFormat( searchVO.getReStdDate(), dtSep, 6 ));
			model.addAttribute("reStdStat"     , searchVO.getReStdStat());
			model.addAttribute("reStdDate"     , BaseUtility.toDateFormat( searchVO.getReStdDate(), dtSep ));
			model.addAttribute("reCurrDate"    , BaseUtility.toDateFormat( systemVO.getToYear() + systemVO.getToMonth(), dtSep ));
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/coun/"+retPath;
	}	// end of scrListOfStudio

	/**
	 * <pre>
	 * 의사일정을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/schd/monthList.do")
	public String scrListOfSchdMonth ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					retPath      = "acts/monthSchd_list";

		int						listCnt      = 0;
		int						chldCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseReschdVO>		schdList     = new ArrayList<BaseReschdVO>();
		List<BaseReschdVO>		schdChld     = new ArrayList<BaseReschdVO>();
		String[]				schdType     = new String[12];
		StringBuffer			schdDocu     = new StringBuffer();
log.debug(" act: "+ BaseUtility.spaceFill( "MonthSchdListList", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		pageInfo.setTotalRecordCount(1);
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(10);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
//		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setPagePerNo("all");
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "N" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReStdStat(BaseUtility.null2Char( searchVO.getReStdStat(), "C" ));
		searchVO.setReStdDate(BaseUtility.null2Char( searchVO.getReStdDate(), systemVO.getToYear() + systemVO.getToMonth() ).replaceAll("[^0-9]", ""));
		searchVO.setReStdDate("PY".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(), -12 ) : searchVO.getReStdDate());
		searchVO.setReStdDate("PM".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(),  -1 ) : searchVO.getReStdDate());
		searchVO.setReStdDate("NM".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(),   1 ) : searchVO.getReStdDate());
		searchVO.setReStdDate("NY".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(),  12 ) : searchVO.getReStdDate());
		searchVO.setReBgnDate(BaseUtility.getWeekInMonths( BaseUtility.getDateOfLast( searchVO.getReStdDate(), "F" ), "MF" ));
		searchVO.setReEndDate(BaseUtility.getWeekInMonths( BaseUtility.getDateOfLast( searchVO.getReStdDate(), "L" ), "ML" ));
		searchVO.setReLstType("A");

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("stdDate: "+ searchVO.getReStdDate() +" |stdStat: "+ searchVO.getReStdStat());
log.debug("bgnDate: "+ searchVO.getReBgnDate() +" |endDate: "+ searchVO.getReEndDate() +" |lstType: "+ searchVO.getReLstType());
		try { 
			listCnt = (int)reschdMngrService.getCountOfReschdCal( searchVO );

			// 조회 건수를 전체선택인 경우
			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
			}
			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
			searchVO.setPageFirNo(Integer.toString(idxFir));
			searchVO.setPageLstNo(Integer.toString(idxLst));
			searchVO.setListCnt(Integer.toString(listCnt));

			if( listCnt > 0 ) { 
				schdList = (List<BaseReschdVO>)reschdMngrService.getListOfReschdCal( searchVO );
//				schdList = (List<BaseReschdVO>)reschdMngrService.getListOfReschdCal( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				// Data Adjustment(Reschds)
				if( !(schdList == null || schdList.size() <= 0) ) { 
					schdDocu.setLength(0);
					for( int d = 0; d < schdList.size(); d++ ) { 
						schdList.get(d).setViewNo(schdList.get(d).getStdUUID() + schdList.get(d).getRowSeq());

//						schdList.get(d).setStdDate(BaseUtility.toDateFormat( schdList.get(d).getStdDate(), dtSep, 8 ));

						if( d % 7 == 0 ) schdDocu.append("<tr>");
						schdDocu.append("\n\t\t\t\t\t\t\t").append("<td class='day'"+ (schdList.get(d).getStdDate().equals(systemVO.getToDate())?" style='background-color:#83d3ca;'":"") +">");
						schdDocu.append("\n\t\t\t\t\t\t\t\t").append("<div class='date"+ (d%7==0?" sun":(d%7==6?" sat":"")) +"'><span class='d_number'");
						schdDocu.append(schdList.get(d).getStdDate().substring(0,6).equals(searchVO.getReStdDate())?"":" style='opacity:0.3;'");
						schdDocu.append(">"+ Integer.parseInt(schdList.get(d).getStdDay()) +"</span></div>");
						schdDocu.append("\n\t\t\t\t\t\t\t\t").append("<div class='day-content'>");
						schdChld = (List<BaseReschdVO>)schdList.get(d).getChildItem();
						chldCnt  = schdChld == null || schdChld.size() <= 0 ? 0 : schdChld.size();
						if( chldCnt <= 0 || ("F".equals(propertiesService.getString("viewSun")) && BaseUtility.dayOfWeek( schdList.get(d).getStdDate() ) == 1) || ("F".equals(propertiesService.getString("viewSat")) && BaseUtility.dayOfWeek( schdList.get(d).getStdDate() ) == 7) ) { 
						} else { 
							schdDocu.append("<ul class='p_icon'><li>");
							for( int c = 0; c < chldCnt; c++ ) { 
								schdType = BaseUtility.null2Blank( schdChld.get(c).getSchdType() ).split("[`]", schdType.length);
								for( int s = 0; s < schdType.length-1; s ++ ) if( "CS".equals(schdType[s]) || "OS".equals(schdType[s])  || "GS".equals(schdType[s]) || "WS".equals(schdType[s])  || "BS".equals(schdType[s]) ) {
									schdDocu.append("\n\t\t\t\t\t\t\t\t\t").append("<p title='["+ getSchdTypeName( "N", schdType[s] ) +"] "+ schdChld.get(c).getSchdSTitle() +"'><a href='javascript:void(0);' onclick='fnActMonth(\""+ schdChld.get(c).getSchdUUID() +"\",\""+ schdList.get(d).getStdDate() +"\")'><span>");
									schdDocu.append(BaseUtility.toTimeFormat( schdChld.get(c).getSchdBTime() ) +"&nbsp; ");
									schdDocu.append(schdChld.get(c).getSchdSTitle()).append("</span></a></p>");
								}
							}
							schdDocu.append("\n\t\t\t\t\t\t\t\t").append("</li></ul>");
						}
						schdDocu.append("</div>").append("\n\t\t\t\t\t\t\t").append("</td>");
						if( d % 7 == 6 ) schdDocu.append("\n\t\t\t\t\t\t").append("</tr>");
					}
				}

				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
				pageInfo.setTotalRecordCount(listCnt);
			}

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
//			model.addAttribute("dataList"      , dataList);
//			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (dataList == null ? 0 : dataList.size()) : listCnt ));
//			model.addAttribute("pageInfo"      , pageInfo);
//			model.addAttribute("cateList"      , cateList);
			model.addAttribute("dispDocu"      , schdDocu.toString());
			model.addAttribute("dispDate"      , BaseUtility.toDateFormat( searchVO.getReStdDate(), dtSep, 6 ));
			model.addAttribute("reStdStat"     , "C");
			model.addAttribute("reStdDate"     , BaseUtility.toDateFormat( searchVO.getReStdDate(), dtSep ));
			model.addAttribute("reCurrDate"    , BaseUtility.toDateFormat( systemVO.getToYear() + systemVO.getToMonth(), dtSep ));
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/coun/"+retPath;
	}	// end of scrListOfSchdMonth

	/**
	 * <pre>
	 * 의사일정을(를) 상세조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/monthView.do")
	public String scrAjaxOfSchdMonth ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		BaseReschdVO			dataView     = new BaseReschdVO();
		StringBuffer			dataRult     = new StringBuffer();
log.debug(" act: "+ BaseUtility.spaceFill( "MonthCounView", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 추가 조회조건 취득
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));
		searchVO.setActMode("L".equals(searchVO.getActMode()) ? "생방송일정" : "의사일정");

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)reschdMngrService.getInfoOfReschdUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(게시판)
				searchVO.setSchdYear(viewNo[0]);
				searchVO.setSchdSeq(viewNo[1]);
log.debug("viewNo: "+ searchVO.getSchdYear() +" | "+ searchVO.getSchdSeq());

				dataView = (BaseReschdVO)reschdMngrService.getDetailOfReschd( searchVO );
				if( dataView != null ) { 
					// Data Adjustment(Reschds)
					dataView.setActMode("Update");
					dataView.setViewNo(dataView.getSchdUUID());

					dataView.setSchdBDate(BaseUtility.toDateFormat( dataView.getSchdBDate(), dtSep, 8 ));
					dataView.setSchdEDate(BaseUtility.toDateFormat( dataView.getSchdEDate(), dtSep, 8 ));
					dataView.setSchdBTime(BaseUtility.toTimeFormat( dataView.getSchdBTime() ));
					dataView.setSchdETime(BaseUtility.toTimeFormat( dataView.getSchdETime() ));
					dataView.setSchdDescBR(BaseUtility.tag2Sym( dataView.getSchdDesc() ));

					dataRult.append("<h1>"+BaseUtility.toDateFormat( BaseUtility.null2Blank( searchVO.getStdDate() ).replaceAll("[^0-9]", ""), 8, "YM" ) +" ["+ BaseUtility.date2Week( searchVO.getStdDate() ) +"요일] &nbsp; "+ searchVO.getActMode() +"</h1>");
					dataRult.append("<div class='layer_media'><dl class='clender_pop'>");
					dataRult.append("<dt>일정명</dt><dd><ul><li><strong>"+ dataView.getSchdTitle() +"</strong></li></ul></dd>");
					dataRult.append("<dt>기간</dt><dd><ul><li>"+ dataView.getSchdBDate() +" "+ dataView.getSchdBTime() +" ~ "+ dataView.getSchdEDate() +" "+ dataView.getSchdETime() +"</li></ul></dd>");
					dataRult.append("<dt>장소</dt><dd><ul><li>"+ BaseUtility.null2Nbsp( dataView.getSchdPlace() ) +"</li></ul></dd>");
					dataRult.append("<dt>일정내용</dt><dd><ul><li>"+ BaseUtility.null2Nbsp( dataView.getSchdDescBR() ) +"</li></ul></dd>");
					dataRult.append("</dl></div>");
					dataRult.append("<div class='close_btn'><a href='javascript:void(0);' class='btn_c'><img src='"+ webBase +"/images/coun/popup_close.png' alt='닫기'/></a></div>");
				}
			}

//log.debug(dataRult.toString());
			response.setContentType("text/html;charset=utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(dataRult.toString());
			response.getWriter().flush();
			response.getWriter().close();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return null;
	}	// end of scrAjaxOfSchdMonth

}	// end of class