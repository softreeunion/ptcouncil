/*******************************************************************************
  Program ID  : NoticeCounController
  Description : 클라이언트의 요청(공통)을 처리할 메서드를 구현
  Author      : HexaMedia new0man
  Write Date  : 2019.06.04
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v1.0    (주)헥사미디어 개발자  2014.00.00  최초 생성
  v2.0    HexaMedia new0man      2019.06.04  평택시의회 Site Renewal
  v2.1    HexaMedia new0man      2020.03.17  비공개 목록 표시 및 검색조건 추가
 *******************************************************************************/
package egovframework.ptcc.coun.content.web;

import java.security.*;
import java.security.spec.*;
import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.dao.*;
import org.springframework.jdbc.*;
import org.springframework.jdbc.datasource.*;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.*;
import org.springframework.transaction.support.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.*;
import org.springframework.web.servlet.*;
import org.springframework.web.util.WebUtils;
import org.springmodules.validation.commons.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.service.interceptor.*;
import egovframework.ptcc.cmmn.util.*;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.board.service.*;
import egovframework.ptcc.mngr.login.service.*;
import egovframework.ptcc.mngr.member.service.UserMngrService;

@Controller
@RequestMapping(value="/coun")
public class NoticeCounController implements HandlerExceptionResolver {

	@Autowired
	private WebApplicationContext			webContext;
	@Autowired
	private DataSourceTransactionManager	txManager;

	@Resource(name="beanValidator")
	protected DefaultBeanValidator			beanValidator;
	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="loginMngrService")
	protected LoginMngrService				loginMngrService; 
	@Resource(name="boardMngrService")
	protected BoardMngrService				boardMngrService;
	@Resource(name="userMngrService")
	protected UserMngrService				userMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected String						hideMark     = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.HideMark" );
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));
	protected String						defsPass     = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsPass" );
	protected String						replyImage   = "<img src='@@/images/board_reply_icon.png' alt='reply'/>";
	protected String						lnkName      = "PT_BOARD";
	protected String						bbsType      = "10150010";
	protected boolean						notiItems    = false;
	protected boolean						moveItems    = true;
	protected boolean						fileUP       = true;
	protected boolean						tempUP       = false;
	protected boolean						inLogin      = false;
	protected boolean						removeSign   = true;

	private String							RSA_WEB_KEY  = "__rsaPrivateKey__";	// 개인키 session key
	private String							RSA_INSTANCE = "RSA";				// rsa transformation

	private void initRSA ( 
			HttpServletRequest request 
		) throws Exception { 

		HttpSession				session      = request == null ? null : request.getSession();
		KeyPairGenerator		generator;

		try { 
			generator = KeyPairGenerator.getInstance(this.RSA_INSTANCE);
			generator.initialize(1024);

			KeyPair keyPair = generator.genKeyPair();
			KeyFactory keyFactory = KeyFactory.getInstance(this.RSA_INSTANCE);

			PublicKey publicKey = keyPair.getPublic();
			PrivateKey privateKey = keyPair.getPrivate();

			// 세션에 공개키의 문자열을 키로하여 개인키를 저장한다.
			session.setAttribute(this.RSA_WEB_KEY, privateKey);

			// 공개키를 문자열로 변환하여 JavaScript RSA 라이브러리 넘겨준다.
			RSAPublicKeySpec publicSpec = (RSAPublicKeySpec) keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);
			String publicKeyModulus = publicSpec.getModulus().toString(16);
			String publicKeyExponent = publicSpec.getPublicExponent().toString(16);
			request.setAttribute("RSAModulus", publicKeyModulus); // rsa modulus 를 request 에 추가
			request.setAttribute("RSAExponent", publicKeyExponent); // rsa exponent 를 request 에 추가
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		}
	}


	/**
	 * <pre>
	 * 게시판 - 로그인 화면으로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath}/noticeHuin.do")
	public String scrLoginOfNotice ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		BaseCommonVO			searchSess   = (BaseCommonVO)WebUtils.getSessionAttribute(request, "searchSess");
		String					retPath      = "";

		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Huin", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_huin";
		} else if( "arch".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150090";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_huin";
		}
		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 세션의 기본 조회조건 취득
		if( !BaseUtility.isValidNull( searchSess ) ) { 
			searchVO = searchSess;
			session.removeAttribute("searchSess");
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		if( BaseUtility.isValidNull( searchVO.getNextMode() ) ) { 
log.debug("REFERER :"+ request.getHeader("REFERER"));
			if( request.getHeader("REFERER").contains("List") ) { 
				model.addAttribute("nextMode"      , "List");
			} else if( request.getHeader("REFERER").contains("View") ) { 
				model.addAttribute("nextMode"      , "View");
			} else if( request.getHeader("REFERER").contains("Inst") ) { 
				model.addAttribute("nextMode"      , "Inst");
			} else if( request.getHeader("REFERER").contains("Edit") ) { 
				model.addAttribute("nextMode"      , "Edit");
			} else if( request.getHeader("REFERER").contains("Create") ) { 
				model.addAttribute("nextMode"      , "List");
			} else if( request.getHeader("REFERER").contains("Update") ) { 
				model.addAttribute("nextMode"      , "List");
			} else if( request.getHeader("REFERER").contains("Delete") ) { 
				model.addAttribute("nextMode"      , "View");
			} else if( request.getHeader("REFERER").contains("Modify") ) { 
				model.addAttribute("nextMode"      , "View");
			} else { 
				model.addAttribute("nextMode"      , "List");
			}
		} else { 
			model.addAttribute("nextMode"      , searchVO.getNextMode());
		}
		this.initRSA(request);
log.debug("nextMode: "+ (String)model.get("nextMode"));

		return "/coun/"+retPath;
	}	// end of scrLoginOfNotice

	/**
	 * <pre>
	 * 게시판 - 로그인 처리를 해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath}/noticeHuin_proc.do")
	public String doLoginOfNotice ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseMemberVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		PrivateKey				privateKey   = (PrivateKey)session.getAttribute(this.RSA_WEB_KEY);
		BaseMemberVO			resultVO     = new BaseMemberVO();
		String					resultPID    = "";
		String					resultPWD    = "";
		String					returnMSG    = "";
		String					retPath      = "";

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			retPath  = "/coun/"+ paramPath.toLowerCase() +"/noticeList.do";
			inLogin  = false;
		} else if( "arch".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150090";
			retPath  = "/coun/"+ paramPath.toLowerCase() +"/noticeList.do";
			inLogin  = true;
		}

		try { 
			if( !(BaseUtility.isValidNull( searchVO.getJ_username() ) || BaseUtility.isValidNull( searchVO.getJ_userpass() )) ) { 
				resultPID = BaseCategory.decryptRSA( privateKey, searchVO.getJ_username() );
				resultPWD = BaseCategory.decryptRSA( privateKey, searchVO.getJ_userpass() );
				session.removeAttribute(this.RSA_WEB_KEY);	// 재사용 금지
				resultVO = loginMngrService.getInfoOfUserGngr( resultPID );

				if( resultVO != null ) { 
//log.debug("saved: "+ resultPWD);
//log.debug("input: "+ BaseCategory.encryptSHA256( resultPID + resultVO.getBgnDate() + resultPWD ));
					if( "T".equals(resultVO.getAuthFlag()) ) { 
						if( resultVO.getUserPID().equals(resultPID) && resultVO.getUserPWD().toLowerCase().equals(BaseCategory.encryptSHA256( resultPID + resultVO.getBgnDate() + resultPWD )) ) { 
							if( "T".equals(resultVO.getPwdDateUse()) && "T".equals(propertiesService.getString("dateTerm")) && !resultPWD.equals(defsPass) ) 
								if( "".equals(resultVO.getPwdDateMod()) || systemVO.getToDateMN3().equals(resultVO.getPwdDateMod()) ) { 
									model.addAttribute("message"       , "3개월 이상 접속을 하지 않아 사용이 중지되었습니다.\\n\\n관리자에 문의바랍니다.");
									model.addAttribute("returnUrl"     , webBase + retPath);
									return "/cmmn/exe_script";
								}

							// 최종접속 일시 갱신
							loginMngrService.doRtnOfUpdateMember( resultPID );
							request.getSession().setAttribute("MemUInfo"      , resultVO);

							// 중복로그인 방지
							AuthHttpSessionBindingListener listener = new AuthHttpSessionBindingListener();
							request.getSession().setAttribute(resultPID, listener);

							if( resultPWD.equals(defsPass) ) { 
								request.getSession().setAttribute("viewNo"        , resultVO.getUserUUID());
								model.addAttribute("message"       , "임시 비밀번호를 사용하고 있습니다.\\n\\n비밀번호 변경화면으로 이동합니다.");
								model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticePass.do");
								return "/cmmn/exe_script";
							}
							return "redirect:"+retPath;
						} else { 
							returnMSG = "아이디 및 패스워드 정보가 일치하지 않습니다.";
						}
					} else { 
						returnMSG = "사용승인이 되지 않았습니다.\\n\\n관리자에게 문의바랍니다.";
					}
				} else { 
					returnMSG = "아이디 및 패스워드 정보가 일치하지 않습니다.";
				}
			} else { 
				returnMSG = "아이디. 및 패스워드 정보가 일치하지 않습니다..";
			}
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			returnMSG = "아이디 및 패스워드. 정보가 일치하지 않습니다..";
		}

		model.addAttribute("message"       , returnMSG);
		model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeHuin.do");

		return "/cmmn/exe_script";
	}	// end of doLoginOfNotice

	/**
	 * <pre>
	 * 게시판 - 개인정보 수집화면으로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath}/noticeAgre.do")
	public String scrAgreOfNotice ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		BaseCommonVO			searchSess   = (BaseCommonVO)WebUtils.getSessionAttribute(request, "searchSess");
		String					retPath      = "";

		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Agre", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_agre";
		} else if( "hope".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150060";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_agre";
		} else if( "tip".equalsIgnoreCase(paramPath) ) {
			bbsType  = "10150150";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_agre";
		}

		System.out.println(">>>>>>>>>>> paramPath " + paramPath);
		System.out.println(">>>>>>>>>>> retPath " + retPath);

		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 세션의 기본 조회조건 취득
		if( !BaseUtility.isValidNull( searchSess ) ) { 
			searchVO = searchSess;
			session.removeAttribute("searchSess");
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reUseStat"     , searchVO.getReUseStat());
		if( BaseUtility.isValidNull( searchVO.getNextMode() ) ) { 
log.debug("REFERER :"+ request.getHeader("REFERER"));
			if( request.getHeader("REFERER").contains("List") ) { 
				model.addAttribute("nextMode"      , "List");
			} else if( request.getHeader("REFERER").contains("View") ) { 
				model.addAttribute("nextMode"      , "View");
			} else if( request.getHeader("REFERER").contains("Inst") ) { 
				model.addAttribute("nextMode"      , "Inst");
			} else if( request.getHeader("REFERER").contains("Edit") ) { 
				model.addAttribute("nextMode"      , "Edit");
			} else if( request.getHeader("REFERER").contains("Create") ) { 
				model.addAttribute("nextMode"      , "List");
			} else if( request.getHeader("REFERER").contains("Update") ) { 
				model.addAttribute("nextMode"      , "List");
			} else if( request.getHeader("REFERER").contains("Delete") ) { 
				model.addAttribute("nextMode"      , "View");
			} else if( request.getHeader("REFERER").contains("Modify") ) { 
				model.addAttribute("nextMode"      , "View");
			} else { 
				model.addAttribute("nextMode"      , "List");
			}
		} else { 
			model.addAttribute("nextMode"      , searchVO.getNextMode());
		}
log.debug("nextMode: "+ (String)model.get("nextMode"));

		model.addAttribute("certTest"      , BaseUtility.null2Char( propertiesService.getString("certTest"), "F" ));
		if( "T".equals(propertiesService.getString("certTest")) ) { 
			session.setAttribute("NMinto", propertiesService.getString("certName"));
			session.setAttribute("DIinto", propertiesService.getString("certInfo"));
		}

		return "/coun/"+retPath;
	}	// end of scrAgreOfNotice

	/**
	 * <pre>
	 * 통합게시판을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath}/noticeList.do")
	public String scrListOfNotice ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					retPath      = "";

		int						listCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseBoardsVO>		notiList     = new ArrayList<BaseBoardsVO>();
		List<BaseBoardsVO>		dataList     = new ArrayList<BaseBoardsVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"List", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_list";
			inLogin  = false;
			notiItems = true;
		} else if( "hope".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150060";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_list";
			inLogin  = false;
			notiItems = true;
		} else if( "arch".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150090";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_list";
			inLogin  = true;
		} else if( "press".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150040";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_list";
			inLogin  = false;
		} else if( "news".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150120";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_list";
			inLogin  = false;
		} else if( "tip".equalsIgnoreCase(paramPath) ) {
			bbsType  = "10150150";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_list";
			inLogin  = false;
		}
		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}
		if( inLogin ) { 
			if( !UserDetailsHelper.isAuthenticatedUser()) { 
//				model.addAttribute("message"       , "로그인 후 사용 가능합니다.\\n\\n로그인화면으로 이동합니다.");
				model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeHuin.do");
				return "/cmmn/exe_script";
			}
			if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
				model.addAttribute("message"       , "임시 비밀번호를 사용하고 있습니다.\\n\\n비밀번호 변경화면으로 이동합니다.");
				model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticePass.do");
				return "/cmmn/exe_script";
			}
		}

		// 기본 조회조건 취득
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(propertiesService.getInt("pageSize"));
		pageInfo.setTotalRecordCount(1);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondName("hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) ? "IMGS" : "BASE");
		searchVO.setBbsType(BaseUtility.null2Char( searchVO.getBbsType(), bbsType ));
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReCreHide("hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) ? "" : "N");

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("bbsType: "+ searchVO.getBbsType());
		try { 
			listCnt = (int)boardMngrService.getCountOfBoard( searchVO );

			// 조회 건수를 전체선택인 경우
			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
			}
			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
			searchVO.setPageFirNo(Integer.toString(idxFir));
			searchVO.setPageLstNo(Integer.toString(idxLst));
			searchVO.setListCnt(Integer.toString(listCnt));

			if( listCnt > 0 ) { 
				dataList = (List<BaseBoardsVO>)boardMngrService.getListOfBoard( searchVO );
//				dataList = (List<BaseBoardsVO>)boardMngrService.getListOfBoard( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				// Data Adjustment(Boards)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					for( int d = 0; d < dataList.size(); d++ ) { 
						dataList.get(d).setViewNo(dataList.get(d).getBbsUUID() + dataList.get(d).getRowSeq());

						dataList.get(d).setBbsFiles(BaseUtility.toNumberFormat( dataList.get(d).getBbsFiles() ));
						dataList.get(d).setBgnDate(BaseUtility.toDateFormat( dataList.get(d).getBgnDate(), dtSep, 8 ));
						dataList.get(d).setEndDate(BaseUtility.toDateFormat( dataList.get(d).getEndDate(), dtSep, 8 ));
						dataList.get(d).setHitCount(BaseUtility.toNumberFormat( dataList.get(d).getHitCount() ));
						dataList.get(d).setCreDate(BaseUtility.toDateFormat( dataList.get(d).getCreDate(), dtSep, 8 ));
						dataList.get(d).setModDate(BaseUtility.toDateFormat( dataList.get(d).getModDate(), dtSep, 8 ));
						dataList.get(d).setDelDate(BaseUtility.toDateFormat( dataList.get(d).getDelDate(), dtSep, 8 ));

						dataList.get(d).setBbsTitle(("Y".equals(dataList.get(d).getCreHide())?hideMark:"") + dataList.get(d).getBbsTitle());
						dataList.get(d).setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( dataList.get(d).getAnsPos() )) <= 0 ? dataList.get(d).getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ dataList.get(d).getBbsTitle());
						dataList.get(d).setContDescBR(BaseUtility.tag2Sym( dataList.get(d).getContDesc() ));
						dataList.get(d).setContDescCR(BaseUtility.sym2Tag( dataList.get(d).getContDesc() ));
						dataList.get(d).setContLinkC(BaseUtility.getUriCheck( dataList.get(d).getBbsSiteUri() ) ? "T" : "F");
						dataList.get(d).setCreLock(BaseUtility.null2Char( dataList.get(d).getCrePasswd(), "", "Y" ));
						dataList.get(d).setDownExt(FileProcess.getFileLowerExt( dataList.get(d).getDownRel() ));
						dataList.get(d).setFileUUID(BaseUtility.null2Char(dataList.get(d).getFileUUID(), "", "2"+ dataList.get(d).getFileUUID() ));
						dataList.get(d).setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( dataList.get(d).getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
						dataList.get(d).setRegiDate(BaseUtility.toDateFormat( BaseUtility.null2Char( dataList.get(d).getRegiDate(), dataList.get(d).getCreDate() ), dtSep, 8 ));
						if( !UserDetailsHelper.isAuthenticatedUser() && !dataList.get(d).getCreUser().equals(dataList.get(d).getCreName()) ) dataList.get(d).setCreName(propertiesService.getString("creName"));
						if( "hope".equalsIgnoreCase(paramPath) && dataList.get(d).getBbsSeq().equals(dataList.get(d).getAnsRef()) ) 
							dataList.get(d).setCreName(BaseUtility.null2Blank( dataList.get(d).getCreName() ).substring(0,1)+"○○");
						else if( "tip".equalsIgnoreCase(paramPath) && dataList.get(d).getBbsSeq().equals(dataList.get(d).getAnsRef()) )
							dataList.get(d).setCreName(BaseUtility.null2Blank( dataList.get(d).getCreName() ).substring(0,1)+"○○");

						// 상임위원회 구분
						if( "stand".equalsIgnoreCase(paramPath) && BaseUtility.isValidNull( dataList.get(d).getBbsCateName() ) ) { 
							if( "OPER".equals(dataList.get(d).getBbsCate())  ) dataList.get(d).setBbsCateName("의회운영위원회");
							if( "GVADM".equals(dataList.get(d).getBbsCate()) ) dataList.get(d).setBbsCateName("기획행정위원회");
							if( "WELFARE".equals(dataList.get(d).getBbsCate()) ) dataList.get(d).setBbsCateName("복지환경위원회");
							if( "BUILD".equals(dataList.get(d).getBbsCate()) ) dataList.get(d).setBbsCateName("산업건설위원회");
						}
					}
				}

				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
				pageInfo.setTotalRecordCount(listCnt);
			}

			// 공지 추가
			notiList = notiItems ? (List<BaseBoardsVO>)boardMngrService.getListOfBoardNoti( searchVO ) : null;

			log.debug("notiItems: "+ notiItems);

			if( !(notiList == null || notiList.size() <= 0) ) {
				for( int d = 0; d < notiList.size(); d++ ) {
					notiList.get(d).setViewNo(notiList.get(d).getBbsUUID() + notiList.get(d).getRowSeq());

					notiList.get(d).setBbsFiles(BaseUtility.toNumberFormat( notiList.get(d).getBbsFiles() ));
					notiList.get(d).setBgnDate(BaseUtility.toDateFormat( notiList.get(d).getBgnDate(), dtSep, 8 ));
					notiList.get(d).setEndDate(BaseUtility.toDateFormat( notiList.get(d).getEndDate(), dtSep, 8 ));
					notiList.get(d).setHitCount(BaseUtility.toNumberFormat( notiList.get(d).getHitCount() ));
					notiList.get(d).setCreDate(BaseUtility.toDateFormat( notiList.get(d).getCreDate(), dtSep, 8 ));
					notiList.get(d).setModDate(BaseUtility.toDateFormat( notiList.get(d).getModDate(), dtSep, 8 ));
					notiList.get(d).setDelDate(BaseUtility.toDateFormat( notiList.get(d).getDelDate(), dtSep, 8 ));

					notiList.get(d).setContLinkC(BaseUtility.getUriCheck( notiList.get(d).getBbsSiteUri() ) ? "T" : "F");
					notiList.get(d).setDownExt(FileProcess.getFileLowerExt( notiList.get(d).getDownRel() ));

					if( !UserDetailsHelper.isAuthenticatedUser() && !notiList.get(d).getCreUser().equals(notiList.get(d).getCreName()) )
						notiList.get(d).setCreName(propertiesService.getString("creName"));
				}
			}

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			model.addAttribute("dataList"      , dataList);
			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (dataList == null ? 0 : dataList.size()) : listCnt ));
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("notiList"      , notiList);
			model.addAttribute("cateList"      , cateList);
			model.remove("searchVO");
			if( removeSign ) { 
				session.removeAttribute("NMinto");
				session.removeAttribute("DIinto");
			}
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/coun/"+retPath;
	}	// end of scrListOfNotice

	/**
	 * <pre>
	 * 통합게시판을(를) 상세조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath}/noticeView.do")
	public String scrViewOfNotice ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					retPath      = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_view";

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		BaseBoardsVO			dataView     = new BaseBoardsVO();
		BaseBoardsVO			prevView     = new BaseBoardsVO();
		BaseBoardsVO			nextView     = new BaseBoardsVO();
		BaseCommonVO			findFile     = new BaseCommonVO();
		List<BaseFilesVO>		dataFile     = new ArrayList<BaseFilesVO>();
		List<BaseFilesVO>		thmbFile     = new ArrayList<BaseFilesVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		dataSupp     = new ArrayList<BaseCommonVO>();
		BaseBoardsVO			tempSupp     = new BaseBoardsVO();
		BaseBoardsVO			tempView     = new BaseBoardsVO();
		String[][]				tempList     = new String[4][10];
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"View", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_view";
			inLogin  = false;
		} else if( "hope".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150060";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_view";
			inLogin  = false;
		} else if( "arch".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150090";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_view";
			inLogin  = true;
		} else if( "press".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150040";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_view";
			inLogin  = false;
		} else if( "news".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150120";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_view";
			inLogin  = false;
		} else if( "tip".equalsIgnoreCase(paramPath) ) {
			bbsType  = "10150150";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_view";
			inLogin  = false;
		}
		if( "".equals(retPath) || BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}
		if( inLogin ) { 
			if( !UserDetailsHelper.isAuthenticatedUser()) { 
				model.addAttribute("message"       , "로그인 후 사용 가능합니다.\\n\\n로그인화면으로 이동합니다.");
				model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeHuin.do");
				return "/cmmn/exe_script";
			}
			if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
				model.addAttribute("message"       , "임시 비밀번호를 사용하고 있습니다.\\n\\n비밀번호 변경화면으로 이동합니다.");
				model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticePass.do");
				return "/cmmn/exe_script";
			}
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondName("hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) ? "HIDE" : "BASE");
		searchVO.setBbsType(BaseUtility.null2Char( searchVO.getBbsType(), bbsType ));
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));
		searchVO.setReCreHide("hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) ? "" : "N");

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq() +" |reUseStat: "+ searchVO.getReUseStat());
		try { 
			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)boardMngrService.getInfoOfBoardUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(게시판)
				searchVO.setBbsType(viewNo[0]);
				searchVO.setBbsSeq(viewNo[1]);
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());

				dataView = (BaseBoardsVO)boardMngrService.getDetailOfBoard( searchVO );
				// 의회에 바란다의 경우(비공개글 및 추가인증 처리시)
				if( dataView != null && ("hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath)) ) {
					if( "Y".equals(BaseUtility.null2Blank( dataView.getCreHide() )) || "T".equals(BaseUtility.null2Blank( searchVO.getReUseStat() )) ) { 
						// 인증내역 확인
						BaseMemberVO			authInfo     = new BaseMemberVO();
						if( "hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) ) {
							authInfo.setCertName(BaseUtility.null2Blank( (String)session.getAttribute("NMinto") ));
							authInfo.setCertInfo(BaseUtility.null2Blank( (String)session.getAttribute("DIinto") ));
							// 인증내역이 없으면 인증화면으로 이동
							if( BaseUtility.isValidNull( authInfo.getCertName() ) && BaseUtility.isValidNull( authInfo.getCertInfo() ) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"View", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
								WebUtils.setSessionAttribute(request, "searchSess", searchVO);
//								model.addAttribute("message"       , "본인인증 후 사용 가능합니다.\\n\\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
								model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeAgre.do");
								return "/cmmn/exe_script";
							}
							model.addAttribute("authInfo"      , authInfo);
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"View", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + authInfo.getCertName());
						}
						if( BaseUtility.isValidNull(searchVO.getRePass()) ) { 
							// 인증내역이 있는 경우(본인작성글이 아닌 경우)
							if( !(dataView.getCertInfo().equals(authInfo.getCertInfo())) ) { 
								model.addAttribute("message"       , "작성된 게시글 및 답변글이 인증된 내역과 달라 열람이 제한되었습니다.\\n\\n확인하시기 바랍니다..");
								model.addAttribute("returnUrl"     , "noticeList.do");
								return "/cmmn/exe_script";
							}
						} else { 
							// 입력된 비밀번호가 다른경우
							if( !(dataView.getCrePasswd().equals(searchVO.getRePass())) ) { 
								model.addAttribute("message"       , "작성된 게시글이 입력한 비밀번호가 달라 열람이 제한되었습니다.\\n\\n확인하시기 바랍니다..");
								model.addAttribute("returnUrl"     , "noticeList.do");
								return "/cmmn/exe_script";
							}
						}
					}
				}
				if( dataView != null ) { 
					boardMngrService.doRtnOfModifyBoardHit( dataView );

					findFile.setLnkName(lnkName);
					findFile.setLnkKey(viewNo[0] + viewNo[1]);
					// Data Adjustment(Data Files)
					dataFile = (List<BaseFilesVO>)baseCmmnService.getListOfDataFile( findFile );
					// Data Adjustment(Thumb Files)
					thmbFile = (List<BaseFilesVO>)baseCmmnService.getListOfThmbFile( findFile );
					if( !(thmbFile == null || thmbFile.size() <= 0) ) { 
						for( int f = 0; f < thmbFile.size(); f++ ) { 
							thmbFile.get(f).setFileImage(BaseUtility.getFileAllowOfImage( thmbFile.get(f).getFileAbs() ) ? "T" : null);
						}
					}

					// Data Adjustment(Boards)
					dataView.setActMode("Update");
					dataView.setViewNo(dataView.getBbsUUID());

					dataView.setBbsFiles(BaseUtility.toNumberFormat( dataView.getBbsFiles() ));
					dataView.setBgnDate(BaseUtility.toDateFormat( dataView.getBgnDate(), dtSep, 8 ));
					dataView.setEndDate(BaseUtility.toDateFormat( dataView.getEndDate(), dtSep, 8 ));
					dataView.setHitCount(BaseUtility.toNumberFormat( Integer.parseInt(BaseUtility.null2Zero( dataView.getHitCount() )) + 1 ));
					dataView.setCreDate(BaseUtility.toDateFormat( dataView.getCreDate(), dtSep, 8 ));
					dataView.setModDate(BaseUtility.toDateFormat( dataView.getModDate(), dtSep, 8 ));
					dataView.setDelDate(BaseUtility.toDateFormat( dataView.getDelDate(), dtSep, 8 ));

					dataView.setBbsTitle(("Y".equals(dataView.getCreHide())?hideMark:"") + dataView.getBbsTitle());
					dataView.setContDescBR(BaseUtility.tag2Sym( dataView.getContDesc() ));
					dataView.setContDescCR(BaseUtility.sym2Tag( dataView.getContDesc() ));
					dataView.setContLinkC(BaseUtility.getUriCheck( dataView.getBbsSiteUri() ) ? "T" : "F");
					dataView.setCreLock(BaseUtility.null2Char( dataView.getCrePasswd(), "", "Y" ));
					dataView.setDownExt(FileProcess.getFileLowerExt( dataView.getDownRel() ));
					dataView.setFileUUID(BaseUtility.null2Char(dataView.getFileUUID(), "", "6"+ dataView.getFileUUID() ));
					dataView.setFileLinkS(FileProcess.getInfoOfFileBBS( request, response, dataFile, "S" ));
					dataView.setFileLinkM(FileProcess.getInfoOfFileBBS( request, response, dataFile, "M" ));

//log.debug("dataFile: " + dataFile);

						dataView.setRegiDate(BaseUtility.toDateFormat( BaseUtility.null2Char( dataView.getRegiDate(), dataView.getCreDate() ), dtSep, 8 ));
					if( !UserDetailsHelper.isAuthenticatedUser() && !dataView.getCreUser().equals(dataView.getCreName()) ) dataView.setCreName(propertiesService.getString("creName"));
					if( "hope".equalsIgnoreCase(paramPath) && dataView.getBbsSeq().equals(dataView.getAnsRef()) ) 
						dataView.setCreName(BaseUtility.null2Blank( dataView.getCreName() ).substring(0,1)+"○○");
					else if( "tip".equalsIgnoreCase(paramPath) && dataView.getBbsSeq().equals(dataView.getAnsRef()) )
						dataView.setCreName(BaseUtility.null2Blank( dataView.getCreName() ).substring(0,1)+"○○");

					// 추가게시판 내역
					tempSupp = (BaseBoardsVO)boardMngrService.getDetailOfAdder( searchVO );
					if( tempSupp != null ) { 
						dataView.setConfCode1(tempSupp.getConfCode1());
						dataView.setConfName1((String)baseCmmnService.getInfoOfConfName( tempSupp.getConfCode1() ));
						dataView.setRegiDate1(BaseUtility.toDateFormat( tempSupp.getRegDate1(), dtSep, 8 ));

						tempList[0] = BaseUtility.isValidNull( tempSupp.getConfCode2() ) ? null : BaseUtility.null2Blank( tempSupp.getConfCode2() ).split("[◆]");
						tempList[1] = BaseUtility.isValidNull( tempSupp.getRegDate2()  ) ? null : BaseUtility.null2Blank( tempSupp.getRegDate2() ).split("[◆]");
						tempList[2] = BaseUtility.isValidNull( tempSupp.getRegName2()  ) ? null : BaseUtility.null2Blank( tempSupp.getRegName2() ).split("[◆]");
						tempList[3] = BaseUtility.isValidNull( tempSupp.getContent2()  ) ? null : BaseUtility.null2Blank( tempSupp.getContent2() ).split("[◆]");

						if( !(tempList[2] == null || tempList[2].length <= 0) ) { 
							tempList[0] = tempList[0] == null ? new String[tempList[2].length] : tempList[0];
							tempList[1] = tempList[1] == null ? new String[tempList[2].length] : tempList[1];
							tempList[3] = tempList[3] == null ? new String[tempList[2].length] : tempList[3];
							for( int i = 0; i < tempList[2].length; i++ ) { 
								tempView = new BaseBoardsVO();
								tempView.setConfCode2(BaseUtility.null2Blank( tempList[0][i] ));
								tempView.setConfName2((String)baseCmmnService.getInfoOfConfName( tempList[0][i] ));
								tempView.setRegiDate2(BaseUtility.toDateFormat( tempList[1][i], dtSep, 8 ));
								tempView.setRegiName2(BaseUtility.null2Blank( tempList[2][i] ));
								tempView.setContents2(BaseUtility.null2Blank( tempList[3][i]) );
								tempView.setContents2BR(BaseUtility.tag2Sym( tempList[3][i] ));
								tempView.setContents2CR(BaseUtility.sym2Tag( tempList[3][i] ));
								dataSupp.add(tempView);
							}
						}
					}

					// 상임위원회 구분
					if( "stand".equalsIgnoreCase(paramPath) && BaseUtility.isValidNull( dataView.getBbsCateName() ) ) { 
						if( "OPER".equals(dataView.getBbsCate())  ) dataView.setBbsCateName("의회운영위원회");
						if( "GVADM".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("기획행정위원회");
						if( "WELFARE".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("복지환경위원회");
						if( "BUILD".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("산업건설위원회");
					}

					// Data Adjustment(Move Items)
					if( moveItems && !BaseUtility.isValidNull( searchVO.getRowSeq() ) ) { 
						searchVO.setRowSeq(String.valueOf(Integer.parseInt(searchVO.getRowSeq()) + 1));
						prevView = (BaseBoardsVO)boardMngrService.getMovingOfBoard( searchVO );
						if( prevView != null ) { 
							prevView.setViewNo(prevView.getBbsUUID() + prevView.getRowSeq());
							prevView.setBbsTitle(("Y".equals(prevView.getCreHide())?hideMark:"") + prevView.getBbsTitle());
							prevView.setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( prevView.getAnsPos() )) <= 0 ? prevView.getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ prevView.getBbsTitle());
							prevView.setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( prevView.getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
							if( !prevView.getCreUser().equals(prevView.getCreName()) ) prevView.setCreName(propertiesService.getString("creName"));
						}
						searchVO.setRowSeq(String.valueOf(Integer.parseInt(searchVO.getRowSeq()) - 2));
						nextView = (BaseBoardsVO)boardMngrService.getMovingOfBoard( searchVO );
						if( nextView != null ) { 
							nextView.setViewNo(nextView.getBbsUUID() + nextView.getRowSeq());
							nextView.setBbsTitle(("Y".equals(nextView.getCreHide())?hideMark:"") + nextView.getBbsTitle());
							nextView.setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( nextView.getAnsPos() )) <= 0 ? nextView.getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ nextView.getBbsTitle());
							nextView.setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( nextView.getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
							if( !nextView.getCreUser().equals(nextView.getCreName()) ) nextView.setCreName(propertiesService.getString("creName"));
						}
					} else { 
						prevView = null;
						nextView = null;
					}
				}
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("dataFile"      , dataFile);
			model.addAttribute("thmbFile"      , thmbFile);
			model.addAttribute("moveItem"      , moveItems?"T":"F");
			model.addAttribute("prevView"      , prevView);
			model.addAttribute("nextView"      , nextView);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("dataSupp"      , dataSupp);
//			if( removeSign && "Q".equals(dataView.getAnsMode()) ) { 
//				session.removeAttribute("NMinto");
//				session.removeAttribute("DIinto");
//			}
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/coun/"+retPath;
	}	// end of scrViewOfNotice

	/**
	 * <pre>
	 * 게시판 등록화면으로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath}/noticeInst.do")
	public String scrInstOfNotice ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					retPath      = "";

		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();

		if( !("hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) || "arch".equalsIgnoreCase(paramPath) || "news".equalsIgnoreCase(paramPath)) ) {
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Inst", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			model.addAttribute("message"       , "접근이 불가능합니다.\\n\\n이전화면으로 이동합니다.");
			model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeList.do");
			return "/cmmn/exe_script";
		}
		// 인증내역 확인
		BaseMemberVO			authInfo     = new BaseMemberVO();
		if( "hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) ) {
			authInfo.setCertName(BaseUtility.null2Blank( (String)session.getAttribute("NMinto") ));
			authInfo.setCertInfo(BaseUtility.null2Blank( (String)session.getAttribute("DIinto") ));
			// 인증내역이 없으면 인증화면으로 이동
			if( BaseUtility.isValidNull( authInfo.getCertName() ) && BaseUtility.isValidNull( authInfo.getCertInfo() ) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Inst", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
				WebUtils.setSessionAttribute(request, "searchSess", searchVO);
//				model.addAttribute("message"       , "본인인증 후 사용 가능합니다.\\n\\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
				model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeAgre.do");
				return "/cmmn/exe_script";
			}
			model.addAttribute("authInfo"      , authInfo);
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Inst", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + authInfo.getCertName());
		}
		if( "arch".equalsIgnoreCase(paramPath) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Inst", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			if( !UserDetailsHelper.isAuthenticatedUser() ) { 
				model.addAttribute("message"       , "로그인 후 사용 가능합니다.\\n\\n로그인화면으로 이동합니다.");
				model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeHuin.do");
				return "/cmmn/exe_script";
			}
			if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
				model.addAttribute("message"       , "임시 비밀번호를 사용하고 있습니다.\\n\\n비밀번호 변경화면으로 이동합니다.");
				model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticePass.do");
				return "/cmmn/exe_script";
			}
		}

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_inst";
		} else if( "hope".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150060";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_inst";
		} else if( "arch".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150090";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_inst";
//		} else if( "press".equalsIgnoreCase(paramPath) ) { 
//			bbsType  = "10150040";
//			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_inst";
		} else if( "news".equalsIgnoreCase(paramPath) ) {
			bbsType  = "10150120";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_inst";
		} else if( "tip".equalsIgnoreCase(paramPath) ) {
			bbsType  = "10150150";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_inst";
		}
		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		model.addAttribute("cateList"      , cateList);

		System.out.println(">>>>>>>>>>>>>(Inst) retPath " + retPath);

		return "/coun/"+retPath;
	}	// end of scrInstOfNotice

	/**
	 * <pre>
	 * 게시판 수정화면으로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath}/noticeEdit.do")
	public String scrEditOfNotice ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					retPath      = "";

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		BaseBoardsVO			dataView     = new BaseBoardsVO();
		BaseCommonVO			findFile     = new BaseCommonVO();
		List<BaseFilesVO>		dataFile     = new ArrayList<BaseFilesVO>();
		List<BaseFilesVO>		thmbFile     = new ArrayList<BaseFilesVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		dataSupp     = new ArrayList<BaseCommonVO>();
		BaseBoardsVO			tempSupp     = new BaseBoardsVO();
		BaseBoardsVO			tempView     = new BaseBoardsVO();
		String[][]				tempList     = new String[4][10];



		if( !("hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) || "arch".equalsIgnoreCase(paramPath)) ) {
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Edit", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			model.addAttribute("message"       , "접근이 불가능합니다.\\n\\n이전화면으로 이동합니다.");
			model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeList.do");
			return "/cmmn/exe_script";
		}
		// 인증내역 확인
		BaseMemberVO			authInfo     = new BaseMemberVO();
		// 비밀번호 입력글은 인증제외
		if( "hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) ) {
			if( !BaseUtility.isValidNull(searchVO.getRePass()) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Edit", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			} else { 
				authInfo.setCertName(BaseUtility.null2Blank( (String)session.getAttribute("NMinto") ));
				authInfo.setCertInfo(BaseUtility.null2Blank( (String)session.getAttribute("DIinto") ));
				// 인증내역이 없으면 인증화면으로 이동
				if( BaseUtility.isValidNull( authInfo.getCertName() ) && BaseUtility.isValidNull( authInfo.getCertInfo() ) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Edit", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
					WebUtils.setSessionAttribute(request, "searchSess", searchVO);
//					model.addAttribute("message"       , "본인인증 후 사용 가능합니다.\\n\\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
					model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeAgre.do");
					return "/cmmn/exe_script";
				}
				model.addAttribute("authInfo"      , authInfo);
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Edit", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + authInfo.getCertName());
			}
		} 
		if( "arch".equalsIgnoreCase(paramPath) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Edit", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			if( !UserDetailsHelper.isAuthenticatedUser() ) { 
				model.addAttribute("message"       , "로그인 후 사용 가능합니다.\\n\\n로그인화면으로 이동합니다.");
				model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeHuin.do");
				return "/cmmn/exe_script";
			}
			if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
				model.addAttribute("message"       , "임시 비밀번호를 사용하고 있습니다.\\n\\n비밀번호 변경화면으로 이동합니다.");
				model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticePass.do");
				return "/cmmn/exe_script";
			}
		}

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_edit";
		} else if( "hope".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150060";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_edit";
		} else if( "arch".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150090";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_edit";
//		} else if( "press".equalsIgnoreCase(paramPath) ) { 
//			bbsType  = "10150040";
//			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_edit";
//		} else if( "news".equalsIgnoreCase(paramPath) ) { 
//			bbsType  = "10150120";
//			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_edit";
		} else if( "tip".equalsIgnoreCase(paramPath) ) {
			bbsType  = "10150150";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_edit";
		}
		if( "".equals(retPath) || BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			viewNo = BaseUtility.null2Char( (String)boardMngrService.getInfoOfBoardUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

			// 입력내역 취득(게시판)
			searchVO.setBbsType(viewNo[0]);
			searchVO.setBbsSeq(viewNo[1]);
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());

			dataView = (BaseBoardsVO)boardMngrService.getDetailOfBoard( searchVO );
			if( dataView != null ) { 
				if( BaseUtility.isValidNull(searchVO.getRePass()) ) { 
					// 인증내역이 있는 경우(본인작성글이 아닌 경우)
					if( !(dataView.getCertInfo().equals(authInfo.getCertInfo())) ) { 
						model.addAttribute("message"       , "작성된 게시글이 인증된 내역과 달라 수정이 제한되었습니다.\\n\\n확인하시기 바랍니다.");
						model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeList.do");
						return "/cmmn/exe_script";
					}
				} else { 
					// 입력된 비밀번호가 다른경우
					if( !(dataView.getCrePasswd().equals(searchVO.getRePass())) ) { 
						model.addAttribute("message"       , "작성된 게시글이 입력한 비밀번호가 달라 수정이 제한되었습니다.\\n\\n확인하시기 바랍니다.");
						model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeList.do");
						return "/cmmn/exe_script";
					}
				}
				findFile.setLnkName(lnkName);
				findFile.setLnkKey(viewNo[0] + viewNo[1]);
				// Data Adjustment(Data Files)
				dataFile = (List<BaseFilesVO>)baseCmmnService.getListOfDataFile( findFile );
				// Data Adjustment(Thumb Files)
				thmbFile = (List<BaseFilesVO>)baseCmmnService.getListOfThmbFile( findFile );
				if( !(thmbFile == null || thmbFile.size() <= 0) ) { 
					for( int f = 0; f < thmbFile.size(); f++ ) { 
						thmbFile.get(f).setFileImage(BaseUtility.getFileAllowOfImage( thmbFile.get(f).getFileAbs() ) ? "T" : null);
					}
				}

				// Data Adjustment(Boards)
				dataView.setActMode("Update");
				dataView.setViewNo(dataView.getBbsUUID());

				dataView.setBbsFiles(BaseUtility.toNumberFormat( dataView.getBbsFiles() ));
				dataView.setBgnDate(BaseUtility.toDateFormat( dataView.getBgnDate(), dtSep, 8 ));
				dataView.setEndDate(BaseUtility.toDateFormat( dataView.getEndDate(), dtSep, 8 ));
				dataView.setHitCount(BaseUtility.toNumberFormat( dataView.getHitCount() ));
				dataView.setCreDate(BaseUtility.toDateFormat( dataView.getCreDate(), dtSep, 8 ));
				dataView.setModDate(BaseUtility.toDateFormat( dataView.getModDate(), dtSep, 8 ));
				dataView.setDelDate(BaseUtility.toDateFormat( dataView.getDelDate(), dtSep, 8 ));

				dataView.setContDescBR(BaseUtility.tag2Sym( dataView.getContDesc() ));
				dataView.setContDescCR(BaseUtility.sym2Tag( dataView.getContDesc() ));
				dataView.setContLinkC(BaseUtility.getUriCheck( dataView.getBbsSiteUri() ) ? "T" : "F");
				dataView.setCreLock(BaseUtility.null2Char( dataView.getCrePasswd(), "", "Y" ));
				dataView.setDownExt(FileProcess.getFileLowerExt( dataView.getDownRel() ));
				dataView.setFileUUID(BaseUtility.null2Char(dataView.getFileUUID(), "", "6"+ dataView.getFileUUID() ));
				dataView.setFileLinkS(FileProcess.getInfoOfFileBBS( request, response, dataFile, "S" ));
				dataView.setFileLinkM(FileProcess.getInfoOfFileBBS( request, response, dataFile, "M" ));
				dataView.setRegiDate(BaseUtility.toDateFormat( BaseUtility.null2Char( dataView.getRegiDate(), dataView.getCreDate() ), dtSep, 8 ));
				if( !UserDetailsHelper.isAuthenticatedUser() && !dataView.getCreUser().equals(dataView.getCreName()) ) dataView.setCreName(propertiesService.getString("creName"));
				if( "hope".equalsIgnoreCase(paramPath) && dataView.getBbsSeq().equals(dataView.getAnsRef()) ) 
					dataView.setCreName(BaseUtility.null2Blank( dataView.getCreName() ).substring(0,1)+"○○");
				else if( "tip".equalsIgnoreCase(paramPath) && dataView.getBbsSeq().equals(dataView.getAnsRef()) )
					dataView.setCreName(BaseUtility.null2Blank( dataView.getCreName() ).substring(0,1)+"○○");

				// 추가게시판 내역
				tempSupp = (BaseBoardsVO)boardMngrService.getDetailOfAdder( searchVO );
				if( tempSupp != null ) { 
					dataView.setConfCode1(tempSupp.getConfCode1());
					dataView.setConfName1((String)baseCmmnService.getInfoOfConfName( tempSupp.getConfCode1() ));
					dataView.setRegiDate1(BaseUtility.toDateFormat( tempSupp.getRegDate1(), dtSep, 8 ));

					tempList[0] = BaseUtility.isValidNull( tempSupp.getConfCode2() ) ? null : BaseUtility.null2Blank( tempSupp.getConfCode2() ).split("[◆]");
					tempList[1] = BaseUtility.isValidNull( tempSupp.getRegDate2()  ) ? null : BaseUtility.null2Blank( tempSupp.getRegDate2() ).split("[◆]");
					tempList[2] = BaseUtility.isValidNull( tempSupp.getRegName2()  ) ? null : BaseUtility.null2Blank( tempSupp.getRegName2() ).split("[◆]");
					tempList[3] = BaseUtility.isValidNull( tempSupp.getContent2()  ) ? null : BaseUtility.null2Blank( tempSupp.getContent2() ).split("[◆]");

					if( !(tempList[2] == null || tempList[2].length <= 0) ) { 
						tempList[0] = tempList[0] == null ? new String[tempList[2].length] : tempList[0];
						tempList[1] = tempList[1] == null ? new String[tempList[2].length] : tempList[1];
						tempList[3] = tempList[3] == null ? new String[tempList[2].length] : tempList[3];
						for( int i = 0; i < tempList[2].length; i++ ) { 
							tempView = new BaseBoardsVO();
							tempView.setConfCode2(BaseUtility.null2Blank( tempList[0][i] ));
							tempView.setConfName2((String)baseCmmnService.getInfoOfConfName( tempList[0][i] ));
							tempView.setRegiDate2(BaseUtility.toDateFormat( tempList[1][i], dtSep, 8 ));
							tempView.setRegiName2(BaseUtility.null2Blank( tempList[2][i] ));
							tempView.setContents2(BaseUtility.null2Blank( tempList[3][i] ));
							tempView.setContents2BR(BaseUtility.tag2Sym( tempList[3][i] ));
							tempView.setContents2CR(BaseUtility.sym2Tag( tempList[3][i] ));
							dataSupp.add(tempView);
						}
					}
				}

				// 상임위원회 구분
				if( "stand".equalsIgnoreCase(paramPath) && BaseUtility.isValidNull( dataView.getBbsCateName() ) ) { 
					if( "OPER".equals(dataView.getBbsCate())  ) dataView.setBbsCateName("의회운영위원회");
					if( "GVADM".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("기획행정위원회");
					if( "WELFARE".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("복지환경위원회");
					if( "BUILD".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("산업건설위원회");
				}
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("dataFile"      , dataFile);
			model.addAttribute("thmbFile"      , thmbFile);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("dataSupp"      , dataSupp);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/coun/"+retPath;
	}	// end of scrEditOfNotice

	/**
	 * <pre>
	 * 게시판 비번 변경화면으로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath}/noticePass.do")
	public String scrPassOfNotice ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					retPath      = "";

		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Pass", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_pass";
			inLogin  = false;
		} else if( "arch".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150090";
			retPath  = "base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_pass";
			inLogin  = true;
		}
		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}
		if( inLogin ) { 
			if( !UserDetailsHelper.isAuthenticatedUser() ) { 
				model.addAttribute("message"       , "로그인 정보가 존재하지 않습니다.\\n\\n로그인 화면으로 전환합니다.");
				model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeHuin.do");
				return "/cmmn/exe_script";
			}
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		model.addAttribute("cateList"      , cateList);

		this.initRSA(request);

		return "/coun/"+retPath;
	}	// end of scrPassOfNotice


	/**
	 * <pre>
	 * 게시판 내역을 등록처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  MultipartHttpServletRequest		// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/{paramPath}/noticeCreate.do")
	public ModelAndView doActOfCreateNotice ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseBoardsVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			final MultipartHttpServletRequest mpRequest, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception {


		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

//		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
//		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseBoardsVO			dataView     = new BaseBoardsVO();
		Map<String, MultipartFile> fileMap   = mpRequest == null ? null : mpRequest.getFileMap();
		String					filePath     = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.NoteBase" );
		int						fileCnt      = 0;
		int						thmbCnt      = 0;

		if( !("hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) || "arch".equalsIgnoreCase(paramPath)) ) {
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Create", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접근이 불가능합니다.\n\n이전화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeList.do");
		}
		// 인증내역 확인
		BaseMemberVO			authInfo     = new BaseMemberVO();
		if( "hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) ) {
			authInfo.setCertName(BaseUtility.null2Blank( (String)session.getAttribute("NMinto") ));
			authInfo.setCertInfo(BaseUtility.null2Blank( (String)session.getAttribute("DIinto") ));
			// 인증내역이 없으면 인증화면으로 이동
			if( BaseUtility.isValidNull( authInfo.getCertName() ) && BaseUtility.isValidNull( authInfo.getCertInfo() ) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Create", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
				WebUtils.setSessionAttribute(request, "searchSess", searchVO);
//				model.addAttribute("message"       , "본인인증 후 사용 가능합니다.\\n\\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
//				model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeAgre.do");
//				return "/cmmn/exe_script";
				mav.addObject("isError"       , "C");
//				mav.addObject("message"       , "본인인증 후 사용 가능합니다.\n\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
				mav.addObject("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeAgre.do");
				return mav;
			}
			model.addAttribute("authInfo"      , authInfo);
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Create", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + authInfo.getCertName());
		} 
		if( "arch".equalsIgnoreCase(paramPath) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Create", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			if( !UserDetailsHelper.isAuthenticatedUser() ) { 
				mav.addObject("isError"       , "T");
//				mav.addObject("message"       , "로그인 후 사용 가능합니다.\n\n로그인화면으로 이동합니다.");
				mav.addObject("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeHuin.do");
				return mav;
			}
			if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
				mav.addObject("isError"       , "T");
				mav.addObject("message"       , "임시 비밀번호를 사용하고 있습니다.\n\n비밀번호 변경화면으로 이동합니다.");
				mav.addObject("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticePass.do");
			}
		}


		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.NoteBase" );
		} else if( "hope".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150060";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.NoteHope" );
		} else if( "arch".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150090";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.NoteArch" );
//		} else if( "press".equalsIgnoreCase(paramPath) ) { 
//			bbsType  = "10150040";
//			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.NoteProm" );
//		} else if( "news".equalsIgnoreCase(paramPath) ) { 
//			bbsType  = "10150120";
//			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.NewsCoun" );
		} else if( "tip".equalsIgnoreCase(paramPath) ) {
			bbsType  = "10150150";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.NoteTip" );
		}
		if( "".equals(paramPath) ) { 
//			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
//			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
//			return "/cmmn/exe_script";
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "잘못된 경로로 접속하였습니다.\n\n메인화면으로 전환합니다.");
			return mav;
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setBbsType(BaseUtility.null2Char( searchVO.getBbsType(), bbsType ));


//log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/coun/base/notice"+ BaseUtility.toCapitalize( paramPath ) +"_inst";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			// 지정된 파일만 업로드 처리
			if( !(fileMap == null || fileMap.size() <= 0) && fileUP ) for( MultipartFile mFile : fileMap.values() ) { 
				if( !(mFile.isEmpty() || mFile.getSize() < 0) && mFile.getName().contains("fileBase") && !BaseUtility.getFileAllowOfDocuImage( mFile.getOriginalFilename() ) ) { 
//					model.addAttribute("error"         , "첨부 불가능한 파일이 존재합니다.");
//					return "/cmmn/exe_script";
					mav.addObject("message"       , "첨부 불가능한 파일이 존재합니다.").addObject("isError"       , "T");
					return mav;
				}
			}


			// 입력내역 취득(게시판)
			dataView = (BaseBoardsVO)boardMngrService.getInfoOfBoardSeq( searchVO );
			searchVO.setBbsSeq(dataView == null ? "1" : dataView.getBbsSeq());
			searchVO.setBbsUUID(BaseCategory.encryptSHA256( searchVO.getBbsType() + searchVO.getBbsSeq() ).toUpperCase());
			searchVO.setBgnDate(BaseUtility.null2Char( searchVO.getBgnDate(), systemVO.getToDate() ).replaceAll("[^0-9]", ""));
			searchVO.setEndDate(BaseUtility.null2Char( searchVO.getEndDate(), "20991231" ).replaceAll("[^0-9]", ""));
			searchVO.setContSize(String.valueOf(BaseUtility.removeAllTags( searchVO.getContDesc() ).length()));
			searchVO.setContPattern("P");
			searchVO.setContCrypt("F");
			searchVO.setHitCount("0");
			searchVO.setAnsRef(searchVO.getBbsSeq());
			searchVO.setAnsStep("0");
			searchVO.setAnsPos("0");
			searchVO.setCreOrder("on".equalsIgnoreCase(BaseUtility.null2Blank( searchVO.getCreOrder() )) ? searchVO.getBbsSeq() : null);
//			searchVO.setCreHide("on".equalsIgnoreCase(BaseUtility.null2Blank( searchVO.getCreHide() )) ? "Y" : BaseUtility.null2Blank( searchVO.getCreHide() ));
			searchVO.setUseDele("N");
			searchVO.setCreIPv4(BaseUtility.reqIPv4Address( request ));
			searchVO.setModIPv4(BaseUtility.reqIPv4Address( request ));
			if( "hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) ) {
				searchVO.setCreUser(BaseUtility.null2Char( searchVO.getCreUser(), authInfo.getCertName() ));
				searchVO.setCreName(BaseUtility.null2Char( searchVO.getCreName(), authInfo.getCertName() ));
				searchVO.setCreCert(authInfo.getCertInfo());
				searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), authInfo.getCertName() ));
				searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), authInfo.getCertName() ));
			} else { 
				searchVO.setCreUser(BaseUtility.null2Char( searchVO.getCreUser(), memberVO.getUserPID() ));
				searchVO.setCreName(BaseUtility.null2Char( searchVO.getCreName(), memberVO.getUserName() ));
				searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
				searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));
			}
			searchVO.setViewNo(searchVO.getBbsUUID() +"1");
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());


			// 썸네일 갱신처리
			if( fileUP ) thmbCnt = FileProcess.doActOfCreateFileThmb( request, response, baseCmmnService, systemVO, memberVO, fileMap, fileCnt, filePath, lnkName, searchVO.getBbsType() + searchVO.getBbsSeq() );
			// 첨부파일 갱신처리
			if( fileUP ) fileCnt = FileProcess.doActOfCreateFileBase( request, response, baseCmmnService, systemVO, memberVO, fileMap, fileCnt, filePath, lnkName, searchVO.getBbsType() + searchVO.getBbsSeq() );

			// 게시판 내역 갱신
			searchVO.setBbsFiles(String.valueOf(fileCnt));
			boardMngrService.doRtnOfCreateBoard( searchVO );
			model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
			if( removeSign ) { 
				model.remove("authInfo");
				session.removeAttribute("NMinto");
				session.removeAttribute("DIinto");
			}

		} catch( Exception E ) {
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//-		return "redirect:/coun/"+ paramPath.toLowerCase() +"/noticeView.do";
//		return "redirect:/coun/"+ paramPath.toLowerCase() +"/noticeList.do";
		return mav;
	}	// end of doActOfCreateNotice

	/**
	 * <pre>
	 * 게시판 내역을 수정처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  MultipartHttpServletRequest		// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/{paramPath}/noticeUpdate.do")
	public ModelAndView doActOfUpdateNotice ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseBoardsVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			final MultipartHttpServletRequest mpRequest, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseBoardsVO			dataView     = new BaseBoardsVO();
		Map<String, MultipartFile> fileMap   = mpRequest == null ? null : mpRequest.getFileMap();
		String					filePath     = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.NoteBase" );
		int						fileCnt      = 0;
		int						thmbCnt      = 0;
		String[]				deleList     = null;
		BaseFilesVO				fileDele     = new BaseFilesVO();

		if( !("hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) || "arch".equalsIgnoreCase(paramPath)) ) {
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Update", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접근이 불가능합니다.\n\n이전화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeList.do");
		}
		// 인증내역 확인
		BaseMemberVO			authInfo     = new BaseMemberVO();
		if( "hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) ) {
			authInfo.setCertName(BaseUtility.null2Blank( (String)session.getAttribute("NMinto") ));
			authInfo.setCertInfo(BaseUtility.null2Blank( (String)session.getAttribute("DIinto") ));
			// 인증내역이 없으면 인증화면으로 이동
			if( BaseUtility.isValidNull( authInfo.getCertName() ) && BaseUtility.isValidNull( authInfo.getCertInfo() ) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Update", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
				WebUtils.setSessionAttribute(request, "searchSess", searchVO);
//				model.addAttribute("message"       , "본인인증 후 사용 가능합니다.\\n\\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
//				model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeAgre.do");
//				return "/cmmn/exe_script";
				mav.addObject("isError"       , "C");
//				mav.addObject("message"       , "본인인증 후 사용 가능합니다.\n\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
				mav.addObject("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeAgre.do");
				return mav;
			}
			model.addAttribute("authInfo"      , authInfo);
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Update", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + authInfo.getCertName());
		} 
		if( "arch".equalsIgnoreCase(paramPath) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Update", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			if( !UserDetailsHelper.isAuthenticatedUser() ) { 
				mav.addObject("isError"       , "T");
//				mav.addObject("message"       , "로그인 후 사용 가능합니다.\n\n로그인화면으로 이동합니다.");
				mav.addObject("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeHuin.do");
				return mav;
			}
			if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
				mav.addObject("isError"       , "T");
				mav.addObject("message"       , "임시 비밀번호를 사용하고 있습니다.\n\n비밀번호 변경화면으로 이동합니다.");
				mav.addObject("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticePass.do");
			}
		}

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.NoteBase" );
		} else if( "hope".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150060";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.NoteHope" );
		} else if( "arch".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150090";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.NoteArch" );
//		} else if( "press".equalsIgnoreCase(paramPath) ) { 
//			bbsType  = "10150040";
//			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.NoteProm" );
//		} else if( "news".equalsIgnoreCase(paramPath) ) { 
//			bbsType  = "10150120";
//			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.NewsCoun" );
		} else if( "tip".equalsIgnoreCase(paramPath) ) {
			bbsType  = "10150150";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.NoteTip" );
		}
		if( "".equals(paramPath) || BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
//			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
//			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
//			return "/cmmn/exe_script";
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "잘못된 경로로 접속하였습니다.\n\n메인화면으로 전환합니다.");
			return mav;
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/board/note"+ BaseUtility.toCapitalize( paramPath ) +"_edit";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			// 지정된 파일만 업로드 처리
			if( !(fileMap == null || fileMap.size() <= 0) && fileUP ) for( MultipartFile mFile : fileMap.values() ) { 
				if( !(mFile.isEmpty() || mFile.getSize() < 0) && mFile.getName().contains("fileBase") && !BaseUtility.getFileAllowOfDocuImage( mFile.getOriginalFilename() ) ) { 
//					model.addAttribute("error"         , "첨부 불가능한 파일이 존재합니다.");
//					return "/cmmn/exe_script";
					mav.addObject("message"       , "첨부 불가능한 파일이 존재합니다.").addObject("isError"       , "T");
					return mav;
				}
			}

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)boardMngrService.getInfoOfBoardUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);
				dataView = (BaseBoardsVO)boardMngrService.getDetailOfBoard( searchVO );
				if( BaseUtility.isValidNull(searchVO.getRePass()) ) { 
					// 인증내역이 있는 경우(본인작성글이 아닌 경우)
					if( !(dataView.getCertInfo().equals(authInfo.getCertInfo())) ) { 
						mav.addObject("isError"       , "T");
						mav.addObject("message"       , "작성된 게시글이 인증된 내역과 달라 수정이 제한되었습니다.\n\n확인하시기 바랍니다.");
						return mav;
					}
				} else { 
					// 입력된 비밀번호가 다른경우
					if( !(dataView.getCrePasswd().equals(searchVO.getRePass())) ) { 
						mav.addObject("isError"       , "T");
						mav.addObject("message"       , "작성된 게시글이 입력한 비밀번호가 달라 수정이 제한되었습니다.\n\n확인하시기 바랍니다.");
						return mav;
					}
				}

				// 입력내역 취득(게시판)
				searchVO.setBbsType(viewNo[0]);
				searchVO.setBbsSeq(viewNo[1]);
				searchVO.setBgnDate(BaseUtility.null2Blank( searchVO.getBgnDate() ).replaceAll("[^0-9]", ""));
				searchVO.setEndDate(BaseUtility.null2Blank( searchVO.getEndDate() ).replaceAll("[^0-9]", ""));
				searchVO.setContSize(String.valueOf(BaseUtility.removeAllTags( searchVO.getContDesc() ).length()));
				searchVO.setCreOrder("on".equalsIgnoreCase(BaseUtility.null2Blank( searchVO.getCreOrder() )) ? searchVO.getBbsSeq() : null);
//				searchVO.setCreHide("on".equalsIgnoreCase(BaseUtility.null2Blank( searchVO.getCreHide() )) ? "Y" : BaseUtility.null2Blank( searchVO.getCreHide() ));
				searchVO.setModIPv4(BaseUtility.reqIPv4Address( request ));
				if( "hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) ) {
					searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), authInfo.getCertName() ));
					searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), authInfo.getCertName() ));
				} else { 
					searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
					searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));
				}
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq() +" | "+ searchVO.getCondDele());

				// 첨부파일 삭제처리
				if( fileUP ) { 
					deleList = BaseUtility.isValidNull( searchVO.getCondDele() ) ? null : BaseUtility.null2Blank( searchVO.getCondDele() ).split("`");
					if( !(deleList == null || deleList.length <= 0) ) { 
						for( int f = 0; f < deleList.length; f++ ) { 
							fileDele = new BaseFilesVO();
							fileDele.setRowUUID(deleList[f]);
							fileDele.setDelIPv4(BaseUtility.reqIPv4Address( request ));
							fileDele.setDelUser(BaseUtility.null2Char( searchVO.getDelUser(), memberVO.getUserPID() ));
							fileDele.setDelName(BaseUtility.null2Char( searchVO.getDelName(), memberVO.getUserName() ));

							baseCmmnService.doRtnOfDeleteDataFile( fileDele );
							fileCnt--;
						}
					}
				}
				// 썸네일 갱신처리
				if( fileUP ) thmbCnt = FileProcess.doActOfCreateFileThmb( request, response, baseCmmnService, systemVO, memberVO, fileMap, fileCnt, filePath, lnkName, searchVO.getBbsType() + searchVO.getBbsSeq() );
				// 첨부파일 갱신처리
				if( fileUP ) fileCnt = FileProcess.doActOfCreateFileBase( request, response, baseCmmnService, systemVO, memberVO, fileMap, fileCnt, filePath, lnkName, searchVO.getBbsType() + searchVO.getBbsSeq() );

				// 게시판 내역 갱신
				searchVO.setBbsFiles(String.valueOf(fileCnt));
				boardMngrService.doRtnOfUpdateBoard( searchVO );
				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
			if( removeSign && "Q".equals(dataView.getAnsMode()) && "0".equals(dataView.getAnsCount()) ) { 
				model.remove("authInfo");
				session.removeAttribute("NMinto");
				session.removeAttribute("DIinto");
			}
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//-		return "redirect:/coun/"+ paramPath.toLowerCase() +"/noticeView.do";
//		return "redirect:/coun/"+ paramPath.toLowerCase() +"/noticeList.do";
		return mav;
	}	// end of doActOfUpdateNotice

	/**
	 * <pre>
	 * 게시판 내역을 삭제처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return String
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/{paramPath}/noticeDelete.do")
	public ModelAndView doActOfDeleteNotice ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseBoardsVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseBoardsVO			dataView     = new BaseBoardsVO();

		if( !("hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) || "arch".equalsIgnoreCase(paramPath)) ) {
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Delete", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접근이 불가능합니다.\n\n이전화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeList.do");
		}
		// 인증내역 확인
		BaseMemberVO			authInfo     = new BaseMemberVO();
		if( "hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) ) {
			// 비밀번호 입력글은 인증제외
			if( !BaseUtility.isValidNull(searchVO.getRePass()) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Delete", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			} else { 
				authInfo.setCertName(BaseUtility.null2Blank( (String)session.getAttribute("NMinto") ));
				authInfo.setCertInfo(BaseUtility.null2Blank( (String)session.getAttribute("DIinto") ));
				searchVO.setNextMode("Delete");
				// 인증내역이 없으면 인증화면으로 이동
				if( BaseUtility.isValidNull( authInfo.getCertName() ) && BaseUtility.isValidNull( authInfo.getCertInfo() ) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Delete", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
					WebUtils.setSessionAttribute(request, "searchSess", searchVO);
//					model.addAttribute("message"       , "본인인증 후 사용 가능합니다.\\n\\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
//					model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeAgre.do");
//					return "/cmmn/exe_script";
					mav.addObject("isError"       , "C");
//					mav.addObject("message"       , "본인인증 후 사용 가능합니다.\n\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
					mav.addObject("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeAgre.do");
					return mav;
				}
				model.addAttribute("authInfo"      , authInfo);
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Delete", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + authInfo.getCertName());
			}
		}
		if( "arch".equalsIgnoreCase(paramPath) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Delete", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			if( !UserDetailsHelper.isAuthenticatedUser() ) { 
				mav.addObject("isError"       , "T");
//				mav.addObject("message"       , "로그인 후 사용 가능합니다.\n\n로그인화면으로 이동합니다.");
				mav.addObject("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeHuin.do");
				return mav;
			}
			if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
				mav.addObject("isError"       , "T");
				mav.addObject("message"       , "임시 비밀번호를 사용하고 있습니다.\n\n비밀번호 변경화면으로 이동합니다.");
				mav.addObject("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticePass.do");
			}
		}

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
		} else if( "hope".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150060";
		} else if( "arch".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150090";
//		} else if( "press".equalsIgnoreCase(paramPath) ) { 
//			bbsType  = "10150040";
//		} else if( "news".equalsIgnoreCase(paramPath) ) { 
//			bbsType  = "10150120";
		} else if( "tip".equalsIgnoreCase(paramPath) ) {
			bbsType  = "10150150";
		}
		if( "".equals(paramPath) || BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
//			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
//			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
//			return "/cmmn/exe_script";
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "잘못된 경로로 접속하였습니다.\n\n메인화면으로 전환합니다.");
			return mav;
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)boardMngrService.getInfoOfBoardUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(게시판)
				searchVO.setBbsType(viewNo[0]);
				searchVO.setBbsSeq(viewNo[1]);
				dataView = (BaseBoardsVO)boardMngrService.getDetailOfBoard( searchVO );
				if( "hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) ) {
					if( BaseUtility.isValidNull(searchVO.getRePass()) ) { 
						// 인증내역이 있는 경우(본인작성글이 아닌 경우)
						if( !(dataView.getCertInfo().equals(authInfo.getCertInfo())) ) { 
							mav.addObject("isError"       , "T");
							mav.addObject("message"       , "작성된 게시글이 인증된 내역과 달라 삭제가 제한되었습니다.\n\n확인하시기 바랍니다.");
							return mav;
						}
					} else { 
						// 입력된 비밀번호가 다른경우
						if( !(dataView.getCrePasswd().equals(searchVO.getRePass())) ) { 
							mav.addObject("isError"       , "T");
							mav.addObject("message"       , "작성된 게시글이 입력한 비밀번호가 달라 삭제가 제한되었습니다.\n\n확인하시기 바랍니다.");
							return mav;
						}
					}
				}
				searchVO.setDelIPv4(BaseUtility.reqIPv4Address( request ));
				if( "hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) ) {
					searchVO.setDelUser(BaseUtility.null2Char( searchVO.getDelUser(), authInfo ==  null ? dataView.getCreUser() : authInfo.getCertName() ));
					searchVO.setDelName(BaseUtility.null2Char( searchVO.getDelName(), authInfo ==  null ? dataView.getCreName() : authInfo.getCertName() ));
				} else { 
					searchVO.setDelUser(BaseUtility.null2Char( searchVO.getDelUser(), memberVO.getUserPID() ));
					searchVO.setDelName(BaseUtility.null2Char( searchVO.getDelName(), memberVO.getUserName() ));
				}
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());

				// 게시판 내역 삭제
				boardMngrService.doRtnOfDeleteBoard( searchVO );
				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
			if( removeSign && "Q".equals(dataView.getAnsMode()) && "0".equals(dataView.getAnsCount()) ) { 
				model.remove("authInfo");
				session.removeAttribute("NMinto");
				session.removeAttribute("DIinto");
			}
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/coun/"+ paramPath.toLowerCase() +"/noticeList.do";
		return mav;
	}	// end of doActOfDeleteNotice

	/**
	 * <pre>
	 * 게시판 내역을 변경처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/{paramPath}/noticeModify.do")
	public ModelAndView doActOfModifyNotice ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseBoardsVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseBoardsVO			dataView     = new BaseBoardsVO();

		if( !("hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) || "arch".equalsIgnoreCase(paramPath)) ) {
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Modify", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접근이 불가능합니다.\n\n이전화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeList.do");
		}
		// 인증내역 확인
		BaseMemberVO			authInfo     = new BaseMemberVO();
		if( "hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) ) {
			// 비밀번호 입력글은 인증제외
			if( !BaseUtility.isValidNull(searchVO.getRePass()) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Modify", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			} else { 
				authInfo.setCertName(BaseUtility.null2Blank( (String)session.getAttribute("NMinto") ));
				authInfo.setCertInfo(BaseUtility.null2Blank( (String)session.getAttribute("DIinto") ));
				searchVO.setNextMode("Modify");
				// 인증내역이 없으면 인증화면으로 이동
				if( BaseUtility.isValidNull( authInfo.getCertName() ) && BaseUtility.isValidNull( authInfo.getCertInfo() ) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Modify", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
					WebUtils.setSessionAttribute(request, "searchSess", searchVO);
//					model.addAttribute("message"       , "본인인증 후 사용 가능합니다.\\n\\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
//					model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeAgre.do");
//					return "/cmmn/exe_script";
					mav.addObject("isError"       , "C");
//					mav.addObject("message"       , "본인인증 후 사용 가능합니다.\n\n인증내역이 존재하지 않으므로 인증화면으로 이동합니다.");
					mav.addObject("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeAgre.do");
					return mav;
				}
				model.addAttribute("authInfo"      , authInfo);
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Modify", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + authInfo.getCertName());
			}
		}
		if( "arch".equalsIgnoreCase(paramPath) ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Modify", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			if( !UserDetailsHelper.isAuthenticatedUser() ) { 
				mav.addObject("isError"       , "T");
//				mav.addObject("message"       , "로그인 후 사용 가능합니다.\n\n로그인화면으로 이동합니다.");
				mav.addObject("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeHuin.do");
				return mav;
			}
			if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
				mav.addObject("isError"       , "T");
				mav.addObject("message"       , "임시 비밀번호를 사용하고 있습니다.\n\n비밀번호 변경화면으로 이동합니다.");
				mav.addObject("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticePass.do");
			}
		}

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
		} else if( "hope".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150060";
		} else if( "arch".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150090";
//		} else if( "press".equalsIgnoreCase(paramPath) ) { 
//			bbsType  = "10150040";
//		} else if( "news".equalsIgnoreCase(paramPath) ) { 
//			bbsType  = "10150120";
		} else if( "tip".equalsIgnoreCase(paramPath) ) {
			bbsType  = "10150150";
		}
		if( "".equals(paramPath) || BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
//			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
//			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
//			return "/cmmn/exe_script";
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "잘못된 경로로 접속하였습니다.\n\n메인화면으로 전환합니다.");;
			return mav;
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/board/note"+ BaseUtility.toCapitalize( paramPath ) +"_view";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)boardMngrService.getInfoOfBoardUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(게시판)
				searchVO.setBbsType(viewNo[0]);
				searchVO.setBbsSeq(viewNo[1]);
				dataView = (BaseBoardsVO)boardMngrService.getDetailOfBoard( searchVO );
				if( "hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) ) {
					if( BaseUtility.isValidNull(searchVO.getRePass()) ) { 
						// 인증내역이 있는 경우(본인작성글이 아닌 경우)
						if( !(dataView.getCertInfo().equals(authInfo.getCertInfo())) ) { 
							mav.addObject("isError"       , "T");
							mav.addObject("message"       , "작성된 게시글이 인증된 내역과 달라 내용변경이 제한되었습니다.\n\n확인하시기 바랍니다.");
							return mav;
						}
					} else { 
						// 입력된 비밀번호가 다른경우
						if( !(dataView.getCrePasswd().equals(searchVO.getRePass())) ) { 
							mav.addObject("isError"       , "T");
							mav.addObject("message"       , "작성된 게시글이 입력한 비밀번호가 달라 내용변경이 제한되었습니다.\n\n확인하시기 바랍니다.");
							return mav;
						}
					}
				}
//				searchVO.setCreHide("on".equalsIgnoreCase(BaseUtility.null2Blank( searchVO.getCreHide() )) ? "Y" : BaseUtility.null2Blank( searchVO.getCreHide() ));
				searchVO.setModIPv4(BaseUtility.reqIPv4Address( request ));
				if( "hope".equalsIgnoreCase(paramPath) || "tip".equalsIgnoreCase(paramPath) ) {
					searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), authInfo ==  null ? dataView.getCreUser() : authInfo.getCertName() ));
					searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), authInfo ==  null ? dataView.getCreName() : authInfo.getCertName() ));
				} else { 
					searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
					searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));
				}
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());

				// 게시판 내역 갱신
				boardMngrService.doRtnOfModifyBoardRef( searchVO );
				boardMngrService.doRtnOfModifyBoardAns( searchVO );
				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
			if( removeSign && "Q".equals(dataView.getAnsMode()) ) { 
				model.remove("authInfo");
				session.removeAttribute("NMinto");
				session.removeAttribute("DIinto");
			}
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/coun/"+ paramPath.toLowerCase() +"/noticeView.do";
		return mav;
	}	// end of doActOfModifyNotice

	/**
	 * <pre>
	 * 사용자 비밀번호 내역을 변경처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/{paramPath}/noticeChange.do")
	public ModelAndView doActOfChangeNotice ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseMemberVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		PrivateKey				privateKey   = (PrivateKey)session.getAttribute(this.RSA_WEB_KEY);
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseMemberVO			dataView     = new BaseMemberVO();
		String[]				resultPWD    = new String[4];

		if( !"arch".equalsIgnoreCase(paramPath)  ) { 
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Change", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접근이 불가능합니다.\n\n이전화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeList.do");
		}
log.debug(" act: "+ BaseUtility.spaceFill( "Notice"+ BaseUtility.toCapitalize( paramPath ) +"Change", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
//			model.addAttribute("message"       , "로그인 정보가 존재하지 않습니다.\\n\\n로그인 화면으로 전환합니다.");
//			model.addAttribute("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeHuin.do");
//			return "/cmmn/exe_script";
			mav.addObject("isError"       , "T");
//			mav.addObject("message"       , "로그인 정보가 존재하지 않습니다.\n\n로그인 화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/coun/"+ paramPath.toLowerCase() +"/noticeHuin.do");
			return mav;
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		uuidNo = memberVO.getUserUUID() +"1";
		searchVO.setViewNo(uuidNo);
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("viewNo: "+ searchVO.getRowUUID() +" | "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/manage/membStaff_pass";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

//log.debug("p_pass: "+ searchVO.getP_userpass());	// 변환 기존
//log.debug("c_pass: "+ searchVO.getC_userpass());	// 변환 신규
//log.debug("j_pass: "+ searchVO.getJ_userpass());	// 변환 최종
//log.debug("o_pass: "+ searchVO.getO_userpass());	// 입력 기존
//log.debug("n_pass: "+ searchVO.getN_userpass());	// 입력 신규
//log.debug("k_pass: "+ searchVO.getK_userpass());	// 입력 최종
			if( !BaseUtility.isValidNull( searchVO.getJ_userpass() ) ) { 
				resultPWD = new String[4];
				resultPWD[0] = BaseCategory.decryptRSA( privateKey, searchVO.getJ_userpass() );
				resultPWD[1] = BaseCategory.decryptRSA( privateKey, searchVO.getP_userpass() );
				resultPWD[2] = BaseCategory.decryptRSA( privateKey, searchVO.getC_userpass() );
				session.removeAttribute(this.RSA_WEB_KEY);	// 재사용 금지
//log.debug("o_pass: "+ resultPWD[1]);
//log.debug("n_pass: "+ resultPWD[2]);
//log.debug("k_pass: "+ resultPWD[0]);

				if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
					dataView = (BaseMemberVO)userMngrService.getDetailOfStaff( searchVO );

					// 기존 비밀번호 확인
					if( !dataView.getUserPWD().equals(BaseCategory.encryptSHA256( dataView.getUserPID() + dataView.getBgnDate() + resultPWD[1] ).toUpperCase()) ) { 
						mav.addObject("isError"       , "T");
						mav.addObject("message"       , "기존 비밀번호를 다르게 입력되었습니다.\n\n다시 입력하시기 바랍니다..");
						return mav;
					}

					// 입력내역 취득(사용자)
					searchVO.setUserPWD(BaseCategory.encryptSHA256( dataView.getUserPID() + dataView.getBgnDate() + resultPWD[0] ).toUpperCase());
					searchVO.setPwdDateUse("A".equals(dataView.getAuthFlag())||"SU".equals(dataView.getAuthFlag())?"F":"T");
					searchVO.setPwdDateTerm("20991231");
					searchVO.setPwdDateMod("A".equals(dataView.getAuthFlag())||"SU".equals(dataView.getAuthFlag())?"20991231":systemVO.getToDate());
					searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
					searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));

					// 사용자 비번 내역 갱신
					userMngrService.doRtnOfModifyStaffPass( searchVO );
					searchVO.setViewNo(searchVO.getRowUUID() +"1");
					model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());

					memberVO.setUserPWD(searchVO.getUserPWD());
					memberVO.setPwdDateUse(searchVO.getPwdDateUse());
					memberVO.setPwdDateTerm(searchVO.getPwdDateTerm());
					memberVO.setPwdDateMod(searchVO.getPwdDateMod());
					request.getSession().setAttribute("MemUInfo"      , memberVO);
				}
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/ptnhexa/User/MemberList.do";
		return mav;
	}	// end of doActOfChangeMember

//	@ExceptionHandler(MaxUploadSizeExceededException.class)
	public ModelAndView resolveException(HttpServletRequest request,HttpServletResponse response,Object handler,Exception exception){
		ModelAndView			mav          = new ModelAndView("jsonView");
		if(exception instanceof MaxUploadSizeExceededException)
			mav.addObject("message"       , "첨부파일 사이즈는 "+ BaseUtility.toNumberFormat( (((MaxUploadSizeExceededException)exception).getMaxUploadSize()/1024/1024) ) +"MB를 초과할 수 없습니다.").addObject("isError"       , "T");
		return mav;
	}

}	// end of class