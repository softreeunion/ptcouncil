package egovframework.ptcc.coun.content.web;

import egovframework.ptcc.cmmn.vo.BaseAgendaVO;
import egovframework.ptcc.mngr.agenda.service.AgendaMngrService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping(value = "/coun")
public class ExcelCounController {

    @Resource(name="agendaMngrService")
    protected AgendaMngrService agendaMngrService;

    @RequestMapping(value = "/excel/test.do")
    @ResponseBody
    public String Test() {

        try {
            FileInputStream file = new FileInputStream("C:/Users/gintoki/Desktop/PTCOUNCIL/6대.xlsx");
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //sheet수 취득
            int sheetCnt = workbook.getNumberOfSheets();
            System.out.println("sheet수 : " + sheetCnt);
            int selSheetNum = 0;
            for (int num = 0; num < sheetCnt; num++) {
                System.out.println("sheet 이름 : " + workbook.getSheetName(num));
                if ("규칙공포번호".equals(workbook.getSheetName(num))) {
                    selSheetNum = num;
                    break;
                }
            }


            int rowindex = 0;
            int columnindex = 0;
            //시트 수 (첫번째에만 존재하므로 0을 준다)
            //만약 각 시트를 읽기위해서는 FOR문을 한번더 돌려준다
            XSSFSheet sheet = workbook.getSheetAt(selSheetNum); // sheet 번호
            //행의 수
            int rows = sheet.getPhysicalNumberOfRows();
            for (rowindex = 0; rowindex < rows; rowindex++) {
                //행을읽는다
                XSSFRow row = sheet.getRow(rowindex);
                if (row != null) {
                    //셀의 수
                    int cells = row.getPhysicalNumberOfCells();
                    String sugSeq = "";
                    String billNum = "";
                    String tranDate = "";
                    String publDate = "";
                    String publNum = "";
                    for (columnindex = 0; columnindex <= cells; columnindex++) {
                        //셀값을 읽는다
                        XSSFCell cell = row.getCell(columnindex);
                        String value = "";
                        //셀이 빈값일경우를 위한 널체크
                        if (cell == null) {
                            continue;
                        } else {
                            //타입별로 내용 읽기
                            switch (cell.getCellType()) {
                                case XSSFCell.CELL_TYPE_FORMULA:
                                    value = cell.getCellFormula();
                                    break;
                                case XSSFCell.CELL_TYPE_NUMERIC:
                                    value = (int)cell.getNumericCellValue() + "";
                                    break;
                                case XSSFCell.CELL_TYPE_STRING:
                                    value = cell.getStringCellValue() + "";
                                    break;
                                case XSSFCell.CELL_TYPE_BLANK:
                                    value = cell.getBooleanCellValue() + "";
                                    break;
                                case XSSFCell.CELL_TYPE_ERROR:
                                    value = cell.getErrorCellValue() + "";
                                    break;
                            }
                        }
//                        System.out.println(rowindex + "번 행 : " + columnindex + "번 열 값은: " + value);

                        if(columnindex == 0) sugSeq = value;
                        else if(columnindex == 2) billNum = value;
                        else if(columnindex == 3) tranDate = value;
                        else if(columnindex == 4) publDate = value;
                        else if(columnindex == 5) publNum = value;
                    }

                    if ( isInteger(sugSeq) ) {
                        BaseAgendaVO vo = new BaseAgendaVO();
                        vo.setSugSeq(sugSeq);
                        vo.setBillNum(billNum);
                        vo.setTranDate(tranDate);
                        vo.setPublDate(publDate);
                        vo.setPublNum(publNum);
                        System.out.println("Update Agenda: " + vo.getSugSeq() + ", " + vo.getBillNum() + ", " + vo.getTranDate() + ", " + vo.getPublDate() + ", " + vo.getPublNum());
                        agendaMngrService.doRtnOfUpdateConfSug(vo);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "Test";
    }

    public boolean isInteger(String num)
    {
        try
        {
            Integer.parseInt(num);
            return true; // 이상없으면 true를 리턴
        }
        catch (NumberFormatException e)
        {
            return false; // 이상 있으면 false를 리턴
        }
    }
}
