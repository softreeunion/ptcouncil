/*******************************************************************************
  Program ID  : MemberCounController
  Description : 클라이언트의 요청(공통)을 처리할 메서드를 구현
  Author      : HexaMedia new0man
  Write Date  : 2019.06.04
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v1.0    (주)헥사미디어 개발자  2014.00.00  최초 생성
  v2.0    HexaMedia new0man      2019.06.04  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.coun.content.web;

import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.jdbc.datasource.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springmodules.validation.commons.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.util.UserDetailsHelper;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.member.service.*;

@Controller
@RequestMapping(value="/coun")
public class MemberCounController {

	@Autowired
	private WebApplicationContext			webContext;
	@Autowired
	private DataSourceTransactionManager	txManager;

	@Resource(name="beanValidator")
	protected DefaultBeanValidator			beanValidator;
	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="membMngrService")
	protected MembMngrService				membMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));
	protected boolean						notiItems    = false;
	protected boolean						moveItems    = true;
	protected boolean						fileUP       = false;
	protected boolean						tempUP       = false;


	/**
	 * <pre>
	 * 의원 내역을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/memb/active{paramPath}.do")
	public String scrListOfMember ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					retPath      = "";

		int						listCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseAgendaVO>		dataList     = new ArrayList<BaseAgendaVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		eraList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		regList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		tempList     = new ArrayList<BaseCommonVO>();
		BaseCommonVO			tempView     = new BaseCommonVO();
		String					numbText     = "";
		String[]				numbList     = new String[] {"zero","one","two","three","four","five","six","seven","eight","nine"};
log.debug(" act: "+ BaseUtility.spaceFill( "active"+paramPath, 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "base".equalsIgnoreCase(paramPath) ) { 
			retPath  = "memb/active"+paramPath+"_list";
		} else if( "popup".equalsIgnoreCase(paramPath) ) { 
			retPath  = "memb/active"+paramPath+"_view";
		} else { 
			retPath  = "memb/active"+paramPath+"_list";
		}
		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		pageInfo.setTotalRecordCount(1);
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(10);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "N" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reEraCode"     , searchVO.getReEraCode());
		model.addAttribute("reRegCode"     , searchVO.getReRegCode());

		// 추가 조회조건 취득
//		searchVO.setCondCate(BaseUtility.null2Char( searchVO.getCondCate(), "%" ));
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("eraCode: "+ searchVO.getReEraCode() +" |regCode: "+ searchVO.getRegCode());
		try { 
			if( BaseUtility.isValidNull( searchVO.getReEraCode() ) ) { 
				// 현재 회의대수 취득 - 현역위원,의장단
				if( "Name".equalsIgnoreCase(paramPath) || "Area".equalsIgnoreCase(paramPath) || "stand".equalsIgnoreCase(paramPath) || "Party".equalsIgnoreCase(paramPath) || "Chair".equalsIgnoreCase(paramPath) ) {
					log.debug("1. eraCode: "+ searchVO.getReEraCode());
					// 분류구분 취득 - 현재 회의대수 구분
					tempView = (BaseCommonVO)baseCmmnService.getInfoOfClassNOW( "" );
					log.debug("2. tempView eraCode: "+ tempView.getEraCode());
					searchVO.setReEraCode(tempView == null ? BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsCate" ) : tempView.getEraCode());
					model.addAttribute("reEraCode"     , searchVO.getReEraCode());
					log.debug("3. new eraCode: "+ searchVO.getReEraCode());

//log.debug("new eraCode: "+ searchVO.getReEraCode());
				}
				// 이전 회의대수 취득 - 역대의원
				if( "Former".equalsIgnoreCase(paramPath) ) { 
					// 분류구분 취득 - 이전 회의대수 구분
					tempView = (BaseCommonVO)baseCmmnService.getInfoOfClassAGO( "" );
					searchVO.setReEraCode(tempView == null ? "07" : tempView.getEraCode());
					model.addAttribute("reEraCode"     , searchVO.getReEraCode());
log.debug("new eraCode: "+ searchVO.getReEraCode());
				}
			}
			listCnt = (int)membMngrService.getCountOfMemb( searchVO );

			// 조회건수 전체선택 - 현역의원,의장단,역대위원
			if( "Name".equalsIgnoreCase(paramPath) || "Area".equalsIgnoreCase(paramPath) || "Stand".equalsIgnoreCase(paramPath) || "Party".equalsIgnoreCase(paramPath) || "Chair".equalsIgnoreCase(paramPath) || "Former".equalsIgnoreCase(paramPath) ) { 
				searchVO.setPagePerNo("all");
			}
			// 정렬기준
			if( "Area".equalsIgnoreCase(paramPath) ) searchVO.setOrdMode("A");
			if( "Party".equalsIgnoreCase(paramPath) ) searchVO.setOrdMode("P");

			// 조회 건수를 전체선택인 경우
			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
			}
			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
			searchVO.setPageFirNo(Integer.toString(idxFir));
			searchVO.setPageLstNo(Integer.toString(idxLst));
			searchVO.setListCnt(Integer.toString(listCnt));

			if( listCnt > 0 ) { 
				dataList = (List<BaseAgendaVO>)membMngrService.getListOfMemb( searchVO );
//				dataList = (List<BaseAgendaVO>)membMngrService.getListOfMemb( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				// Data Adjustment(Members)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					for( int d = 0; d < dataList.size(); d++ ) { 
						dataList.get(d).setViewNo(dataList.get(d).getMemID());

						if( !"Finder".equalsIgnoreCase(paramPath) ) dataList.get(d).setComName(BaseUtility.null2Char( dataList.get(d).getComName(), "의원" ));
					}
				}

				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
				pageInfo.setTotalRecordCount(listCnt);
			}

			// 분류구분 취득 - 전체 회의대수 구분
			eraList = (List<BaseCommonVO>)baseCmmnService.getListOfClassALL( "" );
			// 분류구분 취득 - 선거구 구분
			regList = (List<BaseCommonVO>)baseCmmnService.getListOfParty( searchVO.getReEraCode() );

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			model.addAttribute("dataList"      , dataList);
			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (dataList == null ? 0 : dataList.size()) : listCnt ));
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("eraList"       , eraList);
			model.addAttribute("regList"       , regList);
			model.addAttribute("numbText"      , numbText = eraList == null || eraList.size() <= 0 ? numbList[1] : numbList[eraList.size()-1]);
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/coun/"+retPath;
	}	// end of scrListOfMember

	/**
	 * <pre>
	 * 의원 내역을(를) 상세조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/activePopup.do")
	public String scrPopupOfMember ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseAgendaVO			dataView     = new BaseAgendaVO();
		List<BaseFilesVO>		dataFile     = new ArrayList<BaseFilesVO>();
		List<BaseFilesVO>		thmbFile     = new ArrayList<BaseFilesVO>();
log.debug(" act: "+ BaseUtility.spaceFill( "activePopup", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 추가 조회조건 취득
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));
		searchVO.setActMode(BaseUtility.null2Char(searchVO.getActMode(), "N"));

log.debug("viewNo: "+ searchVO.getRowUUID() +" | "+ searchVO.getRowSeq() +" |actMode: "+ searchVO.getActMode());
		try { 
			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				dataView = (BaseAgendaVO)membMngrService.getDetailOfMemb( searchVO );
				if( dataView != null ) { 
					// Data Adjustment(Members)
					dataView.setActMode("Update");
					dataView.setViewNo(dataView.getMemID());

					if( "A".equals(searchVO.getActMode()) ) 
						dataView.setComName("위원장");
					else if( "B".equals(searchVO.getActMode()) ) 
						dataView.setComName("부위원장");
					else if( "C".equals(searchVO.getActMode()) ) 
						dataView.setComName("위원");
					else 
						dataView.setComName(BaseUtility.null2Char( dataView.getComName(), "의원" ));

					dataView.setMemIntroBR(BaseUtility.tag2Sym( dataView.getMemIntro() ));
					dataView.setMemIntroCR(BaseUtility.sym2Tag( dataView.getMemIntro() ));
					dataView.setMemProfileBR(BaseUtility.tag2Sym( dataView.getMemProfile() ));
					dataView.setMemProfileCR(BaseUtility.sym2Tag( dataView.getMemProfile() ));
					dataView.setMemPromiseBR(BaseUtility.tag2Sym( dataView.getMemPromise() ));
					dataView.setMemPromiseCR(BaseUtility.sym2Tag( dataView.getMemPromise() ));
				}
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("dataFile"      , dataFile);
			model.addAttribute("thmbFile"      , thmbFile);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/coun/memb/activePopup_view";
	}	// end of scrPopupOfMember

}	// end of class