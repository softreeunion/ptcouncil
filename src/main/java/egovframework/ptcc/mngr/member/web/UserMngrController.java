/*******************************************************************************
  Program ID  : UserMngrController
  Description : 사용자 관리에 관한 controller 클래스를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.member.web;

import java.security.*;
import java.security.spec.*;
import java.util.*;

import javax.annotation.*;
import javax.servlet.http.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.dao.*;
import org.springframework.jdbc.*;
import org.springframework.jdbc.datasource.*;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.*;
import org.springframework.transaction.support.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.*;
import org.springmodules.validation.commons.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.excel.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.util.*;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.member.service.*;

@Controller
@RequestMapping(value="/ptnhexa/User")
public class UserMngrController {

	@Autowired
	private WebApplicationContext			webContext;
	@Autowired
	private DataSourceTransactionManager	txManager;

	@Resource(name="beanValidator")
	protected DefaultBeanValidator			beanValidator;
	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="userMngrService")
	protected UserMngrService				userMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));
	protected String						defsPass     = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsPass" );
	protected boolean						notiItems    = false;
	protected boolean						moveItems    = false;
	protected boolean						fileUP       = false;
	protected boolean						tempUP       = false;

	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelMap model, 
			boolean connMode 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			model.addAttribute("message"       , "로그인 정보가 존재하지 않습니다.\\n\\n로그인 화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			model.addAttribute("message"       , "임시 비밀번호를 사용하고 있습니다.\\n\\n비밀번호 변경화면으로 이동합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			model.addAttribute("close"         , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			model.addAttribute("error"         , "접속권한이 없습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		return false;
	}
	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelMap model, 
			boolean connMode, 
			String viewNo 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			model.addAttribute("message"       , "로그인 정보가 존재하지 않습니다.\\n\\n로그인 화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			model.addAttribute("message"       , "임시 비밀번호를 사용하고 있습니다.\\n\\n비밀번호 변경화면으로 이동합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			model.addAttribute("close"         , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			model.addAttribute("error"         , "접속권한이 없습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 불량 접속 확인
		if( BaseUtility.isValidNull( viewNo ) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		return false;
	}
	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelAndView mav, 
			boolean connMode 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "로그인 정보가 존재하지 않습니다.\n\n로그인 화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "임시 비밀번호를 사용하고 있습니다.\n\n비밀번호 변경화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속권한이 없습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		return false;
	}
	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelAndView mav, 
			boolean connMode, 
			String viewNo 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "로그인 정보가 존재하지 않습니다.\n\n로그인 화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "임시 비밀번호를 사용하고 있습니다.\n\n비밀번호 변경화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속권한이 없습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 불량 접속 확인
		if( BaseUtility.isValidNull( viewNo ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "잘못된 경로로 접속하였습니다.\n\n메인화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		return false;
	}

	private String							RSA_WEB_KEY  = "__rsaPrivateKey__";	// 개인키 session key
	private String							RSA_INSTANCE = "RSA";				// rsa transformation

	private void initRSA ( 
			HttpServletRequest request 
		) throws Exception { 

		HttpSession				session      = request == null ? null : request.getSession();
		KeyPairGenerator		generator;

		try { 
			generator = KeyPairGenerator.getInstance(this.RSA_INSTANCE);
			generator.initialize(1024);

			KeyPair keyPair = generator.genKeyPair();
			KeyFactory keyFactory = KeyFactory.getInstance(this.RSA_INSTANCE);

			PublicKey publicKey = keyPair.getPublic();
			PrivateKey privateKey = keyPair.getPrivate();

			// 세션에 공개키의 문자열을 키로하여 개인키를 저장한다.
			session.setAttribute(this.RSA_WEB_KEY, privateKey);

			// 공개키를 문자열로 변환하여 JavaScript RSA 라이브러리 넘겨준다.
			RSAPublicKeySpec publicSpec = (RSAPublicKeySpec) keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);
			String publicKeyModulus = publicSpec.getModulus().toString(16);
			String publicKeyExponent = publicSpec.getPublicExponent().toString(16);
			request.setAttribute("RSAModulus", publicKeyModulus); // rsa modulus 를 request 에 추가
			request.setAttribute("RSAExponent", publicKeyExponent); // rsa exponent 를 request 에 추가
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		}
	}

	/**
	 * <pre>
	 * 사용자 내역을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/MemberList.do")
	public String scrListOfMember ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		int						listCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseMemberVO>		dataList     = new ArrayList<BaseMemberVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
log.debug(" act: "+ BaseUtility.spaceFill( "MemberList", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, true ) ) return "/cmmn/exe_script";

		// 기본 조회조건 취득
		pageInfo.setTotalRecordCount(1);
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(10);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reAuthRole"    , searchVO.getReAuthRole());
		model.addAttribute("reAuthFlag"    , searchVO.getReAuthFlag());

		// 추가 조회조건 취득
//		searchVO.setCondCate(BaseUtility.null2Char( searchVO.getCondCate(), "%" ));
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("authRole: "+ searchVO.getReAuthRole() +" |authFlag: "+ searchVO.getReAuthFlag());
		try { 
			listCnt = (int)userMngrService.getCountOfStaff( searchVO );

			// 조회 건수를 전체선택인 경우
			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
			}
			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
			searchVO.setPageFirNo(Integer.toString(idxFir));
			searchVO.setPageLstNo(Integer.toString(idxLst));
			searchVO.setListCnt(Integer.toString(listCnt));

			if( listCnt > 0 ) { 
				dataList = (List<BaseMemberVO>)userMngrService.getListOfStaff( searchVO );
//				dataList = (List<BaseMemberVO>)userMngrService.getListOfStaff( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				// Data Adjustment(Users)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					for( int d = 0; d < dataList.size(); d++ ) { 
						dataList.get(d).setViewNo(dataList.get(d).getUserUUID());

						dataList.get(d).setBgnDate(BaseUtility.toDateFormat( dataList.get(d).getBgnDate(), dtSep, 8 ));
						dataList.get(d).setEndDate(BaseUtility.toDateFormat( dataList.get(d).getEndDate(), dtSep, 8 ));
						dataList.get(d).setPwdDateLst(BaseUtility.toDateFormat( dataList.get(d).getPwdDateLst(), dtSep, 12 ));
						dataList.get(d).setPwdDateTerm(BaseUtility.toDateFormat( dataList.get(d).getPwdDateTerm(), dtSep, 8 ));
						dataList.get(d).setPwdDateMod(BaseUtility.toDateFormat( dataList.get(d).getPwdDateMod(), dtSep, 8 ));
						dataList.get(d).setCreDate(BaseUtility.toDateFormat( dataList.get(d).getCreDate(), dtSep, 8 ));
						dataList.get(d).setModDate(BaseUtility.toDateFormat( dataList.get(d).getModDate(), dtSep, 8 ));
					}
				}

				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
				pageInfo.setTotalRecordCount(listCnt);
			}

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			model.addAttribute("dataList"      , dataList);
			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (dataList == null ? 0 : dataList.size()) : listCnt ));
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("cateList"      , cateList);
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/manage/membStaff_list";
	}	// end of scrListOfMember

	/**
	 * <pre>
	 * 사용자 내역을(를) 상세조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/MemberView.do")
	public String scrViewOfMember ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseMemberVO			dataView     = new BaseMemberVO();
		BaseMemberVO			prevView     = new BaseMemberVO();
		BaseMemberVO			nextView     = new BaseMemberVO();
		List<BaseFilesVO>		dataFile     = new ArrayList<BaseFilesVO>();
		List<BaseFilesVO>		thmbFile     = new ArrayList<BaseFilesVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		authList     = new ArrayList<BaseCommonVO>();
log.debug(" act: "+ BaseUtility.spaceFill( "MemberView", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, true, searchVO.getViewNo() ) ) return "/cmmn/exe_script";

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reAuthRole"    , searchVO.getReAuthRole());
		model.addAttribute("reAuthFlag"    , searchVO.getReAuthFlag());

		// 추가 조회조건 취득
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("viewNo: "+ searchVO.getRowUUID() +" | "+ searchVO.getRowSeq());
		try { 
			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				dataView = (BaseMemberVO)userMngrService.getDetailOfStaff( searchVO );
				if( dataView != null ) { 
					// Data Adjustment(Users)
					dataView.setActMode("Update");
					dataView.setViewNo(dataView.getUserUUID());

					dataView.setBgnDate(BaseUtility.toDateFormat( dataView.getBgnDate(), dtSep, 8 ));
					dataView.setEndDate(BaseUtility.toDateFormat( dataView.getEndDate(), dtSep, 8 ));
					dataView.setPwdDateLst(BaseUtility.toDateFormat( dataView.getPwdDateLst(), dtSep, 12 ));
					dataView.setPwdDateTerm(BaseUtility.toDateFormat( dataView.getPwdDateTerm(), dtSep, 8 ));
					dataView.setPwdDateMod(BaseUtility.toDateFormat( dataView.getPwdDateMod(), dtSep, 8 ));
					dataView.setCreDate(BaseUtility.toDateFormat( dataView.getCreDate(), dtSep, 8 ));
					dataView.setModDate(BaseUtility.toDateFormat( dataView.getModDate(), dtSep, 8 ));
				}
			}

			// 분류구분 취득 - 권한 구분
			authList = (List<BaseCommonVO>)baseCmmnService.getListOfAuth( searchVO );

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("dataFile"      , dataFile);
			model.addAttribute("thmbFile"      , thmbFile);
			model.addAttribute("moveItem"      , moveItems?"T":"F");
			model.addAttribute("prevView"      , prevView);
			model.addAttribute("nextView"      , nextView);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("authList"      , authList);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/manage/membStaff_view";
	}	// end of scrViewOfMember

	/**
	 * <pre>
	 * 사용자 내역 등록화면으로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/MemberInst.do")
	public String scrInstOfMember ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		authList     = new ArrayList<BaseCommonVO>();
log.debug(" act: "+ BaseUtility.spaceFill( "MemberInst", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, true ) ) return "/cmmn/exe_script";

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reAuthRole"    , searchVO.getReAuthRole());
		model.addAttribute("reAuthFlag"    , searchVO.getReAuthFlag());

		// 분류구분 취득 - 권한 구분
//		authList = (List<BaseCommonVO>)baseCmmnService.getListOfAuth( searchVO );

		model.addAttribute("cateList"      , cateList);
		model.addAttribute("authList"      , authList);

		return "/mngr/manage/membStaff_inst";
	}	// end of scrInstOfMember

	/**
	 * <pre>
	 * 사용자 내역 수정화면으로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/MemberEdit.do")
	public String scrEditOfMember ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseMemberVO			dataView     = new BaseMemberVO();
		List<BaseFilesVO>		dataFile     = new ArrayList<BaseFilesVO>();
		List<BaseFilesVO>		thmbFile     = new ArrayList<BaseFilesVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		authList     = new ArrayList<BaseCommonVO>();
		String[]				tempSplit    = null;
log.debug(" act: "+ BaseUtility.spaceFill( "MemberEdit", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, true, searchVO.getViewNo() ) ) return "/cmmn/exe_script";

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reAuthRole"    , searchVO.getReAuthRole());
		model.addAttribute("reAuthFlag"    , searchVO.getReAuthFlag());

		// 추가 조회조건 취득
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("viewNo: "+ searchVO.getRowUUID() +" | "+ searchVO.getRowSeq());
		try { 
			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				dataView = (BaseMemberVO)userMngrService.getDetailOfStaff( searchVO );
				if( dataView != null ) { 
					// Data Adjustment(Users)
					dataView.setActMode("Update");
					dataView.setViewNo(dataView.getUserUUID());

					dataView.setBgnDate(BaseUtility.toDateFormat( dataView.getBgnDate(), dtSep, 8 ));
					dataView.setEndDate(BaseUtility.toDateFormat( dataView.getEndDate(), dtSep, 8 ));
					dataView.setPwdDateLst(BaseUtility.toDateFormat( dataView.getPwdDateLst(), dtSep, 12 ));
					dataView.setPwdDateTerm(BaseUtility.toDateFormat( dataView.getPwdDateTerm(), dtSep, 8 ));
					dataView.setPwdDateMod(BaseUtility.toDateFormat( dataView.getPwdDateMod(), dtSep, 8 ));
					dataView.setCreDate(BaseUtility.toDateFormat( dataView.getCreDate(), dtSep, 8 ));
					dataView.setModDate(BaseUtility.toDateFormat( dataView.getModDate(), dtSep, 8 ));
					if( BaseUtility.null2Blank( dataView.getUserMail() ).replaceAll("@", "").length()  <= 0 ) { 
						dataView.setUserMail("");
					} else { 
						tempSplit = BaseUtility.null2Blank( dataView.getUserMail() ).split("@");
						dataView.setSeleMailA(tempSplit == null || tempSplit.length <= 0 ? "" : tempSplit[0]);
						dataView.setSeleMailT(tempSplit == null || tempSplit.length <= 1 ? "" : tempSplit[1]);
					}
				}
			}

			// 분류구분 취득 - 권한 구분
//			authList = (List<BaseCommonVO>)baseCmmnService.getListOfAuth( searchVO );

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("dataFile"      , dataFile);
			model.addAttribute("thmbFile"      , thmbFile);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("authList"      , authList);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/manage/membStaff_edit";
	}	// end of scrEditOfMember

	/**
	 * <pre>
	 * 사용자 비번 변경화면으로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/MemberPass.do")
	public String scrPassOfMember ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		authList     = new ArrayList<BaseCommonVO>();
log.debug(" act: "+ BaseUtility.spaceFill( "MemberPass", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			model.addAttribute("message"       , "로그인 정보가 존재하지 않습니다.\\n\\n로그인 화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reAuthRole"    , searchVO.getReAuthRole());
		model.addAttribute("reAuthFlag"    , searchVO.getReAuthFlag());

		// 분류구분 취득 - 권한 구분
//		authList = (List<BaseCommonVO>)baseCmmnService.getListOfAuth( searchVO );

		model.addAttribute("cateList"      , cateList);
		model.addAttribute("authList"      , authList);
		model.addAttribute("nextMode"      , webBase+"/ptnhexa"+(BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() )?"/User/Member":"/Base/Notice"));

		this.initRSA(request);

		return "/mngr/manage/membStaff_pass";
	}	// end of scrPassOfMember

	/**
	 * <pre>
	 * 사용자 일괄등록을 위한 엑셀 샘플자료를 다운로드해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
//	@RequestMapping(value="/MemberSample.do")
	public String getSampleOfMember ( 
			@ModelAttribute("searchVO")BaseFilesVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		String[]				fileName     = {"temp_Member.xlsx|","사용자_양식.xlsx|"};
log.debug(" act: "+ BaseUtility.spaceFill( "MemberSample", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

log.debug("fileName: "+ fileName[1]);
		try { 
			model.remove("searchVO");
			searchVO.setFileRel(fileName[0]);
			searchVO.setFileAbs(fileName[1]);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return "redirect:/cmmn/TempDown.do";
	}	// end of getSampleOfMember

	/**
	 * <pre>
	 * 사용자 내역을 엑셀출력해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
//	@RequestMapping(value="/MemberExport.do")
	public View getExportOfMember ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		int						listCnt      = 0;
		List<BaseMemberVO>		dataList     = new ArrayList<BaseMemberVO>();

		String					fileName   = "MemberXLS";
		String					dataName   = "사용자 내역";
		List<String>			colName    = new ArrayList<String>();
		List<String>			colRegion  = new ArrayList<String>();
		List<String>			colAlign   = new ArrayList<String>();
		List<String[]>			colValue   = new ArrayList<String[]>();
		List<Integer>			colWidth   = new ArrayList<Integer>();
log.debug(" act: "+ BaseUtility.spaceFill( "MemberExport", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, true ) ) return null;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
//		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
//		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
//		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정

		// 추가 조회조건 취득
//		searchVO.setCondCate(BaseUtility.null2Char( searchVO.getCondCate(), "%" ));
//		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("authRole: "+ searchVO.getReAuthRole() +" |authFlag: "+ searchVO.getReAuthFlag());
		try { 
			listCnt = (int)userMngrService.getCountOfStaff( searchVO );

			if( listCnt > 0 ) { 
				colName.clear();
				colRegion.clear();
				colAlign.clear();
				colWidth.clear();
				colValue.clear();

				// 상단제목
				colName.add("No");
				colName.add("사용자ID");
				colName.add("사용자명");
				colName.add("권한구분");
				colName.add("승인여부");
				colName.add("최종접속일");
				colName.add("작성일");

				// 행증가,시작열,종료열
				colRegion.add("0`0`0");
				colRegion.add("0`1`1");
				colRegion.add("0`2`2");
				colRegion.add("0`3`3");
				colRegion.add("0`4`4");
				colRegion.add("0`5`5");
				colRegion.add("0`6`6");

				// 내용 가로정렬
				colAlign.add("center");
				colAlign.add("center");
				colAlign.add("left");
				colAlign.add("center");
				colAlign.add("center");
				colAlign.add("center");
				colAlign.add("center");

				// 열너비
				colWidth.add(100);	// 순번
				colWidth.add(225);	// 사용자ID
				colWidth.add(750);	// 사용자명
				colWidth.add(200);	// 권한구분
				colWidth.add(200);	// 승인여부
				colWidth.add(225);	// 최종접속일
				colWidth.add(200);	// 작성일

				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
				dataList = (List<BaseMemberVO>)userMngrService.getListOfStaff( searchVO );
//				dataList = (List<BaseMemberVO>)userMngrService.getListOfStaff( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				// Data Adjustment(Excels)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					for( int d = 0; d < dataList.size(); d++ ) { 
						String[] array = {Integer.toString(d+1) 
										, dataList.get(d).getUserPID() 
										, dataList.get(d).getUserName() 
										, dataList.get(d).getAuthRoleName() 
										, dataList.get(d).getAuthFlagName() 
										, BaseUtility.toDateFormat( dataList.get(d).getPwdDateLst(), dtSep, 8 ) 
										, BaseUtility.toDateFormat( dataList.get(d).getCreDate(), dtSep, 8 ) 
						};
						colValue.add(array);
					}
				}

				model.put("relFile"     , fileName);
				model.put("absFile"     , dataName);
				model.put("titLine"     , "2");
				model.put("colName"     , colName);
				model.put("colRegion"   , colRegion);
				model.put("colAlign"    , colAlign);
				model.put("colWidth"    , colWidth);
				model.put("colValue"    , colValue);
			}
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return new ExcelBuilderPOI();
	}	// end of getExportOfMember

	/**
	 * <pre>
	 * 사용자 내역을 등록처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/MemberCreate.do")
	public ModelAndView doActOfCreateMember ( 
			@ModelAttribute("searchVO")BaseMemberVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

//		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
//		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
log.debug(" act: "+ BaseUtility.spaceFill( "MemberCreate", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, true ) ) return mav;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reAuthRole"    , searchVO.getReAuthRole());
		model.addAttribute("reAuthFlag"    , searchVO.getReAuthFlag());

//		// 추가 조회조건 취득
//		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
//		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

//log.debug("viewNo: "+ searchVO.getRowUUID() +" | "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/manage/membStaff_inst";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			// 입력내역 취득(사용자)
			searchVO.setUserPID(BaseUtility.null2Blank( searchVO.getUserPNO() ));
			searchVO.setBgnDate(BaseUtility.null2Char( searchVO.getBgnDate(), systemVO.getToDate() ).replaceAll("[^0-9]", ""));
			searchVO.setEndDate(BaseUtility.null2Char( searchVO.getEndDate(), "20991231" ).replaceAll("[^0-9]", ""));
			searchVO.setUserUUID(BaseCategory.encryptSHA256( searchVO.getUserPID() + searchVO.getBgnDate() ).toUpperCase());
//			searchVO.setUserPWD(BaseUtility.null2Char( searchVO.getUserPWC(), "", BaseCategory.encryptSHA256( searchVO.getUserPNO() + searchVO.getBgnDate() + searchVO.getUserPWC() ).toUpperCase() ));
			searchVO.setUserPWD(BaseCategory.encryptSHA256( searchVO.getUserPNO() + searchVO.getBgnDate() + BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsPass" ) ).toUpperCase());
			searchVO.setUserMail(BaseUtility.null2Char( searchVO.getSeleMailA(), "", BaseUtility.null2Blank( searchVO.getSeleMailA() ).replaceAll("@", "") +"@"+ BaseUtility.null2Blank( searchVO.getSeleMailT() ).replaceAll("@", "") ));
			searchVO.setAuthRole(BaseUtility.null2Char( searchVO.getAuthRole(), "GE" ));
			searchVO.setAuthFlag(BaseUtility.null2Char( searchVO.getAuthFlag(), "F" ));
			searchVO.setPwdDateUse("A".equals(memberVO.getAuthFlag())||"SU".equals(memberVO.getAuthFlag())?"F":"T");
			searchVO.setPwdDateTerm("20991231");
			searchVO.setPwdDateMod("A".equals(memberVO.getAuthFlag())||"SU".equals(memberVO.getAuthFlag())?"20991231":systemVO.getToDateMN3());
			searchVO.setPwdDateErr("0");
			searchVO.setCreIPv4(BaseUtility.reqIPv4Address( request ));
			searchVO.setModIPv4(BaseUtility.reqIPv4Address( request ));
			searchVO.setCreUser(BaseUtility.null2Char( searchVO.getCreUser(), memberVO.getUserPID() ));
			searchVO.setCreName(BaseUtility.null2Char( searchVO.getCreName(), memberVO.getUserName() ));
			searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
			searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));
			searchVO.setViewNo(searchVO.getUserUUID() +"1");
log.debug("viewNo: "+ searchVO.getUserPNO() +" | "+ searchVO.getBgnDate());

			if( BaseUtility.checkParam( "mail", searchVO.getUserMail() )) { 
//				model.addAttribute("error"         , "이메일 입력 중 문제가 발생하여 더 이상 진행되지 않습니다.");
//				model.addAttribute("message"       , "사용자메일 입력 오류");
//				return "/cmmn/exe_script";
				mav.addObject("isError"       , "T");
				mav.addObject("message"       , "사용자메일 입력 오류");
				return mav;
			}

			// 사용자 내역 갱신
			userMngrService.doRtnOfCreateStaff( searchVO );
			model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReAuthRole((String)model.get("reAuthRole"));
			searchVO.setReAuthFlag((String)model.get("reAuthflag"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reAuthRole"    , (String)model.get("reAuthRole"));
			mav.addObject("reAuthFlag"    , (String)model.get("reAuthFlag"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//-		return "redirect:/ptnhexa/User/MemberView.do";
//		return "redirect:/ptnhexa/User/MemberList.do";
		return mav;
	}	// end of doActOfCreateMember

	/**
	 * <pre>
	 * 사용자 내역을 수정처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/MemberUpdate.do")
	public ModelAndView doActOfUpdateMember ( 
			@ModelAttribute("searchVO")BaseMemberVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
log.debug(" act: "+ BaseUtility.spaceFill( "MemberUpdate", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, true, searchVO.getViewNo() ) ) return mav;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reAuthRole"    , searchVO.getReAuthRole());
		model.addAttribute("reAuthFlag"    , searchVO.getReAuthFlag());

		// 추가 조회조건 취득
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("viewNo: "+ searchVO.getRowUUID() +" | "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/manage/membStaff_edit";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				// 입력내역 취득(사용자)
				searchVO.setUserPWD(BaseUtility.null2Char( searchVO.getUserPWC(), "", BaseCategory.encryptSHA256( searchVO.getUserPNO() + searchVO.getBgnDate() + searchVO.getUserPWC() ).toUpperCase() ));
				searchVO.setUserMail(BaseUtility.null2Char( searchVO.getSeleMailA(), "", BaseUtility.null2Blank( searchVO.getSeleMailA() ).replaceAll("@", "") +"@"+ BaseUtility.null2Blank( searchVO.getSeleMailT() ).replaceAll("@", "") ));
				searchVO.setAuthRole(BaseUtility.null2Char( searchVO.getAuthRole(), "GE" ));
				searchVO.setAuthFlag(BaseUtility.null2Char( searchVO.getAuthFlag(), "F" ));
				searchVO.setModIPv4(BaseUtility.reqIPv4Address( request ));
				searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
				searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));

				if( BaseUtility.checkParam( "mail", searchVO.getUserMail() )) { 
//					model.addAttribute("error"         , "이메일 입력 중 문제가 발생하여 더 이상 진행되지 않습니다.");
//					model.addAttribute("message"       , "사용자메일 입력 오류");
//					return "/cmmn/exe_script";
					mav.addObject("isError"       , "T");
					mav.addObject("message"       , "사용자메일 입력 오류");
					return mav;
				}

				// 사용자 내역 갱신
				userMngrService.doRtnOfUpdateStaff( searchVO );
				searchVO.setViewNo(searchVO.getRowUUID() +"1");
				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReAuthRole((String)model.get("reAuthRole"));
			searchVO.setReAuthFlag((String)model.get("reAuthflag"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reAuthRole"    , (String)model.get("reAuthRole"));
			mav.addObject("reAuthFlag"    , (String)model.get("reAuthFlag"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//-		return "redirect:/ptnhexa/User/MemberView.do";
//		return "redirect:/ptnhexa/User/MemberList.do";
		return mav;
	}	// end of doActOfUpdateMember

	/**
	 * <pre>
	 * 사용자 내역을 삭제처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/MemberDelete.do")
	public ModelAndView doActOfDeleteMember ( 
			@ModelAttribute("searchVO")BaseMemberVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
log.debug(" act: "+ BaseUtility.spaceFill( "MemberDelete", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, true, searchVO.getViewNo() ) ) return mav;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reAuthRole"    , searchVO.getReAuthRole());
		model.addAttribute("reAuthFlag"    , searchVO.getReAuthFlag());

		// 추가 조회조건 취득
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("viewNo: "+ searchVO.getRowUUID() +" | "+ searchVO.getRowSeq());
		try { 
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				// 입력내역 취득(사용자)
				searchVO.setAuthFlag("F");
				searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
				searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));
				searchVO.setDelUser(BaseUtility.null2Char( searchVO.getDelUser(), memberVO.getUserPID() ));
				searchVO.setDelName(BaseUtility.null2Char( searchVO.getDelName(), memberVO.getUserName() ));

				// 사용자 내역 삭제
				userMngrService.doRtnOfDeleteStaff( searchVO );
				searchVO.setViewNo(searchVO.getRowUUID() +"1");
				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReAuthRole((String)model.get("reAuthRole"));
			searchVO.setReAuthFlag((String)model.get("reAuthflag"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reAuthRole"    , (String)model.get("reAuthRole"));
			mav.addObject("reAuthFlag"    , (String)model.get("reAuthFlag"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/ptnhexa/User/MemberList.do";
		return mav;
	}	// end of doActOfDeleteMember

	/**
	 * <pre>
	 * 사용자 내역을 변경처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/MemberModify.do")
	public ModelAndView doActOfModifyMember ( 
			@ModelAttribute("searchVO")BaseMemberVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
log.debug(" act: "+ BaseUtility.spaceFill( "MemberModify", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, true, searchVO.getViewNo() ) ) return mav;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reAuthRole"    , searchVO.getReAuthRole());
		model.addAttribute("reAuthFlag"    , searchVO.getReAuthFlag());

		// 추가 조회조건 취득
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("viewNo: "+ searchVO.getRowUUID() +" | "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/manage/membStaff_edit";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				// 입력내역 취득(사용자)
				searchVO.setAuthFlag(BaseUtility.null2Char( searchVO.getAuthFlag(), "F" ));
				searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
				searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));

				// 사용자 내역 갱신
				userMngrService.doRtnOfModifyStaff( searchVO );
				searchVO.setViewNo(searchVO.getRowUUID() +"1");
				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReAuthRole((String)model.get("reAuthRole"));
			searchVO.setReAuthFlag((String)model.get("reAuthflag"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reAuthRole"    , (String)model.get("reAuthRole"));
			mav.addObject("reAuthFlag"    , (String)model.get("reAuthFlag"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/ptnhexa/User/MemberList.do";
		return mav;
	}	// end of doActOfModifyMember

	/**
	 * <pre>
	 * 사용자 비밀번호 내역을 초기화처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/MemberInitial.do")
	public ModelAndView doActOfInitialMember ( 
			@ModelAttribute("searchVO")BaseMemberVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseMemberVO			dataView     = new BaseMemberVO();
log.debug(" act: "+ BaseUtility.spaceFill( "MemberInitial", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, true, searchVO.getViewNo() ) ) return mav;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reAuthRole"    , searchVO.getReAuthRole());
		model.addAttribute("reAuthFlag"    , searchVO.getReAuthFlag());

		// 추가 조회조건 취득
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("viewNo: "+ searchVO.getRowUUID() +" | "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/manage/membStaff_view";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				dataView = (BaseMemberVO)userMngrService.getDetailOfStaff( searchVO );

				// 입력내역 취득(사용자)
				searchVO.setUserPWD(BaseCategory.encryptSHA256( dataView.getUserPID() + dataView.getBgnDate() + BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsPass" ) ).toUpperCase());
				searchVO.setPwdDateUse("A".equals(dataView.getAuthFlag())||"SU".equals(dataView.getAuthFlag())?"F":"T");
				searchVO.setPwdDateTerm("20991231");
				searchVO.setPwdDateMod("A".equals(dataView.getAuthFlag())||"SU".equals(dataView.getAuthFlag())?"20991231":systemVO.getToDateMN3());
				searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
				searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));

				// 사용자 비번 내역 갱신
				userMngrService.doRtnOfModifyStaffPass( searchVO );
				searchVO.setViewNo(searchVO.getRowUUID() +"1");
				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReAuthRole((String)model.get("reAuthRole"));
			searchVO.setReAuthFlag((String)model.get("reAuthflag"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reAuthRole"    , (String)model.get("reAuthRole"));
			mav.addObject("reAuthFlag"    , (String)model.get("reAuthFlag"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/ptnhexa/User/MemberList.do";
		return mav;
	}	// end of doActOfInitialMember

	/**
	 * <pre>
	 * 사용자 비밀번호 내역을 변경처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/MemberChange.do")
	public ModelAndView doActOfChangeMember ( 
			@ModelAttribute("searchVO")BaseMemberVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		PrivateKey				privateKey   = (PrivateKey)session.getAttribute(this.RSA_WEB_KEY);
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseMemberVO			dataView     = new BaseMemberVO();
		String[]				resultPWD    = new String[4];
log.debug(" act: "+ BaseUtility.spaceFill( "MemberChange", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "로그인 정보가 존재하지 않습니다.\n\n로그인 화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
			return mav;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return mav;
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reAuthRole"    , searchVO.getReAuthRole());
		model.addAttribute("reAuthFlag"    , searchVO.getReAuthFlag());

		// 추가 조회조건 취득
		uuidNo = memberVO.getUserUUID() +"1";
		searchVO.setViewNo(uuidNo);
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("viewNo: "+ searchVO.getRowUUID() +" | "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/manage/membStaff_pass";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

//log.debug("p_pass: "+ searchVO.getP_userpass());	// 변환 기존
//log.debug("c_pass: "+ searchVO.getC_userpass());	// 변환 신규
//log.debug("j_pass: "+ searchVO.getJ_userpass());	// 변환 최종
//log.debug("o_pass: "+ searchVO.getO_userpass());	// 입력 기존
//log.debug("n_pass: "+ searchVO.getN_userpass());	// 입력 신규
//log.debug("k_pass: "+ searchVO.getK_userpass());	// 입력 최종
			if( !BaseUtility.isValidNull( searchVO.getJ_userpass() ) ) { 
				resultPWD = new String[4];
				resultPWD[0] = BaseCategory.decryptRSA( privateKey, searchVO.getJ_userpass() );
				resultPWD[1] = BaseCategory.decryptRSA( privateKey, searchVO.getP_userpass() );
				resultPWD[2] = BaseCategory.decryptRSA( privateKey, searchVO.getC_userpass() );
				session.removeAttribute(this.RSA_WEB_KEY);	// 재사용 금지
//log.debug("o_pass: "+ resultPWD[1]);
//log.debug("n_pass: "+ resultPWD[2]);
//log.debug("k_pass: "+ resultPWD[0]);

				if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
					dataView = (BaseMemberVO)userMngrService.getDetailOfStaff( searchVO );

					// 기존 비밀번호 확인
					if( !dataView.getUserPWD().equals(BaseCategory.encryptSHA256( dataView.getUserPID() + dataView.getBgnDate() + resultPWD[1] ).toUpperCase()) ) { 
						mav.addObject("isError"       , "T");
						mav.addObject("message"       , "기존 비밀번호를 다르게 입력되었습니다.\n\n다시 입력하시기 바랍니다..");
						return mav;
					}

					// 입력내역 취득(사용자)
					searchVO.setUserPWD(BaseCategory.encryptSHA256( dataView.getUserPID() + dataView.getBgnDate() + resultPWD[0] ).toUpperCase());
					searchVO.setPwdDateUse("A".equals(dataView.getAuthFlag())||"SU".equals(dataView.getAuthFlag())?"F":"T");
					searchVO.setPwdDateTerm("20991231");
					searchVO.setPwdDateMod("A".equals(dataView.getAuthFlag())||"SU".equals(dataView.getAuthFlag())?"20991231":systemVO.getToDate());
					searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
					searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));

					// 사용자 비번 내역 갱신
					userMngrService.doRtnOfModifyStaffPass( searchVO );
					searchVO.setViewNo(searchVO.getRowUUID() +"1");
					model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());

					memberVO.setUserPWD(searchVO.getUserPWD());
					memberVO.setPwdDateUse(searchVO.getPwdDateUse());
					memberVO.setPwdDateTerm(searchVO.getPwdDateTerm());
					memberVO.setPwdDateMod(searchVO.getPwdDateMod());
					request.getSession().setAttribute("MemRInfo"      , memberVO);
				}
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReAuthRole((String)model.get("reAuthRole"));
			searchVO.setReAuthFlag((String)model.get("reAuthflag"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reAuthRole"    , (String)model.get("reAuthRole"));
			mav.addObject("reAuthFlag"    , (String)model.get("reAuthFlag"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/ptnhexa/User/MemberList.do";
		return mav;
	}	// end of doActOfChangeMember

}	// end of class