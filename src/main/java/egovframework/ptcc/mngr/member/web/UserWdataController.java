/*******************************************************************************
  Program ID  : UserMngrController
  Description : 사용자 관리에 관한 controller 클래스를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.member.web;

import egovframework.com.cmm.util.BaseUtility;
import egovframework.ptcc.cmmn.excel.ExcelBuilderPOI;
import egovframework.ptcc.cmmn.service.BaseCmmnService;
import egovframework.ptcc.cmmn.util.BaseCategory;
import egovframework.ptcc.cmmn.util.UserDetailsHelper;
import egovframework.ptcc.cmmn.vo.BaseCommonVO;
import egovframework.ptcc.cmmn.vo.BaseFilesVO;
import egovframework.ptcc.cmmn.vo.BaseMemberVO;
import egovframework.ptcc.mngr.member.service.UserMngrService;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.*;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springmodules.validation.commons.DefaultBeanValidator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.security.*;
import java.security.spec.RSAPublicKeySpec;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value="/webdata/user")
public class UserWdataController {

	@Autowired
	private WebApplicationContext			webContext;
	@Autowired
	private DataSourceTransactionManager	txManager;

	@Resource(name="beanValidator")
	protected DefaultBeanValidator			beanValidator;
	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="userMngrService")
	protected UserMngrService				userMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));
	protected String						defsPass     = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsPass" );
	protected boolean						notiItems    = false;
	protected boolean						moveItems    = false;
	protected boolean						fileUP       = false;
	protected boolean						tempUP       = false;

	private String							RSA_WEB_KEY  = "__rsaPrivateKey__";	// 개인키 session key
	private String							RSA_INSTANCE = "RSA";				// rsa transformation

	private void initRSA ( 
			HttpServletRequest request 
		) throws Exception { 

		HttpSession				session      = request == null ? null : request.getSession();
		KeyPairGenerator		generator;

		try { 
			generator = KeyPairGenerator.getInstance(this.RSA_INSTANCE);
			generator.initialize(1024);

			KeyPair keyPair = generator.genKeyPair();
			KeyFactory keyFactory = KeyFactory.getInstance(this.RSA_INSTANCE);

			PublicKey publicKey = keyPair.getPublic();
			PrivateKey privateKey = keyPair.getPrivate();

			// 세션에 공개키의 문자열을 키로하여 개인키를 저장한다.
			session.setAttribute(this.RSA_WEB_KEY, privateKey);

			// 공개키를 문자열로 변환하여 JavaScript RSA 라이브러리 넘겨준다.
			RSAPublicKeySpec publicSpec = (RSAPublicKeySpec) keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);
			String publicKeyModulus = publicSpec.getModulus().toString(16);
			String publicKeyExponent = publicSpec.getPublicExponent().toString(16);
			request.setAttribute("RSAModulus", publicKeyModulus); // rsa modulus 를 request 에 추가
			request.setAttribute("RSAExponent", publicKeyExponent); // rsa exponent 를 request 에 추가
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		}
	}

	/**
	 * <pre>
	 * 사용자 비번 변경화면으로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/MemberPass.do")
	public String scrPassOfMember ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		authList     = new ArrayList<BaseCommonVO>();
log.debug(" act: "+ BaseUtility.spaceFill( "MemberPass", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			model.addAttribute("message"       , "로그인 정보가 존재하지 않습니다.\\n\\n로그인 화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/webdata/index.do");
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reAuthRole"    , searchVO.getReAuthRole());
		model.addAttribute("reAuthFlag"    , searchVO.getReAuthFlag());

		// 분류구분 취득 - 권한 구분
//		authList = (List<BaseCommonVO>)baseCmmnService.getListOfAuth( searchVO );
		String userComm = memberVO.getUserComm();
		String baseServ;
		if(userComm == null) {
			baseServ = "/base/Notice";
		} else {
			String userCommTemp = userComm.substring(0,2);
			if ("00".equals(userCommTemp)) {
				baseServ = "/chair/Notice";
			} else if ("01".equals(userCommTemp)) {
				baseServ = "/oper/Notice";
			} else if ("02".equals(userCommTemp)) {
				baseServ = "/gvadm/Notice";
			} else if ("03".equals(userCommTemp)) {
				baseServ = "/welfare/Notice";
			} else if ("04".equals(userCommTemp)) {
				baseServ = "/build/Notice";
			} else if ("06".equals(userCommTemp)) {
				baseServ = "/base/Notice";
			} else if ("07".equals(userCommTemp)) {
				baseServ = "/proc/Notice";
			} else if ("08".equals(userCommTemp)) {
				baseServ = "/prom/Notice";
			} else if ("09".equals(userCommTemp)) {
				baseServ = "/supp/Notice";
			} else {
				baseServ = "/base/Notice";
			}
		}

		model.addAttribute("cateList"      , cateList);
		model.addAttribute("authList"      , authList);
		model.addAttribute("nextMode"      , webBase+"/webdata"+baseServ);

		this.initRSA(request);

		return "/wdata/manage/membStaff_pass";
	}	// end of scrPassOfMember

	/**
	 * <pre>
	 * 사용자 비밀번호 내역을 변경처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/MemberChange.do")
	public ModelAndView doActOfChangeMember ( 
			@ModelAttribute("searchVO")BaseMemberVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		PrivateKey				privateKey   = (PrivateKey)session.getAttribute(this.RSA_WEB_KEY);
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseMemberVO			dataView     = new BaseMemberVO();
		String[]				resultPWD    = new String[4];
log.debug(" act: "+ BaseUtility.spaceFill( "MemberChange", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "로그인 정보가 존재하지 않습니다.\n\n로그인 화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/webdata/index.do");
			return mav;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return mav;
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reAuthRole"    , searchVO.getReAuthRole());
		model.addAttribute("reAuthFlag"    , searchVO.getReAuthFlag());

		// 추가 조회조건 취득
		uuidNo = memberVO.getUserUUID() +"1";
		searchVO.setViewNo(uuidNo);
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("viewNo: "+ searchVO.getRowUUID() +" | "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/manage/membStaff_pass";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

//log.debug("p_pass: "+ searchVO.getP_userpass());	// 변환 기존
//log.debug("c_pass: "+ searchVO.getC_userpass());	// 변환 신규
//log.debug("j_pass: "+ searchVO.getJ_userpass());	// 변환 최종
//log.debug("o_pass: "+ searchVO.getO_userpass());	// 입력 기존
//log.debug("n_pass: "+ searchVO.getN_userpass());	// 입력 신규
//log.debug("k_pass: "+ searchVO.getK_userpass());	// 입력 최종
			if( !BaseUtility.isValidNull( searchVO.getJ_userpass() ) ) { 
				resultPWD = new String[4];
				resultPWD[0] = BaseCategory.decryptRSA( privateKey, searchVO.getJ_userpass() );
				resultPWD[1] = BaseCategory.decryptRSA( privateKey, searchVO.getP_userpass() );
				resultPWD[2] = BaseCategory.decryptRSA( privateKey, searchVO.getC_userpass() );
				session.removeAttribute(this.RSA_WEB_KEY);	// 재사용 금지
//log.debug("o_pass: "+ resultPWD[1]);
//log.debug("n_pass: "+ resultPWD[2]);
//log.debug("k_pass: "+ resultPWD[0]);

				if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
					dataView = (BaseMemberVO)userMngrService.getDetailOfStaff( searchVO );

					// 기존 비밀번호 확인
					if( !dataView.getUserPWD().equals(BaseCategory.encryptSHA256( dataView.getUserPID() + dataView.getBgnDate() + resultPWD[1] ).toUpperCase()) ) { 
						mav.addObject("isError"       , "T");
						mav.addObject("message"       , "기존 비밀번호를 다르게 입력되었습니다.\n\n다시 입력하시기 바랍니다..");
						return mav;
					}

					// 입력내역 취득(사용자)
					searchVO.setUserPWD(BaseCategory.encryptSHA256( dataView.getUserPID() + dataView.getBgnDate() + resultPWD[0] ).toUpperCase());
					searchVO.setPwdDateUse("A".equals(dataView.getAuthFlag())||"SU".equals(dataView.getAuthFlag())?"F":"T");
					searchVO.setPwdDateTerm("20991231");
					searchVO.setPwdDateMod("A".equals(dataView.getAuthFlag())||"SU".equals(dataView.getAuthFlag())?"20991231":systemVO.getToDate());
					searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
					searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));

					// 사용자 비번 내역 갱신
					userMngrService.doRtnOfModifyStaffPass( searchVO );
					searchVO.setViewNo(searchVO.getRowUUID() +"1");
					model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());

					memberVO.setUserPWD(searchVO.getUserPWD());
					memberVO.setPwdDateUse(searchVO.getPwdDateUse());
					memberVO.setPwdDateTerm(searchVO.getPwdDateTerm());
					memberVO.setPwdDateMod(searchVO.getPwdDateMod());
					request.getSession().setAttribute("MemRInfo"      , memberVO);
				}
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReAuthRole((String)model.get("reAuthRole"));
			searchVO.setReAuthFlag((String)model.get("reAuthflag"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reAuthRole"    , (String)model.get("reAuthRole"));
			mav.addObject("reAuthFlag"    , (String)model.get("reAuthFlag"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/Manage/User/MemberList.do";
		return mav;
	}	// end of doActOfChangeMember

}	// end of class