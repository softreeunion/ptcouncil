/*******************************************************************************
  Program ID  : UserMngrDAO
  Description : 사용자 정보에 대한 Data Access Object Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.member.service.impl;

import java.util.*;

import org.springframework.stereotype.Repository;

import egovframework.ptcc.cmmn.vo.*;

@Repository("userMngrDAO")
public class UserMngrDAO extends egovframework.rte.psl.dataaccess.EgovAbstractDAO { 

	/**
	 * <pre>
	 * 사용자 중복건수을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return Integer
	 * @throws Exception
	 */
	public int getCheckOfStaff(String paramVal) throws Exception { 
		return (Integer)select("ptcouncil.new.member.ptSelectStaffCheck",paramVal);
	}	// end of getCheckOfStaff

	/**
	 * <pre>
	 * 사용자 UUID을(를) 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return String
	 * @throws Exception
	 */
	public String getInfoOfStaffUUID(String paramVal) throws Exception { 
		return (String)select("ptcouncil.new.member.ptSelectStaffUUID",paramVal);
	}	// end of getInfoOfStaffUUID

	/**
	 * <pre>
	 * 사용자 목록건수을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return Integer
	 * @throws Exception
	 */
	public int getCountOfStaff(BaseCommonVO paramObj) throws Exception { 
		return (Integer)select("ptcouncil.new.member.ptSelectStaffCount",paramObj);
	}	// end of getCountOfStaff

	/**
	 * <pre>
	 * 사용자 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseMemberVO> getListOfStaff(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseMemberVO>)list("ptcouncil.new.member.ptSelectStaffList",paramObj);
	}	// end of getListOfStaff

	/**
	 * <pre>
	 * 사용자 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseMemberVO> getListOfStaff(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseMemberVO>)listWithPaging("ptcouncil.new.member.ptSelectStaffList",paramObj,Integer.parseInt(pageIndex)-1,Integer.parseInt(pageSize));
	}	// end of getListOfStaff

	/**
	 * <pre>
	 * 사용자 상세내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseMemberVO
	 * @throws Exception
	 */
	public BaseMemberVO getDetailOfStaff(BaseCommonVO paramObj) throws Exception { 
		return (BaseMemberVO)select("ptcouncil.new.member.ptSelectStaffDetail",paramObj);
	}	// end of getDetailOfStaff

	/**
	 * <pre>
	 * 사용자 상세내역(UUID)을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return BaseMemberVO
	 * @throws Exception
	 */
	public BaseMemberVO getDetailOfStaff(String paramVal) throws Exception { 
		return (BaseMemberVO)select("ptcouncil.new.member.ptSelectStaffDetailUUID",paramVal);
	}	// end of getDetailOfStaff

	/**
	 * <pre>
	 * 사용자 일련번호을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseMemberVO						// 
	 * @return BaseMemberVO
	 * @throws Exception
	 */
	public 	BaseMemberVO getInfoOfStaffSeq(BaseMemberVO paramObj) throws Exception { 
		return (BaseMemberVO)select("ptcouncil.new.member.ptSelectStaffSeq",paramObj);
	}	// end of getInfoOfStaffSeq

	/**
	 * <pre>
	 * 사용자 내역을(를) 등록 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseMemberVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfCreateStaff(BaseMemberVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.member.ptInsertStaff",paramObj)<=0);
	}	// end of doActOfCreateStaff

	/**
	 * <pre>
	 * 사용자 내역을(를) 변경 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseMemberVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfUpdateStaff(BaseMemberVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.member.ptUpdateStaff",paramObj)<=0);
	}	// end of doActOfUpdateStaff

	/**
	 * <pre>
	 * 사용자 내역을(를) 삭제 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseMemberVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfDeleteStaff(BaseMemberVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.member.ptDeleteStaff",paramObj)<=0);
	}	// end of doActOfDeleteStaff

	/**
	 * <pre>
	 * 사용자 내역을(를) 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseMemberVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfModifyStaff(BaseMemberVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.member.ptModifyStaff",paramObj)<=0);
	}	// end of doActOfModifyStaff

	/**
	 * <pre>
	 * 사용자 비번 내역을(를) 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseMemberVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfModifyStaffPass(BaseMemberVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.member.ptModifyStaffPass",paramObj)<=0);
	}	// end of doActOfModifyStaffPass

}	// end of class