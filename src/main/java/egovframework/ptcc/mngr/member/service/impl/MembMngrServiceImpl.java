/*******************************************************************************
  Program ID  : MembMngrServiceImpl
  Description : 사용자 관리에 관한 Service Implement Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.member.service.impl;

import java.util.*;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.member.service.*;

@Service("membMngrService")
public class MembMngrServiceImpl extends egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl implements MembMngrService { 

	@Resource(name="membMngrDAO")
	private MembMngrDAO						membMngrDAO;

	@Override
	public String getInfoOfMembUUID(String paramVal) throws Exception { 
		return (String)membMngrDAO.getInfoOfMembUUID( paramVal );
	}
	@Override
	public int getCountOfMemb(BaseCommonVO paramObj) throws Exception { 
		return (int)membMngrDAO.getCountOfMemb( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getListOfMemb(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)membMngrDAO.getListOfMemb( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getListOfMemb(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseAgendaVO>)membMngrDAO.getListOfMemb( paramObj, pageIndex, pageSize );
	}
	@Override
	public 	BaseAgendaVO getDetailOfMemb(BaseCommonVO paramObj) throws Exception { 
		return (BaseAgendaVO)membMngrDAO.getDetailOfMemb( paramObj );
	}
	@Override
	public 	BaseAgendaVO getDetailOfMemb(String paramVal) throws Exception { 
		return (BaseAgendaVO)membMngrDAO.getDetailOfMemb( paramVal );
	}
	@Override
	public 	BaseAgendaVO getInfoOfMembSeq(BaseAgendaVO paramObj) throws Exception { 
		return (BaseAgendaVO)membMngrDAO.getInfoOfMembSeq( paramObj );
	}
	@Override
	public boolean doRtnOfCreateMemb(BaseAgendaVO paramObj) throws Exception { 
		return (boolean)membMngrDAO.doRtnOfCreateMemb( paramObj );
	}
	@Override
	public boolean doRtnOfUpdateMemb(BaseAgendaVO paramObj) throws Exception { 
		return (boolean)membMngrDAO.doRtnOfUpdateMemb( paramObj );
	}
	@Override
	public boolean doRtnOfDeleteMemb(BaseAgendaVO paramObj) throws Exception { 
		return (boolean)membMngrDAO.doRtnOfDeleteMemb( paramObj );
	}
	@Override
	public boolean doRtnOfModifyMemb(BaseAgendaVO paramObj) throws Exception { 
		return (boolean)membMngrDAO.doRtnOfModifyMemb( paramObj );
	}

}	// end of class