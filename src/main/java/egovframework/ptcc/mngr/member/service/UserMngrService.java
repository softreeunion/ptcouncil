/*******************************************************************************
  Program ID  : UserMngrService
  Description : 사용자 관리에 관한 Service Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.member.service;

import java.util.List;

import egovframework.ptcc.cmmn.vo.*;

public interface UserMngrService { 

	// 사용자 관리
	int getCheckOfStaff(String paramVal) throws Exception;
	String getInfoOfStaffUUID(String paramVal) throws Exception;
	int getCountOfStaff(BaseCommonVO paramObj) throws Exception;
	List<BaseMemberVO> getListOfStaff(BaseCommonVO paramObj) throws Exception;
	List<BaseMemberVO> getListOfStaff(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception;
	BaseMemberVO getDetailOfStaff(BaseCommonVO paramObj) throws Exception;
	BaseMemberVO getDetailOfStaff(String paramVal) throws Exception;
	BaseMemberVO getInfoOfStaffSeq(BaseMemberVO paramObj) throws Exception;
	boolean doRtnOfCreateStaff(BaseMemberVO paramObj) throws Exception;
	boolean doRtnOfUpdateStaff(BaseMemberVO paramObj) throws Exception;
	boolean doRtnOfDeleteStaff(BaseMemberVO paramObj) throws Exception;
	boolean doRtnOfModifyStaff(BaseMemberVO paramObj) throws Exception;

	boolean doRtnOfModifyStaffPass(BaseMemberVO paramObj) throws Exception;

}	// end of interface