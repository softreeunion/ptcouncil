/*******************************************************************************
  Program ID  : MembMngrDAO
  Description : 의원 정보에 대한 Data Access Object Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.member.service.impl;

import java.util.*;

import org.springframework.stereotype.Repository;

import egovframework.ptcc.cmmn.vo.*;

@Repository("membMngrDAO")
public class MembMngrDAO extends egovframework.rte.psl.dataaccess.EgovAbstractDAO { 

	/**
	 * <pre>
	 * 의원 UUID을(를) 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return String
	 * @throws Exception
	 */
	public String getInfoOfMembUUID(String paramVal) throws Exception { 
		return (String)select("ptcouncil.new.agenda.ptSelectMembUUID",paramVal);
	}	// end of getInfoOfMembUUID

	/**
	 * <pre>
	 * 의원 목록건수을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return Integer
	 * @throws Exception
	 */
	public int getCountOfMemb(BaseCommonVO paramObj) throws Exception { 
		return (Integer)select("ptcouncil.new.agenda.ptSelectMembCount",paramObj);
	}	// end of getCountOfMemb

	/**
	 * <pre>
	 * 의원 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getListOfMemb(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectMembList",paramObj);
	}	// end of getListOfMemb

	/**
	 * <pre>
	 * 의원 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getListOfMemb(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseAgendaVO>)listWithPaging("ptcouncil.new.agenda.ptSelectMembList",paramObj,Integer.parseInt(pageIndex)-1,Integer.parseInt(pageSize));
	}	// end of getListOfMemb

	/**
	 * <pre>
	 * 의원 상세내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseAgendaVO
	 * @throws Exception
	 */
	public BaseAgendaVO getDetailOfMemb(BaseCommonVO paramObj) throws Exception { 
		return (BaseAgendaVO)select("ptcouncil.new.agenda.ptSelectMembDetail",paramObj);
	}	// end of getDetailOfMemb

	/**
	 * <pre>
	 * 의원 상세내역(UUID)을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return BaseAgendaVO
	 * @throws Exception
	 */
	public BaseAgendaVO getDetailOfMemb(String paramVal) throws Exception { 
		return (BaseAgendaVO)select("ptcouncil.new.agenda.ptSelectMembDetailUUID",paramVal);
	}	// end of getDetailOfMemb

	/**
	 * <pre>
	 * 의원 일련번호을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAgendaVO						// 
	 * @return BaseAgendaVO
	 * @throws Exception
	 */
	public 	BaseAgendaVO getInfoOfMembSeq(BaseAgendaVO paramObj) throws Exception { 
		return (BaseAgendaVO)select("ptcouncil.new.agenda.ptSelectMembSeq",paramObj);
	}	// end of getInfoOfMembSeq

	/**
	 * <pre>
	 * 의원 내역을(를) 등록 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAgendaVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfCreateMemb(BaseAgendaVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.agenda.ptInsertMemb",paramObj)<=0);
	}	// end of doActOfCreateMemb

	/**
	 * <pre>
	 * 의원 내역을(를) 변경 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAgendaVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfUpdateMemb(BaseAgendaVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.agenda.ptUpdateMemb",paramObj)<=0);
	}	// end of doActOfUpdateMemb

	/**
	 * <pre>
	 * 의원 내역을(를) 삭제 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAgendaVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfDeleteMemb(BaseAgendaVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.agenda.ptDeleteMemb",paramObj)<=0);
	}	// end of doActOfDeleteMemb

	/**
	 * <pre>
	 * 의원 내역을(를) 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAgendaVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfModifyMemb(BaseAgendaVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.agenda.ptModifyMemb",paramObj)<=0);
	}	// end of doActOfModifyMemb

}	// end of class