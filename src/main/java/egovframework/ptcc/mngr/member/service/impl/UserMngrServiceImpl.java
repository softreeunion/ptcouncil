/*******************************************************************************
  Program ID  : UserMngrServiceImpl
  Description : 사용자 관리에 관한 Service Implement Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.member.service.impl;

import java.util.*;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.member.service.*;

@Service("userMngrService")
public class UserMngrServiceImpl extends egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl implements UserMngrService { 

	@Resource(name="userMngrDAO")
	private UserMngrDAO						userMngrDAO;

	@Override
	public int getCheckOfStaff(String paramVal) throws Exception { 
		return (int)userMngrDAO.getCheckOfStaff( paramVal );
	}
	@Override
	public String getInfoOfStaffUUID(String paramVal) throws Exception { 
		return (String)userMngrDAO.getInfoOfStaffUUID( paramVal );
	}
	@Override
	public int getCountOfStaff(BaseCommonVO paramObj) throws Exception { 
		return (int)userMngrDAO.getCountOfStaff( paramObj );
	}
	@Override
	public List<BaseMemberVO> getListOfStaff(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseMemberVO>)userMngrDAO.getListOfStaff( paramObj );
	}
	@Override
	public List<BaseMemberVO> getListOfStaff(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseMemberVO>)userMngrDAO.getListOfStaff( paramObj, pageIndex, pageSize );
	}
	@Override
	public 	BaseMemberVO getDetailOfStaff(BaseCommonVO paramObj) throws Exception { 
		return (BaseMemberVO)userMngrDAO.getDetailOfStaff( paramObj );
	}
	@Override
	public 	BaseMemberVO getDetailOfStaff(String paramVal) throws Exception { 
		return (BaseMemberVO)userMngrDAO.getDetailOfStaff( paramVal );
	}
	@Override
	public 	BaseMemberVO getInfoOfStaffSeq(BaseMemberVO paramObj) throws Exception { 
		return (BaseMemberVO)userMngrDAO.getInfoOfStaffSeq( paramObj );
	}
	@Override
	public boolean doRtnOfCreateStaff(BaseMemberVO paramObj) throws Exception { 
		return (boolean)userMngrDAO.doRtnOfCreateStaff( paramObj );
	}
	@Override
	public boolean doRtnOfUpdateStaff(BaseMemberVO paramObj) throws Exception { 
		return (boolean)userMngrDAO.doRtnOfUpdateStaff( paramObj );
	}
	@Override
	public boolean doRtnOfDeleteStaff(BaseMemberVO paramObj) throws Exception { 
		return (boolean)userMngrDAO.doRtnOfDeleteStaff( paramObj );
	}
	@Override
	public boolean doRtnOfModifyStaff(BaseMemberVO paramObj) throws Exception { 
		return (boolean)userMngrDAO.doRtnOfModifyStaff( paramObj );
	}
	@Override
	public boolean doRtnOfModifyStaffPass(BaseMemberVO paramObj) throws Exception { 
		return (boolean)userMngrDAO.doRtnOfModifyStaffPass( paramObj );
	}

}	// end of class