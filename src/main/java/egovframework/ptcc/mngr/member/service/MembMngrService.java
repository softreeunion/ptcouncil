/*******************************************************************************
  Program ID  : MembMngrService
  Description : 의원 관리에 관한 Service Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.member.service;

import java.util.List;

import egovframework.ptcc.cmmn.vo.*;

public interface MembMngrService { 

	// 의원 관리
	String getInfoOfMembUUID(String paramVal) throws Exception;
	int getCountOfMemb(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getListOfMemb(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getListOfMemb(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception;
	BaseAgendaVO getDetailOfMemb(BaseCommonVO paramObj) throws Exception;
	BaseAgendaVO getDetailOfMemb(String paramVal) throws Exception;
	BaseAgendaVO getInfoOfMembSeq(BaseAgendaVO paramObj) throws Exception;
	boolean doRtnOfCreateMemb(BaseAgendaVO paramObj) throws Exception;
	boolean doRtnOfUpdateMemb(BaseAgendaVO paramObj) throws Exception;
	boolean doRtnOfDeleteMemb(BaseAgendaVO paramObj) throws Exception;
	boolean doRtnOfModifyMemb(BaseAgendaVO paramObj) throws Exception;

}	// end of interface