/*******************************************************************************
  Program ID  : LoginMngrDAO
  Description : 로그인 처리에 관한 Data Access Object 클래스를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.login.service.impl;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.util.BaseUtility;
import egovframework.ptcc.cmmn.vo.*;

@Repository("loginMngrDAO")
public class LoginMngrDAO extends egovframework.rte.psl.dataaccess.EgovAbstractDAO { 

	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");


	/**
	 * <pre>
	 * 관리자 아이디을(를) 조회해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return BaseMemberVO
	 * @throws Exception
	 */
	public BaseMemberVO getInfoOfUserMngr(String paramVal) throws Exception { 
		BaseCommonVO			commonVO     = new BaseCommonVO();

		commonVO.setFirDate(firDate.replaceAll("[^0-9]", ""));
		commonVO.setUseSync("ADM");
		commonVO.setUserPID(paramVal);

		return (BaseMemberVO)select("ptcouncil.new.system.ptSelectLogin",commonVO);
	}	// end of getInfoOfUserMngr

	/**
	 * <pre>
	 * 사용자 아이디을(를) 조회해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return BaseMemberVO
	 * @throws Exception
	 */
	public BaseMemberVO getInfoOfUserGngr(String paramVal) throws Exception { 
		BaseCommonVO			commonVO     = new BaseCommonVO();

		commonVO.setFirDate(firDate.replaceAll("[^0-9]", ""));
		commonVO.setUseSync("USR");
		commonVO.setUserPID(paramVal);

		return (BaseMemberVO)select("ptcouncil.new.system.ptSelectLogin",commonVO);
	}	// end of getInfoOfUserGngr

	/**
	 * <pre>
	 * 최종접속일시를 갱신해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfUpdateMember(String paramVal) throws Exception { 
		return !(update("ptcouncil.new.system.ptUpdateLogin",paramVal)<=0);
	}	// end of selectMngrLogin

}	// end of class