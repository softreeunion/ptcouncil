/*******************************************************************************
  Program ID  : LoginMngService
  Description : 로그인 처리에 관한 Service Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.login.service;

import egovframework.ptcc.cmmn.vo.*;

public interface LoginMngrService { 

	// 로그인 관리
	BaseMemberVO getInfoOfUserMngr(String paramVal) throws Exception;
	BaseMemberVO getInfoOfUserGngr(String paramVal) throws Exception;
	boolean doRtnOfUpdateMember(String paramVal) throws Exception;

}	// end of interface