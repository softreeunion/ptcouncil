/*******************************************************************************
  Program ID  : LoginMngrServiceImpl
  Description : 로그인 처리에 관한 Service Implement 클래스를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.login.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.login.service.*;

@Service("loginMngrService")
public class LoginMngrServiceImpl extends egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl implements LoginMngrService { 

	@Resource(name="loginMngrDAO")
	private LoginMngrDAO 					loginMngrDAO;

	@Override
	public BaseMemberVO getInfoOfUserMngr(String paramVal) throws Exception { 
		return (BaseMemberVO)loginMngrDAO.getInfoOfUserMngr( paramVal );
	}
	@Override
	public BaseMemberVO getInfoOfUserGngr(String paramVal) throws Exception { 
		return (BaseMemberVO)loginMngrDAO.getInfoOfUserGngr( paramVal );
	}
	@Override
	public boolean doRtnOfUpdateMember(String paramVal) throws Exception { 
		return (boolean)loginMngrDAO.doRtnOfUpdateMember( paramVal );
	}

}	// end of class