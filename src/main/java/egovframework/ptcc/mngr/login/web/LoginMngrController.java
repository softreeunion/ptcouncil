/*******************************************************************************
  Program ID  : LoginMngrController
  Description : 로그인 처리에 관한 controller 클래스를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.login.web;

import java.security.*;
import java.security.spec.*;

import javax.annotation.*;
import javax.servlet.http.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.*;
import org.springmodules.validation.commons.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.service.interceptor.*;
import egovframework.ptcc.cmmn.util.*;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.login.service.*;

@Controller
@RequestMapping(value="/ptnhexa")
public class LoginMngrController { 

	@Autowired
	private WebApplicationContext			webContext;

	@Resource(name="beanValidator")
	protected DefaultBeanValidator			beanValidator;
	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="loginMngrService")
	protected LoginMngrService				loginMngrService; 

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));
	protected String						basePWD      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.passWord" );
	protected String[]						baseNext     = {"/ptnhexa/index.do","/ptnhexa/User/MemberList.do","/ptnhexa/Base/NoticeList.do","/ptnhexa/User/MemberPass.do"};

	private String							baseServ;
	private String							RSA_WEB_KEY  = "__rsaPrivateKey__";	// 개인키 session key
	private String							RSA_INSTANCE = "RSA";				// rsa transformation

	private void initRSA ( 
			HttpServletRequest request 
		) throws Exception { 

		HttpSession				session      = request == null ? null : request.getSession();
		KeyPairGenerator		generator;

		try { 
			generator = KeyPairGenerator.getInstance(this.RSA_INSTANCE);
			generator.initialize(1024);

			KeyPair keyPair = generator.genKeyPair();
			KeyFactory keyFactory = KeyFactory.getInstance(this.RSA_INSTANCE);

			PublicKey publicKey = keyPair.getPublic();
			PrivateKey privateKey = keyPair.getPrivate();

			// 세션에 공개키의 문자열을 키로하여 개인키를 저장한다.
			session.setAttribute(this.RSA_WEB_KEY, privateKey);

			// 공개키를 문자열로 변환하여 JavaScript RSA 라이브러리 넘겨준다.
			RSAPublicKeySpec publicSpec = (RSAPublicKeySpec) keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);
			String publicKeyModulus = publicSpec.getModulus().toString(16);
			String publicKeyExponent = publicSpec.getPublicExponent().toString(16);
			request.setAttribute("RSAModulus", publicKeyModulus); // rsa modulus 를 request 에 추가
			request.setAttribute("RSAExponent", publicKeyExponent); // rsa exponent 를 request 에 추가
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		}
	}


	/**
	 * <pre>
	 * 로그인 화면을(를) 조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/login.do")
	public String scrLoginOfManage ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		if( memberVO == null ) { 
			this.initRSA(request);
			baseServ = "/mngr/index";
		} else { 
			if( BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
				baseServ = "redirect:"+ baseNext[1];
			} else { 
				baseServ = "redirect:"+ baseNext[2];
			}
		}

		return baseServ;
	}	// end of scrLoginOfManage


	/**
	 * <pre>
	 * 로그인 처리를 해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/huin_proc.do")
	public String doLoginOfManage ( 
			@ModelAttribute("searchVO")BaseMemberVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		PrivateKey				privateKey   = (PrivateKey)session.getAttribute(this.RSA_WEB_KEY);
		BaseMemberVO			resultVO     = new BaseMemberVO();
		String					resultPID    = "";
		String					resultPWD    = "";
		String					returnMSG    = "";

		try { 
			if( !(BaseUtility.isValidNull( searchVO.getJ_username() ) || BaseUtility.isValidNull( searchVO.getJ_userpass() )) ) { 
				resultPID = BaseCategory.decryptRSA( privateKey, searchVO.getJ_username() );
				resultPWD = BaseCategory.decryptRSA( privateKey, searchVO.getJ_userpass() );
				session.removeAttribute(this.RSA_WEB_KEY);	// 재사용 금지
				resultVO = loginMngrService.getInfoOfUserMngr( resultPID );

				if( resultVO != null ) { 
//log.debug("saved: "+ resultPWD);
//log.debug("input: "+ BaseCategory.encryptSHA256( resultPID + resultVO.getBgnDate() + resultPWD ));
					if( "T".equals(resultVO.getAuthFlag()) ) { 
						if( resultVO.getUserPID().equals(resultPID) && resultVO.getUserPWD().toLowerCase().equals(BaseCategory.encryptSHA256( resultPID + resultVO.getBgnDate() + resultPWD )) ) { 
							if( BaseCategory.getCheckOfAdmin( resultVO.getAuthRole() ) ) { 
								baseServ = "redirect:"+ baseNext[1];
							} else { 
								baseServ = "redirect:"+ baseNext[2];
							}
							if( "T".equals(resultVO.getPwdDateUse()) && "T".equals(propertiesService.getString("dateTerm"))  && !resultPWD.equals(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsPass" )) )  
								if( "".equals(resultVO.getPwdDateMod()) || systemVO.getToDateMN3().equals(resultVO.getPwdDateMod()) ) { 
									model.addAttribute("message"       , "3개월 이상 접속을 하지 않아 사용이 중지되었습니다.\\n\\n관리자에 문의바랍니다.");
									model.addAttribute("returnUrl"     , webBase + baseNext[0]);
									return "/cmmn/exe_script";
								}

							// 최종접속 일시 갱신
							loginMngrService.doRtnOfUpdateMember( resultPID );
							request.getSession().setAttribute("MemRInfo"      , resultVO);

							// 중복로그인 방지
							AuthHttpSessionBindingListener listener = new AuthHttpSessionBindingListener();
							request.getSession().setAttribute(resultPID, listener);

							if( resultPWD.equals(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsPass" )) ) { 
								request.getSession().setAttribute("viewNo"        , resultVO.getUserUUID());
								model.addAttribute("message"       , "임시 비밀번호를 사용하고 있습니다.\\n\\n비밀번호 변경화면으로 이동합니다.");
								model.addAttribute("returnUrl"     , webBase + baseNext[3]);
								return "/cmmn/exe_script";
							}
							return baseServ;
						} else { //패스워드 불일치
							returnMSG = "아이디 및 패스워드 정보가 일치하지 않습니다.";
						}
					} else { 
						returnMSG = "사용승인이 되지 않았습니다.\\n\\n관리자에게 문의바랍니다.";
					}
				} else { 
					// 최초 관리자 로그인 정보
//log.debug("saved: "+ basePWD);
//log.debug("input: "+ BaseCategory.encryptSHA256( resultPID + firDate.replaceAll("[^0-9]", "") + resultPWD ));
					if( "madmin".equals(resultPID) && basePWD.toLowerCase().equals(BaseCategory.encryptSHA256( resultPID + firDate.replaceAll("[^0-9]", "") + resultPWD )) ) { 
						resultVO = new BaseMemberVO();
						resultVO.setUserPID(resultPID);
						resultVO.setBgnDate(BaseUtility.toDateFormat( firDate, dtSep ));
						resultVO.setEndDate(BaseUtility.toDateFormat( "20991231", dtSep ));
						resultVO.setUserName("임시관리");
						resultVO.setAuthRole("SU");
						resultVO.setPwdDateLst(resultVO.getEndDate());
						resultVO.setAuthRoleName("임시관리자");
						resultVO.setUserCate(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsCate" ));
						request.getSession().setAttribute("MemRInfo"      , resultVO);

						// 중복로그인 방지
						AuthHttpSessionBindingListener listener = new AuthHttpSessionBindingListener();
						request.getSession().setAttribute(resultPID, listener);

						return "redirect:"+ baseNext[1];
					} else { 
						returnMSG = "아이디 및 패스워드 정보가 일치하지 않습니다..";
					}
				}
			} else { 
				returnMSG = "아이디. 및 패스워드 정보가 일치하지 않습니다..";
			}
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			returnMSG = "아이디 및 패스워드. 정보가 일치하지 않습니다..";
		}

		model.addAttribute("message"       , returnMSG);
		model.addAttribute("returnUrl"     , webBase + baseNext[0]);

		return "/cmmn/exe_script";
	}	// end of doLoginOfManage

}	// end of class