/*******************************************************************************
  Program ID  : AgendaMngrDAO
  Description : 회의록 정보에 대한 Data Access Object Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.agenda.service.impl;

import java.util.*;

import org.springframework.stereotype.Repository;

import egovframework.ptcc.cmmn.vo.*;

@Repository("agendaMngrDAO")
public class AgendaMngrDAO extends egovframework.rte.psl.dataaccess.EgovAbstractDAO { 

	/**
	 * <pre>
	 * 통합의안 UUID을(를) 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return String
	 * @throws Exception
	 */
	public String getInfoOfAgendaUUID(String paramVal) throws Exception { 
		return (String)select("ptcouncil.new.agenda.ptSelectAgendaUUID",paramVal);
	}	// end of getInfoOfAgendaUUID

	/**
	 * <pre>
	 * 통합의안 목록건수을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return Integer
	 * @throws Exception
	 */
	public int getCountOfAgenda(BaseCommonVO paramObj) throws Exception { 
		return (Integer)select("ptcouncil.new.agenda.ptSelectAgendaCount",paramObj);
	}	// end of getCountOfAgenda

	/**
	 * <pre>
	 * 통합의안 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getListOfAgenda(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectAgendaList",paramObj);
	}	// end of getListOfAgenda

	/**
	 * <pre>
	 * 통합의안 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @param  String							// 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getListOfAgenda(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseAgendaVO>)listWithPaging("ptcouncil.new.agenda.ptSelectAgendaList",paramObj,Integer.parseInt(pageIndex)-1,Integer.parseInt(pageSize));
	}	// end of getListOfAgenda

	/**
	 * <pre>
	 * 통합의안 상세내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseAgendaVO
	 * @throws Exception
	 */
	public BaseAgendaVO getDetailOfAgenda(BaseCommonVO paramObj) throws Exception { 
		return (BaseAgendaVO)select("ptcouncil.new.agenda.ptSelectAgendaDetail",paramObj);
	}	// end of getDetailOfAgenda

	/**
	 * <pre>
	 * 통합의안 상세내역(UUID)을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return BaseAgendaVO
	 * @throws Exception
	 */
	public BaseAgendaVO getDetailOfAgenda(String paramVal) throws Exception { 
		return (BaseAgendaVO)select("ptcouncil.new.agenda.ptSelectAgendaDetailUUID",paramVal);
	}	// end of getDetailOfAgenda

	/**
	 * <pre>
	 * 통합의안 일련번호을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAgendaVO						// 
	 * @return BaseAgendaVO
	 * @throws Exception
	 */
	public BaseAgendaVO getInfoOfAgendaSUGSeq(BaseAgendaVO paramObj) throws Exception { 
		return (BaseAgendaVO)select("ptcouncil.new.agenda.ptSelectAgendaSUGSeq",paramObj);
	}	// end of getInfoOfAgendaSUGSeq

	/**
	 * <pre>
	 * 통합의안 내역을(를) 등록 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAgendaVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfCreateAgendaSUG(BaseAgendaVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.agenda.ptInsertAgendaSUG",paramObj)<=0);
	}	// end of doRtnOfCreateAgendaSUG

	/**
	 * <pre>
	 * 통합의안 내역을(를) 변경 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAgendaVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfUpdateAgendaSUG(BaseAgendaVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.agenda.ptUpdateAgendaSUG",paramObj)<=0);
	}	// end of doRtnOfUpdateAgendaSUG

	/**
	 * <pre>
	 * 통합의안 내역을(를) 삭제 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAgendaVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfDeleteAgendaSUG(BaseAgendaVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.agenda.ptDeleteAgendaSUG",paramObj)<=0);
	}	// end of doRtnOfDeleteAgendaSUG

	/**
	 * <pre>
	 * 통합의안 내역을(를) 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAgendaVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfModifyAgendaSUG(BaseAgendaVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.agenda.ptModifyAgendaSUG",paramObj)<=0);
	}	// end of doRtnOfModifyAgendaSUG

	/**
	 * <pre>
	 * 통합의안 내역을(를) 초기화 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAgendaVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfInitialAgendaSUG(BaseAgendaVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.agenda.ptInitialAgendaSUG",paramObj)<=0);
	}	// end of doRtnOfInitialAgendaSUG

	/**
	 * <pre>
	 * 통합의안 일련번호을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAgendaVO						// 
	 * @return BaseAgendaVO
	 * @throws Exception
	 */
	public BaseAgendaVO getInfoOfAgendaNAMESeq(BaseAgendaVO paramObj) throws Exception { 
		return (BaseAgendaVO)select("ptcouncil.new.agenda.ptSelectAgendaNAMESeq",paramObj);
	}	// end of getInfoOfAgendaNAMESeq

	/**
	 * <pre>
	 * 통합의안 내역을(를) 등록 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAgendaVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfCreateAgendaNAME(BaseAgendaVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.agenda.ptInsertAgendaNAME",paramObj)<=0);
	}	// end of doRtnOfCreateAgendaNAME

	/**
	 * <pre>
	 * 통합의안 내역을(를) 변경 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAgendaVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfUpdateAgendaNAME(BaseAgendaVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.agenda.ptUpdateAgendaNAME",paramObj)<=0);
	}	// end of doRtnOfUpdateAgendaNAME

	/**
	 * <pre>
	 * 통합의안 내역을(를) 삭제 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAgendaVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfDeleteAgendaNAME(BaseAgendaVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.agenda.ptDeleteAgendaNAME",paramObj)<=0);
	}	// end of doRtnOfDeleteAgendaNAME

	/**
	 * <pre>
	 * 통합의안 내역을(를) 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAgendaVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfModifyAgendaNAME(BaseAgendaVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.agenda.ptModifyAgendaNAME",paramObj)<=0);
	}	// end of doRtnOfModifyAgendaNAME

	/**
	 * <pre>
	 * 통합의안 내역을(를) 초기화 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAgendaVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfInitialAgendaNAME(BaseAgendaVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.agenda.ptInitialAgendaNAME",paramObj)<=0);
	}	// end of doRtnOfInitialAgendaNAME

	/**
	 * <pre>
	 * 회의록 기본 목록건수을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return Integer
	 * @throws Exception
	 */
	public int getCountOfConfBase(BaseCommonVO paramObj) throws Exception { 
		return (Integer)select("ptcouncil.new.agenda.ptSelectConfBaseCount",paramObj);
	}	// end of getCountOfConfBase

	/**
	 * <pre>
	 * 회의록 기본 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getListOfConfBase(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectConfBaseList",paramObj);
	}	// end of getListOfConfBase

	/**
	 * <pre>
	 * 회의록 기본 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getListOfConfBase(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseAgendaVO>)listWithPaging("ptcouncil.new.agenda.ptSelectConfBaseList",paramObj,Integer.parseInt(pageIndex)-1,Integer.parseInt(pageSize));
	}	// end of getListOfConfBase

	/**
	 * <pre>
	 * 회의록 기본 상세내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseAgendaVO
	 * @throws Exception
	 */
	public BaseAgendaVO getDetailOfConfBase(BaseCommonVO paramObj) throws Exception { 
		return (BaseAgendaVO)select("ptcouncil.new.agenda.ptSelectConfBaseDetail",paramObj);
	}	// end of getDetailOfConfBase

	/**
	 * <pre>
	 * 회의록 기본 상세내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return BaseAgendaVO
	 * @throws Exception
	 */
	public BaseAgendaVO getDetailOfConfBase(String paramVal) throws Exception { 
		return (BaseAgendaVO)select("ptcouncil.new.agenda.ptSelectConfBaseDetailUUID",paramVal);
	}	// end of getDetailOfConfBase

	/**
	 * <pre>
	 * 통합의안 내역을(를) 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  HashMap							// 
	 * @return Boolean
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public boolean doRtnOfModifyConfBase(HashMap paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.agenda.ptModifyConfBase",paramObj)<=0);
	}	// end of doRtnOfModifyConfBase


	/**
	 * <pre>
	 * 회의록 회기별 검색 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getListOfConfSess(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectConfSessList",paramObj);
	}	// end of getListOfConfSess

	/**
	 * <pre>
	 * 회의록 회의별 검색 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getListOfConfMeet(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectConfMeetList",paramObj);
	}	// end of getListOfConfMeet

	/**
	 * <pre>
	 * 회의록 연도별 검색 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getListOfConfYear(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectConfYearList",paramObj);
	}	// end of getListOfConfYear

	/**
	 * <pre>
	 * 회의록 회의차수 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getLoadOfConfPoint(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectConfPointLoad",paramObj);
	}	// end of getLoadOfConfPoint

	/**
	 * <pre>
	 * 회기별 - 회의구분(상위) 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getLoadOfConfHKind(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectConfHKindLoad",paramObj);
	}	// end of getLoadOfConfHKind

	/**
	 * <pre>
	 * 회기별 - 회의구분(하위) 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getLoadOfConfLKind(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectConfLKindLoad",paramObj);
	}	// end of getLoadOfConfLKind

	/**
	 * <pre>
	 * 회의별 - 회의대수 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getLoadOfConfPeriod(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectConfPeriodLoad",paramObj);
	}	// end of getLoadOfConfPeriod

	/**
	 * <pre>
	 * 회기별 - 회의일정 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getLoadOfSessRound(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectSessRoundLoad",paramObj);
	}	// end of getLoadOfSessRound

	/**
	 * <pre>
	 * 회의별 - 회의일정 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getLoadOfMeetRound(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectMeetRoundLoad",paramObj);
	}	// end of getLoadOfMeetRound

	/**
	 * <pre>
	 * 연도별 - 회의일정 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getLoadOfYearRound(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectYearRoundLoad",paramObj);
	}	// end of getLoadOfYearRound

	/**
	 * <pre>
	 * 회의통계 - 회의일자별 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getStatOfAgendaDays(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectStatDays",paramObj);
	}	// end of getStatOfAgendaDays

	/**
	 * <pre>
	 * 회의통계 - 회의시간별 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getStatOfAgendaTime(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectStatTime",paramObj);
	}	// end of getStatOfAgendaTime

	/**
	 * <pre>
	 * 의안검색 목록건수을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return Integer
	 * @throws Exception
	 */
	public int getCountOfAgendaFind(BaseCommonVO paramObj) throws Exception { 
		return (Integer)select("ptcouncil.new.agenda.ptSelectAgendaFindCount",paramObj);
	}	// end of getCountOfAgendaFind

	/**
	 * <pre>
	 * 의안검색 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getListOfAgendaFind(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectAgendaFindList",paramObj);
	}	// end of getListOfAgendaFind

	/**
	 * <pre>
	 * 의안검색 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @param  String							// 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getListOfAgendaFind(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseAgendaVO>)listWithPaging("ptcouncil.new.agenda.ptSelectAgendaFindList",paramObj,Integer.parseInt(pageIndex)-1,Integer.parseInt(pageSize));
	}	// end of getListOfAgendaFind

	/**
	 * <pre>
	 * 의안검색 상세(이전/다음)을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseAgendaVO
	 * @throws Exception
	 */
	public BaseAgendaVO getMovingOfAgendaFind(BaseCommonVO paramObj) throws Exception { 
		return (BaseAgendaVO)select("ptcouncil.new.agenda.ptSelectAgendaFindMove",paramObj);
	}	// end of getMovingOfAgendaFind

	/**
	 * <pre>
	 * 의안검색 상세내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseAgendaVO
	 * @throws Exception
	 */
	public BaseAgendaVO getDetailOfAgendaFind(BaseCommonVO paramObj) throws Exception { 
		return (BaseAgendaVO)select("ptcouncil.new.agenda.ptSelectAgendaFindDetail",paramObj);
	}	// end of getDetailOfAgendaFind

	/**
	 * <pre>
	 * 의안검색 파일내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getListOfAgendaFile(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectAgendaFindFile",paramObj);
	}	// end of getListOfAgendaFile

	/**
	 * <pre>
	 * 의안정보 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getListOfAgendaInfo(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectAgendaInfoList",paramObj);
	}	// end of getListOfAgendaInfo

	/**
	 * <pre>
	 * 의안정보 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @param  String							// 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getListOfAgendaInfo(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseAgendaVO>)listWithPaging("ptcouncil.new.agenda.ptSelectAgendaInfoList",paramObj,Integer.parseInt(pageIndex)-1,Integer.parseInt(pageSize));
	}	// end of getListOfAgendaInfo

	/**
	 * <pre>
	 * 의안정보 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getChldOfAgendaInfo(String paramVal) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectAgendaInfoChild",paramVal);
	}	// end of getChldOfAgendaInfo

	/**
	 * <pre>
	 * 의안항목 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getListOfAgendaItem(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectAgendaItemList",paramObj);
	}	// end of getListOfAgendaItem

	/**
	 * <pre>
	 * 의안항목 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @param  String							// 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getListOfAgendaItem(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseAgendaVO>)listWithPaging("ptcouncil.new.agenda.ptSelectAgendaItemList",paramObj,Integer.parseInt(pageIndex)-1,Integer.parseInt(pageSize));
	}	// end of getListOfAgendaItem
	/**
	 * <pre>
	 * 의안항목 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getChldOfAgendaItem(String paramVal) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectAgendaItemChild",paramVal);
	}	// end of getChldOfAgendaItem

	/**
	 * <pre>
	 * 회의록 의안통계 - 안건종류 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getStatOfAgendaKind(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectStatKind",paramObj);
	}	// end of getStatOfAgendaKind

	/**
	 * <pre>
	 * 회의록 의안통계 - 위원회 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getStatOfAgendaComm(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectStatComm",paramObj);
	}	// end of getStatOfAgendaComm

	/**
	 * <pre>
	 * 회의록 영상 목록건수을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return Integer
	 * @throws Exception
	 */
	public int getCountOfConfCast(BaseCommonVO paramObj) throws Exception { 
		return (Integer)select("ptcouncil.new.agenda.ptSelectConfCastCount",paramObj);
	}	// end of getCountOfConfCast

	/**
	 * <pre>
	 * 회의록 영상 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getListOfConfCast(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectConfCastList",paramObj);
	}	// end of getListOfConfCast

	/**
	 * <pre>
	 * 회의록 영상 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getListOfConfCast(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseAgendaVO>)listWithPaging("ptcouncil.new.agenda.ptSelectConfCastList",paramObj,Integer.parseInt(pageIndex)-1,Integer.parseInt(pageSize));
	}	// end of getListOfConfCast

	/**
	 * <pre>
	 * 회의록 영상 상세내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseAgendaVO
	 * @throws Exception
	 */
	public BaseAgendaVO getDetailOfConfCast(BaseCommonVO paramObj) throws Exception { 
		return (BaseAgendaVO)select("ptcouncil.new.agenda.ptSelectConfCastDetail",paramObj);
	}	// end of getDetailOfConfCast

	/**
	 * <pre>
	 * 회의록 영상 - Child 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getChldOfConfCast(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectConfCastChild",paramObj);
	}	// end of getListOfConfCast

	/**
	 * <pre>
	 * 회의록 영상 - Child 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getChldOfConfCast(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseAgendaVO>)listWithPaging("ptcouncil.new.agenda.ptSelectConfCastChild",paramObj,Integer.parseInt(pageIndex)-1,Integer.parseInt(pageSize));
	}	// end of getListOfConfCast

	/**
	 * <pre>
	 * 현역의원 메인글 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getMainOfAgendaMemb(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectMainMemb",paramObj);
	}	// end of getMainOfAgendaMemb

	/**
	 * <pre>
	 * 접수의안 메인글 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getMainOfAgendaRecv(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectMainRecv",paramObj);
	}	// end of getMainOfAgendaRecv

	/**
	 * <pre>
	 * 처리의안 메인글 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getMainOfAgendaProc(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectMainProc",paramObj);
	}	// end of getMainOfAgendaProc

	/**
	 * <pre>
	 * 회의록정보 메인글 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getMainOfAgendaConf(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectMainConf",paramObj);
	}	// end of getMainOfAgendaConf

	/**
	 * <pre>
	 * 의안정보 메인글 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAgendaVO> getMainOfAgendaItem(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)list("ptcouncil.new.agenda.ptSelectMainItem",paramObj);
	}	// end of getMainOfAgendaItem


	//	엑셀 안건 정보 업데이트
	public 	boolean doRtnOfUpdateConfSug(BaseAgendaVO paramObj) throws Exception {
		return !((int)update("ptcouncil.new.agenda.ptUpdateConfSug",paramObj)<=0);
	}

}	// end of class