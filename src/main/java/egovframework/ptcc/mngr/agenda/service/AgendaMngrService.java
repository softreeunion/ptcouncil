/*******************************************************************************
  Program ID  : AgendaMngrService
  Description : 회의록 관리에 관한 Service Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.agenda.service;

import java.util.*;

import egovframework.ptcc.cmmn.vo.*;

public interface AgendaMngrService {

	// 통합의안 관리
	String getInfoOfAgendaUUID(String paramVal) throws Exception;
	int getCountOfAgenda(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getListOfAgenda(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getListOfAgenda(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception;
	BaseAgendaVO getDetailOfAgenda(BaseCommonVO paramObj) throws Exception;
	BaseAgendaVO getDetailOfAgenda(String paramVal) throws Exception;
	BaseAgendaVO getInfoOfAgendaSUGSeq(BaseAgendaVO paramObj) throws Exception;
	boolean doRtnOfCreateAgendaSUG(BaseAgendaVO paramObj) throws Exception;
	boolean doRtnOfUpdateAgendaSUG(BaseAgendaVO paramObj) throws Exception;
	boolean doRtnOfDeleteAgendaSUG(BaseAgendaVO paramObj) throws Exception;
	boolean doRtnOfModifyAgendaSUG(BaseAgendaVO paramObj) throws Exception;
	boolean doRtnOfInitialAgendaSUG(BaseAgendaVO paramObj) throws Exception;
	BaseAgendaVO getInfoOfAgendaNAMESeq(BaseAgendaVO paramObj) throws Exception;
	boolean doRtnOfCreateAgendaNAME(BaseAgendaVO paramObj) throws Exception;
	boolean doRtnOfUpdateAgendaNAME(BaseAgendaVO paramObj) throws Exception;
	boolean doRtnOfDeleteAgendaNAME(BaseAgendaVO paramObj) throws Exception;
	boolean doRtnOfModifyAgendaNAME(BaseAgendaVO paramObj) throws Exception;
	boolean doRtnOfInitialAgendaNAME(BaseAgendaVO paramObj) throws Exception;

	// 회의록 기본 관리
	int getCountOfConfBase(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getListOfConfBase(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getListOfConfBase(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception;
	BaseAgendaVO getDetailOfConfBase(BaseCommonVO paramObj) throws Exception;
	BaseAgendaVO getDetailOfConfBase(String paramVal) throws Exception;
	@SuppressWarnings("rawtypes")
	boolean doRtnOfModifyConfBase(HashMap paramObj) throws Exception;

	// 회의록 검색 관리
	List<BaseAgendaVO> getListOfConfSess(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getListOfConfMeet(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getListOfConfYear(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getLoadOfConfPoint(BaseCommonVO paramObj) throws Exception;

	List<BaseAgendaVO> getLoadOfConfHKind(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getLoadOfConfLKind(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getLoadOfConfPeriod(BaseCommonVO paramObj) throws Exception;

	List<BaseAgendaVO> getLoadOfSessRound(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getLoadOfMeetRound(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getLoadOfYearRound(BaseCommonVO paramObj) throws Exception;

	// 회의록 통계 관리
	List<BaseAgendaVO> getStatOfAgendaDays(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getStatOfAgendaTime(BaseCommonVO paramObj) throws Exception;

	// 회의록 의안 관리
	int getCountOfAgendaFind(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getListOfAgendaFind(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getListOfAgendaFind(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception;
	BaseAgendaVO getMovingOfAgendaFind(BaseCommonVO paramObj) throws Exception;
	BaseAgendaVO getDetailOfAgendaFind(BaseCommonVO paramObj) throws Exception;

	List<BaseAgendaVO> getListOfAgendaInfo(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getListOfAgendaInfo(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception;
	List<BaseAgendaVO> getChldOfAgendaInfo(String paramVal) throws Exception;
	List<BaseAgendaVO> getListOfAgendaItem(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getListOfAgendaItem(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception;
	List<BaseAgendaVO> getChldOfAgendaItem(String paramVal) throws Exception;

	// 회의록 부록 관리
	List<BaseAgendaVO> getListOfAgendaFile(BaseCommonVO paramObj) throws Exception;

	// 회의록 의안 통계
	List<BaseAgendaVO> getStatOfAgendaKind(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getStatOfAgendaComm(BaseCommonVO paramObj) throws Exception;

	// 회의록 영상 관리
	int getCountOfConfCast(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getListOfConfCast(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getListOfConfCast(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception;
	BaseAgendaVO getDetailOfConfCast(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getChldOfConfCast(BaseCommonVO paramObj) throws Exception;
	List<BaseAgendaVO> getChldOfConfCast(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception;


	// 현역의원 메인글 관리
	List<BaseAgendaVO> getMainOfAgendaMemb(BaseCommonVO paramObj) throws Exception;
	// 접수의안 메인글 관리
	List<BaseAgendaVO> getMainOfAgendaRecv(BaseCommonVO paramObj) throws Exception;
	// 처리의안 메인글 관리
	List<BaseAgendaVO> getMainOfAgendaProc(BaseCommonVO paramObj) throws Exception;

	// 회의록정보 메인글 관리
	List<BaseAgendaVO> getMainOfAgendaConf(BaseCommonVO paramObj) throws Exception;
	// 의안정보 메인글 관리
	List<BaseAgendaVO> getMainOfAgendaItem(BaseCommonVO paramObj) throws Exception;


	//	엑셀 안건 정보 업데이트
	boolean doRtnOfUpdateConfSug(BaseAgendaVO paramObj) throws Exception;

}	// end of interface