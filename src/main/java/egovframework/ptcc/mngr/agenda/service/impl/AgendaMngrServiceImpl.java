/*******************************************************************************
  Program ID  : AgendaMngrServiceImpl
  Description : 회의록 관리에 관한 Service Implement Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.agenda.service.impl;

import java.util.*;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.agenda.service.*;

@Service("agendaMngrService")
public class AgendaMngrServiceImpl extends egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl implements AgendaMngrService { 

	@Resource(name="agendaMngrDAO")
	private AgendaMngrDAO					agendaMngrDAO;

	@Override
	public String getInfoOfAgendaUUID(String paramVal) throws Exception { 
		return (String)agendaMngrDAO.getInfoOfAgendaUUID( paramVal );
	}
	@Override
	public int getCountOfAgenda(BaseCommonVO paramObj) throws Exception { 
		return (int)agendaMngrDAO.getCountOfAgenda( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getListOfAgenda(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getListOfAgenda( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getListOfAgenda(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getListOfAgenda( paramObj, pageIndex, pageSize );
	}
	@Override
	public BaseAgendaVO getDetailOfAgenda(BaseCommonVO paramObj) throws Exception { 
		return (BaseAgendaVO)agendaMngrDAO.getDetailOfAgenda( paramObj );
	}
	@Override
	public BaseAgendaVO getDetailOfAgenda(String paramVal) throws Exception { 
		return (BaseAgendaVO)agendaMngrDAO.getDetailOfAgenda( paramVal );
	}
	@Override
	public BaseAgendaVO getInfoOfAgendaSUGSeq(BaseAgendaVO paramObj) throws Exception { 
		return (BaseAgendaVO)agendaMngrDAO.getInfoOfAgendaSUGSeq( paramObj );
	}
	@Override
	public boolean doRtnOfCreateAgendaSUG(BaseAgendaVO paramObj) throws Exception { 
		return (boolean)agendaMngrDAO.doRtnOfCreateAgendaSUG( paramObj );
	}
	@Override
	public boolean doRtnOfUpdateAgendaSUG(BaseAgendaVO paramObj) throws Exception { 
		return (boolean)agendaMngrDAO.doRtnOfUpdateAgendaSUG( paramObj );
	}
	@Override
	public boolean doRtnOfDeleteAgendaSUG(BaseAgendaVO paramObj) throws Exception { 
		return (boolean)agendaMngrDAO.doRtnOfDeleteAgendaSUG( paramObj );
	}
	@Override
	public boolean doRtnOfModifyAgendaSUG(BaseAgendaVO paramObj) throws Exception { 
		return (boolean)agendaMngrDAO.doRtnOfModifyAgendaSUG( paramObj );
	}
	@Override
	public boolean doRtnOfInitialAgendaSUG(BaseAgendaVO paramObj) throws Exception { 
		return (boolean)agendaMngrDAO.doRtnOfInitialAgendaSUG( paramObj );
	}
	@Override
	public BaseAgendaVO getInfoOfAgendaNAMESeq(BaseAgendaVO paramObj) throws Exception { 
		return (BaseAgendaVO)agendaMngrDAO.getInfoOfAgendaNAMESeq( paramObj );
	}
	@Override
	public boolean doRtnOfCreateAgendaNAME(BaseAgendaVO paramObj) throws Exception { 
		return (boolean)agendaMngrDAO.doRtnOfCreateAgendaNAME( paramObj );
	}
	@Override
	public boolean doRtnOfUpdateAgendaNAME(BaseAgendaVO paramObj) throws Exception { 
		return (boolean)agendaMngrDAO.doRtnOfUpdateAgendaNAME( paramObj );
	}
	@Override
	public boolean doRtnOfDeleteAgendaNAME(BaseAgendaVO paramObj) throws Exception { 
		return (boolean)agendaMngrDAO.doRtnOfDeleteAgendaNAME( paramObj );
	}
	@Override
	public boolean doRtnOfModifyAgendaNAME(BaseAgendaVO paramObj) throws Exception { 
		return (boolean)agendaMngrDAO.doRtnOfModifyAgendaNAME( paramObj );
	}
	@Override
	public boolean doRtnOfInitialAgendaNAME(BaseAgendaVO paramObj) throws Exception { 
		return (boolean)agendaMngrDAO.doRtnOfInitialAgendaNAME( paramObj );
	}

	@Override
	public int getCountOfConfBase(BaseCommonVO paramObj) throws Exception { 
		return (int)agendaMngrDAO.getCountOfConfBase( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getListOfConfBase(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getListOfConfBase( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getListOfConfBase(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getListOfConfBase( paramObj, pageIndex, pageSize );
	}
	@Override
	public BaseAgendaVO getDetailOfConfBase(BaseCommonVO paramObj) throws Exception { 
		return (BaseAgendaVO)agendaMngrDAO.getDetailOfConfBase( paramObj );
	}
	@Override
	public BaseAgendaVO getDetailOfConfBase(String paramVal) throws Exception { 
		return (BaseAgendaVO)agendaMngrDAO.getDetailOfConfBase( paramVal );
	}
	@SuppressWarnings("rawtypes")
	@Override
	public boolean doRtnOfModifyConfBase(HashMap paramObj) throws Exception { 
		return (boolean)agendaMngrDAO.doRtnOfModifyConfBase( paramObj );
	}

	@Override
	public List<BaseAgendaVO> getListOfConfSess(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getListOfConfSess( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getListOfConfMeet(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getListOfConfMeet( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getListOfConfYear(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getListOfConfYear( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getLoadOfConfPoint(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getLoadOfConfPoint( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getLoadOfConfHKind(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getLoadOfConfHKind( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getLoadOfConfLKind(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getLoadOfConfLKind( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getLoadOfConfPeriod(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getLoadOfConfPeriod( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getLoadOfSessRound(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getLoadOfSessRound( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getLoadOfMeetRound(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getLoadOfMeetRound( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getLoadOfYearRound(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getLoadOfYearRound( paramObj );
	}

	@Override
	public List<BaseAgendaVO> getStatOfAgendaDays(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getStatOfAgendaDays( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getStatOfAgendaTime(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getStatOfAgendaTime( paramObj );
	}

	@Override
	public int getCountOfAgendaFind(BaseCommonVO paramObj) throws Exception { 
		return (int)agendaMngrDAO.getCountOfAgendaFind( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getListOfAgendaFind(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getListOfAgendaFind( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getListOfAgendaFind(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getListOfAgendaFind( paramObj );
	}
	@Override
	public BaseAgendaVO getMovingOfAgendaFind(BaseCommonVO paramObj) throws Exception { 
		return (BaseAgendaVO)agendaMngrDAO.getMovingOfAgendaFind( paramObj );
	}
	@Override
	public BaseAgendaVO getDetailOfAgendaFind(BaseCommonVO paramObj) throws Exception { 
		return (BaseAgendaVO)agendaMngrDAO.getDetailOfAgendaFind( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getListOfAgendaFile(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getListOfAgendaFile( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getListOfAgendaInfo(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getListOfAgendaInfo( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getListOfAgendaInfo(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getListOfAgendaInfo( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getChldOfAgendaInfo(String paramVal) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getChldOfAgendaInfo( paramVal );
	}
	@Override
	public List<BaseAgendaVO> getListOfAgendaItem(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getListOfAgendaItem( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getListOfAgendaItem(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getListOfAgendaItem( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getChldOfAgendaItem(String paramVal) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getChldOfAgendaItem( paramVal );
	}

	@Override
	public List<BaseAgendaVO> getStatOfAgendaKind(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getStatOfAgendaKind( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getStatOfAgendaComm(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getStatOfAgendaComm( paramObj );
	}

	@Override
	public int getCountOfConfCast(BaseCommonVO paramObj) throws Exception { 
		return (int)agendaMngrDAO.getCountOfConfCast( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getListOfConfCast(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getListOfConfCast( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getListOfConfCast(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getListOfConfCast( paramObj, pageIndex, pageSize );
	}
	@Override
	public BaseAgendaVO getDetailOfConfCast(BaseCommonVO paramObj) throws Exception { 
		return (BaseAgendaVO)agendaMngrDAO.getDetailOfConfCast( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getChldOfConfCast(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getChldOfConfCast( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getChldOfConfCast(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getChldOfConfCast( paramObj, pageIndex, pageSize );
	}

	@Override
	public List<BaseAgendaVO> getMainOfAgendaMemb(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getMainOfAgendaMemb( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getMainOfAgendaRecv(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getMainOfAgendaRecv( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getMainOfAgendaProc(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getMainOfAgendaProc( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getMainOfAgendaConf(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getMainOfAgendaConf( paramObj );
	}
	@Override
	public List<BaseAgendaVO> getMainOfAgendaItem(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAgendaVO>)agendaMngrDAO.getMainOfAgendaItem( paramObj );
	}


	//	엑셀 안건 정보 업데이트
	@Override
	public 	boolean doRtnOfUpdateConfSug(BaseAgendaVO paramObj) throws Exception {
		return (boolean)agendaMngrDAO.doRtnOfUpdateConfSug( paramObj );
	}

}	// end of class