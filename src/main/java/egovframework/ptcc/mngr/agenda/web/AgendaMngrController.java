/*******************************************************************************
  Program ID  : AgendaMngrController
  Description : 회의록 관리에 관한 controller 클래스를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.agenda.web;

import java.io.*;
import java.util.*;

import javax.annotation.*;
import javax.servlet.http.*;

import net.sf.json.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.dao.*;
import org.springframework.jdbc.*;
import org.springframework.jdbc.datasource.*;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.*;
import org.springframework.transaction.support.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.*;
import org.springframework.web.servlet.*;
import org.springmodules.validation.commons.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.excel.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.util.*;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.agenda.service.*;

@Controller
@RequestMapping(value="/ptnhexa/{paramPath}")
public class AgendaMngrController implements HandlerExceptionResolver {

	@Autowired
	private WebApplicationContext			webContext;
	@Autowired
	private DataSourceTransactionManager	txManager;

	@Resource(name="beanValidator")
	protected DefaultBeanValidator			beanValidator;
	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="agendaMngrService")
	protected AgendaMngrService			agendaMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));
	protected String						defsPass     = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsPass" );
	protected String						filePath     = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.BodyConfer" );
	protected String						DATAS_HTML   = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DATAS_HTML" );
	protected String						DATAS_VODS   = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DATAS_VOD" );
	protected String						lnkName      = "CONF_MS";
	protected boolean						notiItems    = false;
	protected boolean						moveItems    = false;
	protected boolean						fileUP       = true;
	protected boolean						tempUP       = false;

	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelMap model, 
			boolean connMode 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			model.addAttribute("message"       , "로그인 정보가 존재하지 않습니다.\\n\\n로그인 화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			model.addAttribute("message"       , "임시 비밀번호를 사용하고 있습니다.\\n\\n비밀번호 변경화면으로 이동합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			model.addAttribute("close"         , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			model.addAttribute("error"         , "접속권한이 없습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		return false;
	}
	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelMap model, 
			boolean connMode, 
			String viewNo 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			model.addAttribute("message"       , "로그인 정보가 존재하지 않습니다.\\n\\n로그인 화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			model.addAttribute("message"       , "임시 비밀번호를 사용하고 있습니다.\\n\\n비밀번호 변경화면으로 이동합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			model.addAttribute("close"         , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			model.addAttribute("error"         , "접속권한이 없습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 불량 접속 확인
		if( BaseUtility.isValidNull( viewNo ) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		return false;
	}
	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelAndView mav, 
			boolean connMode 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "로그인 정보가 존재하지 않습니다.\n\n로그인 화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "임시 비밀번호를 사용하고 있습니다.\n\n비밀번호 변경화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속권한이 없습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		return false;
	}
	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelAndView mav, 
			boolean connMode, 
			String viewNo 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "로그인 정보가 존재하지 않습니다.\n\n로그인 화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "임시 비밀번호를 사용하고 있습니다.\n\n비밀번호 변경화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속권한이 없습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 불량 접속 확인
		if( BaseUtility.isValidNull( viewNo ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "잘못된 경로로 접속하였습니다.\n\n메인화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		return false;
	}


	/**
	 * <pre>
	 * 회의록 정보을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/AgendaList.do")
	public String scrListOfAgenda ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					retPath      = "agenda/agenda"+ BaseUtility.toCapitalize( paramPath ) +"_list";

		int						listCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseAgendaVO>		dataList     = new ArrayList<BaseAgendaVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		eraList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		kndList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		rndList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		memList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		pntList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		tempList     = new ArrayList<BaseCommonVO>();
		BaseCommonVO			tempView     = new BaseCommonVO();
		JSONObject				jsonObjs     = new JSONObject();
		JSONArray				tempTree     = new JSONArray();
		List<String>			rndCodes     = new ArrayList<String>();
log.debug(" act: "+ BaseUtility.spaceFill( "Agenda"+ BaseUtility.toCapitalize( paramPath ) +"List", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false ) ) return "/cmmn/exe_script";

		// 기본 조회조건 취득
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(10);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "N" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reConfType"    , searchVO.getReConfType());
		model.addAttribute("reEraCode"     , searchVO.getReEraCode());
		model.addAttribute("reKndCode"     , searchVO.getReKndCode());
		model.addAttribute("reRndCode"     , searchVO.getReRndCode());
		model.addAttribute("reRndDiv"      , searchVO.getReRndDiv());
		model.addAttribute("reMemID"       , searchVO.getReMemID());
		model.addAttribute("searchText"    , searchVO.getSearchText());

		// 추가 조회조건 취득
//		searchVO.setCondCate(BaseUtility.null2Char( searchVO.getCondCate(), "%" ));
//		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReBgnDate(BaseUtility.null2Blank( searchVO.getReBgnDate() ).replaceAll("[^0-9]", ""));
		searchVO.setReEndDate(BaseUtility.null2Blank( searchVO.getReEndDate() ).replaceAll("[^0-9]", ""));

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("bgnDate: "+ searchVO.getReBgnDate() +" |endDate: "+ searchVO.getReEndDate());
log.debug("eraCode: "+ searchVO.getReEraCode() +" |kndCode: "+ searchVO.getReKndCode() +" |rndCode: "+ searchVO.getReRndCode() +" |rndDiv: "+ searchVO.getReRndDiv());
log.debug("confType: "+ searchVO.getReConfType() +" |memID: "+ searchVO.getReMemID() +" |searchText: "+ searchVO.getSearchText());
		try { 
			listCnt = (int)agendaMngrService.getCountOfConfBase( searchVO );

			// 조회 건수를 전체선택인 경우
			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
			}
			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
			searchVO.setPageFirNo(Integer.toString(idxFir));
			searchVO.setPageLstNo(Integer.toString(idxLst));
			searchVO.setListCnt(Integer.toString(listCnt));

			if( listCnt > 0 ) { 
				dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfBase( searchVO );
//				dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfBase( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				// Data Adjustment(Agendas)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					for( int d = 0; d < dataList.size(); d++ ) { 
						dataList.get(d).setViewNo(dataList.get(d).getConfUUID() + dataList.get(d).getRowSeq());

						dataList.get(d).setConfDate(BaseUtility.toDateFormat( dataList.get(d).getConfDate(), dtSep ));
						dataList.get(d).setConfStartTime(BaseUtility.toTimeFormat( dataList.get(d).getConfStartTime() ));
						dataList.get(d).setConfCloseTime(BaseUtility.toTimeFormat( dataList.get(d).getConfCloseTime() ));
						dataList.get(d).setConfReqTime(BaseUtility.toTimeFormat( dataList.get(d).getConfReqTime() ));
						dataList.get(d).setConfWeek(BaseUtility.date2Week( dataList.get(d).getConfDate() ));
					}
				}

				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
				pageInfo.setTotalRecordCount(listCnt);
			}

			// 분류구분 취득 - 전체 회의대수 구분
			eraList = (List<BaseCommonVO>)baseCmmnService.getListOfClassALL( "" );
			// 분류구분 취득 - 회의회수 구분
			rndList = (List<BaseCommonVO>)baseCmmnService.getListOfRoundALL( searchVO.getReEraCode() );
			// 분류구분 취득 - 희의분류 구분
			kndList = (List<BaseCommonVO>)baseCmmnService.getListOfGroup( "" );
			// 분류구분 취득 - 위원명
			memList = (List<BaseCommonVO>)baseCmmnService.getListOfMembALL( searchVO.getReEraCode() );

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			model.addAttribute("dataList"      , dataList);
			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (dataList == null ? 0 : dataList.size()) : listCnt ));
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("eraList"       , eraList);
			model.addAttribute("kndList"       , kndList);
			model.addAttribute("rndList"       , rndList);
			model.addAttribute("pntList"       , pntList);
			model.addAttribute("memList"       , memList);
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/"+retPath;
	}	// end of scrListOfAgenda

	/**
	 * <pre>
	 * 회의록 내역을(를) 상세조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/AgendaView.do")
	public String scrViewOfAgenda ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "agenda/agenda"+ BaseUtility.toCapitalize( paramPath ) +"_view";

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[3]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[3] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		BaseAgendaVO			dataView     = new BaseAgendaVO();
		BaseAgendaVO			prevView     = new BaseAgendaVO();
		BaseAgendaVO			nextView     = new BaseAgendaVO();
		List<BaseFilesVO>		dataFile     = new ArrayList<BaseFilesVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		memList      = new ArrayList<BaseCommonVO>();
		BaseFilesVO				fileView     = new BaseFilesVO();
log.debug(" act: "+ BaseUtility.spaceFill( "Agenda"+ BaseUtility.toCapitalize( paramPath ) +"View", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false, searchVO.getViewNo() ) ) return "/cmmn/exe_script";

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reConfType"    , searchVO.getReConfType());
		model.addAttribute("reEraCode"     , searchVO.getReEraCode());
		model.addAttribute("reKndCode"     , searchVO.getReKndCode());
		model.addAttribute("reRndCode"     , searchVO.getReRndCode());
		model.addAttribute("reRndDiv"      , searchVO.getReRndDiv());
		model.addAttribute("reMemID"       , searchVO.getReMemID());
		model.addAttribute("searchText"    , searchVO.getSearchText());

		// 추가 조회조건 취득
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReBgnDate(BaseUtility.null2Blank( searchVO.getReBgnDate() ).replaceAll("[^0-9]", ""));
		searchVO.setReEndDate(BaseUtility.null2Blank( searchVO.getReEndDate() ).replaceAll("[^0-9]", ""));
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				dataView = (BaseAgendaVO)agendaMngrService.getDetailOfConfBase( searchVO );
				if( dataView != null ) { 
					// Data Adjustment(Agendas)
					dataView.setActMode("Update");
					dataView.setViewNo(dataView.getConfUUID());

					dataView.setConfDate(BaseUtility.toDateFormat( dataView.getConfDate(), dtSep, 8 ));
					dataView.setConfStartTime(BaseUtility.toTimeFormat( dataView.getConfStartTime() ));
					dataView.setConfCloseTime(BaseUtility.toTimeFormat( dataView.getConfCloseTime() ));
					dataView.setConfReqTime(BaseUtility.toTimeFormat( dataView.getConfReqTime() ));

					if( FileProcess.existFile( BaseUtility.getFileBase().replaceAll("/datas", "") + DATAS_VODS +"/"+ dataView.getConfCode() +".mp4" ) ) { 
						fileView = new BaseFilesVO();
						fileView.setFileSeq("1");
						fileView.setFileUUID(dataView.getConfUUID());
						fileView.setFileType("mp4");
						fileView.setFileRel(dataView.getConfCode() +".mp4");
						fileView.setFileAbs(DATAS_VODS +"/"+ dataView.getConfCode() +".mp4");
						dataFile.add(fileView);
					}
				}

				// 분류구분 취득 - 위원명
				memList = (List<BaseCommonVO>)baseCmmnService.getListOfMembALL( dataView.getEraCode() );
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("dataFile"      , dataFile);
			model.addAttribute("moveItem"      , moveItems?"T":"F");
			model.addAttribute("prevView"      , prevView);
			model.addAttribute("nextView"      , nextView);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("memList"       , memList);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/"+retPath;
	}	// end of scrViewOfAgenda

	/**
	 * <pre>
	 * 회의록 내역을 변경처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return String
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/AgendaModify.do")
	public ModelAndView doActOfModifyAgenda ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseAgendaVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[3]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[3] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseAgendaVO			dataView     = new BaseAgendaVO();
		BaseAgendaVO			dataTemp     = new BaseAgendaVO();
		String[]				deleList     = null;
log.debug(" act: "+ BaseUtility.spaceFill( "Agenda"+ BaseUtility.toCapitalize( paramPath ) +"Modify", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false ) ) return mav;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reConfType"    , searchVO.getReConfType());
		model.addAttribute("reEraCode"     , searchVO.getReEraCode());
		model.addAttribute("reKndCode"     , searchVO.getReKndCode());
		model.addAttribute("reRndCode"     , searchVO.getReRndCode());
		model.addAttribute("reRndDiv"      , searchVO.getReRndDiv());
		model.addAttribute("reMemID"       , searchVO.getReMemID());
		model.addAttribute("searchText"    , searchVO.getSearchText());

		// 추가 조회조건 취득
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReBgnDate(BaseUtility.null2Blank( searchVO.getReBgnDate() ).replaceAll("[^0-9]", ""));
		searchVO.setReEndDate(BaseUtility.null2Blank( searchVO.getReEndDate() ).replaceAll("[^0-9]", ""));
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/agenda/agenda"+ BaseUtility.toCapitalize( paramPath ) +"_edit";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				dataView = (BaseAgendaVO)agendaMngrService.getDetailOfConfBase( searchVO );
				dataView.setSugType("N");

				// 발의의원 탐색값 중복확인
				if( !BaseUtility.isValidNull( request.getParameterValues("nSugPosi") ) ) { 
					String[] chkList = request.getParameterValues("nSugPosi");
					for( int i = 0; i < chkList.length; i++ ) { 
						if( BaseUtility.isValidNull( chkList[i] ) ) continue;
						for( int j = 0; j < chkList.length; j++ ) { 
							if( BaseUtility.isValidNull( chkList[j] ) || i == j ) continue;
							if( chkList[i].equals(chkList[j]) ) { 
								mav.addObject("isError"       , "T");
								mav.addObject("message"       , "탐색위치가 중복되어 더 이상 진행되지 않습니다.\n\n확인하시기 바랍니다.");
								return mav;
							}
						}
					}
				}

				// 회의록 내역 삭제
				deleList = BaseUtility.isValidNull( searchVO.getCondDele() ) ? null : BaseUtility.null2Blank( searchVO.getCondDele() ).split("`");
log.debug("condDele: "+ searchVO.getCondDele());
				if( !(deleList == null || deleList.length <= 0) ) { 
					for( int d = 0; d < deleList.length; d++ ) { 
						dataTemp = new BaseAgendaVO();
						dataTemp.setRowUUID(deleList[d]);
						dataTemp.setDelIPv4(BaseUtility.reqIPv4Address( request ));
						dataTemp.setDelUser(BaseUtility.null2Char( searchVO.getDelUser(), memberVO.getUserPID() ));
						dataTemp.setDelName(BaseUtility.null2Char( searchVO.getDelName(), memberVO.getUserName() ));

						agendaMngrService.doRtnOfDeleteAgendaNAME( dataTemp );
					}
				}
				// 회의의안 기본내역 갱신
				if( !BaseUtility.isValidNull( request.getParameterValues("sConfUUID") ) ) { 
					for( int i = 0; i < request.getParameterValues("sConfUUID").length; i++ ) { 
//						if( !BaseUtility.isValidNull( request.getParameterValues("sConfUUID")[i] ) && !(BaseUtility.isValidNull( request.getParameterValues("sTimeStamp")[i] ) && BaseUtility.isValidNull( request.getParameterValues("sTimeMemb")[i] )) ) { 
						if( !BaseUtility.isValidNull( request.getParameterValues("sConfUUID")[i] ) ) { 
							dataTemp = new BaseAgendaVO();
							dataTemp.setRowUUID(BaseUtility.null2Blank( request.getParameterValues("sConfUUID")[i] ));
							dataTemp.setTimeStamp(BaseUtility.toTimeFormat( request.getParameterValues("sTimeStamp")[i] ));
							dataTemp.setTimeMemb(BaseUtility.null2Blank( request.getParameterValues("sTimeMemb")[i] ));

							agendaMngrService.doRtnOfUpdateAgendaSUG(dataTemp);
						}
					}
				}

				// 회의의안 추가내역 갱신
				if( !BaseUtility.isValidNull( request.getParameterValues("nConfUUID") ) ) { 
					for( int i = 0; i < request.getParameterValues("nConfUUID").length; i++ ) { 
						if( BaseUtility.isValidNull( request.getParameterValues("nConfUUID")[i] ) ) { 
							dataTemp = (BaseAgendaVO)agendaMngrService.getInfoOfAgendaNAMESeq( dataView );
							if( dataTemp == null ) { 
								dataTemp = new BaseAgendaVO();
								dataTemp.setConfSeq(dataView.getConfSeq());
								dataTemp.setSugSeq("1");
							}
						} else { 
							viewNo = BaseUtility.null2Char( (String)agendaMngrService.getInfoOfAgendaUUID( BaseUtility.null2Blank( request.getParameterValues("nConfUUID")[i] ) ), "`" ).split("[`]", viewNo.length);

							dataTemp = new BaseAgendaVO();
							dataTemp.setConfSeq(viewNo[0]);
							dataTemp.setSugSeq(viewNo[2]);
						}
						dataTemp.setSugTitle(BaseUtility.null2Blank( request.getParameterValues("nSugTitle")[i] ));
						dataTemp.setBillNum("0");
						dataTemp.setBillTitle(dataTemp.getSugTitle());
						dataTemp.setSugMan(dataTemp.getSugTitle());
						dataTemp.setTimeStamp(BaseUtility.toTimeFormat( request.getParameterValues("nTimeStamp")[i] ));
						dataTemp.setTimeMemb(BaseUtility.null2Blank( request.getParameterValues("nTimeMemb")[i] ));
						dataTemp.setSugPosi(BaseUtility.null2Blank( request.getParameterValues("nSugPosi")[i] ));
						dataTemp.setCreIPv4(BaseUtility.reqIPv4Address( request ));
						dataTemp.setCreUser(BaseUtility.null2Char( searchVO.getCreUser(), memberVO.getUserPID() ));
						dataTemp.setCreName(BaseUtility.null2Char( searchVO.getCreName(), memberVO.getUserName() ));
						dataTemp.setModIPv4(BaseUtility.reqIPv4Address( request ));
						dataTemp.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
						dataTemp.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));

						agendaMngrService.doRtnOfModifyAgendaNAME(dataTemp);
					}
				}

				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReBgnDate((String)model.get("reBgnDate"));
			searchVO.setReEndDate((String)model.get("reEndDate"));
			searchVO.setReConfType((String)model.get("reConfType"));
			searchVO.setReEraCode((String)model.get("reEraCode"));
			searchVO.setReKndCode((String)model.get("reKndCode"));
			searchVO.setReRndCode((String)model.get("reRndCode"));
			searchVO.setReRndDiv((String)model.get("reRndDiv"));
			searchVO.setReMemID((String)model.get("reMemID"));
			searchVO.setSearchText((String)model.get("searchText"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reBgnDate"     , (String)model.get("reBgnDate"));
			mav.addObject("reEndDate"     , (String)model.get("reEndDate"));
			mav.addObject("reConfType"    , (String)model.get("reConfType"));
			mav.addObject("reEraCode"     , (String)model.get("reEraCode"));
			mav.addObject("reKndCode"     , (String)model.get("reKndCode"));
			mav.addObject("reRndCode"     , (String)model.get("reRndCode"));
			mav.addObject("reRndDiv"      , (String)model.get("reRndDiv"));
			mav.addObject("reMemID"       , (String)model.get("reMemID"));
			mav.addObject("searchText"    , (String)model.get("searchText"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/ptnhexa/"+ BaseUtility.toCapitalize( paramPath ) +"/AgendaView.do";
		return mav;
	}	// end of doActOfModifyAgenda

	/**
	 * <pre>
	 * 회의록 내역을 변경처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return String
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/AgendaInitial.do")
	public ModelAndView doActOfInitialAgenda ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseAgendaVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[3]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[3] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseAgendaVO			dataView     = new BaseAgendaVO();
		String[]				deleList     = null;
log.debug(" act: "+ BaseUtility.spaceFill( "Agenda"+ BaseUtility.toCapitalize( paramPath ) +"Initial", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false ) ) return mav;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reConfType"    , searchVO.getReConfType());
		model.addAttribute("reEraCode"     , searchVO.getReEraCode());
		model.addAttribute("reKndCode"     , searchVO.getReKndCode());
		model.addAttribute("reRndCode"     , searchVO.getReRndCode());
		model.addAttribute("reRndDiv"      , searchVO.getReRndDiv());
		model.addAttribute("reMemID"       , searchVO.getReMemID());
		model.addAttribute("searchText"    , searchVO.getSearchText());

		// 추가 조회조건 취득
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReBgnDate(BaseUtility.null2Blank( searchVO.getReBgnDate() ).replaceAll("[^0-9]", ""));
		searchVO.setReEndDate(BaseUtility.null2Blank( searchVO.getReEndDate() ).replaceAll("[^0-9]", ""));
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/agenda/agenda"+ BaseUtility.toCapitalize( paramPath ) +"_edit";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				dataView = (BaseAgendaVO)agendaMngrService.getDetailOfConfBase( searchVO );

				// 회의의안 기본내역 갱신
				dataView.setSugType("S");
				agendaMngrService.doRtnOfInitialAgendaSUG(dataView);
				// 회의의안 추가내역 갱신
				dataView.setSugType("N");
				agendaMngrService.doRtnOfInitialAgendaNAME(dataView);

				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReBgnDate((String)model.get("reBgnDate"));
			searchVO.setReEndDate((String)model.get("reEndDate"));
			searchVO.setReConfType((String)model.get("reConfType"));
			searchVO.setReEraCode((String)model.get("reEraCode"));
			searchVO.setReKndCode((String)model.get("reKndCode"));
			searchVO.setReRndCode((String)model.get("reRndCode"));
			searchVO.setReRndDiv((String)model.get("reRndDiv"));
			searchVO.setReMemID((String)model.get("reMemID"));
			searchVO.setSearchText((String)model.get("searchText"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reBgnDate"     , (String)model.get("reBgnDate"));
			mav.addObject("reEndDate"     , (String)model.get("reEndDate"));
			mav.addObject("reConfType"    , (String)model.get("reConfType"));
			mav.addObject("reEraCode"     , (String)model.get("reEraCode"));
			mav.addObject("reKndCode"     , (String)model.get("reKndCode"));
			mav.addObject("reRndCode"     , (String)model.get("reRndCode"));
			mav.addObject("reRndDiv"      , (String)model.get("reRndDiv"));
			mav.addObject("reMemID"       , (String)model.get("reMemID"));
			mav.addObject("searchText"    , (String)model.get("searchText"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/ptnhexa/"+ BaseUtility.toCapitalize( paramPath ) +"/AgendaView.do";
		return mav;
	}	// end of doActOfInitialAgenda

	/**
	 * <pre>
	 * 회의록 내역을 변경처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  MultipartHttpServletRequest		// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/AgendaInFile.do")
	public ModelAndView doActOfUpdateAgendaFile ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseBoardsVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			final MultipartHttpServletRequest mpRequest, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseAgendaVO			dataView     = new BaseAgendaVO();
		Map<String, MultipartFile> fileMap   = mpRequest == null ? null : mpRequest.getFileMap();
		String					fileVods     = BaseUtility.getFileBase().replaceAll("/datas", "") + DATAS_VODS +"/";
		String					fileName     = "";
log.debug(" act: "+ BaseUtility.spaceFill( "Agenda"+ BaseUtility.toCapitalize( paramPath ) +"Update", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, false ) ) return mav;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reConfType"    , searchVO.getReConfType());
		model.addAttribute("reEraCode"     , searchVO.getReEraCode());
		model.addAttribute("reKndCode"     , searchVO.getReKndCode());
		model.addAttribute("reRndCode"     , searchVO.getReRndCode());
		model.addAttribute("reRndDiv"      , searchVO.getReRndDiv());
		model.addAttribute("reMemID"       , searchVO.getReMemID());
		model.addAttribute("searchText"    , searchVO.getSearchText());

		// 추가 조회조건 취득
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			if( !(fileMap == null || fileMap.size() <= 0) && fileUP ) { 
				for( MultipartFile mFile : fileMap.values() ) { 
					if( !(mFile.isEmpty() || mFile.getSize() < 0) && mFile.getName().contains("fileBase") && !BaseUtility.getFileAllowOfMovie( mFile.getOriginalFilename() ) ) { 
//						model.addAttribute("error"         , "첨부 불가능한 파일이 존재합니다.");
//						return "/cmmn/exe_script";
						mav.addObject("message"       , "첨부 불가능한 파일이 존재합니다.").addObject("isError"       , "T");
						return mav;
					}
				}

				if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
					dataView = (BaseAgendaVO)agendaMngrService.getDetailOfConfBase( searchVO.getRowUUID() );

					if( dataView != null ) { 
						fileName = dataView.getConfCode() +".mp4";
						if( FileProcess.existFile( fileVods + fileName ) ) FileProcess.renameFileB( fileVods, fileName, fileName +"."+ System.currentTimeMillis() ); 
						for( MultipartFile mFile : fileMap.values() ) { 
							mFile.transferTo(new File(fileVods + fileName));
						}
					}
				}
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReBgnDate((String)model.get("reBgnDate"));
			searchVO.setReEndDate((String)model.get("reEndDate"));
			searchVO.setReConfType((String)model.get("reConfType"));
			searchVO.setReEraCode((String)model.get("reEraCode"));
			searchVO.setReKndCode((String)model.get("reKndCode"));
			searchVO.setReRndCode((String)model.get("reRndCode"));
			searchVO.setReRndDiv((String)model.get("reRndDiv"));
			searchVO.setReMemID((String)model.get("reMemID"));
			searchVO.setSearchText((String)model.get("searchText"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reBgnDate"     , (String)model.get("reBgnDate"));
			mav.addObject("reEndDate"     , (String)model.get("reEndDate"));
			mav.addObject("reConfType"    , (String)model.get("reConfType"));
			mav.addObject("reEraCode"     , (String)model.get("reEraCode"));
			mav.addObject("reKndCode"     , (String)model.get("reKndCode"));
			mav.addObject("reRndCode"     , (String)model.get("reRndCode"));
			mav.addObject("reRndDiv"      , (String)model.get("reRndDiv"));
			mav.addObject("reMemID"       , (String)model.get("reMemID"));
			mav.addObject("searchText"    , (String)model.get("searchText"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
//			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

//		return "redirect:/ptnhexa/"+ BaseUtility.toCapitalize( paramPath ) +"/MinutesView.do";
		return mav;
	}	// end of doActOfUpdateMinutesFile


	/**
	 * <pre>
	 * 통합의안 내역을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/AgendaStat.do")
	public String scrStatOfAgenda ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "board/agenda"+ BaseUtility.toCapitalize( paramPath ) +"_stat";

		int						listCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseAgendaVO>		dataList     = new ArrayList<BaseAgendaVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		eraList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		tempList     = new ArrayList<BaseCommonVO>();
		BaseCommonVO			tempView     = new BaseCommonVO();
log.debug(" act: "+ BaseUtility.spaceFill( "Stand"+ BaseUtility.toCapitalize( paramPath ) +"List", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false ) ) return "/cmmn/exe_script";

		if( "base".equalsIgnoreCase(paramPath) ) { 
		} else if( "kind".equalsIgnoreCase(paramPath) ) { 
		} else if( "comm".equalsIgnoreCase(paramPath) ) { 
		}

		// 기본 조회조건 취득
		pageInfo.setTotalRecordCount(1);
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(10);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "N" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reEraCode"     , searchVO.getReEraCode());

		// 추가 조회조건 취득
//		searchVO.setCondCate(BaseUtility.null2Char( searchVO.getCondCate(), "%" ));
//		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("eraCode: "+ searchVO.getReEraCode() +" |stdStat: "+ searchVO.getReStdStat());
		try { 
			if( BaseUtility.isValidNull( searchVO.getReEraCode() ) ) { 
				// 분류구분 취득 - 현재 회의대수 구분
				tempView = (BaseCommonVO)baseCmmnService.getInfoOfClassNOW( "" );
				searchVO.setReEraCode(tempView == null ? BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsCate" ) : tempView.getEraCode());
				model.addAttribute("reEraCode"     , searchVO.getReEraCode());
log.debug("new eraCode: "+ searchVO.getReEraCode());
			}
//			listCnt = (int)agendaMngrService.getCountOfAgenda( searchVO );
//
//			// 조회 건수를 전체선택인 경우
//			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
//				searchVO.setPageCurNo("1");
//				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
//			}
//			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
//			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
//			searchVO.setPageFirNo(Integer.toString(idxFir));
//			searchVO.setPageLstNo(Integer.toString(idxLst));
//			searchVO.setListCnt(Integer.toString(listCnt));
//
//			if( listCnt > 0 ) { 
				if( "base".equalsIgnoreCase(paramPath) ) { 
					dataList = (List<BaseAgendaVO>)agendaMngrService.getStatOfAgendaKind( searchVO );
				} else if( "kind".equalsIgnoreCase(paramPath) ) { 
					dataList = (List<BaseAgendaVO>)agendaMngrService.getStatOfAgendaKind( searchVO );
				} else if( "comm".equalsIgnoreCase(paramPath) ) { 
					dataList = (List<BaseAgendaVO>)agendaMngrService.getStatOfAgendaComm( searchVO );
				}
				// Data Adjustment(Agendas)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					for( int d = 0; d < dataList.size(); d++ ) { 
						dataList.get(d).setCodSpace(BaseUtility.toNumberFormat( dataList.get(d).getCodSpace() ));
						dataList.get(d).setCod0000(BaseUtility.toNumberFormat( dataList.get(d).getCod0000() ));
						dataList.get(d).setCod0010(BaseUtility.toNumberFormat( dataList.get(d).getCod0010() ));
						dataList.get(d).setCod0020(BaseUtility.toNumberFormat( dataList.get(d).getCod0020() ));
						dataList.get(d).setCod0030(BaseUtility.toNumberFormat( dataList.get(d).getCod0030() ));
						dataList.get(d).setCod0040(BaseUtility.toNumberFormat( dataList.get(d).getCod0040() ));
						dataList.get(d).setCod0050(BaseUtility.toNumberFormat( dataList.get(d).getCod0050() ));
						dataList.get(d).setCod0051(BaseUtility.toNumberFormat( dataList.get(d).getCod0051() ));
						dataList.get(d).setCod0052(BaseUtility.toNumberFormat( dataList.get(d).getCod0052() ));
						dataList.get(d).setCod0053(BaseUtility.toNumberFormat( dataList.get(d).getCod0053() ));
						dataList.get(d).setCod0060(BaseUtility.toNumberFormat( dataList.get(d).getCod0060() ));
						dataList.get(d).setCod0070(BaseUtility.toNumberFormat( dataList.get(d).getCod0070() ));
						dataList.get(d).setCod0080(BaseUtility.toNumberFormat( dataList.get(d).getCod0080() ));
						dataList.get(d).setCodNull(BaseUtility.toNumberFormat( dataList.get(d).getCodNull() ));

						if( "xsum".equals(dataList.get(d).getRultCode()) ) { 
							dataList.get(d).setFontColor(" style='color:#00f;font-weight:500;'");
						} else if( "000".equals(dataList.get(d).getRultCode()) ) { 
							dataList.get(d).setFontColor(" style='color:#f00;'");
						} else if( "999".equals(dataList.get(d).getRultCode()) ) { 
							dataList.get(d).setFontColor(" style='color:#f00;'");
						} else { 
							dataList.get(d).setFontColor("");
						}
					}
				}
//
//				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
//				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
//				pageInfo.setTotalRecordCount(listCnt);
//			}

			// 분류구분 취득 - 전체 회의대수 구분
			eraList = (List<BaseCommonVO>)baseCmmnService.getListOfClassALL( "" );

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			model.addAttribute("dataList"      , dataList);
			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (dataList == null ? 0 : dataList.size()) : listCnt ));
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("eraList"       , eraList);
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/"+retPath;
	}	// end of scrStatOfAgenda

	/**
	 * <pre>
	 * 의안통계 내역을 엑셀출력해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/AgendaExportStat.do")
	public View getExportOfAgendaStat ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		int						listCnt      = 0;
		List<BaseAgendaVO>		dataList     = new ArrayList<BaseAgendaVO>();
		List<BaseCommonVO>		tempList     = new ArrayList<BaseCommonVO>();
		BaseCommonVO			tempView     = new BaseCommonVO();

		String					fileName   = "AgendaXLS";
		String					dataName   = "의안통계 내역";
		List<String>			colName    = new ArrayList<String>();
		List<String>			colRegion  = new ArrayList<String>();
		List<String>			colAlign   = new ArrayList<String>();
		List<String[]>			colValue   = new ArrayList<String[]>();
		List<Integer>			colWidth   = new ArrayList<Integer>();
log.debug(" act: "+ BaseUtility.spaceFill( "Agenda"+ BaseUtility.toCapitalize( paramPath ) +"ExportStat", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false ) ) return null;

		if( "base".equalsIgnoreCase(paramPath) ) { 
		} else if( "kind".equalsIgnoreCase(paramPath) ) { 
			dataName = "의안통계(안건종류별) 내역";
		} else if( "comm".equalsIgnoreCase(paramPath) ) { 
			dataName = "의안통계(위원회별) 내역";
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
//		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
//		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
//		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정

		// 추가 조회조건 취득
//		searchVO.setCondCate(BaseUtility.null2Char( searchVO.getCondCate(), "%" ));
//		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("eraCode: "+ searchVO.getReEraCode() +" |stdStat: "+ searchVO.getReStdStat());
		try { 
			if( BaseUtility.isValidNull( searchVO.getReEraCode() ) ) { 
				// 분류구분 취득 - 현재 회의대수 구분
				tempView = (BaseCommonVO)baseCmmnService.getInfoOfClassNOW( "" );
				searchVO.setReEraCode(tempView == null ? BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsCate" ) : tempView.getEraCode());
				model.addAttribute("reEraCode"     , searchVO.getReEraCode());
log.debug("new eraCode: "+ searchVO.getReEraCode());
			}
//			listCnt = (int)agendaMngrService.getCountOfAgenda( searchVO );

//			if( listCnt > 0 ) { 
				colName.clear();
				colRegion.clear();
				colAlign.clear();
				colWidth.clear();
				colValue.clear();

				// 상단제목
				colName.add("구분");
				colName.add("본회의직상");
				colName.add("원안가결");
				colName.add("수정가결");
				colName.add("원안의결");
				colName.add("수정의결");
				colName.add("부분승인");
				colName.add("보류");
				colName.add("미료");
				colName.add("부결");
				colName.add("철회");
				colName.add("계류중");
				colName.add("코드없음");
				colName.add("빈값");
				colName.add("미지정");

				// 행증가,시작열,종료열
				colRegion.add("0`0`0");
				colRegion.add("0`1`1");
				colRegion.add("0`2`2");
				colRegion.add("0`3`3");
				colRegion.add("0`4`4");
				colRegion.add("0`5`5");
				colRegion.add("0`6`6");
				colRegion.add("0`7`7");
				colRegion.add("0`8`8");
				colRegion.add("0`9`9");
				colRegion.add("0`10`10");
				colRegion.add("0`11`11");
				colRegion.add("0`12`12");
				colRegion.add("0`13`13");
				colRegion.add("0`14`14");

				// 내용 가로정렬
				colAlign.add("center");
				colAlign.add("center");
				colAlign.add("center");
				colAlign.add("center");
				colAlign.add("center");
				colAlign.add("center");
				colAlign.add("center");
				colAlign.add("center");
				colAlign.add("center");
				colAlign.add("center");
				colAlign.add("center");
				colAlign.add("center");
				colAlign.add("center");
				colAlign.add("center");
				colAlign.add("center");

				// 열너비
				colWidth.add(225);	// 구분
				colWidth.add(150);	// 본회의직상
				colWidth.add(150);	// 원안가결
				colWidth.add(150);	// 수정가결
				colWidth.add(150);	// 원안의결
				colWidth.add(150);	// 수정의결
				colWidth.add(150);	// 부분승인
				colWidth.add(150);	// 보류
				colWidth.add(150);	// 미료
				colWidth.add(150);	// 부결
				colWidth.add(150);	// 철회
				colWidth.add(150);	// 계류중
				colWidth.add(150);	// 코드없음
				colWidth.add(150);	// 빈값
				colWidth.add(150);	// 미지정


//				searchVO.setPageCurNo("1");
//				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
				if( "base".equalsIgnoreCase(paramPath) ) { 
					dataList = (List<BaseAgendaVO>)agendaMngrService.getStatOfAgendaKind( searchVO );
				} else if( "kind".equalsIgnoreCase(paramPath) ) { 
					dataList = (List<BaseAgendaVO>)agendaMngrService.getStatOfAgendaKind( searchVO );
				} else if( "comm".equalsIgnoreCase(paramPath) ) { 
					dataList = (List<BaseAgendaVO>)agendaMngrService.getStatOfAgendaComm( searchVO );
				}
				// Data Adjustment(Excels)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					for( int d = 0; d < dataList.size(); d++ ) { 
						String[] array = {dataList.get(d).getRultName() 
										, BaseUtility.toNumberFormat( dataList.get(d).getCod0010() ) 
										, BaseUtility.toNumberFormat( dataList.get(d).getCod0030() ) 
										, BaseUtility.toNumberFormat( dataList.get(d).getCod0050() ) 
										, BaseUtility.toNumberFormat( dataList.get(d).getCod0020() ) 
										, BaseUtility.toNumberFormat( dataList.get(d).getCod0040() ) 
										, BaseUtility.toNumberFormat( dataList.get(d).getCod0051() ) 
										, BaseUtility.toNumberFormat( dataList.get(d).getCod0052() ) 
										, BaseUtility.toNumberFormat( dataList.get(d).getCod0052() ) 
										, BaseUtility.toNumberFormat( dataList.get(d).getCod0060() ) 
										, BaseUtility.toNumberFormat( dataList.get(d).getCod0070() ) 
										, BaseUtility.toNumberFormat( dataList.get(d).getCod0080() ) 
										, BaseUtility.toNumberFormat( dataList.get(d).getCod0000() ) 
										, BaseUtility.toNumberFormat( dataList.get(d).getCodSpace() ) 
										, BaseUtility.toNumberFormat( dataList.get(d).getCodNull() ) 
						};
						colValue.add(array);
					}
				}

				model.put("relFile"     , fileName);
				model.put("absFile"     , dataName);
				model.put("titLine"     , "2");
				model.put("colName"     , colName);
				model.put("colRegion"   , colRegion);
				model.put("colAlign"    , colAlign);
				model.put("colWidth"    , colWidth);
				model.put("colValue"    , colValue);
//			}
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return new ExcelBuilderPOI();
	}	// end of getExportOfAgenda

	/**
	 * <pre>
	 * 회의록 정보을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/MinutesList.do")
	public String scrListOfMinutes ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "agenda/minutes"+ BaseUtility.toCapitalize( paramPath ) +"_list";

		int						listCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseAgendaVO>		dataList     = new ArrayList<BaseAgendaVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		eraList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		kndList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		rndList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		memList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		pntList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		tempList     = new ArrayList<BaseCommonVO>();
		BaseCommonVO			tempView     = new BaseCommonVO();
		List<String>			rndCodes     = new ArrayList<String>();
log.debug(" act: "+ BaseUtility.spaceFill( "Minutes"+ BaseUtility.toCapitalize( paramPath ) +"List", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false ) ) return "/cmmn/exe_script";

		// 기본 조회조건 취득
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(10);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "N" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reConfType"    , searchVO.getReConfType());
		model.addAttribute("reEraCode"     , searchVO.getReEraCode());
		model.addAttribute("reKndCode"     , searchVO.getReKndCode());
		model.addAttribute("reRndCode"     , searchVO.getReRndCode());
		model.addAttribute("reRndDiv"      , searchVO.getReRndDiv());
		model.addAttribute("reMemID"       , searchVO.getReMemID());
		model.addAttribute("searchText"    , searchVO.getSearchText());

		// 추가 조회조건 취득
//		searchVO.setCondCate(BaseUtility.null2Char( searchVO.getCondCate(), "%" ));
//		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReBgnDate(BaseUtility.null2Blank( searchVO.getReBgnDate() ).replaceAll("[^0-9]", ""));
		searchVO.setReEndDate(BaseUtility.null2Blank( searchVO.getReEndDate() ).replaceAll("[^0-9]", ""));

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("bgnDate: "+ searchVO.getReBgnDate() +" |endDate: "+ searchVO.getReEndDate());
log.debug("eraCode: "+ searchVO.getReEraCode() +" |kndCode: "+ searchVO.getReKndCode() +" |rndCode: "+ searchVO.getReRndCode() +" |rndDiv: "+ searchVO.getReRndDiv());
log.debug("confType: "+ searchVO.getReConfType() +" |memID: "+ searchVO.getReMemID() +" |searchText: "+ searchVO.getSearchText());
		try { 
			listCnt = (int)agendaMngrService.getCountOfConfBase( searchVO );

			// 조회 건수를 전체선택인 경우
			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
			}
			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
			searchVO.setPageFirNo(Integer.toString(idxFir));
			searchVO.setPageLstNo(Integer.toString(idxLst));
			searchVO.setListCnt(Integer.toString(listCnt));

			if( listCnt > 0 ) { 
				dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfBase( searchVO );
//				dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfBase( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				// Data Adjustment(Minutes)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					for( int d = 0; d < dataList.size(); d++ ) { 
						dataList.get(d).setViewNo(dataList.get(d).getConfUUID() + dataList.get(d).getRowSeq());

						dataList.get(d).setConfDate(BaseUtility.toDateFormat( dataList.get(d).getConfDate(), dtSep ));
						dataList.get(d).setConfStartTime(BaseUtility.toTimeFormat( dataList.get(d).getConfStartTime() ));
						dataList.get(d).setConfCloseTime(BaseUtility.toTimeFormat( dataList.get(d).getConfCloseTime() ));
						dataList.get(d).setConfReqTime(BaseUtility.toTimeFormat( dataList.get(d).getConfReqTime() ));
						dataList.get(d).setConfWeek(BaseUtility.date2Week( dataList.get(d).getConfDate() ));
					}
				}

				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
				pageInfo.setTotalRecordCount(listCnt);
			}

			// 분류구분 취득 - 전체 회의대수 구분
			eraList = (List<BaseCommonVO>)baseCmmnService.getListOfClassALL( "" );
			// 분류구분 취득 - 회의회수 구분
			rndList = (List<BaseCommonVO>)baseCmmnService.getListOfRoundALL( searchVO.getReEraCode() );
			// 분류구분 취득 - 희의분류 구분
			kndList = (List<BaseCommonVO>)baseCmmnService.getListOfGroup( "" );
			// 분류구분 취득 - 위원명
			memList = (List<BaseCommonVO>)baseCmmnService.getListOfMembALL( searchVO.getReEraCode() );

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			model.addAttribute("dataList"      , dataList);
			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (dataList == null ? 0 : dataList.size()) : listCnt ));
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("eraList"       , eraList);
			model.addAttribute("kndList"       , kndList);
			model.addAttribute("rndList"       , rndList);
			model.addAttribute("pntList"       , pntList);
			model.addAttribute("memList"       , memList);
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/"+retPath;
	}	// end of scrListOfMinutes

	/**
	 * <pre>
	 * 회의록 내역을(를) 상세조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/MinutesView.do")
	public String scrViewOfMinutes ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "agenda/minutes"+ BaseUtility.toCapitalize( paramPath ) +"_view";

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[3]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[3] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		BaseAgendaVO			dataView     = new BaseAgendaVO();
		BaseAgendaVO			prevView     = new BaseAgendaVO();
		BaseAgendaVO			nextView     = new BaseAgendaVO();
		BaseCommonVO			findFile     = new BaseCommonVO();
		List<BaseFilesVO>		dataFile     = new ArrayList<BaseFilesVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();

		String					fileHtml     = BaseUtility.getFileBase().replaceAll("/datas", "") + DATAS_HTML +"/";
		String					fontSize     = "11";
		String					fontKind     = "굴림";
		String					fontLine     = "160";
		String					fontLink     = "blue";
		StringBuffer			confStyle    = new StringBuffer();
		StringBuffer			confView     = new StringBuffer();
log.debug(" act: "+ BaseUtility.spaceFill( "Minutes"+ BaseUtility.toCapitalize( paramPath ) +"View", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false, searchVO.getViewNo() ) ) return "/cmmn/exe_script";

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reConfType"    , searchVO.getReConfType());
		model.addAttribute("reEraCode"     , searchVO.getReEraCode());
		model.addAttribute("reKndCode"     , searchVO.getReKndCode());
		model.addAttribute("reRndCode"     , searchVO.getReRndCode());
		model.addAttribute("reRndDiv"      , searchVO.getReRndDiv());
		model.addAttribute("reMemID"       , searchVO.getReMemID());
		model.addAttribute("searchText"    , searchVO.getSearchText());

		// 추가 조회조건 취득
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				dataView = (BaseAgendaVO)agendaMngrService.getDetailOfConfBase( searchVO );
				if( dataView != null ) { 
					findFile.setLnkName(lnkName);
					findFile.setLnkKey(dataView.getConfCode() + dataView.getConfSeq());
					// Data Adjustment(Data Files)
					dataFile = (List<BaseFilesVO>)baseCmmnService.getListOfDataFile( findFile );

					// Data Adjustment(Agendas)
					dataView.setActMode("Update");
					dataView.setViewNo(dataView.getConfUUID());

					dataView.setConfDate(BaseUtility.toDateFormat( dataView.getConfDate(), dtSep, 8 ));
					dataView.setConfStartTime(BaseUtility.toTimeFormat( dataView.getConfStartTime() ));
					dataView.setConfCloseTime(BaseUtility.toTimeFormat( dataView.getConfCloseTime() ));
					dataView.setConfReqTime(BaseUtility.toTimeFormat( dataView.getConfReqTime() ));

					// 회의록 본문
					//	fSize:11, fLine:160,fKind:굴림
					//	span{font-size:11pt;line-height:160%;font-family:굴림;}
					//	.Title{font-size:26pt;line-height:160%;font-family:굴림;font-weight:bold;text-align:center;}
					//	.Sub{font-size:16pt;line-height:160%;font-family:굴림;font-weight:bold;text-align:right;}
					//	.Sug{font-size:13pt;line-height:160%;font-family:굴림;font-weight:bold;text-align:right;}
					//	.HStyle0{text-indent:0.000pt;margin-right:0.000pt;margin-top:0.000pt;margin-bottom:0.000pt;}
					confStyle= new StringBuffer();
					confStyle.append("\n\tspan{font-size:"+ fontSize +"pt;line-height:"+ fontLine +"%;font-family:"+ fontKind +";}");
					confStyle.append("\n\t.Title{font-size:"+ String.valueOf(Integer.parseInt(fontSize)+15) +"pt;line-height:"+ fontLine +"%;font-family:"+ fontKind +";font-weight:bold;text-align:center;}");
					confStyle.append("\n\t.Sub{font-size:"+ String.valueOf(Integer.parseInt(fontSize)+5) +"pt;line-height:"+ fontLine +"%;font-family:"+ fontKind +";font-weight:bold;text-align:right;}");
					confStyle.append("\n\t.Sug{font-size:"+ String.valueOf(Integer.parseInt(fontSize)+2) +"pt;line-height:"+ fontLine +"%;font-family:"+ fontKind +";font-weight:bold;text-align:right;}");
					confStyle.append("\n\tA:link{color:"+ fontLink +";text-decoration:none;}");
					confStyle.append("\n\tA:active{color:blue;text-decoration:none;}");
					confStyle.append("\n\tA:visited{color:"+ fontLink +";text-decoration:none;}");
					confStyle.append("\n\tA:hover{color:red;text-decoration:underline;}");
					confStyle.append("\n\tHR{color:#aee0b3;display:block !important;}");
					confStyle.append("\n\t.HStyle0{text-indent:0.000pt;margin-right:0.000pt;margin-top:0.000pt;margin-bottom:0.000pt;}");
					confStyle.append("\n\t");
					confView = new StringBuffer();
					confView.append(FileProcess.getReadFile("euc-kr", fileHtml + dataView.getConfCode() +".html"));
				}
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("dataFile"      , dataFile);
			model.addAttribute("confStyle"     , confStyle);
			model.addAttribute("confNView"     , BaseUtility.tag2Sym( confView.toString() ));
			model.addAttribute("confView"      , confView.toString());
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/"+retPath;
	}	// end of scrViewOfMinutes

	/**
	 * <pre>
	 * 회의록 내역을 변경처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/MinutesUpdate.do")
	public ModelAndView doActOfUpdateMinutes ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseBoardsVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseAgendaVO			dataView     = new BaseAgendaVO();

		String					fileHtml     = BaseUtility.getFileBase().replaceAll("/datas", "") + DATAS_HTML +"/";
log.debug(" act: "+ BaseUtility.spaceFill( "Minutes"+ BaseUtility.toCapitalize( paramPath ) +"Update", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, false ) ) return mav;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reConfType"    , searchVO.getReConfType());
		model.addAttribute("reEraCode"     , searchVO.getReEraCode());
		model.addAttribute("reKndCode"     , searchVO.getReKndCode());
		model.addAttribute("reRndCode"     , searchVO.getReRndCode());
		model.addAttribute("reRndDiv"      , searchVO.getReRndDiv());
		model.addAttribute("reMemID"       , searchVO.getReMemID());
		model.addAttribute("searchText"    , searchVO.getSearchText());

		// 추가 조회조건 취득
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/board/note"+ BaseUtility.toCapitalize( paramPath ) +"_edit";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				dataView = (BaseAgendaVO)agendaMngrService.getDetailOfConfBase( searchVO.getRowUUID() );
				searchVO.setContDesc(searchVO.getContDesc().replaceAll("&lt;","<"));
				searchVO.setContDesc(searchVO.getContDesc().replaceAll("&gt;",">"));
				searchVO.setContDesc(searchVO.getContDesc().replaceAll("&amp;","&"));
				searchVO.setContDesc(searchVO.getContDesc().replaceAll("&quot;","\""));
				searchVO.setContDesc(searchVO.getContDesc().replaceAll("&apos;","\'"));

				if( dataView != null ) { 
					if( FileProcess.renameFileB( fileHtml, dataView.getConfCode() +".html", dataView.getConfCode() +".html."+ System.currentTimeMillis() ) ) { 
						BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileHtml + dataView.getConfCode() +".html"), "euc-kr"));
						try { 
							writer.write(searchVO.getContDesc());
							writer.flush();
							writer.close();
						} catch( IOException IOE ) { 
							log.debug(IOE.getMessage());
						} finally { 
							try { 
								if(writer != null) writer.close();
							} catch( IOException IOE ) { 
								log.debug(IOE.getMessage());
							}
						}
					}
				}
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReBgnDate((String)model.get("reBgnDate"));
			searchVO.setReEndDate((String)model.get("reEndDate"));
			searchVO.setReConfType((String)model.get("reConfType"));
			searchVO.setReEraCode((String)model.get("reEraCode"));
			searchVO.setReKndCode((String)model.get("reKndCode"));
			searchVO.setReRndCode((String)model.get("reRndCode"));
			searchVO.setReRndDiv((String)model.get("reRndDiv"));
			searchVO.setReMemID((String)model.get("reMemID"));
			searchVO.setSearchText((String)model.get("searchText"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reBgnDate"     , (String)model.get("reBgnDate"));
			mav.addObject("reEndDate"     , (String)model.get("reEndDate"));
			mav.addObject("reConfType"    , (String)model.get("reConfType"));
			mav.addObject("reEraCode"     , (String)model.get("reEraCode"));
			mav.addObject("reKndCode"     , (String)model.get("reKndCode"));
			mav.addObject("reRndCode"     , (String)model.get("reRndCode"));
			mav.addObject("reRndDiv"      , (String)model.get("reRndDiv"));
			mav.addObject("reMemID"       , (String)model.get("reMemID"));
			mav.addObject("searchText"    , (String)model.get("searchText"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/ptnhexa/"+ BaseUtility.toCapitalize( paramPath ) +"/MinutesList.do";
		return mav;
	}	// end of doActOfModifyMinutes

	/**
	 * <pre>
	 * 회의록 내역을 변경처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@SuppressWarnings({"unchecked","rawtypes"})
	@RequestMapping(value="/MinutesModify.do")
	public ModelAndView doActOfModifyMinutes ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseBoardsVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseBoardsVO			dataView     = new BaseBoardsVO();
		ArrayList<String>		valsList     = new ArrayList<String>();
		HashMap					paramMap     = new HashMap();
log.debug(" act: "+ BaseUtility.spaceFill( "Minutes"+ BaseUtility.toCapitalize( paramPath ) +"Modify", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, false ) ) return mav;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reConfType"    , searchVO.getReConfType());
		model.addAttribute("reEraCode"     , searchVO.getReEraCode());
		model.addAttribute("reKndCode"     , searchVO.getReKndCode());
		model.addAttribute("reRndCode"     , searchVO.getReRndCode());
		model.addAttribute("reRndDiv"      , searchVO.getReRndDiv());
		model.addAttribute("reMemID"       , searchVO.getReMemID());
		model.addAttribute("searchText"    , searchVO.getSearchText());

		// 추가 조회조건 취득
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/board/note"+ BaseUtility.toCapitalize( paramPath ) +"_edit";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( request.getParameterValues("rows") ) ) { 
				valsList = new ArrayList<String>();
				for( int i = 0; i < request.getParameterValues("rows").length; i++ ) { 
					valsList.add(request.getParameterValues("rows")[i]);
				}
				paramMap = new HashMap();
				paramMap.put("tempFlag"      , "T".equals(BaseUtility.null2Blank( searchVO.getRultCode() ))?"T":null);
				paramMap.put("valsList"      , valsList);
				agendaMngrService.doRtnOfModifyConfBase(paramMap);
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReBgnDate((String)model.get("reBgnDate"));
			searchVO.setReEndDate((String)model.get("reEndDate"));
			searchVO.setReConfType((String)model.get("reConfType"));
			searchVO.setReEraCode((String)model.get("reEraCode"));
			searchVO.setReKndCode((String)model.get("reKndCode"));
			searchVO.setReRndCode((String)model.get("reRndCode"));
			searchVO.setReRndDiv((String)model.get("reRndDiv"));
			searchVO.setReMemID((String)model.get("reMemID"));
			searchVO.setSearchText((String)model.get("searchText"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reBgnDate"     , (String)model.get("reBgnDate"));
			mav.addObject("reEndDate"     , (String)model.get("reEndDate"));
			mav.addObject("reConfType"    , (String)model.get("reConfType"));
			mav.addObject("reEraCode"     , (String)model.get("reEraCode"));
			mav.addObject("reKndCode"     , (String)model.get("reKndCode"));
			mav.addObject("reRndCode"     , (String)model.get("reRndCode"));
			mav.addObject("reRndDiv"      , (String)model.get("reRndDiv"));
			mav.addObject("reMemID"       , (String)model.get("reMemID"));
			mav.addObject("searchText"    , (String)model.get("searchText"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/ptnhexa/"+ BaseUtility.toCapitalize( paramPath ) +"/MinutesList.do";
		return mav;
	}	// end of doActOfModifyMinutes

	/**
	 * <pre>
	 * 회의록 내역을 변경처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  MultipartHttpServletRequest		// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/MinutesInFile.do")
	public ModelAndView doActOfUpdateMinutesFile ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseBoardsVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			final MultipartHttpServletRequest mpRequest, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseAgendaVO			dataView     = new BaseAgendaVO();
		Map<String, MultipartFile> fileMap   = mpRequest == null ? null : mpRequest.getFileMap();
		int						fileCnt      = 0;
		int						thmbCnt      = 0;
		String[]				deleList     = null;
		BaseFilesVO				fileDele     = new BaseFilesVO();
log.debug(" act: "+ BaseUtility.spaceFill( "Minutes"+ BaseUtility.toCapitalize( paramPath ) +"Update", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, false ) ) return mav;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reConfType"    , searchVO.getReConfType());
		model.addAttribute("reEraCode"     , searchVO.getReEraCode());
		model.addAttribute("reKndCode"     , searchVO.getReKndCode());
		model.addAttribute("reRndCode"     , searchVO.getReRndCode());
		model.addAttribute("reRndDiv"      , searchVO.getReRndDiv());
		model.addAttribute("reMemID"       , searchVO.getReMemID());
		model.addAttribute("searchText"    , searchVO.getSearchText());

		// 추가 조회조건 취득
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/board/note"+ BaseUtility.toCapitalize( paramPath ) +"_edit";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !(fileMap == null || fileMap.size() <= 0) && fileUP ) for( MultipartFile mFile : fileMap.values() ) { 
				if( !(mFile.isEmpty() || mFile.getSize() < 0) && mFile.getName().contains("fileBase") && !BaseUtility.getFileAllowOfDocuImage( mFile.getOriginalFilename() ) ) { 
//					model.addAttribute("error"         , "첨부 불가능한 파일이 존재합니다.");
//					return "/cmmn/exe_script";
					mav.addObject("message"       , "첨부 불가능한 파일이 존재합니다.").addObject("isError"       , "T");
					return mav;
				}
			}

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				dataView = (BaseAgendaVO)agendaMngrService.getDetailOfConfBase( searchVO.getRowUUID() );

				if( dataView != null ) { 
					// 첨부파일 삭제처리
					if( fileUP ) { 
						deleList = BaseUtility.isValidNull( searchVO.getCondDele() ) ? null : BaseUtility.null2Blank( searchVO.getCondDele() ).split("`");
						if( !(deleList == null || deleList.length <= 0) ) { 
							for( int f = 0; f < deleList.length; f++ ) { 
								fileDele = new BaseFilesVO();
								fileDele.setRowUUID(deleList[f]);
								fileDele.setDelIPv4(BaseUtility.reqIPv4Address( request ));
								fileDele.setDelUser(BaseUtility.null2Char( searchVO.getDelUser(), memberVO.getUserPID() ));
								fileDele.setDelName(BaseUtility.null2Char( searchVO.getDelName(), memberVO.getUserName() ));

								baseCmmnService.doRtnOfDeleteDataFile( fileDele );
								fileCnt--;
							}
						}
					}

					// 첨부파일 갱신처리
					if( fileUP ) fileCnt = FileProcess.doActOfCreateFileBase( request, response, baseCmmnService, systemVO, memberVO, fileMap, fileCnt, filePath, lnkName, dataView.getConfCode() + dataView.getConfSeq() );
				}
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReBgnDate((String)model.get("reBgnDate"));
			searchVO.setReEndDate((String)model.get("reEndDate"));
			searchVO.setReConfType((String)model.get("reConfType"));
			searchVO.setReEraCode((String)model.get("reEraCode"));
			searchVO.setReKndCode((String)model.get("reKndCode"));
			searchVO.setReRndCode((String)model.get("reRndCode"));
			searchVO.setReRndDiv((String)model.get("reRndDiv"));
			searchVO.setReMemID((String)model.get("reMemID"));
			searchVO.setSearchText((String)model.get("searchText"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reBgnDate"     , (String)model.get("reBgnDate"));
			mav.addObject("reEndDate"     , (String)model.get("reEndDate"));
			mav.addObject("reConfType"    , (String)model.get("reConfType"));
			mav.addObject("reEraCode"     , (String)model.get("reEraCode"));
			mav.addObject("reKndCode"     , (String)model.get("reKndCode"));
			mav.addObject("reRndCode"     , (String)model.get("reRndCode"));
			mav.addObject("reRndDiv"      , (String)model.get("reRndDiv"));
			mav.addObject("reMemID"       , (String)model.get("reMemID"));
			mav.addObject("searchText"    , (String)model.get("searchText"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/ptnhexa/"+ BaseUtility.toCapitalize( paramPath ) +"/MinutesView.do";
		return mav;
	}	// end of doActOfUpdateMinutesFile

//	@ExceptionHandler(MaxUploadSizeExceededException.class)
	public ModelAndView resolveException(HttpServletRequest request,HttpServletResponse response,Object handler,Exception exception){
		ModelAndView			mav          = new ModelAndView("jsonView");
		if(exception instanceof MaxUploadSizeExceededException)
			mav.addObject("message"       , "첨부파일 사이즈는 "+ BaseUtility.toNumberFormat( (((MaxUploadSizeExceededException)exception).getMaxUploadSize()/1024/1024) ) +"MB를 초과할 수 없습니다.").addObject("isError"       , "T");
		return mav;
	}

}	// end of class