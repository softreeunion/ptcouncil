/*******************************************************************************
  Program ID  : MainMngrController
  Description : 클라이언트의 요청(공통)을 처리할 메서드를 구현
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.main.web;

import egovframework.com.cmm.util.BaseUtility;
import egovframework.ptcc.cmmn.util.BaseCategory;
import egovframework.ptcc.cmmn.util.UserDetailsHelper;
import egovframework.ptcc.cmmn.vo.BaseCommonVO;
import egovframework.ptcc.cmmn.vo.BaseMemberVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.security.*;
import java.security.spec.RSAPublicKeySpec;

@Controller
@RequestMapping(value="/webdata")
public class MainWdataController {

	@Autowired
	private WebApplicationContext			webContext;

	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));

	private String							baseServ;
	private String							RSA_WEB_KEY  = "__rsaPrivateKey__";	// 개인키 session key
	private String							RSA_INSTANCE = "RSA";				// rsa transformation

	private void initRSA ( 
			HttpServletRequest request 
		) {

		HttpSession				session      = request == null ? null : request.getSession();
		KeyPairGenerator		generator;

		try { 
			generator = KeyPairGenerator.getInstance(this.RSA_INSTANCE);
			generator.initialize(1024);

			KeyPair keyPair = generator.genKeyPair();
			KeyFactory keyFactory = KeyFactory.getInstance(this.RSA_INSTANCE);

			PublicKey publicKey = keyPair.getPublic();
			PrivateKey privateKey = keyPair.getPrivate();

			// 세션에 공개키의 문자열을 키로하여 개인키를 저장한다.
			session.setAttribute(this.RSA_WEB_KEY, privateKey);

			// 공개키를 문자열로 변환하여 JavaScript RSA 라이브러리 넘겨준다.
			RSAPublicKeySpec publicSpec = (RSAPublicKeySpec) keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);
			String publicKeyModulus = publicSpec.getModulus().toString(16);
			String publicKeyExponent = publicSpec.getPublicExponent().toString(16);
			request.setAttribute("RSAModulus", publicKeyModulus); // rsa modulus 를 request 에 추가
			request.setAttribute("RSAExponent", publicKeyExponent); // rsa exponent 를 request 에 추가
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		}
	}


	/**
	 * <pre>
	 * 관리자 메인화면을(를) 조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value={"","/","/index.do"})
	public String scrInitOfMain ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseMemberVO			memberVO     = new BaseMemberVO();
		Boolean					isLogin      = UserDetailsHelper.isAuthenticatedAdmin();

		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			model.addAttribute("close"         , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return "/cmmn/exe_script";
		}
		if( isLogin ) { 
			memberVO = (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin();

			if( memberVO == null ) { 
				this.initRSA(request);
				baseServ = "/wdata/index";
			} else {
				String userComm = memberVO.getUserComm();
				if(userComm == null) {
					baseServ = "redirect:/webdata/base/NoticeList.do";
				} else {
					String userCommTemp = userComm.substring(0,2);
					if ("00".equals(userCommTemp)) {
						baseServ = "redirect:/webdata/chair/NoticeList.do";
					} else if ("01".equals(userCommTemp)) {
						baseServ = "redirect:/webdata/oper/NoticeList.do";
					} else if ("02".equals(userCommTemp)) {
						baseServ = "redirect:/webdata/gvadm/NoticeList.do";
					} else if ("03".equals(userCommTemp)) {
						baseServ = "redirect:/webdata/welfare/NoticeList.do";
					} else if ("04".equals(userCommTemp)) {
						baseServ = "redirect:/webdata/build/NoticeList.do";
					} else if ("06".equals(userCommTemp)) {
						baseServ = "redirect:/webdata/base/NoticeList.do";
					} else if ("07".equals(userCommTemp)) {
						baseServ = "redirect:/webdata/proc/NoticeList.do";
					} else if ("08".equals(userCommTemp)) {
						baseServ = "redirect:/webdata/prom/NoticeList.do";
					} else if ("09".equals(userCommTemp)) {
						baseServ = "redirect:/webdata/supp/NoticeList.do";
					} else {
						baseServ = "redirect:/webdata/base/NoticeList.do";
					}
				}
			}
		} else { 
			this.initRSA(request);
			baseServ = "/wdata/index";
		}

		return baseServ;
	}	// end of scrInitOfMain

}	// end of class