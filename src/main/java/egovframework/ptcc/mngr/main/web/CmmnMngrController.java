/*******************************************************************************
  Program ID  : CmmnMngrController
  Description : 클라이언트의 요청(공통)을 처리할 메서드를 구현
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.main.web;

import javax.annotation.Resource;
import javax.servlet.http.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.member.service.*;

@Controller
@RequestMapping(value="/ptnhexa")
public class CmmnMngrController { 

	@Autowired
	private WebApplicationContext			webContext;

	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="userMngrService")
	protected UserMngrService				userMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));


	/**
	 * <pre>
	 * 로그아웃 처리를 해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/huout_proc.do")
	public String doLogoutProcess ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		String					returnMSG    = "로그아웃 되었습니다.";

//		request.getSession().invalidate(); 
		request.getSession().removeAttribute("MemRInfo"); 

		model.addAttribute("message"       , returnMSG);
		model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");

		return "/cmmn/exe_script";
	}	// end of doLogoutProcess

	/**
	 * <pre>
	 * 사용자 내역 중 중복내역을 점검해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/User/MemberFound.do",produces="application/json")  // consumes="application/json" // produces="application/json"
	public ModelAndView getFoundOfMember ( 
			@ModelAttribute("searchVO")BaseMemberVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();

		int						listCnt      = 0;

log.debug("userPNC: "+ searchVO.getUserPNC() +" |userPNO: "+ searchVO.getUserPNO());
		try { 
			listCnt = (int)userMngrService.getCheckOfStaff( searchVO.getUserPNO() );

			model.addAttribute("isExist"       , listCnt <= 0 ? "F" : "T");
			model.addAttribute("isResult"      , listCnt <= 0 ? ""  : searchVO.getUserPNO());

			// 최종 결과기록
			mav.addObject("isExist"       , (String)model.get("isExist"));
			mav.addObject("isResult"      , (String)model.get("isResult"));
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return mav;
	}	// end of getFoundOfMember

}	// end of class