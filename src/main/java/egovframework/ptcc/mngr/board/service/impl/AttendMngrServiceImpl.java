/*******************************************************************************
  Program ID  : AttendMngrServiceImpl
  Description : 방청신청 관리에 관한 Service Implement Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.10.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.board.service.impl;

import java.util.*;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.board.service.*;

@Service("attendMngrService")
public class AttendMngrServiceImpl extends egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl implements AttendMngrService { 

	@Resource(name="attendMngrDAO")
	private AttendMngrDAO					attendMngrDAO;

	@Override
	public String getInfoOfAttendUUID(String paramVal) throws Exception { 
		return (String)attendMngrDAO.getInfoOfAttendUUID( paramVal );
	}
	@Override
	public int getCountOfAttend(BaseCommonVO paramObj) throws Exception { 
		return (int)attendMngrDAO.getCountOfAttend( paramObj );
	}
	@Override
	public List<BaseAttendVO> getListOfAttend(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAttendVO>)attendMngrDAO.getListOfAttend( paramObj );
	}
	@Override
	public List<BaseAttendVO> getListOfAttend(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseAttendVO>)attendMngrDAO.getListOfAttend( paramObj, pageIndex, pageSize );
	}
	@Override
	public BaseAttendVO getDetailOfAttend(BaseCommonVO paramObj) throws Exception { 
		return (BaseAttendVO)attendMngrDAO.getDetailOfAttend( paramObj );
	}
	@Override
	public BaseAttendVO getDetailOfAttend(String paramVal) throws Exception { 
		return (BaseAttendVO)attendMngrDAO.getDetailOfAttend( paramVal );
	}
	@Override
	public BaseAttendVO getInfoOfAttendSeq(BaseAttendVO paramObj) throws Exception { 
		return (BaseAttendVO)attendMngrDAO.getInfoOfAttendSeq( paramObj );
	}
	@Override
	public boolean doRtnOfCreateAttend(BaseAttendVO paramObj) throws Exception { 
		return (boolean)attendMngrDAO.doRtnOfCreateAttend( paramObj );
	}
	@Override
	public boolean doRtnOfUpdateAttend(BaseAttendVO paramObj) throws Exception { 
		return (boolean)attendMngrDAO.doRtnOfUpdateAttend( paramObj );
	}
	@Override
	public boolean doRtnOfDeleteAttend(BaseAttendVO paramObj) throws Exception { 
		return (boolean)attendMngrDAO.doRtnOfDeleteAttend( paramObj );
	}
	@Override
	public boolean doRtnOfModifyAttend(BaseAttendVO paramObj) throws Exception { 
		return (boolean)attendMngrDAO.doRtnOfModifyAttend( paramObj );
	}

}	// end of class