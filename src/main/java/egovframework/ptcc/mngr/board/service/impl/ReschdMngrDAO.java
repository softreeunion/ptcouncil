/*******************************************************************************
  Program ID  : ReschdMngrDAO
  Description : 통합일정 정보에 대한 Data Access Object Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.10.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.board.service.impl;

import java.util.*;

import org.springframework.stereotype.Repository;

import egovframework.ptcc.cmmn.vo.*;

@Repository("reschdMngrDAO")
public class ReschdMngrDAO extends egovframework.rte.psl.dataaccess.EgovAbstractDAO { 

	/**
	 * <pre>
	 * 통합일정 UUID을(를) 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return String
	 * @throws Exception
	 */
	public String getInfoOfReschdUUID(String paramVal) throws Exception { 
		return (String)select("ptcouncil.new.reschd.ptSelectReschdUUID",paramVal);
	}	// end of getInfoOfReschdUUID

	/**
	 * <pre>
	 * 통합일정 목록건수을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return Integer
	 * @throws Exception
	 */
	public int getCountOfReschd(BaseCommonVO paramObj) throws Exception { 
		return (Integer)select("ptcouncil.new.reschd.ptSelectReschdCount",paramObj);
	}	// end of getCountOfReschd

	/**
	 * <pre>
	 * 통합일정 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseReschdVO> getListOfReschd(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseReschdVO>)list("ptcouncil.new.reschd.ptSelectReschdList",paramObj);
	}	// end of getListOfReschd

	/**
	 * <pre>
	 * 통합일정 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @param  String							// 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseReschdVO> getListOfReschd(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseReschdVO>)listWithPaging("ptcouncil.new.reschd.ptSelectReschdList",paramObj,Integer.parseInt(pageIndex)-1,Integer.parseInt(pageSize));
	}	// end of getListOfReschd

	/**
	 * <pre>
	 * 통합일정 상세내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseReschdVO
	 * @throws Exception
	 */
	public BaseReschdVO getDetailOfReschd(BaseCommonVO paramObj) throws Exception { 
		return (BaseReschdVO)select("ptcouncil.new.reschd.ptSelectReschdDetail",paramObj);
	}	// end of getDetailOfReschd

	/**
	 * <pre>
	 * 통합일정 상세내역(UUID)을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return BaseReschdVO
	 * @throws Exception
	 */
	public BaseReschdVO getDetailOfReschd(String paramVal) throws Exception { 
		return (BaseReschdVO)select("ptcouncil.new.reschd.ptSelectReschdDetailUUID",paramVal);
	}	// end of getDetailOfReschd

	/**
	 * <pre>
	 * 통합일정 일련번호을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseReschdVO						// 
	 * @return BaseReschdVO
	 * @throws Exception
	 */
	public BaseReschdVO getInfoOfReschdSeq(BaseReschdVO paramObj) throws Exception { 
		return (BaseReschdVO)select("ptcouncil.new.reschd.ptSelectReschdSeq",paramObj);
	}	// end of getInfoOfReschdSeq

	/**
	 * <pre>
	 * 통합일정 내역을(를) 등록 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseReschdVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfCreateReschd(BaseReschdVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.reschd.ptInsertReschd",paramObj)<=0);
	}	// end of doActOfCreateReschd

	/**
	 * <pre>
	 * 통합일정 내역을(를) 변경 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseReschdVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfUpdateReschd(BaseReschdVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.reschd.ptUpdateReschd",paramObj)<=0);
	}	// end of doActOfUpdateReschd

	/**
	 * <pre>
	 * 통합일정 내역을(를) 삭제 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseReschdVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfDeleteReschd(BaseReschdVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.reschd.ptDeleteReschd",paramObj)<=0);
	}	// end of doActOfDeleteReschd

	/**
	 * <pre>
	 * 통합일정 내역을(를) 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseReschdVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfModifyReschd(BaseReschdVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.reschd.ptModifyReschd",paramObj)<=0);
	}	// end of doActOfModifyReschd


	/**
	 * <pre>
	 * 통합일정 달력 목록건수을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return Integer
	 * @throws Exception
	 */
	public int getCountOfReschdCal(BaseCommonVO paramObj) throws Exception { 
		return (Integer)select("ptcouncil.new.reschd.ptSelectReschdCalCount",paramObj);
	}	// end of getCountOfReschdCal

	/**
	 * <pre>
	 * 통합일정 달력 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseReschdVO> getListOfReschdCal(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseReschdVO>)list("ptcouncil.new.reschd.ptSelectReschdCalList",paramObj);
	}	// end of getListOfReschdCal

	/**
	 * <pre>
	 * 통합일정 달력 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseReschdVO> getListOfReschdCal(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseReschdVO>)listWithPaging("ptcouncil.new.reschd.ptSelectReschdCalList",paramObj,Integer.parseInt(pageIndex)-1,Integer.parseInt(pageSize));
	}	// end of getListOfReschdCal

	/**
	 * <pre>
	 * 통합일정 달력 상세내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseReschdVO
	 * @throws Exception
	 */
	public BaseReschdVO getDetailOfReschdCal(BaseCommonVO paramObj) throws Exception { 
		return (BaseReschdVO)select("ptcouncil.new.reschd.ptSelectReschdCalDetail",paramObj);
	}	// end of getDetailOfReschdCal

	/**
	 * <pre>
	 * 통합일정 달력 - Child 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseReschdVO> getChldOfReschdCal(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseReschdVO>)list("ptcouncil.new.reschd.ptSelectReschdCalChild",paramObj);
	}	// end of getListOfReschdCal

	/**
	 * <pre>
	 * 통합일정 달력 - Child 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseReschdVO> getChldOfReschdCal(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseReschdVO>)listWithPaging("ptcouncil.new.reschd.ptSelectReschdCalChild",paramObj,Integer.parseInt(pageIndex)-1,Integer.parseInt(pageSize));
	}	// end of getListOfReschdCal

}	// end of class