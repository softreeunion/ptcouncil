/*******************************************************************************
  Program ID  : AttendMngrService
  Description : 방청신청 관리에 관한 Service Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.10.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.board.service;

import java.util.*;

import egovframework.ptcc.cmmn.vo.*;

public interface AttendMngrService {

	// 방청신청 관리
	String getInfoOfAttendUUID(String paramVal) throws Exception;
	int getCountOfAttend(BaseCommonVO paramObj) throws Exception;
	List<BaseAttendVO> getListOfAttend(BaseCommonVO paramObj) throws Exception;
	List<BaseAttendVO> getListOfAttend(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception;
	BaseAttendVO getDetailOfAttend(BaseCommonVO paramObj) throws Exception;
	BaseAttendVO getDetailOfAttend(String paramVal) throws Exception;
	BaseAttendVO getInfoOfAttendSeq(BaseAttendVO paramObj) throws Exception;
	boolean doRtnOfCreateAttend(BaseAttendVO paramObj) throws Exception;
	boolean doRtnOfUpdateAttend(BaseAttendVO paramObj) throws Exception;
	boolean doRtnOfDeleteAttend(BaseAttendVO paramObj) throws Exception;
	boolean doRtnOfModifyAttend(BaseAttendVO paramObj) throws Exception;

}	// end of interface