/*******************************************************************************
  Program ID  : BoardMngrServiceImpl
  Description : 게시판 관리에 관한 Service Implement Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.board.service.impl;

import java.util.*;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.board.service.*;

@Service("boardMngrService")
public class BoardMngrServiceImpl extends egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl implements BoardMngrService { 

	@Resource(name="boardMngrDAO")
	private BoardMngrDAO					boardMngrDAO;

	@Override
	public 	String getInfoOfReplyUUID(String paramVal) throws Exception { 
		return (String)boardMngrDAO.getInfoOfReplyUUID( paramVal );
	}
	@Override
	public 	int getCountOfReply(BaseCommonVO paramObj) throws Exception { 
		return (int)boardMngrDAO.getCountOfReply( paramObj );
	}
	@Override
	public 	List<BaseBoardsVO> getListOfReply(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseBoardsVO>)boardMngrDAO.getListOfReply( paramObj );
	}
	@Override
	public 	List<BaseBoardsVO> getListOfReply(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseBoardsVO>)boardMngrDAO.getListOfReply( paramObj, pageIndex, pageSize );
	}
	@Override
	public 	BaseBoardsVO getDetailOfReply(BaseCommonVO paramObj) throws Exception { 
		return (BaseBoardsVO)boardMngrDAO.getDetailOfReply( paramObj );
	}
	@Override
	public 	BaseBoardsVO getDetailOfReply(String paramVal) throws Exception { 
		return (BaseBoardsVO)boardMngrDAO.getDetailOfReply( paramVal );
	}
	@Override
	public 	BaseBoardsVO getInfoOfReplySeq(BaseBoardsVO paramObj) throws Exception { 
		return (BaseBoardsVO)boardMngrDAO.getInfoOfReplySeq( paramObj );
	}
	@Override
	public 	boolean doRtnOfCreateReply(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfCreateReply( paramObj );
	}
	@Override
	public 	boolean doRtnOfUpdateReply(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfUpdateReply( paramObj );
	}
	@Override
	public 	boolean doRtnOfDeleteReply(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfDeleteReply( paramObj );
	}
	@Override
	public 	boolean doRtnOfModifyReply(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfModifyReply( paramObj );
	}
	@Override
	public String getInfoOfBoardUUID(String paramVal) throws Exception { 
		return (String)boardMngrDAO.getInfoOfBoardUUID( paramVal );
	}
	@Override
	public int getCountOfBoard(BaseCommonVO paramObj) throws Exception { 
		return (int)boardMngrDAO.getCountOfBoard( paramObj );
	}
	@Override
	public List<BaseBoardsVO> getListOfBoard(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseBoardsVO>)boardMngrDAO.getListOfBoard( paramObj );
	}
	@Override
	public List<BaseBoardsVO> getListOfBoard(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseBoardsVO>)boardMngrDAO.getListOfBoard( paramObj, pageIndex, pageSize );
	}
	@Override
	public 	BaseBoardsVO getMovingOfBoard(BaseCommonVO paramObj) throws Exception { 
		return (BaseBoardsVO)boardMngrDAO.getMovingOfBoard( paramObj );
	}
	@Override
	public 	BaseBoardsVO getDetailOfBoard(BaseCommonVO paramObj) throws Exception { 
		return (BaseBoardsVO)boardMngrDAO.getDetailOfBoard( paramObj );
	}
	@Override
	public 	BaseBoardsVO getDetailOfBoard(String paramVal) throws Exception { 
		return (BaseBoardsVO)boardMngrDAO.getDetailOfBoard( paramVal );
	}
	@Override
	public BaseBoardsVO getInfoOfBoardSeq(BaseBoardsVO paramObj) throws Exception { 
		return (BaseBoardsVO)boardMngrDAO.getInfoOfBoardSeq( paramObj );
	}
	@Override
	public BaseBoardsVO getInfoOfBoardStepMin(BaseBoardsVO paramObj) throws Exception { 
		return (BaseBoardsVO)boardMngrDAO.getInfoOfBoardStepMin( paramObj );
	}
	@Override
	public BaseBoardsVO getInfoOfBoardStepMax(BaseBoardsVO paramObj) throws Exception { 
		return (BaseBoardsVO)boardMngrDAO.getInfoOfBoardStepMax( paramObj );
	}
	@Override
	public boolean doRtnOfCreateBoard(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfCreateBoard( paramObj );
	}
	@Override
	public boolean doRtnOfUpdateBoard(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfUpdateBoard( paramObj );
	}
	@Override
	public boolean doRtnOfDeleteBoard(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfDeleteBoard( paramObj );
	}
	@Override
	public boolean doRtnOfModifyBoard(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfModifyBoard( paramObj );
	}
	@Override
	public boolean doRtnOfModifyBoardHit(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfModifyBoardHit( paramObj );
	}
	@Override
	public boolean doRtnOfModifyBoardStep(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfModifyBoardStep( paramObj );
	}
	@Override
	public boolean doRtnOfModifyBoardOrdrInc(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfModifyBoardOrdrInc( paramObj );
	}
	@Override
	public boolean doRtnOfModifyBoardOrdrDec(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfModifyBoardOrdrDec( paramObj );
	}
	@Override
	public boolean doRtnOfModifyBoardOrdrIns(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfModifyBoardOrdrIns( paramObj );
	}
	@Override
	public boolean doRtnOfModifyBoardRef(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfModifyBoardRef( paramObj );
	}
	@Override
	public boolean doRtnOfModifyBoardAns(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfModifyBoardAns( paramObj );
	}

	@Override
	public List<BaseBoardsVO> getListOfAdder(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseBoardsVO>)boardMngrDAO.getListOfAdder( paramObj );
	}
	@Override
	public BaseBoardsVO getDetailOfAdder(BaseCommonVO paramObj) throws Exception { 
		return (BaseBoardsVO)boardMngrDAO.getDetailOfAdder( paramObj );
	}
	@Override
	public boolean doRtnOfCreateAdder(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfCreateAdder( paramObj );
	}
	@Override
	public boolean doRtnOfUpdateAdder(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfUpdateAdder( paramObj );
	}
	@Override
	public boolean doRtnOfDeleteAdder(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfDeleteAdder( paramObj );
	}
	@Override
	public boolean doRtnOfModifyAdder(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfModifyAdder( paramObj );
	}

	@Override
	public String getInfoOfPhotoUUID(String paramVal) throws Exception { 
		return (String)boardMngrDAO.getInfoOfPhotoUUID( paramVal );
	}
	@Override
	public int getCountOfPhoto(BaseCommonVO paramObj) throws Exception { 
		return (int)boardMngrDAO.getCountOfPhoto( paramObj );
	}
	@Override
	public List<BaseBoardsVO> getListOfPhoto(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseBoardsVO>)boardMngrDAO.getListOfPhoto( paramObj );
	}
	@Override
	public List<BaseBoardsVO> getListOfPhoto(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseBoardsVO>)boardMngrDAO.getListOfPhoto( paramObj, pageIndex, pageSize );
	}
	@Override
	public 	BaseBoardsVO getMovingOfPhoto(BaseCommonVO paramObj) throws Exception { 
		return (BaseBoardsVO)boardMngrDAO.getMovingOfPhoto( paramObj );
	}
	@Override
	public BaseBoardsVO getDetailOfPhoto(BaseCommonVO paramObj) throws Exception { 
		return (BaseBoardsVO)boardMngrDAO.getDetailOfPhoto( paramObj );
	}
	@Override
	public 	BaseBoardsVO getDetailOfPhoto(String paramVal) throws Exception { 
		return (BaseBoardsVO)boardMngrDAO.getDetailOfPhoto( paramVal );
	}
	@Override
	public boolean doRtnOfModifyPhotoHit(BaseBoardsVO paramObj) throws Exception { 
		return (boolean)boardMngrDAO.doRtnOfModifyPhotoHit( paramObj );
	}
	@Override
	public List<BaseBoardsVO> getChldOfPhoto(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseBoardsVO>)boardMngrDAO.getChldOfPhoto( paramObj );
	}
	@Override
	public List<BaseBoardsVO> getChldOfPhoto(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseBoardsVO>)boardMngrDAO.getChldOfPhoto( paramObj, pageIndex, pageSize );
	}

	@Override
	public List<BaseBoardsVO> getListOfBoardNote(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseBoardsVO>)boardMngrDAO.getListOfBoardNote( paramObj );
	}
	@Override
	public List<BaseBoardsVO> getListOfBoardMain(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseBoardsVO>)boardMngrDAO.getListOfBoardMain( paramObj );
	}
	@Override
	public List<BaseBoardsVO> getListOfBoardImgs(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseBoardsVO>)boardMngrDAO.getListOfBoardImgs( paramObj );
	}
	@Override
	public List<BaseBoardsVO> getListOfPhotoMain(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseBoardsVO>)boardMngrDAO.getListOfPhotoMain( paramObj );
	}

	@Override
	public BaseLiveVO getDataLive() throws Exception {
		return (BaseLiveVO)boardMngrDAO.getDataLive();
	}

	@Override
	public boolean updateDataLive(BaseLiveVO paramObj) throws Exception {
		return (boolean)boardMngrDAO.updateDataLive( paramObj );
	}

	@Override
	public List<BaseBoardsVO> getListOfBoardNoti(BaseCommonVO paramObj) throws Exception {
		return (List<BaseBoardsVO>)boardMngrDAO.getListOfBoardNoti( paramObj );
	}

	@Override
	public BasePageVO getPageContents(String param) throws Exception {
		return (BasePageVO)boardMngrDAO.getPageContents(param);
	}

	@Override
	public boolean updatePageContents(BasePageVO paramObj) throws Exception {
		return (boolean)boardMngrDAO.updatePageContents( paramObj );
	}

}	// end of class