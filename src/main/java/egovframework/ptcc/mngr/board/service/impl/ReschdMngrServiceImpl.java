/*******************************************************************************
  Program ID  : ReschdMngrServiceImpl
  Description : 통합일정 관리에 관한 Service Implement Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.10.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.board.service.impl;

import java.util.*;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.board.service.*;

@Service("reschdMngrService")
public class ReschdMngrServiceImpl extends egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl implements ReschdMngrService { 

	@Resource(name="reschdMngrDAO")
	private ReschdMngrDAO					reschdMngrDAO;

	@Override
	public String getInfoOfReschdUUID(String paramVal) throws Exception { 
		return (String)reschdMngrDAO.getInfoOfReschdUUID( paramVal );
	}
	@Override
	public int getCountOfReschd(BaseCommonVO paramObj) throws Exception { 
		return (int)reschdMngrDAO.getCountOfReschd( paramObj );
	}
	@Override
	public List<BaseReschdVO> getListOfReschd(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseReschdVO>)reschdMngrDAO.getListOfReschd( paramObj );
	}
	@Override
	public List<BaseReschdVO> getListOfReschd(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseReschdVO>)reschdMngrDAO.getListOfReschd( paramObj, pageIndex, pageSize );
	}
	@Override
	public BaseReschdVO getDetailOfReschd(BaseCommonVO paramObj) throws Exception { 
		return (BaseReschdVO)reschdMngrDAO.getDetailOfReschd( paramObj );
	}
	@Override
	public BaseReschdVO getDetailOfReschd(String paramVal) throws Exception { 
		return (BaseReschdVO)reschdMngrDAO.getDetailOfReschd( paramVal );
	}
	@Override
	public BaseReschdVO getInfoOfReschdSeq(BaseReschdVO paramObj) throws Exception { 
		return (BaseReschdVO)reschdMngrDAO.getInfoOfReschdSeq( paramObj );
	}
	@Override
	public boolean doRtnOfCreateReschd(BaseReschdVO paramObj) throws Exception { 
		return (boolean)reschdMngrDAO.doRtnOfCreateReschd( paramObj );
	}
	@Override
	public boolean doRtnOfUpdateReschd(BaseReschdVO paramObj) throws Exception { 
		return (boolean)reschdMngrDAO.doRtnOfUpdateReschd( paramObj );
	}
	@Override
	public boolean doRtnOfDeleteReschd(BaseReschdVO paramObj) throws Exception { 
		return (boolean)reschdMngrDAO.doRtnOfDeleteReschd( paramObj );
	}
	@Override
	public boolean doRtnOfModifyReschd(BaseReschdVO paramObj) throws Exception { 
		return (boolean)reschdMngrDAO.doRtnOfModifyReschd( paramObj );
	}

	@Override
	public int getCountOfReschdCal(BaseCommonVO paramObj) throws Exception { 
		return (int)reschdMngrDAO.getCountOfReschdCal( paramObj );
	}
	@Override
	public List<BaseReschdVO> getListOfReschdCal(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseReschdVO>)reschdMngrDAO.getListOfReschdCal( paramObj );
	}
	@Override
	public List<BaseReschdVO> getListOfReschdCal(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseReschdVO>)reschdMngrDAO.getListOfReschdCal( paramObj, pageIndex, pageSize );
	}
	@Override
	public BaseReschdVO getDetailOfReschdCal(BaseCommonVO paramObj) throws Exception { 
		return (BaseReschdVO)reschdMngrDAO.getDetailOfReschdCal( paramObj );
	}
	@Override
	public List<BaseReschdVO> getChldOfReschdCal(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseReschdVO>)reschdMngrDAO.getChldOfReschdCal( paramObj );
	}
	@Override
	public List<BaseReschdVO> getChldOfReschdCal(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseReschdVO>)reschdMngrDAO.getChldOfReschdCal( paramObj, pageIndex, pageSize );
	}

}	// end of class