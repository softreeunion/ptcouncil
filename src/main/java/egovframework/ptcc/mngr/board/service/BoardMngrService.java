/*******************************************************************************
  Program ID  : BoardMngrService
  Description : 게시판 관리에 관한 Service Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.board.service;

import java.util.*;

import egovframework.ptcc.cmmn.vo.*;

public interface BoardMngrService { 

	// 댓글게시판 관리
	String getInfoOfReplyUUID(String paramVal) throws Exception;
	int getCountOfReply(BaseCommonVO paramObj) throws Exception;
	List<BaseBoardsVO> getListOfReply(BaseCommonVO paramObj) throws Exception;
	List<BaseBoardsVO> getListOfReply(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception;
	BaseBoardsVO getDetailOfReply(BaseCommonVO paramObj) throws Exception;
	BaseBoardsVO getDetailOfReply(String paramVal) throws Exception;
	BaseBoardsVO getInfoOfReplySeq(BaseBoardsVO paramObj) throws Exception;
	boolean doRtnOfCreateReply(BaseBoardsVO paramObj) throws Exception;
	boolean doRtnOfUpdateReply(BaseBoardsVO paramObj) throws Exception;
	boolean doRtnOfDeleteReply(BaseBoardsVO paramObj) throws Exception;
	boolean doRtnOfModifyReply(BaseBoardsVO paramObj) throws Exception;

	// 통합게시판 관리
	String getInfoOfBoardUUID(String paramVal) throws Exception;
	int getCountOfBoard(BaseCommonVO paramObj) throws Exception;
	List<BaseBoardsVO> getListOfBoard(BaseCommonVO paramObj) throws Exception;
	List<BaseBoardsVO> getListOfBoard(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception;
	BaseBoardsVO getMovingOfBoard(BaseCommonVO paramObj) throws Exception;
	BaseBoardsVO getDetailOfBoard(BaseCommonVO paramObj) throws Exception;
	BaseBoardsVO getDetailOfBoard(String paramVal) throws Exception;
	BaseBoardsVO getInfoOfBoardSeq(BaseBoardsVO paramObj) throws Exception;
	BaseBoardsVO getInfoOfBoardStepMin(BaseBoardsVO paramObj) throws Exception;
	BaseBoardsVO getInfoOfBoardStepMax(BaseBoardsVO paramObj) throws Exception;
	boolean doRtnOfCreateBoard(BaseBoardsVO paramObj) throws Exception;
	boolean doRtnOfUpdateBoard(BaseBoardsVO paramObj) throws Exception;
	boolean doRtnOfDeleteBoard(BaseBoardsVO paramObj) throws Exception;
	boolean doRtnOfModifyBoard(BaseBoardsVO paramObj) throws Exception;
	boolean doRtnOfModifyBoardHit(BaseBoardsVO paramObj) throws Exception;
	boolean doRtnOfModifyBoardStep(BaseBoardsVO paramObj) throws Exception;
	boolean doRtnOfModifyBoardOrdrInc(BaseBoardsVO paramObj) throws Exception;
	boolean doRtnOfModifyBoardOrdrDec(BaseBoardsVO paramObj) throws Exception;
	boolean doRtnOfModifyBoardOrdrIns(BaseBoardsVO paramObj) throws Exception;
	boolean doRtnOfModifyBoardRef(BaseBoardsVO paramObj) throws Exception;
	boolean doRtnOfModifyBoardAns(BaseBoardsVO paramObj) throws Exception;

	// 추가게시판 관리
	List<BaseBoardsVO> getListOfAdder(BaseCommonVO paramObj) throws Exception;
	BaseBoardsVO getDetailOfAdder(BaseCommonVO paramObj) throws Exception;
	boolean doRtnOfCreateAdder(BaseBoardsVO paramObj) throws Exception;
	boolean doRtnOfUpdateAdder(BaseBoardsVO paramObj) throws Exception;
	boolean doRtnOfDeleteAdder(BaseBoardsVO paramObj) throws Exception;
	boolean doRtnOfModifyAdder(BaseBoardsVO paramObj) throws Exception;

	// 사진게시판 관리
	String getInfoOfPhotoUUID(String paramVal) throws Exception;
	int getCountOfPhoto(BaseCommonVO paramObj) throws Exception;
	List<BaseBoardsVO> getListOfPhoto(BaseCommonVO paramObj) throws Exception;
	List<BaseBoardsVO> getListOfPhoto(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception;
	BaseBoardsVO getMovingOfPhoto(BaseCommonVO paramObj) throws Exception;
	BaseBoardsVO getDetailOfPhoto(BaseCommonVO paramObj) throws Exception;
	BaseBoardsVO getDetailOfPhoto(String paramVal) throws Exception;
	boolean doRtnOfModifyPhotoHit(BaseBoardsVO paramObj) throws Exception;
	List<BaseBoardsVO> getChldOfPhoto(BaseCommonVO paramObj) throws Exception;
	List<BaseBoardsVO> getChldOfPhoto(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception;

	// 통합게시판 공지글 관리
	List<BaseBoardsVO> getListOfBoardNote(BaseCommonVO paramObj) throws Exception;
	// 통합게시판 메인글 관리
	List<BaseBoardsVO> getListOfBoardMain(BaseCommonVO paramObj) throws Exception;
	// 통합게시판 그림글 관리
	List<BaseBoardsVO> getListOfBoardImgs(BaseCommonVO paramObj) throws Exception;
	// 사진게시판 메인글 관리
	List<BaseBoardsVO> getListOfPhotoMain(BaseCommonVO paramObj) throws Exception;

	BaseLiveVO getDataLive() throws Exception;
	boolean updateDataLive(BaseLiveVO paramObj) throws Exception;
	List<BaseBoardsVO> getListOfBoardNoti(BaseCommonVO paramObj) throws Exception;
	BasePageVO getPageContents(String param) throws Exception;
	boolean updatePageContents(BasePageVO paramObj) throws Exception;

}	// end of interface