/*******************************************************************************
  Program ID  : ReschdMngrService
  Description : 통합일정 관리에 관한 Service Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.10.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.board.service;

import java.util.*;

import egovframework.ptcc.cmmn.vo.*;

public interface ReschdMngrService {

	// 통합일정 관리
	String getInfoOfReschdUUID(String paramVal) throws Exception;
	int getCountOfReschd(BaseCommonVO paramObj) throws Exception;
	List<BaseReschdVO> getListOfReschd(BaseCommonVO paramObj) throws Exception;
	List<BaseReschdVO> getListOfReschd(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception;
	BaseReschdVO getDetailOfReschd(BaseCommonVO paramObj) throws Exception;
	BaseReschdVO getDetailOfReschd(String paramVal) throws Exception;
	BaseReschdVO getInfoOfReschdSeq(BaseReschdVO paramObj) throws Exception;
	boolean doRtnOfCreateReschd(BaseReschdVO paramObj) throws Exception;
	boolean doRtnOfUpdateReschd(BaseReschdVO paramObj) throws Exception;
	boolean doRtnOfDeleteReschd(BaseReschdVO paramObj) throws Exception;
	boolean doRtnOfModifyReschd(BaseReschdVO paramObj) throws Exception;

	// 통합일정 달력 관리
	int getCountOfReschdCal(BaseCommonVO paramObj) throws Exception;
	List<BaseReschdVO> getListOfReschdCal(BaseCommonVO paramObj) throws Exception;
	List<BaseReschdVO> getListOfReschdCal(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception;
	BaseReschdVO getDetailOfReschdCal(BaseCommonVO paramObj) throws Exception;
	List<BaseReschdVO> getChldOfReschdCal(BaseCommonVO paramObj) throws Exception;
	List<BaseReschdVO> getChldOfReschdCal(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception;

}	// end of interface