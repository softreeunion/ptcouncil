/*******************************************************************************
  Program ID  : BoardMngrDAO
  Description : 게시판 정보에 대한 Data Access Object Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.board.service.impl;

import java.util.*;

import org.springframework.stereotype.Repository;

import egovframework.ptcc.cmmn.vo.*;

@Repository("boardMngrDAO")
public class BoardMngrDAO extends egovframework.rte.psl.dataaccess.EgovAbstractDAO { 

	/**
	 * <pre>
	 * 댓글게시판 UUID을(를) 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return String
	 * @throws Exception
	 */
	public String getInfoOfReplyUUID(String paramVal) throws Exception { 
		return (String)select("ptcouncil.new.boards.ptSelectReplyUUID",paramVal);
	}	// end of getInfoOfReplyUUID

	/**
	 * <pre>
	 * 댓글게시판 목록건수을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return Integer
	 * @throws Exception
	 */
	public int getCountOfReply(BaseCommonVO paramObj) throws Exception { 
		return (Integer)select("ptcouncil.new.boards.ptSelectReplyCount",paramObj);
	}	// end of getCountOfReply

	/**
	 * <pre>
	 * 댓글게시판 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseBoardsVO> getListOfReply(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseBoardsVO>)list("ptcouncil.new.boards.ptSelectReplyList",paramObj);
	}	// end of getListOfReply

	/**
	 * <pre>
	 * 댓글게시판 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @param  String							// 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseBoardsVO> getListOfReply(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseBoardsVO>)listWithPaging("ptcouncil.new.boards.ptSelectReplyList",paramObj,Integer.parseInt(pageIndex)-1,Integer.parseInt(pageSize));
	}	// end of getListOfReply

	/**
	 * <pre>
	 * 댓글게시판 상세내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseBoardsVO
	 * @throws Exception
	 */
	public BaseBoardsVO getDetailOfReply(BaseCommonVO paramObj) throws Exception { 
		return (BaseBoardsVO)select("ptcouncil.new.boards.ptSelectReplyDetail",paramObj);
	}	// end of getDetailOfReply

	/**
	 * <pre>
	 * 댓글게시판 상세내역(UUID)을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return BaseBoardsVO
	 * @throws Exception
	 */
	public BaseBoardsVO getDetailOfReply(String paramVal) throws Exception { 
		return (BaseBoardsVO)select("ptcouncil.new.boards.ptSelectReplyDetailUUID",paramVal);
	}	// end of getDetailOfReply

	/**
	 * <pre>
	 * 댓글게시판 일련번호을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return BaseBoardsVO
	 * @throws Exception
	 */
	public BaseBoardsVO getInfoOfReplySeq(BaseBoardsVO paramObj) throws Exception { 
		return (BaseBoardsVO)select("ptcouncil.new.boards.ptSelectReplySeq",paramObj);
	}	// end of getInfoOfReplySeq

	/**
	 * <pre>
	 * 댓글게시판 내역을(를) 등록 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfCreateReply(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.boards.ptInsertReply",paramObj)<=0);
	}	// end of doActOfCreateReply

	/**
	 * <pre>
	 * 댓글게시판 내역을(를) 변경 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfUpdateReply(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.boards.ptUpdateReply",paramObj)<=0);
	}	// end of doActOfUpdateReply

	/**
	 * <pre>
	 * 댓글게시판 내역을(를) 삭제 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfDeleteReply(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.boards.ptDeleteReply",paramObj)<=0);
	}	// end of doActOfDeleteReply

	/**
	 * <pre>
	 * 댓글게시판 내역을(를) 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfModifyReply(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.boards.ptModifyReply",paramObj)<=0);
	}	// end of doActOfModifyReply


	/**
	 * <pre>
	 * 통합게시판 UUID을(를) 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return String
	 * @throws Exception
	 */
	public String getInfoOfBoardUUID(String paramVal) throws Exception { 
		return (String)select("ptcouncil.new.boards.ptSelectBoardUUID",paramVal);
	}	// end of getInfoOfBoardUUID

	/**
	 * <pre>
	 * 통합게시판 목록건수을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return Integer
	 * @throws Exception
	 */
	public int getCountOfBoard(BaseCommonVO paramObj) throws Exception { 
		return (Integer)select("ptcouncil.new.boards.ptSelectBoardCount",paramObj);
	}	// end of getCountOfBoard

	/**
	 * <pre>
	 * 통합게시판 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseBoardsVO> getListOfBoard(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseBoardsVO>)list("ptcouncil.new.boards.ptSelectBoardList",paramObj);
	}	// end of getListOfBoard

	/**
	 * <pre>
	 * 통합게시판 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @param  String							// 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseBoardsVO> getListOfBoard(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseBoardsVO>)listWithPaging("ptcouncil.new.boards.ptSelectBoardList",paramObj,Integer.parseInt(pageIndex)-1,Integer.parseInt(pageSize));
	}	// end of getListOfBoard

	/**
	 * <pre>
	 * 통합게시판 상세(이전/다음)을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseBoardsVO
	 * @throws Exception
	 */
	public BaseBoardsVO getMovingOfBoard(BaseCommonVO paramObj) throws Exception { 
		return (BaseBoardsVO)select("ptcouncil.new.boards.ptSelectBoardMove",paramObj);
	}	// end of getMovingOfBoard

	/**
	 * <pre>
	 * 통합게시판 상세내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseBoardsVO
	 * @throws Exception
	 */
	public BaseBoardsVO getDetailOfBoard(BaseCommonVO paramObj) throws Exception { 
		return (BaseBoardsVO)select("ptcouncil.new.boards.ptSelectBoardDetail",paramObj);
	}	// end of getDetailOfBoard

	/**
	 * <pre>
	 * 통합게시판 상세내역(UUID)을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return BaseBoardsVO
	 * @throws Exception
	 */
	public BaseBoardsVO getDetailOfBoard(String paramVal) throws Exception { 
		return (BaseBoardsVO)select("ptcouncil.new.boards.ptSelectBoardDetailUUID",paramVal);
	}	// end of getDetailOfBoard

	/**
	 * <pre>
	 * 통합게시판 일련번호을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return BaseBoardsVO
	 * @throws Exception
	 */
	public BaseBoardsVO getInfoOfBoardSeq(BaseBoardsVO paramObj) throws Exception { 
		return (BaseBoardsVO)select("ptcouncil.new.boards.ptSelectBoardSeq",paramObj);
	}	// end of getInfoOfBoardSeq

	/**
	 * <pre>
	 * 통합게시판 답글순번(Min)을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return BaseBoardsVO
	 * @throws Exception
	 */
	public BaseBoardsVO getInfoOfBoardStepMin(BaseBoardsVO paramObj) throws Exception { 
		return (BaseBoardsVO)select("ptcouncil.new.boards.ptSelectBoardStepMin",paramObj);
	}	// end of getInfoOfBoardStepMin

	/**
	 * <pre>
	 * 통합게시판 답글순번(Max)을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return BaseBoardsVO
	 * @throws Exception
	 */
	public BaseBoardsVO getInfoOfBoardStepMax(BaseBoardsVO paramObj) throws Exception { 
		return (BaseBoardsVO)select("ptcouncil.new.boards.ptSelectBoardStepMax",paramObj);
	}	// end of getInfoOfBoardStepMax

	/**
	 * <pre>
	 * 통합게시판 내역을(를) 등록 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfCreateBoard(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.boards.ptInsertBoard",paramObj)<=0);
	}	// end of doActOfCreateBoard

	/**
	 * <pre>
	 * 통합게시판 내역을(를) 변경 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfUpdateBoard(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.boards.ptUpdateBoard",paramObj)<=0);
	}	// end of doActOfUpdateBoard

	/**
	 * <pre>
	 * 통합게시판 내역을(를) 삭제 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfDeleteBoard(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.boards.ptDeleteBoard",paramObj)<=0);
	}	// end of doActOfDeleteBoard

	/**
	 * <pre>
	 * 통합게시판 내역을(를) 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfModifyBoard(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.boards.ptModifyBoard",paramObj)<=0);
	}	// end of doActOfModifyBoard

	/**
	 * <pre>
	 * 통합게시판 내역(조회건수)을 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfModifyBoardHit(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.boards.ptModifyBoardHit",paramObj)<=0);
	}	// end of doActOfModifyBoardHit

	/**
	 * <pre>
	 * 통합게시판 내역(답변순번)을 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfModifyBoardStep(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.boards.ptModifyBoardStep",paramObj)<=0);
	}	// end of doActOfModifyBoardStep

	/**
	 * <pre>
	 * 통합게시판 내역(보기순번_증가)을 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfModifyBoardOrdrInc(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.boards.ptModifyBoardOrdrInc",paramObj)<=0);
	}	// end of doActOfModifyBoardOrdrInc

	/**
	 * <pre>
	 * 통합게시판 내역(보기순번_감소)을 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfModifyBoardOrdrDec(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.boards.ptModifyBoardOrdrDec",paramObj)<=0);
	}	// end of doActOfModifyBoardOrdrDec

	/**
	 * <pre>
	 * 통합게시판 내역(보기순번_신규)을 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfModifyBoardOrdrIns(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.boards.ptModifyBoardOrdrIns",paramObj)<=0);
	}	// end of doActOfModifyBoardOrdrIns

	/**
	 * <pre>
	 * 통합게시판 내역(자신_공개)을 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfModifyBoardAns(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.boards.ptModifyBoardAns",paramObj)<=0);
	}	// end of doActOfModifyBoardAns

	/**
	 * <pre>
	 * 통합게시판 내역(답변_공개)을 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfModifyBoardRef(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.boards.ptModifyBoardRef",paramObj)<=0);
	}	// end of doActOfModifyBoardRef


	/**
	 * <pre>
	 * 추가게시판 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseBoardsVO> getListOfAdder(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseBoardsVO>)list("ptcouncil.new.boards.ptSelectAdderList",paramObj);
	}

	/**
	 * <pre>
	 * 추가게시판 상세내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseBoardsVO
	 * @throws Exception
	 */
	public BaseBoardsVO getDetailOfAdder(BaseCommonVO paramObj) throws Exception { 
		return (BaseBoardsVO)select("ptcouncil.new.boards.ptSelectAdderDetail",paramObj);
	}

	/**
	 * <pre>
	 * 추가게시판 내역을(를) 등록 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfCreateAdder(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.boards.ptInsertAdder",paramObj)<=0);
	}

	/**
	 * <pre>
	 * 추가게시판 내역을(를) 변경 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfUpdateAdder(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.boards.ptUpdateAdder",paramObj)<=0);
	}

	/**
	 * <pre>
	 * 추가게시판 내역을(를) 삭제 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfDeleteAdder(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.boards.ptDeleteAdder",paramObj)<=0);
	}

	/**
	 * <pre>
	 * 추가게시판 내역을(를) 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfModifyAdder(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.boards.ptModifyAdder",paramObj)<=0);
	}


	/**
	 * <pre>
	 * 사진게시판 UUID을(를) 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return String
	 * @throws Exception
	 */
	public String getInfoOfPhotoUUID(String paramVal) throws Exception { 
		return (String)select("ptcouncil.new.photos.ptSelectPhotoUUID",paramVal);
	}	// end of getInfoOfPhotoUUID

	/**
	 * <pre>
	 * 사진게시판 목록건수을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return Integer
	 * @throws Exception
	 */
	public int getCountOfPhoto(BaseCommonVO paramObj) throws Exception { 
		return (Integer)select("ptcouncil.new.photos.ptSelectPhotoCount",paramObj);
	}	// end of getCountOfPhoto

	/**
	 * <pre>
	 * 사진게시판 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseBoardsVO> getListOfPhoto(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseBoardsVO>)list("ptcouncil.new.photos.ptSelectPhotoList",paramObj);
	}	// end of getListOfPhoto

	/**
	 * <pre>
	 * 사진게시판 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @param  String							// 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseBoardsVO> getListOfPhoto(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseBoardsVO>)listWithPaging("ptcouncil.new.photos.ptSelectPhotoList",paramObj,Integer.parseInt(pageIndex)-1,Integer.parseInt(pageSize));
	}	// end of getListOfPhoto

	/**
	 * <pre>
	 * 사진게시판 상세(이전/다음)을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseBoardsVO
	 * @throws Exception
	 */
	public BaseBoardsVO getMovingOfPhoto(BaseCommonVO paramObj) throws Exception { 
		return (BaseBoardsVO)select("ptcouncil.new.photos.ptSelectPhotoMove",paramObj);
	}	// end of getMovingOfPhoto

	/**
	 * <pre>
	 * 사진게시판 상세내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseBoardsVO
	 * @throws Exception
	 */
	public BaseBoardsVO getDetailOfPhoto(BaseCommonVO paramObj) throws Exception { 
		return (BaseBoardsVO)select("ptcouncil.new.photos.ptSelectPhotoDetail",paramObj);
	}	// end of getDetailOfPhoto

	/**
	 * <pre>
	 * 사진게시판 상세내역(UUID)을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return BaseBoardsVO
	 * @throws Exception
	 */
	public BaseBoardsVO getDetailOfPhoto(String paramVal) throws Exception { 
		return (BaseBoardsVO)select("ptcouncil.new.photos.ptSelectPhotoDetailUUID",paramVal);
	}	// end of getDetailOfPhoto

	/**
	 * <pre>
	 * 사진게시판 내역(조회건수)을 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseBoardsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfModifyPhotoHit(BaseBoardsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.photos.ptModifyPhotoHit",paramObj)<=0);
	}	// end of doRtnOfModifyPhotoHit

	/**
	 * <pre>
	 * 사진게시판 - Child 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseBoardsVO> getChldOfPhoto(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseBoardsVO>)list("ptcouncil.new.photos.ptSelectPhotoChild",paramObj);
	}	// end of getChldOfPhoto

	/**
	 * <pre>
	 * 사진게시판 - Child 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @param  String							// 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseBoardsVO> getChldOfPhoto(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseBoardsVO>)listWithPaging("ptcouncil.new.photos.ptSelectPhotoChild",paramObj,Integer.parseInt(pageIndex)-1,Integer.parseInt(pageSize));
	}	// end of getChldOfPhoto


	/**
	 * <pre>
	 * 통합게시판 공지글 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseBoardsVO> getListOfBoardNote(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseBoardsVO>)list("ptcouncil.new.boards.ptSelectBoardNote",paramObj);
	}	// end of getListOfBoardNote

	/**
	 * <pre>
	 * 통합게시판 메인글 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseBoardsVO> getListOfBoardMain(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseBoardsVO>)list("ptcouncil.new.boards.ptSelectBoardMain",paramObj);
	}	// end of getListOfBoardMain

	/**
	 * <pre>
	 * 통합게시판 그림글 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseBoardsVO> getListOfBoardImgs(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseBoardsVO>)list("ptcouncil.new.boards.ptSelectBoardImgs",paramObj);
	}	// end of getListOfBoardImgs

	/**
	 * <pre>
	 * 사진게시판 메인글 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseBoardsVO> getListOfPhotoMain(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseBoardsVO>)list("ptcouncil.new.photos.ptSelectPhotoMain",paramObj);
	}	// end of getListOfPhotoMain

	public BaseLiveVO getDataLive() throws Exception {
		return (BaseLiveVO)select("ptcouncil.new.boards.ptSelectLive");
	}

	public boolean updateDataLive(BaseLiveVO paramObj) throws Exception {
		return !((int)update("ptcouncil.new.boards.ptUpdateLive",paramObj)<=0);
	}

	@SuppressWarnings("unchecked")
	public List<BaseBoardsVO> getListOfBoardNoti(BaseCommonVO paramObj) throws Exception {
		return (List<BaseBoardsVO>)list("ptcouncil.new.boards.ptSelectBoardNoti",paramObj);
	}	// end of getListOfBoardNoti

	public BasePageVO getPageContents(String param) throws Exception {
		return (BasePageVO)select("ptcouncil.new.boards.ptSelectPageContents");
	}

	public boolean updatePageContents(BasePageVO paramObj) throws Exception {
		return !((int)update("ptcouncil.new.boards.ptUpdatePageContents",paramObj)<=0);
	}


}	// end of class