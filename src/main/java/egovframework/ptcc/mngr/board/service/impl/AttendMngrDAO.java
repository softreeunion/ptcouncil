/*******************************************************************************
  Program ID  : AttendMngrDAO
  Description : 방청신청 정보에 대한 Data Access Object Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.10.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.board.service.impl;

import java.util.*;

import org.springframework.stereotype.Repository;

import egovframework.ptcc.cmmn.vo.*;

@Repository("attendMngrDAO")
public class AttendMngrDAO extends egovframework.rte.psl.dataaccess.EgovAbstractDAO { 

	/**
	 * <pre>
	 * 방청신청 UUID을(를) 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return String
	 * @throws Exception
	 */
	public String getInfoOfAttendUUID(String paramVal) throws Exception { 
		return (String)select("ptcouncil.new.attend.ptSelectAttendUUID",paramVal);
	}	// end of getInfoOfAttendUUID

	/**
	 * <pre>
	 * 방청신청 목록건수을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return Integer
	 * @throws Exception
	 */
	public int getCountOfAttend(BaseCommonVO paramObj) throws Exception { 
		return (Integer)select("ptcouncil.new.attend.ptSelectAttendCount",paramObj);
	}	// end of getCountOfAttend

	/**
	 * <pre>
	 * 방청신청 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAttendVO> getListOfAttend(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseAttendVO>)list("ptcouncil.new.attend.ptSelectAttendList",paramObj);
	}	// end of getListOfAttend

	/**
	 * <pre>
	 * 방청신청 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @param  String							// 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseAttendVO> getListOfAttend(BaseCommonVO paramObj,String pageIndex,String pageSize) throws Exception { 
		return (List<BaseAttendVO>)listWithPaging("ptcouncil.new.attend.ptSelectAttendList",paramObj,Integer.parseInt(pageIndex)-1,Integer.parseInt(pageSize));
	}	// end of getListOfAttend

	/**
	 * <pre>
	 * 방청신청 상세내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseAttendVO
	 * @throws Exception
	 */
	public BaseAttendVO getDetailOfAttend(BaseCommonVO paramObj) throws Exception { 
		return (BaseAttendVO)select("ptcouncil.new.attend.ptSelectAttendDetail",paramObj);
	}	// end of getDetailOfAttend

	/**
	 * <pre>
	 * 방청신청 상세내역(UUID)을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return BaseAttendVO
	 * @throws Exception
	 */
	public BaseAttendVO getDetailOfAttend(String paramVal) throws Exception { 
		return (BaseAttendVO)select("ptcouncil.new.attend.ptSelectAttendDetailUUID",paramVal);
	}	// end of getDetailOfAttend

	/**
	 * <pre>
	 * 방청신청 일련번호을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAttendVO						// 
	 * @return BaseAttendVO
	 * @throws Exception
	 */
	public BaseAttendVO getInfoOfAttendSeq(BaseAttendVO paramObj) throws Exception { 
		return (BaseAttendVO)select("ptcouncil.new.attend.ptSelectAttendSeq",paramObj);
	}	// end of getInfoOfAttendSeq

	/**
	 * <pre>
	 * 방청신청 내역을(를) 등록 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAttendVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfCreateAttend(BaseAttendVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.attend.ptInsertAttend",paramObj)<=0);
	}	// end of doActOfCreateAttend

	/**
	 * <pre>
	 * 방청신청 내역을(를) 변경 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAttendVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfUpdateAttend(BaseAttendVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.attend.ptUpdateAttend",paramObj)<=0);
	}	// end of doActOfUpdateAttend

	/**
	 * <pre>
	 * 방청신청 내역을(를) 삭제 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAttendVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfDeleteAttend(BaseAttendVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.attend.ptDeleteAttend",paramObj)<=0);
	}	// end of doActOfDeleteAttend

	/**
	 * <pre>
	 * 방청신청 내역을(를) 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseAttendVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfModifyAttend(BaseAttendVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.attend.ptModifyAttend",paramObj)<=0);
	}	// end of doActOfModifyAttend

}	// end of class