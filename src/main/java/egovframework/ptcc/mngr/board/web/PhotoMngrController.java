/*******************************************************************************
  Program ID  : PhotoMngrController
  Description : 게시판 관리에 관한 controller 클래스를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
  v1.1    HexaMedia new0man      2020.03.17  비공개 목록 표시 및 검색조건 추가
 *******************************************************************************/
package egovframework.ptcc.mngr.board.web;

import java.util.*;

import javax.annotation.*;
import javax.servlet.http.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.dao.*;
import org.springframework.jdbc.*;
import org.springframework.jdbc.datasource.*;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.*;
import org.springframework.transaction.support.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.*;
import org.springframework.web.servlet.*;
import org.springmodules.validation.commons.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.excel.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.util.*;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.board.service.*;

@Controller
@RequestMapping(value="/ptnhexa/{paramPath}")
public class PhotoMngrController implements HandlerExceptionResolver {

	@Autowired
	private WebApplicationContext			webContext;
	@Autowired
	private DataSourceTransactionManager	txManager;

	@Resource(name="beanValidator")
	protected DefaultBeanValidator			beanValidator;
	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="boardMngrService")
	protected BoardMngrService				boardMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected String						hideMark     = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.HideMark" );
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));
	protected String						defsPass     = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsPass" );
	protected String						filePath     = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.PhotoBase" );
	protected String						replyImage   = "<img src='@@/images/icon/ico_reply.png' alt=''/>";
	protected String						lnkName      = "PT_BOARD";
	protected String						bbsType      = "10150010";
	protected boolean						notiItems    = false;
	protected boolean						moveItems    = true;
	protected boolean						fileUP       = true;
	protected boolean						tempUP       = false;

	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelMap model, 
			boolean connMode 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			model.addAttribute("message"       , "로그인 정보가 존재하지 않습니다.\\n\\n로그인 화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			model.addAttribute("message"       , "임시 비밀번호를 사용하고 있습니다.\\n\\n비밀번호 변경화면으로 이동합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			model.addAttribute("close"         , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			model.addAttribute("error"         , "접속권한이 없습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		return false;
	}
	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelMap model, 
			boolean connMode, 
			String viewNo 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			model.addAttribute("message"       , "로그인 정보가 존재하지 않습니다.\\n\\n로그인 화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			model.addAttribute("message"       , "임시 비밀번호를 사용하고 있습니다.\\n\\n비밀번호 변경화면으로 이동합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			model.addAttribute("close"         , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			model.addAttribute("error"         , "접속권한이 없습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 불량 접속 확인
		if( BaseUtility.isValidNull( viewNo ) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		return false;
	}
	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelAndView mav, 
			boolean connMode 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "로그인 정보가 존재하지 않습니다.\n\n로그인 화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "임시 비밀번호를 사용하고 있습니다.\n\n비밀번호 변경화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속권한이 없습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		return false;
	}
	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelAndView mav, 
			boolean connMode, 
			String viewNo 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "로그인 정보가 존재하지 않습니다.\n\n로그인 화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "임시 비밀번호를 사용하고 있습니다.\n\n비밀번호 변경화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속권한이 없습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 불량 접속 확인
		if( BaseUtility.isValidNull( viewNo ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "잘못된 경로로 접속하였습니다.\n\n메인화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		return false;
	}


	/**
	 * <pre>
	 * 게시판 내역을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/PhotoList.do")
	public String scrListOfPhoto ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "board/photo"+ BaseUtility.toCapitalize( paramPath ) +"_list";

		int						listCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseBoardsVO>		notiList     = new ArrayList<BaseBoardsVO>();
		List<BaseBoardsVO>		dataList     = new ArrayList<BaseBoardsVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
log.debug(" act: "+ BaseUtility.spaceFill( "Photo"+ BaseUtility.toCapitalize( paramPath ) +"List", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false ) ) return "/cmmn/exe_script";

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			retPath  = "board/photo"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		} else if( "coun".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110010";
			retPath  = "board/photo"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		} else if( "stand".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110020";
			retPath  = "stand/photo"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		} else if( "youth".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110030";
			retPath  = "youth/photo"+ BaseUtility.toCapitalize( paramPath ) +"_list";
		}

		// 기본 조회조건 취득
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(propertiesService.getInt("pageSize"));
		pageInfo.setTotalRecordCount(1);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("imgsUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setBbsType(BaseUtility.null2Char( searchVO.getBbsType(), bbsType ));
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("bbsType: "+ searchVO.getBbsType());
		try { 
			listCnt = (int)boardMngrService.getCountOfBoard( searchVO );

			// 조회 건수를 전체선택인 경우
			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
			}
			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
			searchVO.setPageFirNo(Integer.toString(idxFir));
			searchVO.setPageLstNo(Integer.toString(idxLst));
			searchVO.setListCnt(Integer.toString(listCnt));

			if( listCnt > 0 ) { 
				dataList = (List<BaseBoardsVO>)boardMngrService.getListOfBoard( searchVO );
//				dataList = (List<BaseBoardsVO>)boardMngrService.getListOfBoard( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				// Data Adjustment(Boards)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					for( int d = 0; d < dataList.size(); d++ ) { 
						dataList.get(d).setViewNo(dataList.get(d).getBbsUUID() + dataList.get(d).getRowSeq());

						dataList.get(d).setBbsFiles(BaseUtility.toNumberFormat( dataList.get(d).getBbsFiles() ));
						dataList.get(d).setBgnDate(BaseUtility.toDateFormat( dataList.get(d).getBgnDate(), dtSep, 8 ));
						dataList.get(d).setEndDate(BaseUtility.toDateFormat( dataList.get(d).getEndDate(), dtSep, 8 ));
						dataList.get(d).setHitCount(BaseUtility.toNumberFormat( dataList.get(d).getHitCount() ));
						dataList.get(d).setCreDate(BaseUtility.toDateFormat( dataList.get(d).getCreDate(), dtSep, 8 ));
						dataList.get(d).setModDate(BaseUtility.toDateFormat( dataList.get(d).getModDate(), dtSep, 8 ));
						dataList.get(d).setDelDate(BaseUtility.toDateFormat( dataList.get(d).getDelDate(), dtSep, 8 ));

						dataList.get(d).setBbsTitle(("Y".equals(dataList.get(d).getCreHide())?hideMark:"") + dataList.get(d).getBbsTitle());
						dataList.get(d).setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( dataList.get(d).getAnsPos() )) <= 0 ? dataList.get(d).getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ dataList.get(d).getBbsTitle());
						dataList.get(d).setContDescBR(BaseUtility.tag2Sym( dataList.get(d).getContDesc() ));
						dataList.get(d).setContDescCR(BaseUtility.sym2Tag( dataList.get(d).getContDesc() ));
						dataList.get(d).setContLinkC(BaseUtility.getUriCheck( dataList.get(d).getBbsSiteUri() ) ? "T" : "F");
						dataList.get(d).setCreLock(BaseUtility.null2Char( dataList.get(d).getCrePasswd(), "", "Y" ));
						dataList.get(d).setDownExt(FileProcess.getFileLowerExt( dataList.get(d).getDownRel() ));
						dataList.get(d).setFileUUID(BaseUtility.null2Char(dataList.get(d).getFileUUID(), "", "2"+ dataList.get(d).getFileUUID() ));
						dataList.get(d).setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( dataList.get(d).getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
						dataList.get(d).setRegiDate(BaseUtility.toDateFormat( BaseUtility.null2Char( dataList.get(d).getRegiDate(), dataList.get(d).getCreDate() ), dtSep, 8 ));

						// 상임위원회 구분
						if( "stand".equalsIgnoreCase(paramPath) && BaseUtility.isValidNull( dataList.get(d).getBbsCateName() ) ) { 
							if( "OPER".equals(dataList.get(d).getBbsCate())  ) dataList.get(d).setBbsCateName("의회운영위원회");
							if( "GVADM".equals(dataList.get(d).getBbsCate()) ) dataList.get(d).setBbsCateName("기획행정위원회");
							if( "WELFARE".equals(dataList.get(d).getBbsCate()) ) dataList.get(d).setBbsCateName("복지환경위원회");
							if( "BUILD".equals(dataList.get(d).getBbsCate()) ) dataList.get(d).setBbsCateName("산업건설위원회");
						}
					}
				}

				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
				pageInfo.setTotalRecordCount(listCnt);
			}

			// 게시판 내역(공지내역) 취득
			notiList = notiItems ? (List<BaseBoardsVO>)boardMngrService.getListOfBoardNote( searchVO ) : null;
			if( !(notiList == null || notiList.size() <= 0) ) { 
				for( int d = 0; d < notiList.size(); d++ ) { 
					notiList.get(d).setViewNo(notiList.get(d).getBbsUUID() + notiList.get(d).getRowSeq());

					notiList.get(d).setBbsFiles(BaseUtility.toNumberFormat( notiList.get(d).getBbsFiles() ));
					notiList.get(d).setBgnDate(BaseUtility.toDateFormat( notiList.get(d).getBgnDate(), dtSep, 8 ));
					notiList.get(d).setEndDate(BaseUtility.toDateFormat( notiList.get(d).getEndDate(), dtSep, 8 ));
					notiList.get(d).setHitCount(BaseUtility.toNumberFormat( notiList.get(d).getHitCount() ));
					notiList.get(d).setCreDate(BaseUtility.toDateFormat( notiList.get(d).getCreDate(), dtSep, 8 ));
					notiList.get(d).setModDate(BaseUtility.toDateFormat( notiList.get(d).getModDate(), dtSep, 8 ));
					notiList.get(d).setDelDate(BaseUtility.toDateFormat( notiList.get(d).getDelDate(), dtSep, 8 ));

					notiList.get(d).setContLinkC(BaseUtility.getUriCheck( notiList.get(d).getBbsSiteUri() ) ? "T" : "F");
					notiList.get(d).setDownExt(FileProcess.getFileLowerExt( notiList.get(d).getDownRel() ));
				}
			}

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			model.addAttribute("dataList"      , dataList);
			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (dataList == null ? 0 : dataList.size()) : listCnt ));
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("notiList"      , notiList);
			model.addAttribute("cateList"      , cateList);
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/"+retPath;
	}	// end of scrListOfPhoto

	/**
	 * <pre>
	 * 게시판 내역을(를) 상세조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/PhotoView.do")
	public String scrViewOfPhoto ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "board/photo"+ BaseUtility.toCapitalize( paramPath ) +"_view";

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		BaseBoardsVO			dataView     = new BaseBoardsVO();
		BaseBoardsVO			prevView     = new BaseBoardsVO();
		BaseBoardsVO			nextView     = new BaseBoardsVO();
		BaseCommonVO			findFile     = new BaseCommonVO();
		List<BaseFilesVO>		dataFile     = new ArrayList<BaseFilesVO>();
		List<BaseFilesVO>		thmbFile     = new ArrayList<BaseFilesVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		dataSupp     = new ArrayList<BaseCommonVO>();
		BaseBoardsVO			tempSupp     = new BaseBoardsVO();
		BaseBoardsVO			tempView     = new BaseBoardsVO();
		String[][]				tempList     = new String[4][10];
log.debug(" act: "+ BaseUtility.spaceFill( "Photo"+ BaseUtility.toCapitalize( paramPath ) +"View", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false, searchVO.getViewNo() ) ) return "/cmmn/exe_script";

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			retPath  = "board/photo"+ BaseUtility.toCapitalize( paramPath ) +"_view";
		} else if( "coun".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110010";
			retPath  = "board/photo"+ BaseUtility.toCapitalize( paramPath ) +"_view";
		} else if( "stand".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110020";
			retPath  = "stand/photo"+ BaseUtility.toCapitalize( paramPath ) +"_view";
		} else if( "youth".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110030";
			retPath  = "youth/photo"+ BaseUtility.toCapitalize( paramPath ) +"_view";
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("imgsUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setBbsType(BaseUtility.null2Char( searchVO.getBbsType(), bbsType ));
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)boardMngrService.getInfoOfBoardUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(게시판)
				searchVO.setBbsType(viewNo[0]);
				searchVO.setBbsSeq(viewNo[1]);
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());

				dataView = (BaseBoardsVO)boardMngrService.getDetailOfBoard( searchVO );
				if( dataView != null ) { 
					// 자신의 작성글이거나 관리자 이상 권한자는 조회수 증가 제외
					if( !(BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) || dataView.getCreUser().equals(memberVO.getUserPID())) ) { 
						boardMngrService.doRtnOfModifyBoardHit( dataView );
					}

					findFile.setLnkName(lnkName);
					findFile.setLnkKey(viewNo[0] + viewNo[1]);
					// Data Adjustment(Data Files)
					dataFile = (List<BaseFilesVO>)baseCmmnService.getListOfDataFile( findFile );
					// Data Adjustment(Thumb Files)
					thmbFile = (List<BaseFilesVO>)baseCmmnService.getListOfThmbFile( findFile );
					if( !(thmbFile == null || thmbFile.size() <= 0) ) { 
						for( int f = 0; f < thmbFile.size(); f++ ) { 
							thmbFile.get(f).setFileImage(BaseUtility.getFileAllowOfImage( thmbFile.get(f).getFileAbs() ) ? "T" : null);
						}
					}

					// Data Adjustment(Boards)
					dataView.setActMode("Update");
					dataView.setViewNo(dataView.getBbsUUID());

					dataView.setBbsFiles(BaseUtility.toNumberFormat( dataView.getBbsFiles() ));
					dataView.setBgnDate(BaseUtility.toDateFormat( dataView.getBgnDate(), dtSep, 8 ));
					dataView.setEndDate(BaseUtility.toDateFormat( dataView.getEndDate(), dtSep, 8 ));
					if( !(BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) || dataView.getCreUser().equals(memberVO.getUserPID())) ) { 
						dataView.setHitCount(BaseUtility.toNumberFormat( Integer.parseInt(BaseUtility.null2Zero( dataView.getHitCount() )) + 1 ));
					} else { 
						dataView.setHitCount(BaseUtility.toNumberFormat( dataView.getHitCount() ));
					}
					dataView.setCreDate(BaseUtility.toDateFormat( dataView.getCreDate(), dtSep, 8 ));
					dataView.setModDate(BaseUtility.toDateFormat( dataView.getModDate(), dtSep, 8 ));
					dataView.setDelDate(BaseUtility.toDateFormat( dataView.getDelDate(), dtSep, 8 ));

					dataView.setBbsTitle(("Y".equals(dataView.getCreHide())?hideMark:"") + dataView.getBbsTitle());
					dataView.setContDescBR(BaseUtility.tag2Sym( dataView.getContDesc() ));
					dataView.setContDescCR(BaseUtility.sym2Tag( dataView.getContDesc() ));
					dataView.setContLinkC(BaseUtility.getUriCheck( dataView.getBbsSiteUri() ) ? "T" : "F");
					dataView.setCreLock(BaseUtility.null2Char( dataView.getCrePasswd(), "", "Y" ));
					dataView.setDownExt(FileProcess.getFileLowerExt( dataView.getDownRel() ));
					dataView.setFileUUID(BaseUtility.null2Char(dataView.getFileUUID(), "", "6"+ dataView.getFileUUID() ));
					dataView.setFileLinkS(FileProcess.getInfoOfFileBBS( request, response, dataFile, "S" ));
					dataView.setFileLinkM(FileProcess.getInfoOfFileBBS( request, response, dataFile, "M" ));
					dataView.setRegiDate(BaseUtility.toDateFormat( BaseUtility.null2Char( dataView.getRegiDate(), dataView.getCreDate() ), dtSep, 8 ));

					// 추가게시판 내역
					tempSupp = (BaseBoardsVO)boardMngrService.getDetailOfAdder( searchVO );
					if( tempSupp != null ) { 
						dataView.setConfCode1(tempSupp.getConfCode1());
						dataView.setConfName1((String)baseCmmnService.getInfoOfConfName( tempSupp.getConfCode1() ));
						dataView.setRegiDate1(BaseUtility.toDateFormat( tempSupp.getRegDate1(), dtSep, 8 ));

						tempList[0] = BaseUtility.isValidNull( tempSupp.getConfCode2() ) ? null : BaseUtility.null2Blank( tempSupp.getConfCode2() ).split("[◆]");
						tempList[1] = BaseUtility.isValidNull( tempSupp.getRegDate2()  ) ? null : BaseUtility.null2Blank( tempSupp.getRegDate2() ).split("[◆]");
						tempList[2] = BaseUtility.isValidNull( tempSupp.getRegName2()  ) ? null : BaseUtility.null2Blank( tempSupp.getRegName2() ).split("[◆]");
						tempList[3] = BaseUtility.isValidNull( tempSupp.getContent2()  ) ? null : BaseUtility.null2Blank( tempSupp.getContent2() ).split("[◆]");

						if( !(tempList[2] == null || tempList[2].length <= 0) ) { 
							tempList[0] = tempList[0] == null ? new String[tempList[2].length] : tempList[0];
							tempList[1] = tempList[1] == null ? new String[tempList[2].length] : tempList[1];
							tempList[3] = tempList[3] == null ? new String[tempList[2].length] : tempList[3];
							for( int i = 0; i < tempList[2].length; i++ ) { 
								tempView = new BaseBoardsVO();
								tempView.setConfCode2(BaseUtility.null2Blank( tempList[0][i] ));
								tempView.setConfName2((String)baseCmmnService.getInfoOfConfName( tempList[0][i] ));
								tempView.setRegiDate2(BaseUtility.toDateFormat( tempList[1][i], dtSep, 8 ));
								tempView.setRegiName2(BaseUtility.null2Blank( tempList[2][i] ));
								tempView.setContents2(BaseUtility.null2Blank( tempList[3][i] ));
								tempView.setContents2BR(BaseUtility.tag2Sym( tempList[3][i] ));
								tempView.setContents2CR(BaseUtility.sym2Tag( tempList[3][i] ));
								dataSupp.add(tempView);
							}
						}
					}

					// 상임위원회 구분
					if( "stand".equalsIgnoreCase(paramPath) && BaseUtility.isValidNull( dataView.getBbsCateName() ) ) { 
						if( "OPER".equals(dataView.getBbsCate())  ) dataView.setBbsCateName("의회운영위원회");
						if( "GVADM".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("기획행정위원회");
						if( "WELFARE".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("복지환경위원회");
						if( "BUILD".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("산업건설위원회");
					}

					// Data Adjustment(Move Items)
					if( moveItems && !BaseUtility.isValidNull( searchVO.getRowSeq() ) ) { 
						searchVO.setRowSeq(String.valueOf(Integer.parseInt(searchVO.getRowSeq()) + 1));
						prevView = (BaseBoardsVO)boardMngrService.getMovingOfBoard( searchVO );
						if( prevView != null ) { 
							prevView.setViewNo(prevView.getBbsUUID() + prevView.getRowSeq());
							prevView.setBbsTitle(("Y".equals(prevView.getCreHide())?hideMark:"") + prevView.getBbsTitle());
							prevView.setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( prevView.getAnsPos() )) <= 0 ? prevView.getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ prevView.getBbsTitle());
							prevView.setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( prevView.getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
						}
						searchVO.setRowSeq(String.valueOf(Integer.parseInt(searchVO.getRowSeq()) - 2));
						nextView = (BaseBoardsVO)boardMngrService.getMovingOfBoard( searchVO );
						if( nextView != null ) { 
							nextView.setViewNo(nextView.getBbsUUID() + nextView.getRowSeq());
							nextView.setBbsTitle(("Y".equals(nextView.getCreHide())?hideMark:"") + nextView.getBbsTitle());
							nextView.setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( nextView.getAnsPos() )) <= 0 ? nextView.getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ nextView.getBbsTitle());
							nextView.setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( nextView.getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
						}
					} else { 
						prevView = null;
						nextView = null;
					}
				}
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("dataFile"      , dataFile);
			model.addAttribute("thmbFile"      , thmbFile);
			model.addAttribute("moveItem"      , moveItems?"T":"F");
			model.addAttribute("prevView"      , prevView);
			model.addAttribute("nextView"      , nextView);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("dataSupp"      , dataSupp);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/"+retPath;
	}	// end of scrViewOfPhoto

	/**
	 * <pre>
	 * 게시판 등록화면으로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/PhotoInst.do")
	public String scrInstOfPhoto ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "board/photo"+ BaseUtility.toCapitalize( paramPath ) +"_inst";

		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
log.debug(" act: "+ BaseUtility.spaceFill( "Photo"+ BaseUtility.toCapitalize( paramPath ) +"Inst", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false ) ) return "/cmmn/exe_script";

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			retPath  = "board/photo"+ BaseUtility.toCapitalize( paramPath ) +"_inst";
		} else if( "coun".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110010";
			retPath  = "board/photo"+ BaseUtility.toCapitalize( paramPath ) +"_inst";
		} else if( "stand".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110020";
			retPath  = "stand/photo"+ BaseUtility.toCapitalize( paramPath ) +"_inst";
		} else if( "youth".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110030";
			retPath  = "youth/photo"+ BaseUtility.toCapitalize( paramPath ) +"_inst";
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("imgsUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		model.addAttribute("cateList"      , cateList);

		return "/mngr/"+retPath;
	}	// end of scrInstOfPhoto

	/**
	 * <pre>
	 * 게시판 수정화면으로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/PhotoEdit.do")
	public String scrEditOfPhoto ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "board/photo"+ BaseUtility.toCapitalize( paramPath ) +"_edit";

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		BaseBoardsVO			dataView     = new BaseBoardsVO();
		BaseCommonVO			findFile     = new BaseCommonVO();
		List<BaseFilesVO>		dataFile     = new ArrayList<BaseFilesVO>();
		List<BaseFilesVO>		thmbFile     = new ArrayList<BaseFilesVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		dataSupp     = new ArrayList<BaseCommonVO>();
		BaseBoardsVO			tempSupp     = new BaseBoardsVO();
		BaseBoardsVO			tempView     = new BaseBoardsVO();
		String[][]				tempList     = new String[4][10];
log.debug(" act: "+ BaseUtility.spaceFill( "Photo"+ BaseUtility.toCapitalize( paramPath ) +"Edit", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false, searchVO.getViewNo() ) ) return "/cmmn/exe_script";

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			retPath  = "board/photo"+ BaseUtility.toCapitalize( paramPath ) +"_edit";
		} else if( "coun".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110010";
			retPath  = "board/photo"+ BaseUtility.toCapitalize( paramPath ) +"_edit";
		} else if( "stand".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110020";
			retPath  = "stand/photo"+ BaseUtility.toCapitalize( paramPath ) +"_edit";
		} else if( "youth".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110030";
			retPath  = "youth/photo"+ BaseUtility.toCapitalize( paramPath ) +"_edit";
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("imgsUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)boardMngrService.getInfoOfBoardUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(게시판)
				searchVO.setBbsType(viewNo[0]);
				searchVO.setBbsSeq(viewNo[1]);
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());

				dataView = (BaseBoardsVO)boardMngrService.getDetailOfBoard( searchVO );
				if( dataView != null ) { 
					findFile.setLnkName(lnkName);
					findFile.setLnkKey(viewNo[0] + viewNo[1]);
					// Data Adjustment(Data Files)
					dataFile = (List<BaseFilesVO>)baseCmmnService.getListOfDataFile( findFile );
					// Data Adjustment(Thumb Files)
					thmbFile = (List<BaseFilesVO>)baseCmmnService.getListOfThmbFile( findFile );
					if( !(thmbFile == null || thmbFile.size() <= 0) ) { 
						for( int f = 0; f < thmbFile.size(); f++ ) { 
							thmbFile.get(f).setFileImage(BaseUtility.getFileAllowOfImage( thmbFile.get(f).getFileAbs() ) ? "T" : null);
						}
					}

					// Data Adjustment(Boards)
					dataView.setActMode("Update");
					dataView.setViewNo(dataView.getBbsUUID());

					dataView.setBbsFiles(BaseUtility.toNumberFormat( dataView.getBbsFiles() ));
					dataView.setBgnDate(BaseUtility.toDateFormat( dataView.getBgnDate(), dtSep, 8 ));
					dataView.setEndDate(BaseUtility.toDateFormat( dataView.getEndDate(), dtSep, 8 ));
					dataView.setHitCount(BaseUtility.toNumberFormat( dataView.getHitCount() ));
					dataView.setCreDate(BaseUtility.toDateFormat( dataView.getCreDate(), dtSep, 12 ));
					dataView.setModDate(BaseUtility.toDateFormat( dataView.getModDate(), dtSep, 12 ));
					dataView.setDelDate(BaseUtility.toDateFormat( dataView.getDelDate(), dtSep, 12 ));

					dataView.setContDescBR(BaseUtility.tag2Sym( dataView.getContDesc() ));
					dataView.setContDescCR(BaseUtility.sym2Tag( dataView.getContDesc() ));
					dataView.setContLinkC(BaseUtility.getUriCheck( dataView.getBbsSiteUri() ) ? "T" : "F");
					dataView.setCreLock(BaseUtility.null2Char( dataView.getCrePasswd(), "", "Y" ));
					dataView.setDownExt(FileProcess.getFileLowerExt( dataView.getDownRel() ));
					dataView.setFileUUID(BaseUtility.null2Char(dataView.getFileUUID(), "", "6"+ dataView.getFileUUID() ));
					dataView.setFileLinkS(FileProcess.getInfoOfFileBBS( request, response, dataFile, "S" ));
					dataView.setFileLinkM(FileProcess.getInfoOfFileBBS( request, response, dataFile, "M" ));
					dataView.setRegiDate(BaseUtility.toDateFormat( BaseUtility.null2Char( dataView.getRegiDate(), dataView.getCreDate() ), dtSep, 8 ));

					// 추가게시판 내역
					tempSupp = (BaseBoardsVO)boardMngrService.getDetailOfAdder( searchVO );
					if( tempSupp != null ) { 
						dataView.setConfCode1(tempSupp.getConfCode1());
						dataView.setConfName1((String)baseCmmnService.getInfoOfConfName( tempSupp.getConfCode1() ));
						dataView.setRegiDate1(BaseUtility.toDateFormat( tempSupp.getRegDate1(), dtSep, 8 ));

						tempList[0] = BaseUtility.isValidNull( tempSupp.getConfCode2() ) ? null : BaseUtility.null2Blank( tempSupp.getConfCode2() ).split("[◆]");
						tempList[1] = BaseUtility.isValidNull( tempSupp.getRegDate2()  ) ? null : BaseUtility.null2Blank( tempSupp.getRegDate2() ).split("[◆]");
						tempList[2] = BaseUtility.isValidNull( tempSupp.getRegName2()  ) ? null : BaseUtility.null2Blank( tempSupp.getRegName2() ).split("[◆]");
						tempList[3] = BaseUtility.isValidNull( tempSupp.getContent2()  ) ? null : BaseUtility.null2Blank( tempSupp.getContent2() ).split("[◆]");

						if( !(tempList[2] == null || tempList[2].length <= 0) ) { 
							tempList[0] = tempList[0] == null ? new String[tempList[2].length] : tempList[0];
							tempList[1] = tempList[1] == null ? new String[tempList[2].length] : tempList[1];
							tempList[3] = tempList[3] == null ? new String[tempList[2].length] : tempList[3];
							for( int i = 0; i < tempList[2].length; i++ ) { 
								tempView = new BaseBoardsVO();
								tempView.setConfCode2(BaseUtility.null2Blank( tempList[0][i] ));
								tempView.setConfName2((String)baseCmmnService.getInfoOfConfName( tempList[0][i] ));
								tempView.setRegiDate2(BaseUtility.toDateFormat( tempList[1][i], dtSep, 8 ));
								tempView.setRegiName2(BaseUtility.null2Blank( tempList[2][i] ));
								tempView.setContents2(BaseUtility.null2Blank( tempList[3][i] ));
								tempView.setContents2BR(BaseUtility.tag2Sym( tempList[3][i] ));
								tempView.setContents2CR(BaseUtility.sym2Tag( tempList[3][i] ));
								dataSupp.add(tempView);
							}
						}
					}

					// 상임위원회 구분
					if( "stand".equalsIgnoreCase(paramPath) && BaseUtility.isValidNull( dataView.getBbsCateName() ) ) { 
						if( "OPER".equals(dataView.getBbsCate())  ) dataView.setBbsCateName("의회운영위원회");
						if( "GVADM".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("기획행정위원회");
						if( "WELFARE".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("복지환경위원회");
						if( "BUILD".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("산업건설위원회");
					}
				}
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("dataFile"      , dataFile);
			model.addAttribute("thmbFile"      , thmbFile);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("dataSupp"      , dataSupp);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/"+retPath;
	}	// end of scrEditOfPhoto

	/**
	 * <pre>
	 * 게시판 답변화면으로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/PhotoReEdit.do")
	public String scrReplyOfPhoto ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "board/photo"+ BaseUtility.toCapitalize( paramPath ) +"_repy";

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		BaseBoardsVO			dataView     = new BaseBoardsVO();
		BaseCommonVO			findFile     = new BaseCommonVO();
		List<BaseFilesVO>		dataFile     = new ArrayList<BaseFilesVO>();
		List<BaseFilesVO>		thmbFile     = new ArrayList<BaseFilesVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		dataSupp     = new ArrayList<BaseCommonVO>();
		BaseBoardsVO			tempSupp     = new BaseBoardsVO();
		BaseBoardsVO			tempView     = new BaseBoardsVO();
		String[][]				tempList     = new String[4][10];
log.debug(" act: "+ BaseUtility.spaceFill( "Photo"+ BaseUtility.toCapitalize( paramPath ) +"ReEdit", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false, searchVO.getViewNo() ) ) return "/cmmn/exe_script";

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			retPath  = "board/photo"+ BaseUtility.toCapitalize( paramPath ) +"_repy";
		} else if( "coun".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110010";
			retPath  = "board/photo"+ BaseUtility.toCapitalize( paramPath ) +"_repy";
		} else if( "stand".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110020";
			retPath  = "stand/photo"+ BaseUtility.toCapitalize( paramPath ) +"_repy";
		} else if( "youth".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110030";
			retPath  = "youth/photo"+ BaseUtility.toCapitalize( paramPath ) +"_repy";
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("imgsUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)boardMngrService.getInfoOfBoardUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(게시판)
				searchVO.setBbsType(viewNo[0]);
				searchVO.setBbsSeq(viewNo[1]);
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());

				dataView = (BaseBoardsVO)boardMngrService.getDetailOfBoard( searchVO );
				if( dataView != null ) { 
					findFile.setLnkName(lnkName);
					findFile.setLnkKey(viewNo[0] + viewNo[1]);
					// Data Adjustment(Data Files)
					dataFile = (List<BaseFilesVO>)baseCmmnService.getListOfDataFile( findFile );
					// Data Adjustment(Thumb Files)
					thmbFile = (List<BaseFilesVO>)baseCmmnService.getListOfThmbFile( findFile );
					if( !(thmbFile == null || thmbFile.size() <= 0) ) { 
						for( int f = 0; f < thmbFile.size(); f++ ) { 
							thmbFile.get(f).setFileImage(BaseUtility.getFileAllowOfImage( thmbFile.get(f).getFileAbs() ) ? "T" : null);
						}
					}

					// Data Adjustment(Boards)
					dataView.setActMode("Update");
					dataView.setViewNo(dataView.getBbsUUID());

					dataView.setBbsFiles(BaseUtility.toNumberFormat( dataView.getBbsFiles() ));
					dataView.setBgnDate(BaseUtility.toDateFormat( dataView.getBgnDate(), dtSep, 8 ));
					dataView.setEndDate(BaseUtility.toDateFormat( dataView.getEndDate(), dtSep, 8 ));
					dataView.setHitCount(BaseUtility.toNumberFormat( dataView.getHitCount() ));
					dataView.setCreDate(BaseUtility.toDateFormat( dataView.getCreDate(), dtSep, 12 ));
					dataView.setModDate(BaseUtility.toDateFormat( dataView.getModDate(), dtSep, 12 ));
					dataView.setDelDate(BaseUtility.toDateFormat( dataView.getDelDate(), dtSep, 12 ));

					dataView.setContDescBR(BaseUtility.tag2Sym( dataView.getContDesc() ));
					dataView.setContDescCR(BaseUtility.sym2Tag( dataView.getContDesc() ));
					dataView.setContLinkC(BaseUtility.getUriCheck( dataView.getBbsSiteUri() ) ? "T" : "F");
					dataView.setCreLock(BaseUtility.null2Char( dataView.getCrePasswd(), "", "Y" ));
					dataView.setDownExt(FileProcess.getFileLowerExt( dataView.getDownRel() ));
					dataView.setFileUUID(BaseUtility.null2Char(dataView.getFileUUID(), "", "6"+ dataView.getFileUUID() ));
					dataView.setFileLinkS(FileProcess.getInfoOfFileBBS( request, response, dataFile, "S" ));
					dataView.setFileLinkM(FileProcess.getInfoOfFileBBS( request, response, dataFile, "M" ));
					dataView.setRegiDate(BaseUtility.toDateFormat( BaseUtility.null2Char( dataView.getRegiDate(), dataView.getCreDate() ), dtSep, 8 ));

					// 추가게시판 내역
					tempSupp = (BaseBoardsVO)boardMngrService.getDetailOfAdder( searchVO );
					if( tempSupp != null ) { 
						dataView.setConfCode1(tempSupp.getConfCode1());
						dataView.setConfName1((String)baseCmmnService.getInfoOfConfName( tempSupp.getConfCode1() ));
						dataView.setRegiDate1(BaseUtility.toDateFormat( tempSupp.getRegDate1(), dtSep, 8 ));

						tempList[0] = BaseUtility.isValidNull( tempSupp.getConfCode2() ) ? null : BaseUtility.null2Blank( tempSupp.getConfCode2() ).split("[◆]");
						tempList[1] = BaseUtility.isValidNull( tempSupp.getRegDate2()  ) ? null : BaseUtility.null2Blank( tempSupp.getRegDate2() ).split("[◆]");
						tempList[2] = BaseUtility.isValidNull( tempSupp.getRegName2()  ) ? null : BaseUtility.null2Blank( tempSupp.getRegName2() ).split("[◆]");
						tempList[3] = BaseUtility.isValidNull( tempSupp.getContent2()  ) ? null : BaseUtility.null2Blank( tempSupp.getContent2() ).split("[◆]");

						if( !(tempList[2] == null || tempList[2].length <= 0) ) { 
							tempList[0] = tempList[0] == null ? new String[tempList[2].length] : tempList[0];
							tempList[1] = tempList[1] == null ? new String[tempList[2].length] : tempList[1];
							tempList[3] = tempList[3] == null ? new String[tempList[2].length] : tempList[3];
							for( int i = 0; i < tempList[2].length; i++ ) { 
								tempView = new BaseBoardsVO();
								tempView.setConfCode2(BaseUtility.null2Blank( tempList[0][i] ));
								tempView.setConfName2((String)baseCmmnService.getInfoOfConfName( tempList[0][i] ));
								tempView.setRegiDate2(BaseUtility.toDateFormat( tempList[1][i], dtSep, 8 ));
								tempView.setRegiName2(BaseUtility.null2Blank( tempList[2][i] ));
								tempView.setContents2(BaseUtility.null2Blank( tempList[3][i] ));
								tempView.setContents2BR(BaseUtility.tag2Sym( tempList[3][i] ));
								tempView.setContents2CR(BaseUtility.sym2Tag( tempList[3][i] ));
								dataSupp.add(tempView);
							}
						}
					}

					// 상임위원회 구분
					if( "stand".equalsIgnoreCase(paramPath) && BaseUtility.isValidNull( dataView.getBbsCateName() ) ) { 
						if( "OPER".equals(dataView.getBbsCate())  ) dataView.setBbsCateName("의회운영위원회");
						if( "GVADM".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("기획행정위원회");
						if( "WELFARE".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("복지환경위원회");
						if( "BUILD".equals(dataView.getBbsCate()) ) dataView.setBbsCateName("산업건설위원회");
					}
				}
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("dataFile"      , dataFile);
			model.addAttribute("thmbFile"      , thmbFile);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("dataSupp"      , dataSupp);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/"+retPath;
	}	// end of scrReplyOfPhoto

	/**
	 * <pre>
	 * 게시판 일괄등록을 위한 엑셀 샘플자료를 다운로드해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
//	@RequestMapping(value="/PhotoSample.do")
	public String getSampleOfPhoto ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseFilesVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		String[]				fileName     = {"temp_Photo.xlsx|","게시판_양식.xlsx|"};
log.debug(" act: "+ BaseUtility.spaceFill( "Photo"+ BaseUtility.toCapitalize( paramPath ) +"Sample", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

log.debug("fileName: "+ fileName[1]);
		try { 
			model.remove("searchVO");
			searchVO.setFileRel(fileName[0]);
			searchVO.setFileAbs(fileName[1]);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return "redirect:/cmmn/TempDown.do";
	}	// end of getSampleOfPhoto

	/**
	 * <pre>
	 * 게시판 내역을 엑셀출력해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
//	@RequestMapping(value="/PhotoExport.do")
	public View getExportOfPhoto ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		int						listCnt      = 0;
		List<BaseBoardsVO>		dataList     = new ArrayList<BaseBoardsVO>();

		String					fileName   = "PhotoXLS";
		String					dataName   = "게시판 내역";
		List<String>			colName    = new ArrayList<String>();
		List<String>			colRegion  = new ArrayList<String>();
		List<String>			colAlign   = new ArrayList<String>();
		List<String[]>			colValue   = new ArrayList<String[]>();
		List<Integer>			colWidth   = new ArrayList<Integer>();
log.debug(" act: "+ BaseUtility.spaceFill( "Photo"+ BaseUtility.toCapitalize( paramPath ) +"Export", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false ) ) return null;

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
		} else if( "coun".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110010";
		} else if( "stand".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110020";
		} else if( "youth".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110030";
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
//		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
//		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("imgsUnit") ));
//		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setBbsType(BaseUtility.null2Char( searchVO.getBbsType(), bbsType ));
//		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("bbsType: "+ searchVO.getBbsType());
		try { 
			listCnt = (int)boardMngrService.getCountOfBoard( searchVO );

			if( listCnt > 0 ) { 
				colName.clear();
				colRegion.clear();
				colAlign.clear();
				colWidth.clear();
				colValue.clear();

				// 상단제목
				colName.add("No");
				colName.add("제목");
				colName.add("작성자");
				colName.add("적성일");
				colName.add("조회수");

				// 행증가,시작열,종료열
				colRegion.add("0`0`0");
				colRegion.add("0`1`1");
				colRegion.add("0`2`2");
				colRegion.add("0`3`3");
				colRegion.add("0`4`4");

				// 내용 가로정렬
				colAlign.add("center");
				colAlign.add("left");
				colAlign.add("center");
				colAlign.add("center");
				colAlign.add("center");

				// 열너비
				colWidth.add(100);	// 순번
				colWidth.add(750);	// 제목
				colWidth.add(225);	// 작성자
				colWidth.add(200);	// 작성일
				colWidth.add(100);	// 조회수

				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
				dataList = (List<BaseBoardsVO>)boardMngrService.getListOfBoard( searchVO );
//				dataList = (List<BaseBoardsVO>)boardMngrService.getListOfBoard( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				// Data Adjustment(Excels)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					for( int d = 0; d < dataList.size(); d++ ) { 
						String[] array = {Integer.toString(d+1) 
										, dataList.get(d).getBbsTitle() 
										, dataList.get(d).getCreName() 
										, BaseUtility.toDateFormat( dataList.get(d).getCreDate(), dtSep, 8 ) 
										, BaseUtility.toNumberFormat( dataList.get(d).getHitCount() ) 
						};
						colValue.add(array);
					}
				}

				model.put("relFile"     , fileName);
				model.put("absFile"     , dataName);
				model.put("titLine"     , "2");
				model.put("colName"     , colName);
				model.put("colRegion"   , colRegion);
				model.put("colAlign"    , colAlign);
				model.put("colWidth"    , colWidth);
				model.put("colValue"    , colValue);
			}
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return new ExcelBuilderPOI();
	}	// end of getExportOfPhoto

	/**
	 * <pre>
	 * 게시판 내역을 등록처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  MultipartHttpServletRequest		// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/PhotoCreate.do")
	public ModelAndView doActOfCreatePhoto ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseBoardsVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			final MultipartHttpServletRequest mpRequest, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

//		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
//		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseBoardsVO			dataView     = new BaseBoardsVO();
		Map<String, MultipartFile> fileMap   = mpRequest == null ? null : mpRequest.getFileMap();
		int						fileCnt      = 0;
		int						thmbCnt      = 0;
log.debug(" act: "+ BaseUtility.spaceFill( "Photo"+ BaseUtility.toCapitalize( paramPath ) +"Create", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, false ) ) return mav;

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.PhotoBase" );
		} else if( "coun".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110010";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.PhotoCoun" );
		} else if( "stand".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110020";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.PhotoStand" );
		} else if( "youth".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110030";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.PhotoYouth" );
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("imgsUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setBbsType(BaseUtility.null2Char( searchVO.getBbsType(), bbsType ));

//log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/board/photo"+ BaseUtility.toCapitalize( paramPath ) +"_inst";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			// 지정된 파일만 업로드 처리
			if( !(fileMap == null || fileMap.size() <= 0) && fileUP ) for( MultipartFile mFile : fileMap.values() ) { 
				if( !(mFile.isEmpty() || mFile.getSize() < 0) && mFile.getName().contains("fileThmb") && !BaseUtility.getFileAllowOfImage( mFile.getOriginalFilename() ) ) { 
//					model.addAttribute("error"         , "첨부 불가능한 이미지파일이 존재합니다.");
//					return "/cmmn/exe_script";
					mav.addObject("message"       , "첨부 불가능한 이미지파일이 존재합니다.").addObject("isError"       , "T");
					return mav;
				}
			}

			// 입력내역 취득(게시판)
			dataView = (BaseBoardsVO)boardMngrService.getInfoOfBoardSeq( searchVO );
			searchVO.setBbsSeq(dataView == null ? "1" : dataView.getBbsSeq());
			searchVO.setBbsUUID(BaseCategory.encryptSHA256( searchVO.getBbsType() + searchVO.getBbsSeq() ).toUpperCase());
			searchVO.setBgnDate(BaseUtility.null2Char( searchVO.getBgnDate(), systemVO.getToDate() ).replaceAll("[^0-9]", ""));
			searchVO.setEndDate(BaseUtility.null2Char( searchVO.getEndDate(), "20991231" ).replaceAll("[^0-9]", ""));
			searchVO.setContSize(String.valueOf(BaseUtility.removeAllTags( searchVO.getContDesc() ).length()));
			searchVO.setContPattern("P");
			searchVO.setContCrypt("F");
			searchVO.setHitCount("0");
			searchVO.setAnsRef(searchVO.getBbsSeq());
			searchVO.setAnsStep("0");
			searchVO.setAnsPos("0");
			searchVO.setCreOrder("on".equalsIgnoreCase(BaseUtility.null2Blank( searchVO.getCreOrder() )) ? searchVO.getBbsSeq() : null);
//			searchVO.setCreHide("on".equalsIgnoreCase(BaseUtility.null2Blank( searchVO.getCreHide() )) ? "Y" : BaseUtility.null2Blank( searchVO.getCreHide() ));
			searchVO.setCreIPv4(BaseUtility.reqIPv4Address( request ));
			searchVO.setModIPv4(BaseUtility.reqIPv4Address( request ));
			searchVO.setCreUser(BaseUtility.null2Char( searchVO.getCreUser(), memberVO.getUserPID() ));
			searchVO.setCreName(BaseUtility.null2Char( searchVO.getCreName(), memberVO.getUserName() ));
			searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
			searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));
			searchVO.setViewNo(searchVO.getBbsUUID() +"1");
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());

			// 썸네일 갱신처리
			if( fileUP ) thmbCnt = FileProcess.doActOfCreateFileThmb( request, response, baseCmmnService, systemVO, memberVO, fileMap, fileCnt, filePath, lnkName, searchVO.getBbsType() + searchVO.getBbsSeq() );
			// 첨부파일 갱신처리
			if( fileUP ) fileCnt = FileProcess.doActOfCreateFileBase( request, response, baseCmmnService, systemVO, memberVO, fileMap, fileCnt, filePath, lnkName, searchVO.getBbsType() + searchVO.getBbsSeq() );

			// 게시판 내역 갱신
			searchVO.setBbsFiles(String.valueOf(fileCnt));
			boardMngrService.doRtnOfCreateBoard( searchVO );
			model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//-		return "redirect:/ptnhexa/"+ BaseUtility.toCapitalize( paramPath ) +"/PhotoView.do";
//		return "redirect:/ptnhexa/"+ BaseUtility.toCapitalize( paramPath ) +"/PhotoList.do";
		return mav;
	}	// end of doActOfCreatePhoto

	/**
	 * <pre>
	 * 게시판 내역을 수정처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  MultipartHttpServletRequest		// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/PhotoUpdate.do")
	public ModelAndView doActOfUpdatePhoto ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseBoardsVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			final MultipartHttpServletRequest mpRequest, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseBoardsVO			dataView     = new BaseBoardsVO();
		Map<String, MultipartFile> fileMap   = mpRequest == null ? null : mpRequest.getFileMap();
		int						fileCnt      = 0;
		int						thmbCnt      = 0;
		String[]				deleList     = null;
		BaseFilesVO				fileDele     = new BaseFilesVO();
log.debug(" act: "+ BaseUtility.spaceFill( "Photo"+ BaseUtility.toCapitalize( paramPath ) +"Update", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, false, searchVO.getViewNo() ) ) return mav;

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.PhotoBase" );
		} else if( "coun".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110010";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.PhotoCoun" );
		} else if( "stand".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110020";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.PhotoStand" );
		} else if( "youth".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110030";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.PhotoYouth" );
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("imgsUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/board/photo"+ BaseUtility.toCapitalize( paramPath ) +"_edit";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			// 지정된 파일만 업로드 처리
			if( !(fileMap == null || fileMap.size() <= 0) && fileUP ) for( MultipartFile mFile : fileMap.values() ) { 
				if( !(mFile.isEmpty() || mFile.getSize() < 0) && mFile.getName().contains("fileThmb") && !BaseUtility.getFileAllowOfImage( mFile.getOriginalFilename() ) ) { 
//					model.addAttribute("error"         , "첨부 불가능한 이미지파일이 존재합니다.");
//					return "/cmmn/exe_script";
					mav.addObject("message"       , "첨부 불가능한 이미지파일이 존재합니다.").addObject("isError"       , "T");
					return mav;
				}
			}

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)boardMngrService.getInfoOfBoardUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(게시판)
				searchVO.setBbsType(viewNo[0]);
				searchVO.setBbsSeq(viewNo[1]);
				searchVO.setBgnDate(BaseUtility.null2Blank( searchVO.getBgnDate() ).replaceAll("[^0-9]", ""));
				searchVO.setEndDate(BaseUtility.null2Blank( searchVO.getEndDate() ).replaceAll("[^0-9]", ""));
				searchVO.setContSize(String.valueOf(BaseUtility.removeAllTags( searchVO.getContDesc() ).length()));
				searchVO.setCreOrder("on".equalsIgnoreCase(BaseUtility.null2Blank( searchVO.getCreOrder() )) ? searchVO.getBbsSeq() : null);
//				searchVO.setCreHide("on".equalsIgnoreCase(BaseUtility.null2Blank( searchVO.getCreHide() )) ? "Y" : BaseUtility.null2Blank( searchVO.getCreHide() ));
				searchVO.setModIPv4(BaseUtility.reqIPv4Address( request ));
				searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
				searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq() +" | "+ searchVO.getCondDele());

				// 첨부파일 삭제처리
				if( fileUP ) { 
					deleList = BaseUtility.isValidNull( searchVO.getCondDele() ) ? null : BaseUtility.null2Blank( searchVO.getCondDele() ).split("`");
					if( !(deleList == null || deleList.length <= 0) ) { 
						for( int f = 0; f < deleList.length; f++ ) { 
							fileDele = new BaseFilesVO();
							fileDele.setRowUUID(deleList[f]);
							fileDele.setDelIPv4(BaseUtility.reqIPv4Address( request ));
							fileDele.setDelUser(BaseUtility.null2Char( searchVO.getDelUser(), memberVO.getUserPID() ));
							fileDele.setDelName(BaseUtility.null2Char( searchVO.getDelName(), memberVO.getUserName() ));

							baseCmmnService.doRtnOfDeleteDataFile( fileDele );
							fileCnt--;
						}
					}
				}
				// 썸네일 갱신처리
				if( fileUP ) thmbCnt = FileProcess.doActOfCreateFileThmb( request, response, baseCmmnService, systemVO, memberVO, fileMap, fileCnt, filePath, lnkName, searchVO.getBbsType() + searchVO.getBbsSeq() );
				// 첨부파일 갱신처리
				if( fileUP ) fileCnt = FileProcess.doActOfCreateFileBase( request, response, baseCmmnService, systemVO, memberVO, fileMap, fileCnt, filePath, lnkName, searchVO.getBbsType() + searchVO.getBbsSeq() );

				// 게시판 내역 갱신
				searchVO.setBbsFiles(String.valueOf(fileCnt));
				boardMngrService.doRtnOfUpdateBoard( searchVO );
				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//-		return "redirect:/ptnhexa/"+ BaseUtility.toCapitalize( paramPath ) +"/PhotoView.do";
//		return "redirect:/ptnhexa/"+ BaseUtility.toCapitalize( paramPath ) +"/PhotoList.do";
		return mav;
	}	// end of doActOfUpdatePhoto

	/**
	 * <pre>
	 * 게시판 내역을 삭제처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/PhotoDelete.do")
	public ModelAndView doActOfDeletePhoto ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseBoardsVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseBoardsVO			dataView     = new BaseBoardsVO();
log.debug(" act: "+ BaseUtility.spaceFill( "Photo"+ BaseUtility.toCapitalize( paramPath ) +"Delete", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, false, searchVO.getViewNo() ) ) return mav;

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
		} else if( "coun".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110010";
		} else if( "stand".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110020";
		} else if( "youth".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110030";
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("imgsUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)boardMngrService.getInfoOfBoardUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(게시판)
				searchVO.setBbsType(viewNo[0]);
				searchVO.setBbsSeq(viewNo[1]);
				searchVO.setDelIPv4(BaseUtility.reqIPv4Address( request ));
				searchVO.setDelUser(BaseUtility.null2Char( searchVO.getDelUser(), memberVO.getUserPID() ));
				searchVO.setDelName(BaseUtility.null2Char( searchVO.getDelName(), memberVO.getUserName() ));
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());

				// 게시판 내역 삭제
				boardMngrService.doRtnOfDeleteBoard( searchVO );
				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/ptnhexa/"+ BaseUtility.toCapitalize( paramPath ) +"/PhotoList.do";
		return mav;
	}	// end of doActOfDeletePhoto

	/**
	 * <pre>
	 * 게시판 내역을 변경처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return String
	 * @throws Exception
	 */
	@ResponseBody
//	@RequestMapping(value="/PhotoModify.do")
	public ModelAndView doActOfModifyPhoto ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseBoardsVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseBoardsVO			dataView     = new BaseBoardsVO();
log.debug(" act: "+ BaseUtility.spaceFill( "Photo"+ BaseUtility.toCapitalize( paramPath ) +"Modify", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, false, searchVO.getViewNo() ) ) return mav;

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
		} else if( "coun".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110010";
		} else if( "stand".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110020";
		} else if( "youth".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110030";
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("imgsUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/board/photo"+ BaseUtility.toCapitalize( paramPath ) +"_edit";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)boardMngrService.getInfoOfBoardUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(게시판)
				searchVO.setBbsType(viewNo[0]);
				searchVO.setBbsSeq(viewNo[1]);
				searchVO.setModIPv4(BaseUtility.reqIPv4Address( request ));
				searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
				searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());

				// 게시판 내역 갱신
				boardMngrService.doRtnOfModifyBoard( searchVO );
				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/ptnhexa/"+ BaseUtility.toCapitalize( paramPath ) +"/PhotoList.do";
		return mav;
	}	// end of doActOfModifyPhoto

	/**
	 * <pre>
	 * 게시판 내역을 답변처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  MultipartHttpServletRequest		// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/PhotoReply.do")
	public ModelAndView doActOfReplyPhoto ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseBoardsVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			final MultipartHttpServletRequest mpRequest, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

//		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
//		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseBoardsVO			dataView     = new BaseBoardsVO();
		Map<String, MultipartFile> fileMap   = mpRequest == null ? null : mpRequest.getFileMap();
		int						fileCnt      = 0;
		int						thmbCnt      = 0;
		BaseBoardsVO			qustView     = new BaseBoardsVO();
		BaseBoardsVO			stepView     = new BaseBoardsVO();
log.debug(" act: "+ BaseUtility.spaceFill( "Photo"+ BaseUtility.toCapitalize( paramPath ) +"Reply", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, false, searchVO.getViewNo() ) ) return mav;

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.PhotoBase" );
		} else if( "coun".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110010";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.PhotoCoun" );
		} else if( "stand".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110020";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.PhotoStand" );
		} else if( "youth".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110030";
			filePath = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.PhotoYouth" );
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("imgsUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setBbsType(BaseUtility.null2Char( searchVO.getBbsType(), bbsType ));

log.debug("bbsType: "+ searchVO.getBbsType() +" |bbsSeq: "+ searchVO.getBbsSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/board/photo"+ BaseUtility.toCapitalize( paramPath ) +"_repy";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			// 지정된 파일만 업로드 처리
			if( !(fileMap == null || fileMap.size() <= 0) && fileUP ) for( MultipartFile mFile : fileMap.values() ) { 
				if( !(mFile.isEmpty() || mFile.getSize() < 0) && mFile.getName().contains("fileThmb") && !BaseUtility.getFileAllowOfImage( mFile.getOriginalFilename() ) ) { 
//					model.addAttribute("error"         , "첨부 불가능한 이미지파일이 존재합니다.");
//					return "/cmmn/exe_script";
					mav.addObject("message"       , "첨부 불가능한 이미지파일이 존재합니다.").addObject("isError"       , "T");
					return mav;
				}
			}

			// 질문내역 취득
			qustView = (BaseBoardsVO)boardMngrService.getDetailOfBoard( searchVO );
			stepView = (BaseBoardsVO)boardMngrService.getInfoOfBoardStepMin( qustView );
			if( stepView == null || "0".equals(stepView.getAnsStep()) ) { 
				stepView = (BaseBoardsVO)boardMngrService.getInfoOfBoardStepMax( qustView );
				searchVO.setAnsRef(qustView.getAnsRef());
				searchVO.setAnsStep(stepView.getAnsStep());
				searchVO.setAnsPos(String.valueOf(Integer.parseInt(qustView.getAnsPos())+1));
			} else { 
				boardMngrService.doRtnOfModifyBoardStep( stepView );
				searchVO.setAnsRef(qustView.getAnsRef());
				searchVO.setAnsStep(stepView.getAnsStep());
				searchVO.setAnsPos(String.valueOf(Integer.parseInt(qustView.getAnsPos())+1));
			}

			// 입력내역 취득(게시판)
			dataView = (BaseBoardsVO)boardMngrService.getInfoOfBoardSeq( searchVO );
			searchVO.setBbsSeq(dataView == null ? "1" : dataView.getBbsSeq());
			searchVO.setBbsUUID(BaseCategory.encryptSHA256( searchVO.getBbsType() + searchVO.getBbsSeq() ).toUpperCase());
			searchVO.setBgnDate(BaseUtility.null2Char( searchVO.getBgnDate(), systemVO.getToDate() ).replaceAll("[^0-9]", ""));
			searchVO.setEndDate(BaseUtility.null2Char( searchVO.getEndDate(), "20991231" ).replaceAll("[^0-9]", ""));
			searchVO.setContSize(String.valueOf(BaseUtility.removeAllTags( searchVO.getContDesc() ).length()));
			searchVO.setContPattern("P");
			searchVO.setContCrypt("F");
			searchVO.setHitCount("0");
			searchVO.setCreOrder("on".equalsIgnoreCase(BaseUtility.null2Blank( searchVO.getCreOrder() )) ? searchVO.getBbsSeq() : null);
			searchVO.setCrePasswd(qustView.getCrePasswd());
			searchVO.setCreHide(qustView.getCreHide());
			searchVO.setCreIPv4(BaseUtility.reqIPv4Address( request ));
			searchVO.setModIPv4(BaseUtility.reqIPv4Address( request ));
			searchVO.setCreUser(BaseUtility.null2Char( searchVO.getCreUser(), memberVO.getUserPID() ));
			searchVO.setCreName(BaseUtility.null2Char( searchVO.getCreName(), memberVO.getUserName() ));
			searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
			searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));
			searchVO.setViewNo(dataView.getBbsUUID() +"1");
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());

			// 썸네일 갱신처리
			if( fileUP ) thmbCnt = FileProcess.doActOfCreateFileThmb( request, response, baseCmmnService, systemVO, memberVO, fileMap, fileCnt, filePath, lnkName, searchVO.getBbsType() + searchVO.getBbsSeq() );
			// 첨부파일 갱신처리
			if( fileUP ) fileCnt = FileProcess.doActOfCreateFileBase( request, response, baseCmmnService, systemVO, memberVO, fileMap, fileCnt, filePath, lnkName, searchVO.getBbsType() + searchVO.getBbsSeq() );

			// 게시판 내역 갱신
			searchVO.setBbsFiles(String.valueOf(fileCnt));
			boardMngrService.doRtnOfCreateBoard( searchVO );
			model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//-		return "redirect:/ptnhexa/"+ BaseUtility.toCapitalize( paramPath ) +"/PhotoView.do";
//		return "redirect:/ptnhexa/"+ BaseUtility.toCapitalize( paramPath ) +"/PhotoList.do";
		return mav;
	}	// end of doActOfReplyPhoto

//	@ExceptionHandler(MaxUploadSizeExceededException.class)
	public ModelAndView resolveException(HttpServletRequest request,HttpServletResponse response,Object handler,Exception exception){
		ModelAndView			mav          = new ModelAndView("jsonView");
		if(exception instanceof MaxUploadSizeExceededException)
			mav.addObject("message"       , "첨부파일 사이즈는 "+ BaseUtility.toNumberFormat( (((MaxUploadSizeExceededException)exception).getMaxUploadSize()/1024/1024) ) +"MB를 초과할 수 없습니다.").addObject("isError"       , "T");
		return mav;
	}

}	// end of class