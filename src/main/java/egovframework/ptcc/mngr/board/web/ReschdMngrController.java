/*******************************************************************************
  Program ID  : ReschdMngrController
  Description : 통합일정 관리에 관한 controller 클래스를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.10.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.10.01  최초 생성
  v1.0    HexaMedia new0man      2019.10.01  평택시의회 Site Renewal
  v1.1    HexaMedia new0man      2020.04.01  일정관리에서 일요일 및 토요일 포함여부
 *******************************************************************************/
package egovframework.ptcc.mngr.board.web;

import java.util.*;

import javax.annotation.*;
import javax.servlet.http.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.dao.*;
import org.springframework.jdbc.*;
import org.springframework.jdbc.datasource.*;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.*;
import org.springframework.transaction.support.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.*;
import org.springmodules.validation.commons.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cmm.util.*;
//import egovframework.ptcc.cmmn.excel.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.util.*;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.board.service.*;

@Controller
@RequestMapping(value="/ptnhexa/Schd")
public class ReschdMngrController {

	@Autowired
	private WebApplicationContext			webContext;
	@Autowired
	private DataSourceTransactionManager	txManager;

	@Resource(name="beanValidator")
	protected DefaultBeanValidator			beanValidator;
	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="reschdMngrService")
	protected ReschdMngrService			reschdMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));
	protected String						defsPass     = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsPass" );
	protected String						lnkName      = "PT_RESCHD";
	protected boolean						notiItems    = false;
	protected boolean						moveItems    = false;
	protected boolean						fileUP       = false;
	protected boolean						tempUP       = false;

	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelMap model, 
			boolean connMode 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			model.addAttribute("message"       , "로그인 정보가 존재하지 않습니다.\\n\\n로그인 화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			model.addAttribute("message"       , "임시 비밀번호를 사용하고 있습니다.\\n\\n비밀번호 변경화면으로 이동합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			model.addAttribute("close"         , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			model.addAttribute("error"         , "접속권한이 없습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		return false;
	}
	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelMap model, 
			boolean connMode, 
			String viewNo 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			model.addAttribute("message"       , "로그인 정보가 존재하지 않습니다.\\n\\n로그인 화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			model.addAttribute("message"       , "임시 비밀번호를 사용하고 있습니다.\\n\\n비밀번호 변경화면으로 이동합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			model.addAttribute("close"         , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			model.addAttribute("error"         , "접속권한이 없습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 불량 접속 확인
		if( BaseUtility.isValidNull( viewNo ) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		return false;
	}
	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelAndView mav, 
			boolean connMode 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "로그인 정보가 존재하지 않습니다.\n\n로그인 화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "임시 비밀번호를 사용하고 있습니다.\n\n비밀번호 변경화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속권한이 없습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		return false;
	}
	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelAndView mav, 
			boolean connMode, 
			String viewNo 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "로그인 정보가 존재하지 않습니다.\n\n로그인 화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "임시 비밀번호를 사용하고 있습니다.\n\n비밀번호 변경화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속권한이 없습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 불량 접속 확인
		if( BaseUtility.isValidNull( viewNo ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "잘못된 경로로 접속하였습니다.\n\n메인화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		return false;
	}

	private String getSchdTypeName ( 
			String argType, 
			String argValue 
		) throws Exception { 

		String					retS         = "";

		try { 
			if( BaseUtility.isValidNull( argValue ) ) { 
				retS = "";
			} else if( "CL".equals(argValue) ) { 
				retS = "S".equals(argType) ? "생방(본)"   : ("N".equals(argType) ? "본회의"   : "생 방 송 (본회의)");
			} else if( "OL".equals(argValue) ) { 
				retS = "S".equals(argType) ? "생방(운영)" : ("N".equals(argType) ? "운영위"   : "생 방 송 (의회운영위원회)");
			} else if( "GL".equals(argValue) ) { 
//				retS = "S".equals(argType) ? "생방(자치)" : ("N".equals(argType) ? "자치위"   : "생 방 송 (자치행정위원회)");
				retS = "S".equals(argType) ? "생방(기획)" : ("N".equals(argType) ? "기획위"   : "생 방 송 (기획행정위원회)");
			} else if( "WL".equals(argValue) ) {
				retS = "S".equals(argType) ? "생방(복지)" : ("N".equals(argType) ? "복지위"   : "생 방 송 (복지환경위원회)");
			} else if( "BL".equals(argValue) ) { 
				retS = "S".equals(argType) ? "생방(산건)" : ("N".equals(argType) ? "산건위"   : "생 방 송 (산업건설위원회)");
			} else if( "CS".equals(argValue) ) { 
				retS = "S".equals(argType) ? "의사(시)"   : ("N".equals(argType) ? "시의회"   : "의사일정 (시의회)");
			} else if( "OS".equals(argValue) ) { 
				retS = "S".equals(argType) ? "의사(운영)" : ("N".equals(argType) ? "운영위"   : "의사일정 (의회운영위원회)");
			} else if( "GS".equals(argValue) ) { 
//				retS = "S".equals(argType) ? "의사(자치)" : ("N".equals(argType) ? "자치위"   : "의사일정 (자치행정위원회)");
				retS = "S".equals(argType) ? "의사(기획)" : ("N".equals(argType) ? "기획위"   : "의사일정 (기획행정위원회)");
			} else if( "WS".equals(argValue) ) {
				retS = "S".equals(argType) ? "의사(복지)" : ("N".equals(argType) ? "복지위"   : "의사일정 (복지환경위원회)");
			} else if( "BS".equals(argValue) ) { 
				retS = "S".equals(argType) ? "의사(산건)" : ("N".equals(argType) ? "산건위"   : "의사일정 (산업건설위원회)");
			} else if( "CW".equals(argValue) ) { 
				retS = "S".equals(argType) ? "방청"       : ("N".equals(argType) ? "방청견학" : "방청견학");
			}
		} catch( Exception E ) { 
			retS = "";
		}

		return retS;
	}	// end of getSchdTypeName


	/**
	 * <pre>
	 * 통합일정 월별 내역을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/MonthList.do")
	public String scrListOfSchdMonth ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "reschd/monthSchd_list";

		int						listCnt      = 0;
		int						chldCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseReschdVO>		schdList     = new ArrayList<BaseReschdVO>();
		List<BaseReschdVO>		schdChld     = new ArrayList<BaseReschdVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		String[]				schdType     = new String[12];
		StringBuffer			schdDocu     = new StringBuffer();
log.debug(" act: "+ BaseUtility.spaceFill( "MonthSchdList", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false ) ) return "/cmmn/exe_script";

		// 기본 조회조건 취득
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(propertiesService.getInt("pageSize"));
		pageInfo.setTotalRecordCount(1);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
//		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setPagePerNo("all");
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReStdStat(BaseUtility.null2Char( searchVO.getReStdStat(), "C" ));
		searchVO.setReStdDate(BaseUtility.null2Char( searchVO.getReStdDate(), systemVO.getToYear() + systemVO.getToMonth() ).replaceAll("[^0-9]", ""));
		searchVO.setReStdDate("PY".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(), -12 ) : searchVO.getReStdDate());
		searchVO.setReStdDate("PM".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(),  -1 ) : searchVO.getReStdDate());
		searchVO.setReStdDate("NM".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(),   1 ) : searchVO.getReStdDate());
		searchVO.setReStdDate("NY".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(),  12 ) : searchVO.getReStdDate());
		searchVO.setReBgnDate(BaseUtility.getWeekInMonths( BaseUtility.getDateOfLast( searchVO.getReStdDate(), "F" ), "MF" ));
		searchVO.setReEndDate(BaseUtility.getWeekInMonths( BaseUtility.getDateOfLast( searchVO.getReStdDate(), "L" ), "ML" ));
		searchVO.setReLstType("A");

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("stdDate: "+ searchVO.getReStdDate() +" |stdStat: "+ searchVO.getReStdStat());
log.debug("bgnDate: "+ searchVO.getReBgnDate() +" |endDate: "+ searchVO.getReEndDate() +" |lstType: "+ searchVO.getReLstType());
		try { 
			listCnt = (int)reschdMngrService.getCountOfReschdCal( searchVO );

			// 조회 건수를 전체선택인 경우
			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
			}
			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
			searchVO.setPageFirNo(Integer.toString(idxFir));
			searchVO.setPageLstNo(Integer.toString(idxLst));
			searchVO.setListCnt(Integer.toString(listCnt));

			if( listCnt > 0 ) {
				schdList = (List<BaseReschdVO>)reschdMngrService.getListOfReschdCal( searchVO );
//				schdList = (List<BaseReschdVO>)reschdMngrService.getListOfReschdCal( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				// Data Adjustment(Reschds)
				if( !(schdList == null || schdList.size() <= 0) ) { 
					schdDocu.setLength(0);
					for( int d = 0; d < schdList.size(); d++ ) { 
						schdList.get(d).setViewNo(schdList.get(d).getStdUUID() + schdList.get(d).getRowSeq());

//						schdList.get(d).setStdDate(BaseUtility.toDateFormat( schdList.get(d).getStdDate(), dtSep, 8 ));

						if( d % 7 == 0 ) schdDocu.append("<tr>");
						schdDocu.append("\n\t\t\t\t\t\t").append("<td class='h95 left vat"+ (systemVO.getToDate().equals(schdList.get(d).getStdDate())?" bold5":"") +"'>");
//						schdDocu.append("<div class='w100p right' style='color:"+ (d%7==0?"#f66":(d%7==6?"#66f":"#808080")) +";'>"+ Integer.parseInt(schdList.get(d).getStdDay()) +"</div>");
						schdDocu.append("<div class='w100p right"+ (d%7==0?" td_red":(d%7==6?" td_blu":"")) +"'");
						if( schdList.get(d).getStdDate().equals(systemVO.getToDate()) ) 
							schdDocu.append(" style='background-color:#0f0;");
						else 
							schdDocu.append(" style='");
						if( searchVO.getReStdDate().equals(schdList.get(d).getStdDate().substring(0,6)) ) 
							schdDocu.append("'>").append(Integer.parseInt(schdList.get(d).getStdDay()) +"</div>");
						else 
							schdDocu.append("opacity:0.3;'>").append(Integer.parseInt(schdList.get(d).getStdDay()) +"</div>");
						schdChld = (List<BaseReschdVO>)schdList.get(d).getChildItem();
						chldCnt  = schdChld == null || schdChld.size() <= 0 ? 0 : schdChld.size();
						if( chldCnt <= 0 || ("F".equals(propertiesService.getString("viewSun")) && BaseUtility.dayOfWeek( schdList.get(d).getStdDate() ) == 1) || ("F".equals(propertiesService.getString("viewSat")) && BaseUtility.dayOfWeek( schdList.get(d).getStdDate() ) == 7) ) { 
						} else { 
							for( int c = 0; c < chldCnt; c++ ) { 
								schdType = BaseUtility.null2Blank( schdChld.get(c).getSchdType() ).split("[`]", schdType.length);
								for( int s = 0; s < schdType.length-1; s ++ ) if( !"".equals(schdType[s]) ) { 
									schdDocu.append("<div ");
									if( "CL".equals(schdType[s]) || "OL".equals(schdType[s]) || "GL".equals(schdType[s]) || "BL".equals(schdType[s]) || "WL".equals(schdType[s]) ) schdDocu.append("style='color:rgb(253,80,51);' ");
									if( "CS".equals(schdType[s]) || "OS".equals(schdType[s]) || "GS".equals(schdType[s]) || "BS".equals(schdType[s]) || "WS".equals(schdType[s]) ) schdDocu.append("style='color:rgb(64,55,249);' ");
									if( "CW".equals(schdType[s]) ) schdDocu.append("style='color:rgb(122,223,81);' ");
									schdDocu.append("class='w100p pnt' title='"+ getSchdTypeName( "F", schdType[s] ) +" "+ schdChld.get(c).getSchdTitle() +"' onclick='fnActDetail(\""+ schdChld.get(c).getSchdUUID() +"\")'>");
									schdDocu.append(BaseUtility.toTimeFormat( schdChld.get(c).getSchdBTime() ) +"&nbsp;[");
									schdDocu.append(getSchdTypeName( "S", schdType[s] ) +"]&nbsp;");
									schdDocu.append(schdChld.get(c).getSchdSTitle()).append("</div>");
								}
							}
						}
						schdDocu.append("</td>");
						if( d % 7 == 6 ) schdDocu.append("\n\t\t\t\t\t").append("</tr>");
					}
				}

				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
				pageInfo.setTotalRecordCount(listCnt);
			}

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			model.addAttribute("dataList"      , schdList);
			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (schdList == null ? 0 : schdList.size()) : listCnt ));
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("dispDocu"      , schdDocu.toString().replaceAll(" style=''", ""));
			model.addAttribute("dispDate"      , BaseUtility.toDateFormat( searchVO.getReStdDate(), 6, "YM" ));
			model.addAttribute("reStdStat"     , "C");
			model.addAttribute("reStdDate"     , BaseUtility.toDateFormat( searchVO.getReStdDate(), dtSep ));
			model.addAttribute("reCurrDate"    , BaseUtility.toDateFormat( systemVO.getToYear() + systemVO.getToMonth(), dtSep ));
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/"+retPath;
	}	// end of scrListOfSchdMonth

	/**
	 * <pre>
	 * 통합일정 주별 내역을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/WeeklyList.do")
	public String scrListOfSchdWeekly ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "reschd/weeklySchd_list";

		int						listCnt      = 0;
		int						chldCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseReschdVO>		schdList     = new ArrayList<BaseReschdVO>();
		List<BaseReschdVO>		schdChld     = new ArrayList<BaseReschdVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		String[]				schdType     = new String[12];
		StringBuffer			schdDocu     = new StringBuffer();
		String[]				schdTemp     = new String[3];
		Calendar				objCal       = Calendar.getInstance();
log.debug(" act: "+ BaseUtility.spaceFill( "WeeklySchdList", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false ) ) return "/cmmn/exe_script";

		// 기본 조회조건 취득
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(propertiesService.getInt("pageSize"));
		pageInfo.setTotalRecordCount(1);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
//		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setPagePerNo("all");
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReStdStat(BaseUtility.null2Char( searchVO.getReStdStat(), "C" ));
		searchVO.setReStdDate(BaseUtility.null2Char( searchVO.getReStdDate(), systemVO.getToDate() ).replaceAll("[^0-9]", ""));
		searchVO.setReStdDate("PM".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(), -30 ) : searchVO.getReStdDate());
		searchVO.setReStdDate("PW".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(),  -7 ) : searchVO.getReStdDate());
		searchVO.setReStdDate("NW".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(),   7 ) : searchVO.getReStdDate());
		searchVO.setReStdDate("NM".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(),  30 ) : searchVO.getReStdDate());
		searchVO.setReStdWeek(String.valueOf(BaseUtility.getWeekOfMonths( searchVO.getReStdDate(), "C" )));
		searchVO.setReBgnDate(BaseUtility.getWeekInMonths( searchVO.getReStdDate(), "WF" ));
		searchVO.setReEndDate(BaseUtility.getWeekInMonths( searchVO.getReStdDate(), "WL" ));
		searchVO.setReLstType("A");

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("stdDate: "+ searchVO.getReStdDate() +" |stdWeek: "+ searchVO.getReStdWeek() +" |stdStat: "+ searchVO.getReStdStat());
log.debug("bgnDate: "+ searchVO.getReBgnDate() +" |endDate: "+ searchVO.getReEndDate() +" |lstType: "+ searchVO.getReLstType());
		try { 
			listCnt = (int)reschdMngrService.getCountOfReschdCal( searchVO );

			// 조회 건수를 전체선택인 경우
			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
			}
			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
			searchVO.setPageFirNo(Integer.toString(idxFir));
			searchVO.setPageLstNo(Integer.toString(idxLst));
			searchVO.setListCnt(Integer.toString(listCnt));

			if( listCnt > 0 ) { 
				schdList = (List<BaseReschdVO>)reschdMngrService.getListOfReschdCal( searchVO );
//				schdList = (List<BaseReschdVO>)reschdMngrService.getListOfReschdCal( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				// Data Adjustment(Reschds)
				if( !(schdList == null || schdList.size() <= 0) ) { 
					schdDocu.setLength(0);
					for( int d = 0; d < schdList.size(); d++ ) { 
						schdList.get(d).setViewNo(schdList.get(d).getStdUUID() + schdList.get(d).getRowSeq());

//						schdList.get(d).setStdDate(BaseUtility.toDateFormat( schdList.get(d).getStdDate(), dtSep, 8 ));

						schdChld = (List<BaseReschdVO>)schdList.get(d).getChildItem();
						chldCnt  = schdChld == null || schdChld.size() <= 0 ? 0 : schdChld.size();
						schdDocu.append("<tr"+ (systemVO.getToDate().equals(schdList.get(d).getStdDate())?" class='bold5'":"") +">");
						if( d % 7 == 0 ) 
							schdDocu.append("\n\t\t\t\t\t\t").append("<td class='td_red'"+ (chldCnt<=1?"":" rowspan='"+chldCnt+"'") +">");
						else if( d % 7 == 6 ) 
							schdDocu.append("\n\t\t\t\t\t\t").append("<td class='td_blu'"+ (chldCnt<=1?"":" rowspan='"+chldCnt+"'") +">");
						else 
							schdDocu.append("\n\t\t\t\t\t\t").append("<td"+ (chldCnt<=1?"":" rowspan='"+chldCnt+"'") +">");
						schdDocu.append(BaseUtility.toDateFormat( schdList.get(d).getStdDate(), 8, "YM" ) +" ");
						schdDocu.append(BaseUtility.date2Week( schdList.get(d).getStdDate() ) +"요일</td>");
						if( chldCnt <= 0 || ("F".equals(propertiesService.getString("viewSun")) && BaseUtility.dayOfWeek( schdList.get(d).getStdDate() ) == 1) || ("F".equals(propertiesService.getString("viewSat")) && BaseUtility.dayOfWeek( schdList.get(d).getStdDate() ) == 7) ) { 
							schdDocu.append("\n\t\t\t\t\t\t").append("<td></td>");
							schdDocu.append("\n\t\t\t\t\t\t").append("<td class='left'></td>");
							schdDocu.append("\n\t\t\t\t\t\t").append("<td></td>");	// 비고
							schdDocu.append("\n\t\t\t\t\t").append("</tr>");
						} else { 
							for( int c = 0; c < schdList.get(d).getChildItem().size(); c++ ) { 
								schdTemp[0] = "";
								schdTemp[1] = "";
								schdTemp[2] = "";
								schdType = BaseUtility.null2Blank( schdChld.get(c).getSchdType() ).split("[`]", schdType.length);
								schdDocu.append(c==0?"":"<tr"+ (systemVO.getToDate().equals(schdList.get(d).getStdDate())?" class='bold5'":"") +">");
								for( int s = 0; s < schdType.length-1; s ++ ) if( !"".equals(schdType[s]) ) { 
									schdTemp[0] += BaseUtility.toTimeFormat( schdChld.get(c).getSchdBTime() ) +" ~ "+ BaseUtility.toTimeFormat( schdChld.get(c).getSchdETime() ) +"<br/>";
									schdTemp[1] += "["+ getSchdTypeName( "F", schdType[s] ) +"]&nbsp;"+ schdChld.get(c).getSchdTitle() +"<br/>";
								}
								schdDocu.append("\n\t\t\t\t\t\t").append("<td class='pnt' onclick='fnActDetail(\""+ schdChld.get(c).getSchdUUID() +"\")'>"+ BaseUtility.lastOfRemove( schdTemp[0], "<br/>" ) +"</td>");
								schdDocu.append("\n\t\t\t\t\t\t").append("<td class='left pnt' onclick='fnActDetail(\""+ schdChld.get(c).getSchdUUID() +"\")'>"+ BaseUtility.lastOfRemove( schdTemp[1], "<br/>" ) +"</td>");
								schdDocu.append("\n\t\t\t\t\t\t").append("<td class='pnt' onclick='fnActDetail(\""+ schdChld.get(c).getSchdUUID() +"\")'></td>");
								schdDocu.append("\n\t\t\t\t\t").append("</tr>");
							}
						}
					}
				}

				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
				pageInfo.setTotalRecordCount(listCnt);
			}

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			model.addAttribute("dataList"      , schdList);
			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (schdList == null ? 0 : schdList.size()) : listCnt ));
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("dispDocu"      , schdDocu.toString());
			model.addAttribute("dispDate"      , BaseUtility.toDateFormat( searchVO.getReStdDate(), 6, "YM" ) +" "+ searchVO.getReStdWeek() +"주차");
			model.addAttribute("reStdStat"     , "C");
			model.addAttribute("reStdDate"     , BaseUtility.toDateFormat( searchVO.getReStdDate(), dtSep ));
			model.addAttribute("reStdWeek"     , searchVO.getReStdWeek());
			model.addAttribute("reCurrDate"    , BaseUtility.toDateFormat( systemVO.getToDate(), dtSep ));
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/"+retPath;
	}	// end of scrListOfSchdWeekly

	/**
	 * <pre>
	 * 통합일정 주별 내역을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/DailyList.do")
	public String scrListOfSchdDaily ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "reschd/dailySchd_list";

		int						listCnt      = 0;
		int						chldCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseReschdVO>		schdList     = new ArrayList<BaseReschdVO>();
		List<BaseReschdVO>		schdChld     = new ArrayList<BaseReschdVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		String[]				schdType     = new String[12];
		StringBuffer			schdDocu     = new StringBuffer();
		String[]				schdTemp     = new String[3];
		Calendar				objCal       = Calendar.getInstance();
log.debug(" act: "+ BaseUtility.spaceFill( "DailySchdList", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false ) ) return "/cmmn/exe_script";

		// 기본 조회조건 취득
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(propertiesService.getInt("pageSize"));
		pageInfo.setTotalRecordCount(1);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
//		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setPagePerNo("all");
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReStdDate(BaseUtility.null2Char( searchVO.getReStdDate(), systemVO.getToDate() ).replaceAll("[^0-9]", ""));
		searchVO.setReBgnDate(searchVO.getReStdDate());
		searchVO.setReEndDate(searchVO.getReStdDate());
		searchVO.setReLstType("A");

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("stdDate: "+ searchVO.getReStdDate() +" |stdStat: "+ searchVO.getReStdStat());
log.debug("bgnDate: "+ searchVO.getReBgnDate() +" |endDate: "+ searchVO.getReEndDate() +" |lstType: "+ searchVO.getReLstType());
		try { 
			listCnt = (int)reschdMngrService.getCountOfReschdCal( searchVO );

			// 조회 건수를 전체선택인 경우
			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
			}
			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
			searchVO.setPageFirNo(Integer.toString(idxFir));
			searchVO.setPageLstNo(Integer.toString(idxLst));
			searchVO.setListCnt(Integer.toString(listCnt));

			if( listCnt > 0 ) { 
				schdList = (List<BaseReschdVO>)reschdMngrService.getListOfReschdCal( searchVO );
//				schdList = (List<BaseReschdVO>)reschdMngrService.getListOfReschdCal( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				// Data Adjustment(Reschds)
				if( !(schdList == null || schdList.size() <= 0) ) { 
					schdDocu.setLength(0);
					for( int d = 0; d < schdList.size(); d++ ) { 
						schdList.get(d).setViewNo(schdList.get(d).getStdUUID() + schdList.get(d).getRowSeq());

//						schdList.get(d).setStdDate(BaseUtility.toDateFormat( schdList.get(d).getStdDate(), dtSep, 8 ));

						schdChld = (List<BaseReschdVO>)schdList.get(d).getChildItem();
						chldCnt  = schdChld == null || schdChld.size() <= 0 ? 0 : schdChld.size();
						if( chldCnt <= 0 || ("F".equals(propertiesService.getString("viewSun")) && BaseUtility.dayOfWeek( schdList.get(d).getStdDate() ) == 1) || ("F".equals(propertiesService.getString("viewSat")) && BaseUtility.dayOfWeek( schdList.get(d).getStdDate() ) == 7) ) { 
						} else { 
							for( int c = 0; c < schdChld.size(); c++ ) { 
								schdDocu.append("<tr class='on' onclick='fnActDetail(\""+ schdChld.get(c).getSchdUUID() +"\")'>");
								schdTemp[0] = "";
								schdTemp[1] = "";
								schdTemp[2] = "";
								schdType = BaseUtility.null2Blank( schdChld.get(c).getSchdType() ).split("[`]", schdType.length);
								for( int s = 0; s < schdType.length-1; s ++ ) if( !"".equals(schdType[s]) ) { 
									schdTemp[0] += BaseUtility.toTimeFormat( schdChld.get(c).getSchdBTime() ) +" ~ "+ BaseUtility.toTimeFormat( schdChld.get(c).getSchdETime() ) +"<br/>";
									schdTemp[1] += "["+ getSchdTypeName( "F", schdType[s] ) +"]&nbsp;"+ schdChld.get(c).getSchdTitle() +"<br/>";
								}
								schdDocu.append("\n\t\t\t\t\t\t").append("<td>"+ BaseUtility.lastOfRemove( schdTemp[0], "<br/>" ) +"</td>");
								schdDocu.append("\n\t\t\t\t\t\t").append("<td class='left'>"+ BaseUtility.lastOfRemove( schdTemp[1], "<br/>" ) +"</td>");
								schdDocu.append("\n\t\t\t\t\t\t").append("<td></td>");
								schdDocu.append("\n\t\t\t\t\t").append("</tr>");
							}
						}
					}
				}

				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
				pageInfo.setTotalRecordCount(listCnt);
			}

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			model.addAttribute("dataList"      , schdList);
			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (schdList == null ? 0 : schdList.size()) : listCnt ));
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("dispDocu"      , schdDocu.toString());
			model.addAttribute("reStdStat"     , "C");
			model.addAttribute("reStdDate"     , BaseUtility.toDateFormat( searchVO.getReStdDate(), dtSep ));
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/"+retPath;
	}	// end of scrListOfSchdDaily

	/**
	 * <pre>
	 * 통합일정 내역을(를) 상세조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath}View.do")
	public String scrViewOfSchdDaily ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "reschd/dailySchd_view";

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		BaseReschdVO			dataView     = new BaseReschdVO();
		BaseReschdVO			prevView     = new BaseReschdVO();
		BaseReschdVO			nextView     = new BaseReschdVO();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		String[]				schdType     = new String[12];
		String[]				schdTemp     = new String[3];
log.debug(" act: "+ BaseUtility.spaceFill( "DailySchdView", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false ) ) return "/cmmn/exe_script";

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reStdStat"     , searchVO.getReStdStat());
		model.addAttribute("reStdDate"     , searchVO.getReStdDate());
		model.addAttribute("reStdWeek"     , searchVO.getReStdWeek());

		// 추가 조회조건 취득
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			viewNo = BaseUtility.null2Char( (String)reschdMngrService.getInfoOfReschdUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

			// 입력내역 취득(통합일정)
			searchVO.setSchdYear(viewNo[0]);
			searchVO.setSchdSeq(viewNo[1]);
log.debug("viewNo: "+ searchVO.getSchdYear() +" | "+ searchVO.getSchdSeq());

			dataView = (BaseReschdVO)reschdMngrService.getDetailOfReschd( searchVO );
			if( dataView != null ) { 
				// Data Adjustment(Reschds)
				dataView.setActMode("Update");
				dataView.setViewNo(dataView.getSchdUUID());

				dataView.setSchdBDate(BaseUtility.toDateFormat( dataView.getSchdBDate(), dtSep, 8 ));
				dataView.setSchdEDate(BaseUtility.toDateFormat( dataView.getSchdEDate(), dtSep, 8 ));
				dataView.setSchdBTime(BaseUtility.toTimeFormat( dataView.getSchdBTime() ));
				dataView.setSchdETime(BaseUtility.toTimeFormat( dataView.getSchdETime() ));
				dataView.setCreDate(BaseUtility.toDateFormat( dataView.getCreDate(), dtSep, 8 ));
				dataView.setModDate(BaseUtility.toDateFormat( dataView.getModDate(), dtSep, 8 ));
				dataView.setDelDate(BaseUtility.toDateFormat( dataView.getDelDate(), dtSep, 8 ));

				dataView.setSchdDescBR(BaseUtility.tag2Sym( dataView.getSchdDesc() ));
				dataView.setSchdDescCR(BaseUtility.sym2Tag( dataView.getSchdDesc() ));
				dataView.setDownExt(FileProcess.getFileLowerExt( dataView.getDownRel() ));
				dataView.setFileUUID(BaseUtility.null2Char(dataView.getFileUUID(), "", "6"+ dataView.getFileUUID() ));

				// 일정시간
				model.addAttribute("schdBTimH"     , dataView.getSchdBTime().split("[:]", 2)[0]);
				model.addAttribute("schdBTimM"     , dataView.getSchdBTime().split("[:]", 2)[1]);
				model.addAttribute("schdETimH"     , dataView.getSchdETime().split("[:]", 2)[0]);
				model.addAttribute("schdETimM"     , dataView.getSchdETime().split("[:]", 2)[1]);
				// 일정구분
				schdTemp[0] = "";
				schdTemp[1] = "";
				schdTemp[2] = "";
				schdType = BaseUtility.null2Blank( dataView.getSchdType() ).split("[`]", schdType.length);
				for( int s = 0; s < schdType.length-1; s ++ ) if( !"".equals(schdType[s]) ) { 
					schdTemp[1] += getSchdTypeName( "F", schdType[s] ) +", ";
				}
				dataView.setSchdTypeName(BaseUtility.lastOfRemove( schdTemp[1], "," ));
//				model.addAttribute("schdTyCL"      , schdType[0]);
//				model.addAttribute("schdTyCLName"  , getSchdTypeName( "F", schdType[0] ) );
//				model.addAttribute("schdTyOL"      , schdType[1]);
//				model.addAttribute("schdTyOLName"  , getSchdTypeName( "F", schdType[1] ) );
//				model.addAttribute("schdTyGL"      , schdType[2]);
//				model.addAttribute("schdTyGLName"  , getSchdTypeName( "F", schdType[2] ) );
//				model.addAttribute("schdTyBL"      , schdType[3]);
//				model.addAttribute("schdTyBLName"  , getSchdTypeName( "F", schdType[3] ) );
//				model.addAttribute("schdTyCS"      , schdType[4]);
//				model.addAttribute("schdTyCSName"  , getSchdTypeName( "F", schdType[4] ) );
//				model.addAttribute("schdTyOS"      , schdType[5]);
//				model.addAttribute("schdTyOSName"  , getSchdTypeName( "F", schdType[5] ) );
//				model.addAttribute("schdTyGS"      , schdType[6]);
//				model.addAttribute("schdTyGSName"  , getSchdTypeName( "F", schdType[6] ) );
//				model.addAttribute("schdTyBS"      , schdType[7]);
//				model.addAttribute("schdTyBSName"  , getSchdTypeName( "F", schdType[7] ) );
//				model.addAttribute("schdTyCW"      , schdType[8]);
//				model.addAttribute("schdTyCWName"  , getSchdTypeName( "F", schdType[8] ) );
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("moveItem"      , moveItems?"T":"F");
			model.addAttribute("prevView"      , prevView);
			model.addAttribute("nextView"      , nextView);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("nextMode"      , BaseUtility.toCapitalize( paramPath ));
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/"+retPath;
	}	// end of scrViewOfSchdDaily

	/**
	 * <pre>
	 * 통합일정 등록화면으로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath}Inst.do")
	public String scrInstOfSchdDaily ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "reschd/dailySchd_inst";

		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
log.debug(" act: "+ BaseUtility.spaceFill( BaseUtility.toCapitalize( paramPath ) +"SchdInst", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false ) ) return "/cmmn/exe_script";

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reStdStat"     , searchVO.getReStdStat());
		model.addAttribute("reStdDate"     , searchVO.getReStdDate());
		model.addAttribute("reStdWeek"     , searchVO.getReStdWeek());

		model.addAttribute("cateList"      , cateList);
		model.addAttribute("nextMode"      , BaseUtility.toCapitalize( paramPath ));

		return "/mngr/"+retPath;
	}	// end of scrInstOfSchdDaily

	/**
	 * <pre>
	 * 게시판 수정화면으로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath}Edit.do")
	public String scrEditOfSchdDaily ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "reschd/dailySchd_edit";

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		BaseReschdVO			dataView     = new BaseReschdVO();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		String[]				schdType     = new String[12];
		String[]				schdTemp     = new String[3];
log.debug(" act: "+ BaseUtility.spaceFill( BaseUtility.toCapitalize( paramPath ) +"SchdEdit", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			model.addAttribute("close"         , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return "/cmmn/exe_script";
		}
		if( BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reStdStat"     , searchVO.getReStdStat());
		model.addAttribute("reStdDate"     , searchVO.getReStdDate());
		model.addAttribute("reStdWeek"     , searchVO.getReStdWeek());

		// 추가 조회조건 취득
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			viewNo = BaseUtility.null2Char( (String)reschdMngrService.getInfoOfReschdUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

			// 입력내역 취득(게시판)
			searchVO.setSchdYear(viewNo[0]);
			searchVO.setSchdSeq(viewNo[1]);
log.debug("viewNo: "+ searchVO.getSchdYear() +" | "+ searchVO.getSchdSeq());

			dataView = (BaseReschdVO)reschdMngrService.getDetailOfReschd( searchVO );
			if( dataView != null ) { 
				// Data Adjustment(Reschds)
				dataView.setActMode("Update");
				dataView.setViewNo(dataView.getSchdUUID());

				dataView.setSchdBDate(BaseUtility.toDateFormat( dataView.getSchdBDate(), dtSep, 8 ));
				dataView.setSchdEDate(BaseUtility.toDateFormat( dataView.getSchdEDate(), dtSep, 8 ));
				dataView.setSchdBTime(BaseUtility.toTimeFormat( dataView.getSchdBTime() ));
				dataView.setSchdETime(BaseUtility.toTimeFormat( dataView.getSchdETime() ));
				dataView.setCreDate(BaseUtility.toDateFormat( dataView.getCreDate(), dtSep, 8 ));
				dataView.setModDate(BaseUtility.toDateFormat( dataView.getModDate(), dtSep, 8 ));
				dataView.setDelDate(BaseUtility.toDateFormat( dataView.getDelDate(), dtSep, 8 ));

				dataView.setSchdDescBR(BaseUtility.tag2Sym( dataView.getSchdDesc() ));
				dataView.setSchdDescCR(BaseUtility.sym2Tag( dataView.getSchdDesc() ));
				dataView.setDownExt(FileProcess.getFileLowerExt( dataView.getDownRel() ));
				dataView.setFileUUID(BaseUtility.null2Char(dataView.getFileUUID(), "", "6"+ dataView.getFileUUID() ));

				// 일정시간
				model.addAttribute("schdBTimH"     , dataView.getSchdBTime().split("[:]", 2)[0]);
				model.addAttribute("schdBTimM"     , dataView.getSchdBTime().split("[:]", 2)[1]);
				model.addAttribute("schdETimH"     , dataView.getSchdETime().split("[:]", 2)[0]);
				model.addAttribute("schdETimM"     , dataView.getSchdETime().split("[:]", 2)[1]);
				// 일정구분
				schdTemp[0] = "";
				schdTemp[1] = "";
				schdTemp[2] = "";
				schdType = BaseUtility.null2Blank( dataView.getSchdType() ).split("[`]", schdType.length);
				for( int s = 0; s < schdType.length-1; s ++ ) if( !"".equals(schdType[s]) ) { 
					schdTemp[1] += getSchdTypeName( "F", schdType[s] ) +", ";
				}
				dataView.setSchdTypeName(BaseUtility.lastOfRemove( schdTemp[1], "," ));
//				model.addAttribute("schdTyCL"      , schdType[0]);
//				model.addAttribute("schdTyCLName"  , getSchdTypeName( "F", schdType[0] ) );
//				model.addAttribute("schdTyOL"      , schdType[1]);
//				model.addAttribute("schdTyOLName"  , getSchdTypeName( "F", schdType[1] ) );
//				model.addAttribute("schdTyGL"      , schdType[2]);
//				model.addAttribute("schdTyGLName"  , getSchdTypeName( "F", schdType[2] ) );
//				model.addAttribute("schdTyBL"      , schdType[3]);
//				model.addAttribute("schdTyBLName"  , getSchdTypeName( "F", schdType[3] ) );
//				model.addAttribute("schdTyCS"      , schdType[4]);
//				model.addAttribute("schdTyCSName"  , getSchdTypeName( "F", schdType[4] ) );
//				model.addAttribute("schdTyOS"      , schdType[5]);
//				model.addAttribute("schdTyOSName"  , getSchdTypeName( "F", schdType[5] ) );
//				model.addAttribute("schdTyGS"      , schdType[6]);
//				model.addAttribute("schdTyGSName"  , getSchdTypeName( "F", schdType[6] ) );
//				model.addAttribute("schdTyBS"      , schdType[7]);
//				model.addAttribute("schdTyBSName"  , getSchdTypeName( "F", schdType[7] ) );
//				model.addAttribute("schdTyCW"      , schdType[8]);
//				model.addAttribute("schdTyCWName"  , getSchdTypeName( "F", schdType[8] ) );
//				model.addAttribute("schdTyWL"      , schdType[9]);
//				model.addAttribute("schdTyWLName"  , getSchdTypeName( "F", schdType[9] ) );
//				model.addAttribute("schdTyWS"      , schdType[10]);
//				model.addAttribute("schdTyWSName"  , getSchdTypeName( "F", schdType[10] ) );


				model.addAttribute("schdTyCL"      , schdType[0]);
				model.addAttribute("schdTyCLName"  , getSchdTypeName( "F", schdType[0] ) );
				model.addAttribute("schdTyOL"      , schdType[1]);
				model.addAttribute("schdTyOLName"  , getSchdTypeName( "F", schdType[1] ) );
				model.addAttribute("schdTyGL"      , schdType[2]);
				model.addAttribute("schdTyGLName"  , getSchdTypeName( "F", schdType[2] ) );
				model.addAttribute("schdTyWL"      , schdType[3]);
				model.addAttribute("schdTyWLName"  , getSchdTypeName( "F", schdType[3] ) );
				model.addAttribute("schdTyBL"      , schdType[4]);
				model.addAttribute("schdTyBLName"  , getSchdTypeName( "F", schdType[4] ) );
				model.addAttribute("schdTyCS"      , schdType[5]);
				model.addAttribute("schdTyCSName"  , getSchdTypeName( "F", schdType[5] ) );
				model.addAttribute("schdTyOS"      , schdType[6]);
				model.addAttribute("schdTyOSName"  , getSchdTypeName( "F", schdType[6] ) );
				model.addAttribute("schdTyGS"      , schdType[7]);
				model.addAttribute("schdTyGSName"  , getSchdTypeName( "F", schdType[7] ) );
				model.addAttribute("schdTyWS"      , schdType[8]);
				model.addAttribute("schdTyWSName"  , getSchdTypeName( "F", schdType[8] ) );
				model.addAttribute("schdTyBS"      , schdType[9]);
				model.addAttribute("schdTyBSName"  , getSchdTypeName( "F", schdType[9] ) );
				model.addAttribute("schdTyCW"      , schdType[10]);
				model.addAttribute("schdTyCWName"  , getSchdTypeName( "F", schdType[10] ) );
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("nextMode"      , BaseUtility.toCapitalize( paramPath ));
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/"+retPath;
	}	// end of scrEditOfSchdDaily

	/**
	 * <pre>
	 * 통합일정 내역을 등록처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/{paramPath}Create.do")
	public ModelAndView doActOfCreateSchdDaily ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseReschdVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

//		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
//		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseReschdVO			dataView     = new BaseReschdVO();
log.debug(" act: "+ BaseUtility.spaceFill( BaseUtility.toCapitalize( paramPath ) +"SchdCreate", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, false ) ) return mav;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reStdStat"     , searchVO.getReStdStat());
		model.addAttribute("reStdDate"     , searchVO.getReStdDate());
		model.addAttribute("reStdWeek"     , searchVO.getReStdWeek());

		// 추가 조회조건 취득
		searchVO.setSchdYear(systemVO.getToYear());

//log.debug("viewNo: "+ searchVO.getSchdYear() +" | "+ searchVO.getSchdSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/reschd/dailySchd_inst";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			// 입력내역 취득(통합일정)
			dataView = (BaseReschdVO)reschdMngrService.getInfoOfReschdSeq( searchVO );
			searchVO.setSchdSeq(dataView == null ? "1" : dataView.getSchdSeq());
			searchVO.setSchdUUID(BaseCategory.encryptSHA256( searchVO.getSchdYear() + searchVO.getSchdSeq() ).toUpperCase());
			searchVO.setSchdType(BaseUtility.null2Blank( searchVO.getSchdType() ).replaceAll("[;]", "`"));
			searchVO.setSchdBDate(BaseUtility.null2Char( searchVO.getSchdBDate(), systemVO.getToDate() ).replaceAll("[^0-9]", ""));
			searchVO.setSchdEDate(BaseUtility.null2Char( searchVO.getSchdEDate(), systemVO.getToDate() ).replaceAll("[^0-9]", ""));
			searchVO.setSchdBTime(BaseUtility.null2Char( searchVO.getSchdBTimH(), "09" ) + BaseUtility.null2Char( searchVO.getSchdBTimM(), "00" ));
			searchVO.setSchdETime(BaseUtility.null2Char( searchVO.getSchdETimH(), "09" ) + BaseUtility.null2Char( searchVO.getSchdETimM(), "00" ));
			searchVO.setUseDele("N");
			searchVO.setCreIPv4(BaseUtility.reqIPv4Address( request ));
			searchVO.setModIPv4(BaseUtility.reqIPv4Address( request ));
			searchVO.setCreUser(BaseUtility.null2Char( searchVO.getCreUser(), memberVO.getUserPID() ));
			searchVO.setCreName(BaseUtility.null2Char( searchVO.getCreName(), memberVO.getUserName() ));
			searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
			searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));
			searchVO.setViewNo(searchVO.getSchdUUID() +"1");
log.debug("viewNo: "+ searchVO.getSchdYear() +" | "+ searchVO.getSchdSeq());

			// 통합일정 내역 갱신
			reschdMngrService.doRtnOfCreateReschd( searchVO );
			model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReStdStat((String)model.get("reStdStat"));
			searchVO.setReStdDate((String)model.get("reStdDate"));
			searchVO.setReStdWeek((String)model.get("reStdWeek"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reStdStat"     , (String)model.get("reStdDate"));
			mav.addObject("reStdDate"     , (String)model.get("reStdDate"));
			mav.addObject("reStdWeek"     , (String)model.get("reStdWeek"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//-		return "redirect:/ptnhexa/Schd/"+ BaseUtility.toCapitalize( paramPath ) +"View.do";
//		return "redirect:/ptnhexa/Schd/"+ BaseUtility.toCapitalize( paramPath ) +"List.do";
		return mav;
	}	// end of doActOfCreateSchdDaily

	/**
	 * <pre>
	 * 게시판 내역을 수정처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/{paramPath}Update.do")
	public ModelAndView doActOfUpdateSchdDaily ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseReschdVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseReschdVO			dataView     = new BaseReschdVO();
log.debug(" act: "+ BaseUtility.spaceFill( BaseUtility.toCapitalize( paramPath ) +"SchdUpdate", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, false, searchVO.getViewNo() ) ) return mav;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reStdStat"     , searchVO.getReStdStat());
		model.addAttribute("reStdDate"     , searchVO.getReStdDate());
		model.addAttribute("reStdWeek"     , searchVO.getReStdWeek());

		// 추가 조회조건 취득
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/reschd/dailySchd_edit";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)reschdMngrService.getInfoOfReschdUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(게시판)
				searchVO.setSchdYear(viewNo[0]);
				searchVO.setSchdSeq(viewNo[1]);
				searchVO.setSchdType(BaseUtility.null2Blank( searchVO.getSchdType() ).replaceAll("[;]", "`"));
				searchVO.setSchdBDate(BaseUtility.null2Char( searchVO.getSchdBDate(), systemVO.getToDate() ).replaceAll("[^0-9]", ""));
				searchVO.setSchdEDate(BaseUtility.null2Char( searchVO.getSchdEDate(), systemVO.getToDate() ).replaceAll("[^0-9]", ""));
				searchVO.setSchdBTime(BaseUtility.null2Char( searchVO.getSchdBTimH(), "09" ) + BaseUtility.null2Char( searchVO.getSchdBTimM(), "00" ));
				searchVO.setSchdETime(BaseUtility.null2Char( searchVO.getSchdETimH(), "09" ) + BaseUtility.null2Char( searchVO.getSchdETimM(), "00" ));
				searchVO.setModIPv4(BaseUtility.reqIPv4Address( request ));
				searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
				searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));
log.debug("viewNo: "+ searchVO.getSchdYear() +" | "+ searchVO.getSchdSeq());

				// 게시판 내역 갱신
				reschdMngrService.doRtnOfUpdateReschd( searchVO );
				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReStdStat((String)model.get("reStdStat"));
			searchVO.setReStdDate((String)model.get("reStdDate"));
			searchVO.setReStdWeek((String)model.get("reStdWeek"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reStdStat"     , (String)model.get("reStdDate"));
			mav.addObject("reStdDate"     , (String)model.get("reStdDate"));
			mav.addObject("reStdWeek"     , (String)model.get("reStdWeek"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//-		return "redirect:/ptnhexa/Schd/"+ BaseUtility.toCapitalize( paramPath ) +"View.do";
//		return "redirect:/ptnhexa/Schd/"+ BaseUtility.toCapitalize( paramPath ) +"List.do";
		return mav;
	}	// end of doActOfUpdateSchdDaily

	/**
	 * <pre>
	 * 게시판 내역을 삭제처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return String
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/{paramPath}Delete.do")
	public ModelAndView doActOfDeleteSchdDaily ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseReschdVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseReschdVO			dataView     = new BaseReschdVO();
log.debug(" act: "+ BaseUtility.spaceFill( BaseUtility.toCapitalize( paramPath ) +"SchdDelete", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, false, searchVO.getViewNo() ) ) return mav;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reStdStat"     , searchVO.getReStdStat());
		model.addAttribute("reStdDate"     , searchVO.getReStdDate());
		model.addAttribute("reStdWeek"     , searchVO.getReStdWeek());

		// 추가 조회조건 취득
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)reschdMngrService.getInfoOfReschdUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(게시판)
				searchVO.setSchdYear(viewNo[0]);
				searchVO.setSchdSeq(viewNo[1]);
				searchVO.setDelIPv4(BaseUtility.reqIPv4Address( request ));
				searchVO.setDelUser(BaseUtility.null2Char( searchVO.getDelUser(), memberVO.getUserPID() ));
				searchVO.setDelName(BaseUtility.null2Char( searchVO.getDelName(), memberVO.getUserName() ));
log.debug("viewNo: "+ searchVO.getSchdYear() +" | "+ searchVO.getSchdSeq());

				// 게시판 내역 삭제
				reschdMngrService.doRtnOfDeleteReschd( searchVO );
				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReStdStat((String)model.get("reStdStat"));
			searchVO.setReStdDate((String)model.get("reStdDate"));
			searchVO.setReStdWeek((String)model.get("reStdWeek"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reStdStat"     , (String)model.get("reStdDate"));
			mav.addObject("reStdDate"     , (String)model.get("reStdDate"));
			mav.addObject("reStdWeek"     , (String)model.get("reStdWeek"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/ptnhexa/Schd/"+ BaseUtility.toCapitalize( paramPath ) +"List.do";
		return mav;
	}	// end of doActOfDeleteSchdDaily

	/**
	 * <pre>
	 * 게시판 내역을 변경처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
//	@RequestMapping(value="/{paramPath}Modify.do")
	public ModelAndView doActOfModifySchdDaily ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseReschdVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseReschdVO			dataView     = new BaseReschdVO();
log.debug(" act: "+ BaseUtility.spaceFill( BaseUtility.toCapitalize( paramPath ) +"SchdModify", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, false, searchVO.getViewNo() ) ) return mav;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reStdStat"     , searchVO.getReStdStat());
		model.addAttribute("reStdDate"     , searchVO.getReStdDate());
		model.addAttribute("reStdWeek"     , searchVO.getReStdWeek());

		// 추가 조회조건 취득
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/reschd/dailySchd_edit";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)reschdMngrService.getInfoOfReschdUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(게시판)
				searchVO.setSchdYear(viewNo[0]);
				searchVO.setSchdSeq(viewNo[1]);
				searchVO.setModIPv4(BaseUtility.reqIPv4Address( request ));
				searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
				searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));
log.debug("viewNo: "+ searchVO.getSchdYear() +" | "+ searchVO.getSchdSeq());

				// 게시판 내역 갱신
				reschdMngrService.doRtnOfModifyReschd( searchVO );
				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReStdStat((String)model.get("reStdStat"));
			searchVO.setReStdDate((String)model.get("reStdDate"));
			searchVO.setReStdWeek((String)model.get("reStdWeek"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reStdStat"     , (String)model.get("reStdDate"));
			mav.addObject("reStdDate"     , (String)model.get("reStdDate"));
			mav.addObject("reStdWeek"     , (String)model.get("reStdWeek"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/ptnhexa/Schd/"+ BaseUtility.toCapitalize( paramPath ) +"List.do";
		return mav;
	}	// end of doActOfModifySchdDaily

}	// end of class