/*******************************************************************************
  Program ID  : StatsMngrController
  Description : 게시판 관리에 관한 controller 클래스를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.board.web;

import java.util.*;

import javax.annotation.*;
import javax.servlet.http.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.jdbc.datasource.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.*;
import org.springmodules.validation.commons.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.util.*;
import egovframework.ptcc.cmmn.vo.*;

@Controller
@RequestMapping(value="/ptnhexa/Stats")
public class StatsMngrController {

	@Autowired
	private WebApplicationContext			webContext;
	@Autowired
	private DataSourceTransactionManager	txManager;

	@Resource(name="beanValidator")
	protected DefaultBeanValidator			beanValidator;
	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));
	protected String						defsPass     = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsPass" );
	protected boolean						moveItems    = false;
	protected boolean						fileUP       = false;
	protected boolean						tempUP       = false;

	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelMap model, 
			boolean connMode 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			model.addAttribute("message"       , "로그인 정보가 존재하지 않습니다.\\n\\n로그인 화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			model.addAttribute("message"       , "임시 비밀번호를 사용하고 있습니다.\\n\\n비밀번호 변경화면으로 이동합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			model.addAttribute("close"         , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			model.addAttribute("error"         , "접속권한이 없습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		return false;
	}
	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelMap model, 
			boolean connMode, 
			String viewNo 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			model.addAttribute("message"       , "로그인 정보가 존재하지 않습니다.\\n\\n로그인 화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			model.addAttribute("message"       , "임시 비밀번호를 사용하고 있습니다.\\n\\n비밀번호 변경화면으로 이동합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			model.addAttribute("close"         , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			model.addAttribute("error"         , "접속권한이 없습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 불량 접속 확인
		if( BaseUtility.isValidNull( viewNo ) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		return false;
	}
	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelAndView mav, 
			boolean connMode 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "로그인 정보가 존재하지 않습니다.\n\n로그인 화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "임시 비밀번호를 사용하고 있습니다.\n\n비밀번호 변경화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속권한이 없습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		return false;
	}
	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelAndView mav, 
			boolean connMode, 
			String viewNo 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "로그인 정보가 존재하지 않습니다.\n\n로그인 화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "임시 비밀번호를 사용하고 있습니다.\n\n비밀번호 변경화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속권한이 없습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 불량 접속 확인
		if( BaseUtility.isValidNull( viewNo ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "잘못된 경로로 접속하였습니다.\n\n메인화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		return false;
	}


	/**
	 * <pre>
	 * 게시판 내역을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath}List.do")
	public String scrListOfStats ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "agenda/"+ paramPath.toLowerCase() +"Stats_list";

		int						listCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseVisitsVO>		dataList     = new ArrayList<BaseVisitsVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		StringBuffer			dataRult     = new StringBuffer();
log.debug(" act: "+ BaseUtility.spaceFill( "Stats"+ BaseUtility.toCapitalize( paramPath ) +"List", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false ) ) return "/cmmn/exe_script";

		if( "base".equalsIgnoreCase(paramPath) ) { 
			retPath  = "agenda/"+ paramPath.toLowerCase() +"Stats_list";
		} else if( "daily".equalsIgnoreCase(paramPath) ) { 
			retPath  = "agenda/"+ paramPath.toLowerCase() +"Stats_list";
		} else if( "month".equalsIgnoreCase(paramPath) ) { 
			retPath  = "agenda/"+ paramPath.toLowerCase() +"Stats_list";
		} else if( "years".equalsIgnoreCase(paramPath) ) { 
			retPath  = "agenda/"+ paramPath.toLowerCase() +"Stats_list";
		}

		// 기본 조회조건 취득
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(propertiesService.getInt("pageSize"));
		pageInfo.setTotalRecordCount(1);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));
		if( "daily".equalsIgnoreCase(paramPath) ) { 
			searchVO.setReStdYear (BaseUtility.zeroFill(BaseUtility.null2Char( searchVO.getReStdYear() , systemVO.getToYear() ), 4, "L" ));
			searchVO.setReStdMonth(BaseUtility.zeroFill(BaseUtility.null2Char( searchVO.getReStdMonth(), systemVO.getToMonth()), 2, "L" ));
			searchVO.setReBgnDate( BaseUtility.getDateOfLast( searchVO.getReStdYear() + searchVO.getReStdMonth(), "F" ));
			searchVO.setReEndDate( BaseUtility.getDateOfLast( searchVO.getReStdYear() + searchVO.getReStdMonth(), "L" ));
		}
		if( "month".equalsIgnoreCase(paramPath) ) { 
			searchVO.setReStdYear (BaseUtility.zeroFill(BaseUtility.null2Char( searchVO.getReStdYear() , systemVO.getToYear() ), 4, "L" ));
		}
		if( "years".equalsIgnoreCase(paramPath) ) { 
			searchVO.setReBgnDate(BaseUtility.null2Char( searchVO.getReBgnDate(), String.valueOf(Integer.parseInt(systemVO.getToYear())-5) ));
			searchVO.setReEndDate(BaseUtility.null2Char( searchVO.getReEndDate(), systemVO.getToYear() ));
		}

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		if( "daily".equalsIgnoreCase(paramPath) ) { 
			model.addAttribute("reStdYear"     , searchVO.getReStdYear());
			model.addAttribute("reStdMonth"    , searchVO.getReStdMonth());
			model.addAttribute("dpBgnDate"     , BaseUtility.toDateFormat( searchVO.getReBgnDate(), "-" ));
			model.addAttribute("dpEndDate"     , BaseUtility.toDateFormat( searchVO.getReEndDate(), "-" ));
		}
		if( "month".equalsIgnoreCase(paramPath) ) { 
			model.addAttribute("reStdYear"     , searchVO.getReStdYear());
			model.addAttribute("dpBgnDate"     , BaseUtility.toDateFormat( searchVO.getReStdYear()+"0101", "-" ));
			model.addAttribute("dpEndDate"     , BaseUtility.toDateFormat( searchVO.getReStdYear()+"1230", "-" ));
		}
		if( "years".equalsIgnoreCase(paramPath) ) { 
			model.addAttribute("dpBgnDate"     , searchVO.getReBgnDate());
			model.addAttribute("dpEndDate"     , searchVO.getReEndDate());
		}

		// 추가 조회조건 취득
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReBgnDate(BaseUtility.null2Blank( searchVO.getReBgnDate() ).replaceAll("[^0-9]", ""));
		searchVO.setReEndDate(BaseUtility.null2Blank( searchVO.getReEndDate() ).replaceAll("[^0-9]", ""));
		searchVO.setReStdYear(BaseUtility.null2Blank( searchVO.getReStdYear() ).replaceAll("[^0-9]", ""));

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("stdYear: "+ searchVO.getReStdYear() +" |bgnDate: "+ searchVO.getReBgnDate() +" |endDate: "+ searchVO.getReEndDate());
		try { 
//			listCnt = (int)baseCmmnService.getCountOfVisit( searchVO );
//
//			// 조회 건수를 전체선택인 경우
//			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
//				searchVO.setPageCurNo("1");
//				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
//			}
//			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
//			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
//			searchVO.setPageFirNo(Integer.toString(idxFir));
//			searchVO.setPageLstNo(Integer.toString(idxLst));
//			searchVO.setListCnt(Integer.toString(listCnt));
//
//			if( listCnt > 0 ) { 
//				dataList = (List<BaseVisitsVO>)baseCmmnService.getListOfVisitSheet( searchVO );
////				dataList = (List<BaseVisitsVO>)baseCmmnService.getListOfVisitSheet( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
//				// Data Adjustment(Visits)
//				if( !(dataList == null || dataList.size() <= 0) ) { 
//					for( int d = 0; d < dataList.size(); d++ ) { 
//						dataList.get(d).setViewNo(dataList.get(d).getBbsUUID() + dataList.get(d).getRowSeq());
//
//						dataList.get(d).setBbsFiles(BaseUtility.toNumberFormat( dataList.get(d).getBbsFiles() ));
//						dataList.get(d).setBgnDate(BaseUtility.toDateFormat( dataList.get(d).getBgnDate(), dtSep, 8 ));
//						dataList.get(d).setEndDate(BaseUtility.toDateFormat( dataList.get(d).getEndDate(), dtSep, 8 ));
//						dataList.get(d).setHitCount(BaseUtility.toNumberFormat( dataList.get(d).getHitCount() ));
//						dataList.get(d).setCreDate(BaseUtility.toDateFormat( dataList.get(d).getCreDate(), dtSep, 8 ));
//						dataList.get(d).setModDate(BaseUtility.toDateFormat( dataList.get(d).getModDate(), dtSep, 8 ));
//						dataList.get(d).setDelDate(BaseUtility.toDateFormat( dataList.get(d).getDelDate(), dtSep, 8 ));
//					}
//				}
//
//				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
//				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
//				pageInfo.setTotalRecordCount(listCnt);
//			}
			if( "daily".equalsIgnoreCase(paramPath) ) { 
				dataList = (List<BaseVisitsVO>)baseCmmnService.getListOfVisitSheetDate( searchVO );
			}
			if( "month".equalsIgnoreCase(paramPath) ) { 
				dataList = (List<BaseVisitsVO>)baseCmmnService.getListOfVisitSheetMonth( searchVO );
			}
			if( "years".equalsIgnoreCase(paramPath) ) { 
				dataList = (List<BaseVisitsVO>)baseCmmnService.getListOfVisitSheetYear( searchVO );
			}
			// Data Adjustment(Visits)
			if( !(dataList == null || dataList.size() <= 0) ) { 
				dataRult.setLength(0);
				for( int d = 0; d < dataList.size(); d++ ) { 
					if( "daily".equalsIgnoreCase(paramPath) ) { 
						dataList.get(d).setVsnDate(BaseUtility.toDateFormat( dataList.get(d).getVsnDate(), "-", 8 ));
					}
					if( "month".equalsIgnoreCase(paramPath) ) { 
						dataList.get(d).setVsnDate(BaseUtility.toDateFormat( dataList.get(d).getVsnDate(), "-", 6 ));
					}
					dataRult.append("[\"").append(dataList.get(d).getVsnDate()).append("month".equalsIgnoreCase(paramPath)?"-"+"01":"").append("\",").append(dataList.get(d).getHitCount()).append("],");
				}
			}

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			model.addAttribute("dataList"      , dataList);
			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (dataList == null ? 0 : dataList.size()) : listCnt ));
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("dataRult"      , dataRult.toString().substring(0,dataRult.toString().length()-1));
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/"+retPath;
	}	// end of scrListOfStats

}	// end of class