/*******************************************************************************
  Program ID  : StandMngrController
  Description : 게시판 관리에 관한 controller 클래스를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
  v1.1    HexaMedia new0man      2020.03.17  비공개 목록 표시 및 검색조건 추가
 *******************************************************************************/
package egovframework.ptcc.mngr.board.web;

import egovframework.com.cmm.util.BaseUtility;
import egovframework.ptcc.cmmn.service.BaseCmmnService;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.board.service.BoardMngrService;
import egovframework.rte.fdl.property.EgovPropertyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.*;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;
import org.springmodules.validation.commons.DefaultBeanValidator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
//@RequestMapping(value="/ptnhexa/Stand")
public class LiveMngrController {

	@Autowired
	private WebApplicationContext			webContext;
	@Autowired
	private DataSourceTransactionManager	txManager;

	@Resource(name="beanValidator")
	protected DefaultBeanValidator			beanValidator;
	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="boardMngrService")
	protected BoardMngrService				boardMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value="/ptnhexa/Stand/LiveEdit.do")
	public String liveEdit (
			HttpServletRequest request,
			HttpServletResponse response,
			ModelMap model
	) throws Exception {

		log.debug("LiveEdit.do 호출");

		BaseLiveVO				dataView		= new BaseLiveVO();

		try {
			dataView = (BaseLiveVO) boardMngrService.getDataLive();

			if( dataView != null ) {
//				dataView.setViewNo(dataView.getBbsUUID());
			}

			model.addAttribute("dataView"      , dataView);

		} catch( Exception E ) {
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally {
		}	// end of try~catch~finally

		return "/mngr/stand/liveStand_edit";
	}	// end of liveEdit

	@ResponseBody
	@RequestMapping(value="/ptnhexa/Stand/LiveUpdate.do")
	public ModelAndView doActUpdateLive (
			@ModelAttribute("searchVO")BaseLiveVO searchVO,
			HttpServletRequest request,
			HttpServletResponse response,
			ModelMap model,
			SessionStatus status
	) throws Exception {

		log.debug("LiveUpdate.do 호출");

		ModelAndView			mav          = new ModelAndView("jsonView");
		HttpSession				session      = request == null ? null : request.getSession();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		// 기본 조회조건 설정
		model.addAttribute("urlMain"        , searchVO.getUrlMain());
		model.addAttribute("urlOper"     , searchVO.getUrlOper());
		model.addAttribute("urlGvadm"     , searchVO.getUrlGvadm());
		model.addAttribute("urlWelfare"      , searchVO.getUrlWelfare());
		model.addAttribute("urlBuild"      , searchVO.getUrlBuild());

log.debug("url_main: " + searchVO.getUrlMain());
log.debug("url_oper: " + searchVO.getUrlOper());
log.debug("url_gvadm: " + searchVO.getUrlGvadm());
log.debug("url_welfare: " + searchVO.getUrlWelfare());
log.debug("url_build: " + searchVO.getUrlBuild());

		try {
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

//				searchVO.setUrlMain("");

			// URL 갱신
			boardMngrService.updateDataLive( searchVO );

			// 최종 결과기록
			model.remove("searchVO");

			searchVO.setUrlMain((String)model.get("urlMain"));
			searchVO.setUrlOper((String)model.get("urlOper"));
			searchVO.setUrlGvadm((String)model.get("urlGvadm"));
			searchVO.setUrlWelfare((String)model.get("urlWelfare"));
			searchVO.setUrlBuild((String)model.get("urlBuild"));

			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) {
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) {
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) {
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) {
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) {
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) {
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) {
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else {
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally {
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//-		return "redirect:/ptnhexa/User/MemberView.do";
//		return "redirect:/ptnhexa/User/MemberList.do";
		return mav;
	}	// end of doActOfUpdateMember

}	// end of class