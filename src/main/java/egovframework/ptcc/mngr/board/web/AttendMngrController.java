/*******************************************************************************
  Program ID  : AttendMngrController
  Description : 방청신청 관리에 관한 controller 클래스를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.mngr.board.web;

import java.util.*;

import javax.annotation.*;
import javax.servlet.http.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.dao.*;
import org.springframework.jdbc.*;
import org.springframework.jdbc.datasource.*;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.*;
import org.springframework.transaction.support.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.*;
import org.springframework.web.servlet.*;
import org.springmodules.validation.commons.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.excel.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.util.*;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.board.service.*;

@Controller
@RequestMapping(value="/ptnhexa/Seat")
public class AttendMngrController implements HandlerExceptionResolver {

	@Autowired
	private WebApplicationContext			webContext;
	@Autowired
	private DataSourceTransactionManager	txManager;

	@Resource(name="beanValidator")
	protected DefaultBeanValidator			beanValidator;
	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="attendMngrService")
	protected AttendMngrService			attendMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));
	protected String						defsPass     = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsPass" );
	protected String						filePath     = BaseUtility.getKeyProperty( "egovframework.egovProps.files", "path.NoteBase" );
	protected boolean						notiItems    = false;
	protected boolean						moveItems    = false;
	protected boolean						fileUP       = false;
	protected boolean						tempUP       = false;

	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelMap model, 
			boolean connMode 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			model.addAttribute("message"       , "로그인 정보가 존재하지 않습니다.\\n\\n로그인 화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			model.addAttribute("message"       , "임시 비밀번호를 사용하고 있습니다.\\n\\n비밀번호 변경화면으로 이동합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			model.addAttribute("close"         , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			model.addAttribute("error"         , "접속권한이 없습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		return false;
	}
	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelMap model, 
			boolean connMode, 
			String viewNo 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			model.addAttribute("message"       , "로그인 정보가 존재하지 않습니다.\\n\\n로그인 화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			model.addAttribute("message"       , "임시 비밀번호를 사용하고 있습니다.\\n\\n비밀번호 변경화면으로 이동합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			model.addAttribute("close"         , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			model.addAttribute("error"         , "접속권한이 없습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 불량 접속 확인
		if( BaseUtility.isValidNull( viewNo ) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		return false;
	}
	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelAndView mav, 
			boolean connMode 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "로그인 정보가 존재하지 않습니다.\n\n로그인 화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "임시 비밀번호를 사용하고 있습니다.\n\n비밀번호 변경화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속권한이 없습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		return false;
	}
	private boolean loginBaseCheck ( 
			HttpServletRequest request, 
			ModelAndView mav, 
			boolean connMode, 
			String viewNo 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		// 로그인 정보 확인
		if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "로그인 정보가 존재하지 않습니다.\n\n로그인 화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		if( memberVO.getUserPWD().equals(BaseCategory.encryptSHA256( memberVO.getUserPID() + memberVO.getBgnDate() + defsPass ).toUpperCase()) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "임시 비밀번호를 사용하고 있습니다.\n\n비밀번호 변경화면으로 이동합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/User/MemberPass.do");
			return true;
		}
		// 관리자 접속지 확인
		if( !BaseCategory.getCheckOfNetwork( request ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속이 제한된 Client에서 사용되어 더 이상 접속이 진행되지 않습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 권한구분에 따른 접속제한
		if( connMode && !BaseCategory.getCheckOfAdmin( memberVO.getAuthRole() ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "접속권한이 없습니다.\n\n시스템 관리자에게 문의바랍니다.");
			return true;
		}
		// 불량 접속 확인
		if( BaseUtility.isValidNull( viewNo ) ) { 
			mav.addObject("isError"       , "T");
			mav.addObject("message"       , "잘못된 경로로 접속하였습니다.\n\n메인화면으로 전환합니다.");
			mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
			return true;
		}
		return false;
	}


	/**
	 * <pre>
	 * 방청신청 내역을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/AttendList.do")
	public String scrListOfAttend ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "reschd/attendSeat_list";

		int						listCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseAttendVO>		dataList     = new ArrayList<BaseAttendVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
log.debug(" act: "+ BaseUtility.spaceFill( "AttendSeatList", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false ) ) return "/cmmn/exe_script";

		// 기본 조회조건 취득
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(propertiesService.getInt("pageSize"));
		pageInfo.setTotalRecordCount(1);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));
		searchVO.setReBgnDate(BaseUtility.toDateFormat( BaseUtility.null2Char( searchVO.getReBgnDate(), systemVO.getToDatePM() ), dtSep ));
		searchVO.setReEndDate(BaseUtility.toDateFormat( BaseUtility.null2Char( searchVO.getReEndDate(), systemVO.getToDateNM() ), dtSep ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reUseStat"     , searchVO.getReUseStat());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReBgnDate(BaseUtility.null2Blank( searchVO.getReBgnDate() ).replaceAll("[^0-9]", ""));
		searchVO.setReEndDate(BaseUtility.null2Blank( searchVO.getReEndDate() ).replaceAll("[^0-9]", ""));
		searchVO.setReUseStat(BaseUtility.null2Char( searchVO.getReUseStat(), "%" ));

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("bgnDate: "+ searchVO.getReBgnDate() +" |endDate: "+ searchVO.getReEndDate() +" |stdStat: "+ searchVO.getReUseStat());
		try { 
			listCnt = (int)attendMngrService.getCountOfAttend( searchVO );

			// 조회 건수를 전체선택인 경우
			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
			}
			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
			searchVO.setPageFirNo(Integer.toString(idxFir));
			searchVO.setPageLstNo(Integer.toString(idxLst));
			searchVO.setListCnt(Integer.toString(listCnt));

			if( listCnt > 0 ) { 
				dataList = (List<BaseAttendVO>)attendMngrService.getListOfAttend( searchVO );
//				dataList = (List<BaseAttendVO>)attendMngrService.getListOfAttend( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				// Data Adjustment(Attends)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					for( int d = 0; d < dataList.size(); d++ ) { 
						dataList.get(d).setViewNo(dataList.get(d).getReqUUID() + dataList.get(d).getRowSeq());

						dataList.get(d).setReqDate(BaseUtility.toDateFormat( dataList.get(d).getReqDate(), dtSep, 8 ));
						dataList.get(d).setVstDate(BaseUtility.toDateFormat( dataList.get(d).getVstDate(), dtSep, 12 ));
						dataList.get(d).setCreDate(BaseUtility.toDateFormat( dataList.get(d).getCreDate(), dtSep, 8 ));
						dataList.get(d).setModDate(BaseUtility.toDateFormat( dataList.get(d).getModDate(), dtSep, 8 ));
						dataList.get(d).setDelDate(BaseUtility.toDateFormat( dataList.get(d).getDelDate(), dtSep, 8 ));

						dataList.get(d).setVstDescBR(BaseUtility.tag2Sym( dataList.get(d).getVstDesc() ));
						dataList.get(d).setVstDescCR(BaseUtility.sym2Tag( dataList.get(d).getVstDesc() ));
						dataList.get(d).setDownExt(FileProcess.getFileLowerExt( dataList.get(d).getDownRel() ));
						dataList.get(d).setFileUUID(BaseUtility.null2Char(dataList.get(d).getFileUUID(), "", "2"+ dataList.get(d).getFileUUID() ));
						dataList.get(d).setFontColor("G".equals(dataList.get(d).getUseAccept())?" style='color:rgb(64,64,64);'":dataList.get(d).getFontColor());	// 접수
						dataList.get(d).setFontColor("Y".equals(dataList.get(d).getUseAccept())?" style='color:rgb(0,153,0);'":dataList.get(d).getFontColor());	// 접수대기
						dataList.get(d).setFontColor("B".equals(dataList.get(d).getUseAccept())?" style='color:rgb(0,0,255);'":dataList.get(d).getFontColor());	// 승인
						dataList.get(d).setFontColor("R".equals(dataList.get(d).getUseAccept())?" style='color:rgb(255,0,0);'":dataList.get(d).getFontColor());	// 승인불가
						dataList.get(d).setFontColor("C".equals(dataList.get(d).getUseAccept())?"":dataList.get(d).getFontColor());	// 취소
					}
				}

				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
				pageInfo.setTotalRecordCount(listCnt);
			}

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			model.addAttribute("dataList"      , dataList);
			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (dataList == null ? 0 : dataList.size()) : listCnt ));
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("cateList"      , cateList);
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/"+retPath;
	}	// end of scrListOfAttend

	/**
	 * <pre>
	 * 방청신청 내역을(를) 상세조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/AttendView.do")
	public String scrViewOfAttend ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "reschd/attendSeat_view";

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		BaseAttendVO			dataView     = new BaseAttendVO();
		BaseAttendVO			prevView     = new BaseAttendVO();
		BaseAttendVO			nextView     = new BaseAttendVO();
		List<BaseFilesVO>		dataFile     = new ArrayList<BaseFilesVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
log.debug(" act: "+ BaseUtility.spaceFill( "AttendSeatView", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false, searchVO.getViewNo() ) ) return "/cmmn/exe_script";

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reUseStat"     , searchVO.getReUseStat());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			viewNo = BaseUtility.null2Char( (String)attendMngrService.getInfoOfAttendUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

			// 입력내역 취득(방청신청)
			searchVO.setReqDate(viewNo[0]);
			searchVO.setReqSeq(viewNo[1]);
log.debug("viewNo: "+ searchVO.getReqDate() +" | "+ searchVO.getReqSeq());

			dataView = (BaseAttendVO)attendMngrService.getDetailOfAttend( searchVO );
			if( dataView != null ) { 
				// Data Adjustment(Attends)
				dataView.setActMode("Update");
				dataView.setViewNo(dataView.getReqUUID());

				dataView.setReqDate(BaseUtility.toDateFormat( dataView.getReqDate(), dtSep, 8 ));
				dataView.setVstDate(BaseUtility.toDateFormat( dataView.getVstDate(), dtSep, 12 ));
				dataView.setCreDate(BaseUtility.toDateFormat( dataView.getCreDate(), dtSep, 8 ));
				dataView.setModDate(BaseUtility.toDateFormat( dataView.getModDate(), dtSep, 8 ));
				dataView.setDelDate(BaseUtility.toDateFormat( dataView.getDelDate(), dtSep, 8 ));

				dataView.setVstDescBR(BaseUtility.tag2Sym( dataView.getVstDesc() ));
				dataView.setVstDescCR(BaseUtility.sym2Tag( dataView.getVstDesc() ));
				dataView.setDownExt(FileProcess.getFileLowerExt( dataView.getDownRel() ));
				dataView.setFileUUID(BaseUtility.null2Char(dataView.getFileUUID(), "", "6"+ dataView.getFileUUID() ));
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("prevView"      , prevView);
			model.addAttribute("nextView"      , nextView);
			model.addAttribute("cateList"      , cateList);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/"+retPath;
	}	// end of scrViewOfAttend

	/**
	 * <pre>
	 * 방청신청 등록화면으로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/AttendInst.do")
	public String scrInstOfAttend ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "reschd/attendSeat_inst";

		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
log.debug(" act: "+ BaseUtility.spaceFill( "AttendSeatInst", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false ) ) return "/cmmn/exe_script";

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reUseStat"     , searchVO.getReUseStat());

		model.addAttribute("cateList"      , cateList);

		return "/mngr/"+retPath;
	}	// end of scrInstOfAttend

	/**
	 * <pre>
	 * 방청신청 수정화면으로 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/AttendEdit.do")
	public String scrEditOfAttend ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		String					retPath      = "reschd/attendSeat_edit";

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		BaseAttendVO			dataView     = new BaseAttendVO();
		List<BaseFilesVO>		dataFile     = new ArrayList<BaseFilesVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
log.debug(" act: "+ BaseUtility.spaceFill( "AttendSeatEdit", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false ) ) return "/cmmn/exe_script";

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reUseStat"     , searchVO.getReUseStat());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			viewNo = BaseUtility.null2Char( (String)attendMngrService.getInfoOfAttendUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

			// 입력내역 취득(방청신청)
			searchVO.setReqDate(viewNo[0]);
			searchVO.setReqSeq(viewNo[1]);
log.debug("viewNo: "+ searchVO.getReqDate() +" | "+ searchVO.getReqSeq());

			dataView = (BaseAttendVO)attendMngrService.getDetailOfAttend( searchVO );
			if( dataView != null ) { 
				// Data Adjustment(Attends)
				dataView.setActMode("Update");
				dataView.setViewNo(dataView.getReqUUID());

				dataView.setReqDate(BaseUtility.toDateFormat( dataView.getReqDate(), dtSep, 8 ));
				dataView.setVstTime(BaseUtility.toDateFormat( dataView.getVstDate(), dtSep, 8 ));
				dataView.setVstTimeH(BaseUtility.null2Blank( dataView.getVstDate() ).substring(8,10));
				dataView.setVstTimeM(BaseUtility.null2Blank( dataView.getVstDate() ).substring(10,12));
				dataView.setCreDate(BaseUtility.toDateFormat( dataView.getCreDate(), dtSep, 8 ));
				dataView.setModDate(BaseUtility.toDateFormat( dataView.getModDate(), dtSep, 8 ));
				dataView.setDelDate(BaseUtility.toDateFormat( dataView.getDelDate(), dtSep, 8 ));

				dataView.setVstDescBR(BaseUtility.tag2Sym( dataView.getVstDesc() ));
				dataView.setVstDescCR(BaseUtility.sym2Tag( dataView.getVstDesc() ));
				dataView.setDownExt(FileProcess.getFileLowerExt( dataView.getDownRel() ));
				dataView.setFileUUID(BaseUtility.null2Char(dataView.getFileUUID(), "", "6"+ dataView.getFileUUID() ));
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("cateList"      , cateList);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/mngr/"+retPath;
	}	// end of scrEditOfAttend

	/**
	 * <pre>
	 * 방청신청 일괄등록을 위한 엑셀 샘플자료를 다운로드해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
//	@RequestMapping(value="/AttendSample.do")
	public String getSampleOfAttend ( 
			@ModelAttribute("searchVO")BaseFilesVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		String[]				fileName     = {"temp_Attend.xlsx|","방청신청_양식.xlsx|"};
log.debug(" act: "+ BaseUtility.spaceFill( "AttendSeatSample", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

log.debug("fileName: "+ fileName[1]);
		try { 
			model.remove("searchVO");
			searchVO.setFileRel(fileName[0]);
			searchVO.setFileAbs(fileName[1]);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return "redirect:/cmmn/TempDown.do";
	}	// end of getSampleOfAttend

	/**
	 * <pre>
	 * 방청신청 내역을 엑셀출력해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
//	@RequestMapping(value="/AttendExport.do")
	public View getExportOfAttend ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();

		int						listCnt      = 0;
		List<BaseAttendVO>		dataList     = new ArrayList<BaseAttendVO>();

		String					fileName   = "AttendXLS";
		String					dataName   = "방청신청 내역";
		List<String>			colName    = new ArrayList<String>();
		List<String>			colRegion  = new ArrayList<String>();
		List<String>			colAlign   = new ArrayList<String>();
		List<String[]>			colValue   = new ArrayList<String[]>();
		List<Integer>			colWidth   = new ArrayList<Integer>();
log.debug(" act: "+ BaseUtility.spaceFill( "AttendSeatExport", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, model, false ) ) return null;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
//		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
//		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
//		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
//		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("bbsType: "+ searchVO.getBbsType());
		try { 
			listCnt = (int)attendMngrService.getCountOfAttend( searchVO );

			if( listCnt > 0 ) { 
				colName.clear();
				colRegion.clear();
				colAlign.clear();
				colWidth.clear();
				colValue.clear();

				// 상단제목
				colName.add("No");
				colName.add("방문목적");
				colName.add("신청자");
				colName.add("신청일");
				colName.add("방문일시");

				// 행증가,시작열,종료열
				colRegion.add("0`0`0");
				colRegion.add("0`1`1");
				colRegion.add("0`2`2");
				colRegion.add("0`3`3");
				colRegion.add("0`4`4");

				// 내용 가로정렬
				colAlign.add("center");
				colAlign.add("left");
				colAlign.add("center");
				colAlign.add("center");
				colAlign.add("center");

				// 열너비
				colWidth.add(100);	// 순번
				colWidth.add(750);	// 제목
				colWidth.add(225);	// 작성자
				colWidth.add(200);	// 작성일
				colWidth.add(100);	// 조회수

				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
				dataList = (List<BaseAttendVO>)attendMngrService.getListOfAttend( searchVO );
//				dataList = (List<BaseAttendVO>)attendMngrService.getListOfAttend( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				// Data Adjustment(Excels)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					for( int d = 0; d < dataList.size(); d++ ) { 
						String[] array = {Integer.toString(d+1) 
										, dataList.get(d).getVstDesc() 
										, dataList.get(d).getCreName() 
										, BaseUtility.toDateFormat( dataList.get(d).getVstDate(), dtSep, 10 ) 
										, BaseUtility.toDateFormat( dataList.get(d).getReqDate(), dtSep, 8 )  
						};
						colValue.add(array);
					}
				}

				model.put("relFile"     , fileName);
				model.put("absFile"     , dataName);
				model.put("titLine"     , "2");
				model.put("colName"     , colName);
				model.put("colRegion"   , colRegion);
				model.put("colAlign"    , colAlign);
				model.put("colWidth"    , colWidth);
				model.put("colValue"    , colValue);
			}
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return new ExcelBuilderPOI();
	}	// end of getExportOfAttend

	/**
	 * <pre>
	 * 방청신청 내역을 등록처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  MultipartHttpServletRequest		// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/AttendCreate.do")
	public ModelAndView doActOfCreateAttend ( 
			@ModelAttribute("searchVO")BaseAttendVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			final MultipartHttpServletRequest mpRequest, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

//		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
//		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseAttendVO			dataView     = new BaseAttendVO();
		Map<String, MultipartFile> fileMap   = mpRequest == null ? null : mpRequest.getFileMap();
		int						fileCnt      = 0;
		int						thmbCnt      = 0;
log.debug(" act: "+ BaseUtility.spaceFill( "AttendSeatCreate", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, false ) ) return mav;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reUseStat"     , searchVO.getReUseStat());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setReqDate(BaseUtility.null2Char( searchVO.getReqDate(), systemVO.getToDate() ));

//log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/reschd/attendSeat_inst";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			// 입력내역 취득(방청신청)
			dataView = (BaseAttendVO)attendMngrService.getInfoOfAttendSeq( searchVO );
			searchVO.setReqSeq(dataView == null ? "1" : dataView.getReqSeq());
			searchVO.setReqUUID(BaseCategory.encryptSHA256( searchVO.getReqDate() + searchVO.getReqSeq() ).toUpperCase());
			searchVO.setVstDate(BaseUtility.null2Char( searchVO.getVstDate(), systemVO.getToDate() + systemVO.getToTime() ).replaceAll("[^0-9]", ""));
			searchVO.setVstTimeH(BaseUtility.null2Char( searchVO.getVstTimeH(), "09" ));
			searchVO.setVstTimeM(BaseUtility.null2Char( searchVO.getVstTimeM(), "00" ));
			searchVO.setVstDate(searchVO.getVstDate() + searchVO.getVstTimeH() + searchVO.getVstTimeM());
			searchVO.setVstPerson(BaseUtility.null2Char( searchVO.getVstPerson(), "1" ).replaceAll("[^0-9]", ""));
			searchVO.setUseDele("N");
			searchVO.setCreIPv4(BaseUtility.reqIPv4Address( request ));
			searchVO.setModIPv4(BaseUtility.reqIPv4Address( request ));
			searchVO.setCreUser(BaseUtility.null2Char( searchVO.getCreUser(), memberVO.getUserPID() ));
			searchVO.setCreName(BaseUtility.null2Char( searchVO.getCreName(), memberVO.getUserName() ));
			searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
			searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));
			searchVO.setViewNo(searchVO.getBbsUUID() +"1");
log.debug("viewNo: "+ searchVO.getReqDate() +" | "+ searchVO.getReqSeq());

			// 방청신청 내역 갱신
			attendMngrService.doRtnOfCreateAttend( searchVO );
			model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReBgnDate((String)model.get("reBgnDate"));
			searchVO.setReEndDate((String)model.get("reEndDate"));
			searchVO.setReUseStat((String)model.get("reUseStat"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reBgnDate"     , (String)model.get("reBgnDate"));
			mav.addObject("reEndDate"     , (String)model.get("reEndDate"));
			mav.addObject("reUseStat"     , (String)model.get("reUseStat"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//-		return "redirect:/ptnhexa/Seat/AttendView.do";
//		return "redirect:/ptnhexa/Seat/AttendList.do";
		return mav;
	}	// end of doActOfCreateAttend

	/**
	 * <pre>
	 * 방청신청 내역을 수정처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  MultipartHttpServletRequest		// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/AttendUpdate.do")
	public ModelAndView doActOfUpdateAttend ( 
			@ModelAttribute("searchVO")BaseAttendVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			final MultipartHttpServletRequest mpRequest, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseAttendVO			dataView     = new BaseAttendVO();
		Map<String, MultipartFile> fileMap   = mpRequest == null ? null : mpRequest.getFileMap();
		int						fileCnt      = 0;
		int						thmbCnt      = 0;
		String[]				deleList     = null;
		BaseFilesVO				fileDele     = new BaseFilesVO();
log.debug(" act: "+ BaseUtility.spaceFill( "AttendSeatUpdate", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, false, searchVO.getViewNo() ) ) return mav;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reUseStat"     , searchVO.getReUseStat());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/reschd/attendSeat_edit";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)attendMngrService.getInfoOfAttendUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(방청신청)
				searchVO.setReqDate(viewNo[0]);
				searchVO.setReqSeq(viewNo[1]);
				searchVO.setVstDate(BaseUtility.null2Char( searchVO.getVstDate(), systemVO.getToDate() + systemVO.getToTime() ).replaceAll("[^0-9]", ""));
				searchVO.setVstTimeH(BaseUtility.null2Char( searchVO.getVstTimeH(), "09" ));
				searchVO.setVstTimeM(BaseUtility.null2Char( searchVO.getVstTimeM(), "00" ));
				searchVO.setVstDate(searchVO.getVstDate() + searchVO.getVstTimeH() + searchVO.getVstTimeM());
				searchVO.setVstPerson(BaseUtility.null2Char( searchVO.getVstPerson(), "1" ).replaceAll("[^0-9]", ""));
				searchVO.setModIPv4(BaseUtility.reqIPv4Address( request ));
				searchVO.setCreName(BaseUtility.null2Char( searchVO.getCreName(), memberVO.getUserName() ));
				searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
				searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));
log.debug("viewNo: "+ searchVO.getReqDate() +" | "+ searchVO.getReqSeq() +" | "+ searchVO.getCondDele());

				// 방청신청 내역 갱신
				attendMngrService.doRtnOfUpdateAttend( searchVO );
				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReBgnDate((String)model.get("reBgnDate"));
			searchVO.setReEndDate((String)model.get("reEndDate"));
			searchVO.setReUseStat((String)model.get("reUseStat"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reBgnDate"     , (String)model.get("reBgnDate"));
			mav.addObject("reEndDate"     , (String)model.get("reEndDate"));
			mav.addObject("reUseStat"     , (String)model.get("reUseStat"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//-		return "redirect:/ptnhexa/Seat/AttendView.do";
//		return "redirect:/ptnhexa/Seat/AttendList.do";
		return mav;
	}	// end of doActOfUpdateAttend

	/**
	 * <pre>
	 * 방청신청 내역을 삭제처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return String
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/AttendDelete.do")
	public ModelAndView doActOfDeleteAttend ( 
			@ModelAttribute("searchVO")BaseAttendVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseAttendVO			dataView     = new BaseAttendVO();
log.debug(" act: "+ BaseUtility.spaceFill( "AttendSeatDelete", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, false, searchVO.getViewNo() ) ) return mav;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reUseStat"     , searchVO.getReUseStat());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)attendMngrService.getInfoOfAttendUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(방청신청)
				searchVO.setReqDate(viewNo[0]);
				searchVO.setReqSeq(viewNo[1]);
				searchVO.setDelIPv4(BaseUtility.reqIPv4Address( request ));
				searchVO.setDelUser(BaseUtility.null2Char( searchVO.getDelUser(), memberVO.getUserPID() ));
				searchVO.setDelName(BaseUtility.null2Char( searchVO.getDelName(), memberVO.getUserName() ));
log.debug("viewNo: "+ searchVO.getReqDate() +" | "+ searchVO.getReqSeq());

				// 방청신청 내역 삭제
				attendMngrService.doRtnOfDeleteAttend( searchVO );
				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReBgnDate((String)model.get("reBgnDate"));
			searchVO.setReEndDate((String)model.get("reEndDate"));
			searchVO.setReUseStat((String)model.get("reUseStat"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reBgnDate"     , (String)model.get("reBgnDate"));
			mav.addObject("reEndDate"     , (String)model.get("reEndDate"));
			mav.addObject("reUseStat"     , (String)model.get("reUseStat"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/ptnhexa/Seat/AttendList.do";
		return mav;
	}	// end of doActOfDeleteAttend

	/**
	 * <pre>
	 * 방청신청 내역을 변경처리해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @param  SessionStatus					// 
	 * @return ModelAndView
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/AttendModify.do")
	public ModelAndView doActOfModifyAttend ( 
			@ModelAttribute("searchVO")BaseAttendVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model, 
			SessionStatus status 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedAdmin() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedAdmin() : new BaseMemberVO();
		DefaultTransactionDefinition dtd     = new DefaultTransactionDefinition();
		TransactionStatus		txStatus     = null;

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseAttendVO			dataView     = new BaseAttendVO();
log.debug(" act: "+ BaseUtility.spaceFill( "AttendSeatModify", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());
		if( loginBaseCheck( request, mav, false, searchVO.getViewNo() ) ) return mav;

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reUseStat"     , searchVO.getReUseStat());

		// 추가 조회조건 취득
		searchVO.setCondName("BASE");
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
//			// 서버측 검증
//			beanValidator.validate(searchVO, bindingResult);
//			if( bindingResult.hasErrors() ) { 
//				model.addAttribute("dataView"      , searchVO);
//				return "/mngr/reschd/attendSeat_edit";
//			}
			dtd.setName("transaction");
			dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			txStatus = txManager.getTransaction(dtd);

			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)attendMngrService.getInfoOfAttendUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(방청신청)
				searchVO.setReqDate(viewNo[0]);
				searchVO.setReqSeq(viewNo[1]);
				searchVO.setModIPv4(BaseUtility.reqIPv4Address( request ));
				searchVO.setModUser(BaseUtility.null2Char( searchVO.getModUser(), memberVO.getUserPID() ));
				searchVO.setModName(BaseUtility.null2Char( searchVO.getModName(), memberVO.getUserName() ));
log.debug("viewNo: "+ searchVO.getReqDate() +" | "+ searchVO.getReqSeq());

				// 방청신청 내역 갱신
				attendMngrService.doRtnOfModifyAttend( searchVO );
				model.addAttribute("viewNo"        , searchVO.getViewNo());
log.debug("viewNo: "+ searchVO.getViewNo());
			}

			// 최종 결과기록
			model.remove("searchVO");
//			searchVO.setViewNo((String)model.get("viewNo"));
			searchVO.setPageCurNo((String)model.get("pageCurNo"));
			searchVO.setPagePerNo((String)model.get("pagePerNo"));
			searchVO.setCondCate((String)model.get("condCate"));
			searchVO.setCondName((String)model.get("condName"));
			searchVO.setCondType((String)model.get("condType"));
			searchVO.setCondValue((String)model.get("condValue"));
			searchVO.setReBgnDate((String)model.get("reBgnDate"));
			searchVO.setReEndDate((String)model.get("reEndDate"));
			searchVO.setReUseStat((String)model.get("reUseStat"));

			mav.addObject("viewNo"        , (String)model.get("viewNo"));
			mav.addObject("pageCurNo"     , (String)model.get("pageCurNo"));
			mav.addObject("pagePerNo"     , (String)model.get("pagePerNo"));
			mav.addObject("condCate"      , (String)model.get("condCate"));
			mav.addObject("condName"      , (String)model.get("condName"));
			mav.addObject("condType"      , (String)model.get("condType"));
			mav.addObject("condValue"     , (String)model.get("condValue"));
			mav.addObject("reBgnDate"     , (String)model.get("reBgnDate"));
			mav.addObject("reEndDate"     , (String)model.get("reEndDate"));
			mav.addObject("reUseStat"     , (String)model.get("reUseStat"));
			mav.addObject("isError"       , "F");
			mav.addObject("message"       , "정상적으로 처리되었습니다.");
		} catch( Exception E ) { 
			if( txManager.isRollbackOnCommitFailure() ) txManager.rollback(txStatus);
			log.debug(E.getMessage());
			mav.addObject("isError"       , "T");
			if( E instanceof DuplicateKeyException ) { 
				model.addAttribute("error"         , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 중복이 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataIntegrityViolationException ) { 
				model.addAttribute("error"         , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 무결성 제약 조건 오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof BadSqlGrammarException ) { 
				model.addAttribute("error"         , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 질의문이 유효하지 않아 더 이상 진행되지 않습니다.");
			} else if( E instanceof DataAccessResourceFailureException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스 연결오류가 발생하여 더 이상 진행되지 않습니다.");
			} else if( E instanceof CannotAcquireLockException ) { 
				model.addAttribute("error"         , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 데이터베이스가 잠겨있어 더 이상 진행되지 않습니다.");
			} else if( E instanceof DeadlockLoserDataAccessException ) { 
				model.addAttribute("error"         , "데이터 처리중 심각한 오류가 발생하였습니다.\\n\\n시스템 관리자에게 문의바랍니다.");
				mav.addObject("message"       , "데이터 처리중 심각한 오류가 발생하였습니다.\n\n시스템 관리자에게 문의바랍니다.");
			} else { 
				model.addAttribute("error"         , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
				mav.addObject("message"       , "데이터 처리중 오류가 발생하여 더 이상 진행되지 않습니다.");
			}
//			return "/cmmn/exe_script";
		} finally { 
			txManager.commit(txStatus);
			status.setComplete();
		}	// end of try~catch~finally

//		return "redirect:/ptnhexa/Seat/AttendList.do";
		return mav;
	}	// end of doActOfModifyAttend

//	@ExceptionHandler(MaxUploadSizeExceededException.class)
	public ModelAndView resolveException(HttpServletRequest request,HttpServletResponse response,Object handler,Exception exception){
		ModelAndView			mav          = new ModelAndView("jsonView");
		if(exception instanceof MaxUploadSizeExceededException)
			mav.addObject("message"       , "첨부파일 사이즈는 "+ BaseUtility.toNumberFormat( (((MaxUploadSizeExceededException)exception).getMaxUploadSize()/1024/1024) ) +"MB를 초과할 수 없습니다.").addObject("isError"       , "T");
		return mav;
	}

}	// end of class