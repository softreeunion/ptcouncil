package egovframework.ptcc.cmmn.util;

import java.util.*;
import org.springframework.web.context.request.*;
import egovframework.ptcc.cmmn.vo.*;

public class UserDetailsHelper { 

	public static Object getAuthenticatedAdmin() { 
		if(RequestContextHolder.getRequestAttributes()==null){return null;}else{return (BaseMemberVO)RequestContextHolder.getRequestAttributes().getAttribute("MemRInfo",RequestAttributes.SCOPE_SESSION);}
	}

	public static List<String> getAuthoritiesAdmin() { 
		if(RequestContextHolder.getRequestAttributes()==null){return null;}
		if((BaseMemberVO)RequestContextHolder.getRequestAttributes().getAttribute("MemRInfo",RequestAttributes.SCOPE_SESSION)==null){return null;}else{return new ArrayList<String>();}
	}

	public static Boolean isAuthenticatedAdmin() { 
		if(RequestContextHolder.getRequestAttributes()==null){return Boolean.FALSE;}
		if((BaseMemberVO)RequestContextHolder.getRequestAttributes().getAttribute("MemRInfo",RequestAttributes.SCOPE_SESSION)==null){return Boolean.FALSE;}else{return Boolean.TRUE;}
	}


	public static Object getAuthenticatedUser() { 
		if(RequestContextHolder.getRequestAttributes()==null){return null;}else{return (BaseMemberVO)RequestContextHolder.getRequestAttributes().getAttribute("MemUInfo",RequestAttributes.SCOPE_SESSION);}
	}

	public static List<String> getAuthoritiesUser() { 
		if(RequestContextHolder.getRequestAttributes()==null){return null;}
		if((BaseMemberVO)RequestContextHolder.getRequestAttributes().getAttribute("MemUInfo",RequestAttributes.SCOPE_SESSION)==null){return null;}else{return new ArrayList<String>();}
	}

	public static Boolean isAuthenticatedUser() { 
		if(RequestContextHolder.getRequestAttributes()==null){return Boolean.FALSE;}
		if((BaseMemberVO)RequestContextHolder.getRequestAttributes().getAttribute("MemUInfo",RequestAttributes.SCOPE_SESSION)==null){return Boolean.FALSE;}else{return Boolean.TRUE;}
	}

}