/*******************************************************************************
  Program ID  : FileProcess
  Description : 파일 처리 공통모듈
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.cmmn.util;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.zip.*;

import javax.servlet.http.*;

import org.apache.commons.io.FileUtils;
import org.slf4j.*;
import org.springframework.web.multipart.*;

import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.vo.*;

public class FileProcess { 

	private static String					baseEtc      = "90090000";
	private static final Logger			log          = LoggerFactory.getLogger(FileProcess.class);
	private static boolean				retB         = false;
	private static String					retS         = "";
	private static StringBuffer			retSB        = new StringBuffer();
	private static int					retI         = 0;
	private static long					retL         = 0;
	private static String					defS         = "";


	private static File unzipEntry ( 
			ZipInputStream zis, 
			File targetFile 
		) throws Exception { 

		FileOutputStream fos = null;

		try { 
			fos = new FileOutputStream(targetFile);

			byte[] buffer = new byte[1024 * 2];
			int len = 0;
			while( (len = zis.read(buffer)) != -1 ) { 
				fos.write(buffer, 0, len);
			}
		} finally { 
			if( fos != null ) fos.close();
		}

		return targetFile;
	}	// end of unzipEntry

	/**
	 * <pre>
	 * 파일 이름을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 파일 이름
	 * @return String[]				// 파일 이름
	 */
	public static String[] splitFileName ( 
			String fileName 
		) { 

		String[] names	= new String[2];
		int pos			= fileName.lastIndexOf(".");

		if( pos == -1 ) { 
			names[0] = fileName;
			names[1] = "";
		} else { 
			names[0] = fileName.substring(0, pos);
			names[1] = fileName.substring(pos);
		}

		return names;
	}	// end of splitFileName


	/**
	 * <pre>
	 * 사용할 수 없는 특수문자를 제거해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 파일 이름 
	 * @return String
	 */
	public static String checkFileName ( 
			String fileName 
		) { 

		try { 
//			fileName	= fileName.replaceAll("\\", "");
//			fileName	= fileName.replaceAll("/", "");
			fileName	= fileName.replaceAll(":", "");
//			fileName	= fileName.replaceAll("*", "");
//			fileName	= fileName.replaceAll("?", "");
			fileName	= fileName.replaceAll("\"", "");
//			fileName	= fileName.replaceAll("<", "");
//			fileName	= fileName.replaceAll(">", "");
//			fileName	= fileName.replaceAll("|", "");
			fileName	= fileName.replaceAll("'", "");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			return "";
		}	// end of try~catch

		return fileName;
	}

	/**
	 * <pre>
	 * 디렉토리 존재유무를 확인해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 파일 경로
	 * @return Boolean					// [true:디렉토리 있음, false:디렉토리 없음]
	 */
	public static boolean existDirectory ( 
			String filePath 
		) { 

		retB = false;

		try { 
			File file = new File(filePath);

			retB = file.exists();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			return retB = false;
		}	// end of try~catch

		return retB;
	}	// end of existDirectory

	/**
	 * <pre>
	 * 파일 존재유무를 확인해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 파일 경로
	 * @param  String					// 파일 이름
	 * @return Boolean					// [true:파일 있음, false:파일 없음]
	 */
	public static boolean existFile ( 
			String filePath, 
			String fileName 
		) { 

		retB = false;

		try { 
			File file = new File(filePath, fileName);

			retB = file.exists();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			return retB = false;
		}	// end of try~catch

		return retB;
	}	// end of existFile

	/**
	 * <pre>
	 * 파일 존재유무를 확인해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 파일 이름
	 * @return Boolean					// [true:파일 있음, false:파일 없음]
	 */
	public static boolean existFile ( 
			String fileName 
		) { 

		retB = false;

		try { 
			File file = new File(fileName);

			retB = file.exists();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			return retB = false;
		}	// end of try~catch

		return retB;
	}	// end of existFile

	/**
	 * <pre>
	 * 파일이름이 중복시 파일명을 재명명 해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 파일 경로
	 * @param  String					// 파일 명칭
	 * @return String
	 */
	public static String existPolicy ( 
			String filePath,  
			String fileName 
		) { 

		return existPolicy( filePath +"/"+ fileName );
	}	// end of existPolicy

	/**
	 * <pre>
	 * 파일이름이 중복시 파일명을 재명명 해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 파일 경로
	 * @param  String					// 파일 명칭
	 * @return String
	 */
	public static String existPolicy ( 
			String fileName 
		) { 

		retI = 0;

log.debug("OfileName: "+ fileName);
		while( existFile(fileName) && retI < 9999 ) { 
			retI++;
			fileName = fileName.substring(0, fileName.lastIndexOf("_") <= 0 ? fileName.lastIndexOf(".") : fileName.lastIndexOf("_")) +"_"+ retI + fileName.substring(fileName.lastIndexOf("."));
		}
log.debug("NfileName: "+ fileName);

		return fileName.substring(fileName.lastIndexOf("/") + 1);
	}	// end of existPolicy

	/**
	 * <pre>
	 * 디렉토리를 생성해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 파일 경로
	 * @return Boolean					// [true:성공, false:실패]
	 */
	public static boolean createDirectory ( 
			String filePath 
		) { 

		retB = false;

		try { 
			File file = new File(filePath);

			retB = file.mkdirs();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			return retB = false;
		}	// end of try~catch

		return retB;
	}	// end of createDiretory

	/**
	 * <pre>
	 * 디렉토리를 삭제해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 파일 경로
	 * @return Boolean					// [true:성공, false:실패]
	 */
	public static boolean removeDirectory ( 
			String filePath 
		) { 

		retB = false;

		try { 
			File file = new File(filePath);

			if( existDirectory(filePath) ) { 
				file.delete();
				retB = true;
			} else { 
				retB = false;
			}
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			return retB = false;
		}	// end of try~catch

		return retB;
	}	// end of removeDirectory

	/**
	 * <pre>
	 * 파일명을 변경해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 파일 경로
	 * @param  String					// 원본 파일 이름
	 * @param  String					// 대상 파일 이름
	 * @return Boolean					// 변경된 파일 이름
	 */
	public static String renameFileS ( 
			String sFilePath, 
			String sFileName, 
			String dFileName  
		) { 

		retS = "";

		try { 
			if( BaseUtility.isValidNull( sFilePath ) && BaseUtility.isValidNull( sFileName ) && BaseUtility.isValidNull( dFileName ) ) return "";

			File sFile = new File(sFilePath + ("/".equals(sFileName.substring(0,1)) ? "" : "/") + sFileName);
			File dFile = new File(sFilePath + ("/".equals(dFileName.substring(0,1)) ? "" : "/") + dFileName);

			// 파일명 변경중 중복파일명 재명명
			String[] names	= splitFileName( dFile.getName() );
			String baseName	= names[0];
			String extName	= names[1];

			int i = 1;
			while( dFile.exists() ) { 
				dFile = new File(sFilePath, baseName +"_"+ i + extName);
				i++;
			}

			if( sFile.renameTo(dFile) ) 
				retS = dFile.getName();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			retS = "";
		}	// end of try~catch

		return retS;
	}	// end of renameFileS

	/**
	 * <pre>
	 * 파일명을 변경해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 파일 경로
	 * @param  String					// 원본 파일 이름
	 * @param  String					// 대상 파일 이름
	 * @return Boolean					// [true:성공, false:실패]
	 */
	public static boolean renameFileB ( 
			String sFilePath, 
			String sFileName, 
			String dFileName  
		) { 

		retB = false;

		try { 
			if( BaseUtility.isValidNull( sFilePath ) && BaseUtility.isValidNull( sFileName ) && BaseUtility.isValidNull( dFileName ) ) return false;

			File sFile = new File(sFilePath + ("/".equals(sFileName.substring(0,1)) ? "" : "/") + sFileName);
			File dFile = new File(sFilePath + ("/".equals(dFileName.substring(0,1)) ? "" : "/") + dFileName);

			retB = sFile.renameTo(dFile); 
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			retB = false;
		}	// end of try~catch

		return retB;
	}	// end of renameFileB

	/**
	 * <pre>
	 * 서버 파일명을 생성해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 파일 경로
	 * @param  String					// 파일 명칭
	 * @return String
	 */
	public static String renameFileN ( 
			String filePath,  
			String fileName 
		) { 

		return existPolicy( filePath +"/"+ System.currentTimeMillis() + fileName.substring(fileName.lastIndexOf(".")) );
	}	// end of renameFileN

	/**
	 * <pre>
	 * 파일을 삭제 해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 파일 경로
	 * @param  String					// 파일 이름
	 * @return Boolean					// [true:성공, false:실패]
	 */
	public static boolean removeFile ( 
			String filePath, 
			String fileName 
		) { 

		retB = false;

		try { 
			File file = new File(filePath + fileName);

			if( existFile(filePath, fileName) )  
				retB = file.delete();
			else 
				retB = false;
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			return retB = false;
		}	// end of try~catch

		return retB;
	}	// end of removeFile

	/**
	 * <pre>
	 * 파일을 복사해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 원본 파일 경로
	 * @param  String					// 원본 파일 이름
	 * @param  String					// 대상 파일 경로
	 * @param  String					// 대상 파일 이름
	 * @return Boolean					// [true:성공, false:실패]
	 */
	public static boolean copyFile ( 
			String sFilePath, 
			String sFileName, 
			String dFilePath, 
			String dFileName 
		) { 

		retB = false;

		try { 
			File sFile = new File(sFilePath + ("/".equals(sFileName.substring(0,1)) ? "" : "/") + sFileName);
			File dFile = new File(sFilePath + ("/".equals(dFileName.substring(0,1)) ? "" : "/") + dFileName);

			retB = sFile.renameTo(dFile);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			return retB = false;
		}	// end of try~catch

		return retB;
	}	// end of copyFile

	/**
	 * <pre>
	 * 파일을 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 원본 파일 경로
	 * @param  String					// 원본 파일 이름
	 * @param  String					// 대상 파일 경로
	 * @param  String					// 대상 파일 이름
	 * @return Boolean					// [true:성공, false:실패]
	 */
	public static boolean moveFile ( 
			String sFilePath, 
			String sFileName, 
			String dFilePath, 
			String dFileName 
		) { 

		retB = false;

		try { 
			retB = copyFile( sFilePath, sFileName, dFilePath, dFileName );
			removeFile( sFilePath, sFileName );	// 피일이동후 파일삭제 처리
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			return retB = false;
		}	// end of try~catch

		return retB;
	}	// end of moveFile

	/**
	 * <pre>
	 * 파일을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 파일 경로
	 * @param  String					// 파일 이름 
	 * @return File					// [object:파일, null:파일 없음]
	 */
	public static File getFile ( 
			String filePath, 
			String fileName 
		) { 

		try { 
			if( existFile(filePath, fileName) ) { 
				File file = new File(filePath, fileName);

				return file;
			} else  { 
				return null;
			}
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			return null;
		}	// end of try~catch
	}	// end of getFile

	/**
	 * <pre>
	 * 파일 이름을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  File						// 파일
	 * @return String					// 파일 이름
	 */
	public static String getFileName ( 
			File file 
		) { 

		retS = "";

		try { 
			if( file == null ) return "";

			retS = ( file.exists() ? file.getName() : "" );
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			return "";
		}	// end of try~catch

		return retS;
	}	// end of getFileName

	/**
	 * <pre>
	 * 파일 크기을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 파일 경로
	 * @param  String					// 파일 이름
	 * @return Boolean					// 파일 크기
	 */
	public static long getFileSize ( 
			String filePath, 
			String fileName 
		) { 

		retL = 0;

		if( BaseUtility.isValidNull( fileName ) || "null".equalsIgnoreCase(fileName) ) return 0;

		try { 
			File file = new File(filePath + fileName);

			retL = ( existFile(filePath, fileName) ? file.length() : 0 );
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			return 0;
		}	// end of try~catch

		return retL;
	}	// end of getFileSize

	/**
	 * <pre>
	 * 파일 확장자을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 파일 경로
	 * @param  String					// 파일 이름
	 * @return Boolean					// 파일 크기
	 */
	public static String getFileLowerExt ( 
			String fileName 
		) { 

		retS = "";

		if( BaseUtility.isValidNull( fileName ) || "null".equalsIgnoreCase(fileName) ) return "";

		try { 
			retS = fileName.substring(fileName.lastIndexOf(".")+1).toLowerCase();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			return "";
		}	// end of try~catch

		return retS;
	}	// end of getFileLowerExt

	/**
	 * <pre>
	 * 압축파일을(를) 압축해제해주는 메소드
	 * </pre>
	 * 
	 * @param  File						// 파일
	 * @param  File						// 저장 장소
	 * @param  Boolean					// 파일명의 대소구분 여부
	 * @return Integer					// 
	 */
	public static int unzipFile ( 
			File zipFile, 
			File targetPath, 
			boolean nameToLower 
		) throws Exception { 

		FileInputStream fis = null;
		ZipInputStream zis =null;
		ZipEntry zentry = null;

		retI = 0;

		try { 
			fis = new FileInputStream(zipFile);
			zis = new ZipInputStream(fis);

			while( (zentry = zis.getNextEntry()) != null ) { 
				String fileNameToUnzip = zentry.getName();
				if( nameToLower ) { 
					fileNameToUnzip = fileNameToUnzip.toLowerCase();
				}

				File targetFile = new File(targetPath, fileNameToUnzip);

				if( zentry.isDirectory() ) 
					createDirectory( targetFile.getAbsolutePath() );
				else { 
					createDirectory( targetFile.getParent() );
					unzipEntry(zis, targetFile);
				}
				retI++;
			}
		} finally { 
			if( zis != null ) zis.close();
			if( fis != null ) fis.close();
		}

		return retI;
	}	// end of unzipFile

	public static String getReadFile ( 
			String argValue1, 
			String argValue2  
		) { 

		retS = "";

		try { 
			File file = new File(argValue2);
			retS = FileUtils.readFileToString(file, BaseUtility.null2Char(argValue1,"utf-8"));
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		}

		return retS;
	}	// end of getReadFile

	public static String getReadFile ( 
			String argValue1, 
			String argValue2, 
			int page_line, 
			int page_now 
		) throws IOException { 

		retS  = "";
		retSB = new StringBuffer();

		String s;
		int i = 0;

		try { 
			File file = new File(argValue2);
			BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file),BaseUtility.null2Char(argValue1,"utf-8")));
			while((s = in.readLine()) != null && i < page_line * page_now) { 
				if( i < page_line * (page_now-1) ) { 
				} else { 
					retSB.append(s); 
				}
				i++;
			}
			retS = retSB.toString();
			in.close();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		}

		return retS;
	}	// end of getReadFile

	public static String getContentMime ( 
			String fileName 
		) { 

		retS = "";

		if( BaseUtility.isValidNull(fileName) ) return retS;

		if( fileName.toLowerCase().endsWith(".pdf") ) 
			retS = "application/pdf";
		else if( fileName.toLowerCase().endsWith(".png") ) 
			retS = "image/png";
		else if( fileName.toLowerCase().endsWith(".bmp") ) 
			retS = "image/bmp";
		else if( fileName.toLowerCase().endsWith(".gif") ) 
			retS = "image/gif";
		else if( fileName.toLowerCase().endsWith(".jpg") || fileName.toLowerCase().endsWith(".jpeg") ) 
			retS = "image/jpeg";
		else if( fileName.toLowerCase().endsWith(".tif") || fileName.toLowerCase().endsWith(".tiff") ) 
			retS = "image/tiff";
		else 
			retS = "application/octec-stream";

		return retS;
	}	// getContentMime


	/**
	 * <pre>
	 * 요청에 따른 브라우저 명칭을 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  HttpServletRequest		// 
	 * @param  HttpServletResponse		// 
	 * @return String
	 */
	public static String getBrowser( 
			HttpServletRequest request, 
			HttpServletResponse response 
		) { 

		String					userAgent    = request.getHeader("User-Agent").toLowerCase();

		if( userAgent.contains("edge") ) 
			return "EDGE";
		else if( userAgent.contains("msie") || userAgent.contains("trident") ) 
			return "MSIE";
		else if( userAgent.contains("opera") ) 
			return "OPERA";
		else if( userAgent.contains("firefox") ) 
			return "FIREFOX";
		else if( userAgent.contains("chrome") ) 
			return "CHROME";
		else 
			return "SAFARI";
	}	// end of getBrowser

	/**
	 * <pre>
	 * 파일명을 브라우저별로 정의해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  String					// 
	 * @return String
	 */
	public static String getDisposition ( 
			String dispType, 
			String fileName, 
			String browser 
		) throws UnsupportedEncodingException { 

		String					dispoPrefix  = dispType +";filename=";
		String					encFileName  = null;

		if( "edge".equalsIgnoreCase(browser) ) { 
			encFileName = "\"" + URLEncoder.encode(fileName,"utf-8").replaceAll("\\+", "%20") +"\"";
		} else if( "msie".equalsIgnoreCase(browser) ) { 
			encFileName = URLEncoder.encode(fileName,"utf-8").replaceAll("\\+", "%20");
		} else if( "mozilla".equalsIgnoreCase(browser) ) { 
			encFileName = URLEncoder.encode(fileName, "utf-8").replaceAll("\\+", "%20");
		} else if( "opera".equalsIgnoreCase(browser) ) { 
			encFileName = "\"" + new String(fileName.getBytes("utf-8"), "8859_1") +"\"";
		} else if( "firefox".equalsIgnoreCase(browser) || "unknown".equalsIgnoreCase(browser) ) { 
			encFileName = new String(fileName.getBytes("utf-8"), "8859_1");
		} else if( "chrome".equalsIgnoreCase(browser) ) {
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < fileName.length(); i++) {
				char c = fileName.charAt(i);
				if (c > '~') {
					sb.append(URLEncoder.encode("" + c, "utf-8"));
				} else {
					sb.append(c);
				}
			}
			encFileName = sb.toString();
		} else {
			encFileName = URLEncoder.encode(fileName, "utf-8").replaceAll("\\+", "%20");
		}

		return dispoPrefix +"\""+ encFileName +"\"";
	}	// end of getDisposition

	/**
	 * <pre>
	 * 파일명을 브라우저별로 정의해주는 메소드
	 * </pre>
	 * 
	 * @param  HttpServletRequest		// 
	 * @param  HttpServletResponse		// 
	 * @param  String					// 
	 * @return None
	 */
	public static void setDisposition ( 
			HttpServletRequest request, 
			HttpServletResponse response, 
			String fileName 
		) throws Exception { 

		String					browser      = getBrowser( request, response );
		String					dispoPrefix  = "attachment;filename=";
		String					encFileName  = null;

		if( "edge".equalsIgnoreCase(browser) ) { 
			encFileName = "\"" + URLEncoder.encode(fileName,"utf-8").replaceAll("\\+", "%20") +"\"";
		} else if( "msie".equalsIgnoreCase(browser) ) { 
			encFileName = URLEncoder.encode(fileName, "utf-8").replaceAll("\\+", "%20");
		} else if( "mozilla".equalsIgnoreCase(browser) ) { 
			encFileName = URLEncoder.encode(fileName, "utf-8").replaceAll("\\+", "%20");
		} else if( "opera".equalsIgnoreCase(browser) ) { 
			encFileName = "\""+ new String(fileName.getBytes("utf-8"), "8859_1") +"\"";
		} else if( "firefox".equalsIgnoreCase(browser) || "unknown".equalsIgnoreCase(browser) ) { 
			encFileName = new String(fileName.getBytes("utf-8"), "8859_1");
		} else if( "chrome".equalsIgnoreCase(browser) ) { 
			StringBuffer sb = new StringBuffer();
			for( int i = 0; i < fileName.length(); i++ ) { 
				char c = fileName.charAt(i);
				sb.append(c > '~' ? URLEncoder.encode(""+ c, "utf-8") : c);
			}
			encFileName = sb.toString();
		} else { 
			//throw new RuntimeException("Not supported browser");
			throw new IOException("Not supported browser");
		}

		response.setHeader("Content-Disposition", dispoPrefix + encFileName);

		if( "opera".equalsIgnoreCase(browser) ) 
			response.setContentType("application/octet-stream;charset=utf-8");
	}	// end of setDisposition

	/**
	 * <pre>
	 * 메인프레임 호스트 파일전송시 DBCS Character를 변환해주는 메소드
	 * </pre>
	 * 
	 * @param  String					// 
	 * @param  String					// 
	 * @param  HttpServletRequest		// 
	 * @param  HttpServletResponse		// 
	 * @return None
	 */
	public static void setDBCSHeader ( 
			HttpServletRequest request, 
			HttpServletResponse response, 
			String header, 
			String value 
		) { 

		byte bytes[];
		try { 
			bytes = value.getBytes(response.getCharacterEncoding());
		} catch( Exception E ) { 
			bytes = value.getBytes();
		}	// end of try~catch

		char c[] = new char[bytes.length];
		for( int i = 0; i < bytes.length; i++ ) c[i] = (char)(((char)bytes[i]) & 0xff);
		response.setHeader(header,new String(c));
	}	// end of setDBCSHeader


	/**
	 * <pre>
	 * 썸네일 정보을(를) 등록처리해주는 메소드(내부용)
	 * </pre>
	 * 
	 * @param  HttpServletRequest		// 
	 * @param  HttpServletResponse		// 
	 * @param  Map						// 
	 * @param  Integer					// 
	 * @param  String					// 
	 * @param  String					// 
	 * @param  String					// 
	 * @Return Integer
	 * @throws SQLException
	 * @throws Exception
	 */
	public static int doActOfCreateFileThmb ( 
			HttpServletRequest request, 
			HttpServletResponse response, 
			BaseCmmnService baseCmmnService, 
			BaseCommonVO systemVO, 
			BaseMemberVO memberVO, 
			Map<String, MultipartFile> fileMap, 
			int fileCnt, 
			String filePath, 
			String lnkName, 
			String lnkKey 
		) throws Exception { 

		BaseFilesVO				fileView     = new BaseFilesVO();
		String					fileRel      = "";
		String					fileAbs      = "";

		try { 
			if( !(fileMap == null || fileMap.size() <= 0) ) { 
				filePath = filePath +"/"+ systemVO.getToYear() +"/"+ systemVO.getToMonth();
				if( !existDirectory( BaseUtility.getFileBase() + filePath ) ) createDirectory( BaseUtility.getFileBase() + filePath );

				for( MultipartFile mFile : fileMap.values() ) { 
					if( !(mFile.isEmpty() || mFile.getSize() < 0) && "fileThmb".equals(mFile.getName()) ) { 
						fileRel = mFile.getOriginalFilename();
						fileAbs = filePath +"/"+ System.currentTimeMillis() + splitFileName( fileRel )[1];
						mFile.transferTo(new File(BaseUtility.getFileBase() + fileAbs));
log.debug("fileName: "+ mFile.getOriginalFilename() +" |fileSize: "+ mFile.getSize());

						// 입력내역 취득(통합파일)
						fileView = new BaseFilesVO();
						fileView.setLnkName(lnkName);
						fileView.setLnkKey(lnkKey);
						fileView = (BaseFilesVO)baseCmmnService.getInfoOfDataFileSeq( fileView );
						if( fileView == null ) { 
							fileView = new BaseFilesVO();
							fileView.setLnkName(lnkName);
							fileView.setLnkKey(lnkKey);
							fileView.setFileSeq("1");
						}
						fileView.setFileUUID(BaseCategory.encryptSHA256( fileView.getLnkName() + fileView.getLnkKey() + fileView.getFileSeq() ).toUpperCase());
						fileView.setFileType(getFileLowerExt( fileRel ));
						fileView.setFileRel(fileRel);
						fileView.setFileAbs(fileAbs);
						fileView.setFileThumb(fileRel);
						fileView.setFileLSize(String.valueOf(mFile.getSize()));
						fileView.setUseExpt("N");
						fileView.setUseDele("N");
						fileView.setCreIPv4(BaseUtility.reqIPv4Address( request ));
						fileView.setModIPv4(BaseUtility.reqIPv4Address( request ));
						fileView.setCreUser(memberVO.getUserPID());
						fileView.setModUser(memberVO.getUserPID());

						baseCmmnService.doRtnOfCreateDataFile( fileView );
						fileCnt++;
					}
				}
			}
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return fileCnt;
	}	// end of doActOfCreateFileThmb

	/**
	 * <pre>
	 * 첨부파일 정보을(를) 등록처리해주는 메소드(내부용)
	 * </pre>
	 * 
	 * @param  HttpServletRequest		// 
	 * @param  HttpServletResponse		// 
	 * @param  Map						// 
	 * @param  Integer					// 
	 * @param  String					// 
	 * @param  String					// 
	 * @param  String					// 
	 * @Return Integer
	 * @throws SQLException
	 * @throws Exception
	 */
	public static int doActOfCreateFileBase ( 
			HttpServletRequest request, 
			HttpServletResponse response, 
			BaseCmmnService baseCmmnService, 
			BaseCommonVO systemVO, 
			BaseMemberVO memberVO, 
			Map<String, MultipartFile> fileMap, 
			int fileCnt, 
			String filePath, 
			String lnkName, 
			String lnkKey 
		) throws Exception { 

		BaseFilesVO				fileView     = new BaseFilesVO();
		String					fileRel      = "";
		String					fileAbs      = "";

		try { 
			if( !(fileMap == null || fileMap.size() <= 0) ) { 
				filePath = filePath +"/"+ systemVO.getToYear() +"/"+ systemVO.getToMonth();
				if( !existDirectory( BaseUtility.getFileBase() + filePath ) ) createDirectory( BaseUtility.getFileBase() + filePath );

				for( MultipartFile mFile : fileMap.values() ) { 
					if( !(mFile.isEmpty() || mFile.getSize() < 0) && !"fileThmb".equals(mFile.getName()) ) { 
						fileRel = mFile.getOriginalFilename();
						fileAbs = filePath +"/"+ System.currentTimeMillis() + splitFileName( fileRel )[1];
log.debug("fileName: "+ mFile.getOriginalFilename() +" |fileSize: "+ mFile.getSize());
						mFile.transferTo(new File(BaseUtility.getFileBase() + fileAbs));

						// 입력내역 취득(통합파일)
						fileView = new BaseFilesVO();
						fileView.setLnkName(lnkName);
						fileView.setLnkKey(lnkKey);
						fileView = (BaseFilesVO)baseCmmnService.getInfoOfDataFileSeq( fileView );
						if( fileView == null ) { 
							fileView = new BaseFilesVO();
							fileView.setLnkName(lnkName);
							fileView.setLnkKey(lnkKey);
							fileView.setFileSeq("1");
						}
						fileView.setFileUUID(BaseCategory.encryptSHA256( fileView.getLnkName() + fileView.getLnkKey() + fileView.getFileSeq() ).toUpperCase());
						fileView.setFileType(getFileLowerExt( fileRel ));
						fileView.setFileRel(fileRel);
						fileView.setFileAbs(fileAbs);
						fileView.setFileUri("/copilot/"+BaseCategory.encryptSHA256( fileAbs )+fileView.getFileSeq());
						fileView.setFileLSize(String.valueOf(mFile.getSize()));
						fileView.setUseExpt("N");
						fileView.setUseDele("N");
						fileView.setCreIPv4(BaseUtility.reqIPv4Address( request ));
						fileView.setModIPv4(BaseUtility.reqIPv4Address( request ));
						fileView.setCreUser(memberVO.getUserPID());
						fileView.setModUser(memberVO.getUserPID());

						baseCmmnService.doRtnOfCreateDataFile( fileView );
						fileCnt++;
					}
				}
			}
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return fileCnt;
	}	// end of doActOfCreateFileBase

	/**
	 * <pre>
	 * 첨부파일 정보을(를) 상세조회해주는 메소드(내부용)
	 * </pre>
	 * 
	 * @param  HttpServletRequest		// 
	 * @param  HttpServletResponse		// 
	 * @param  List						// 
	 * @param  String					// 다운유형
	 * @throws Exception
	 */
	public static String getInfoOfFileBBS ( 
			HttpServletRequest request, 
			HttpServletResponse response, 
			List<BaseFilesVO> fileList, 
			String optType 
		) throws Exception { 

		String					rFile        = "";
		String					sFile        = "";
		String					tItle        = "";
		String					realBase     = BaseUtility.getFileBase();

		defS		= "S".equalsIgnoreCase(optType) ? "첨부 파일이 없습니다." : "";

		try { 
			retS = "";

			for( BaseFilesVO fileInfo : fileList ) { 
				if( BaseUtility.isValidNull( fileInfo.getFileAbs() ) ) { 
					retS += BaseUtility.null2Blank( fileInfo.getFileRel() );
				} else { 
					if( "D".equalsIgnoreCase(optType) ) { 
						retS += fileInfo.getFileRel() +"<br/>";
					} else { 
						File file = new File(realBase + BaseUtility.null2Blank( fileInfo.getFileAbs() ));
						if( "M".equalsIgnoreCase(optType) ) { 
							if( file == null || !file.isFile() || file.length() == 0 ) { 
								rFile = "";
								tItle += BaseUtility.null2Blank( fileInfo.getFileRel() ) +"\n";
							} else { 
								rFile += BaseUtility.null2Blank( fileInfo.getFileRel() ) +"|";
								sFile += BaseUtility.null2Blank( fileInfo.getFileAbs() ) +"|";
								tItle += BaseUtility.null2Blank( fileInfo.getFileRel() ) +"\n";
							}
						} else { 
							if( file == null || !file.isFile() || file.length() == 0 ) {
								retS += "<span style='color:#0b5085;cursor:pointer;' onmouseover='javascript:this.style.color=\"#7c7507\";' onmouseout='javascript:this.style.color=\"#0b5085\";' onclick='javascript:alert(\"다운로드할 파일이 존재하지 않습니다.\\r관리자에게 문의바랍니다.\")'>" + fileInfo.getFileRel() + "</span><br/>";
							}else {
								retS += "<a href='javascript:void(0);' onclick='previewAjax(\"" + fileInfo.getFileAbs() + "\")' class='btn_view' title='바로보기'>바로보기</a>&nbsp;";
								retS += "<a href='javascript:void(0);' onclick='preListen(\"" + fileInfo.getFileAbs() + "\")' class='btn_sound' title='바로듣기'>바로듣기</a>&nbsp;&nbsp;";
								retS += "<span style='color:#0b5085;cursor:pointer;' onmouseover='javascript:this.style.color=\"#7c7507\";' onmouseout='javascript:this.style.color=\"#0b5085\";' onclick='fnActDownload(\"" + fileInfo.getFileUUID() + "\")'>" + fileInfo.getFileRel() + "</span><br/>";
							}
						}
					}
				}
			}

			if( "M".equalsIgnoreCase(optType) ) 
				retS = "".equals(tItle) ? defS : ("style='cursor:pointer;' tItle='"+ tItle.substring(0, tItle.lastIndexOf("\n")) +"' onclick='"+ ("".equals(rFile) ? "javascript:alert(\"다운로드할 파일이 존재하지 않습니다.\\r관리자에게 문의바랍니다.\")" : "fnActDownload(\""+ rFile.substring(0, rFile.lastIndexOf("|")) +"\",\""+ sFile.substring(0, sFile.lastIndexOf("|")) +"\",\"dnZip\")")+ "'");
			else 
				retS = "".equals(retS ) ? defS : retS.substring(0, retS.lastIndexOf("<br/>"));
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			return defS;
		} finally { 
		}	// end of try~catch~finally

		return retS;
	}	// end of getInfoOfFileBBS

	/**
	 * <pre>
	 * 부록파일 정보을(를) 상세조회해주는 메소드(내부용)
	 * </pre>
	 * 
	 * @param  HttpServletRequest		// 
	 * @param  HttpServletResponse		// 
	 * @param  List						// 
	 * @param  String					// 다운유형
	 * @throws Exception
	 */
	public static String getInfoOfFileAgenda ( 
			HttpServletRequest request, 
			HttpServletResponse response, 
			List<BaseAgendaVO> fileList, 
			String optType 
		) throws Exception { 

		String					rFile        = "";
		String					sFile        = "";
		String					tItle        = "";
		String					realBase     = BaseUtility.getFileBase().replaceAll("/datas", "") + BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DATAS_APP" );

		defS		= "S".equalsIgnoreCase(optType) ? "첨부 파일이 없습니다." : "";

		try { 
			retS = "";

			for( BaseAgendaVO fileInfo : fileList ) { 
				if( BaseUtility.isValidNull( fileInfo.getAppName() ) ) { 
					retS += BaseUtility.null2Blank( fileInfo.getAppTitle() );
				} else { 
					if( "D".equalsIgnoreCase(optType) ) { 
						retS += fileInfo.getAppTitle() +"<br/>";
					} else { 
						File file = new File(realBase +"/"+ BaseUtility.null2Char( fileInfo.getAppName(), "", fileInfo.getAppName() +"."+ fileInfo.getAppExt() ));
						if( "M".equalsIgnoreCase(optType) ) { 
							if( file == null || !file.isFile() || file.length() == 0 ) { 
								rFile = "";
								tItle += BaseUtility.null2Blank( fileInfo.getAppTitle() ) +"\n";
							} else { 
								rFile += BaseUtility.null2Blank( fileInfo.getAppTitle() ) +"|";
								sFile += BaseUtility.null2Blank( fileInfo.getAppName() ) +"|";
								tItle += BaseUtility.null2Blank( fileInfo.getAppTitle() ) +"\n";
							}
						} else { 
							if( file == null || !file.isFile() || file.length() == 0 ) 
								retS += "<span style='color:#0b5085;cursor:pointer;' onmouseover='javascript:this.style.color=\"#7c7507\";' onmouseout='javascript:this.style.color=\"#0b5085\";' onclick='javascript:alert(\"다운로드할 파일이 존재하지 않습니다.\\r관리자에게 문의바랍니다.\")'>"+ fileInfo.getAppTitle() +"</span><br/>";
							else {
								retS += "<a href='javascript:void(0);' onclick='previewAjaxApp(\"" + fileInfo.getAppName() + "." + fileInfo.getAppExt() + "\")' class='btn_view' title='바로보기'>바로보기</a>&nbsp;";
								retS += "<a href='javascript:void(0);' onclick='preListenApp(\"" + fileInfo.getAppName() + "." + fileInfo.getAppExt() + "\")' class='btn_sound' title='바로듣기'>바로듣기</a>&nbsp;&nbsp;";
								retS += "<span style='color:#0b5085;cursor:pointer;' onmouseover='javascript:this.style.color=\"#7c7507\";' onmouseout='javascript:this.style.color=\"#0b5085\";' onclick='fnAppDownload(\"" + fileInfo.getAppUUID() + "\")'>" + fileInfo.getAppTitle() + "</span><br/>";
							}
						}
					}
				}
			}

			if( "M".equalsIgnoreCase(optType) ) 
				retS = "".equals(tItle) ? defS : ("style='cursor:pointer;' tItle='"+ tItle.substring(0, tItle.lastIndexOf("\n")) +"' onclick='"+ ("".equals(rFile) ? "javascript:alert(\"다운로드할 파일이 존재하지 않습니다.\\r관리자에게 문의바랍니다.\")" : "fnActDownload(\""+ rFile.substring(0, rFile.lastIndexOf("|")) +"\",\""+ sFile.substring(0, sFile.lastIndexOf("|")) +"\",\"dnZip\")")+ "'");
			else 
				retS = "".equals(retS ) ? defS : retS.substring(0, retS.lastIndexOf("<br/>"));
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			return defS;
		} finally { 
		}	// end of try~catch~finally

		return retS;
	}	// end of getInfoOfFileAgenda


	// 첨부파일 사진 보여주기
	public static String getInfoOfFileViewImg (
			HttpServletRequest request,
			HttpServletResponse response,
			List<BaseFilesVO> fileList,
			String optType
	) throws Exception {

		String					rFile        = "";
		String					sFile        = "";
		String					tItle        = "";
		String					realBase     = BaseUtility.getFileBase();

		defS		= "첨부 파일이 없습니다.";

		try {
			retS = "";

			for( BaseFilesVO fileInfo : fileList ) {
				if( BaseUtility.isValidNull( fileInfo.getFileAbs() ) ) {
					retS += BaseUtility.null2Blank( fileInfo.getFileRel() );
				} else {
					File file = new File(realBase + BaseUtility.null2Blank( fileInfo.getFileAbs() ));

//					String filePath = BaseUtility.getFileBase() +"/"+ fileInfo.getFileAbs();
					String filePath = "/files" +"/"+ fileInfo.getFileAbs(); // 윈도우 로컬 테스트 위해

//log.debug("======= filePath: " + filePath);

					if( file == null || !file.isFile() || file.length() == 0 || "hwp".equals(getExtension(file)) || "xls".equals(getExtension(file))) {
						retS += "";
					}else {
						if("M".equals(optType))
							retS += "<img src='" + filePath + "' alt='" + fileInfo.getFileRel() + "' class='press-view-img' />";
						else {
							retS = "<img src='" + filePath + "' alt='" + fileInfo.getFileRel() + "' class='press-view-img' />";
							break;
						}
					}
				}
			}

//log.debug("======= retS: " + retS);

		} catch( Exception E ) {
			log.debug(E.getMessage());
			return defS;
		} finally {
		}	// end of try~catch~finally

		return retS;
	}	// end of getInfoOfFileViewImg

	public static String getExtension(File file) {
		String fileName = file.getName();
		String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
		return extension;
	}

}	// end of class