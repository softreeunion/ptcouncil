/*******************************************************************************
  Program ID  : BaseCategory
  Description : 각종 Category들의 공통모듈
  Author      : HexaMedia new0man
  Write Date  : 2019.06.04
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.06.04  최초 생성
  v1.0    HexaMedia new0man      2019.06.04  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.cmmn.util;

import java.security.*;
import java.util.*;

import javax.annotation.Resource;
import javax.crypto.*;
import javax.servlet.http.*;

import org.slf4j.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.vo.*;

public class BaseCategory { 

	@Resource(name="propertiesService")
	protected static EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected static BaseCmmnService				baseCmmnService;

	protected static Logger						log          = LoggerFactory.getLogger(BaseCategory.class);
	protected static String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected static String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected static String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");


	/**
	 * <pre>
	 * 분류구분 내역을(를) 상세조회해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @param  String							// 
	 * @return BaseCommonVO
	 * @throws Exception
	 */
	public static BaseCommonVO getInfoOfCateBASE ( 
			String argValue1, 
			String argValue2 
		) throws Exception { 

		BaseCommonVO			searchVO     = new BaseCommonVO();
		BaseCommonVO			cateInfo     = new BaseCommonVO();

		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setCodLrg(argValue1);
		searchVO.setCodName(argValue2);

		try { 
			cateInfo = (BaseCommonVO)baseCmmnService.getInfoOfCate( searchVO );
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		}	// end of try~catch~finally

		return cateInfo;
	}	// end of getInfoOfCateBASE

	/**
	 * <pre>
	 * 분류정보 내역을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	public static List<BaseCommonVO> getListOfCateBASE ( 
			String argValue 
		) throws Exception { 

		BaseCommonVO			searchVO     = new BaseCommonVO();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();

		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setCodLrg(argValue);

		try { 
			cateList = (List<BaseCommonVO>)baseCmmnService.getListOfCateAll( searchVO );
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		}	// end of try~catch~finally

		return cateList;
	}	// end of getListOfCateBASE

	/**
	 * <pre>
	 * 분류정보(대) 내역을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	public static List<BaseCommonVO> getListOfCateLRG ( 
			String argValue 
		) throws Exception { 

		BaseCommonVO			searchVO     = new BaseCommonVO();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();

		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setCodLrg(argValue);

		try { 
			cateList = (List<BaseCommonVO>)baseCmmnService.getListOfCateLrg( searchVO );
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		}	// end of try~catch~finally

		return cateList;
	}	// end of getListOfCateLRG

	/**
	 * <pre>
	 * 분류정보(중) 내역을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	public static List<BaseCommonVO> getListOfCateMID ( 
			String argValue1, 
			String argValue2 
		) throws Exception { 

		BaseCommonVO			searchVO     = new BaseCommonVO();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();

		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setCodLrg(argValue1);
		searchVO.setCodMid(argValue2);

		try { 
			cateList = (List<BaseCommonVO>)baseCmmnService.getListOfCateMid( searchVO );
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		}	// end of try~catch~finally

		return cateList;
	}	// end of getListOfCateMID

	/**
	 * <pre>
	 * 접속구분 내역을 확인조회해주는 메소드
	 * </pre>
	 * 
	 * @param  HttpServletRequest				// 
	 * @return Boolean
	 * @throws Exception
	 */
	public static boolean getCheckOfNetwork ( 
			HttpServletRequest req 
		) throws Exception { 

		String					chkValue     = BaseUtility.getIPv4Address( req );
		boolean				retB         = false;

		try { 
log.debug(chkValue);
			if( !BaseUtility.isValidNull( chkValue ) ) { 
				if( chkValue.contains("localhost") || chkValue.contains("127.0.0.1") || chkValue.contains("0:0:0:0:0:0:0:1") || chkValue.contains("10.10.10.") || chkValue.contains("175.197.148.") || chkValue.contains("192.168.") ) 
					retB = true;
				else 
					retB = !((int)baseCmmnService.getCountOfNetConn( chkValue ) <= 0);
			}
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		}	// end of try~catch~finally

		return retB;
	}	// end of getCheckOfNetwork

	/**
	 * <pre>
	 * 관리자 내역을 확인조회해주는 메소드
	 * </pre>
	 * 
	 * @param  HttpServletRequest				// 
	 * @return Boolean
	 * @throws Exception
	 */
	public static boolean getCheckOfAdmin ( 
			String data 
		) throws Exception { 

		boolean				retB         = false;

		try { 
			if( !BaseUtility.isValidNull( data ) ) { 
				retB = "A".equalsIgnoreCase(data) || "SU".equalsIgnoreCase(data) || "CM".equalsIgnoreCase(data);
			}
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		}	// end of try~catch~finally

		return retB;
	}	// end of getCheckOfAdmin


	private static String byteToHexString ( 
			byte[] data 
		) { 

		StringBuffer sb = new StringBuffer();
		for( byte b : data) { 
			sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
		}

		return sb.toString();
	}	// end of byteToHexString

	public static String encryptSHA256 ( 
			String data 
		) throws Exception { 

		if( BaseUtility.isValidNull( data ) )	return "";

		MessageDigest md	= MessageDigest.getInstance("SHA-256");
		md.update(data.getBytes());

		return byteToHexString( md.digest() );
	}	// end of encryptSHA256

	public static String encryptMD5 ( 
			String data 
		) throws Exception { 

		if( BaseUtility.isValidNull( data ) )	return "";

		MessageDigest md	= MessageDigest.getInstance("MD5");
		md.update(data.getBytes());

		return byteToHexString( md.digest() );
	}	// end of encryptMD5

	public static String decryptRSA ( 
			PrivateKey pKey, 
			String data 
		) throws Exception { 

		Cipher cipher = Cipher.getInstance("RSA");
		byte[] encryptedBytes = BaseUtility.hex2ByteArray( data );
		cipher.init(Cipher.DECRYPT_MODE, pKey);
		byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
		String decryptedValue = new String(decryptedBytes, "utf-8");

		return decryptedValue;
	}	// end of decryptRSA

}	// end of class