/*******************************************************************************
  Program ID  : BaseCmmnService
  Description : 공통 관리에 관한 Service Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.06.04
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.06.04  최초 생성
  v1.0    HexaMedia new0man      2019.06.04  평택시의회 Site Renewal
  v1.1    HexaMedia new0man      2020.05.08  대수별 기간 최소최대 선택일지정
 *******************************************************************************/
package egovframework.ptcc.cmmn.service;

import java.util.List;

import egovframework.ptcc.cmmn.vo.*;

public interface BaseCmmnService {

	// 시스템일자 취득
	BaseCommonVO getSystemOfDate() throws Exception;
	// 접속지 존재 여부 취득
	int getCountOfNetConn(String paramVal) throws Exception;

	// 분류구분 관리
	List<BaseCommonVO> getListOfYear(String paramVal) throws Exception;
	List<BaseCommonVO> getListOfClassALL(String paramVal) throws Exception;
	BaseCommonVO getInfoOfClassNOW(String paramVal) throws Exception;
	BaseCommonVO getInfoOfClassAGO(String paramVal) throws Exception;
	List<BaseCommonVO> getListOfRoundALL(String paramVal) throws Exception;
	BaseCommonVO getInfoOfRoundNOW(String paramVal) throws Exception;
	List<BaseCommonVO> getListOfGroup(String paramVal) throws Exception;
	List<BaseCommonVO> getListOfSugKind(String paramVal) throws Exception;
	List<BaseCommonVO> getListOfResKind(String paramVal) throws Exception;
	List<BaseCommonVO> getListOfParty(String paramVal) throws Exception;
	List<BaseCommonVO> getListOfComKind(String paramVal) throws Exception;
	List<BaseCommonVO> getListOfStand(String paramVal) throws Exception;
	List<BaseCommonVO> getListOfSpecial(String paramVal) throws Exception;
	List<BaseCommonVO> getListOfAudit(String paramVal) throws Exception;
	List<BaseCommonVO> getListOfBudget(String paramVal) throws Exception;
	List<BaseCommonVO> getListOfMembALL(String paramVal) throws Exception;
	List<BaseCommonVO> getListOfMembNOW(String paramVal) throws Exception;

	// 분류구분 관리
	BaseCommonVO getInfoOfCate(BaseCommonVO paramObj) throws Exception;
	List<BaseCommonVO> getListOfCateAll(BaseCommonVO paramObj) throws Exception;
	List<BaseCommonVO> getListOfCateLrg(BaseCommonVO paramObj) throws Exception;
	List<BaseCommonVO> getListOfCateMid(BaseCommonVO paramObj) throws Exception;

	// 분류권한 관리
	List<BaseCommonVO> getListOfAuth(BaseCommonVO paramObj) throws Exception;

	// 회의명칭 취득
	String getInfoOfConfName(String paramVal) throws Exception;
	BaseCommonVO getInfoOfConfDate(String paramVal) throws Exception;

	// 통합파일 관리
	String getInfoOfDataFileUUID(String paramVal) throws Exception;
	int getCountOfDataFile(BaseCommonVO paramObj) throws Exception;
	List<BaseFilesVO> getListOfDataFile(BaseCommonVO paramObj) throws Exception;
	List<BaseFilesVO> getListOfThmbFile(BaseCommonVO paramObj) throws Exception;
	BaseFilesVO getDetailOfDataFile(BaseCommonVO paramObj) throws Exception;
	BaseFilesVO getDetailOfDataFile(String paramVal) throws Exception;
	BaseFilesVO getInfoOfDataFileSeq(BaseFilesVO paramObj) throws Exception;
	boolean doRtnOfCreateDataFile(BaseFilesVO paramObj) throws Exception;
	boolean doRtnOfUpdateDataFile(BaseFilesVO paramObj) throws Exception;
	boolean doRtnOfDeleteDataFile(BaseFilesVO paramObj) throws Exception;
	boolean doRtnOfModifyDataFile(BaseFilesVO paramObj) throws Exception;

	BaseFilesVO getDetailOfBodyFile(String paramVal) throws Exception;
	BaseAgendaVO getDetailOfSuppFile(String paramVal) throws Exception;

	// 방문자 관리
	String getInfoOfVisitUUID(String paramVal) throws Exception;
	int getCountOfVisit(BaseCommonVO paramObj) throws Exception;
	List<BaseVisitsVO> getListOfVisit(BaseCommonVO paramObj) throws Exception;
	List<BaseVisitsVO> getListOfVisitSheet(BaseCommonVO paramObj) throws Exception;
	List<BaseVisitsVO> getListOfVisitSheetYear(BaseCommonVO paramObj) throws Exception;
	List<BaseVisitsVO> getListOfVisitSheetMonth(BaseCommonVO paramObj) throws Exception;
	List<BaseVisitsVO> getListOfVisitSheetDate(BaseCommonVO paramObj) throws Exception;
	BaseVisitsVO getDetailOfVisit(BaseCommonVO paramObj) throws Exception;
	BaseVisitsVO getDetailOfVisit(String paramVal) throws Exception;
	BaseVisitsVO getInfoOfVisitSeq(BaseVisitsVO paramObj) throws Exception;
	BaseVisitsVO getInfoOfVisitYear(BaseVisitsVO paramObj) throws Exception;
	boolean doRtnOfCreateVisit(BaseVisitsVO paramObj) throws Exception;
	boolean doRtnOfUpdateVisit(BaseVisitsVO paramObj) throws Exception;
	boolean doRtnOfDeleteVisit(BaseVisitsVO paramObj) throws Exception;
	boolean doRtnOfModifyVisit(BaseVisitsVO paramObj) throws Exception;
	BaseVisitsVO getStatOfVisit(BaseCommonVO paramObj) throws Exception;
	int getCountOfVisitDay(String paramVal) throws Exception;

}	// end of interface