package egovframework.ptcc.cmmn.service.interceptor;

import javax.servlet.http.*;

import org.slf4j.*;

import egovframework.ptcc.cmmn.util.UserDetailsHelper;

public class AuthHttpSessionListener implements HttpSessionListener, HttpSessionAttributeListener { 

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());

	@Override
	public void sessionCreated(HttpSessionEvent event) { 
		log.debug("ID: "+ (String)event.getSession().getId() +" | "+ event.getSession().getMaxInactiveInterval());
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) { 
		log.debug("ID: "+ (String)event.getSession().getId() +" | "+ ((long)System.currentTimeMillis() - (long)event.getSession().getLastAccessedTime()) +"ms 만에 세션이 죽음");
		if(UserDetailsHelper.isAuthenticatedUser() || UserDetailsHelper.isAuthenticatedAdmin()) event.getSession().setMaxInactiveInterval(60*30); // 30분 연장
	}

	@Override
	public void attributeAdded(HttpSessionBindingEvent event) { 
	}

	@Override
	public void attributeRemoved(HttpSessionBindingEvent event) { 
		if(UserDetailsHelper.isAuthenticatedUser() || UserDetailsHelper.isAuthenticatedAdmin()) event.getSession().setMaxInactiveInterval(60*30); // 30분 연장
	}

	@Override
	public void attributeReplaced(HttpSessionBindingEvent event) {
	}

}	// end of class