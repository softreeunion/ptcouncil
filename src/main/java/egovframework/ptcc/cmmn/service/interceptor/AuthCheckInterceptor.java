package egovframework.ptcc.cmmn.service.interceptor;

import javax.servlet.http.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.*;

import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.util.*;

public class AuthCheckInterceptor extends org.springframework.web.servlet.handler.HandlerInterceptorAdapter { 

	@Autowired
	private WebApplicationContext			webContext;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws ModelAndViewDefiningException { 

		String					webBase      = webContext.getServletContext().getContextPath();
		String					reqUri       = request == null ? null : request.getRequestURI();
		HttpSession				session      = request == null ? null : request.getSession();
		ModelMap				model        = new ModelMap();
		ModelAndView			mav          = new ModelAndView();

		try { 
			if( !BaseUtility.isValidNull( reqUri ) ) { 
				// 관리자 - 모든 기능
				if( reqUri.contains("/ptnhexa") ) { 
//System.out.println("reqUri: "+ reqUri.contains("/ptnhexa"));
					if( !UserDetailsHelper.isAuthenticatedAdmin() ) { 
//						mav = new ModelAndView("forward:/cmmn/forward.do");
//						mav.addObject("message"       , "로그인 정보가 존재하지 않습니다.\n\n로그인 화면으로 전환합니다.");
//						mav.addObject("returnUrl"     , webBase+"/ptnhexa/index.do");
//						throw new ModelAndViewDefiningException(mav);
						mav = new ModelAndView("redirect:/ptnhexa/index.do");
//						request.setAttribute("message"       , "로그인 정보가 존재하지 않습니다.\\n\\n로그인 화면으로 전환합니다.");
//						request.setAttribute("returnUrl"     , webBase+"/ptnhexa/index.do");
//						response.sendRedirect(webBase +"/cmmn/forward.do");
//						return false;
					}
				}

				return true;
			}
		} catch( Exception E ) { 
			E.printStackTrace();
		}

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception { 
		super.postHandle(request, response, handler, modelAndView);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception { 
		super.afterCompletion(request, response, handler, ex);
	}

	@Override
	public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception { 
		super.afterConcurrentHandlingStarted(request, response, handler);
	}

}	// end of class