package egovframework.ptcc.cmmn.service.interceptor;

import javax.servlet.http.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.*;
import org.springframework.web.context.request.*;
import org.springframework.web.context.support.*;


import egovframework.com.cmm.util.BaseUtility;
import egovframework.ptcc.cmmn.service.impl.BaseCmmnDAO;
import egovframework.ptcc.cmmn.vo.BaseVisitsVO;

public class VisitHttpSessionListener implements HttpSessionListener { 

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());

	@Override
	public void sessionCreated(HttpSessionEvent event) { 
		HttpSession session = event.getSession();
		WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(session.getServletContext());
		HttpServletRequest req = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();
		BaseCmmnDAO baseCmmnDAO = (BaseCmmnDAO)wac.getBean("baseCmmnDAO");
		BaseVisitsVO visitVO = new BaseVisitsVO();
		try { 
			visitVO.setVsnYear(BaseUtility.getDateOfNow("yyyy"));
log.debug("getYear: "+ visitVO.getVsnYear());
			visitVO = (BaseVisitsVO)baseCmmnDAO.getInfoOfVisitSeq(visitVO);
			visitVO.setVsnIPv4(BaseUtility.reqIPv4Address(req));
			visitVO.setVsnSite(req.getRequestURL().toString());
			visitVO.setVsnReferer(req.getHeader("REFERER"));
			visitVO.setVsnAgent(req.getHeader("User-Agent"));
			visitVO.setCreUser("USR");
			baseCmmnDAO.doRtnOfCreateVisit(visitVO);
log.debug("This Visitor is Created");
log.debug("reqIPv4: "+ visitVO.getVsnIPv4());
log.debug("forIPv4: "+ req.getHeader("X-FORWARDED-FOR"));
log.debug("getIPv4: "+ req.getRemoteAddr());
//			visitVO = (BaseVisitsVO)baseCmmnDAO.getStatOfVisit(null);
//			session.removeAttribute("totsVisit");
//			session.removeAttribute("daysVisit");
//			session.setAttribute("totsVisit",BaseUtility.toNumberFormat(visitVO.getTotsCnt()));
//			session.setAttribute("daysVisit",BaseUtility.toNumberFormat(visitVO.getDaysCnt()));
//log.debug("This Visitor is Readable");
		} catch ( Exception E ) { 
			log.debug(E.getMessage());
		}
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
	}

}	// end of class