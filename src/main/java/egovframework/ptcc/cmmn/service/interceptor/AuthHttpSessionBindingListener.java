package egovframework.ptcc.cmmn.service.interceptor;

import javax.servlet.http.*;

public class AuthHttpSessionBindingListener implements HttpSessionBindingListener { 

	@Override
	public void valueBound(HttpSessionBindingEvent event){
		if(AuthMultiLoginPreventor.findByLoginId(event.getName())) AuthMultiLoginPreventor.invalidateByLoginId(event.getName());
		AuthMultiLoginPreventor.loginUsers.put(event.getName(), event.getSession());
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event){
		AuthMultiLoginPreventor.loginUsers.remove(event.getName(), event.getSession());
	}

}	// end of class