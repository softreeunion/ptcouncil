/*******************************************************************************
  Program ID  : BaseCmmnServiceImpl
  Description : 공통 관리에 관한 Service Implement Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.06.04
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.06.04  최초 생성
  v1.0    HexaMedia new0man      2019.06.04  평택시의회 Site Renewal
  v1.1    HexaMedia new0man      2020.05.08  대수별 기간 최소최대 선택일지정
 *******************************************************************************/
package egovframework.ptcc.cmmn.service.impl;

import java.util.*;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.vo.*;

@Service("baseCmmnService")
public class BaseCmmnServiceImpl extends egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl implements BaseCmmnService { 

	@Resource(name="baseCmmnDAO")
	private BaseCmmnDAO 				baseCmmnDAO;

	@Override
	public BaseCommonVO getSystemOfDate() throws Exception { 
		return (BaseCommonVO)baseCmmnDAO.getSystemOfDate();
	}
	@Override
	public int getCountOfNetConn(String paramVal) throws Exception { 
		return (int)baseCmmnDAO.getCountOfNetConn( paramVal );
	}
	@Override
	public List<BaseCommonVO> getListOfYear(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)baseCmmnDAO.getListOfYear( paramVal );
	}
	@Override
	public List<BaseCommonVO> getListOfClassALL(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)baseCmmnDAO.getListOfClassALL( paramVal );
	}
	@Override
	public BaseCommonVO getInfoOfClassNOW(String paramVal) throws Exception { 
		return (BaseCommonVO)baseCmmnDAO.getInfoOfClassNOW( paramVal );
	}
	@Override
	public BaseCommonVO getInfoOfClassAGO(String paramVal) throws Exception { 
		return (BaseCommonVO)baseCmmnDAO.getInfoOfClassAGO( paramVal );
	}
	@Override
	public List<BaseCommonVO> getListOfRoundALL(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)baseCmmnDAO.getListOfRoundALL( paramVal );
	}
	@Override
	public BaseCommonVO getInfoOfRoundNOW(String paramVal) throws Exception { 
		return (BaseCommonVO)baseCmmnDAO.getInfoOfRoundNOW( paramVal );
	}
	@Override
	public List<BaseCommonVO> getListOfGroup(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)baseCmmnDAO.getListOfGroup( paramVal );
	}
	@Override
	public List<BaseCommonVO> getListOfSugKind(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)baseCmmnDAO.getListOfSugKind( paramVal );
	}
	@Override
	public List<BaseCommonVO> getListOfResKind(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)baseCmmnDAO.getListOfResKind( paramVal );
	}
	@Override
	public List<BaseCommonVO> getListOfParty(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)baseCmmnDAO.getListOfParty( paramVal );
	}
	@Override
	public List<BaseCommonVO> getListOfComKind(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)baseCmmnDAO.getListOfComKind( paramVal );
	}
	@Override
	public List<BaseCommonVO> getListOfStand(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)baseCmmnDAO.getListOfStand( paramVal );
	}
	@Override
	public List<BaseCommonVO> getListOfSpecial(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)baseCmmnDAO.getListOfSpecial( paramVal );
	}
	@Override
	public List<BaseCommonVO> getListOfAudit(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)baseCmmnDAO.getListOfAudit( paramVal );
	}
	@Override
	public List<BaseCommonVO> getListOfBudget(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)baseCmmnDAO.getListOfBudget( paramVal );
	}
	@Override
	public List<BaseCommonVO> getListOfMembALL(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)baseCmmnDAO.getListOfMembALL( paramVal );
	}
	@Override
	public List<BaseCommonVO> getListOfMembNOW(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)baseCmmnDAO.getListOfMembNOW( paramVal );
	}
	@Override
	public BaseCommonVO getInfoOfCate(BaseCommonVO paramObj) throws Exception { 
		return (BaseCommonVO)baseCmmnDAO.getInfoOfCate( paramObj );
	}
	@Override
	public List<BaseCommonVO> getListOfCateAll(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseCommonVO>)baseCmmnDAO.getListOfCateAll( paramObj );
	}
	@Override
	public List<BaseCommonVO> getListOfCateLrg(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseCommonVO>)baseCmmnDAO.getListOfCateLrg( paramObj );
	}
	@Override
	public List<BaseCommonVO> getListOfCateMid(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseCommonVO>)baseCmmnDAO.getListOfCateMid( paramObj );
	}
	@Override
	public List<BaseCommonVO> getListOfAuth(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseCommonVO>)baseCmmnDAO.getListOfAuth( paramObj );
	}
	@Override
	public String getInfoOfConfName(String paramVal) throws Exception { 
		return (String)baseCmmnDAO.getInfoOfConfName( paramVal );
	}
	@Override
	public BaseCommonVO getInfoOfConfDate(String paramVal) throws Exception { 
		return (BaseCommonVO)baseCmmnDAO.getInfoOfConfDate( paramVal );
	}
	@Override
	public String getInfoOfDataFileUUID(String paramVal) throws Exception { 
		return (String)baseCmmnDAO.getInfoOfDataFileUUID( paramVal );
	}
	@Override
	public 	int getCountOfDataFile(BaseCommonVO paramObj) throws Exception {
		return (int)baseCmmnDAO.getCountOfDataFile( paramObj );
	}
	@Override
	public 	List<BaseFilesVO> getListOfDataFile(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseFilesVO>)baseCmmnDAO.getListOfDataFile( paramObj );
	}
	@Override
	public 	List<BaseFilesVO> getListOfThmbFile(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseFilesVO>)baseCmmnDAO.getListOfThmbFile( paramObj );
	}
	@Override
	public 	BaseFilesVO getDetailOfDataFile(BaseCommonVO paramObj) throws Exception { 
		return (BaseFilesVO)baseCmmnDAO.getDetailOfDataFile( paramObj );
	}
	@Override
	public 	BaseFilesVO getDetailOfDataFile(String paramVal) throws Exception { 
		return (BaseFilesVO)baseCmmnDAO.getDetailOfDataFile( paramVal );
	}
	@Override
	public BaseFilesVO getInfoOfDataFileSeq(BaseFilesVO paramObj) throws Exception { 
		return (BaseFilesVO)baseCmmnDAO.getInfoOfDataFileSeq( paramObj );
	}
	@Override
	public 	boolean doRtnOfCreateDataFile(BaseFilesVO paramObj) throws Exception { 
		return (boolean)baseCmmnDAO.doRtnOfCreateDataFile( paramObj );
	}
	@Override
	public 	boolean doRtnOfUpdateDataFile(BaseFilesVO paramObj) throws Exception { 
		return (boolean)baseCmmnDAO.doRtnOfUpdateDataFile( paramObj );
	}
	@Override
	public 	boolean doRtnOfDeleteDataFile(BaseFilesVO paramObj) throws Exception { 
		return (boolean)baseCmmnDAO.doRtnOfDeleteDataFile( paramObj );
	}
	@Override
	public 	boolean doRtnOfModifyDataFile(BaseFilesVO paramObj) throws Exception { 
		return (boolean)baseCmmnDAO.doRtnOfModifyDataFile( paramObj );
	}
	@Override
	public 	BaseFilesVO getDetailOfBodyFile(String paramVal) throws Exception { 
		return (BaseFilesVO)baseCmmnDAO.getDetailOfBodyFile( paramVal );
	}
	@Override
	public 	BaseAgendaVO getDetailOfSuppFile(String paramVal) throws Exception { 
		return (BaseAgendaVO)baseCmmnDAO.getDetailOfSuppFile( paramVal );
	}
	@Override
	public 	String getInfoOfVisitUUID(String paramVal) throws Exception { 
		return (String)baseCmmnDAO.getInfoOfVisitUUID( paramVal );
	}
	@Override
	public 	int getCountOfVisit(BaseCommonVO paramObj) throws Exception { 
		return (int)baseCmmnDAO.getCountOfVisit( paramObj );
	}
	@Override
	public 	List<BaseVisitsVO> getListOfVisit(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseVisitsVO>)baseCmmnDAO.getListOfVisit( paramObj );
	}
	@Override
	public 	List<BaseVisitsVO> getListOfVisitSheet(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseVisitsVO>)baseCmmnDAO.getListOfVisitSheet( paramObj );
	}
	@Override
	public 	List<BaseVisitsVO> getListOfVisitSheetYear(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseVisitsVO>)baseCmmnDAO.getListOfVisitSheetYear( paramObj );
	}
	@Override
	public 	List<BaseVisitsVO> getListOfVisitSheetMonth(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseVisitsVO>)baseCmmnDAO.getListOfVisitSheetMonth( paramObj );
	}
	@Override
	public 	List<BaseVisitsVO> getListOfVisitSheetDate(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseVisitsVO>)baseCmmnDAO.getListOfVisitSheetDate( paramObj );
	}
	@Override
	public 	BaseVisitsVO getDetailOfVisit(BaseCommonVO paramObj) throws Exception { 
		return (BaseVisitsVO)baseCmmnDAO.getDetailOfVisit( paramObj );
	}
	@Override
	public 	BaseVisitsVO getDetailOfVisit(String paramVal) throws Exception { 
		return (BaseVisitsVO)baseCmmnDAO.getDetailOfVisit( paramVal );
	}
	@Override
	public 	BaseVisitsVO getInfoOfVisitSeq(BaseVisitsVO paramObj) throws Exception { 
		return (BaseVisitsVO)baseCmmnDAO.getInfoOfVisitSeq( paramObj );
	}
	@Override
	public 	BaseVisitsVO getInfoOfVisitYear(BaseVisitsVO paramObj) throws Exception { 
		return (BaseVisitsVO)baseCmmnDAO.getInfoOfVisitYear( paramObj );
	}
	@Override
	public 	boolean doRtnOfCreateVisit(BaseVisitsVO paramObj) throws Exception { 
		return (boolean)baseCmmnDAO.doRtnOfCreateVisit( paramObj );
	}
	@Override
	public 	boolean doRtnOfUpdateVisit(BaseVisitsVO paramObj) throws Exception { 
		return (boolean)baseCmmnDAO.doRtnOfUpdateVisit( paramObj );
	}
	@Override
	public 	boolean doRtnOfDeleteVisit(BaseVisitsVO paramObj) throws Exception { 
		return (boolean)baseCmmnDAO.doRtnOfDeleteVisit( paramObj );
	}
	@Override
	public 	boolean doRtnOfModifyVisit(BaseVisitsVO paramObj) throws Exception { 
		return (boolean)baseCmmnDAO.doRtnOfModifyVisit( paramObj );
	}
	@Override
	public 	BaseVisitsVO getStatOfVisit(BaseCommonVO paramObj) throws Exception { 
		return (BaseVisitsVO)baseCmmnDAO.getStatOfVisit( paramObj );
	}
	@Override
	public 	int getCountOfVisitDay(String paramVal) throws Exception { 
		return (int)baseCmmnDAO.getCountOfVisitDay( paramVal );
	}

}	 // end of class