/*******************************************************************************
  Program ID  : BaseCmmnDAO
  Description : 공통 정보에 대한 Data Access Object Class를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.06.04
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.06.04  최초 생성
  v1.0    HexaMedia new0man      2019.06.04  평택시의회 Site Renewal
  v1.1    HexaMedia new0man      2020.05.08  대수별 기간 최소최대 선택일지정
 *******************************************************************************/
package egovframework.ptcc.cmmn.service.impl;


import java.sql.*;
import java.util.*;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import egovframework.ptcc.cmmn.vo.*;

@Repository("baseCmmnDAO")
public class BaseCmmnDAO extends egovframework.rte.psl.dataaccess.EgovAbstractDAO { 

	/**
	 * <pre>
	 * 시스템 일자을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @return BaseCommonVO
	 * @throws Exception
	 */
	public BaseCommonVO getSystemOfDate() throws Exception { 
		return (BaseCommonVO)select("ptcouncil.new.system.ptSelectDate");
	}	// end of getSystemOfDate

	/**
	 * <pre>
	 * 접속지 존재 여부을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return Integer
	 * @throws Exception
	 */
	public int getCountOfNetConn(String paramVal) throws Exception { 
		return (int)select("ptcouncil.new.system.ptSelectConn",paramVal);
	}	// end of getCountOfNetConn


	/**
	 * <pre>
	 * 회의년도 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseCommonVO> getListOfYear(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)list("ptcouncil.new.common.ptSelectYear",paramVal);
	}	// end of getListOfYear

	/**
	 * <pre>
	 * 전체 회의대수 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseCommonVO> getListOfClassALL(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)list("ptcouncil.new.common.ptSelectClassALL",paramVal);
	}	// end of getListOfClassALL

	/**
	 * <pre>
	 * 현재 회의대수 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return BaseCommonVO
	 * @throws Exception
	 */
	public BaseCommonVO getInfoOfClassNOW(String paramVal) throws Exception { 
		return (BaseCommonVO)select("ptcouncil.new.common.ptSelectClassNOW",paramVal);
	}	// end of getInfoOfClassNOW

	/**
	 * <pre>
	 * 이전 회의대수 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return BaseCommonVO
	 * @throws Exception
	 */
	public BaseCommonVO getInfoOfClassAGO(String paramVal) throws Exception { 
		return (BaseCommonVO)select("ptcouncil.new.common.ptSelectClassAGO",paramVal);
	}	// end of getInfoOfClassAGO

	/**
	 * <pre>
	 * 전체 회의회수 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseCommonVO> getListOfRoundALL(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)list("ptcouncil.new.common.ptSelectRoundALL",paramVal);
	}	// end of getListOfRoundALL

	/**
	 * <pre>
	 * 현재 회의회수 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return BaseCommonVO
	 * @throws Exception
	 */
	public BaseCommonVO getInfoOfRoundNOW(String paramVal) throws Exception { 
		return (BaseCommonVO)select("ptcouncil.new.common.ptSelectRoundNOW",paramVal);
	}	// end of getInfoOfRoundNOW

	/**
	 * <pre>
	 * 회의분류 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseCommonVO> getListOfGroup(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)list("ptcouncil.new.common.ptSelectGroup",paramVal);
	}	// end of getListOfGroup

	/**
	 * <pre>
	 * 의안종류 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseCommonVO> getListOfSugKind(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)list("ptcouncil.new.common.ptSelectSugKind",paramVal);
	}	// end of getListOfSugKind

	/**
	 * <pre>
	 * 의안결과 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseCommonVO> getListOfResKind(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)list("ptcouncil.new.common.ptSelectResKind",paramVal);
	}	// end of getListOfResKind

	/**
	 * <pre>
	 * 선거구분류 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseCommonVO> getListOfParty(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)list("ptcouncil.new.common.ptSelectParty",paramVal);
	}	// end of getListOfParty

	/**
	 * <pre>
	 * 소관위원회 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseCommonVO> getListOfComKind(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)list("ptcouncil.new.common.ptSelectComKind",paramVal);
	}	// end of getListOfComKind

	/**
	 * <pre>
	 * 상임위원회 분류 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseCommonVO> getListOfStand(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)list("ptcouncil.new.common.ptSelectStand",paramVal);
	}	// end of getListOfStand

	/**
	 * <pre>
	 * 특별위원회 분류 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseCommonVO> getListOfSpecial(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)list("ptcouncil.new.common.ptSelectSpecial",paramVal);
	}	// end of getListOfSpecial

	/**
	 * <pre>
	 * 감사위원회 분류 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseCommonVO> getListOfAudit(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)list("ptcouncil.new.common.ptSelectAudit",paramVal);
	}	// end of getListOfAudit

	/**
	 * <pre>
	 * 예결위원회 분류 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	public List<BaseCommonVO> getListOfBudget(String paramVal) throws Exception { 
//		return (List<BaseCommonVO>)list("ptcouncil.new.common.ptSelectBudget",paramVal);
		return null;
	}	// end of getListOfBudget

	/**
	 * <pre>
	 * 전체위원 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseCommonVO> getListOfMembALL(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)list("ptcouncil.new.common.ptSelectMembALL",paramVal);
	}	// end of getListOfMembALL

	/**
	 * <pre>
	 * 현재의원 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseCommonVO> getListOfMembNOW(String paramVal) throws Exception { 
		return (List<BaseCommonVO>)list("ptcouncil.new.common.ptSelectMembNOW",paramVal);
	}	// end of getListOfMembNOW


	/**
	 * <pre>
	 * 분류구분 내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseCommonVO
	 * @throws Exception
	 */
	public BaseCommonVO getInfoOfCate(BaseCommonVO paramObj) throws Exception { 
//		return (BaseCommonVO)select("ptcouncil.new.common.ptSelectCate",paramObj);
		return null;
	}	// end of getInfoOfCate

	/**
	 * <pre>
	 * 분류구분 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	public List<BaseCommonVO> getListOfCateAll(BaseCommonVO paramObj) throws Exception { 
//		return (List<BaseCommonVO>)list("ptcouncil.new.common.ptSelectCateListAll",paramObj);
		return null;
	}	// end of getListOfCateAll

	/**
	 * <pre>
	 * 분류구분(대) 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	public List<BaseCommonVO> getListOfCateLrg(BaseCommonVO paramObj) throws Exception { 
//		return (List<BaseCommonVO>)list("ptcouncil.new.common.ptSelectCateListLrg",paramObj);
		return null;
	}	// end of getListOfCateLrg

	/**
	 * <pre>
	 * 분류구분(중) 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	public List<BaseCommonVO> getListOfCateMid(BaseCommonVO paramObj) throws Exception { 
//		return (List<BaseCommonVO>)list("ptcouncil.new.common.ptSelectCateListMid",paramObj);
		return null;
	}	// end of getListOfCateMid

	/**
	 * <pre>
	 * 권한구분 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseCommonVO> getListOfAuth(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseCommonVO>)list("ptcouncil.new.common.ptSelectAuth",paramObj);
	}	// end of getListOfAuth

	/**
	 * <pre>
	 * 회의명칭을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return String
	 * @throws Exception
	 */
	String getInfoOfConfName(String paramVal) throws Exception { 
		return (String)select("ptcouncil.new.common.ptSelectConfName",paramVal);
	}	// end of getInfoOfConfName

	/**
	 * <pre>
	 * 회의대수별 회의기간을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return BaseCommonVO
	 * @throws Exception
	 */
	BaseCommonVO getInfoOfConfDate(String paramVal) throws Exception { 
		return (BaseCommonVO)select("ptcouncil.new.common.ptSelectConfDate",paramVal);
	}	// end of getInfoOfConfDate


	/**
	 * <pre>
	 * 통합파일 UUID을(를) 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return String
	 * @throws Exception
	 */
	String getInfoOfDataFileUUID (String paramVal) throws Exception { 
		return (String)select("ptcouncil.new.common.ptSelectFilesUUID",paramVal);
	}	// end of getInfoOfDataFileUUID

	/**
	 * <pre>
	 * 통합파일 목록건수을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return Integer
	 * @throws Exception
	 */
	public int getCountOfDataFile(BaseCommonVO paramObj) throws Exception { 
		return (int)select("ptcouncil.new.common.ptSelectFilesCount",paramObj);
	}	// end of getCountOfDataFile

	/**
	 * <pre>
	 * 통합파일 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseFilesVO> getListOfDataFile(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseFilesVO>)list("ptcouncil.new.common.ptSelectFilesList",paramObj);
	}	// end of getListOfDataFile

	/**
	 * <pre>
	 * 통합파일(썸네일) 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<BaseFilesVO> getListOfThmbFile(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseFilesVO>)list("ptcouncil.new.common.ptSelectFilesThmb",paramObj);
	}	// end of getListOfThmbFile

	/**
	 * <pre>
	 * 통합파일 상세내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseCommonVO
	 * @throws Exception
	 */
	public BaseCommonVO getDetailOfDataFile(BaseCommonVO paramObj) throws Exception { 
		return (BaseCommonVO)select("ptcouncil.new.common.ptSelectFilesDetail",paramObj);
	}	// end of getDetailOfDataFile

	/**
	 * <pre>
	 * 통합파일 상세내역(UUID)을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return BaseFilesVO
	 * @throws Exception
	 */
	public BaseFilesVO getDetailOfDataFile(String paramVal) throws Exception { 
		return (BaseFilesVO)select("ptcouncil.new.common.ptSelectFilesDetailUUID",paramVal);
	}	// end of getDetailOfDataFile

	/**
	 * <pre>
	 * 통합파일 일련번호을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseFilesVO
	 * @throws Exception
	 */
	public BaseFilesVO getInfoOfDataFileSeq(BaseCommonVO paramObj) throws Exception { 
		return (BaseFilesVO)select("ptcouncil.new.common.ptSelectFilesSeq",paramObj);
	}	// end of getInfoOfDataFileSeq

	/**
	 * <pre>
	 * 통합파일 내역을 등록 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfCreateDataFile(BaseCommonVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.common.ptInsertFiles",paramObj)<=0);
	}	// end of doActOfCreateDataFile

	/**
	 * <pre>
	 * 통합파일 내역을 변경 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfUpdateDataFile(BaseCommonVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.common.ptUpdateFiles",paramObj)<=0);
	}	// end of doActOfUpdateDataFile

	/**
	 * <pre>
	 * 통합파일 내역을 삭제 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfDeleteDataFile(BaseCommonVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.common.ptDeleteFiles",paramObj)<=0);
	}	// end of doActOfDeleteDataFile

	/**
	 * <pre>
	 * 통합파일 내역을 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public boolean doRtnOfModifyDataFile(BaseCommonVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.common.ptModifyFiles",paramObj)<=0);
	}	// end of doActOfModifyDataFile

	/**
	 * <pre>
	 * 본문파일 상세내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseFilesVO
	 * @throws Exception
	 */
	public BaseFilesVO getDetailOfBodyFile(String paramVal) throws Exception { 
		return (BaseFilesVO)select("ptcouncil.new.common.ptSelectBodysDetail",paramVal);
	}	// end of getDetailOfBodyFile

	/**
	 * <pre>
	 * 부록파일 상세내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseAgendaVO
	 * @throws Exception
	 */
	public BaseAgendaVO getDetailOfSuppFile(String paramVal) throws Exception { 
		return (BaseAgendaVO)select("ptcouncil.new.common.ptSelectSuppsDetail",paramVal);
	}	// end of getDetailOfSuppFile


	/**
	 * <pre>
	 * 방문자 UUID을(를) 전환해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return String
	 * @throws Exception
	 */
	public 	String getInfoOfVisitUUID(String paramVal) throws Exception { 
		return (String)select("ptcouncil.new.common.ptSelectVisitUUID",paramVal);
	}	// end of getInfoOfVisitUUID

	/**
	 * <pre>
	 * 방문자 목록건수을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return Integer
	 * @throws Exception
	 */
	public 	int getCountOfVisit(BaseCommonVO paramObj) throws Exception { 
		return (int)select("ptcouncil.new.common.ptSelectVisitCount",paramObj);
	}	// end of getCountOfVisit

	/**
	 * <pre>
	 * 방문자 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public 	List<BaseVisitsVO> getListOfVisit(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseVisitsVO>)list("ptcouncil.new.common.ptSelectVisitList",paramObj);
	}	// end of getListOfVisit

	/**
	 * <pre>
	 * 방문자 집계 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public 	List<BaseVisitsVO> getListOfVisitSheet(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseVisitsVO>)list("ptcouncil.new.common.ptSelectVisitSheet",paramObj);
	}	// end of getListOfVisitSheet

	/**
	 * <pre>
	 * 방문자 년별 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public 	List<BaseVisitsVO> getListOfVisitSheetYear(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseVisitsVO>)list("ptcouncil.new.common.ptSelectVisitSheetYear",paramObj);
	}	// end of getListOfVisitSheetYear

	/**
	 * <pre>
	 * 방문자 월별 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public 	List<BaseVisitsVO> getListOfVisitSheetMonth(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseVisitsVO>)list("ptcouncil.new.common.ptSelectVisitSheetMonth",paramObj);
	}	// end of getListOfVisitSheetMonth

	/**
	 * <pre>
	 * 방문자 일별 목록내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public 	List<BaseVisitsVO> getListOfVisitSheetDate(BaseCommonVO paramObj) throws Exception { 
		return (List<BaseVisitsVO>)list("ptcouncil.new.common.ptSelectVisitSheetDate",paramObj);
	}	// end of getListOfVisitSheetDate

	/**
	 * <pre>
	 * 방문자 상세내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseVisitsVO
	 * @throws Exception
	 */
	public 	BaseVisitsVO getDetailOfVisit(BaseCommonVO paramObj) throws Exception { 
		return (BaseVisitsVO)select("ptcouncil.new.common.ptSelectVisitDetail",paramObj);
	}	// end of getDetailOfVisit

	/**
	 * <pre>
	 * 방문자 상세내역(UUID)을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return BaseVisitsVO
	 * @throws Exception
	 */
	public 	BaseVisitsVO getDetailOfVisit(String paramVal) throws Exception { 
		return (BaseVisitsVO)select("ptcouncil.new.common.ptSelectVisitDetailUUID",paramVal);
	}	// end of getDetailOfVisit

	/**
	 * <pre>
	 * 방문자 일련번호을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseVisitsVO
	 * @throws Exception
	 */
	public 	BaseVisitsVO getInfoOfVisitSeq(BaseVisitsVO paramObj) throws Exception { 
		return (BaseVisitsVO)select("ptcouncil.new.common.ptSelectVisitSeq",paramObj);
	}	// end of getInfoOfVisitSeq

	/**
	 * <pre>
	 * 방문자 조회년도을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseVisitsVO
	 * @throws Exception
	 */
	public 	BaseVisitsVO getInfoOfVisitYear(BaseVisitsVO paramObj) throws Exception { 
		return (BaseVisitsVO)select("ptcouncil.new.common.ptSelectVisitYear",paramObj);
	}	// end of getInfoOfVisitYear

	/**
	 * <pre>
	 * 방문자 내역을(를) 등록 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseVisitsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public 	boolean doRtnOfCreateVisit(BaseVisitsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.common.ptInsertVisit",paramObj)<=0);
	}	// end of doActOfCreateVisit

	/**
	 * <pre>
	 * 방문자 내역을(를) 변경 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseVisitsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public 	boolean doRtnOfUpdateVisit(BaseVisitsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.common.ptUpdateVisit",paramObj)<=0);
	}	// end of doActOfUpdateVisit

	/**
	 * <pre>
	 * 통합파일 내역을(를) 삭제 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseVisitsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public 	boolean doRtnOfDeleteVisit(BaseVisitsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.common.ptDeleteVisit",paramObj)<=0);
	}	// end of doActOfDeleteVisit

	/**
	 * <pre>
	 * 통합파일 내역을(를) 갱신 처리해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseVisitsVO						// 
	 * @return Boolean
	 * @throws Exception
	 */
	public 	boolean doRtnOfModifyVisit(BaseVisitsVO paramObj) throws Exception { 
		return !((int)update("ptcouncil.new.common.ptModifyVisit",paramObj)<=0);
	}	// end of doActOfModifyVisit

	/**
	 * <pre>
	 * 방문자 통계내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  BaseCommonVO						// 
	 * @return BaseVisitsVO
	 * @throws Exception
	 */
	public 	BaseVisitsVO getStatOfVisit(BaseCommonVO paramObj) throws Exception { 
		return (BaseVisitsVO)select("ptcouncil.new.common.ptSelectVisitStats");
	}	// end of getStatOfVisit

	/**
	 * <pre>
	 * 당일방문자 내역건수을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return Integer
	 * @throws Exception
	 */
	public 	int getCountOfVisitDay(String paramVal) throws Exception { 
		return (int)select("ptcouncil.new.common.ptSelectVisitDays",paramVal);
	}	// end of getCountOfVisit


	/**
	 * <pre>
	 * 배치작업 트랜잭션 일괄 처리시 사용
	 * </pre>
	 * 
	 * @param  String							// 일괄 처리할 Query ID
	 * @param  List								// 일괄 처리할 List
	 * @param  Object							// 테이블 초기화 시 넘겨줄 Data
	 * @param  String							// 테이블 초기화 시 넘겨줄 Query ID
	 * @return void
	 * @throws DataAccessException
	 * @throws SQLException
	 */
	@SuppressWarnings({"rawtypes","deprecation"})
	public void doActOfBatchLoad (String paramArg0, List paramList, Object paramObj, String paramArg1) throws DataAccessException, SQLException { 
		try { 
			this.getSqlMapClient().startTransaction();
			this.getSqlMapClient().startBatch();

			Iterator iterator = paramList.iterator();
			while( iterator.hasNext() ) { 
				this.getSqlMapClient().insert( paramArg0, (Map)iterator.next() );
			}

			this.getSqlMapClient().executeBatch();
			this.getSqlMapClient().commitTransaction();
		} catch( SQLException SE ) { 
			SE.printStackTrace();
			// delete table
			this.getSqlMapClientTemplate().delete( paramArg1,paramObj);
		} finally { 
			this.getSqlMapClient().endTransaction();
		}
	}

}	// end of Class