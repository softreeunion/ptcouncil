package egovframework.ptcc.cmmn.service.impl;

import java.util.*;
import org.springframework.web.context.request.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.vo.BaseMemberVO;

public class UserDetailsSessionServiceImpl extends egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl implements UserDetailsService { 

	@Override
	public Object getAuthenticatedUser() { 
		if(RequestContextHolder.getRequestAttributes()==null){return null;}else{return (BaseMemberVO)RequestContextHolder.getRequestAttributes().getAttribute("MemberVO",RequestAttributes.SCOPE_SESSION);}
	}

	@Override
	public List<String> getAuthorities() {  
		return new ArrayList<String>();
	}

	@Override
	public Boolean isAuthenticated() { 
		// 인증된 유저인지 확인한다.
		if(RequestContextHolder.getRequestAttributes()==null){return Boolean.FALSE;}
		if((BaseMemberVO)RequestContextHolder.getRequestAttributes().getAttribute("MemberVO",RequestAttributes.SCOPE_SESSION)==null){return Boolean.FALSE;}else{return Boolean.TRUE;}
	}

}