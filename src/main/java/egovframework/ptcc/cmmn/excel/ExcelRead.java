package egovframework.ptcc.cmmn.excel;

import java.util.*;
import javax.annotation.*;

import org.apache.poi.ss.usermodel.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.web.context.WebApplicationContext;

import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;

public class ExcelRead { 

	@Autowired
	private WebApplicationContext			webContext;

	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;

	protected static Logger				log          = LoggerFactory.getLogger(ExcelRead.class);
	protected static String				dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected static String				firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected static String				pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");


	public static List<Map<String, String>> read(ExcelReadOption excelReadOption) { 

		Workbook				work         = ExcelReadType.getWorkbook( excelReadOption.getFileName() );
		Sheet					sheet        = null;
		int						numOfSheet   = work.getNumberOfSheets();
		int						numOfRows    = 0;
		int						numOfCols    = 0;
		int						bgnRow       = 0;
		Row						row          = null;
		Cell					cell         = null;
		String					cellName     = "";
		Map<String,String>		map          = null;
		List<Map<String,String>>result       = new ArrayList<Map<String,String>>();

log.debug("Sheet Count: "+ numOfSheet);
		for( int sheetIndex = 0; sheetIndex < 1; sheetIndex++ ) { 
log.debug("Sheet Name: "+ work.getSheetName(sheetIndex));
			sheet = work.getSheetAt(sheetIndex);

			// 양식은 세번째이나 다운로드된 내역은 다름
			for( int c = excelReadOption.getBgnRow(); c < excelReadOption.getBgnRow() + 4; c++ ) { 
				row = sheet.getRow(c);
				if( row != null ) bgnRow = c;
				if( bgnRow > 0 ) break;
			}
			numOfRows = sheet.getLastRowNum();
log.debug("Last Rows: "+ numOfRows);

			for( int rowIndex = bgnRow; rowIndex <= numOfRows; rowIndex++ ) { 
				row = sheet.getRow(rowIndex);
				if( row != null ) { 
					numOfCols = row.getLastCellNum();
					map = new HashMap<String, String>();
					for( int cellIndex = 0; cellIndex < numOfCols; cellIndex++ ) { 
						cell = row.getCell(cellIndex);
						cellName = ExcelReadCell.getName( cell, cellIndex );
						if( !excelReadOption.getOutputColumns().contains(cellName) ) continue;
						map.put(cellName, ExcelReadCell.getValue( cell ));
					}
					result.add(map);
				}
			}
		}

		return result;
	}	// end of read

}	// end of class