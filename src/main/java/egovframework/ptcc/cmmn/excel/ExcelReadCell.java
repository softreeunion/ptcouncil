package egovframework.ptcc.cmmn.excel;

import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;

public class ExcelReadCell { 

	/**
	 * <pre>
	 * 셀에 해당하는 명칭(Column Name)을 가져온다.
	 * 만약 셀이 널이라면 cellIndex의 값으로 명칭(Column Name)을 가져온다.
	 * </pre>
	 * 
	 * @param  Cell								// 
	 * @param  Integer							// 
	 * @return String
	 */
	public static String getName (Cell cell, int cellIndex) { 
		return CellReference.convertNumToColString((int)(cell == null ? cellIndex : cell.getColumnIndex()));
	}

	@SuppressWarnings("deprecation")
	public static String getValue(Cell cell) { 
		String					cellVal      = "";
		if( cell != null ) { 
			switch( cell.getCellType() ) { 
				case Cell.CELL_TYPE_FORMULA: cellVal = cell.getCellFormula();          break;
				case Cell.CELL_TYPE_NUMERIC: cellVal = cell.getNumericCellValue() +""; break;
				case Cell.CELL_TYPE_STRING:  cellVal = cell.getStringCellValue() +"";  break;
				case Cell.CELL_TYPE_BOOLEAN: cellVal = cell.getBooleanCellValue() +""; break;
				case Cell.CELL_TYPE_ERROR:   cellVal = cell.getErrorCellValue() +"";   break;
				case Cell.CELL_TYPE_BLANK:   cellVal = "";                             break;
				default:                     cellVal = cell.getStringCellValue() +"";  break;
			}
		}

		return cellVal;
	}

}	// end of ExcelCellRef