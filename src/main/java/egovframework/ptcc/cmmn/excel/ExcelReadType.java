package egovframework.ptcc.cmmn.excel;

import java.io.*;
import javax.annotation.*;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.web.context.WebApplicationContext;

import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;

public class ExcelReadType { 

	@Autowired
	private WebApplicationContext			webContext;

	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;

	protected static Logger				log          = LoggerFactory.getLogger(ExcelReadType.class);
	protected static String				dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected static String				firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected static String				pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");


	/**
	 * <pre>
	 * Excel File을 Workbook 형태로 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  String							// 
	 * @return Workbook
	 * @throws Exception
	 */
	public static Workbook getWorkbook (String fileName) { 
		FileInputStream			fileIS       = null;
		Workbook				workbook     = null;

		try { 
			fileIS = new FileInputStream(fileName);
		} catch( FileNotFoundException FNFE ) { 
			log.debug(FNFE.getMessage());
		}
		if( fileName.toLowerCase().endsWith(".xls") ) { 
			try { 
				workbook = new HSSFWorkbook(fileIS);
			} catch( IOException IOE ) { 
				log.debug(IOE.getMessage());
			}
		}
		if( fileName.toLowerCase().endsWith(".xlsx") ) { 
			try { 
				workbook = new XSSFWorkbook(fileIS);
			} catch( IOException IOE ) { 
				log.debug(IOE.getMessage());
			}
		}

		return workbook;
	}	// end of getWorkbook
 
}	// end of class