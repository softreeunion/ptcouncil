package egovframework.ptcc.cmmn.excel;

import java.net.*;
import java.util.*;

import javax.servlet.http.*;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.*;
import org.slf4j.*;

import egovframework.com.cmm.util.*;

public class ExcelBuilderPOI extends org.springframework.web.servlet.view.document.AbstractExcelView { 

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());

	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument ( 
			Map<String, Object> modelMap, 
			HSSFWorkbook workBook, 
			HttpServletRequest request, 
			HttpServletResponse response 
		) throws Exception { 

		String				relFile     = (String)modelMap.get("relFile");
		String				absFile     = (String)modelMap.get("absFile");
//		HSSFWorkbook		workBook    = null;
		HSSFSheet			sheet       = null;
		HSSFRow				row         = null;

		int					titLine     = Integer.parseInt(BaseUtility.null2Char( (String)modelMap.get("titLine"), "1" ));
		List<String>		colName     = (List<String>)modelMap.get("colName");
		List<String>		colRegion   = (List<String>)modelMap.get("colRegion");
		List<String>		colAlign    = (List<String>)modelMap.get("colAlign");
		List<Integer>		colWidth    = (List<Integer>)modelMap.get("colWidth");
		List<String[]>		colValue    = (List<String[]>)modelMap.get("colValue");

		int					headRow     = 2 + titLine;
		int					headAdd     = 0;
		int					bgnCols     = 0;
		int					endCols     = 0;

		if( colValue == null || colValue.size() <= 0 )	return;
log.debug("relFile: "+ relFile +" |absFile: "+ absFile +" |colName: "+ colName.size());

		try { 
			// create a new Excel Workbook
			workBook = new HSSFWorkbook();
			if( workBook != null ) { 
				// create a new Excel sheet
				sheet = workBook.createSheet(absFile);
				sheet.setDefaultColumnWidth(12);

				if( sheet != null ) { 
					// create Blank row
					row = sheet.createRow(headRow - 4);

					// create Subject row
					row   = sheet.createRow(headRow - 3);
					row.createCell(0).setCellValue(absFile);
//					row.getCell(0).setCellStyle(styleToSubject( style, font, "center" ));
					row.getCell(0).setCellStyle(styleToSubject( workBook, "center" ));
//					sheet.addMergedRegion(new Region((int)(headRow-3), (short)0, (int)(headRow-3), (short)(colName.size()-1)));
					sheet.addMergedRegion(new CellRangeAddress(headRow-3, headRow-3, 0, colName.size()-1));

					// create Blank row
					row   = sheet.createRow(headRow - 2);

					// create Print Date row
					row   = sheet.createRow(headRow - 1);
					row.setHeight((short)0x150);
					row.createCell(colName.size() - 4).setCellValue("출력일자 : "+ BaseUtility.getDateOfNow( "yyyy.MM.dd hh:mm" ));
					row.getCell(colName.size() - 4).setCellStyle(styleToNormal( workBook, "right" ));
//					sheet.addMergedRegion(new Region((int)(headRow-1), (short)(colName.size()-4), (int)(headRow-1, (short)(colName.size()-1)));
					sheet.addMergedRegion(new CellRangeAddress(headRow-1, headRow-1, colName.size()-4, colName.size()-1));

					// create Header rows
					// 병합처리
					//   1. Old Version : addMergedRegion(new Region((int)시작행, (short)시작열, (int)종료행, (short)종료열)) 
					//   2. New Version : addMergedRegion(new CellRangeAddress((int)시작행, (int)종료행, (int)시작열, (int)종료열)) 
					if( !(colName == null || colName.size() <= 0) ) { 
						row   = sheet.createRow(headRow);
						if( !(colRegion == null || colRegion.size() <= 0) ) { 
							for( int c = 0; c < colRegion.size(); c++ ) { 
								headAdd = headRow + Integer.parseInt(colRegion.get(c).split("[`]", 3)[0]);	// 행증가
								bgnCols = Integer.parseInt(colRegion.get(c).split("[`]", 3)[1]);			// 시작열
								endCols = Integer.parseInt(colRegion.get(c).split("[`]", 3)[2]);			// 종료열
								if( bgnCols != endCols ) 
//									sheet.addMergedRegion(new Region((int)headRow, (short)bgnCols, (int)headAdd, (short)endCols));
									sheet.addMergedRegion(new CellRangeAddress(headRow, headAdd, bgnCols, endCols));
							}
						}
						for( int c = 0; c < colName.size(); c++ ) { 
							sheet.setColumnWidth(c, (short)(22.2 * (colWidth == null || colWidth.size() <= 0 ? 100 : (int)colWidth.get(c))));
							row.createCell(c).setCellValue(colName.get(c));
							row.getCell(c).setCellStyle(styleToHeaderLN( workBook, "center" ));
						}
					}

					// create Content rows
					if( !(colValue == null || colValue.size() <= 0) ) { 
						for( int r = 0; r < colValue.size(); r++ ) { 
							row   = sheet.createRow(headRow + 1 + r);
							for( int c = 0; c < colName.size(); c++ ) { 
								row.createCell(c).setCellValue(colValue.get(r)[c]);
//								row.getCell(c).setCellStyle(styleToNormalLN( workBook, colAlign.get(c).toLowerCase() ));
							}
						}
					}
				}
			}

			response.setContentType("application/msexcel");
			response.setHeader("Set-Cookie", "fileDownload=true; path=/");
			response.setHeader("Content-Disposition", "attachment; filename=\""+ URLEncoder.encode(absFile, "UTF-8") +".xls\"");
			response.setHeader("Content-Transfer-Encoding", "binary");
			workBook.write(response.getOutputStream());
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			E.printStackTrace();
			response.setHeader("Set-Cookie", "fileDownload=false; path=/");
			response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
			response.setHeader("Content-type", "text/html;charset=utf-8");
		} finally { 
		}	// end of try~catch~finally
	}	// end of buildExcelDocument


	@SuppressWarnings("deprecation")
	private HSSFCellStyle styleToSubject ( 
			HSSFWorkbook workBook, 
			String align 
		) { 

		HSSFCellStyle		style       = workBook.createCellStyle();
		Font				font        = workBook.createFont();

		font.setFontName("맑은 고딕");
		font.setFontHeightInPoints((short)24);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		font.setColor(HSSFColor.BLACK.index);

		style.setFont(font);
		if( "".equalsIgnoreCase(align) ) 
			style.setAlignment(HSSFCellStyle.ALIGN_GENERAL);
		else if( "center".equalsIgnoreCase(align) ) 
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		else if( "left".equalsIgnoreCase(align) ) 
			style.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		else if( "right".equalsIgnoreCase(align) ) 
			style.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		else 
			style.setAlignment(HSSFCellStyle.ALIGN_GENERAL);
		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

		return style;
	}	// end of styleToSubject

	@SuppressWarnings("deprecation")
	private HSSFCellStyle styleToHeaderLN ( 
			HSSFWorkbook workBook, 
			String align 
		) { 

		HSSFCellStyle		style       = workBook.createCellStyle();
		Font				font        = workBook.createFont();

		font.setFontName("맑은 고딕");
		font.setFontHeightInPoints((short)11);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		font.setColor(HSSFColor.BLACK.index);

		style.setFont(font);
		if( "".equalsIgnoreCase(align) ) 
			style.setAlignment(HSSFCellStyle.ALIGN_GENERAL);
		else if( "center".equalsIgnoreCase(align) ) 
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		else if( "left".equalsIgnoreCase(align) ) 
			style.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		else if( "right".equalsIgnoreCase(align) ) 
			style.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		else 
			style.setAlignment(HSSFCellStyle.ALIGN_GENERAL);
		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		style.setFillForegroundColor(HSSFColor.LIGHT_TURQUOISE.index);
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

		return style;
	}	// end of styleToHeaderLN

	@SuppressWarnings("deprecation")
	private HSSFCellStyle styleToNormalLN ( 
			HSSFWorkbook workBook, 
			String align 
		) { 

		HSSFCellStyle		style       = workBook.createCellStyle();
		Font				font        = workBook.createFont();

		font.setFontName("돋움체");
		font.setFontHeightInPoints((short)10);
		font.setColor(HSSFColor.BLACK.index);

		style.setFont(font);
		if( "".equalsIgnoreCase(align) ) 
			style.setAlignment(HSSFCellStyle.ALIGN_GENERAL);
		else if( "center".equalsIgnoreCase(align) ) 
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		else if( "left".equalsIgnoreCase(align) ) 
			style.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		else if( "right".equalsIgnoreCase(align) ) 
			style.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		else 
			style.setAlignment(HSSFCellStyle.ALIGN_GENERAL);
		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);

		return style;
	}	// end of styleToNormalLN

	@SuppressWarnings("deprecation")
	private HSSFCellStyle styleToNormal ( 
			HSSFWorkbook workBook, 
			String align 
		) { 

		HSSFCellStyle		style       = workBook.createCellStyle();
		Font				font        = workBook.createFont();

		font.setFontName("돋움체");
		font.setFontHeightInPoints((short)10);
		font.setColor(HSSFColor.BLACK.index);

		style.setFont(font);
		if( "".equalsIgnoreCase(align) ) 
			style.setAlignment(HSSFCellStyle.ALIGN_GENERAL);
		else if( "center".equalsIgnoreCase(align) ) 
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		else if( "left".equalsIgnoreCase(align) ) 
			style.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		else if( "right".equalsIgnoreCase(align) ) 
			style.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		else 
			style.setAlignment(HSSFCellStyle.ALIGN_GENERAL);
		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

		return style;
	}	// end of styleToNormal

}	// end of class