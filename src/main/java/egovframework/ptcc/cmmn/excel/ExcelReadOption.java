package egovframework.ptcc.cmmn.excel;

import java.util.*;

public class ExcelReadOption { 

	private String							fileName;
	private List<String>					outputCols;
	private int							bgnRow;

	public String getFileName() { 
		return fileName;
	}
	public void setFileName(String fileName) { 
		this.fileName = fileName;
	}
	public List<String> getOutputColumns() { 
		List<String>			temp         = new ArrayList<String>();
		temp.addAll(outputCols);

		return temp;
	}
	public void setOutputColumns(List<String> outputCols) { 
		List<String>			temp         = new ArrayList<String>();
		temp.addAll(outputCols);

		this.outputCols = temp;
	}
	public void setOutputColumns(String ... outputCols) { 
		if(this.outputCols == null) 
			this.outputCols = new ArrayList<String>();
		for(String outputCol : outputCols) 
			this.outputCols.add(outputCol);
	}
	public int getBgnRow() { 
		return bgnRow;
	}
	public void setBgnRow(int bgnRow) { 
		this.bgnRow = bgnRow;
	}

}	// end of class