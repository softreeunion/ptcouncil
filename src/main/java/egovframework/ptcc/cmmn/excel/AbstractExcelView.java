package egovframework.ptcc.cmmn.excel;

import java.io.*;
import java.util.*;

import javax.servlet.http.*;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.core.io.*;
import org.springframework.core.io.support.*;
import org.springframework.web.servlet.support.*;

public abstract class AbstractExcelView extends org.springframework.web.servlet.view.AbstractView { 

	// The content type for an Excel response
	private static final String CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

	// The extension to look for existing templates
	private static final String EXTENSION = ".xlsx";
	private String url;

	// Default Constructor.
	public AbstractExcelView() { 
		setContentType(CONTENT_TYPE);
	}

	public void setUrl ( String url ) { 
		this.url = url;
	}

	@Override
	protected boolean generatesDownloadContent() { 
		return true;
	}

	// Renders the Excel view, given the specified model.
	@Override
	protected final void renderMergedOutputModel ( 
			Map<String, Object> model, 
			HttpServletRequest request, 
			HttpServletResponse response 
		) throws Exception { 

		Workbook workBook;
		ByteArrayOutputStream baos = createTemporaryOutputStream();
//		if( this.url != null ) { 
//			workbook = getTemplateSource(this.url, request);
//		} else { 
			workBook = new XSSFWorkbook();
			logger.debug("Created Excel Workbook from scratch");
//		}

		buildExcelDocument(model, workBook, request, response);

		// Set the content type.
//		response.setContentLength(getContentType());

		// Should we set the content length here?
//		response.setContentLength(workbook.getBytes().length);

		// Flush byte array to servlet output stream.
//		ServletOutputStream out = response.getOutputStream();
		workBook.write(baos);
		writeToResponse(response, baos);
//		out.flush();
	}

	protected Workbook getTemplateSource ( 
			String url, 
			HttpServletRequest request 
		) throws Exception { 

		LocalizedResourceHelper helper = new LocalizedResourceHelper();
		Locale userLocale = RequestContextUtils.getLocale(request);
		Resource inputFile = helper.findLocalizedResource(url, EXTENSION, userLocale);

		// Create the Excel document from the source.
		if( logger.isDebugEnabled() ) { 
			logger.debug("Loading Excel workbook from "+ inputFile);
		}
//		POIFSFileSystem fs = new POIFSFileSystem(InputFile.getInputStream());
		return new XSSFWorkbook(inputFile.getInputStream());
	}

	protected abstract void buildExcelDocument ( 
			Map<String, Object> model, 
			Workbook workBook, 
			HttpServletRequest request, 
			HttpServletResponse response 
		) throws Exception;

	protected Cell getCell ( 
			Sheet sheet, 
			int row, 
			int col  
		) { 

		Row sheetRow = sheet.getRow(row);
		if( sheetRow == null ) { 
			sheetRow = sheet.createRow(row);
		}
		Cell cell = sheetRow.getCell(col);
		if( cell == null ) { 
			cell = sheetRow.createCell(col);
		}

		return cell;
	}

	protected void setText ( 
			Cell cell, 
			String text 
		) { 

		cell.setCellType(Cell.CELL_TYPE_STRING);
		cell.setCellValue(text);
	}

}	// end of class