/*******************************************************************************
  Program ID  : BaseImagePaginationRenderer
  Description : Pagination Renderer UX 구성
  Author      : HexaMedia new0man
  Write Date  : 2019.06.04
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v2.0    HexaMedia new0man      2019.06.04  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.cmmn;

import javax.servlet.ServletContext;

public class BaseImagePaginationRenderer extends egovframework.rte.ptl.mvc.tags.ui.pagination.AbstractPaginationRenderer implements org.springframework.web.context.ServletContextAware { 

	private ServletContext					servletContext;

	public BaseImagePaginationRenderer() {}

	private void initVariables() { 
		firstPageLabel    = " <a href=\"javascript:void(0);\" onclick=\"{0}({1})\" class=\"image\"><img src=\""+ servletContext.getContextPath() +"/images/btns/btn_pagefirst.png\" alt=\"first\"/></a> ";
		previousPageLabel = " <a href=\"javascript:void(0);\" onclick=\"{0}({1})\" class=\"image\"><img src=\""+ servletContext.getContextPath() +"/images/btns/btn_pageprev.png\"  alt=\"prev\" /></a> ";
		currentPageLabel  = " <strong>{0}</strong> ";
		otherPageLabel    = " <a href=\"javascript:void(0);\" onclick=\"{0}({1})\">{2}</a> ";
		nextPageLabel     = " <a href=\"javascript:void(0);\" onclick=\"{0}({1})\" class=\"image\"><img src=\""+ servletContext.getContextPath() +"/images/btns/btn_pagenext.png\"  alt=\"next\" /></a> ";
		lastPageLabel     = " <a href=\"javascript:void(0);\" onclick=\"{0}({1})\" class=\"image\"><img src=\""+ servletContext.getContextPath() +"/images/btns/btn_pagelast.png\"  alt=\"last\" /></a> ";
	}

	@Override
	public void setServletContext(ServletContext servletContext) { 
		this.servletContext = servletContext;
		initVariables();
	}

}	// end of class