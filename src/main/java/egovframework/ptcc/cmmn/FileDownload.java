package egovframework.ptcc.cmmn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@Controller
public class FileDownload {

    protected Logger log          = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value="/cmmn/down/app.do")
    public void FileDownApp (
            HttpServletRequest request,
            HttpServletResponse response
    ) throws Exception {

        // 파일 업로드된 경로
        String root = request.getSession().getServletContext().getRealPath("/");
        String savePath = root + "down";

        // 서버에 실제 저장된 파일명
        String filename = "app.hwp" ;

        // 실제 내보낼 파일명
        String orgfilename = "신청서.hwp";

        InputStream in = null;
        OutputStream os = null;
        File file = null;
        boolean skip = false;
        String client = "";

        try{
            // 파일을 읽어 스트림에 담기
            try{
                file = new File(savePath, filename);
                in = new FileInputStream(file);
            }catch(FileNotFoundException fe){
                skip = true;
            }

            client = request.getHeader("User-Agent");

            // 파일 다운로드 헤더 지정
            response.reset() ;
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Description", "JSP Generated Data");

            if(!skip){

                if(client != null && client.indexOf("MSIE 5.5") > -1) {
                    response.setHeader("Content-Disposition", "filename=" + orgfilename);
                } else if (client.indexOf("MSIE") > -1) { // MS IE(6~10)
                    response.setHeader("Content-Disposition", "attachment; filename="+ java.net.URLEncoder.encode(orgfilename, "UTF-8").replaceAll("\\+", "\\ ") + ";");
                } else if (client.indexOf("Trident") > -1) { //MS IE 11
                    response.setHeader("Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode(orgfilename, "UTF-8").replaceAll("\\+", "\\ ") + ";");
                } else if (client.indexOf("Edge") > -1) { //MS IE Edge
                    response.setHeader("Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode(orgfilename, "UTF-8").replaceAll("\\+", "\\ ") + ";");
                } else { // 그외 mozilla, chrome, opera
                    orgfilename = new String(orgfilename.getBytes("utf-8"),"iso-8859-1");
                    response.setHeader("Content-Disposition", "attachment;filename=" + orgfilename);
                    //response.setHeader("Content-Type", "application/octet-stream; charset=utf-8");
                }

                response.setHeader ("Content-Length", ""+file.length() );

                os = response.getOutputStream();
                byte b[] = new byte[(int)file.length()];
                int leng = 0;

                while( (leng = in.read(b)) > 0 ){
                    os.write(b,0,leng);
                }

            }else{
                log.debug("파일을 찾을 수 없습니다");
            }

            in.close();
            os.close();

        }catch(Exception e){
            e.printStackTrace();
        }
    }	// end of FileDownLink
}
