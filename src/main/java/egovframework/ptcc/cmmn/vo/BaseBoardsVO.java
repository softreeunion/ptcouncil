/*******************************************************************************
  Program ID  : BaseBoardsVO
  Description : 게시판 정보에 대한 Value Object를 정의한다
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v1.0    (주)헥사미디어 개발자  2014.00.00  최초 생성
  v2.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.cmmn.vo;

@SuppressWarnings("serial")
public class BaseBoardsVO extends BaseCommonVO implements java.io.Serializable { 

	// 공통 정보
	private String creIPv4;					// 
	private String creIPv6;					// 
	private String creMac;						// 
	private String modIPv4;					// 
	private String modIPv6;					// 
	private String modMac;						// 
	private String delIPv4;					// 
	private String delIPv6;					// 
	private String delMac;						// 

	private String fileName;					// 
	private String fileSize;					// 
	private String fileRel;					// 
	private String fileAbs;					// 
	private String fileUri;					// 
	private String fileLinkS;					// 첨부파일 내용 (싱글 다운용)
	private String fileLinkM;					// 첨부파일 내용 (멀티 다운용)
	private String fileLinkD;					// 첨부파일 내용 (일반 표시용)
	private String fileViewImg;
	private String downUUID;
	private String downRel;
	private String downAbs;
	private String downExt;

	// 게시글 댓글 정보
	private String rlySize;					// 
	private String rlyPattern;					//
	private String rlyCrypt;					// 
	private String rlyDesc;					// 

	private String rlyDescBR;					// 
	private String rlyDescCR;					// 

	// 게시판 정보
	private String bbsCate;					// 
	private String bbsTitle;					// 
	private String bbsSiteUri;					// 
	private String bbsSiteType;				// 
	private String bbsSiteUse;					// 
	private String bbsFiles;					// 
	private String contSize;					// 
	private String contPattern;				// 
	private String contCrypt;					// 
	private String contDesc;					// 
	private String hitCount;					// 
	private String ansRef;						// 
	private String ansStep;					// 
	private String ansPos;						// 
	private String creOrder;					// 
	private String modOrder;					// 
	private String crePasswd;					// 
	private String creMobile;					// 
	private String creHide;					// 
	private String qstDate;					// 
	private String ansDate;					// 
	private String takeDate;					// 

	private String bbsTypeName;				// 
	private String bbsCateName;				// 
	private String bbsSiteTypeName;			// 
	private String creMobileName;				// 
	private String creHideName;				// 
	private String ansMode;					// 
	private String ansCount;					// 
	private String fileCount;					// 
	private String contDescBR;					// 
	private String contDescCR;					// 
	private String contLinkC;					// 
	private String contLinkS;					// 첨부파일 내용 (싱글 다운용)
	private String contLinkM;					// 첨부파일 내용 (멀티 다운용)
	private String contLinkD;					// 첨부파일 내용 (일반 표시용)

	private String seq;
	private String reLevel;
	private String re_level;
	private String reStep;
	private String re_step;
	private String title;
	private String content;
	private String pwd;
	private String readNum;
	private String name;
	private String mailAddr;
	private String homePage;
	private String ip;
	private String tag;
	private String imgInfo;
	private String relativeCnt;
	private String ssn;
	private String ssn2;
	private String listType;
	private String listTypeName;
	private String list_type;
	private String code1;
	private String code2;
	private String name2;
	private String regDate1;
	private String reg_date1;
	private String regDate2;
	private String reg_date2;
	private String regName2;
	private String content2;

	private String content2CR;
	private String content2BR;

	private String confCode;
	private String confName;
	private String regiDate;
	private String confCode1;
	private String confName1;
	private String regiDate1;
	private String regiName1;
	private String confCode2;
	private String confName2;
	private String regiDate2;
	private String regiName2;
	private String contents2;
	private String confUUID1;
	private String confUUID2;

	private String contents2CR;
	private String contents2BR;
	private String scDate;
	private String creNoti;					//


	/**
	 * @return ansCount를(을) 리턴합니다.
	 */
	public String getAnsCount() {
		return ansCount;
	}
	/**
	 * @return ansDate를(을) 리턴합니다.
	 */
	public String getAnsDate() {
		return ansDate;
	}
	/**
	 * @return ansMode를(을) 리턴합니다.
	 */
	public String getAnsMode() {
		return ansMode;
	}
	/**
	 * @return ansPos를(을) 리턴합니다.
	 */
	public String getAnsPos() {
		return ansPos;
	}
	/**
	 * @return ansRef를(을) 리턴합니다.
	 */
	public String getAnsRef() {
		return ansRef;
	}
	/**
	 * @return ansStep를(을) 리턴합니다.
	 */
	public String getAnsStep() {
		return ansStep;
	}
	/**
	 * @return bbsCate를(을) 리턴합니다.
	 */
	public String getBbsCate() {
		return bbsCate;
	}
	/**
	 * @return bbsCateName를(을) 리턴합니다.
	 */
	public String getBbsCateName() {
		return bbsCateName;
	}
	/**
	 * @return bbsFiles를(을) 리턴합니다.
	 */
	public String getBbsFiles() {
		return bbsFiles;
	}
	/**
	 * @return bbsSiteType를(을) 리턴합니다.
	 */
	public String getBbsSiteType() {
		return bbsSiteType;
	}
	/**
	 * @return bbsSiteTypeName를(을) 리턴합니다.
	 */
	public String getBbsSiteTypeName() {
		return bbsSiteTypeName;
	}
	/**
	 * @return bbsSiteUri를(을) 리턴합니다.
	 */
	public String getBbsSiteUri() {
		return bbsSiteUri;
	}
	/**
	 * @return bbsSiteUse를(을) 리턴합니다.
	 */
	public String getBbsSiteUse() {
		return bbsSiteUse;
	}
	/**
	 * @return bbsTitle를(을) 리턴합니다.
	 */
	public String getBbsTitle() {
		return bbsTitle;
	}
	/**
	 * @return bbsTypeName를(을) 리턴합니다.
	 */
	public String getBbsTypeName() {
		return bbsTypeName;
	}
	/**
	 * @return contCrypt를(을) 리턴합니다.
	 */
	public String getContCrypt() {
		return contCrypt;
	}
	/**
	 * @return contDesc를(을) 리턴합니다.
	 */
	public String getContDesc() {
		return contDesc;
	}
	/**
	 * @return contDescBR를(을) 리턴합니다.
	 */
	public String getContDescBR() {
		return contDescBR;
	}
	/**
	 * @return contDescCR를(을) 리턴합니다.
	 */
	public String getContDescCR() {
		return contDescCR;
	}
	/**
	 * @return contLinkC를(을) 리턴합니다.
	 */
	public String getContLinkC() {
		return contLinkC;
	}
	/**
	 * @return contLinkD를(을) 리턴합니다.
	 */
	public String getContLinkD() {
		return contLinkD;
	}
	/**
	 * @return contLinkM를(을) 리턴합니다.
	 */
	public String getContLinkM() {
		return contLinkM;
	}
	/**
	 * @return contLinkS를(을) 리턴합니다.
	 */
	public String getContLinkS() {
		return contLinkS;
	}
	/**
	 * @return contPattern를(을) 리턴합니다.
	 */
	public String getContPattern() {
		return contPattern;
	}
	/**
	 * @return contSize를(을) 리턴합니다.
	 */
	public String getContSize() {
		return contSize;
	}
	/**
	 * @return creHide를(을) 리턴합니다.
	 */
	public String getCreHide() {
		return creHide;
	}
	/**
	 * @return creHideName를(을) 리턴합니다.
	 */
	public String getCreHideName() {
		return creHideName;
	}
	/**
	 * @return creIPv4를(을) 리턴합니다.
	 */
	public String getCreIPv4() {
		return creIPv4;
	}
	/**
	 * @return creIPv6를(을) 리턴합니다.
	 */
	public String getCreIPv6() {
		return creIPv6;
	}
	/**
	 * @return creMac를(을) 리턴합니다.
	 */
	public String getCreMac() {
		return creMac;
	}
	/**
	 * @return creMobile를(을) 리턴합니다.
	 */
	public String getCreMobile() {
		return creMobile;
	}
	/**
	 * @return creMobileName를(을) 리턴합니다.
	 */
	public String getCreMobileName() {
		return creMobileName;
	}
	/**
	 * @return creOrder를(을) 리턴합니다.
	 */
	public String getCreOrder() {
		return creOrder;
	}
	/**
	 * @return crePasswd를(을) 리턴합니다.
	 */
	public String getCrePasswd() {
		return crePasswd;
	}
	/**
	 * @return delIPv4를(을) 리턴합니다.
	 */
	public String getDelIPv4() {
		return delIPv4;
	}
	/**
	 * @return delIPv6를(을) 리턴합니다.
	 */
	public String getDelIPv6() {
		return delIPv6;
	}
	/**
	 * @return delMac를(을) 리턴합니다.
	 */
	public String getDelMac() {
		return delMac;
	}
	/**
	 * @return downAbs를(을) 리턴합니다.
	 */
	public String getDownAbs() {
		return downAbs;
	}
	/**
	 * @return downExt를(을) 리턴합니다.
	 */
	public String getDownExt() {
		return downExt;
	}
	/**
	 * @return downRel를(을) 리턴합니다.
	 */
	public String getDownRel() {
		return downRel;
	}
	/**
	 * @return downUUID를(을) 리턴합니다.
	 */
	public String getDownUUID() {
		return downUUID;
	}
	/**
	 * @return fileAbs(을) 리턴합니다.
	 */
	public String getFileAbs() {
		return fileAbs;
	}
	/**
	 * @return fileCount를(을) 리턴합니다.
	 */
	public String getFileCount() {
		return fileCount;
	}
	/**
	 * @return fileLinkD를(을) 리턴합니다.
	 */
	public String getFileLinkD() {
		return fileLinkD;
	}
	/**
	 * @return fileLinkM를(을) 리턴합니다.
	 */
	public String getFileLinkM() {
		return fileLinkM;
	}
	/**
	 * @return fileLinkS를(을) 리턴합니다.
	 */
	public String getFileLinkS() {
		return fileLinkS;
	}
	/**
	 * @return fileName를(을) 리턴합니다.
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @return fileRel를(을) 리턴합니다.
	 */
	public String getFileRel() {
		return fileRel;
	}
	/**
	 * @return fileSize를(을) 리턴합니다.
	 */
	public String getFileSize() {
		return fileSize;
	}
	/**
	 * @return fileUri를(을) 리턴합니다.
	 */
	public String getFileUri() {
		return fileUri;
	}
	/**
	 * @return hitCount를(을) 리턴합니다.
	 */
	public String getHitCount() {
		return hitCount;
	}
	/**
	 * @return modIPv4를(을) 리턴합니다.
	 */
	public String getModIPv4() {
		return modIPv4;
	}
	/**
	 * @return modIPv6를(을) 리턴합니다.
	 */
	public String getModIPv6() {
		return modIPv6;
	}
	/**
	 * @return modMac를(을) 리턴합니다.
	 */
	public String getModMac() {
		return modMac;
	}
	/**
	 * @return modOrder를(을) 리턴합니다.
	 */
	public String getModOrder() {
		return modOrder;
	}
	/**
	 * @return qstDate를(을) 리턴합니다.
	 */
	public String getQstDate() {
		return qstDate;
	}
	/**
	 * @return rlyCrypt를(을) 리턴합니다.
	 */
	public String getRlyCrypt() {
		return rlyCrypt;
	}
	/**
	 * @return rlyDesc를(을) 리턴합니다.
	 */
	public String getRlyDesc() {
		return rlyDesc;
	}
	/**
	 * @return rlyDescBR를(을) 리턴합니다.
	 */
	public String getRlyDescBR() {
		return rlyDescBR;
	}
	/**
	 * @return rlyDescCR를(을) 리턴합니다.
	 */
	public String getRlyDescCR() {
		return rlyDescCR;
	}
	/**
	 * @return rlyPattern를(을) 리턴합니다.
	 */
	public String getRlyPattern() {
		return rlyPattern;
	}
	/**
	 * @return rlySize를(을) 리턴합니다.
	 */
	public String getRlySize() {
		return rlySize;
	}
	/**
	 * @return takeDate를(을) 리턴합니다.
	 */
	public String getTakeDate() {
		return takeDate;
	}
	/**
	 * @param ansCount 설정하려는 ansCount.
	 */
	public void setAnsCount(String ansCount) {
		this.ansCount = ansCount;
	}
	/**
	 * @param ansDate 설정하려는 ansDate.
	 */
	public void setAnsDate(String ansDate) {
		this.ansDate = ansDate;
	}
	/**
	 * @param ansMode 설정하려는 ansMode.
	 */
	public void setAnsMode(String ansMode) {
		this.ansMode = ansMode;
	}
	/**
	 * @param ansPos 설정하려는 ansPos.
	 */
	public void setAnsPos(String ansPos) {
		this.ansPos = ansPos;
	}
	/**
	 * @param ansRef 설정하려는 ansRef.
	 */
	public void setAnsRef(String ansRef) {
		this.ansRef = ansRef;
	}
	/**
	 * @param ansStep 설정하려는 ansStep.
	 */
	public void setAnsStep(String ansStep) {
		this.ansStep = ansStep;
	}
	/**
	 * @param bbsCate 설정하려는 bbsCate.
	 */
	public void setBbsCate(String bbsCate) {
		this.bbsCate = bbsCate;
	}
	/**
	 * @param bbsCateName 설정하려는 bbsCateName.
	 */
	public void setBbsCateName(String bbsCateName) {
		this.bbsCateName = bbsCateName;
	}
	/**
	 * @param bbsFiles 설정하려는 bbsFiles.
	 */
	public void setBbsFiles(String bbsFiles) {
		this.bbsFiles = bbsFiles;
	}
	/**
	 * @param bbsSiteType 설정하려는 bbsSiteType.
	 */
	public void setBbsSiteType(String bbsSiteType) {
		this.bbsSiteType = bbsSiteType;
	}
	/**
	 * @param bbsSiteTypeName 설정하려는 bbsSiteTypeName.
	 */
	public void setBbsSiteTypeName(String bbsSiteTypeName) {
		this.bbsSiteTypeName = bbsSiteTypeName;
	}
	/**
	 * @param bbsSiteUri 설정하려는 bbsSiteUri.
	 */
	public void setBbsSiteUri(String bbsSiteUri) {
		this.bbsSiteUri = bbsSiteUri;
	}
	/**
	 * @param bbsSiteUse 설정하려는 bbsSiteUse.
	 */
	public void setBbsSiteUse(String bbsSiteUse) {
		this.bbsSiteUse = bbsSiteUse;
	}
	/**
	 * @param bbsTitle 설정하려는 bbsTitle.
	 */
	public void setBbsTitle(String bbsTitle) {
		this.bbsTitle = bbsTitle;
	}
	/**
	 * @param bbsTypeName 설정하려는 bbsTypeName.
	 */
	public void setBbsTypeName(String bbsTypeName) {
		this.bbsTypeName = bbsTypeName;
	}
	/**
	 * @param contCrypt 설정하려는 contCrypt.
	 */
	public void setContCrypt(String contCrypt) {
		this.contCrypt = contCrypt;
	}
	/**
	 * @param contDesc 설정하려는 contDesc.
	 */
	public void setContDesc(String contDesc) {
		this.contDesc = contDesc;
	}
	/**
	 * @param contDescBR 설정하려는 contDescBR.
	 */
	public void setContDescBR(String contDescBR) {
		this.contDescBR = contDescBR;
	}
	/**
	 * @param contDescCR 설정하려는 contDescCR.
	 */
	public void setContDescCR(String contDescCR) {
		this.contDescCR = contDescCR;
	}
	/**
	 * @param contLinkC 설정하려는 contLinkC.
	 */
	public void setContLinkC(String contLinkC) {
		this.contLinkC = contLinkC;
	}
	/**
	 * @param contLinkD 설정하려는 contLinkD.
	 */
	public void setContLinkD(String contLinkD) {
		this.contLinkD = contLinkD;
	}
	/**
	 * @param contLinkM 설정하려는 contLinkM.
	 */
	public void setContLinkM(String contLinkM) {
		this.contLinkM = contLinkM;
	}
	/**
	 * @param contLinkS 설정하려는 contLinkS.
	 */
	public void setContLinkS(String contLinkS) {
		this.contLinkS = contLinkS;
	}
	/**
	 * @param contPattern 설정하려는 contPattern.
	 */
	public void setContPattern(String contPattern) {
		this.contPattern = contPattern;
	}
	/**
	 * @param contSize 설정하려는 contSize.
	 */
	public void setContSize(String contSize) {
		this.contSize = contSize;
	}
	/**
	 * @param creHide 설정하려는 creHide.
	 */
	public void setCreHide(String creHide) {
		this.creHide = creHide;
	}
	/**
	 * @param creHideName 설정하려는 creHideName.
	 */
	public void setCreHideName(String creHideName) {
		this.creHideName = creHideName;
	}
	/**
	 * @param creIPv4 설정하려는 creIPv4.
	 */
	public void setCreIPv4(String creIPv4) {
		this.creIPv4 = creIPv4;
	}
	/**
	 * @param creIPv6 설정하려는 creIPv6.
	 */
	public void setCreIPv6(String creIPv6) {
		this.creIPv6 = creIPv6;
	}
	/**
	 * @param creMac 설정하려는 creMac.
	 */
	public void setCreMac(String creMac) {
		this.creMac = creMac;
	}
	/**
	 * @param creMobile 설정하려는 creMobile.
	 */
	public void setCreMobile(String creMobile) {
		this.creMobile = creMobile;
	}
	/**
	 * @param creMobileName 설정하려는 creMobileName.
	 */
	public void setCreMobileName(String creMobileName) {
		this.creMobileName = creMobileName;
	}
	/**
	 * @param creOrder 설정하려는 creOrder.
	 */
	public void setCreOrder(String creOrder) {
		this.creOrder = creOrder;
	}
	/**
	 * @param crePasswd 설정하려는 crePasswd.
	 */
	public void setCrePasswd(String crePasswd) {
		this.crePasswd = crePasswd;
	}
	/**
	 * @param delIPv4 설정하려는 delIPv4.
	 */
	public void setDelIPv4(String delIPv4) {
		this.delIPv4 = delIPv4;
	}
	/**
	 * @param delIPv6 설정하려는 delIPv6.
	 */
	public void setDelIPv6(String delIPv6) {
		this.delIPv6 = delIPv6;
	}
	/**
	 * @param delMac 설정하려는 delMac.
	 */
	public void setDelMac(String delMac) {
		this.delMac = delMac;
	}
	/**
	 * @param downAbs 설정하려는 downAbs.
	 */
	public void setDownAbs(String downAbs) {
		this.downAbs = downAbs;
	}
	/**
	 * @param downExt 설정하려는 downExt.
	 */
	public void setDownExt(String downExt) {
		this.downExt = downExt;
	}
	/**
	 * @param downRel 설정하려는 downRel.
	 */
	public void setDownRel(String downRel) {
		this.downRel = downRel;
	}
	/**
	 * @param downUUID 설정하려는 downUUID.
	 */
	public void setDownUUID(String downUUID) {
		this.downUUID = downUUID;
	}
	/**
	 * @param fileAbs 설정하려는 fileAbs.
	 */
	public void setFileAbs(String fileAbs) {
		this.fileAbs = fileAbs;
	}
	/**
	 * @param fileCount 설정하려는 fileCount.
	 */
	public void setFileCount(String fileCount) {
		this.fileCount = fileCount;
	}
	/**
	 * @param fileLinkD 설정하려는 fileLinkD.
	 */
	public void setFileLinkD(String fileLinkD) {
		this.fileLinkD = fileLinkD;
	}
	/**
	 * @param fileLinkM 설정하려는 fileLinkM.
	 */
	public void setFileLinkM(String fileLinkM) {
		this.fileLinkM = fileLinkM;
	}
	/**
	 * @param fileLinkS 설정하려는 fileLinkS.
	 */
	public void setFileLinkS(String fileLinkS) {
		this.fileLinkS = fileLinkS;
	}
	/**
	 * @param fileName 설정하려는 fileName.
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @param fileRel 설정하려는 fileRel.
	 */
	public void setFileRel(String fileRel) {
		this.fileRel = fileRel;
	}
	/**
	 * @param fileSize 설정하려는 fileSize.
	 */
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	/**
	 * @param fileUri 설정하려는 fileUri.
	 */
	public void setFileUri(String fileUri) {
		this.fileUri = fileUri;
	}
	/**
	 * @param hitCount 설정하려는 hitCount.
	 */
	public void setHitCount(String hitCount) {
		this.hitCount = hitCount;
	}
	/**
	 * @param modIPv4 설정하려는 modIPv4.
	 */
	public void setModIPv4(String modIPv4) {
		this.modIPv4 = modIPv4;
	}
	/**
	 * @param modIPv6 설정하려는 modIPv6.
	 */
	public void setModIPv6(String modIPv6) {
		this.modIPv6 = modIPv6;
	}
	/**
	 * @param modMac 설정하려는 modMac.
	 */
	public void setModMac(String modMac) {
		this.modMac = modMac;
	}
	/**
	 * @param modOrder 설정하려는 modOrder.
	 */
	public void setModOrder(String modOrder) {
		this.modOrder = modOrder;
	}
	/**
	 * @param qstDate 설정하려는 qstDate.
	 */
	public void setQstDate(String qstDate) {
		this.qstDate = qstDate;
	}
	/**
	 * @param rlyCrypt 설정하려는 rlyCrypt.
	 */
	public void setRlyCrypt(String rlyCrypt) {
		this.rlyCrypt = rlyCrypt;
	}
	/**
	 * @param rlyDesc 설정하려는 rlyDesc.
	 */
	public void setRlyDesc(String rlyDesc) {
		this.rlyDesc = rlyDesc;
	}
	/**
	 * @param rlyDescBR 설정하려는 rlyDescBR.
	 */
	public void setRlyDescBR(String rlyDescBR) {
		this.rlyDescBR = rlyDescBR;
	}
	/**
	 * @param rlyDescCR 설정하려는 rlyDescCR.
	 */
	public void setRlyDescCR(String rlyDescCR) {
		this.rlyDescCR = rlyDescCR;
	}
	/**
	 * @param rlyPattern 설정하려는 rlyPattern.
	 */
	public void setRlyPattern(String rlyPattern) {
		this.rlyPattern = rlyPattern;
	}
	/**
	 * @param rlySize 설정하려는 rlySize.
	 */
	public void setRlySize(String rlySize) {
		this.rlySize = rlySize;
	}
	/**
	 * @param takeDate 설정하려는 takeDate.
	 */
	public void setTakeDate(String takeDate) {
		this.takeDate = takeDate;
	}

	/**
	 * @return the seq
	 */
	public String getSeq() {
		return seq;
	}
	/**
	 * @param seq the seq to set
	 */
	public void setSeq(String seq) {
		this.seq = seq;
	}
	/**
	 * @return the reLevel
	 */
	public String getReLevel() {
		return reLevel;
	}
	/**
	 * @param reLevel the reLevel to set
	 */
	public void setReLevel(String reLevel) {
		this.reLevel = reLevel;
	}
	/**
	 * @return the re_level
	 */
	public String getRe_level() {
		return re_level;
	}
	/**
	 * @param re_level the re_level to set
	 */
	public void setRe_level(String re_level) {
		this.re_level = re_level;
	}
	/**
	 * @return the reStep
	 */
	public String getReStep() {
		return reStep;
	}
	/**
	 * @param reStep the reStep to set
	 */
	public void setReStep(String reStep) {
		this.reStep = reStep;
	}
	/**
	 * @return the re_step
	 */
	public String getRe_step() {
		return re_step;
	}
	/**
	 * @param re_step the re_step to set
	 */
	public void setRe_step(String re_step) {
		this.re_step = re_step;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @return the pwd
	 */
	public String getPwd() {
		return pwd;
	}
	/**
	 * @param pwd the pwd to set
	 */
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	/**
	 * @return the readNum
	 */
	public String getReadNum() {
		return readNum;
	}
	/**
	 * @param readNum the readNum to set
	 */
	public void setReadNum(String readNum) {
		this.readNum = readNum;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the mailAddr
	 */
	public String getMailAddr() {
		return mailAddr;
	}
	/**
	 * @param mailAddr the mailAddr to set
	 */
	public void setMailAddr(String mailAddr) {
		this.mailAddr = mailAddr;
	}
	/**
	 * @return the homePage
	 */
	public String getHomePage() {
		return homePage;
	}
	/**
	 * @param homePage the homePage to set
	 */
	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}
	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 * @return the tag
	 */
	public String getTag() {
		return tag;
	}
	/**
	 * @param tag the tag to set
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}
	/**
	 * @return the imgInfo
	 */
	public String getImgInfo() {
		return imgInfo;
	}
	/**
	 * @param imgInfo the imgInfo to set
	 */
	public void setImgInfo(String imgInfo) {
		this.imgInfo = imgInfo;
	}
	/**
	 * @return the relativeCnt
	 */
	public String getRelativeCnt() {
		return relativeCnt;
	}
	/**
	 * @param relativeCnt the relativeCnt to set
	 */
	public void setRelativeCnt(String relativeCnt) {
		this.relativeCnt = relativeCnt;
	}
	/**
	 * @return the ssn
	 */
	public String getSsn() {
		return ssn;
	}
	/**
	 * @param ssn the ssn to set
	 */
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	/**
	 * @return the ssn2
	 */
	public String getSsn2() {
		return ssn2;
	}
	/**
	 * @param ssn2 the ssn2 to set
	 */
	public void setSsn2(String ssn2) {
		this.ssn2 = ssn2;
	}
	/**
	 * @return the listType
	 */
	public String getListType() {
		return listType;
	}
	/**
	 * @param listType the listType to set
	 */
	public void setListType(String listType) {
		this.listType = listType;
	}
	/**
	 * @return the listTypeName
	 */
	public String getListTypeName() {
		return listTypeName;
	}
	/**
	 * @param listTypeName the listTypeName to set
	 */
	public void setListTypeName(String listTypeName) {
		this.listTypeName = listTypeName;
	}
	/**
	 * @return the list_type
	 */
	public String getList_type() {
		return list_type;
	}
	/**
	 * @param list_type the list_type to set
	 */
	public void setList_type(String list_type) {
		this.list_type = list_type;
	}
	/**
	 * @return the code1
	 */
	public String getCode1() {
		return code1;
	}
	/**
	 * @param code1 the code1 to set
	 */
	public void setCode1(String code1) {
		this.code1 = code1;
	}
	/**
	 * @return the code2
	 */
	public String getCode2() {
		return code2;
	}
	/**
	 * @param code2 the code2 to set
	 */
	public void setCode2(String code2) {
		this.code2 = code2;
	}
	/**
	 * @return the name2
	 */
	public String getName2() {
		return name2;
	}
	/**
	 * @param name2 the name2 to set
	 */
	public void setName2(String name2) {
		this.name2 = name2;
	}
	/**
	 * @return the regDate1
	 */
	public String getRegDate1() {
		return regDate1;
	}
	/**
	 * @param regDate1 the regDate1 to set
	 */
	public void setRegDate1(String regDate1) {
		this.regDate1 = regDate1;
	}
	/**
	 * @return the reg_date1
	 */
	public String getReg_date1() {
		return reg_date1;
	}
	/**
	 * @param reg_date1 the reg_date1 to set
	 */
	public void setReg_date1(String reg_date1) {
		this.reg_date1 = reg_date1;
	}
	/**
	 * @return the regDate2
	 */
	public String getRegDate2() {
		return regDate2;
	}
	/**
	 * @param regDate2 the regDate2 to set
	 */
	public void setRegDate2(String regDate2) {
		this.regDate2 = regDate2;
	}
	/**
	 * @return the reg_date2
	 */
	public String getReg_date2() {
		return reg_date2;
	}
	/**
	 * @param reg_date2 the reg_date2 to set
	 */
	public void setReg_date2(String reg_date2) {
		this.reg_date2 = reg_date2;
	}
	/**
	 * @return the regName2
	 */
	public String getRegName2() {
		return regName2;
	}
	/**
	 * @param regName2 the regName2 to set
	 */
	public void setRegName2(String regName2) {
		this.regName2 = regName2;
	}
	/**
	 * @return the content2
	 */
	public String getContent2() {
		return content2;
	}
	/**
	 * @param content2 the content2 to set
	 */
	public void setContent2(String content2) {
		this.content2 = content2;
	}
	/**
	 * @return the confUUID1
	 */
	public String getConfUUID1() {
		return confUUID1;
	}
	/**
	 * @param confUUID1 the confUUID1 to set
	 */
	public void setConfUUID1(String confUUID1) {
		this.confUUID1 = confUUID1;
	}
	/**
	 * @return the confUUID2
	 */
	public String getConfUUID2() {
		return confUUID2;
	}
	/**
	 * @param confUUID2 the confUUID2 to set
	 */
	public void setConfUUID2(String confUUID2) {
		this.confUUID2 = confUUID2;
	}
	/**
	 * @return the content2BR
	 */
	public String getContent2BR() {
		return content2BR;
	}
	/**
	 * @param content2BR the content2BR to set
	 */
	public void setContent2BR(String content2BR) {
		this.content2BR = content2BR;
	}
	/**
	 * @return the content2CR
	 */
	public String getContent2CR() {
		return content2CR;
	}
	/**
	 * @param content2CR the content2CR to set
	 */
	public void setContent2CR(String content2CR) {
		this.content2CR = content2CR;
	}
	/**
	 * @return the confCode
	 */
	public String getConfCode() {
		return confCode;
	}
	/**
	 * @param confCode the confCode to set
	 */
	public void setConfCode(String confCode) {
		this.confCode = confCode;
	}
	/**
	 * @return the confName
	 */
	public String getConfName() {
		return confName;
	}
	/**
	 * @param confName the confName to set
	 */
	public void setConfName(String confName) {
		this.confName = confName;
	}
	/**
	 * @return the regiDate
	 */
	public String getRegiDate() {
		return regiDate;
	}
	/**
	 * @param regiDate the regiDate to set
	 */
	public void setRegiDate(String regiDate) {
		this.regiDate = regiDate;
	}
	/**
	 * @return the confCode1
	 */
	public String getConfCode1() {
		return confCode1;
	}
	/**
	 * @param confCode1 the confCode1 to set
	 */
	public void setConfCode1(String confCode1) {
		this.confCode1 = confCode1;
	}
	/**
	 * @return the confName1
	 */
	public String getConfName1() {
		return confName1;
	}
	/**
	 * @param confName1 the confName1 to set
	 */
	public void setConfName1(String confName1) {
		this.confName1 = confName1;
	}
	/**
	 * @return the regiDate1
	 */
	public String getRegiDate1() {
		return regiDate1;
	}
	/**
	 * @param regiDate1 the regiDate1 to set
	 */
	public void setRegiDate1(String regiDate1) {
		this.regiDate1 = regiDate1;
	}
	/**
	 * @return the regiName1
	 */
	public String getRegiName1() {
		return regiName1;
	}
	/**
	 * @param regiName1 the regiName1 to set
	 */
	public void setRegiName1(String regiName1) {
		this.regiName1 = regiName1;
	}
	/**
	 * @return the confCode2
	 */
	public String getConfCode2() {
		return confCode2;
	}
	/**
	 * @param confCode2 the confCode2 to set
	 */
	public void setConfCode2(String confCode2) {
		this.confCode2 = confCode2;
	}
	/**
	 * @return the confName2
	 */
	public String getConfName2() {
		return confName2;
	}
	/**
	 * @param confName2 the confName2 to set
	 */
	public void setConfName2(String confName2) {
		this.confName2 = confName2;
	}
	/**
	 * @return the regiDate2
	 */
	public String getRegiDate2() {
		return regiDate2;
	}
	/**
	 * @param regiDate2 the regiDate2 to set
	 */
	public void setRegiDate2(String regiDate2) {
		this.regiDate2 = regiDate2;
	}
	/**
	 * @return the regiName2
	 */
	public String getRegiName2() {
		return regiName2;
	}
	/**
	 * @param regiName2 the regiName2 to set
	 */
	public void setRegiName2(String regiName2) {
		this.regiName2 = regiName2;
	}
	/**
	 * @return the contents2
	 */
	public String getContents2() {
		return contents2;
	}
	/**
	 * @param contents2 the contents2 to set
	 */
	public void setContents2(String contents2) {
		this.contents2 = contents2;
	}
	/**
	 * @return the contents2BR
	 */
	public String getContents2BR() {
		return contents2BR;
	}
	/**
	 * @param contents2BR the contents2BR to set
	 */
	public void setContents2BR(String contents2BR) {
		this.contents2BR = contents2BR;
	}
	/**
	 * @return the contents2CR
	 */
	public String getContents2CR() {
		return contents2CR;
	}
	/**
	 * @param contents2CR the contents2CR to set
	 */
	public void setContents2CR(String contents2CR) {
		this.contents2CR = contents2CR;
	}


	// add
	public String getScDate() {
		return scDate;
	}
	public void setScDate(String scDate) {
		this.scDate = scDate;
	}

	// add
	public String getCreNoti() {
		return creNoti;
	}
	public void setCreNoti(String creNoti) {
		this.creNoti = creNoti;
	}

	// add
	public String getFileViewImg() {
		return fileViewImg;
	}
	public void setFileViewImg(String fileViewImg) {
		this.fileViewImg = fileViewImg;
	}

}	// end of class