/*******************************************************************************
  Program ID  : BaseFilesVO
  Description : 통합파일 정보에 대한 Value Object를 정의한다
  Author      : (주)엠씨에스텍 오영덕
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    (주)엠씨에스텍 오영덕  2019.07.01  최초 생성
  v1.0    (주)엠씨에스텍 오영덕  2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.cmmn.vo;

import java.util.*;

@SuppressWarnings("serial")
public class BaseFilesVO extends BaseCommonVO implements java.io.Serializable { 

	// 통합파일 정보
	private String fileType;					// 
	private String fileRel;					// 
	private String fileAbs;					// 
	private String fileUri;					// 
	private String fileThumb;					// 
	private String fileDSize;					// 
	private String fileLSize;					// 
	private String fileWSize;					// 
	private String fileHSize;					// 
	private String useDown;					// 
	private String useExpt;					// 
	private String creIPv4;					// 
	private String creIPv6;					// 
	private String creMac;						// 
	private String modIPv4;					// 
	private String modIPv6;					// 
	private String modMac;						// 
	private String delIPv4;					// 
	private String delIPv6;					// 
	private String delMac;						// 

	private String imageID;					// 
	private String fileTypeName;				// 
	private String fileLinkS;					// 첨부파일 내용 (싱글 다운용)
	private String fileLinkM;					// 첨부파일 내용 (멀티 다운용)
	private String fileLinkD;					// 첨부파일 내용 (일반 표시용)
	private String fileID;						// 
	private String fileImage;					// 
	private String downUUID;
	private String downExt;

	@SuppressWarnings("rawtypes")
	List fileList;


	/**
	 * @return creIPv4를(을) 리턴합니다.
	 */
	public String getCreIPv4() {
		return creIPv4;
	}
	/**
	 * @return creIPv6를(을) 리턴합니다.
	 */
	public String getCreIPv6() {
		return creIPv6;
	}
	/**
	 * @return creMac를(을) 리턴합니다.
	 */
	public String getCreMac() {
		return creMac;
	}
	/**
	 * @return delIPv4를(을) 리턴합니다.
	 */
	public String getDelIPv4() {
		return delIPv4;
	}
	/**
	 * @return delIPv6를(을) 리턴합니다.
	 */
	public String getDelIPv6() {
		return delIPv6;
	}
	/**
	 * @return delMac를(을) 리턴합니다.
	 */
	public String getDelMac() {
		return delMac;
	}
	/**
	 * @return downExt를(을) 리턴합니다.
	 */
	public String getDownExt() {
		return downExt;
	}
	/**
	 * @return downUUID를(을) 리턴합니다.
	 */
	public String getDownUUID() {
		return downUUID;
	}
	/**
	 * @return fileAbs를(을) 리턴합니다.
	 */
	public String getFileAbs() {
		return fileAbs;
	}
	/**
	 * @return fileDSize를(을) 리턴합니다.
	 */
	public String getFileDSize() {
		return fileDSize;
	}
	/**
	 * @return fileHSize를(을) 리턴합니다.
	 */
	public String getFileHSize() {
		return fileHSize;
	}
	/**
	 * @return fileID를(을) 리턴합니다.
	 */
	public String getFileID() {
		return fileID;
	}
	/**
	 * @return fileImage를(을) 리턴합니다.
	 */
	public String getFileImage() {
		return fileImage;
	}
	/**
	 * @return fileLinkD를(을) 리턴합니다.
	 */
	public String getFileLinkD() {
		return fileLinkD;
	}
	/**
	 * @return fileLinkM를(을) 리턴합니다.
	 */
	public String getFileLinkM() {
		return fileLinkM;
	}
	/**
	 * @return fileLinkS를(을) 리턴합니다.
	 */
	public String getFileLinkS() {
		return fileLinkS;
	}
	/**
	 * @return fileList를(을) 리턴합니다.
	 */
	@SuppressWarnings("rawtypes")
	public List getFileList() {
		return fileList;
	}
	/**
	 * @return fileLSize를(을) 리턴합니다.
	 */
	public String getFileLSize() {
		return fileLSize;
	}
	/**
	 * @return fileRel를(을) 리턴합니다.
	 */
	public String getFileRel() {
		return fileRel;
	}
	/**
	 * @return fileThumb를(을) 리턴합니다.
	 */
	public String getFileThumb() {
		return fileThumb;
	}
	/**
	 * @return fileType를(을) 리턴합니다.
	 */
	public String getFileType() {
		return fileType;
	}
	/**
	 * @return fileTypeName를(을) 리턴합니다.
	 */
	public String getFileTypeName() {
		return fileTypeName;
	}
	/**
	 * @return fileUri를(을) 리턴합니다.
	 */
	public String getFileUri() {
		return fileUri;
	}
	/**
	 * @return fileWSize를(을) 리턴합니다.
	 */
	public String getFileWSize() {
		return fileWSize;
	}
	/**
	 * @return imageID를(을) 리턴합니다.
	 */
	public String getImageID() {
		return imageID;
	}
	/**
	 * @return modIPv4를(을) 리턴합니다.
	 */
	public String getModIPv4() {
		return modIPv4;
	}
	/**
	 * @return modIPv6를(을) 리턴합니다.
	 */
	public String getModIPv6() {
		return modIPv6;
	}
	/**
	 * @return modMac를(을) 리턴합니다.
	 */
	public String getModMac() {
		return modMac;
	}
	/**
	 * @return useDown를(을) 리턴합니다.
	 */
	public String getUseDown() {
		return useDown;
	}
	/**
	 * @return useExpt를(을) 리턴합니다.
	 */
	public String getUseExpt() {
		return useExpt;
	}
	/**
	 * @param creIPv4 설정하려는 creIPv4.
	 */
	public void setCreIPv4(String creIPv4) {
		this.creIPv4 = creIPv4;
	}
	/**
	 * @param creIPv6 설정하려는 creIPv6.
	 */
	public void setCreIPv6(String creIPv6) {
		this.creIPv6 = creIPv6;
	}
	/**
	 * @param creMac 설정하려는 creMac.
	 */
	public void setCreMac(String creMac) {
		this.creMac = creMac;
	}
	/**
	 * @param delIPv4 설정하려는 delIPv4.
	 */
	public void setDelIPv4(String delIPv4) {
		this.delIPv4 = delIPv4;
	}
	/**
	 * @param delIPv6 설정하려는 delIPv6.
	 */
	public void setDelIPv6(String delIPv6) {
		this.delIPv6 = delIPv6;
	}
	/**
	 * @param delMac 설정하려는 delMac.
	 */
	public void setDelMac(String delMac) {
		this.delMac = delMac;
	}
	/**
	 * @param downExt 설정하려는 downExt.
	 */
	public void setDownExt(String downExt) {
		this.downExt = downExt;
	}
	/**
	 * @param downUUID 설정하려는 downUUID.
	 */
	public void setDownUUID(String downUUID) {
		this.downUUID = downUUID;
	}
	/**
	 * @param fileAbs 설정하려는 fileAbs.
	 */
	public void setFileAbs(String fileAbs) {
		this.fileAbs = fileAbs;
	}
	/**
	 * @param fileDSize 설정하려는 fileDSize.
	 */
	public void setFileDSize(String fileDSize) {
		this.fileDSize = fileDSize;
	}
	/**
	 * @param fileHSize 설정하려는 fileHSize.
	 */
	public void setFileHSize(String fileHSize) {
		this.fileHSize = fileHSize;
	}
	/**
	 * @param fileID 설정하려는 fileID.
	 */
	public void setFileID(String fileID) {
		this.fileID = fileID;
	}
	/**
	 * @param fileImage 설정하려는 fileImage.
	 */
	public void setFileImage(String fileImage) {
		this.fileImage = fileImage;
	}
	/**
	 * @param fileLinkD 설정하려는 fileLinkD.
	 */
	public void setFileLinkD(String fileLinkD) {
		this.fileLinkD = fileLinkD;
	}
	/**
	 * @param fileLinkM 설정하려는 fileLinkM.
	 */
	public void setFileLinkM(String fileLinkM) {
		this.fileLinkM = fileLinkM;
	}
	/**
	 * @param fileLinkS 설정하려는 fileLinkS.
	 */
	public void setFileLinkS(String fileLinkS) {
		this.fileLinkS = fileLinkS;
	}
	/**
	 * @param fileList 설정하려는 fileList.
	 */
	@SuppressWarnings("rawtypes")
	public void setFileList(List fileList) {
		this.fileList = fileList;
	}
	/**
	 * @param fileLSize 설정하려는 fileLSize.
	 */
	public void setFileLSize(String fileLSize) {
		this.fileLSize = fileLSize;
	}
	/**
	 * @param fileRel 설정하려는 fileRel.
	 */
	public void setFileRel(String fileRel) {
		this.fileRel = fileRel;
	}
	/**
	 * @param fileThumb 설정하려는 fileThumb.
	 */
	public void setFileThumb(String fileThumb) {
		this.fileThumb = fileThumb;
	}
	/**
	 * @param fileType 설정하려는 fileType.
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	/**
	 * @param fileTypeName 설정하려는 fileTypeName.
	 */
	public void setFileTypeName(String fileTypeName) {
		this.fileTypeName = fileTypeName;
	}
	/**
	 * @param fileUri 설정하려는 fileUri.
	 */
	public void setFileUri(String fileUri) {
		this.fileUri = fileUri;
	}
	/**
	 * @param fileWSize 설정하려는 fileWSize.
	 */
	public void setFileWSize(String fileWSize) {
		this.fileWSize = fileWSize;
	}
	/**
	 * @param imageID 설정하려는 imageID.
	 */
	public void setImageID(String imageID) {
		this.imageID = imageID;
	}
	/**
	 * @param modIPv4 설정하려는 modIPv4.
	 */
	public void setModIPv4(String modIPv4) {
		this.modIPv4 = modIPv4;
	}
	/**
	 * @param modIPv6 설정하려는 modIPv6.
	 */
	public void setModIPv6(String modIPv6) {
		this.modIPv6 = modIPv6;
	}
	/**
	 * @param modMac 설정하려는 modMac.
	 */
	public void setModMac(String modMac) {
		this.modMac = modMac;
	}
	/**
	 * @param useDown 설정하려는 useDown.
	 */
	public void setUseDown(String useDown) {
		this.useDown = useDown;
	}
	/**
	 * @param useExpt 설정하려는 useExpt.
	 */
	public void setUseExpt(String useExpt) {
		this.useExpt = useExpt;
	}

}	// end of class