/*******************************************************************************
  Program ID  : BaseVisitsVO
  Description : 방문자 정보에 대한 Value Object를 정의한다
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.cmmn.vo;

@SuppressWarnings("serial")
public class BaseVisitsVO extends BaseCommonVO implements java.io.Serializable { 

	// 공통 정보
	private String creIPv4;					// 
	private String creIPv6;					// 
	private String creMac;						// 
	private String modIPv4;					// 
	private String modIPv6;					// 
	private String modMac;						// 
	private String delIPv4;					// 
	private String delIPv6;					// 
	private String delMac;						// 

	// 방문자 정보
	private String vsnYear;					// 
	private String vsnSeq;						// 
	private String vsnIPv4;					// 
	private String vsnIPv6;					// 
	private String vsnMac;						// 
	private String vsnSite;					// 
	private String vsnReferer;					// 
	private String vsnAgent;					// 

	private String vsnDate;					// 
	private String hitCount;					// 

	// 방문 정보
	private String yearCnt;					// 
	private String daysCnt;					// 
	private String totsCnt;					// 


	/**
	 * @return creIPv4를(을) 리턴합니다.
	 */
	public String getCreIPv4() {
		return creIPv4;
	}
	/**
	 * @return creIPv6를(을) 리턴합니다.
	 */
	public String getCreIPv6() {
		return creIPv6;
	}
	/**
	 * @return creMac를(을) 리턴합니다.
	 */
	public String getCreMac() {
		return creMac;
	}
	/**
	 * @return daysCnt를(을) 리턴합니다.
	 */
	public String getDaysCnt() {
		return daysCnt;
	}
	/**
	 * @return delIPv4를(을) 리턴합니다.
	 */
	public String getDelIPv4() {
		return delIPv4;
	}
	/**
	 * @return delIPv6를(을) 리턴합니다.
	 */
	public String getDelIPv6() {
		return delIPv6;
	}
	/**
	 * @return delMac를(을) 리턴합니다.
	 */
	public String getDelMac() {
		return delMac;
	}
	/**
	 * @return hitCount를(을) 리턴합니다.
	 */
	public String getHitCount() {
		return hitCount;
	}
	/**
	 * @return modIPv4를(을) 리턴합니다.
	 */
	public String getModIPv4() {
		return modIPv4;
	}
	/**
	 * @return modIPv6를(을) 리턴합니다.
	 */
	public String getModIPv6() {
		return modIPv6;
	}
	/**
	 * @return modMac를(을) 리턴합니다.
	 */
	public String getModMac() {
		return modMac;
	}
	/**
	 * @return totsCnt를(을) 리턴합니다.
	 */
	public String getTotsCnt() {
		return totsCnt;
	}
	/**
	 * @return vsnAgent를(을) 리턴합니다.
	 */
	public String getVsnAgent() {
		return vsnAgent;
	}
	/**
	 * @return vsnDate를(을) 리턴합니다.
	 */
	public String getVsnDate() {
		return vsnDate;
	}
	/**
	 * @return vsnIPv4를(을) 리턴합니다.
	 */
	public String getVsnIPv4() {
		return vsnIPv4;
	}
	/**
	 * @return vsnIPv6를(을) 리턴합니다.
	 */
	public String getVsnIPv6() {
		return vsnIPv6;
	}
	/**
	 * @return vsnMac를(을) 리턴합니다.
	 */
	public String getVsnMac() {
		return vsnMac;
	}
	/**
	 * @return vsnReferer를(을) 리턴합니다.
	 */
	public String getVsnReferer() {
		return vsnReferer;
	}
	/**
	 * @return vsnSeq를(을) 리턴합니다.
	 */
	public String getVsnSeq() {
		return vsnSeq;
	}
	/**
	 * @return vsnSite를(을) 리턴합니다.
	 */
	public String getVsnSite() {
		return vsnSite;
	}
	/**
	 * @return vsnYear를(을) 리턴합니다.
	 */
	public String getVsnYear() {
		return vsnYear;
	}
	/**
	 * @return yearCnt를(을) 리턴합니다.
	 */
	public String getYearCnt() {
		return yearCnt;
	}
	/**
	 * @param creIPv4 설정하려는 creIPv4.
	 */
	public void setCreIPv4(String creIPv4) {
		this.creIPv4 = creIPv4;
	}
	/**
	 * @param creIPv6 설정하려는 creIPv6.
	 */
	public void setCreIPv6(String creIPv6) {
		this.creIPv6 = creIPv6;
	}
	/**
	 * @param creMac 설정하려는 creMac.
	 */
	public void setCreMac(String creMac) {
		this.creMac = creMac;
	}
	/**
	 * @param daysCnt 설정하려는 daysCnt.
	 */
	public void setDaysCnt(String daysCnt) {
		this.daysCnt = daysCnt;
	}
	/**
	 * @param delIPv4 설정하려는 delIPv4.
	 */
	public void setDelIPv4(String delIPv4) {
		this.delIPv4 = delIPv4;
	}
	/**
	 * @param delIPv6 설정하려는 delIPv6.
	 */
	public void setDelIPv6(String delIPv6) {
		this.delIPv6 = delIPv6;
	}
	/**
	 * @param delMac 설정하려는 delMac.
	 */
	public void setDelMac(String delMac) {
		this.delMac = delMac;
	}
	/**
	 * @param hitCount 설정하려는 hitCount.
	 */
	public void setHitCount(String hitCount) {
		this.hitCount = hitCount;
	}
	/**
	 * @param modIPv4 설정하려는 modIPv4.
	 */
	public void setModIPv4(String modIPv4) {
		this.modIPv4 = modIPv4;
	}
	/**
	 * @param modIPv6 설정하려는 modIPv6.
	 */
	public void setModIPv6(String modIPv6) {
		this.modIPv6 = modIPv6;
	}
	/**
	 * @param modMac 설정하려는 modMac.
	 */
	public void setModMac(String modMac) {
		this.modMac = modMac;
	}
	/**
	 * @param totsCnt 설정하려는 totsCnt.
	 */
	public void setTotsCnt(String totsCnt) {
		this.totsCnt = totsCnt;
	}
	/**
	 * @param vsnAgent 설정하려는 vsnAgent.
	 */
	public void setVsnAgent(String vsnAgent) {
		this.vsnAgent = vsnAgent;
	}
	/**
	 * @param vsnDate 설정하려는 vsnDate.
	 */
	public void setVsnDate(String vsnDate) {
		this.vsnDate = vsnDate;
	}
	/**
	 * @param vsnIPv4 설정하려는 vsnIPv4.
	 */
	public void setVsnIPv4(String vsnIPv4) {
		this.vsnIPv4 = vsnIPv4;
	}
	/**
	 * @param vsnIPv6 설정하려는 vsnIPv6.
	 */
	public void setVsnIPv6(String vsnIPv6) {
		this.vsnIPv6 = vsnIPv6;
	}
	/**
	 * @param vsnMac 설정하려는 vsnMac.
	 */
	public void setVsnMac(String vsnMac) {
		this.vsnMac = vsnMac;
	}
	/**
	 * @param vsnReferer 설정하려는 vsnReferer.
	 */
	public void setVsnReferer(String vsnReferer) {
		this.vsnReferer = vsnReferer;
	}
	/**
	 * @param vsnSeq 설정하려는 vsnSeq.
	 */
	public void setVsnSeq(String vsnSeq) {
		this.vsnSeq = vsnSeq;
	}
	/**
	 * @param vsnSite 설정하려는 vsnSite.
	 */
	public void setVsnSite(String vsnSite) {
		this.vsnSite = vsnSite;
	}
	/**
	 * @param vsnYear 설정하려는 vsnYear.
	 */
	public void setVsnYear(String vsnYear) {
		this.vsnYear = vsnYear;
	}
	/**
	 * @param yearCnt 설정하려는 yearCnt.
	 */
	public void setYearCnt(String yearCnt) {
		this.yearCnt = yearCnt;
	}

}	// end of class