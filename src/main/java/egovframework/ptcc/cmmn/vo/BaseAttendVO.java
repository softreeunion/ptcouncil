/*******************************************************************************
  Program ID  : BaseAttendVO
  Description : 방문신청 정보에 대한 Value Object를 정의한다
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.cmmn.vo;

@SuppressWarnings("serial")
public class BaseAttendVO extends BaseCommonVO implements java.io.Serializable { 

	// 공통 정보
	private String creIPv4;					// 
	private String creIPv6;					// 
	private String creMac;						// 
	private String modIPv4;					// 
	private String modIPv6;					// 
	private String modMac;						// 
	private String delIPv4;					// 
	private String delIPv6;					// 
	private String delMac;						// 

	private String fileName;					// 
	private String fileSize;					// 
	private String fileRel;					// 
	private String fileAbs;					// 
	private String fileUri;					// 
	private String fileLinkS;					// 첨부파일 내용 (싱글 다운용)
	private String fileLinkM;					// 첨부파일 내용 (멀티 다운용)
	private String fileLinkD;					// 첨부파일 내용 (일반 표시용)
	private String downUUID;
	private String downRel;
	private String downAbs;
	private String downExt;

	// 방문신청 정보
	private String reqType;
	private String reqName;
	private String reqPhone;
	private String vstType;
	private String vstDesc;
	private String vstDate;
	private String vstTime;
	private String vstTimeH;
	private String vstTimeM;
	private String vstPerson;
	private String useAccept;

	private String reqTypeName;
	private String vstTypeName;
	private String vstDescBR;
	private String vstDescCR;
	private String useAcceptName;


	/**
	 * @return creIPv4를(을) 리턴합니다.
	 */
	public String getCreIPv4() {
		return creIPv4;
	}
	/**
	 * @return creIPv6를(을) 리턴합니다.
	 */
	public String getCreIPv6() {
		return creIPv6;
	}
	/**
	 * @return creMac를(을) 리턴합니다.
	 */
	public String getCreMac() {
		return creMac;
	}
	/**
	 * @return delIPv4를(을) 리턴합니다.
	 */
	public String getDelIPv4() {
		return delIPv4;
	}
	/**
	 * @return delIPv6를(을) 리턴합니다.
	 */
	public String getDelIPv6() {
		return delIPv6;
	}
	/**
	 * @return delMac를(을) 리턴합니다.
	 */
	public String getDelMac() {
		return delMac;
	}
	/**
	 * @return downAbs를(을) 리턴합니다.
	 */
	public String getDownAbs() {
		return downAbs;
	}
	/**
	 * @return downExt를(을) 리턴합니다.
	 */
	public String getDownExt() {
		return downExt;
	}
	/**
	 * @return downRel를(을) 리턴합니다.
	 */
	public String getDownRel() {
		return downRel;
	}
	/**
	 * @return downUUID를(을) 리턴합니다.
	 */
	public String getDownUUID() {
		return downUUID;
	}
	/**
	 * @return fileAbs를(을) 리턴합니다.
	 */
	public String getFileAbs() {
		return fileAbs;
	}
	/**
	 * @return fileLinkD를(을) 리턴합니다.
	 */
	public String getFileLinkD() {
		return fileLinkD;
	}
	/**
	 * @return fileLinkM를(을) 리턴합니다.
	 */
	public String getFileLinkM() {
		return fileLinkM;
	}
	/**
	 * @return fileLinkS를(을) 리턴합니다.
	 */
	public String getFileLinkS() {
		return fileLinkS;
	}
	/**
	 * @return fileName를(을) 리턴합니다.
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @return fileRel를(을) 리턴합니다.
	 */
	public String getFileRel() {
		return fileRel;
	}
	/**
	 * @return fileSize를(을) 리턴합니다.
	 */
	public String getFileSize() {
		return fileSize;
	}
	/**
	 * @return fileUri를(을) 리턴합니다.
	 */
	public String getFileUri() {
		return fileUri;
	}
	/**
	 * @return modIPv4를(을) 리턴합니다.
	 */
	public String getModIPv4() {
		return modIPv4;
	}
	/**
	 * @return modIPv6를(을) 리턴합니다.
	 */
	public String getModIPv6() {
		return modIPv6;
	}
	/**
	 * @return modMac를(을) 리턴합니다.
	 */
	public String getModMac() {
		return modMac;
	}
	/**
	 * @return reqName를(을) 리턴합니다.
	 */
	public String getReqName() {
		return reqName;
	}
	/**
	 * @return reqPhone를(을) 리턴합니다.
	 */
	public String getReqPhone() {
		return reqPhone;
	}
	/**
	 * @return reqType를(을) 리턴합니다.
	 */
	public String getReqType() {
		return reqType;
	}
	/**
	 * @return reqTypeName를(을) 리턴합니다.
	 */
	public String getReqTypeName() {
		return reqTypeName;
	}
	/**
	 * @return useAccept를(을) 리턴합니다.
	 */
	public String getUseAccept() {
		return useAccept;
	}
	/**
	 * @return useAcceptName를(을) 리턴합니다.
	 */
	public String getUseAcceptName() {
		return useAcceptName;
	}
	/**
	 * @return vstDate를(을) 리턴합니다.
	 */
	public String getVstDate() {
		return vstDate;
	}
	/**
	 * @return vstDesc를(을) 리턴합니다.
	 */
	public String getVstDesc() {
		return vstDesc;
	}
	/**
	 * @return vstDescBR를(을) 리턴합니다.
	 */
	public String getVstDescBR() {
		return vstDescBR;
	}
	/**
	 * @return vstDescCR를(을) 리턴합니다.
	 */
	public String getVstDescCR() {
		return vstDescCR;
	}
	/**
	 * @return vstPerson를(을) 리턴합니다.
	 */
	public String getVstPerson() {
		return vstPerson;
	}
	/**
	 * @return vstTime를(을) 리턴합니다.
	 */
	public String getVstTime() {
		return vstTime;
	}
	/**
	 * @return vstTimeH를(을) 리턴합니다.
	 */
	public String getVstTimeH() {
		return vstTimeH;
	}
	/**
	 * @return vstTimeM를(을) 리턴합니다.
	 */
	public String getVstTimeM() {
		return vstTimeM;
	}
	/**
	 * @return vstType를(을) 리턴합니다.
	 */
	public String getVstType() {
		return vstType;
	}
	/**
	 * @return vstTypeName를(을) 리턴합니다.
	 */
	public String getVstTypeName() {
		return vstTypeName;
	}
	/**
	 * @param creIPv4 설정하려는 creIPv4.
	 */
	public void setCreIPv4(String creIPv4) {
		this.creIPv4 = creIPv4;
	}
	/**
	 * @param creIPv6 설정하려는 creIPv6.
	 */
	public void setCreIPv6(String creIPv6) {
		this.creIPv6 = creIPv6;
	}
	/**
	 * @param creMac 설정하려는 creMac.
	 */
	public void setCreMac(String creMac) {
		this.creMac = creMac;
	}
	/**
	 * @param delIPv4 설정하려는 delIPv4.
	 */
	public void setDelIPv4(String delIPv4) {
		this.delIPv4 = delIPv4;
	}
	/**
	 * @param delIPv6 설정하려는 delIPv6.
	 */
	public void setDelIPv6(String delIPv6) {
		this.delIPv6 = delIPv6;
	}
	/**
	 * @param delMac 설정하려는 delMac.
	 */
	public void setDelMac(String delMac) {
		this.delMac = delMac;
	}
	/**
	 * @param downAbs 설정하려는 downAbs.
	 */
	public void setDownAbs(String downAbs) {
		this.downAbs = downAbs;
	}
	/**
	 * @param downExt 설정하려는 downExt.
	 */
	public void setDownExt(String downExt) {
		this.downExt = downExt;
	}
	/**
	 * @param downRel 설정하려는 downRel.
	 */
	public void setDownRel(String downRel) {
		this.downRel = downRel;
	}
	/**
	 * @param downUUID 설정하려는 downUUID.
	 */
	public void setDownUUID(String downUUID) {
		this.downUUID = downUUID;
	}
	/**
	 * @param fileAbs 설정하려는 fileAbs.
	 */
	public void setFileAbs(String fileAbs) {
		this.fileAbs = fileAbs;
	}
	/**
	 * @param fileLinkD 설정하려는 fileLinkD.
	 */
	public void setFileLinkD(String fileLinkD) {
		this.fileLinkD = fileLinkD;
	}
	/**
	 * @param fileLinkM 설정하려는 fileLinkM.
	 */
	public void setFileLinkM(String fileLinkM) {
		this.fileLinkM = fileLinkM;
	}
	/**
	 * @param fileLinkS 설정하려는 fileLinkS.
	 */
	public void setFileLinkS(String fileLinkS) {
		this.fileLinkS = fileLinkS;
	}
	/**
	 * @param fileName 설정하려는 fileName.
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @param fileRel 설정하려는 fileRel.
	 */
	public void setFileRel(String fileRel) {
		this.fileRel = fileRel;
	}
	/**
	 * @param fileSize 설정하려는 fileSize.
	 */
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	/**
	 * @param fileUri 설정하려는 fileUri.
	 */
	public void setFileUri(String fileUri) {
		this.fileUri = fileUri;
	}
	/**
	 * @param modIPv4 설정하려는 modIPv4.
	 */
	public void setModIPv4(String modIPv4) {
		this.modIPv4 = modIPv4;
	}
	/**
	 * @param modIPv6 설정하려는 modIPv6.
	 */
	public void setModIPv6(String modIPv6) {
		this.modIPv6 = modIPv6;
	}
	/**
	 * @param modMac 설정하려는 modMac.
	 */
	public void setModMac(String modMac) {
		this.modMac = modMac;
	}
	/**
	 * @param reqName 설정하려는 reqName.
	 */
	public void setReqName(String reqName) {
		this.reqName = reqName;
	}
	/**
	 * @param reqPhone 설정하려는 reqPhone.
	 */
	public void setReqPhone(String reqPhone) {
		this.reqPhone = reqPhone;
	}
	/**
	 * @param reqType 설정하려는 reqType.
	 */
	public void setReqType(String reqType) {
		this.reqType = reqType;
	}
	/**
	 * @param reqTypeName 설정하려는 reqTypeName.
	 */
	public void setReqTypeName(String reqTypeName) {
		this.reqTypeName = reqTypeName;
	}
	/**
	 * @param useAccept 설정하려는 useAccept.
	 */
	public void setUseAccept(String useAccept) {
		this.useAccept = useAccept;
	}
	/**
	 * @param useAcceptName 설정하려는 useAcceptName.
	 */
	public void setUseAcceptName(String useAcceptName) {
		this.useAcceptName = useAcceptName;
	}
	/**
	 * @param vstDate 설정하려는 vstDate.
	 */
	public void setVstDate(String vstDate) {
		this.vstDate = vstDate;
	}
	/**
	 * @param vstDesc 설정하려는 vstDesc.
	 */
	public void setVstDesc(String vstDesc) {
		this.vstDesc = vstDesc;
	}
	/**
	 * @param vstDescBR 설정하려는 vstDescBR.
	 */
	public void setVstDescBR(String vstDescBR) {
		this.vstDescBR = vstDescBR;
	}
	/**
	 * @param vstDescCR 설정하려는 vstDescCR.
	 */
	public void setVstDescCR(String vstDescCR) {
		this.vstDescCR = vstDescCR;
	}
	/**
	 * @param vstPerson 설정하려는 vstPerson.
	 */
	public void setVstPerson(String vstPerson) {
		this.vstPerson = vstPerson;
	}
	/**
	 * @param vstTime 설정하려는 vstTime.
	 */
	public void setVstTime(String vstTime) {
		this.vstTime = vstTime;
	}
	/**
	 * @param vstTimeH 설정하려는 vstTimeH.
	 */
	public void setVstTimeH(String vstTimeH) {
		this.vstTimeH = vstTimeH;
	}
	/**
	 * @param vstTimeM 설정하려는 vstTimeM.
	 */
	public void setVstTimeM(String vstTimeM) {
		this.vstTimeM = vstTimeM;
	}
	/**
	 * @param vstType 설정하려는 vstType.
	 */
	public void setVstType(String vstType) {
		this.vstType = vstType;
	}
	/**
	 * @param vstTypeName 설정하려는 vstTypeName.
	 */
	public void setVstTypeName(String vstTypeName) {
		this.vstTypeName = vstTypeName;
	}

}	// end of class