/*******************************************************************************
  Program ID  : BaseFilesVO
  Description : 통합파일 정보에 대한 Value Object를 정의한다
  Author      : (주)엠씨에스텍 오영덕
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    (주)엠씨에스텍 오영덕  2019.07.01  최초 생성
  v1.0    (주)엠씨에스텍 오영덕  2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.cmmn.vo;

import java.util.List;

@SuppressWarnings("serial")
public class BaseLiveVO extends BaseCommonVO implements java.io.Serializable {

	private String urlMain;
	private String urlOper;
	private String urlGvadm;
	private String urlWelfare;
	private String urlBuild;

	public String getUrlMain() { return urlMain; }
	public String getUrlOper() { return urlOper; }
	public String getUrlGvadm() { return urlGvadm; }
	public String getUrlWelfare() {	return urlWelfare; }
	public String getUrlBuild() { return urlBuild; }

	public void setUrlMain(String urlMain) {
		this.urlMain = urlMain;
	}
	public void setUrlOper(String urlOper) {
		this.urlOper = urlOper;
	}
	public void setUrlGvadm(String urlGvadm) {
		this.urlGvadm = urlGvadm;
	}
	public void setUrlWelfare(String urlWelfare) {
		this.urlWelfare = urlWelfare;
	}
	public void setUrlBuild(String urlBuild) {
		this.urlBuild = urlBuild;
	}

}	// end of class