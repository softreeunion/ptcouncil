/*******************************************************************************
  Program ID  : BaseCommonVO
  Description : 공통 정보에 대한 Value Object를 정의한다
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v1.0    (주)헥사미디어 개발자  2014.00.00  최초 생성
  v2.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
  v2.1    HexaMedia new0man      2020.05.08  대수별 기간 최소최대 선택일지정
 *******************************************************************************/
package egovframework.ptcc.cmmn.vo;

import java.util.List;

import egovframework.com.cmm.util.BaseUtility;

@SuppressWarnings("serial")
public class BaseCommonVO implements java.io.Serializable { 

	// 기본조회 조건
	private String rowIdx;						// 조회 순번
	private String rowSeq;						// 조회 순번
	private String rowXls;						// 조회 순번
	private String rowCnt;						// 조회 건수
	private String rowRank;					// 조회 순위
	private String rowPrev;					// 이전 데이터
	private String rowNext;					// 다음 데이터
	private String rowUUID;					// 조회 관리번호
	private String listCnt;					// 조회 데이터수
	private String pageCnt;					// 조회 페이지수
	private String pagePerLn;					// 페이지당 조회건수
	private String pagePerNo;					// 페이지당 조회건수
	private String pageTotNo;					// 전체 페이지 번호
	private String pageCurNo;					// 현재 페이지 번호
	private String pageFirNo;					// 현재블럭 처음페이지 번호
	private String pageLstNo;					// 현재블럭 마지막페이지 번호
	private String pagePerLnP;					// 페이지당 조회건수
	private String pagePerNoP;					// 페이지당 조회건수
	private String pageTotNoP;					// 전체 페이지 번호
	private String pageCurNoP;					// 현재 페이지 번호
	private String pageFirNoP;					// 현재블럭 처음페이지 번호
	private String pageLstNoP;					// 현재블럭 마지막페이지 번호
	private String ordMode;					// 정렬 구분
	private String firDate;					// 최초 사용일자
	private String pubType;					// 최신글 기준일자
	private String gabDay;						// 최신글 처리일수
	private String gabSpace;					// 답변글 공백건수
	private String condCate;					// 조회 구분
	private String condType;					// 조회 조건
	private String condValue;					// 조회 값
	private String condName;					// 조회 값
	private String condDele;					// 조회 값
	private String condClone;					// 
	private String viewNo;						// 조회 관리번호
	private String actMode;					// 처리 방법
	private String nextMode;					// 
	private String errFlag;					// 오류 상태
	private String errDesc;					// 오류 내역
	private String fontColor;					// 
	private String fontName;					// 
	private String fontSize;					// 
	private String rultCode;					// 
	private String rultName;					// 
	private String rultMsg;					// 

	// 추가조회 조건
	private List<String> bbsTypes;				// 
	private List<String> rndCodes;				// 
	private String listType;					// 
	private String ssnUUID;					// 
	private String nextUUID;					// 
	private String lnkName;					// 연계 명칭
	private String lnkKey;						// 연계 번호
	private String lnkUUID;					// 
	private String fileSeq;					// 파일 순번
	private String fileUUID;					// 
	private String autType;					// 
	private String autSeq;						//
	private String autUUID;					// 
	private String defFull;					// 
	private String defUUID;					// 
	private String rlyType;					// 
	private String rlySeq;						// 
	private String rlyUUID;					// 
	private String bbsType;					// 
	private String bbsSeq;						// 
	private String bbsUUID;					// 
	private String mngType;					// 
	private String mngSeq;						// 
	private String mngUUID;					// 
	private String mngDate;					// 
	private String askType;					// 
	private String askSeq;						// 
	private String askUUID;					// 
	private String askYear;					// 
	private String ansType;					// 
	private String ansSeq;						// 
	private String ansUUID;					// 
	private String confType;					// 
	private String confSeq;					// 
	private String confUUID;					// 
	private String confYear;					// 
	private String sugType;					// 
	private String sugSeq;						// 
	private String sugUUID;					// 
	private String sugYear;					// 
	private String appType;					// 
	private String appSeq;						// 
	private String appUUID;					// 
	private String memID;						// 
	private String memUUID;					// 
	private String vsnSeq;						// 
	private String vsnUUID;					// 
	private String vsnYear;					// 
	private String reqDate;					// 
	private String reqSeq;						// 
	private String reqUUID;					// 
	private String schdYear;					// 
	private String schdSeq;					// 
	private String schdUUID;					// 
	private String reLstType;					// 
	private String reBbsCate;					// 
	private String reStdStat;					// 
	private String reStdYear;					// 
	private String reStdMonth;					// 
	private String reStdDate;					// 
	private String reStdWeek;					// 
	private String reUseStat;					// 
	private String rePrevYear;					// 
	private String rePrevMonth;				// 
	private String rePrevDate;					// 
	private String rePrevWeek;					// 
	private String reNextYear;					// 
	private String reNextMonth;				// 
	private String reNextDate;					// 
	private String reNextWeek;					// 
	private String reCurrYear;					// 
	private String reCurrMonth;				// 
	private String reCurrDate;					// 
	private String reCurrWeek;					// 
	private String reBgnDate;					// 
	private String reEndDate;					// 
	private String reSndDate;					// 
	private String reBgnTime;					// 
	private String reEndTime;					// 
	private String reAskAddr;					// 
	private String reCreHide;					// 
	private String reCreMobi;					// 
	private String reSiteUse;					// 
	private String reAuthRole;					// 
	private String reAuthFlag;					// 
	private String rePass;						// 
	private String reConfType;					// 
	private String reEraCode;					// 
	private String reRegCode;					// 
	private String reKndCode;					// 
	private String reKndType;					// 
	private String reSugCode;					// 
	private String reResCode;					// 
	private String reComCode;					// 
	private String reInsCode;					// 
	private String rePntCode;					// 
	private String reRndCode;					// 
	private String reRndUsed;					// 
	private String reRndDiv;					// 
	private String reMemID;					// 
	private String reEraCodes;					// 
	private String reRndCodes;					// 
	private String searchType;					// 
	private String searchText;					// 
	private String searchRegi;					// 

	// 공통 기본정보
	private String modeID;						// 
	private String bgnDate;					// 
	private String endDate;					// 
	private String bgnTime;					// 
	private String endTime;					// 
	private String minYear;					// 
	private String maxYear;					// 
	private String minDate;					// 
	private String maxDate;					// 
	private String stdYear;					// 
	private String stdMonth;					// 
	private String stdDate;					// 
	private String stdWeek;					// 
	private String stdDay;						// 
	private String stdTime;					// 
	private String stdUUID;					// 
	private String stdStat;					// 
	private String userLog;					// 
	private String userSeq;					// 
	private String userUUID;					// 
	private String userPID;					// 
	private String userPWD;					// 
	private String userType;					// 
	private String userName;					// 
	private String userMail;					// 
	private String userDesc;					// 
	private String userIPv4;					// 
	private String userIPv6;					// 
	private String userMac;					// 
	private String authRole;					// 
	private String authFlag;					//
	private String pwdDateLst;					// 
	private String pwdDateUse;					// 
	private String pwdDateTerm;				// 
	private String pwdDateMod;					// 
	private String pwdDateErr;					// 
	private String pwdDateGet;					// 
	private String useDele;					// 
	private String useSync;					// 
	private String useFlag;					// 
	private String creLock;					// 
	private String creType;					// 
	private String creDate;					// 
	private String creUser;					// 
	private String creCert;					// 
	private String modDate;					// 
	private String modUser;					// 
	private String delDate;					// 
	private String delUser;					// 

	private String userCorpTele;				// 
	private String userHomeTele;				// 
	private String userMobiTele;				// 
	private String authRoleName;				// 
	private String authFlagName;				// 
	private String userTypeName;				// 
	private String useDeleName;				// 
	private String useSyncName;				// 
	private String useFlagName;				// 
	private String creTypeName;				// 
	private String creName;					// 
	private String modName;					// 
	private String delName;					// 
	private String seleMailA;					// 
	private String seleMailT;					// 

	// 권한구분 정보
	private String autName;					// 
	private String autDesc;					// 
	private String autOrdr;					// 
	private String aucType;					// 

	// 업무정의 정보
	private String defName;					// 
	private String defPath;					// 
	private String defDesc;					// 
	private String defLink;					// 
	private String defRole;					// 
	private String defOrdr;					// 

	private String defCreate;					// 
	private String defRead;					// 
	private String defUpdate;					// 
	private String defDelete;					// 
	private String defAccess;					// 
	private String defModify;					// 

	// 이력 정보
	private String hstDate;					// 
	private String hstSeq;						// 
	private String hstUser;					// 

	// 인증 정보
	private String certName;					// 
	private String certBirth;					// 
	private String certIdx;					// 
	private String certDate;					// 
	private String certTime;					// 
	private String certCPReq;					// 
	private String certType;					// 
	private String certCI;						// 
	private String certDI;						// 
	private String certGender;					// 
	private String certNation;					// 
	private String certInfo;					// 
	private String certUse;					// 

	// 분류 정보
	private String codFull;					// 
	private String codLrg;						// 
	private String codMid;						// 
	private String codSma;						// 
	private String codName;					// 
	private String codDesc;					// 
	private String codOrdr;					// 
	private String stateName;					// 
	private String memName;					// 

	// 분류 정보
	private String eraCode;					// 
	private String eraName;					// 
	private String roundCode;					// 
	private String roundName;					// 
	private String rndivName;					// 
	private String roundKind;					// 
	private String kindParent;					// 
	private String kindCode;					// 
	private String kindName;					// 
	private String comCode;					// 
	private String comName;					// 
	private String regCode;					// 
	private String regName;					// 
	private String resCode;					// 
	private String resName;					// 
	private String pointCode;					// 
	private String pointName;					// 
	private String insCode;					// 
	private String insName;					// 
	private String confCode;					// 
	private String confName;					// 
	private String sugCode;					// 
	private String sugName;					// 
	private String sugNum;						// 

	// 일자 정보
	private String toDate;						// 시스템 기준일자
	private String toTime;						// 시스템 기준시간
	private String toYear;						// 시스템 기준 년
	private String toMonth;					// 시스템 기준 월
	private String toDay;						// 시스템 기준 일
	private String toDateDP7;					// 시스템 기준 일(7일전)
	private String toDateDN7;					// 시스템 기준 일(7일후)
	private String toDateDP1;					// 시스템 기준 일(1일전)
	private String toDateMP1;					// 시스템 기준 일(1개월전)
	private String toDateMN1;					// 시스템 기준 일(1개월후)
	private String toDateMN3;					// 시스템 기준 일(3개월후)
	private String toDateMN5;					// 시스템 기준 일(5개월후)
	private String toWeek;						// 시스템 기준 일(해당월 주차)

	private String type = "";					// 
	private int pageSize = 10;					// 
	private String nowPage = "1";				// 
	private String round_div = "";				// 
	private String searchText1 = "";			// 
	private String searchText2 = "";			//

	private String bgnScDate;
	private String endScDate;
	private String creNoti;

//	private String eraCode = "";				// 
//	private String memId = "";					// 
//	private String memName = "";				// 
//	private String regName = "";				// 
//	private String hNum1 = "";					// 
//	private String hNum2 = "";					// 
//	private String cNum1 = "";					// 
//	private String cNum2 = "";					// 
//	private String comp = "Or";				// 
//	private String eraAll = "";				// 
//	private String questionSearchText = "";	// 
//
//	private String billResCode;				// 
//	private String billComCode;				// 
//	private String billSugCode;				// 
//	private String billKindCode;				// 
//
//	private String dic_index = "가";			// 
//	private String dic_word = "";				// 
//

	/**
	 * @return actMode를(을) 리턴합니다.
	 */
	public String getActMode() {
		return BaseUtility.null2Blank( actMode );
	}
	/**
	 * @return ansSeq를(을) 리턴합니다.
	 */
	public String getAnsSeq() {
		return BaseUtility.null2Blank( ansSeq );
	}
	/**
	 * @return ansType를(을) 리턴합니다.
	 */
	public String getAnsType() {
		return BaseUtility.null2Blank( ansType );
	}
	/**
	 * @return ansUUID를(을) 리턴합니다.
	 */
	public String getAnsUUID() {
		return BaseUtility.null2Blank( ansUUID );
	}
	/**
	 * @return appSeq를(을) 리턴합니다.
	 */
	public String getAppSeq() {
		return BaseUtility.null2Blank( appSeq );
	}
	/**
	 * @return appType를(을) 리턴합니다.
	 */
	public String getAppType() {
		return BaseUtility.null2Blank( appType );
	}
	/**
	 * @return appUUID를(을) 리턴합니다.
	 */
	public String getAppUUID() {
		return BaseUtility.null2Blank( appUUID );
	}
	/**
	 * @return askSeq를(을) 리턴합니다.
	 */
	public String getAskSeq() {
		return BaseUtility.null2Blank( askSeq );
	}
	/**
	 * @return askType를(을) 리턴합니다.
	 */
	public String getAskType() {
		return BaseUtility.null2Blank( askType );
	}
	/**
	 * @return askUUID를(을) 리턴합니다.
	 */
	public String getAskUUID() {
		return BaseUtility.null2Blank( askUUID );
	}
	/**
	 * @return askYear를(을) 리턴합니다.
	 */
	public String getAskYear() {
		return BaseUtility.null2Blank( askYear );
	}
	/**
	 * @return aucType를(을) 리턴합니다.
	 */
	public String getAucType() {
		return BaseUtility.null2Blank( aucType );
	}
	/**
	 * @return autDesc를(을) 리턴합니다.
	 */
	public String getAutDesc() {
		return BaseUtility.null2Blank( autDesc );
	}
	/**
	 * @return authFlag를(을) 리턴합니다.
	 */
	public String getAuthFlag() {
		return BaseUtility.null2Blank( authFlag );
	}
	/**
	 * @return authFlagName를(을) 리턴합니다.
	 */
	public String getAuthFlagName() {
		return BaseUtility.null2Blank( authFlagName );
	}
	/**
	 * @return authRole를(을) 리턴합니다.
	 */
	public String getAuthRole() {
		return BaseUtility.null2Blank( authRole );
	}
	/**
	 * @return authRoleName를(을) 리턴합니다.
	 */
	public String getAuthRoleName() {
		return BaseUtility.null2Blank( authRoleName );
	}
	/**
	 * @return autName를(을) 리턴합니다.
	 */
	public String getAutName() {
		return BaseUtility.null2Blank( autName );
	}
	/**
	 * @return autOrdr를(을) 리턴합니다.
	 */
	public String getAutOrdr() {
		return BaseUtility.null2Blank( autOrdr );
	}
	/**
	 * @return autSeq를(을) 리턴합니다.
	 */
	public String getAutSeq() {
		return BaseUtility.null2Blank( autSeq );
	}
	/**
	 * @return autType를(을) 리턴합니다.
	 */
	public String getAutType() {
		return BaseUtility.null2Blank( autType );
	}
	/**
	 * @return autUUID를(을) 리턴합니다.
	 */
	public String getAutUUID() {
		return BaseUtility.null2Blank( autUUID );
	}
	/**
	 * @return bbsSeq를(을) 리턴합니다.
	 */
	public String getBbsSeq() {
		return BaseUtility.null2Blank( bbsSeq );
	}
	/**
	 * @return bbsType를(을) 리턴합니다.
	 */
	public String getBbsType() {
		return BaseUtility.null2Blank( bbsType );
	}
	/**
	 * @return bbsTypes를(을) 리턴합니다.
	 */
	public List<String> getBbsTypes() {
		return bbsTypes;
	}
	/**
	 * @return bbsUUID를(을) 리턴합니다.
	 */
	public String getBbsUUID() {
		return BaseUtility.null2Blank( bbsUUID );
	}
	/**
	 * @return bgnDate를(을) 리턴합니다.
	 */
	public String getBgnDate() {
		return BaseUtility.null2Blank( bgnDate );
	}
	/**
	 * @return bgnTime를(을) 리턴합니다.
	 */
	public String getBgnTime() {
		return BaseUtility.null2Blank( bgnTime );
	}
	/**
	 * @return certBirth를(을) 리턴합니다.
	 */
	public String getCertBirth() {
		return BaseUtility.null2Blank( certBirth );
	}
	/**
	 * @return certCI를(을) 리턴합니다.
	 */
	public String getCertCI() {
		return BaseUtility.null2Blank( certCI );
	}
	/**
	 * @return certCPReq를(을) 리턴합니다.
	 */
	public String getCertCPReq() {
		return BaseUtility.null2Blank( certCPReq );
	}
	/**
	 * @return certDate를(을) 리턴합니다.
	 */
	public String getCertDate() {
		return BaseUtility.null2Blank( certDate );
	}
	/**
	 * @return certDI를(을) 리턴합니다.
	 */
	public String getCertDI() {
		return BaseUtility.null2Blank( certDI );
	}
	/**
	 * @return certGender를(을) 리턴합니다.
	 */
	public String getCertGender() {
		return BaseUtility.null2Blank( certGender );
	}
	/**
	 * @return certIdx를(을) 리턴합니다.
	 */
	public String getCertIdx() {
		return BaseUtility.null2Blank( certIdx );
	}
	/**
	 * @return certInfo를(을) 리턴합니다.
	 */
	public String getCertInfo() {
		return BaseUtility.null2Blank( certInfo );
	}
	/**
	 * @return certName를(을) 리턴합니다.
	 */
	public String getCertName() {
		return BaseUtility.null2Blank( certName );
	}
	/**
	 * @return certNation를(을) 리턴합니다.
	 */
	public String getCertNation() {
		return BaseUtility.null2Blank( certNation );
	}
	/**
	 * @return certTime를(을) 리턴합니다.
	 */
	public String getCertTime() {
		return BaseUtility.null2Blank( certTime );
	}
	/**
	 * @return certType를(을) 리턴합니다.
	 */
	public String getCertType() {
		return BaseUtility.null2Blank( certType );
	}
	/**
	 * @return certUse를(을) 리턴합니다.
	 */
	public String getCertUse() {
		return BaseUtility.null2Blank( certUse );
	}
	/**
	 * @return codDesc를(을) 리턴합니다.
	 */
	public String getCodDesc() {
		return BaseUtility.null2Blank( codDesc );
	}
	/**
	 * @return codFull를(을) 리턴합니다.
	 */
	public String getCodFull() {
		return BaseUtility.null2Blank( codFull );
	}
	/**
	 * @return codLrg를(을) 리턴합니다.
	 */
	public String getCodLrg() {
		return BaseUtility.null2Blank( codLrg );
	}
	/**
	 * @return codMid를(을) 리턴합니다.
	 */
	public String getCodMid() {
		return BaseUtility.null2Blank( codMid );
	}
	/**
	 * @return codName를(을) 리턴합니다.
	 */
	public String getCodName() {
		return BaseUtility.null2Blank( codName );
	}
	/**
	 * @return codOrdr를(을) 리턴합니다.
	 */
	public String getCodOrdr() {
		return BaseUtility.null2Blank( codOrdr );
	}
	/**
	 * @return codSma를(을) 리턴합니다.
	 */
	public String getCodSma() {
		return BaseUtility.null2Blank( codSma );
	}
	/**
	 * @return comCode를(을) 리턴합니다.
	 */
	public String getComCode() {
		return BaseUtility.null2Blank( comCode );
	}
	/**
	 * @return comName를(을) 리턴합니다.
	 */
	public String getComName() {
		return BaseUtility.null2Blank( comName );
	}
	/**
	 * @return condCate를(을) 리턴합니다.
	 */
	public String getCondCate() {
		return BaseUtility.null2Blank( condCate );
	}
	/**
	 * @return condClone를(을) 리턴합니다.
	 */
	public String getCondClone() {
		return BaseUtility.null2Blank( condClone );
	}
	/**
	 * @return condDele를(을) 리턴합니다.
	 */
	public String getCondDele() {
		return BaseUtility.null2Blank( condDele );
	}
	/**
	 * @return condName를(을) 리턴합니다.
	 */
	public String getCondName() {
		return BaseUtility.null2Blank( condName );
	}
	/**
	 * @return condType를(을) 리턴합니다.
	 */
	public String getCondType() {
		return BaseUtility.null2Blank( condType );
	}
	/**
	 * @return condValue를(을) 리턴합니다.
	 */
	public String getCondValue() {
		return BaseUtility.null2Blank( condValue );
	}
	/**
	 * @return confCode를(을) 리턴합니다.
	 */
	public String getConfCode() {
		return BaseUtility.null2Blank( confCode );
	}
	/**
	 * @return confName를(을) 리턴합니다.
	 */
	public String getConfName() {
		return BaseUtility.null2Blank( confName );
	}
	/**
	 * @return confSeq를(을) 리턴합니다.
	 */
	public String getConfSeq() {
		return BaseUtility.null2Blank( confSeq );
	}
	/**
	 * @return confType를(을) 리턴합니다.
	 */
	public String getConfType() {
		return BaseUtility.null2Blank( confType );
	}
	/**
	 * @return confUUID를(을) 리턴합니다.
	 */
	public String getConfUUID() {
		return BaseUtility.null2Blank( confUUID );
	}
	/**
	 * @return confYear를(을) 리턴합니다.
	 */
	public String getConfYear() {
		return BaseUtility.null2Blank( confYear );
	}
	/**
	 * @return creCert를(을) 리턴합니다.
	 */
	public String getCreCert() {
		return BaseUtility.null2Blank( creCert );
	}
	/**
	 * @return creDate를(을) 리턴합니다.
	 */
	public String getCreDate() {
		return BaseUtility.null2Blank( creDate );
	}
	/**
	 * @return creLock를(을) 리턴합니다.
	 */
	public String getCreLock() {
		return BaseUtility.null2Blank( creLock );
	}
	/**
	 * @return creName를(을) 리턴합니다.
	 */
	public String getCreName() {
		return BaseUtility.null2Blank( creName );
	}
	/**
	 * @return creType를(을) 리턴합니다.
	 */
	public String getCreType() {
		return BaseUtility.null2Blank( creType );
	}
	/**
	 * @return creTypeName를(을) 리턴합니다.
	 */
	public String getCreTypeName() {
		return BaseUtility.null2Blank( creTypeName );
	}
	/**
	 * @return creUser를(을) 리턴합니다.
	 */
	public String getCreUser() {
		return BaseUtility.null2Blank( creUser );
	}
	/**
	 * @return defAccess를(을) 리턴합니다.
	 */
	public String getDefAccess() {
		return BaseUtility.null2Blank( defAccess );
	}
	/**
	 * @return defCreate를(을) 리턴합니다.
	 */
	public String getDefCreate() {
		return BaseUtility.null2Blank( defCreate );
	}
	/**
	 * @return defDelete를(을) 리턴합니다.
	 */
	public String getDefDelete() {
		return BaseUtility.null2Blank( defDelete );
	}
	/**
	 * @return defDesc를(을) 리턴합니다.
	 */
	public String getDefDesc() {
		return BaseUtility.null2Blank( defDesc );
	}
	/**
	 * @return defFull를(을) 리턴합니다.
	 */
	public String getDefFull() {
		return BaseUtility.null2Blank( defFull );
	}
	/**
	 * @return defLink를(을) 리턴합니다.
	 */
	public String getDefLink() {
		return BaseUtility.null2Blank( defLink );
	}
	/**
	 * @return defModify를(을) 리턴합니다.
	 */
	public String getDefModify() {
		return BaseUtility.null2Blank( defModify );
	}
	/**
	 * @return defName를(을) 리턴합니다.
	 */
	public String getDefName() {
		return BaseUtility.null2Blank( defName );
	}
	/**
	 * @return defOrdr를(을) 리턴합니다.
	 */
	public String getDefOrdr() {
		return BaseUtility.null2Blank( defOrdr );
	}
	/**
	 * @return defPath를(을) 리턴합니다.
	 */
	public String getDefPath() {
		return BaseUtility.null2Blank( defPath );
	}
	/**
	 * @return defRead를(을) 리턴합니다.
	 */
	public String getDefRead() {
		return BaseUtility.null2Blank( defRead );
	}
	/**
	 * @return defRole를(을) 리턴합니다.
	 */
	public String getDefRole() {
		return BaseUtility.null2Blank( defRole );
	}
	/**
	 * @return defUpdate를(을) 리턴합니다.
	 */
	public String getDefUpdate() {
		return BaseUtility.null2Blank( defUpdate );
	}
	/**
	 * @return defUUID를(을) 리턴합니다.
	 */
	public String getDefUUID() {
		return BaseUtility.null2Blank( defUUID );
	}
	/**
	 * @return delDate를(을) 리턴합니다.
	 */
	public String getDelDate() {
		return BaseUtility.null2Blank( delDate );
	}
	/**
	 * @return delName를(을) 리턴합니다.
	 */
	public String getDelName() {
		return BaseUtility.null2Blank( delName );
	}
	/**
	 * @return delUser를(을) 리턴합니다.
	 */
	public String getDelUser() {
		return BaseUtility.null2Blank( delUser );
	}
	/**
	 * @return endDate를(을) 리턴합니다.
	 */
	public String getEndDate() {
		return BaseUtility.null2Blank( endDate );
	}
	/**
	 * @return endTime를(을) 리턴합니다.
	 */
	public String getEndTime() {
		return BaseUtility.null2Blank( endTime );
	}
	/**
	 * @return eraCode를(을) 리턴합니다.
	 */
	public String getEraCode() {
		return BaseUtility.null2Blank( eraCode );
	}
	/**
	 * @return eraName를(을) 리턴합니다.
	 */
	public String getEraName() {
		return BaseUtility.null2Blank( eraName );
	}
	/**
	 * @return errDesc를(을) 리턴합니다.
	 */
	public String getErrDesc() {
		return BaseUtility.null2Blank( errDesc );
	}
	/**
	 * @return errFlag를(을) 리턴합니다.
	 */
	public String getErrFlag() {
		return BaseUtility.null2Blank( errFlag );
	}
	/**
	 * @return fileSeq를(을) 리턴합니다.
	 */
	public String getFileSeq() {
		return BaseUtility.null2Blank( fileSeq );
	}
	/**
	 * @return fileUUID를(을) 리턴합니다.
	 */
	public String getFileUUID() {
		return BaseUtility.null2Blank( fileUUID );
	}
	/**
	 * @return firDate를(을) 리턴합니다.
	 */
	public String getFirDate() {
		return BaseUtility.null2Blank( firDate );
	}
	/**
	 * @return fontColor를(을) 리턴합니다.
	 */
	public String getFontColor() {
		return BaseUtility.null2Blank( fontColor );
	}
	/**
	 * @return fontName를(을) 리턴합니다.
	 */
	public String getFontName() {
		return BaseUtility.null2Blank( fontName );
	}
	/**
	 * @return fontSize를(을) 리턴합니다.
	 */
	public String getFontSize() {
		return BaseUtility.null2Blank( fontSize );
	}
	/**
	 * @return gabDay를(을) 리턴합니다.
	 */
	public String getGabDay() {
		return BaseUtility.null2Blank( gabDay );
	}
	/**
	 * @return gabSpace를(을) 리턴합니다.
	 */
	public String getGabSpace() {
		return BaseUtility.null2Blank( gabSpace );
	}
	/**
	 * @return hstDate를(을) 리턴합니다.
	 */
	public String getHstDate() {
		return BaseUtility.null2Blank( hstDate );
	}
	/**
	 * @return hstSeq를(을) 리턴합니다.
	 */
	public String getHstSeq() {
		return BaseUtility.null2Blank( hstSeq );
	}
	/**
	 * @return hstUser를(을) 리턴합니다.
	 */
	public String getHstUser() {
		return BaseUtility.null2Blank( hstUser );
	}
	/**
	 * @return insCode를(을) 리턴합니다.
	 */
	public String getInsCode() {
		return BaseUtility.null2Blank( insCode );
	}
	/**
	 * @return insName를(을) 리턴합니다.
	 */
	public String getInsName() {
		return BaseUtility.null2Blank( insName );
	}
	/**
	 * @return kindCode를(을) 리턴합니다.
	 */
	public String getKindCode() {
		return BaseUtility.null2Blank( kindCode );
	}
	/**
	 * @return kindName를(을) 리턴합니다.
	 */
	public String getKindName() {
		return BaseUtility.null2Blank( kindName );
	}
	/**
	 * @return kindParent를(을) 리턴합니다.
	 */
	public String getKindParent() {
		return BaseUtility.null2Blank( kindParent );
	}
	/**
	 * @return listCnt를(을) 리턴합니다.
	 */
	public String getListCnt() {
		return BaseUtility.null2Blank( listCnt );
	}
	/**
	 * @return listType를(을) 리턴합니다.
	 */
	public String getListType() {
		return BaseUtility.null2Blank( listType );
	}
	/**
	 * @return lnkKey를(을) 리턴합니다.
	 */
	public String getLnkKey() {
		return BaseUtility.null2Blank( lnkKey );
	}
	/**
	 * @return lnkName를(을) 리턴합니다.
	 */
	public String getLnkName() {
		return BaseUtility.null2Blank( lnkName );
	}
	/**
	 * @return lnkUUID를(을) 리턴합니다.
	 */
	public String getLnkUUID() {
		return BaseUtility.null2Blank( lnkUUID );
	}
	/**
	 * @return maxDate를(을) 리턴합니다.
	 */
	public String getMaxDate() {
		return BaseUtility.null2Blank( maxDate );
	}
	/**
	 * @return maxYear를(을) 리턴합니다.
	 */
	public String getMaxYear() {
		return BaseUtility.null2Blank( maxYear );
	}
	/**
	 * @return memID를(을) 리턴합니다.
	 */
	public String getMemID() {
		return BaseUtility.null2Blank( memID );
	}
	/**
	 * @return memName를(을) 리턴합니다.
	 */
	public String getMemName() {
		return BaseUtility.null2Blank( memName );
	}
	/**
	 * @return memUUID를(을) 리턴합니다.
	 */
	public String getMemUUID() {
		return BaseUtility.null2Blank( memUUID );
	}
	/**
	 * @return minDate를(을) 리턴합니다.
	 */
	public String getMinDate() {
		return BaseUtility.null2Blank( minDate );
	}
	/**
	 * @return minYear를(을) 리턴합니다.
	 */
	public String getMinYear() {
		return BaseUtility.null2Blank( minYear );
	}
	/**
	 * @return mngDate를(을) 리턴합니다.
	 */
	public String getMngDate() {
		return BaseUtility.null2Blank( mngDate );
	}
	/**
	 * @return mngSeq를(을) 리턴합니다.
	 */
	public String getMngSeq() {
		return BaseUtility.null2Blank( mngSeq );
	}
	/**
	 * @return mngType를(을) 리턴합니다.
	 */
	public String getMngType() {
		return BaseUtility.null2Blank( mngType );
	}
	/**
	 * @return mngUUID를(을) 리턴합니다.
	 */
	public String getMngUUID() {
		return BaseUtility.null2Blank( mngUUID );
	}
	/**
	 * @return modDate를(을) 리턴합니다.
	 */
	public String getModDate() {
		return BaseUtility.null2Blank( modDate );
	}
	/**
	 * @return modeID를(을) 리턴합니다.
	 */
	public String getModeID() {
		return BaseUtility.null2Blank( modeID );
	}
	/**
	 * @return modName를(을) 리턴합니다.
	 */
	public String getModName() {
		return BaseUtility.null2Blank( modName );
	}
	/**
	 * @return modUser를(을) 리턴합니다.
	 */
	public String getModUser() {
		return BaseUtility.null2Blank( modUser );
	}
	/**
	 * @return nextMode를(을) 리턴합니다.
	 */
	public String getNextMode() {
		return BaseUtility.null2Blank( nextMode );
	}
	/**
	 * @return nextUUID를(을) 리턴합니다.
	 */
	public String getNextUUID() {
		return BaseUtility.null2Blank( nextUUID );
	}
	/**
	 * @return ordMode를(을) 리턴합니다.
	 */
	public String getOrdMode() {
		return BaseUtility.null2Blank( ordMode );
	}
	/**
	 * @return pageCnt를(을) 리턴합니다.
	 */
	public String getPageCnt() {
		return BaseUtility.null2Blank( pageCnt );
	}
	/**
	 * @return pageCurNo를(을) 리턴합니다.
	 */
	public String getPageCurNo() {
		return BaseUtility.null2Blank( pageCurNo );
	}
	/**
	 * @return pageCurNoP를(을) 리턴합니다.
	 */
	public String getPageCurNoP() {
		return BaseUtility.null2Blank( pageCurNoP );
	}
	/**
	 * @return pageFirNo를(을) 리턴합니다.
	 */
	public String getPageFirNo() {
		return BaseUtility.null2Blank( pageFirNo );
	}
	/**
	 * @return pageFirNoP를(을) 리턴합니다.
	 */
	public String getPageFirNoP() {
		return BaseUtility.null2Blank( pageFirNoP );
	}
	/**
	 * @return pageLstNo를(을) 리턴합니다.
	 */
	public String getPageLstNo() {
		return BaseUtility.null2Blank( pageLstNo );
	}
	/**
	 * @return pageLstNoP를(을) 리턴합니다.
	 */
	public String getPageLstNoP() {
		return BaseUtility.null2Blank( pageLstNoP );
	}
	/**
	 * @return pagePerLn를(을) 리턴합니다.
	 */
	public String getPagePerLn() {
		return BaseUtility.null2Blank( pagePerLn );
	}
	/**
	 * @return pagePerLnP를(을) 리턴합니다.
	 */
	public String getPagePerLnP() {
		return BaseUtility.null2Blank( pagePerLnP );
	}
	/**
	 * @return pagePerNo를(을) 리턴합니다.
	 */
	public String getPagePerNo() {
		return BaseUtility.null2Blank( pagePerNo );
	}
	/**
	 * @return pagePerNoP를(을) 리턴합니다.
	 */
	public String getPagePerNoP() {
		return BaseUtility.null2Blank( pagePerNoP );
	}
	/**
	 * @return pageTotNo를(을) 리턴합니다.
	 */
	public String getPageTotNo() {
		return BaseUtility.null2Blank( pageTotNo );
	}
	/**
	 * @return pageTotNoP를(을) 리턴합니다.
	 */
	public String getPageTotNoP() {
		return BaseUtility.null2Blank( pageTotNoP );
	}
	/**
	 * @return pointCode를(을) 리턴합니다.
	 */
	public String getPointCode() {
		return BaseUtility.null2Blank( pointCode );
	}
	/**
	 * @return pointName를(을) 리턴합니다.
	 */
	public String getPointName() {
		return BaseUtility.null2Blank( pointName );
	}
	/**
	 * @return pubType를(을) 리턴합니다.
	 */
	public String getPubType() {
		return BaseUtility.null2Blank( pubType );
	}
	/**
	 * @return pwdDateErr를(을) 리턴합니다.
	 */
	public String getPwdDateErr() {
		return BaseUtility.null2Blank( pwdDateErr );
	}
	/**
	 * @return pwdDateGet를(을) 리턴합니다.
	 */
	public String getPwdDateGet() {
		return BaseUtility.null2Blank( pwdDateGet );
	}
	/**
	 * @return pwdDateLst를(을) 리턴합니다.
	 */
	public String getPwdDateLst() {
		return BaseUtility.null2Blank( pwdDateLst );
	}
	/**
	 * @return pwdDateMod를(을) 리턴합니다.
	 */
	public String getPwdDateMod() {
		return BaseUtility.null2Blank( pwdDateMod );
	}
	/**
	 * @return pwdDateTerm를(을) 리턴합니다.
	 */
	public String getPwdDateTerm() {
		return BaseUtility.null2Blank( pwdDateTerm );
	}
	/**
	 * @return pwdDateUse를(을) 리턴합니다.
	 */
	public String getPwdDateUse() {
		return BaseUtility.null2Blank( pwdDateUse );
	}
	/**
	 * @return reAskAddr를(을) 리턴합니다.
	 */
	public String getReAskAddr() {
		return BaseUtility.null2Blank( reAskAddr );
	}
	/**
	 * @return reAuthFlag를(을) 리턴합니다.
	 */
	public String getReAuthFlag() {
		return BaseUtility.null2Blank( reAuthFlag );
	}
	/**
	 * @return reAuthRole를(을) 리턴합니다.
	 */
	public String getReAuthRole() {
		return BaseUtility.null2Blank( reAuthRole );
	}
	/**
	 * @return reBbsCate를(을) 리턴합니다.
	 */
	public String getReBbsCate() {
		return BaseUtility.null2Blank( reBbsCate );
	}
	/**
	 * @return reBgnDate를(을) 리턴합니다.
	 */
	public String getReBgnDate() {
		return BaseUtility.null2Blank( reBgnDate );
	}
	/**
	 * @return reBgnTime를(을) 리턴합니다.
	 */
	public String getReBgnTime() {
		return BaseUtility.null2Blank( reBgnTime );
	}
	/**
	 * @return reCreHide를(을) 리턴합니다.
	 */
	public String getReCreHide() {
		return BaseUtility.null2Blank( reCreHide );
	}
	/**
	 * @return reCreMobi를(을) 리턴합니다.
	 */
	public String getReCreMobi() {
		return BaseUtility.null2Blank( reCreMobi );
	}
	/**
	 * @return reComCode를(을) 리턴합니다.
	 */
	public String getReComCode() {
		return BaseUtility.null2Blank( reComCode );
	}
	/**
	 * @return reConfType를(을) 리턴합니다.
	 */
	public String getReConfType() {
		return BaseUtility.null2Blank( reConfType );
	}
	/**
	 * @return reCurrDate를(을) 리턴합니다.
	 */
	public String getReCurrDate() {
		return BaseUtility.null2Blank( reCurrDate );
	}
	/**
	 * @return reCurrMonth를(을) 리턴합니다.
	 */
	public String getReCurrMonth() {
		return BaseUtility.null2Blank( reCurrMonth );
	}
	/**
	 * @return reCurrWeek를(을) 리턴합니다.
	 */
	public String getReCurrWeek() {
		return BaseUtility.null2Blank( reCurrWeek );
	}
	/**
	 * @return reCurrYear를(을) 리턴합니다.
	 */
	public String getReCurrYear() {
		return BaseUtility.null2Blank( reCurrYear );
	}
	/**
	 * @return reEndDate를(을) 리턴합니다.
	 */
	public String getReEndDate() {
		return BaseUtility.null2Blank( reEndDate );
	}
	/**
	 * @return reEndTime를(을) 리턴합니다.
	 */
	public String getReEndTime() {
		return BaseUtility.null2Blank( reEndTime );
	}
	/**
	 * @return reEraCode를(을) 리턴합니다.
	 */
	public String getReEraCode() {
		return BaseUtility.null2Blank( reEraCode );
	}
	/**
	 * @return reEraCodes를(을) 리턴합니다.
	 */
	public String getReEraCodes() {
		return BaseUtility.null2Blank( reEraCodes );
	}
	/**
	 * @return regCode를(을) 리턴합니다.
	 */
	public String getRegCode() {
		return BaseUtility.null2Blank( regCode );
	}
	/**
	 * @return regName를(을) 리턴합니다.
	 */
	public String getRegName() {
		return BaseUtility.null2Blank( regName );
	}
	/**
	 * @return reInsCode를(을) 리턴합니다.
	 */
	public String getReInsCode() {
		return BaseUtility.null2Blank( reInsCode );
	}
	/**
	 * @return reKndCode를(을) 리턴합니다.
	 */
	public String getReKndCode() {
		return BaseUtility.null2Blank( reKndCode );
	}
	/**
	 * @return reKndType를(을) 리턴합니다.
	 */
	public String getReKndType() {
		return BaseUtility.null2Blank( reKndType );
	}
	/**
	 * @return reLstType를(을) 리턴합니다.
	 */
	public String getReLstType() {
		return BaseUtility.null2Blank( reLstType );
	}
	/**
	 * @return reMemID를(을) 리턴합니다.
	 */
	public String getReMemID() {
		return BaseUtility.null2Blank( reMemID );
	}
	/**
	 * @return reNextDate를(을) 리턴합니다.
	 */
	public String getReNextDate() {
		return BaseUtility.null2Blank( reNextDate );
	}
	/**
	 * @return reNextMonth를(을) 리턴합니다.
	 */
	public String getReNextMonth() {
		return BaseUtility.null2Blank( reNextMonth );
	}
	/**
	 * @return reNextWeek를(을) 리턴합니다.
	 */
	public String getReNextWeek() {
		return BaseUtility.null2Blank( reNextWeek );
	}
	/**
	 * @return reNextYear를(을) 리턴합니다.
	 */
	public String getReNextYear() {
		return BaseUtility.null2Blank( reNextYear );
	}
	/**
	 * @return rePass를(을) 리턴합니다.
	 */
	public String getRePass() {
		return BaseUtility.null2Blank( rePass );
	}
	/**
	 * @return rePntCode를(을) 리턴합니다.
	 */
	public String getRePntCode() {
		return BaseUtility.null2Blank( rePntCode );
	}
	/**
	 * @return rePrevDate를(을) 리턴합니다.
	 */
	public String getRePrevDate() {
		return BaseUtility.null2Blank( rePrevDate );
	}
	/**
	 * @return rePrevMonth를(을) 리턴합니다.
	 */
	public String getRePrevMonth() {
		return BaseUtility.null2Blank( rePrevMonth );
	}
	/**
	 * @return rePrevWeek를(을) 리턴합니다.
	 */
	public String getRePrevWeek() {
		return BaseUtility.null2Blank( rePrevWeek );
	}
	/**
	 * @return rePrevYear를(을) 리턴합니다.
	 */
	public String getRePrevYear() {
		return BaseUtility.null2Blank( rePrevYear );
	}
	/**
	 * @return reReqDate를(을) 리턴합니다.
	 */
	public String getReqDate() {
		return BaseUtility.null2Blank( reqDate );
	}
	/**
	 * @return reReqSeq를(을) 리턴합니다.
	 */
	public String getReqSeq() {
		return BaseUtility.null2Blank( reqSeq );
	}
	/**
	 * @return reReqUUID를(을) 리턴합니다.
	 */
	public String getReqUUID() {
		return BaseUtility.null2Blank( reqUUID );
	}
	/**
	 * @return reRegCode를(을) 리턴합니다.
	 */
	public String getReRegCode() {
		return BaseUtility.null2Blank( reRegCode );
	}
	/**
	 * @return reResCode를(을) 리턴합니다.
	 */
	public String getReResCode() {
		return BaseUtility.null2Blank( reResCode );
	}
	/**
	 * @return reRndCode를(을) 리턴합니다.
	 */
	public String getReRndCode() {
		return BaseUtility.null2Blank( reRndCode );
	}
	/**
	 * @return reRndCodes를(을) 리턴합니다.
	 */
	public String getReRndCodes() {
		return BaseUtility.null2Blank( reRndCodes );
	}
	/**
	 * @return reRndDiv를(을) 리턴합니다.
	 */
	public String getReRndDiv() {
		return BaseUtility.null2Blank( reRndDiv );
	}
	/**
	 * @return reRndUsed를(을) 리턴합니다.
	 */
	public String getReRndUsed() {
		return BaseUtility.null2Blank( reRndUsed );
	}
	/**
	 * @return resCode를(을) 리턴합니다.
	 */
	public String getResCode() {
		return BaseUtility.null2Blank( resCode );
	}
	/**
	 * @return reSiteUse를(을) 리턴합니다.
	 */
	public String getReSiteUse() {
		return BaseUtility.null2Blank( reSiteUse );
	}
	/**
	 * @return resName를(을) 리턴합니다.
	 */
	public String getResName() {
		return BaseUtility.null2Blank( resName );
	}
	/**
	 * @return reSndDate를(을) 리턴합니다.
	 */
	public String getReSndDate() {
		return BaseUtility.null2Blank( reSndDate );
	}
	/**
	 * @return reStdDate를(을) 리턴합니다.
	 */
	public String getReStdDate() {
		return BaseUtility.null2Blank( reStdDate );
	}
	/**
	 * @return reStdMonth를(을) 리턴합니다.
	 */
	public String getReStdMonth() {
		return BaseUtility.null2Blank( reStdMonth );
	}
	/**
	 * @return reStdStat를(을) 리턴합니다.
	 */
	public String getReStdStat() {
		return BaseUtility.null2Blank( reStdStat );
	}
	/**
	 * @return reStdWeek를(을) 리턴합니다.
	 */
	public String getReStdWeek() {
		return BaseUtility.null2Blank( reStdWeek );
	}
	/**
	 * @return reStdYear를(을) 리턴합니다.
	 */
	public String getReStdYear() {
		return BaseUtility.null2Blank( reStdYear );
	}
	/**
	 * @return reSugCode를(을) 리턴합니다.
	 */
	public String getReSugCode() {
		return BaseUtility.null2Blank( reSugCode );
	}
	/**
	 * @return reUseStat를(을) 리턴합니다.
	 */
	public String getReUseStat() {
		return BaseUtility.null2Blank( reUseStat );
	}
	/**
	 * @return rlySeq를(을) 리턴합니다.
	 */
	public String getRlySeq() {
		return BaseUtility.null2Blank( rlySeq );
	}
	/**
	 * @return rlyType를(을) 리턴합니다.
	 */
	public String getRlyType() {
		return BaseUtility.null2Blank( rlyType );
	}
	/**
	 * @return rlyUUID를(을) 리턴합니다.
	 */
	public String getRlyUUID() {
		return BaseUtility.null2Blank( rlyUUID );
	}
	/**
	 * @return rndCodes를(을) 리턴합니다.
	 */
	public List<String> getRndCodes() {
		return rndCodes;
	}
	/**
	 * @return rndivName를(을) 리턴합니다.
	 */
	public String getRndivName() {
		return rndivName;
	}
	/**
	 * @return roundCode를(을) 리턴합니다.
	 */
	public String getRoundCode() {
		return BaseUtility.null2Blank( roundCode );
	}
	/**
	 * @return roundKind를(을) 리턴합니다.
	 */
	public String getRoundKind() {
		return BaseUtility.null2Blank( roundKind );
	}
	/**
	 * @return roundName를(을) 리턴합니다.
	 */
	public String getRoundName() {
		return BaseUtility.null2Blank( roundName );
	}
	/**
	 * @return rowCnt를(을) 리턴합니다.
	 */
	public String getRowCnt() {
		return BaseUtility.null2Blank( rowCnt );
	}
	/**
	 * @return rowIdx를(을) 리턴합니다.
	 */
	public String getRowIdx() {
		return BaseUtility.null2Blank( rowIdx );
	}
	/**
	 * @return rowNext를(을) 리턴합니다.
	 */
	public String getRowNext() {
		return BaseUtility.null2Blank( rowNext );
	}
	/**
	 * @return rowPrev를(을) 리턴합니다.
	 */
	public String getRowPrev() {
		return BaseUtility.null2Blank( rowPrev );
	}
	/**
	 * @return rowRank를(을) 리턴합니다.
	 */
	public String getRowRank() {
		return BaseUtility.null2Blank( rowRank );
	}
	/**
	 * @return rowSeq를(을) 리턴합니다.
	 */
	public String getRowSeq() {
		return BaseUtility.null2Blank( rowSeq );
	}
	/**
	 * @return rowUUID를(을) 리턴합니다.
	 */
	public String getRowUUID() {
		return BaseUtility.null2Blank( rowUUID );
	}
	/**
	 * @return rowXls를(을) 리턴합니다.
	 */
	public String getRowXls() {
		return BaseUtility.null2Blank( rowXls );
	}
	/**
	 * @return rultCode를(을) 리턴합니다.
	 */
	public String getRultCode() {
		return BaseUtility.null2Blank( rultCode );
	}
	/**
	 * @return rultMsg를(을) 리턴합니다.
	 */
	public String getRultMsg() {
		return BaseUtility.null2Blank( rultMsg );
	}
	/**
	 * @return rultName를(을) 리턴합니다.
	 */
	public String getRultName() {
		return BaseUtility.null2Blank( rultName );
	}
	/**
	 * @return schdSeq를(을) 리턴합니다.
	 */
	public String getSchdSeq() {
		return BaseUtility.null2Blank( schdSeq );
	}
	/**
	 * @return schdUUID를(을) 리턴합니다.
	 */
	public String getSchdUUID() {
		return BaseUtility.null2Blank( schdUUID );
	}
	/**
	 * @return schdYear를(을) 리턴합니다.
	 */
	public String getSchdYear() {
		return BaseUtility.null2Blank( schdYear );
	}
	/**
	 * @return searchRegi를(을) 리턴합니다.
	 */
	public String getSearchRegi() {
		return BaseUtility.null2Blank( searchRegi );
	}
	/**
	 * @return searchText를(을) 리턴합니다.
	 */
	public String getSearchText() {
		return BaseUtility.null2Blank( searchText );
	}
	/**
	 * @return searchType를(을) 리턴합니다.
	 */
	public String getSearchType() {
		return BaseUtility.null2Blank( searchType );
	}
	/**
	 * @return seleMailA를(을) 리턴합니다.
	 */
	public String getSeleMailA() {
		return BaseUtility.null2Blank( seleMailA );
	}
	/**
	 * @return seleMailT를(을) 리턴합니다.
	 */
	public String getSeleMailT() {
		return BaseUtility.null2Blank( seleMailT );
	}
	/**
	 * @return ssnUUID를(을) 리턴합니다.
	 */
	public String getSsnUUID() {
		return BaseUtility.null2Blank( ssnUUID );
	}
	/**
	 * @return stateName를(을) 리턴합니다.
	 */
	public String getStateName() {
		return BaseUtility.null2Blank( stateName );
	}
	/**
	 * @return stdDate를(을) 리턴합니다.
	 */
	public String getStdDate() {
		return BaseUtility.null2Blank( stdDate );
	}
	/**
	 * @return stdDay를(을) 리턴합니다.
	 */
	public String getStdDay() {
		return BaseUtility.null2Blank( stdDay );
	}
	/**
	 * @return stdMonth를(을) 리턴합니다.
	 */
	public String getStdMonth() {
		return BaseUtility.null2Blank( stdMonth );
	}
	/**
	 * @return stdStat를(을) 리턴합니다.
	 */
	public String getStdStat() {
		return BaseUtility.null2Blank( stdStat );
	}
	/**
	 * @return stdTime를(을) 리턴합니다.
	 */
	public String getStdTime() {
		return BaseUtility.null2Blank( stdTime );
	}
	/**
	 * @return stdUUID를(을) 리턴합니다.
	 */
	public String getStdUUID() {
		return BaseUtility.null2Blank( stdUUID );
	}
	/**
	 * @return stdWeek를(을) 리턴합니다.
	 */
	public String getStdWeek() {
		return BaseUtility.null2Blank( stdWeek );
	}
	/**
	 * @return stdYear를(을) 리턴합니다.
	 */
	public String getStdYear() {
		return BaseUtility.null2Blank( stdYear );
	}
	/**
	 * @return sugCode를(을) 리턴합니다.
	 */
	public String getSugCode() {
		return BaseUtility.null2Blank( sugCode );
	}
	/**
	 * @return sugName를(을) 리턴합니다.
	 */
	public String getSugName() {
		return BaseUtility.null2Blank( sugName );
	}
	/**
	 * @return sugNum를(을) 리턴합니다.
	 */
	public String getSugNum() {
		return BaseUtility.null2Blank( sugNum );
	}
	/**
	 * @return sugSeq를(을) 리턴합니다.
	 */
	public String getSugSeq() {
		return BaseUtility.null2Blank( sugSeq );
	}
	/**
	 * @return sugType를(을) 리턴합니다.
	 */
	public String getSugType() {
		return BaseUtility.null2Blank( sugType );
	}
	/**
	 * @return sugUUID를(을) 리턴합니다.
	 */
	public String getSugUUID() {
		return BaseUtility.null2Blank( sugUUID );
	}
	/**
	 * @return sugYear를(을) 리턴합니다.
	 */
	public String getSugYear() {
		return BaseUtility.null2Blank( sugYear );
	}
	/**
	 * @return toDate를(을) 리턴합니다.
	 */
	public String getToDate() {
		return BaseUtility.null2Blank( toDate );
	}
	/**
	 * @return toDateDN7를(을) 리턴합니다.
	 */
	public String getToDateDN7() {
		return BaseUtility.null2Blank( toDateDN7 );
	}
	/**
	 * @return toDateDP1를(을) 리턴합니다.
	 */
	public String getToDateDP1() {
		return BaseUtility.null2Blank( toDateDP1 );
	}
	/**
	 * @return toDateDP7를(을) 리턴합니다.
	 */
	public String getToDateP7() {
		return BaseUtility.null2Blank( toDateDP7 );
	}
	/**
	 * @return toDateMN1를(을) 리턴합니다.
	 */
	public String getToDateMN1() {
		return BaseUtility.null2Blank( toDateMN1 );
	}
	/**
	 * @return toDateMN3를(을) 리턴합니다.
	 */
	public String getToDateMN3() {
		return BaseUtility.null2Blank( toDateMN3 );
	}
	/**
	 * @return toDateMN5를(을) 리턴합니다.
	 */
	public String getToDateNM() {
		return BaseUtility.null2Blank( toDateMN5 );
	}
	/**
	 * @return toDateMP1를(을) 리턴합니다.
	 */
	public String getToDatePM() {
		return BaseUtility.null2Blank( toDateMP1 );
	}
	/**
	 * @return toDay를(을) 리턴합니다.
	 */
	public String getToDay() {
		return BaseUtility.null2Blank( toDay );
	}
	/**
	 * @return toMonth를(을) 리턴합니다.
	 */
	public String getToMonth() {
		return BaseUtility.null2Blank( toMonth );
	}
	/**
	 * @return toTime를(을) 리턴합니다.
	 */
	public String getToTime() {
		return BaseUtility.null2Blank( toTime );
	}
	/**
	 * @return toWeek를(을) 리턴합니다.
	 */
	public String getToWeek() {
		return BaseUtility.null2Blank( toWeek );
	}
	/**
	 * @return toYear를(을) 리턴합니다.
	 */
	public String getToYear() {
		return BaseUtility.null2Blank( toYear );
	}
	/**
	 * @return useDele를(을) 리턴합니다.
	 */
	public String getUseDele() {
		return BaseUtility.null2Blank( useDele );
	}
	/**
	 * @return useDeleName를(을) 리턴합니다.
	 */
	public String getUseDeleName() {
		return BaseUtility.null2Blank( useDeleName );
	}
	/**
	 * @return useFlag를(을) 리턴합니다.
	 */
	public String getUseFlag() {
		return BaseUtility.null2Blank( useFlag );
	}
	/**
	 * @return useFlagName를(을) 리턴합니다.
	 */
	public String getUseFlagName() {
		return BaseUtility.null2Blank( useFlagName );
	}
	/**
	 * @return userCorpTele를(을) 리턴합니다.
	 */
	public String getUserCorpTele() {
		return BaseUtility.null2Blank( userCorpTele );
	}
	/**
	 * @return userDesc를(을) 리턴합니다.
	 */
	public String getUserDesc() {
		return BaseUtility.null2Blank( userDesc );
	}
	/**
	 * @return userHomeTele를(을) 리턴합니다.
	 */
	public String getUserHomeTele() {
		return BaseUtility.null2Blank( userHomeTele );
	}
	/**
	 * @return userIPv4를(을) 리턴합니다.
	 */
	public String getUserIPv4() {
		return BaseUtility.null2Blank( userIPv4 );
	}
	/**
	 * @return userIPv6를(을) 리턴합니다.
	 */
	public String getUserIPv6() {
		return BaseUtility.null2Blank( userIPv6 );
	}
	/**
	 * @return userLog를(을) 리턴합니다.
	 */
	public String getUserLog() {
		return BaseUtility.null2Blank( userLog );
	}
	/**
	 * @return userMac를(을) 리턴합니다.
	 */
	public String getUserMac() {
		return BaseUtility.null2Blank( userMac );
	}
	/**
	 * @return userMail를(을) 리턴합니다.
	 */
	public String getUserMail() {
		return BaseUtility.null2Blank( userMail );
	}
	/**
	 * @return userMobiTele를(을) 리턴합니다.
	 */
	public String getUserMobiTele() {
		return BaseUtility.null2Blank( userMobiTele );
	}
	/**
	 * @return userName를(을) 리턴합니다.
	 */
	public String getUserName() {
		return BaseUtility.null2Blank( userName );
	}
	/**
	 * @return userPID를(을) 리턴합니다.
	 */
	public String getUserPID() {
		return BaseUtility.null2Blank( userPID );
	}
	/**
	 * @return userPWD를(을) 리턴합니다.
	 */
	public String getUserPWD() {
		return BaseUtility.null2Blank( userPWD );
	}
	/**
	 * @return userSeq를(을) 리턴합니다.
	 */
	public String getUserSeq() {
		return BaseUtility.null2Blank( userSeq );
	}
	/**
	 * @return userType를(을) 리턴합니다.
	 */
	public String getUserType() {
		return BaseUtility.null2Blank( userType );
	}
	/**
	 * @return userTypeName를(을) 리턴합니다.
	 */
	public String getUserTypeName() {
		return BaseUtility.null2Blank( userTypeName );
	}
	/**
	 * @return userUUID를(을) 리턴합니다.
	 */
	public String getUserUUID() {
		return BaseUtility.null2Blank( userUUID );
	}
	/**
	 * @return useSync를(을) 리턴합니다.
	 */
	public String getUseSync() {
		return BaseUtility.null2Blank( useSync );
	}
	/**
	 * @return useSyncName를(을) 리턴합니다.
	 */
	public String getUseSyncName() {
		return BaseUtility.null2Blank( useSyncName );
	}
	/**
	 * @return viewNo를(을) 리턴합니다.
	 */
	public String getViewNo() {
		return BaseUtility.null2Blank( viewNo );
	}
	/**
	 * @return vsnSeq를(을) 리턴합니다.
	 */
	public String getVsnSeq() {
		return BaseUtility.null2Blank( vsnSeq );
	}
	/**
	 * @return vsnUUID를(을) 리턴합니다.
	 */
	public String getVsnUUID() {
		return BaseUtility.null2Blank( vsnUUID );
	}
	/**
	 * @return vsnYear를(을) 리턴합니다.
	 */
	public String getVsnYear() {
		return BaseUtility.null2Blank( vsnYear );
	}
	/**
	 * @param actMode 설정하려는 actMode.
	 */
	public void setActMode(String actMode) {
		this.actMode = actMode;
	}
	/**
	 * @param ansSeq 설정하려는 ansSeq.
	 */
	public void setAnsSeq(String ansSeq) {
		this.ansSeq = ansSeq;
	}
	/**
	 * @param ansType 설정하려는 ansType.
	 */
	public void setAnsType(String ansType) {
		this.ansType = ansType;
	}
	/**
	 * @param ansUUID 설정하려는 ansUUID.
	 */
	public void setAnsUUID(String ansUUID) {
		this.ansUUID = ansUUID;
	}
	/**
	 * @param appSeq 설정하려는 appSeq.
	 */
	public void setAppSeq(String appSeq) {
		this.appSeq = appSeq;
	}
	/**
	 * @param appType 설정하려는 appType.
	 */
	public void setAppType(String appType) {
		this.appType = appType;
	}
	/**
	 * @param appUUID 설정하려는 appUUID.
	 */
	public void setAppUUID(String appUUID) {
		this.appUUID = appUUID;
	}
	/**
	 * @param askSeq 설정하려는 askSeq.
	 */
	public void setAskSeq(String askSeq) {
		this.askSeq = askSeq;
	}
	/**
	 * @param askType 설정하려는 askType.
	 */
	public void setAskType(String askType) {
		this.askType = askType;
	}
	/**
	 * @param askUUID 설정하려는 askUUID.
	 */
	public void setAskUUID(String askUUID) {
		this.askUUID = askUUID;
	}
	/**
	 * @param askYear 설정하려는 askYear.
	 */
	public void setAskYear(String askYear) {
		this.askYear = askYear;
	}
	/**
	 * @param aucType 설정하려는 aucType.
	 */
	public void setAucType(String aucType) {
		this.aucType = aucType;
	}
	/**
	 * @param autDesc 설정하려는 autDesc.
	 */
	public void setAutDesc(String autDesc) {
		this.autDesc = autDesc;
	}
	/**
	 * @param authFlag 설정하려는 authFlag.
	 */
	public void setAuthFlag(String authFlag) {
		this.authFlag = authFlag;
	}
	/**
	 * @param authFlagName 설정하려는 authFlagName.
	 */
	public void setAuthFlagName(String authFlagName) {
		this.authFlagName = authFlagName;
	}
	/**
	 * @param authRole 설정하려는 authRole.
	 */
	public void setAuthRole(String authRole) {
		this.authRole = authRole;
	}
	/**
	 * @param authRoleName 설정하려는 authRoleName.
	 */
	public void setAuthRoleName(String authRoleName) {
		this.authRoleName = authRoleName;
	}
	/**
	 * @param autName 설정하려는 autName.
	 */
	public void setAutName(String autName) {
		this.autName = autName;
	}
	/**
	 * @param autOrdr 설정하려는 autOrdr.
	 */
	public void setAutOrdr(String autOrdr) {
		this.autOrdr = autOrdr;
	}
	/**
	 * @param autSeq 설정하려는 autSeq.
	 */
	public void setAutSeq(String autSeq) {
		this.autSeq = autSeq;
	}
	/**
	 * @param autType 설정하려는 autType.
	 */
	public void setAutType(String autType) {
		this.autType = autType;
	}
	/**
	 * @param autUUID 설정하려는 autUUID.
	 */
	public void setAutUUID(String autUUID) {
		this.autUUID = autUUID;
	}
	/**
	 * @param bbsSeq 설정하려는 bbsSeq.
	 */
	public void setBbsSeq(String bbsSeq) {
		this.bbsSeq = bbsSeq;
	}
	/**
	 * @param bbsType 설정하려는 bbsType.
	 */
	public void setBbsType(String bbsType) {
		this.bbsType = bbsType;
	}
	/**
	 * @param bbsTypes 설정하려는 bbsTypes.
	 */
	public void setBbsTypes(List<String> bbsTypes) {
		this.bbsTypes = bbsTypes;
	}
	/**
	 * @param bbsUUID 설정하려는 bbsUUID.
	 */
	public void setBbsUUID(String bbsUUID) {
		this.bbsUUID = bbsUUID;
	}
	/**
	 * @param bgnDate 설정하려는 bgnDate.
	 */
	public void setBgnDate(String bgnDate) {
		this.bgnDate = bgnDate;
	}
	/**
	 * @param bgnTime 설정하려는 bgnTime.
	 */
	public void setBgnTime(String bgnTime) {
		this.bgnTime = bgnTime;
	}
	/**
	 * @param certBirth 설정하려는 certBirth.
	 */
	public void setCertBirth(String certBirth) {
		this.certBirth = certBirth;
	}
	/**
	 * @param certCI 설정하려는 certCI.
	 */
	public void setCertCI(String certCI) {
		this.certCI = certCI;
	}
	/**
	 * @param certCPReq 설정하려는 certCPReq.
	 */
	public void setCertCPReq(String certCPReq) {
		this.certCPReq = certCPReq;
	}
	/**
	 * @param certDate 설정하려는 certDate.
	 */
	public void setCertDate(String certDate) {
		this.certDate = certDate;
	}
	/**
	 * @param certDI 설정하려는 certDI.
	 */
	public void setCertDI(String certDI) {
		this.certDI = certDI;
	}
	/**
	 * @param certGender 설정하려는 certGender.
	 */
	public void setCertGender(String certGender) {
		this.certGender = certGender;
	}
	/**
	 * @param certIdx 설정하려는 certIdx.
	 */
	public void setCertIdx(String certIdx) {
		this.certIdx = certIdx;
	}
	/**
	 * @param certInfo 설정하려는 certInfo.
	 */
	public void setCertInfo(String certInfo) {
		this.certInfo = certInfo;
	}
	/**
	 * @param certName 설정하려는 certName.
	 */
	public void setCertName(String certName) {
		this.certName = certName;
	}
	/**
	 * @param certNation 설정하려는 certNation.
	 */
	public void setCertNation(String certNation) {
		this.certNation = certNation;
	}
	/**
	 * @param certTime 설정하려는 certTime.
	 */
	public void setCertTime(String certTime) {
		this.certTime = certTime;
	}
	/**
	 * @param certType 설정하려는 certType.
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	/**
	 * @param certUse 설정하려는 certUse.
	 */
	public void setCertUse(String certUse) {
		this.certUse = certUse;
	}
	/**
	 * @param codDesc 설정하려는 codDesc.
	 */
	public void setCodDesc(String codDesc) {
		this.codDesc = codDesc;
	}
	/**
	 * @param codFull 설정하려는 codFull.
	 */
	public void setCodFull(String codFull) {
		this.codFull = codFull;
	}
	/**
	 * @param codLrg 설정하려는 codLrg.
	 */
	public void setCodLrg(String codLrg) {
		this.codLrg = codLrg;
	}
	/**
	 * @param codMid 설정하려는 codMid.
	 */
	public void setCodMid(String codMid) {
		this.codMid = codMid;
	}
	/**
	 * @param codName 설정하려는 codName.
	 */
	public void setCodName(String codName) {
		this.codName = codName;
	}
	/**
	 * @param codOrdr 설정하려는 codOrdr.
	 */
	public void setCodOrdr(String codOrdr) {
		this.codOrdr = codOrdr;
	}
	/**
	 * @param codSma 설정하려는 codSma.
	 */
	public void setCodSma(String codSma) {
		this.codSma = codSma;
	}
	/**
	 * @param comCode 설정하려는 comCode.
	 */
	public void setComCode(String comCode) {
		this.comCode = comCode;
	}
	/**
	 * @param comName 설정하려는 comName.
	 */
	public void setComName(String comName) {
		this.comName = comName;
	}
	/**
	 * @param condCate 설정하려는 condCate.
	 */
	public void setCondCate(String condCate) {
		this.condCate = condCate;
	}
	/**
	 * @param condClone 설정하려는 condClone.
	 */
	public void setCondClone(String condClone) {
		this.condClone = condClone;
	}
	/**
	 * @param condDele 설정하려는 condDele.
	 */
	public void setCondDele(String condDele) {
		this.condDele = condDele;
	}
	/**
	 * @param condName 설정하려는 condName.
	 */
	public void setCondName(String condName) {
		this.condName = condName;
	}
	/**
	 * @param condType 설정하려는 condType.
	 */
	public void setCondType(String condType) {
		this.condType = condType;
	}
	/**
	 * @param condValue 설정하려는 condValue.
	 */
	public void setCondValue(String condValue) {
		this.condValue = condValue;
	}
	/**
	 * @param confCode 설정하려는 confCode.
	 */
	public void setConfCode(String confCode) {
		this.confCode = confCode;
	}
	/**
	 * @param confName 설정하려는 confName.
	 */
	public void setConfName(String confName) {
		this.confName = confName;
	}
	/**
	 * @param confSeq 설정하려는 confSeq.
	 */
	public void setConfSeq(String confSeq) {
		this.confSeq = confSeq;
	}
	/**
	 * @param confType 설정하려는 confType.
	 */
	public void setConfType(String confType) {
		this.confType = confType;
	}
	/**
	 * @param confUUID 설정하려는 confUUID.
	 */
	public void setConfUUID(String confUUID) {
		this.confUUID = confUUID;
	}
	/**
	 * @param confYear 설정하려는 confYear.
	 */
	public void setConfYear(String confYear) {
		this.confYear = confYear;
	}
	/**
	 * @param creCert 설정하려는 creCert.
	 */
	public void setCreCert(String creCert) {
		this.creCert = creCert;
	}
	/**
	 * @param creDate 설정하려는 creDate.
	 */
	public void setCreDate(String creDate) {
		this.creDate = creDate;
	}
	/**
	 * @param creLock 설정하려는 creLock.
	 */
	public void setCreLock(String creLock) {
		this.creLock = creLock;
	}
	/**
	 * @param creName 설정하려는 creName.
	 */
	public void setCreName(String creName) {
		this.creName = creName;
	}
	/**
	 * @param creType 설정하려는 creType.
	 */
	public void setCreType(String creType) {
		this.creType = creType;
	}
	/**
	 * @param creTypeName 설정하려는 creTypeName.
	 */
	public void setCreTypeName(String creTypeName) {
		this.creTypeName = creTypeName;
	}
	/**
	 * @param creUser 설정하려는 creUser.
	 */
	public void setCreUser(String creUser) {
		this.creUser = creUser;
	}
	/**
	 * @param defAccess 설정하려는 defAccess.
	 */
	public void setDefAccess(String defAccess) {
		this.defAccess = defAccess;
	}
	/**
	 * @param defCreate 설정하려는 defCreate.
	 */
	public void setDefCreate(String defCreate) {
		this.defCreate = defCreate;
	}
	/**
	 * @param defDelete 설정하려는 defDelete.
	 */
	public void setDefDelete(String defDelete) {
		this.defDelete = defDelete;
	}
	/**
	 * @param defDesc 설정하려는 defDesc.
	 */
	public void setDefDesc(String defDesc) {
		this.defDesc = defDesc;
	}
	/**
	 * @param defFull 설정하려는 defFull.
	 */
	public void setDefFull(String defFull) {
		this.defFull = defFull;
	}
	/**
	 * @param defLink 설정하려는 defLink.
	 */
	public void setDefLink(String defLink) {
		this.defLink = defLink;
	}
	/**
	 * @param defModify 설정하려는 defModify.
	 */
	public void setDefModify(String defModify) {
		this.defModify = defModify;
	}
	/**
	 * @param defName 설정하려는 defName.
	 */
	public void setDefName(String defName) {
		this.defName = defName;
	}
	/**
	 * @param defOrdr 설정하려는 defOrdr.
	 */
	public void setDefOrdr(String defOrdr) {
		this.defOrdr = defOrdr;
	}
	/**
	 * @param defPath 설정하려는 defPath.
	 */
	public void setDefPath(String defPath) {
		this.defPath = defPath;
	}
	/**
	 * @param defRead 설정하려는 defRead.
	 */
	public void setDefRead(String defRead) {
		this.defRead = defRead;
	}
	/**
	 * @param defRole 설정하려는 defRole.
	 */
	public void setDefRole(String defRole) {
		this.defRole = defRole;
	}
	/**
	 * @param defUpdate 설정하려는 defUpdate.
	 */
	public void setDefUpdate(String defUpdate) {
		this.defUpdate = defUpdate;
	}
	/**
	 * @param defUUID 설정하려는 defUUID.
	 */
	public void setDefUUID(String defUUID) {
		this.defUUID = defUUID;
	}
	/**
	 * @param delDate 설정하려는 delDate.
	 */
	public void setDelDate(String delDate) {
		this.delDate = delDate;
	}
	/**
	 * @param delName 설정하려는 delName.
	 */
	public void setDelName(String delName) {
		this.delName = delName;
	}
	/**
	 * @param delUser 설정하려는 delUser.
	 */
	public void setDelUser(String delUser) {
		this.delUser = delUser;
	}
	/**
	 * @param endDate 설정하려는 endDate.
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	/**
	 * @param endTime 설정하려는 endTime.
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	/**
	 * @param eraCode 설정하려는 eraCode.
	 */
	public void setEraCode(String eraCode) {
		this.eraCode = eraCode;
	}
	/**
	 * @param eraName 설정하려는 eraName.
	 */
	public void setEraName(String eraName) {
		this.eraName = eraName;
	}
	/**
	 * @param errDesc 설정하려는 errDesc.
	 */
	public void setErrDesc(String errDesc) {
		this.errDesc = errDesc;
	}
	/**
	 * @param errFlag 설정하려는 errFlag.
	 */
	public void setErrFlag(String errFlag) {
		this.errFlag = errFlag;
	}
	/**
	 * @param fileSeq 설정하려는 fileSeq.
	 */
	public void setFileSeq(String fileSeq) {
		this.fileSeq = fileSeq;
	}
	/**
	 * @param fileUUID 설정하려는 fileUUID.
	 */
	public void setFileUUID(String fileUUID) {
		this.fileUUID = fileUUID;
	}
	/**
	 * @param firDate 설정하려는 firDate.
	 */
	public void setFirDate(String firDate) {
		this.firDate = firDate;
	}
	/**
	 * @param fontColor 설정하려는 fontColor.
	 */
	public void setFontColor(String fontColor) {
		this.fontColor = fontColor;
	}
	/**
	 * @param fontName 설정하려는 fontName.
	 */
	public void setFontName(String fontName) {
		this.fontName = fontName;
	}
	/**
	 * @param fontSize 설정하려는 fontSize.
	 */
	public void setFontSize(String fontSize) {
		this.fontSize = fontSize;
	}
	/**
	 * @param gabDay 설정하려는 gabDay.
	 */
	public void setGabDay(String gabDay) {
		this.gabDay = gabDay;
	}
	/**
	 * @param gabSpace 설정하려는 gabSpace.
	 */
	public void setGabSpace(String gabSpace) {
		this.gabSpace = gabSpace;
	}
	/**
	 * @param hstDate 설정하려는 hstDate.
	 */
	public void setHstDate(String hstDate) {
		this.hstDate = hstDate;
	}
	/**
	 * @param hstSeq 설정하려는 hstSeq.
	 */
	public void setHstSeq(String hstSeq) {
		this.hstSeq = hstSeq;
	}
	/**
	 * @param hstUser 설정하려는 hstUser.
	 */
	public void setHstUser(String hstUser) {
		this.hstUser = hstUser;
	}
	/**
	 * @param insCode 설정하려는 insCode.
	 */
	public void setInsCode(String insCode) {
		this.insCode = insCode;
	}
	/**
	 * @param insName 설정하려는 insName.
	 */
	public void setInsName(String insName) {
		this.insName = insName;
	}
	/**
	 * @param kindCode 설정하려는 kindCode.
	 */
	public void setKindCode(String kindCode) {
		this.kindCode = kindCode;
	}
	/**
	 * @param kindName 설정하려는 kindName.
	 */
	public void setKindName(String kindName) {
		this.kindName = kindName;
	}
	/**
	 * @param kindParent 설정하려는 kindParent.
	 */
	public void setKindParent(String kindParent) {
		this.kindParent = kindParent;
	}
	/**
	 * @param listCnt 설정하려는 listCnt.
	 */
	public void setListCnt(String listCnt) {
		this.listCnt = listCnt;
	}
	/**
	 * @param listType 설정하려는 listType.
	 */
	public void setListType(String listType) {
		this.listType = listType;
	}
	/**
	 * @param lnkKey 설정하려는 lnkKey.
	 */
	public void setLnkKey(String lnkKey) {
		this.lnkKey = lnkKey;
	}
	/**
	 * @param lnkName 설정하려는 lnkName.
	 */
	public void setLnkName(String lnkName) {
		this.lnkName = lnkName;
	}
	/**
	 * @param lnkUUID 설정하려는 lnkUUID.
	 */
	public void setLnkUUID(String lnkUUID) {
		this.lnkUUID = lnkUUID;
	}
	/**
	 * @param maxDate 설정하려는 maxDate.
	 */
	public void setMaxDate(String maxDate) {
		this.maxDate = maxDate;
	}
	/**
	 * @param maxYear 설정하려는 maxYear.
	 */
	public void setMaxYear(String maxYear) {
		this.maxYear = maxYear;
	}
	/**
	 * @param memID 설정하려는 memID.
	 */
	public void setMemID(String memID) {
		this.memID = memID;
	}
	/**
	 * @param memName 설정하려는 memName.
	 */
	public void setMemName(String memName) {
		this.memName = memName;
	}
	/**
	 * @param memUUID 설정하려는 memUUID.
	 */
	public void setMemUUID(String memUUID) {
		this.memUUID = memUUID;
	}
	/**
	 * @param minDate 설정하려는 minDate.
	 */
	public void setMinDate(String minDate) {
		this.minDate = minDate;
	}
	/**
	 * @param minYear 설정하려는 minYear.
	 */
	public void setMinYear(String minYear) {
		this.minYear = minYear;
	}
	/**
	 * @param mngDate 설정하려는 mngDate.
	 */
	public void setMngDate(String mngDate) {
		this.mngDate = mngDate;
	}
	/**
	 * @param mngSeq 설정하려는 mngSeq.
	 */
	public void setMngSeq(String mngSeq) {
		this.mngSeq = mngSeq;
	}
	/**
	 * @param mngType 설정하려는 mngType.
	 */
	public void setMngType(String mngType) {
		this.mngType = mngType;
	}
	/**
	 * @param mngUUID 설정하려는 mngUUID.
	 */
	public void setMngUUID(String mngUUID) {
		this.mngUUID = mngUUID;
	}
	/**
	 * @param modDate 설정하려는 modDate.
	 */
	public void setModDate(String modDate) {
		this.modDate = modDate;
	}
	/**
	 * @param modeID 설정하려는 modeID.
	 */
	public void setModeID(String modeID) {
		this.modeID = modeID;
	}
	/**
	 * @param modName 설정하려는 modName.
	 */
	public void setModName(String modName) {
		this.modName = modName;
	}
	/**
	 * @param modUser 설정하려는 modUser.
	 */
	public void setModUser(String modUser) {
		this.modUser = modUser;
	}
	/**
	 * @param nextMode 설정하려는 nextMode.
	 */
	public void setNextMode(String nextMode) {
		this.nextMode = nextMode;
	}
	/**
	 * @param nextUUID 설정하려는 nextUUID.
	 */
	public void setNextUUID(String nextUUID) {
		this.nextUUID = nextUUID;
	}
	/**
	 * @param ordMode 설정하려는 ordMode.
	 */
	public void setOrdMode(String ordMode) {
		this.ordMode = ordMode;
	}
	/**
	 * @param pageCnt 설정하려는 pageCnt.
	 */
	public void setPageCnt(String pageCnt) {
		this.pageCnt = pageCnt;
	}
	/**
	 * @param pageCurNo 설정하려는 pageCurNo.
	 */
	public void setPageCurNo(String pageCurNo) {
		this.pageCurNo = pageCurNo;
	}
	/**
	 * @param pageCurNoP 설정하려는 pageCurNoP.
	 */
	public void setPageCurNoP(String pageCurNoP) {
		this.pageCurNoP = pageCurNoP;
	}
	/**
	 * @param pageFirNo 설정하려는 pageFirNo.
	 */
	public void setPageFirNo(String pageFirNo) {
		this.pageFirNo = pageFirNo;
	}
	/**
	 * @param pageFirNoP 설정하려는 pageFirNoP.
	 */
	public void setPageFirNoP(String pageFirNoP) {
		this.pageFirNoP = pageFirNoP;
	}
	/**
	 * @param pageLstNo 설정하려는 pageLstNo.
	 */
	public void setPageLstNo(String pageLstNo) {
		this.pageLstNo = pageLstNo;
	}
	/**
	 * @param pageLstNoP 설정하려는 pageLstNoP.
	 */
	public void setPageLstNoP(String pageLstNoP) {
		this.pageLstNoP = pageLstNoP;
	}
	/**
	 * @param pagePerLn 설정하려는 pagePerLn.
	 */
	public void setPagePerLn(String pagePerLn) {
		this.pagePerLn = pagePerLn;
	}
	/**
	 * @param pagePerLnP 설정하려는 pagePerLnP.
	 */
	public void setPagePerLnP(String pagePerLnP) {
		this.pagePerLnP = pagePerLnP;
	}
	/**
	 * @param pagePerNo 설정하려는 pagePerNo.
	 */
	public void setPagePerNo(String pagePerNo) {
		this.pagePerNo = pagePerNo;
	}
	/**
	 * @param pagePerNoP 설정하려는 pagePerNoP.
	 */
	public void setPagePerNoP(String pagePerNoP) {
		this.pagePerNoP = pagePerNoP;
	}
	/**
	 * @param pageTotNo 설정하려는 pageTotNo.
	 */
	public void setPageTotNo(String pageTotNo) {
		this.pageTotNo = pageTotNo;
	}
	/**
	 * @param pageTotNoP 설정하려는 pageTotNoP.
	 */
	public void setPageTotNoP(String pageTotNoP) {
		this.pageTotNoP = pageTotNoP;
	}
	/**
	 * @param pointCode 설정하려는 pointCode.
	 */
	public void setPointCode(String pointCode) {
		this.pointCode = pointCode;
	}
	/**
	 * @param pointName 설정하려는 pointName.
	 */
	public void setPointName(String pointName) {
		this.pointName = pointName;
	}
	/**
	 * @param pubType 설정하려는 pubType.
	 */
	public void setPubType(String pubType) {
		this.pubType = pubType;
	}
	/**
	 * @param pwdDateErr 설정하려는 pwdDateErr.
	 */
	public void setPwdDateErr(String pwdDateErr) {
		this.pwdDateErr = pwdDateErr;
	}
	/**
	 * @param pwdDateGet 설정하려는 pwdDateGet.
	 */
	public void setPwdDateGet(String pwdDateGet) {
		this.pwdDateGet = pwdDateGet;
	}
	/**
	 * @param pwdDateLst 설정하려는 pwdDateLst.
	 */
	public void setPwdDateLst(String pwdDateLst) {
		this.pwdDateLst = pwdDateLst;
	}
	/**
	 * @param pwdDateMod 설정하려는 pwdDateMod.
	 */
	public void setPwdDateMod(String pwdDateMod) {
		this.pwdDateMod = pwdDateMod;
	}
	/**
	 * @param pwdDateTerm 설정하려는 pwdDateTerm.
	 */
	public void setPwdDateTerm(String pwdDateTerm) {
		this.pwdDateTerm = pwdDateTerm;
	}
	/**
	 * @param pwdDateUse 설정하려는 pwdDateUse.
	 */
	public void setPwdDateUse(String pwdDateUse) {
		this.pwdDateUse = pwdDateUse;
	}
	/**
	 * @param reAskAddr 설정하려는 reAskAddr.
	 */
	public void setReAskAddr(String reAskAddr) {
		this.reAskAddr = reAskAddr;
	}
	/**
	 * @param reAuthFlag 설정하려는 reAuthFlag.
	 */
	public void setReAuthFlag(String reAuthFlag) {
		this.reAuthFlag = reAuthFlag;
	}
	/**
	 * @param reAuthRole 설정하려는 reAuthRole.
	 */
	public void setReAuthRole(String reAuthRole) {
		this.reAuthRole = reAuthRole;
	}
	/**
	 * @param reBbsCate 설정하려는 reBbsCate.
	 */
	public void setReBbsCate(String reBbsCate) {
		this.reBbsCate = reBbsCate;
	}
	/**
	 * @param reBgnDate 설정하려는 reBgnDate.
	 */
	public void setReBgnDate(String reBgnDate) {
		this.reBgnDate = reBgnDate;
	}
	/**
	 * @param reBgnTime 설정하려는 reBgnTime.
	 */
	public void setReBgnTime(String reBgnTime) {
		this.reBgnTime = reBgnTime;
	}
	/**
	 * @param reCreHide 설정하려는 reCreHide.
	 */
	public void setReCreHide(String reCreHide) {
		this.reCreHide = reCreHide;
	}
	/**
	 * @param reCreMobi 설정하려는 reCreMobi.
	 */
	public void setReCreMobi(String reCreMobi) {
		this.reCreMobi = reCreMobi;
	}
	/**
	 * @param reComCode 설정하려는 reComCode.
	 */
	public void setReComCode(String reComCode) {
		this.reComCode = reComCode;
	}
	/**
	 * @param reConfType 설정하려는 reConfType.
	 */
	public void setReConfType(String reConfType) {
		this.reConfType = reConfType;
	}
	/**
	 * @param reCurrDate 설정하려는 reCurrDate.
	 */
	public void setReCurrDate(String reCurrDate) {
		this.reCurrDate = reCurrDate;
	}
	/**
	 * @param reCurrMonth 설정하려는 reCurrMonth.
	 */
	public void setReCurrMonth(String reCurrMonth) {
		this.reCurrMonth = reCurrMonth;
	}
	/**
	 * @param reCurrWeek 설정하려는 reCurrWeek.
	 */
	public void setReCurrWeek(String reCurrWeek) {
		this.reCurrWeek = reCurrWeek;
	}
	/**
	 * @param reCurrYear 설정하려는 reCurrYear.
	 */
	public void setReCurrYear(String reCurrYear) {
		this.reCurrYear = reCurrYear;
	}
	/**
	 * @param reEndDate 설정하려는 reEndDate.
	 */
	public void setReEndDate(String reEndDate) {
		this.reEndDate = reEndDate;
	}
	/**
	 * @param reEndTime 설정하려는 reEndTime.
	 */
	public void setReEndTime(String reEndTime) {
		this.reEndTime = reEndTime;
	}
	/**
	 * @param reEraCode 설정하려는 reEraCode.
	 */
	public void setReEraCode(String reEraCode) {
		this.reEraCode = reEraCode;
	}
	/**
	 * @param reEraCodes 설정하려는 reEraCodes.
	 */
	public void setReEraCodes(String reEraCodes) {
		this.reEraCodes = reEraCodes;
	}
	/**
	 * @param regCode 설정하려는 regCode.
	 */
	public void setRegCode(String regCode) {
		this.regCode = regCode;
	}
	/**
	 * @param regName 설정하려는 regName.
	 */
	public void setRegName(String regName) {
		this.regName = regName;
	}
	/**
	 * @param reInsCode 설정하려는 reInsCode.
	 */
	public void setReInsCode(String reInsCode) {
		this.reInsCode = reInsCode;
	}
	/**
	 * @param reKndCode 설정하려는 reKndCode.
	 */
	public void setReKndCode(String reKndCode) {
		this.reKndCode = reKndCode;
	}
	/**
	 * @param reKndType 설정하려는 reKndType.
	 */
	public void setReKndType(String reKndType) {
		this.reKndType = reKndType;
	}
	/**
	 * @param reLstType 설정하려는 reLstType.
	 */
	public void setReLstType(String reLstType) {
		this.reLstType = reLstType;
	}
	/**
	 * @param reMemID 설정하려는 reMemID.
	 */
	public void setReMemID(String reMemID) {
		this.reMemID = reMemID;
	}
	/**
	 * @param reNextDate 설정하려는 reNextDate.
	 */
	public void setReNextDate(String reNextDate) {
		this.reNextDate = reNextDate;
	}
	/**
	 * @param reNextMonth 설정하려는 reNextMonth.
	 */
	public void setReNextMonth(String reNextMonth) {
		this.reNextMonth = reNextMonth;
	}
	/**
	 * @param reNextWeek 설정하려는 reNextWeek.
	 */
	public void setReNextWeek(String reNextWeek) {
		this.reNextWeek = reNextWeek;
	}
	/**
	 * @param reNextYear 설정하려는 reNextYear.
	 */
	public void setReNextYear(String reNextYear) {
		this.reNextYear = reNextYear;
	}
	/**
	 * @param rePass 설정하려는 rePass.
	 */
	public void setRePass(String rePass) {
		this.rePass = rePass;
	}
	/**
	 * @param rePntCode 설정하려는 rePntCode.
	 */
	public void setRePntCode(String rePntCode) {
		this.rePntCode = rePntCode;
	}
	/**
	 * @param rePrevDate 설정하려는 rePrevDate.
	 */
	public void setRePrevDate(String rePrevDate) {
		this.rePrevDate = rePrevDate;
	}
	/**
	 * @param rePrevMonth 설정하려는 rePrevMonth.
	 */
	public void setRePrevMonth(String rePrevMonth) {
		this.rePrevMonth = rePrevMonth;
	}
	/**
	 * @param rePrevWeek 설정하려는 rePrevWeek.
	 */
	public void setRePrevWeek(String rePrevWeek) {
		this.rePrevWeek = rePrevWeek;
	}
	/**
	 * @param rePrevYear 설정하려는 rePrevYear.
	 */
	public void setRePrevYear(String rePrevYear) {
		this.rePrevYear = rePrevYear;
	}
	/**
	 * @param reqDate 설정하려는 reqDate.
	 */
	public void setReqDate(String reqDate) {
		this.reqDate = reqDate;
	}
	/**
	 * @param reqSeq 설정하려는 reqSeq.
	 */
	public void setReqSeq(String reqSeq) {
		this.reqSeq = reqSeq;
	}
	/**
	 * @param reqUUID 설정하려는 reqUUID.
	 */
	public void setReqUUID(String reqUUID) {
		this.reqUUID = reqUUID;
	}
	/**
	 * @param reRegCode 설정하려는 reRegCode.
	 */
	public void setReRegCode(String reRegCode) {
		this.reRegCode = reRegCode;
	}
	/**
	 * @param reResCode 설정하려는 reResCode.
	 */
	public void setReResCode(String reResCode) {
		this.reResCode = reResCode;
	}
	/**
	 * @param reRndCode 설정하려는 reRndCode.
	 */
	public void setReRndCode(String reRndCode) {
		this.reRndCode = reRndCode;
	}
	/**
	 * @param reRndCodes 설정하려는 reRndCodes.
	 */
	public void setReRndCodes(String reRndCodes) {
		this.reRndCodes = reRndCodes;
	}
	/**
	 * @param reRndDiv 설정하려는 reRndDiv.
	 */
	public void setReRndDiv(String reRndDiv) {
		this.reRndDiv = reRndDiv;
	}
	/**
	 * @param reRndUsed 설정하려는 reRndUsed.
	 */
	public void setReRndUsed(String reRndUsed) {
		this.reRndUsed = reRndUsed;
	}
	/**
	 * @param resCode 설정하려는 resCode.
	 */
	public void setResCode(String resCode) {
		this.resCode = resCode;
	}
	/**
	 * @param reSiteUse 설정하려는 reSiteUse.
	 */
	public void setReSiteUse(String reSiteUse) {
		this.reSiteUse = reSiteUse;
	}
	/**
	 * @param resName 설정하려는 resName.
	 */
	public void setResName(String resName) {
		this.resName = resName;
	}
	/**
	 * @param reSndDate 설정하려는 reSndDate.
	 */
	public void setReSndDate(String reSndDate) {
		this.reSndDate = reSndDate;
	}
	/**
	 * @param reStdDate 설정하려는 reStdDate.
	 */
	public void setReStdDate(String reStdDate) {
		this.reStdDate = reStdDate;
	}
	/**
	 * @param reStdMonth 설정하려는 reStdMonth.
	 */
	public void setReStdMonth(String reStdMonth) {
		this.reStdMonth = reStdMonth;
	}
	/**
	 * @param reStdStat 설정하려는 reStdStat.
	 */
	public void setReStdStat(String reStdStat) {
		this.reStdStat = reStdStat;
	}
	/**
	 * @param reStdWeek 설정하려는 reStdWeek.
	 */
	public void setReStdWeek(String reStdWeek) {
		this.reStdWeek = reStdWeek;
	}
	/**
	 * @param reStdYear 설정하려는 reStdYear.
	 */
	public void setReStdYear(String reStdYear) {
		this.reStdYear = reStdYear;
	}
	/**
	 * @param reUseStat 설정하려는 reUseStat.
	 */
	public void setReUseStat(String reUseStat) {
		this.reUseStat = reUseStat;
	}
	/**
	 * @param reSugCode 설정하려는 reSugCode.
	 */
	public void setReSugCode(String reSugCode) {
		this.reSugCode = reSugCode;
	}
	/**
	 * @param rlySeq 설정하려는 rlySeq.
	 */
	public void setRlySeq(String rlySeq) {
		this.rlySeq = rlySeq;
	}
	/**
	 * @param rlyType 설정하려는 rlyType.
	 */
	public void setRlyType(String rlyType) {
		this.rlyType = rlyType;
	}
	/**
	 * @param rlyUUID 설정하려는 rlyUUID.
	 */
	public void setRlyUUID(String rlyUUID) {
		this.rlyUUID = rlyUUID;
	}
	/**
	 * @param rndCodes 설정하려는 rndCodes.
	 */
	public void setRndCodes(List<String> rndCodes) {
		this.rndCodes = rndCodes;
	}
	/**
	 * @param rndivName 설정하려는 rndivName.
	 */
	public void setRndivName(String rndivName) {
		this.rndivName = rndivName;
	}
	/**
	 * @param roundCode 설정하려는 roundCode.
	 */
	public void setRoundCode(String roundCode) {
		this.roundCode = roundCode;
	}
	/**
	 * @param roundKind 설정하려는 roundKind.
	 */
	public void setRoundKind(String roundKind) {
		this.roundKind = roundKind;
	}
	/**
	 * @param roundName 설정하려는 roundName.
	 */
	public void setRoundName(String roundName) {
		this.roundName = roundName;
	}
	/**
	 * @param rowCnt 설정하려는 rowCnt.
	 */
	public void setRowCnt(String rowCnt) {
		this.rowCnt = rowCnt;
	}
	/**
	 * @param rowIdx 설정하려는 rowIdx.
	 */
	public void setRowIdx(String rowIdx) {
		this.rowIdx = rowIdx;
	}
	/**
	 * @param rowNext 설정하려는 rowNext.
	 */
	public void setRowNext(String rowNext) {
		this.rowNext = rowNext;
	}
	/**
	 * @param rowPrev 설정하려는 rowPrev.
	 */
	public void setRowPrev(String rowPrev) {
		this.rowPrev = rowPrev;
	}
	/**
	 * @param rowRank 설정하려는 rowRank.
	 */
	public void setRowRank(String rowRank) {
		this.rowRank = rowRank;
	}
	/**
	 * @param rowSeq 설정하려는 rowSeq.
	 */
	public void setRowSeq(String rowSeq) {
		this.rowSeq = rowSeq;
	}
	/**
	 * @param rowUUID 설정하려는 rowUUID.
	 */
	public void setRowUUID(String rowUUID) {
		this.rowUUID = rowUUID;
	}
	/**
	 * @param rowXls 설정하려는 rowXls.
	 */
	public void setRowXls(String rowXls) {
		this.rowXls = rowXls;
	}
	/**
	 * @param rultCode 설정하려는 rultCode.
	 */
	public void setRultCode(String rultCode) {
		this.rultCode = rultCode;
	}
	/**
	 * @param rultMsg 설정하려는 rultMsg.
	 */
	public void setRultMsg(String rultMsg) {
		this.rultMsg = rultMsg;
	}
	/**
	 * @param rultName 설정하려는 rultName.
	 */
	public void setRultName(String rultName) {
		this.rultName = rultName;
	}
	/**
	 * @param searchText 설정하려는 searchText.
	 */
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	/**
	 * @param schdSeq 설정하려는 schdSeq.
	 */
	public void setSchdSeq(String schdSeq) {
		this.schdSeq = schdSeq;
	}
	/**
	 * @param schdUUID 설정하려는 schdUUID.
	 */
	public void setSchdUUID(String schdUUID) {
		this.schdUUID = schdUUID;
	}
	/**
	 * @param schdYear 설정하려는 schdYear.
	 */
	public void setSchdYear(String schdYear) {
		this.schdYear = schdYear;
	}
	/**
	 * @param searchRegi 설정하려는 searchRegi.
	 */
	public void setSearchRegi(String searchRegi) {
		this.searchRegi = searchRegi;
	}
	/**
	 * @param searchType 설정하려는 searchType.
	 */
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	/**
	 * @param seleMailA 설정하려는 seleMailA.
	 */
	public void setSeleMailA(String seleMailA) {
		this.seleMailA = seleMailA;
	}
	/**
	 * @param seleMailT 설정하려는 seleMailT.
	 */
	public void setSeleMailT(String seleMailT) {
		this.seleMailT = seleMailT;
	}
	/**
	 * @param ssnUUID 설정하려는 ssnUUID.
	 */
	public void setSsnUUID(String ssnUUID) {
		this.ssnUUID = ssnUUID;
	}
	/**
	 * @param stateName 설정하려는 stateName.
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	/**
	 * @param stdDate 설정하려는 stdDate.
	 */
	public void setStdDate(String stdDate) {
		this.stdDate = stdDate;
	}
	/**
	 * @param stdDay 설정하려는 stdDay.
	 */
	public void setStdDay(String stdDay) {
		this.stdDay = stdDay;
	}
	/**
	 * @param stdMonth 설정하려는 stdMonth.
	 */
	public void setStdMonth(String stdMonth) {
		this.stdMonth = stdMonth;
	}
	/**
	 * @param stdStat 설정하려는 stdStat.
	 */
	public void setStdStat(String stdStat) {
		this.stdStat = stdStat;
	}
	/**
	 * @param stdTime 설정하려는 stdTime.
	 */
	public void setStdTime(String stdTime) {
		this.stdTime = stdTime;
	}
	/**
	 * @param stdUUID 설정하려는 stdUUID.
	 */
	public void setStdUUID(String stdUUID) {
		this.stdUUID = stdUUID;
	}
	/**
	 * @param stdWeek 설정하려는 stdWeek.
	 */
	public void setStdWeek(String stdWeek) {
		this.stdWeek = stdWeek;
	}
	/**
	 * @param stdYear 설정하려는 stdYear.
	 */
	public void setStdYear(String stdYear) {
		this.stdYear = stdYear;
	}
	/**
	 * @param sugCode 설정하려는 sugCode.
	 */
	public void setSugCode(String sugCode) {
		this.sugCode = sugCode;
	}
	/**
	 * @param sugName 설정하려는 sugName.
	 */
	public void setSugName(String sugName) {
		this.sugName = sugName;
	}
	/**
	 * @param sugNum 설정하려는 sugNum.
	 */
	public void setSugNum(String sugNum) {
		this.sugNum = sugNum;
	}
	/**
	 * @param sugSeq 설정하려는 sugSeq.
	 */
	public void setSugSeq(String sugSeq) {
		this.sugSeq = sugSeq;
	}
	/**
	 * @param sugType 설정하려는 sugType.
	 */
	public void setSugType(String sugType) {
		this.sugType = sugType;
	}
	/**
	 * @param sugUUID 설정하려는 sugUUID.
	 */
	public void setSugUUID(String sugUUID) {
		this.sugUUID = sugUUID;
	}
	/**
	 * @param sugYear 설정하려는 sugYear.
	 */
	public void setSugYear(String sugYear) {
		this.sugYear = sugYear;
	}
	/**
	 * @param toDate 설정하려는 toDate.
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	/**
	 * @param toDateDN7 설정하려는 toDateDN7.
	 */
	public void setToDateDN7(String toDateDN7) {
		this.toDateDN7 = toDateDN7;
	}
	/**
	 * @param toDateDP1 설정하려는 toDateDP1.
	 */
	public void setToDateDP1(String toDateDP1) {
		this.toDateDP1 = toDateDP1;
	}
	/**
	 * @param toDateDP7 설정하려는 toDateDP7.
	 */
	public void setToDateDP7(String toDateDP7) {
		this.toDateDP7 = toDateDP7;
	}
	/**
	 * @param toDateMN1 설정하려는 toDateMN1.
	 */
	public void setToDateMN1(String toDateMN1) {
		this.toDateMN1 = toDateMN1;
	}
	/**
	 * @param toDateMN3 설정하려는 toDateMN3.
	 */
	public void setToDateMN3(String toDateMN3) {
		this.toDateMN3 = toDateMN3;
	}
	/**
	 * @param toDateMN5 설정하려는 toDateMN5.
	 */
	public void setToDateMN5(String toDateMN5) {
		this.toDateMN5 = toDateMN5;
	}
	/**
	 * @param toDateMP1 설정하려는 toDateMP1.
	 */
	public void setToDateMP1(String toDateMP1) {
		this.toDateMP1 = toDateMP1;
	}
	/**
	 * @param toDay 설정하려는 toDay.
	 */
	public void setToDay(String toDay) {
		this.toDay = toDay;
	}
	/**
	 * @param toMonth 설정하려는 toMonth.
	 */
	public void setToMonth(String toMonth) {
		this.toMonth = toMonth;
	}
	/**
	 * @param toTime 설정하려는 toTime.
	 */
	public void setToTime(String toTime) {
		this.toTime = toTime;
	}
	/**
	 * @param toWeek 설정하려는 toWeek.
	 */
	public void setToWeek(String toWeek) {
		this.toWeek = toWeek;
	}
	/**
	 * @param toYear 설정하려는 toYear.
	 */
	public void setToYear(String toYear) {
		this.toYear = toYear;
	}
	/**
	 * @param useDele 설정하려는 useDele.
	 */
	public void setUseDele(String useDele) {
		this.useDele = useDele;
	}
	/**
	 * @param useDeleName 설정하려는 useDeleName.
	 */
	public void setUseDeleName(String useDeleName) {
		this.useDeleName = useDeleName;
	}
	/**
	 * @param useFlag 설정하려는 useFlag.
	 */
	public void setUseFlag(String useFlag) {
		this.useFlag = useFlag;
	}
	/**
	 * @param useFlagName 설정하려는 useFlagName.
	 */
	public void setUseFlagName(String useFlagName) {
		this.useFlagName = useFlagName;
	}
	/**
	 * @param userCorpTele 설정하려는 userCorpTele.
	 */
	public void setUserCorpTele(String userCorpTele) {
		this.userCorpTele = userCorpTele;
	}
	/**
	 * @param userDesc 설정하려는 userDesc.
	 */
	public void setUserDesc(String userDesc) {
		this.userDesc = userDesc;
	}
	/**
	 * @param userHomeTele 설정하려는 userHomeTele.
	 */
	public void setUserHomeTele(String userHomeTele) {
		this.userHomeTele = userHomeTele;
	}
	/**
	 * @param userIPv4 설정하려는 userIPv4.
	 */
	public void setUserIPv4(String userIPv4) {
		this.userIPv4 = userIPv4;
	}
	/**
	 * @param userIPv6 설정하려는 userIPv6.
	 */
	public void setUserIPv6(String userIPv6) {
		this.userIPv6 = userIPv6;
	}
	/**
	 * @param userLog 설정하려는 userLog.
	 */
	public void setUserLog(String userLog) {
		this.userLog = userLog;
	}
	/**
	 * @param userMac 설정하려는 userMac.
	 */
	public void setUserMac(String userMac) {
		this.userMac = userMac;
	}
	/**
	 * @param userMail 설정하려는 userMail.
	 */
	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}
	/**
	 * @param userMobiTele 설정하려는 userMobiTele.
	 */
	public void setUserMobiTele(String userMobiTele) {
		this.userMobiTele = userMobiTele;
	}
	/**
	 * @param userName 설정하려는 userName.
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @param userPID 설정하려는 userPID.
	 */
	public void setUserPID(String userPID) {
		this.userPID = userPID;
	}
	/**
	 * @param userPWD 설정하려는 userPWD.
	 */
	public void setUserPWD(String userPWD) {
		this.userPWD = userPWD;
	}
	/**
	 * @param userSeq 설정하려는 userSeq.
	 */
	public void setUserSeq(String userSeq) {
		this.userSeq = userSeq;
	}
	/**
	 * @param userType 설정하려는 userType.
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}
	/**
	 * @param userTypeName 설정하려는 userTypeName.
	 */
	public void setUserTypeName(String userTypeName) {
		this.userTypeName = userTypeName;
	}
	/**
	 * @param userUUID 설정하려는 userUUID.
	 */
	public void setUserUUID(String userUUID) {
		this.userUUID = userUUID;
	}
	/**
	 * @param useSync 설정하려는 useSync.
	 */
	public void setUseSync(String useSync) {
		this.useSync = useSync;
	}
	/**
	 * @param useSyncName 설정하려는 useSyncName.
	 */
	public void setUseSyncName(String useSyncName) {
		this.useSyncName = useSyncName;
	}
	/**
	 * @param viewNo 설정하려는 viewNo.
	 */
	public void setViewNo(String viewNo) {
		this.viewNo = viewNo;
	}
	/**
	 * @param vsnSeq 설정하려는 vsnSeq.
	 */
	public void setVsnSeq(String vsnSeq) {
		this.vsnSeq = vsnSeq;
	}
	/**
	 * @param vsnUUID 설정하려는 vsnUUID.
	 */
	public void setVsnUUID(String vsnUUID) {
		this.vsnUUID = vsnUUID;
	}
	/**
	 * @param vsnYear 설정하려는 vsnYear.
	 */
	public void setVsnYear(String vsnYear) {
		this.vsnYear = vsnYear;
	}


	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public String getNowPage() {
		return nowPage;
	}
	public void setNowPage(String nowPage) {
		this.nowPage = nowPage;
	}
	public String getRound_div() {
		return round_div;
	}
	public void setRound_div(String round_div) {
		this.round_div = round_div;
	}
	public String getSearchText1() {
		return searchText1;
	}
	public void setSearchText1(String searchText1) {
		this.searchText1 = searchText1;
	}
	public String getSearchText2() {
		return searchText2;
	}
	public void setSearchText2(String searchText2) {
		this.searchText2 = searchText2;
	}

	// add
	public String getBgnScDate() {
		return bgnScDate;
	}
	public void setBgnScDate(String bgnScDate) {
		this.bgnScDate = bgnScDate;
	}
	public String getEndScDate() {
		return endScDate;
	}
	public void setEndScDate(String endScDate) {
		this.endScDate = endScDate;
	}

	// add
	public String getCreNoti() {
		return creNoti;
	}
	public void setCreNoti(String creNoti) {
		this.creNoti = creNoti;
	}

//	public String getDic_word() {
//		return dic_word;
//	}
//	public void setDic_word(String dic_word) {
//		this.dic_word = dic_word;
//	}
//	public String getDic_index() {
//		return dic_index;
//	}
//	public void setDic_index(String dic_index) {
//		this.dic_index = dic_index;
//	}
//	public String getBillKindCode() {
//		return billKindCode;
//	}
//	public void setBillKindCode(String billKindCode) {
//		this.billKindCode = billKindCode;
//	}
//	public String getBillResCode() {
//		return billResCode;
//	}
//	public void setBillResCode(String billResCode) {
//		this.billResCode = billResCode;
//	}
//	public String getBillComCode() {
//		return billComCode;
//	}
//	public void setBillComCode(String billComCode) {
//		this.billComCode = billComCode;
//	}
//	public String getBillSugCode() {
//		return billSugCode;
//	}
//	public void setBillSugCode(String billSugCode) {
//		this.billSugCode = billSugCode;
//	}
//	public String gethNum1() {
//		return hNum1;
//	}
//	public void sethNum1(String hNum1) {
//		try { 
//			this.hNum1 = String.valueOf(Integer.parseInt(hNum1, 10));
//		} catch( NumberFormatException NFE ) { 
//			this.hNum1 = "";
//		}
//	}
//	public String gethNum2() {
//		return hNum2;
//	}
//	public void sethNum2(String hNum2) {
//		try { 
//			this.hNum2 = String.valueOf(Integer.parseInt(hNum2, 10));
//		} catch( NumberFormatException NFE ) { 
//			this.hNum2 = "";
//		}
//	}
//	public String getcNum1() {
//		return cNum1;
//	}
//	public void setcNum1(String cNum1) {
//		try { 
//			this.cNum1 = String.valueOf(Integer.parseInt(cNum1, 10));
//		} catch( NumberFormatException NFE ) { 
//			this.cNum1 = "";
//		}
//	}
//	public String getcNum2() {
//		return cNum2;
//	}
//	public void setcNum2(String cNum2) {
//		try { 
//			this.cNum2 = String.valueOf(Integer.parseInt(cNum2, 10));
//		} catch( NumberFormatException NFE ) { 
//			this.cNum2 = "";
//		}
//	}
//	public String getComp() {
//		return comp;
//	}
//	public void setComp(String comp) {
//		this.comp = comp;
//	}
//	public String getEraAll() {
//		return eraAll;
//	}
//	public void setEraAll(String eraAll) {
//		this.eraAll = eraAll;
//	}
//	public String getQuestionSearchText() {
//		return questionSearchText;
//	}
//	public void setQuestionSearchText(String questionSearchText) {
//		this.questionSearchText = questionSearchText;
//	}
//	@Override
//	public String toString() { 
//		String rult = "";
//
//		rult += "&cNum1="+ cNum1;
//		rult += "&cNum2="+ cNum2;
//		rult += "&hNum1="+ hNum1;
//		rult += "&hNum2="+ hNum2;
//		rult += "&searchText1="+ searchText1;
//		rult += "&searchText2="+ searchText2;
//		rult += "&comp="+ comp;
//		rult += "&eraCode="+ eraCode;
//		rult += "&memId="+ memId;
//		rult += "&kindCode="+ kindCode;
//		rult += "&eraAll="+ eraAll;
//		rult += "&billResCode="+ billResCode;
//		rult += "&billComCode="+ billComCode;
//		rult += "&billSugCode="+ billSugCode;
//		rult += "&search=search";
//
//		return rult;
//	}

}	// end of class