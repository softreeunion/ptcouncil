/*******************************************************************************
  Program ID  : BaseReschdVO
  Description : 통합일정 정보에 대한 Value Object를 정의한다
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.cmmn.vo;

import java.util.List;

@SuppressWarnings("serial")
public class BaseReschdVO extends BaseCommonVO implements java.io.Serializable { 

	// 공통 정보
	private String creIPv4;					// 
	private String creIPv6;					// 
	private String creMac;						// 
	private String modIPv4;					// 
	private String modIPv6;					// 
	private String modMac;						// 
	private String delIPv4;					// 
	private String delIPv6;					// 
	private String delMac;						// 

	private String fileName;					// 
	private String fileSize;					// 
	private String fileRel;					// 
	private String fileAbs;					// 
	private String fileUri;					// 
	private String fileLinkS;					// 첨부파일 내용 (싱글 다운용)
	private String fileLinkM;					// 첨부파일 내용 (멀티 다운용)
	private String fileLinkD;					// 첨부파일 내용 (일반 표시용)
	private String downUUID;
	private String downRel;
	private String downAbs;
	private String downExt;

	// 통합일정 정보
	private String schdType;					// 
	private String schdKind;					// 
	private String schdIpcr;					// 
	private String schdBDate;					// 
	private String schdEDate;					// 
	private String schdBTime;					// 
	private String schdBTimH;					// 
	private String schdBTimM;					// 
	private String schdETime;					// 
	private String schdETimH;					// 
	private String schdETimM;					// 
	private String schdTitle;					// 
	private String schdSTitle;					// 
	private String schdDesc;					// 
	private String schdPlace;					// 
	private String schdReptit;					// 

	private String schdDescCR;					// 
	private String schdDescBR;					// 
	private String schdTypeName;				// 
	private String schdKindName;				// 
	private String schdIpcrName;				// 
	private String schdReptitName;				// 
	private String liveBefore;					// 
	private String livePresent;				// 
	private String liveAfter;					// 

	@SuppressWarnings("rawtypes")
	List childList;
	@SuppressWarnings("rawtypes")
	List childInfo;
	@SuppressWarnings("rawtypes")
	List childItem;
	@SuppressWarnings("rawtypes")
	List childName;
	@SuppressWarnings("rawtypes")
	List childTime;


	/**
	 * @return childInfo를(을) 리턴합니다.
	 */
	@SuppressWarnings("rawtypes")
	public List getChildInfo() {
		return childInfo;
	}
	/**
	 * @return childItem를(을) 리턴합니다.
	 */
	@SuppressWarnings("rawtypes")
	public List getChildItem() {
		return childItem;
	}
	/**
	 * @return childList를(을) 리턴합니다.
	 */
	@SuppressWarnings("rawtypes")
	public List getChildList() {
		return childList;
	}
	/**
	 * @return childName를(을) 리턴합니다.
	 */
	@SuppressWarnings("rawtypes")
	public List getChildName() {
		return childName;
	}
	/**
	 * @return childTime를(을) 리턴합니다.
	 */
	@SuppressWarnings("rawtypes")
	public List getChildTime() {
		return childTime;
	}
	/**
	 * @return creIPv4를(을) 리턴합니다.
	 */
	public String getCreIPv4() {
		return creIPv4;
	}
	/**
	 * @return creIPv6를(을) 리턴합니다.
	 */
	public String getCreIPv6() {
		return creIPv6;
	}
	/**
	 * @return creMac를(을) 리턴합니다.
	 */
	public String getCreMac() {
		return creMac;
	}
	/**
	 * @return delIPv4를(을) 리턴합니다.
	 */
	public String getDelIPv4() {
		return delIPv4;
	}
	/**
	 * @return delIPv6를(을) 리턴합니다.
	 */
	public String getDelIPv6() {
		return delIPv6;
	}
	/**
	 * @return delMac를(을) 리턴합니다.
	 */
	public String getDelMac() {
		return delMac;
	}
	/**
	 * @return downAbs를(을) 리턴합니다.
	 */
	public String getDownAbs() {
		return downAbs;
	}
	/**
	 * @return downExt를(을) 리턴합니다.
	 */
	public String getDownExt() {
		return downExt;
	}
	/**
	 * @return downRel를(을) 리턴합니다.
	 */
	public String getDownRel() {
		return downRel;
	}
	/**
	 * @return downUUID를(을) 리턴합니다.
	 */
	public String getDownUUID() {
		return downUUID;
	}
	/**
	 * @return fileAbs를(을) 리턴합니다.
	 */
	public String getFileAbs() {
		return fileAbs;
	}
	/**
	 * @return fileLinkD를(을) 리턴합니다.
	 */
	public String getFileLinkD() {
		return fileLinkD;
	}
	/**
	 * @return fileLinkM를(을) 리턴합니다.
	 */
	public String getFileLinkM() {
		return fileLinkM;
	}
	/**
	 * @return fileLinkS를(을) 리턴합니다.
	 */
	public String getFileLinkS() {
		return fileLinkS;
	}
	/**
	 * @return fileName를(을) 리턴합니다.
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @return fileRel를(을) 리턴합니다.
	 */
	public String getFileRel() {
		return fileRel;
	}
	/**
	 * @return fileSize를(을) 리턴합니다.
	 */
	public String getFileSize() {
		return fileSize;
	}
	/**
	 * @return fileUri를(을) 리턴합니다.
	 */
	public String getFileUri() {
		return fileUri;
	}
	/**
	 * @return liveAfter를(을) 리턴합니다.
	 */
	public String getLiveAfter() {
		return liveAfter;
	}
	/**
	 * @return liveBefore를(을) 리턴합니다.
	 */
	public String getLiveBefore() {
		return liveBefore;
	}
	/**
	 * @return livePresent를(을) 리턴합니다.
	 */
	public String getLivePresent() {
		return livePresent;
	}
	/**
	 * @return modIPv4를(을) 리턴합니다.
	 */
	public String getModIPv4() {
		return modIPv4;
	}
	/**
	 * @return modIPv6를(을) 리턴합니다.
	 */
	public String getModIPv6() {
		return modIPv6;
	}
	/**
	 * @return modMac를(을) 리턴합니다.
	 */
	public String getModMac() {
		return modMac;
	}
	/**
	 * @return schdBDate를(을) 리턴합니다.
	 */
	public String getSchdBDate() {
		return schdBDate;
	}
	/**
	 * @return schdBTime를(을) 리턴합니다.
	 */
	public String getSchdBTime() {
		return schdBTime;
	}
	/**
	 * @return schdBTimH를(을) 리턴합니다.
	 */
	public String getSchdBTimH() {
		return schdBTimH;
	}
	/**
	 * @return schdBTimM를(을) 리턴합니다.
	 */
	public String getSchdBTimM() {
		return schdBTimM;
	}
	/**
	 * @return schdDesc를(을) 리턴합니다.
	 */
	public String getSchdDesc() {
		return schdDesc;
	}
	/**
	 * @return schdDescBR를(을) 리턴합니다.
	 */
	public String getSchdDescBR() {
		return schdDescBR;
	}
	/**
	 * @return schdDescCR를(을) 리턴합니다.
	 */
	public String getSchdDescCR() {
		return schdDescCR;
	}
	/**
	 * @return schdEDate를(을) 리턴합니다.
	 */
	public String getSchdEDate() {
		return schdEDate;
	}
	/**
	 * @return schdETime를(을) 리턴합니다.
	 */
	public String getSchdETime() {
		return schdETime;
	}
	/**
	 * @return schdETimH를(을) 리턴합니다.
	 */
	public String getSchdETimH() {
		return schdETimH;
	}
	/**
	 * @return schdETimM를(을) 리턴합니다.
	 */
	public String getSchdETimM() {
		return schdETimM;
	}
	/**
	 * @return schdIpcr를(을) 리턴합니다.
	 */
	public String getSchdIpcr() {
		return schdIpcr;
	}
	/**
	 * @return schdIpcrName를(을) 리턴합니다.
	 */
	public String getSchdIpcrName() {
		return schdIpcrName;
	}
	/**
	 * @return schdKind를(을) 리턴합니다.
	 */
	public String getSchdKind() {
		return schdKind;
	}
	/**
	 * @return schdKindName를(을) 리턴합니다.
	 */
	public String getSchdKindName() {
		return schdKindName;
	}
	/**
	 * @return schdPlace를(을) 리턴합니다.
	 */
	public String getSchdPlace() {
		return schdPlace;
	}
	/**
	 * @return schdReptit를(을) 리턴합니다.
	 */
	public String getSchdReptit() {
		return schdReptit;
	}
	/**
	 * @return schdReptitName를(을) 리턴합니다.
	 */
	public String getSchdReptitName() {
		return schdReptitName;
	}
	/**
	 * @return schdSTitle를(을) 리턴합니다.
	 */
	public String getSchdSTitle() {
		return schdSTitle;
	}
	/**
	 * @return schdTitle를(을) 리턴합니다.
	 */
	public String getSchdTitle() {
		return schdTitle;
	}
	/**
	 * @return schdType를(을) 리턴합니다.
	 */
	public String getSchdType() {
		return schdType;
	}
	/**
	 * @return schdTypeName를(을) 리턴합니다.
	 */
	public String getSchdTypeName() {
		return schdTypeName;
	}
	/**
	 * @param childInfo 설정하려는 childInfo.
	 */
	@SuppressWarnings("rawtypes")
	public void setChildInfo(List childInfo) {
		this.childInfo = childInfo;
	}
	/**
	 * @param childItem 설정하려는 childItem.
	 */
	@SuppressWarnings("rawtypes")
	public void setChildItem(List childItem) {
		this.childItem = childItem;
	}
	/**
	 * @param childList 설정하려는 childList.
	 */
	@SuppressWarnings("rawtypes")
	public void setChildList(List childList) {
		this.childList = childList;
	}
	/**
	 * @param childName 설정하려는 childName.
	 */
	@SuppressWarnings("rawtypes")
	public void setChildName(List childName) {
		this.childName = childName;
	}
	/**
	 * @param childTime 설정하려는 childTime.
	 */
	@SuppressWarnings("rawtypes")
	public void setChildTime(List childTime) {
		this.childTime = childTime;
	}
	/**
	 * @param creIPv4 설정하려는 creIPv4.
	 */
	public void setCreIPv4(String creIPv4) {
		this.creIPv4 = creIPv4;
	}
	/**
	 * @param creIPv6 설정하려는 creIPv6.
	 */
	public void setCreIPv6(String creIPv6) {
		this.creIPv6 = creIPv6;
	}
	/**
	 * @param creMac 설정하려는 creMac.
	 */
	public void setCreMac(String creMac) {
		this.creMac = creMac;
	}
	/**
	 * @param delIPv4 설정하려는 delIPv4.
	 */
	public void setDelIPv4(String delIPv4) {
		this.delIPv4 = delIPv4;
	}
	/**
	 * @param delIPv6 설정하려는 delIPv6.
	 */
	public void setDelIPv6(String delIPv6) {
		this.delIPv6 = delIPv6;
	}
	/**
	 * @param delMac 설정하려는 delMac.
	 */
	public void setDelMac(String delMac) {
		this.delMac = delMac;
	}
	/**
	 * @param downAbs 설정하려는 downAbs.
	 */
	public void setDownAbs(String downAbs) {
		this.downAbs = downAbs;
	}
	/**
	 * @param downExt 설정하려는 downExt.
	 */
	public void setDownExt(String downExt) {
		this.downExt = downExt;
	}
	/**
	 * @param downRel 설정하려는 downRel.
	 */
	public void setDownRel(String downRel) {
		this.downRel = downRel;
	}
	/**
	 * @param downUUID 설정하려는 downUUID.
	 */
	public void setDownUUID(String downUUID) {
		this.downUUID = downUUID;
	}
	/**
	 * @param fileAbs 설정하려는 fileAbs.
	 */
	public void setFileAbs(String fileAbs) {
		this.fileAbs = fileAbs;
	}
	/**
	 * @param fileLinkD 설정하려는 fileLinkD.
	 */
	public void setFileLinkD(String fileLinkD) {
		this.fileLinkD = fileLinkD;
	}
	/**
	 * @param fileLinkM 설정하려는 fileLinkM.
	 */
	public void setFileLinkM(String fileLinkM) {
		this.fileLinkM = fileLinkM;
	}
	/**
	 * @param fileLinkS 설정하려는 fileLinkS.
	 */
	public void setFileLinkS(String fileLinkS) {
		this.fileLinkS = fileLinkS;
	}
	/**
	 * @param fileName 설정하려는 fileName.
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @param fileRel 설정하려는 fileRel.
	 */
	public void setFileRel(String fileRel) {
		this.fileRel = fileRel;
	}
	/**
	 * @param fileSize 설정하려는 fileSize.
	 */
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	/**
	 * @param fileUri 설정하려는 fileUri.
	 */
	public void setFileUri(String fileUri) {
		this.fileUri = fileUri;
	}
	/**
	 * @param liveAfter 설정하려는 liveAfter.
	 */
	public void setLiveAfter(String liveAfter) {
		this.liveAfter = liveAfter;
	}
	/**
	 * @param liveBefore 설정하려는 liveBefore.
	 */
	public void setLiveBefore(String liveBefore) {
		this.liveBefore = liveBefore;
	}
	/**
	 * @param livePresent 설정하려는 livePresent.
	 */
	public void setLivePresent(String livePresent) {
		this.livePresent = livePresent;
	}
	/**
	 * @param modIPv4 설정하려는 modIPv4.
	 */
	public void setModIPv4(String modIPv4) {
		this.modIPv4 = modIPv4;
	}
	/**
	 * @param modIPv6 설정하려는 modIPv6.
	 */
	public void setModIPv6(String modIPv6) {
		this.modIPv6 = modIPv6;
	}
	/**
	 * @param modMac 설정하려는 modMac.
	 */
	public void setModMac(String modMac) {
		this.modMac = modMac;
	}
	/**
	 * @param schdBDate 설정하려는 schdBDate.
	 */
	public void setSchdBDate(String schdBDate) {
		this.schdBDate = schdBDate;
	}
	/**
	 * @param schdBTime 설정하려는 schdBTime.
	 */
	public void setSchdBTime(String schdBTime) {
		this.schdBTime = schdBTime;
	}
	/**
	 * @param schdBTimH 설정하려는 schdBTimH.
	 */
	public void setSchdBTimH(String schdBTimH) {
		this.schdBTimH = schdBTimH;
	}
	/**
	 * @param schdBTimM 설정하려는 schdBTimM.
	 */
	public void setSchdBTimM(String schdBTimM) {
		this.schdBTimM = schdBTimM;
	}
	/**
	 * @param schdDesc 설정하려는 schdDesc.
	 */
	public void setSchdDesc(String schdDesc) {
		this.schdDesc = schdDesc;
	}
	/**
	 * @param schdDescBR 설정하려는 schdDescBR.
	 */
	public void setSchdDescBR(String schdDescBR) {
		this.schdDescBR = schdDescBR;
	}
	/**
	 * @param schdDescCR 설정하려는 schdDescCR.
	 */
	public void setSchdDescCR(String schdDescCR) {
		this.schdDescCR = schdDescCR;
	}
	/**
	 * @param schdEDate 설정하려는 schdEDate.
	 */
	public void setSchdEDate(String schdEDate) {
		this.schdEDate = schdEDate;
	}
	/**
	 * @param schdETime 설정하려는 schdETime.
	 */
	public void setSchdETime(String schdETime) {
		this.schdETime = schdETime;
	}
	/**
	 * @param schdETimH 설정하려는 schdETimH.
	 */
	public void setSchdETimH(String schdETimH) {
		this.schdETimH = schdETimH;
	}
	/**
	 * @param schdETimM 설정하려는 schdETimM.
	 */
	public void setSchdETimM(String schdETimM) {
		this.schdETimM = schdETimM;
	}
	/**
	 * @param schdIpcr 설정하려는 schdIpcr.
	 */
	public void setSchdIpcr(String schdIpcr) {
		this.schdIpcr = schdIpcr;
	}
	/**
	 * @param schdIpcrName 설정하려는 schdIpcrName.
	 */
	public void setSchdIpcrName(String schdIpcrName) {
		this.schdIpcrName = schdIpcrName;
	}
	/**
	 * @param schdKind 설정하려는 schdKind.
	 */
	public void setSchdKind(String schdKind) {
		this.schdKind = schdKind;
	}
	/**
	 * @param schdKindName 설정하려는 schdKindName.
	 */
	public void setSchdKindName(String schdKindName) {
		this.schdKindName = schdKindName;
	}
	/**
	 * @param schdPlace 설정하려는 schdPlace.
	 */
	public void setSchdPlace(String schdPlace) {
		this.schdPlace = schdPlace;
	}
	/**
	 * @param schdReptit 설정하려는 schdReptit.
	 */
	public void setSchdReptit(String schdReptit) {
		this.schdReptit = schdReptit;
	}
	/**
	 * @param schdReptitName 설정하려는 schdReptitName.
	 */
	public void setSchdReptitName(String schdReptitName) {
		this.schdReptitName = schdReptitName;
	}
	/**
	 * @param schdSTitle 설정하려는 schdSTitle.
	 */
	public void setSchdSTitle(String schdSTitle) {
		this.schdSTitle = schdSTitle;
	}
	/**
	 * @param schdTitle 설정하려는 schdTitle.
	 */
	public void setSchdTitle(String schdTitle) {
		this.schdTitle = schdTitle;
	}
	/**
	 * @param schdType 설정하려는 schdType.
	 */
	public void setSchdType(String schdType) {
		this.schdType = schdType;
	}
	/**
	 * @param schdTypeName 설정하려는 schdTypeName.
	 */
	public void setSchdTypeName(String schdTypeName) {
		this.schdTypeName = schdTypeName;
	}

}	// end of class