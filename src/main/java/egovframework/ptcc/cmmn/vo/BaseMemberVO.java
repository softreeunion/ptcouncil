/*******************************************************************************
  Program ID  : BaseMemberVO
  Description : 사용자 정보에 대한 Value Object를 정의한다
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.cmmn.vo;

@SuppressWarnings("serial")
public class BaseMemberVO extends BaseCommonVO implements java.io.Serializable { 

	// 공통 정보
	private String creIPv4;					// 
	private String creIPv6;					// 
	private String creMac;						// 
	private String modIPv4;					// 
	private String modIPv6;					// 
	private String modMac;						// 
	private String delIPv4;					// 
	private String delIPv6;					// 
	private String delMac;						// 

	// 사용자 정보
	private String k_username;					// 로그인 입력
	private String k_userpass;					// 로그인 입력
	private String j_username;					// 로그인 검증
	private String j_userpass;					// 로그인 검증
	private String o_username;					// 비번변경 입력
	private String o_userpass;					// 비번변경 입력
	private String p_username;					// 비번변경 검증
	private String p_userpass;					// 비번변경 검증
	private String n_username;					// 비번변경 입력
	private String n_userpass;					// 비번변경 입력
	private String c_username;					// 비번변경 검증
	private String c_userpass;					// 비번변경 검증
	private String userPNO;					// 
	private String userPNC;					// 
	private String userPWI;					// 
	private String userPWC;					// 
	private String userAccess;					// 
	private String userCate;					//
	private String userComm;


	/**
	 * @return c_username를(을) 리턴합니다.
	 */
	public String getC_username() {
		return c_username;
	}
	/**
	 * @return c_userpass를(을) 리턴합니다.
	 */
	public String getC_userpass() {
		return c_userpass;
	}
	/**
	 * @return creIPv4를(을) 리턴합니다.
	 */
	public String getCreIPv4() {
		return creIPv4;
	}
	/**
	 * @return creIPv6를(을) 리턴합니다.
	 */
	public String getCreIPv6() {
		return creIPv6;
	}
	/**
	 * @return creMac를(을) 리턴합니다.
	 */
	public String getCreMac() {
		return creMac;
	}
	/**
	 * @return delIPv4를(을) 리턴합니다.
	 */
	public String getDelIPv4() {
		return delIPv4;
	}
	/**
	 * @return delIPv6를(을) 리턴합니다.
	 */
	public String getDelIPv6() {
		return delIPv6;
	}
	/**
	 * @return delMac를(을) 리턴합니다.
	 */
	public String getDelMac() {
		return delMac;
	}
	/**
	 * @return j_username를(을) 리턴합니다.
	 */
	public String getJ_username() {
		return j_username;
	}
	/**
	 * @return j_userpass를(을) 리턴합니다.
	 */
	public String getJ_userpass() {
		return j_userpass;
	}
	/**
	 * @return k_username를(을) 리턴합니다.
	 */
	public String getK_username() {
		return k_username;
	}
	/**
	 * @return k_userpass를(을) 리턴합니다.
	 */
	public String getK_userpass() {
		return k_userpass;
	}
	/**
	 * @return modIPv4를(을) 리턴합니다.
	 */
	public String getModIPv4() {
		return modIPv4;
	}
	/**
	 * @return modIPv6를(을) 리턴합니다.
	 */
	public String getModIPv6() {
		return modIPv6;
	}
	/**
	 * @return modMac를(을) 리턴합니다.
	 */
	public String getModMac() {
		return modMac;
	}
	/**
	 * @return n_username를(을) 리턴합니다.
	 */
	public String getN_username() {
		return n_username;
	}
	/**
	 * @return n_userpass를(을) 리턴합니다.
	 */
	public String getN_userpass() {
		return n_userpass;
	}
	/**
	 * @return o_username를(을) 리턴합니다.
	 */
	public String getO_username() {
		return o_username;
	}
	/**
	 * @return o_userpass를(을) 리턴합니다.
	 */
	public String getO_userpass() {
		return o_userpass;
	}
	/**
	 * @return p_username를(을) 리턴합니다.
	 */
	public String getP_username() {
		return p_username;
	}
	/**
	 * @return p_userpass를(을) 리턴합니다.
	 */
	public String getP_userpass() {
		return p_userpass;
	}
	/**
	 * @return userAccess를(을) 리턴합니다.
	 */
	public String getUserAccess() {
		return userAccess;
	}
	/**
	 * @return userCate를(을) 리턴합니다.
	 */
	public String getUserCate() {
		return userCate;
	}
	/**
	 * @return userPNC를(을) 리턴합니다.
	 */
	public String getUserPNC() {
		return userPNC;
	}
	/**
	 * @return userPNO를(을) 리턴합니다.
	 */
	public String getUserPNO() {
		return userPNO;
	}
	/**
	 * @return userPWC를(을) 리턴합니다.
	 */
	public String getUserPWC() {
		return userPWC;
	}
	/**
	 * @return userPWI를(을) 리턴합니다.
	 */
	public String getUserPWI() {
		return userPWI;
	}
	/**
	 * @param c_username 설정하려는 c_username.
	 */
	public void setC_username(String c_username) {
		this.c_username = c_username;
	}
	/**
	 * @param c_userpass 설정하려는 c_userpass.
	 */
	public void setC_userpass(String c_userpass) {
		this.c_userpass = c_userpass;
	}
	/**
	 * @param creIPv4 설정하려는 creIPv4.
	 */
	public void setCreIPv4(String creIPv4) {
		this.creIPv4 = creIPv4;
	}
	/**
	 * @param creIPv6 설정하려는 creIPv6.
	 */
	public void setCreIPv6(String creIPv6) {
		this.creIPv6 = creIPv6;
	}
	/**
	 * @param creMac 설정하려는 creMac.
	 */
	public void setCreMac(String creMac) {
		this.creMac = creMac;
	}
	/**
	 * @param delIPv4 설정하려는 delIPv4.
	 */
	public void setDelIPv4(String delIPv4) {
		this.delIPv4 = delIPv4;
	}
	/**
	 * @param delIPv6 설정하려는 delIPv6.
	 */
	public void setDelIPv6(String delIPv6) {
		this.delIPv6 = delIPv6;
	}
	/**
	 * @param delMac 설정하려는 delMac.
	 */
	public void setDelMac(String delMac) {
		this.delMac = delMac;
	}
	/**
	 * @param j_username 설정하려는 j_username.
	 */
	public void setJ_username(String j_username) {
		this.j_username = j_username;
	}
	/**
	 * @param j_userpass 설정하려는 j_userpass.
	 */
	public void setJ_userpass(String j_userpass) {
		this.j_userpass = j_userpass;
	}
	/**
	 * @param k_username 설정하려는 k_username.
	 */
	public void setK_username(String k_username) {
		this.k_username = k_username;
	}
	/**
	 * @param k_userpass 설정하려는 k_userpass.
	 */
	public void setK_userpass(String k_userpass) {
		this.k_userpass = k_userpass;
	}
	/**
	 * @param modIPv4 설정하려는 modIPv4.
	 */
	public void setModIPv4(String modIPv4) {
		this.modIPv4 = modIPv4;
	}
	/**
	 * @param modIPv6 설정하려는 modIPv6.
	 */
	public void setModIPv6(String modIPv6) {
		this.modIPv6 = modIPv6;
	}
	/**
	 * @param modMac 설정하려는 modMac.
	 */
	public void setModMac(String modMac) {
		this.modMac = modMac;
	}
	/**
	 * @param n_username 설정하려는 n_username.
	 */
	public void setN_username(String n_username) {
		this.n_username = n_username;
	}
	/**
	 * @param n_userpass 설정하려는 n_userpass.
	 */
	public void setN_userpass(String n_userpass) {
		this.n_userpass = n_userpass;
	}
	/**
	 * @param o_username 설정하려는 o_username.
	 */
	public void setO_username(String o_username) {
		this.o_username = o_username;
	}
	/**
	 * @param o_userpass 설정하려는 o_userpass.
	 */
	public void setO_userpass(String o_userpass) {
		this.o_userpass = o_userpass;
	}
	/**
	 * @param p_username 설정하려는 p_username.
	 */
	public void setP_username(String p_username) {
		this.p_username = p_username;
	}
	/**
	 * @param p_userpass 설정하려는 p_userpass.
	 */
	public void setP_userpass(String p_userpass) {
		this.p_userpass = p_userpass;
	}
	/**
	 * @param userAccess 설정하려는 userAccess.
	 */
	public void setUserAccess(String userAccess) {
		this.userAccess = userAccess;
	}
	/**
	 * @param userCate 설정하려는 userCate.
	 */
	public void setUserCate(String userCate) {
		this.userCate = userCate;
	}
	/**
	 * @param userPNC 설정하려는 userPNC.
	 */
	public void setUserPNC(String userPNC) {
		this.userPNC = userPNC;
	}
	/**
	 * @param userPNO 설정하려는 userPNO.
	 */
	public void setUserPNO(String userPNO) {
		this.userPNO = userPNO;
	}
	/**
	 * @param userPWC 설정하려는 userPWC.
	 */
	public void setUserPWC(String userPWC) {
		this.userPWC = userPWC;
	}
	/**
	 * @param userPWI 설정하려는 userPWI.
	 */
	public void setUserPWI(String userPWI) {
		this.userPWI = userPWI;
	}

	public String getUserComm() {
		return userComm;
	}
	public void setUserComm(String userComm) {
		this.userComm = userComm;
	}
}	// end of class