/*******************************************************************************
  Program ID  : BaseFilesVO
  Description : 통합파일 정보에 대한 Value Object를 정의한다
  Author      : (주)엠씨에스텍 오영덕
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    (주)엠씨에스텍 오영덕  2019.07.01  최초 생성
  v1.0    (주)엠씨에스텍 오영덕  2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.cmmn.vo;

import java.util.List;

@SuppressWarnings("serial")
public class BasePageVO extends BaseCommonVO implements java.io.Serializable {

	private String page;
	private String contents;
	private String cont_desc;

	public String getPage() { return page; }
	public String getContents() { return contents; }
	public String getContDesc() { return cont_desc; }

	public void setPage(String page) {
		this.page = page;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public void setContDesc(String cont_desc) {
		this.cont_desc = cont_desc;
	}

}	// end of class