/*******************************************************************************
  Program ID  : BaseAgendaVO
  Description : 회의록 정보에 대한 Value Object를 정의한다
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v1.0    (주)헥사미디어 개발자  2014.00.00  최초 생성
  v2.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.cmmn.vo;

import java.util.List;

import egovframework.com.cmm.util.BaseUtility;

@SuppressWarnings("serial")
public class BaseAgendaVO extends BaseCommonVO implements java.io.Serializable { 

	// 공통 정보
	private String creIPv4;					// 
	private String creIPv6;					// 
	private String creMac;						// 
	private String modIPv4;					// 
	private String modIPv6;					// 
	private String modMac;						// 
	private String delIPv4;					// 
	private String delIPv6;					// 
	private String delMac;						// 

	// 의원 정보
	private String memSeq;
	private String memEng;
	private String memChi;
	private String memJumin;
	private String memBirth;
	private String memHome;
	private String memOffice;
	private String memMobile;
	private String memEmail;
	private String memAddr;
	private String zipCode;
	private String memParty;
	private String memIntro;
	private String memProfile;
	private String memUri;
	private String memPic;
	private String memVod;
	private String memPromise;
	private String memOrder;

	private String memIntroBR;
	private String memIntroCR;
	private String memProfileBR;
	private String memProfileCR;
	private String memPromiseBR;
	private String memPromiseCR;

	// 회의기본 정보
	private String confDate;
	private String confWeek;
	private String confPlace;
	private String confStartTime;
	private String confCloseTime;
	private String confReqTime;
	private String confService;
	private String confOrder;
	private String confClose;
	private String confMedia;
	private String confData;
	private String tempFlag;
	private String userId;

	// 회의관련 정보
	private String startDate;
	private String closeDate;
	private String eraStartDate;
	private String eraCloseDate;
	private String roundStartDate;
	private String roundCloseDate;

	// 회의의안 정보
	private String sugSeq;
	private String sugTitle;
	private String billNum;
	private String billTitle;
	private String sugMan;
	private String sugDate;
	private String comCon;
	private String comRep;
	private String comIntro;
	private String comVote;
	private String comRes;
	private String genCon;
	private String genRep;
	private String genIntro;
	private String genVote;
	private String genRes;
	private String tranDate;
	private String publDate;
	private String publNum;
	private String etc;
	private String contents;
	private String timeStamp;
	private String timeMemb;
	private String timePos;
	private String sugPosi;

	private String comResName;
	private String genResName;
	private String contentsCR;
	private String contentsBR;
	private String sugWork;

	// 회의부록 정보
	private String appSeq;
	private String appNum;
	private String appName;
	private String appTitle;
	private String appSub;
	private String appExt;

	private String fileLinkS;					// 첨부파일 내용 (싱글 다운용)
	private String fileLinkM;					// 첨부파일 내용 (멀티 다운용)
	private String fileLinkD;					// 첨부파일 내용 (일반 표시용)
	private String fileID;						// 
	private String downUUID;
	private String downRel;
	private String downAbs;
	private String downExt;

	// 검색관련
	private String bgnClass;
	private String endClass;
	private String bgnRound;
	private String endRound;
	private String eraTitle;
	private String eraCount;
	private String rndType;
	private String rndCode;
	private String rndName;
	private String rndTitle;
	private String rndCount;
	private String divName;
	private String kndType;
	private String kndCode;
	private String kndName;
	private String kndTitle;
	private String kndCount;
	private String pntType;
	private String pntCode;
	private String pntName;
	private String pntTitle;
	private String pntCount;
	private String insType;
	private String insCode;
	private String insName;
	private String insTitle;
	private String insCount;
	private String yosType;
	private String yosCode;
	private String yosName;
	private String yosTitle;
	private String yosCount;

	// 통계관련
	private String rndDiffDate;
	private String knd000;
	private String knd100;
	private String knd200;
	private String knd300;
	private String knd400;
	private String knd500;
	private String knd600;
	private String knd700;
	private String knd800;
	private String knd900;
	private String codSpace;
	private String codNull;
	private String cod0000;
	private String cod0010;
	private String cod0020;
	private String cod0030;
	private String cod0040;
	private String cod0050;
	private String cod0051;
	private String cod0052;
	private String cod0053;
	private String cod0060;
	private String cod0070;
	private String cod0080;
	private String cod0090;

	@SuppressWarnings("rawtypes")
	List childList;
	@SuppressWarnings("rawtypes")
	List childInfo;
	@SuppressWarnings("rawtypes")
	List childItem;
	@SuppressWarnings("rawtypes")
	List childName;
	@SuppressWarnings("rawtypes")
	List childTime;
	@SuppressWarnings("rawtypes")
	List childConf;


	/**
	 * @return appExt를(을) 리턴합니다.
	 */
	public String getAppExt() {
		return BaseUtility.null2Blank( appExt );
	}
	/**
	 * @return appName를(을) 리턴합니다.
	 */
	public String getAppName() {
		return BaseUtility.null2Blank( appName );
	}
	/**
	 * @return appNum를(을) 리턴합니다.
	 */
	public String getAppNum() {
		return BaseUtility.null2Blank( appNum );
	}
	/**
	 * @return appSeq를(을) 리턴합니다.
	 */
	public String getAppSeq() {
		return BaseUtility.null2Blank( appSeq );
	}
	/**
	 * @return appSub를(을) 리턴합니다.
	 */
	public String getAppSub() {
		return BaseUtility.null2Blank( appSub );
	}
	/**
	 * @return appTitle를(을) 리턴합니다.
	 */
	public String getAppTitle() {
		return BaseUtility.null2Blank( appTitle );
	}
	/**
	 * @return bgnClass를(을) 리턴합니다.
	 */
	public String getBgnClass() {
		return BaseUtility.null2Blank( bgnClass );
	}
	/**
	 * @return bgnRound를(을) 리턴합니다.
	 */
	public String getBgnRound() {
		return BaseUtility.null2Blank( bgnRound );
	}
	/**
	 * @return billNum를(을) 리턴합니다.
	 */
	public String getBillNum() {
		return BaseUtility.null2Blank( billNum );
	}
	/**
	 * @return billTitle를(을) 리턴합니다.
	 */
	public String getBillTitle() {
		return BaseUtility.null2Blank( billTitle );
	}
	/**
	 * @return childConf를(을) 리턴합니다.
	 */
	@SuppressWarnings("rawtypes")
	public List getChildConf() {
		return childConf;
	}
	/**
	 * @return childInfo를(을) 리턴합니다.
	 */
	@SuppressWarnings("rawtypes")
	public List getChildInfo() {
		return childInfo;
	}
	/**
	 * @return childItem를(을) 리턴합니다.
	 */
	@SuppressWarnings("rawtypes")
	public List getChildItem() {
		return childItem;
	}
	/**
	 * @return childList를(을) 리턴합니다.
	 */
	@SuppressWarnings("rawtypes")
	public List getChildList() {
		return childList;
	}
	/**
	 * @return childName를(을) 리턴합니다.
	 */
	@SuppressWarnings("rawtypes")
	public List getChildName() {
		return childName;
	}
	/**
	 * @return childTime를(을) 리턴합니다.
	 */
	@SuppressWarnings("rawtypes")
	public List getChildTime() {
		return childTime;
	}
	/**
	 * @return closeDate를(을) 리턴합니다.
	 */
	public String getCloseDate() {
		return BaseUtility.null2Blank( closeDate );
	}
	/**
	 * @return cod0000를(을) 리턴합니다.
	 */
	public String getCod0000() {
		return BaseUtility.null2Blank( cod0000 );
	}
	/**
	 * @return cod0010를(을) 리턴합니다.
	 */
	public String getCod0010() {
		return BaseUtility.null2Blank( cod0010 );
	}
	/**
	 * @return cod0020를(을) 리턴합니다.
	 */
	public String getCod0020() {
		return BaseUtility.null2Blank( cod0020 );
	}
	/**
	 * @return cod0030를(을) 리턴합니다.
	 */
	public String getCod0030() {
		return BaseUtility.null2Blank( cod0030 );
	}
	/**
	 * @return cod0040를(을) 리턴합니다.
	 */
	public String getCod0040() {
		return BaseUtility.null2Blank( cod0040 );
	}
	/**
	 * @return cod0050를(을) 리턴합니다.
	 */
	public String getCod0050() {
		return BaseUtility.null2Blank( cod0050 );
	}
	/**
	 * @return cod0051를(을) 리턴합니다.
	 */
	public String getCod0051() {
		return BaseUtility.null2Blank( cod0051 );
	}
	/**
	 * @return cod0052를(을) 리턴합니다.
	 */
	public String getCod0052() {
		return BaseUtility.null2Blank( cod0052 );
	}
	/**
	 * @return cod0053를(을) 리턴합니다.
	 */
	public String getCod0053() {
		return BaseUtility.null2Blank( cod0053 );
	}
	/**
	 * @return cod0060를(을) 리턴합니다.
	 */
	public String getCod0060() {
		return BaseUtility.null2Blank( cod0060 );
	}
	/**
	 * @return cod0070를(을) 리턴합니다.
	 */
	public String getCod0070() {
		return BaseUtility.null2Blank( cod0070 );
	}
	/**
	 * @return cod0080를(을) 리턴합니다.
	 */
	public String getCod0080() {
		return BaseUtility.null2Blank( cod0080 );
	}
	/**
	 * @return cod0090를(을) 리턴합니다.
	 */
	public String getCod0090() {
		return BaseUtility.null2Blank( cod0090 );
	}
	/**
	 * @return codNull를(을) 리턴합니다.
	 */
	public String getCodNull() {
		return BaseUtility.null2Blank( codNull );
	}
	/**
	 * @return codSpace를(을) 리턴합니다.
	 */
	public String getCodSpace() {
		return BaseUtility.null2Blank( codSpace );
	}
	/**
	 * @return comCon를(을) 리턴합니다.
	 */
	public String getComCon() {
		return BaseUtility.null2Blank( comCon );
	}
	/**
	 * @return comIntro를(을) 리턴합니다.
	 */
	public String getComIntro() {
		return BaseUtility.null2Blank( comIntro );
	}
	/**
	 * @return comRep를(을) 리턴합니다.
	 */
	public String getComRep() {
		return BaseUtility.null2Blank( comRep );
	}
	/**
	 * @return comRes를(을) 리턴합니다.
	 */
	public String getComRes() {
		return BaseUtility.null2Blank( comRes );
	}
	/**
	 * @return comResName를(을) 리턴합니다.
	 */
	public String getComResName() {
		return BaseUtility.null2Blank( comResName );
	}
	/**
	 * @return comVote를(을) 리턴합니다.
	 */
	public String getComVote() {
		return BaseUtility.null2Blank( comVote );
	}
	/**
	 * @return confClose를(을) 리턴합니다.
	 */
	public String getConfClose() {
		return BaseUtility.null2Blank( confClose );
	}
	/**
	 * @return confCloseTime를(을) 리턴합니다.
	 */
	public String getConfCloseTime() {
		return BaseUtility.null2Blank( confCloseTime );
	}
	/**
	 * @return confData를(을) 리턴합니다.
	 */
	public String getConfData() {
		return BaseUtility.null2Blank( confData );
	}
	/**
	 * @return confDate를(을) 리턴합니다.
	 */
	public String getConfDate() {
		return BaseUtility.null2Blank( confDate );
	}
	/**
	 * @return confMedia를(을) 리턴합니다.
	 */
	public String getConfMedia() {
		return BaseUtility.null2Blank( confMedia );
	}
	/**
	 * @return confOrder를(을) 리턴합니다.
	 */
	public String getConfOrder() {
		return BaseUtility.null2Blank( confOrder );
	}
	/**
	 * @return confPlace를(을) 리턴합니다.
	 */
	public String getConfPlace() {
		return BaseUtility.null2Blank( confPlace );
	}
	/**
	 * @return confReqTime를(을) 리턴합니다.
	 */
	public String getConfReqTime() {
		return BaseUtility.null2Blank( confReqTime );
	}
	/**
	 * @return confService를(을) 리턴합니다.
	 */
	public String getConfService() {
		return BaseUtility.null2Blank( confService );
	}
	/**
	 * @return confStartTime를(을) 리턴합니다.
	 */
	public String getConfStartTime() {
		return BaseUtility.null2Blank( confStartTime );
	}
	/**
	 * @return confWeek를(을) 리턴합니다.
	 */
	public String getConfWeek() {
		return BaseUtility.null2Blank( confWeek );
	}
	/**
	 * @return contents를(을) 리턴합니다.
	 */
	public String getContents() {
		return BaseUtility.null2Blank( contents );
	}
	/**
	 * @return contentsBR를(을) 리턴합니다.
	 */
	public String getContentsBR() {
		return BaseUtility.null2Blank( contentsBR );
	}
	/**
	 * @return contentsCR를(을) 리턴합니다.
	 */
	public String getContentsCR() {
		return BaseUtility.null2Blank( contentsCR );
	}
	/**
	 * @return creIPv4를(을) 리턴합니다.
	 */
	public String getCreIPv4() {
		return BaseUtility.null2Blank( creIPv4 );
	}
	/**
	 * @return creIPv6를(을) 리턴합니다.
	 */
	public String getCreIPv6() {
		return BaseUtility.null2Blank( creIPv6 );
	}
	/**
	 * @return creMac를(을) 리턴합니다.
	 */
	public String getCreMac() {
		return BaseUtility.null2Blank( creMac );
	}
	/**
	 * @return delIPv4를(을) 리턴합니다.
	 */
	public String getDelIPv4() {
		return BaseUtility.null2Blank( delIPv4 );
	}
	/**
	 * @return delIPv6를(을) 리턴합니다.
	 */
	public String getDelIPv6() {
		return BaseUtility.null2Blank( delIPv6 );
	}
	/**
	 * @return delMac를(을) 리턴합니다.
	 */
	public String getDelMac() {
		return BaseUtility.null2Blank( delMac );
	}
	/**
	 * @return divName를(을) 리턴합니다.
	 */
	public String getDivName() {
		return BaseUtility.null2Blank( divName );
	}
	/**
	 * @return downAbs를(을) 리턴합니다.
	 */
	public String getDownAbs() {
		return BaseUtility.null2Blank( downAbs );
	}
	/**
	 * @return downExt를(을) 리턴합니다.
	 */
	public String getDownExt() {
		return BaseUtility.null2Blank( downExt );
	}
	/**
	 * @return downRel를(을) 리턴합니다.
	 */
	public String getDownRel() {
		return BaseUtility.null2Blank( downRel );
	}
	/**
	 * @return downUUID를(을) 리턴합니다.
	 */
	public String getDownUUID() {
		return BaseUtility.null2Blank( downUUID );
	}
	/**
	 * @return endClass를(을) 리턴합니다.
	 */
	public String getEndClass() {
		return BaseUtility.null2Blank( endClass );
	}
	/**
	 * @return endRound를(을) 리턴합니다.
	 */
	public String getEndRound() {
		return BaseUtility.null2Blank( endRound );
	}
	/**
	 * @return eraCloseDate를(을) 리턴합니다.
	 */
	public String getEraCloseDate() {
		return BaseUtility.null2Blank( eraCloseDate );
	}
	/**
	 * @return eraCount를(을) 리턴합니다.
	 */
	public String getEraCount() {
		return BaseUtility.null2Blank( eraCount );
	}
	/**
	 * @return eraStartDate를(을) 리턴합니다.
	 */
	public String getEraStartDate() {
		return BaseUtility.null2Blank( eraStartDate );
	}
	/**
	 * @return eraTitle를(을) 리턴합니다.
	 */
	public String getEraTitle() {
		return BaseUtility.null2Blank( eraTitle );
	}
	/**
	 * @return etc를(을) 리턴합니다.
	 */
	public String getEtc() {
		return BaseUtility.null2Blank( etc );
	}
	/**
	 * @return fileID를(을) 리턴합니다.
	 */
	public String getFileID() {
		return BaseUtility.null2Blank( fileID );
	}
	/**
	 * @return fileLinkD를(을) 리턴합니다.
	 */
	public String getFileLinkD() {
		return BaseUtility.null2Blank( fileLinkD );
	}
	/**
	 * @return fileLinkM를(을) 리턴합니다.
	 */
	public String getFileLinkM() {
		return BaseUtility.null2Blank( fileLinkM );
	}
	/**
	 * @return fileLinkS를(을) 리턴합니다.
	 */
	public String getFileLinkS() {
		return BaseUtility.null2Blank( fileLinkS );
	}
	/**
	 * @return genCon를(을) 리턴합니다.
	 */
	public String getGenCon() {
		return BaseUtility.null2Blank( genCon );
	}
	/**
	 * @return genIntro를(을) 리턴합니다.
	 */
	public String getGenIntro() {
		return BaseUtility.null2Blank( genIntro );
	}
	/**
	 * @return genRep를(을) 리턴합니다.
	 */
	public String getGenRep() {
		return BaseUtility.null2Blank( genRep );
	}
	/**
	 * @return genRes를(을) 리턴합니다.
	 */
	public String getGenRes() {
		return BaseUtility.null2Blank( genRes );
	}
	/**
	 * @return genResName를(을) 리턴합니다.
	 */
	public String getGenResName() {
		return BaseUtility.null2Blank( genResName );
	}
	/**
	 * @return genVote를(을) 리턴합니다.
	 */
	public String getGenVote() {
		return BaseUtility.null2Blank( genVote );
	}
	/**
	 * @return insCode를(을) 리턴합니다.
	 */
	public String getInsCode() {
		return BaseUtility.null2Blank( insCode );
	}
	/**
	 * @return insCount를(을) 리턴합니다.
	 */
	public String getInsCount() {
		return BaseUtility.null2Blank( insCount );
	}
	/**
	 * @return insName를(을) 리턴합니다.
	 */
	public String getInsName() {
		return BaseUtility.null2Blank( insName );
	}
	/**
	 * @return insTitle를(을) 리턴합니다.
	 */
	public String getInsTitle() {
		return BaseUtility.null2Blank( insTitle );
	}
	/**
	 * @return insType를(을) 리턴합니다.
	 */
	public String getInsType() {
		return BaseUtility.null2Blank( insType );
	}
	/**
	 * @return knd000를(을) 리턴합니다.
	 */
	public String getKnd000() {
		return BaseUtility.null2Blank( knd000 );
	}
	/**
	 * @return knd100를(을) 리턴합니다.
	 */
	public String getKnd100() {
		return BaseUtility.null2Blank( knd100 );
	}
	/**
	 * @return knd200를(을) 리턴합니다.
	 */
	public String getKnd200() {
		return BaseUtility.null2Blank( knd200 );
	}
	/**
	 * @return knd300를(을) 리턴합니다.
	 */
	public String getKnd300() {
		return BaseUtility.null2Blank( knd300 );
	}
	/**
	 * @return knd400를(을) 리턴합니다.
	 */
	public String getKnd400() {
		return BaseUtility.null2Blank( knd400 );
	}
	/**
	 * @return knd500를(을) 리턴합니다.
	 */
	public String getKnd500() {
		return BaseUtility.null2Blank( knd500 );
	}
	/**
	 * @return knd600를(을) 리턴합니다.
	 */
	public String getKnd600() {
		return BaseUtility.null2Blank( knd600 );
	}
	/**
	 * @return knd700를(을) 리턴합니다.
	 */
	public String getKnd700() {
		return BaseUtility.null2Blank( knd700 );
	}
	/**
	 * @return knd800를(을) 리턴합니다.
	 */
	public String getKnd800() {
		return BaseUtility.null2Blank( knd800 );
	}
	/**
	 * @return knd900를(을) 리턴합니다.
	 */
	public String getKnd900() {
		return BaseUtility.null2Blank( knd900 );
	}
	/**
	 * @return kndCode를(을) 리턴합니다.
	 */
	public String getKndCode() {
		return BaseUtility.null2Blank( kndCode );
	}
	/**
	 * @return kndCount를(을) 리턴합니다.
	 */
	public String getKndCount() {
		return BaseUtility.null2Blank( kndCount );
	}
	/**
	 * @return kndName를(을) 리턴합니다.
	 */
	public String getKndName() {
		return BaseUtility.null2Blank( kndName );
	}
	/**
	 * @return kndTitle를(을) 리턴합니다.
	 */
	public String getKndTitle() {
		return BaseUtility.null2Blank( kndTitle );
	}
	/**
	 * @return kndType를(을) 리턴합니다.
	 */
	public String getKndType() {
		return BaseUtility.null2Blank( kndType );
	}
	/**
	 * @return memAddr를(을) 리턴합니다.
	 */
	public String getMemAddr() {
		return BaseUtility.null2Blank( memAddr );
	}
	/**
	 * @return memBirth를(을) 리턴합니다.
	 */
	public String getMemBirth() {
		return BaseUtility.null2Blank( memBirth );
	}
	/**
	 * @return memChi를(을) 리턴합니다.
	 */
	public String getMemChi() {
		return BaseUtility.null2Blank( memChi );
	}
	/**
	 * @return memEmail를(을) 리턴합니다.
	 */
	public String getMemEmail() {
		return BaseUtility.null2Blank( memEmail );
	}
	/**
	 * @return memEng를(을) 리턴합니다.
	 */
	public String getMemEng() {
		return BaseUtility.null2Blank( memEng );
	}
	/**
	 * @return memHome를(을) 리턴합니다.
	 */
	public String getMemHome() {
		return BaseUtility.null2Blank( memHome );
	}
	/**
	 * @return memIntro를(을) 리턴합니다.
	 */
	public String getMemIntro() {
		return BaseUtility.null2Blank( memIntro );
	}
	/**
	 * @return memIntroBR를(을) 리턴합니다.
	 */
	public String getMemIntroBR() {
		return BaseUtility.null2Blank( memIntroBR );
	}
	/**
	 * @return memIntroCR를(을) 리턴합니다.
	 */
	public String getMemIntroCR() {
		return BaseUtility.null2Blank( memIntroCR );
	}
	/**
	 * @return memJumin를(을) 리턴합니다.
	 */
	public String getMemJumin() {
		return BaseUtility.null2Blank( memJumin );
	}
	/**
	 * @return memMobile를(을) 리턴합니다.
	 */
	public String getMemMobile() {
		return BaseUtility.null2Blank( memMobile );
	}
	/**
	 * @return memOffice를(을) 리턴합니다.
	 */
	public String getMemOffice() {
		return BaseUtility.null2Blank( memOffice );
	}
	/**
	 * @return memOrder를(을) 리턴합니다.
	 */
	public String getMemOrder() {
		return BaseUtility.null2Blank( memOrder );
	}
	/**
	 * @return memParty를(을) 리턴합니다.
	 */
	public String getMemParty() {
		return BaseUtility.null2Blank( memParty );
	}
	/**
	 * @return memPic를(을) 리턴합니다.
	 */
	public String getMemPic() {
		return BaseUtility.null2Blank( memPic );
	}
	/**
	 * @return memProfile를(을) 리턴합니다.
	 */
	public String getMemProfile() {
		return BaseUtility.null2Blank( memProfile );
	}
	/**
	 * @return memProfileBR를(을) 리턴합니다.
	 */
	public String getMemProfileBR() {
		return BaseUtility.null2Blank( memProfileBR );
	}
	/**
	 * @return memProfileCR를(을) 리턴합니다.
	 */
	public String getMemProfileCR() {
		return BaseUtility.null2Blank( memProfileCR );
	}
	/**
	 * @return memPromise를(을) 리턴합니다.
	 */
	public String getMemPromise() {
		return BaseUtility.null2Blank( memPromise );
	}
	/**
	 * @return memPromiseBR를(을) 리턴합니다.
	 */
	public String getMemPromiseBR() {
		return BaseUtility.null2Blank( memPromiseBR );
	}
	/**
	 * @return memPromiseCR를(을) 리턴합니다.
	 */
	public String getMemPromiseCR() {
		return BaseUtility.null2Blank( memPromiseCR );
	}
	/**
	 * @return memSeq를(을) 리턴합니다.
	 */
	public String getMemSeq() {
		return BaseUtility.null2Blank( memSeq );
	}
	/**
	 * @return memUri를(을) 리턴합니다.
	 */
	public String getMemUri() {
		return BaseUtility.null2Blank( memUri );
	}
	/**
	 * @return memVod를(을) 리턴합니다.
	 */
	public String getMemVod() {
		return BaseUtility.null2Blank( memVod );
	}
	/**
	 * @return modIPv4를(을) 리턴합니다.
	 */
	public String getModIPv4() {
		return BaseUtility.null2Blank( modIPv4 );
	}
	/**
	 * @return modIPv6를(을) 리턴합니다.
	 */
	public String getModIPv6() {
		return BaseUtility.null2Blank( modIPv6 );
	}
	/**
	 * @return modMac를(을) 리턴합니다.
	 */
	public String getModMac() {
		return BaseUtility.null2Blank( modMac );
	}
	/**
	 * @return pntCode를(을) 리턴합니다.
	 */
	public String getPntCode() {
		return BaseUtility.null2Blank( pntCode );
	}
	/**
	 * @return pntCount를(을) 리턴합니다.
	 */
	public String getPntCount() {
		return BaseUtility.null2Blank( pntCount );
	}
	/**
	 * @return pntName를(을) 리턴합니다.
	 */
	public String getPntName() {
		return BaseUtility.null2Blank( pntName );
	}
	/**
	 * @return pntTitle를(을) 리턴합니다.
	 */
	public String getPntTitle() {
		return BaseUtility.null2Blank( pntTitle );
	}
	/**
	 * @return pntType를(을) 리턴합니다.
	 */
	public String getPntType() {
		return BaseUtility.null2Blank( pntType );
	}
	/**
	 * @return publDate를(을) 리턴합니다.
	 */
	public String getPublDate() {
		return BaseUtility.null2Blank( publDate );
	}
	/**
	 * @return publNum를(을) 리턴합니다.
	 */
	public String getPublNum() {
		return BaseUtility.null2Blank( publNum );
	}
	/**
	 * @return rndCode를(을) 리턴합니다.
	 */
	public String getRndCode() {
		return BaseUtility.null2Blank( rndCode );
	}
	/**
	 * @return rndCount를(을) 리턴합니다.
	 */
	public String getRndCount() {
		return BaseUtility.null2Blank( rndCount );
	}
	/**
	 * @return rndDiffDate를(을) 리턴합니다.
	 */
	public String getRndDiffDate() {
		return BaseUtility.null2Blank( rndDiffDate );
	}
	/**
	 * @return rndName를(을) 리턴합니다.
	 */
	public String getRndName() {
		return BaseUtility.null2Blank( rndName );
	}
	/**
	 * @return rndTitle를(을) 리턴합니다.
	 */
	public String getRndTitle() {
		return BaseUtility.null2Blank( rndTitle );
	}
	/**
	 * @return rndType를(을) 리턴합니다.
	 */
	public String getRndType() {
		return BaseUtility.null2Blank( rndType );
	}
	/**
	 * @return roundCloseDate를(을) 리턴합니다.
	 */
	public String getRoundCloseDate() {
		return BaseUtility.null2Blank( roundCloseDate );
	}
	/**
	 * @return roundStartDate를(을) 리턴합니다.
	 */
	public String getRoundStartDate() {
		return BaseUtility.null2Blank( roundStartDate );
	}
	/**
	 * @return startDate를(을) 리턴합니다.
	 */
	public String getStartDate() {
		return BaseUtility.null2Blank( startDate );
	}
	/**
	 * @return sugDate를(을) 리턴합니다.
	 */
	public String getSugDate() {
		return BaseUtility.null2Blank( sugDate );
	}
	/**
	 * @return sugMan를(을) 리턴합니다.
	 */
	public String getSugMan() {
		return BaseUtility.null2Blank( sugMan );
	}
	/**
	 * @return sugPosi를(을) 리턴합니다.
	 */
	public String getSugPosi() {
		return BaseUtility.null2Blank( sugPosi );
	}
	/**
	 * @return sugTitle를(을) 리턴합니다.
	 */
	public String getSugTitle() {
		return BaseUtility.null2Blank( sugTitle );
	}
	/**
	 * @return sugWork를(을) 리턴합니다.
	 */
	public String getSugWork() {
		return BaseUtility.null2Blank( sugWork );
	}
	/**
	 * @return tempFlag를(을) 리턴합니다.
	 */
	public String getTempFlag() {
		return BaseUtility.null2Blank( tempFlag );
	}
	/**
	 * @return timeMemb를(을) 리턴합니다.
	 */
	public String getTimeMemb() {
		return BaseUtility.null2Blank( timeMemb );
	}
	/**
	 * @return timePos를(을) 리턴합니다.
	 */
	public String getTimePos() {
		return BaseUtility.null2Blank( timePos );
	}
	/**
	 * @return timeStamp를(을) 리턴합니다.
	 */
	public String getTimeStamp() {
		return BaseUtility.null2Blank( timeStamp );
	}
	/**
	 * @return tranDate를(을) 리턴합니다.
	 */
	public String getTranDate() {
		return BaseUtility.null2Blank( tranDate );
	}
	/**
	 * @return userId를(을) 리턴합니다.
	 */
	public String getUserId() {
		return BaseUtility.null2Blank( userId );
	}
	/**
	 * @return yosCode를(을) 리턴합니다.
	 */
	public String getYosCode() {
		return BaseUtility.null2Blank( yosCode );
	}
	/**
	 * @return yosCount를(을) 리턴합니다.
	 */
	public String getYosCount() {
		return BaseUtility.null2Blank( yosCount );
	}
	/**
	 * @return yosName를(을) 리턴합니다.
	 */
	public String getYosName() {
		return BaseUtility.null2Blank( yosName );
	}
	/**
	 * @return yosTitle를(을) 리턴합니다.
	 */
	public String getYosTitle() {
		return BaseUtility.null2Blank( yosTitle );
	}
	/**
	 * @return yosType를(을) 리턴합니다.
	 */
	public String getYosType() {
		return BaseUtility.null2Blank( yosType );
	}
	/**
	 * @return zipCode를(을) 리턴합니다.
	 */
	public String getZipCode() {
		return BaseUtility.null2Blank( zipCode );
	}
	/**
	 * @param appExt 설정하려는 appExt.
	 */
	public void setAppExt(String appExt) {
		this.appExt = appExt;
	}
	/**
	 * @param appName 설정하려는 appName.
	 */
	public void setAppName(String appName) {
		this.appName = appName;
	}
	/**
	 * @param appNum 설정하려는 appNum.
	 */
	public void setAppNum(String appNum) {
		this.appNum = appNum;
	}
	/**
	 * @param appSeq 설정하려는 appSeq.
	 */
	public void setAppSeq(String appSeq) {
		this.appSeq = appSeq;
	}
	/**
	 * @param appSub 설정하려는 appSub.
	 */
	public void setAppSub(String appSub) {
		this.appSub = appSub;
	}
	/**
	 * @param appTitle 설정하려는 appTitle.
	 */
	public void setAppTitle(String appTitle) {
		this.appTitle = appTitle;
	}
	/**
	 * @param bgnClass 설정하려는 bgnClass.
	 */
	public void setBgnClass(String bgnClass) {
		this.bgnClass = bgnClass;
	}
	/**
	 * @param bgnRound 설정하려는 bgnRound.
	 */
	public void setBgnRound(String bgnRound) {
		this.bgnRound = bgnRound;
	}
	/**
	 * @param billNum 설정하려는 billNum.
	 */
	public void setBillNum(String billNum) {
		this.billNum = billNum;
	}
	/**
	 * @param billTitle 설정하려는 billTitle.
	 */
	public void setBillTitle(String billTitle) {
		this.billTitle = billTitle;
	}
	/**
	 * @param childConf 설정하려는 childConf.
	 */
	@SuppressWarnings("rawtypes")
	public void setChildConf(List childConf) {
		this.childConf = childConf;
	}
	/**
	 * @param childInfo 설정하려는 childInfo.
	 */
	@SuppressWarnings("rawtypes")
	public void setChildInfo(List childInfo) {
		this.childInfo = childInfo;
	}
	/**
	 * @param childItem 설정하려는 childItem.
	 */
	@SuppressWarnings("rawtypes")
	public void setChildItem(List childItem) {
		this.childItem = childItem;
	}
	/**
	 * @param childList 설정하려는 childList.
	 */
	@SuppressWarnings("rawtypes")
	public void setChildList(List childList) {
		this.childList = childList;
	}
	/**
	 * @param childName 설정하려는 childName.
	 */
	@SuppressWarnings("rawtypes")
	public void setChildName(List childName) {
		this.childName = childName;
	}
	/**
	 * @param childTime 설정하려는 childTime.
	 */
	@SuppressWarnings("rawtypes")
	public void setChildTime(List childTime) {
		this.childTime = childTime;
	}
	/**
	 * @param closeDate 설정하려는 closeDate.
	 */
	public void setCloseDate(String closeDate) {
		this.closeDate = closeDate;
	}
	/**
	 * @param cod0000 설정하려는 cod0000.
	 */
	public void setCod0000(String cod0000) {
		this.cod0000 = cod0000;
	}
	/**
	 * @param cod0010 설정하려는 cod0010.
	 */
	public void setCod0010(String cod0010) {
		this.cod0010 = cod0010;
	}
	/**
	 * @param cod0020 설정하려는 cod0020.
	 */
	public void setCod0020(String cod0020) {
		this.cod0020 = cod0020;
	}
	/**
	 * @param cod0030 설정하려는 cod0030.
	 */
	public void setCod0030(String cod0030) {
		this.cod0030 = cod0030;
	}
	/**
	 * @param cod0040 설정하려는 cod0040.
	 */
	public void setCod0040(String cod0040) {
		this.cod0040 = cod0040;
	}
	/**
	 * @param cod0050 설정하려는 cod0050.
	 */
	public void setCod0050(String cod0050) {
		this.cod0050 = cod0050;
	}
	/**
	 * @param cod0051 설정하려는 cod0051.
	 */
	public void setCod0051(String cod0051) {
		this.cod0051 = cod0051;
	}
	/**
	 * @param cod0052 설정하려는 cod0052.
	 */
	public void setCod0052(String cod0052) {
		this.cod0052 = cod0052;
	}
	/**
	 * @param cod0053 설정하려는 cod0053.
	 */
	public void setCod0053(String cod0053) {
		this.cod0053 = cod0053;
	}
	/**
	 * @param cod0060 설정하려는 cod0060.
	 */
	public void setCod0060(String cod0060) {
		this.cod0060 = cod0060;
	}
	/**
	 * @param cod0070 설정하려는 cod0070.
	 */
	public void setCod0070(String cod0070) {
		this.cod0070 = cod0070;
	}
	/**
	 * @param cod0080 설정하려는 cod0080.
	 */
	public void setCod0080(String cod0080) {
		this.cod0080 = cod0080;
	}
	/**
	 * @param cod0090 설정하려는 cod0090.
	 */
	public void setCod0090(String cod0090) {
		this.cod0090 = cod0090;
	}
	/**
	 * @param codNull 설정하려는 codNull.
	 */
	public void setCodNull(String codNull) {
		this.codNull = codNull;
	}
	/**
	 * @param codSpace 설정하려는 codSpace.
	 */
	public void setCodSpace(String codSpace) {
		this.codSpace = codSpace;
	}
	/**
	 * @param comCon 설정하려는 comCon.
	 */
	public void setComCon(String comCon) {
		this.comCon = comCon;
	}
	/**
	 * @param comIntro 설정하려는 comIntro.
	 */
	public void setComIntro(String comIntro) {
		this.comIntro = comIntro;
	}
	/**
	 * @param comRep 설정하려는 comRep.
	 */
	public void setComRep(String comRep) {
		this.comRep = comRep;
	}
	/**
	 * @param comRes 설정하려는 comRes.
	 */
	public void setComRes(String comRes) {
		this.comRes = comRes;
	}
	/**
	 * @param comResName 설정하려는 comResName.
	 */
	public void setComResName(String comResName) {
		this.comResName = comResName;
	}
	/**
	 * @param comVote 설정하려는 comVote.
	 */
	public void setComVote(String comVote) {
		this.comVote = comVote;
	}
	/**
	 * @param confClose 설정하려는 confClose.
	 */
	public void setConfClose(String confClose) {
		this.confClose = confClose;
	}
	/**
	 * @param confCloseTime 설정하려는 confCloseTime.
	 */
	public void setConfCloseTime(String confCloseTime) {
		this.confCloseTime = confCloseTime;
	}
	/**
	 * @param confData 설정하려는 confData.
	 */
	public void setConfData(String confData) {
		this.confData = confData;
	}
	/**
	 * @param confDate 설정하려는 confDate.
	 */
	public void setConfDate(String confDate) {
		this.confDate = confDate;
	}
	/**
	 * @param confMedia 설정하려는 confMedia.
	 */
	public void setConfMedia(String confMedia) {
		this.confMedia = confMedia;
	}
	/**
	 * @param confOrder 설정하려는 confOrder.
	 */
	public void setConfOrder(String confOrder) {
		this.confOrder = confOrder;
	}
	/**
	 * @param confPlace 설정하려는 confPlace.
	 */
	public void setConfPlace(String confPlace) {
		this.confPlace = confPlace;
	}
	/**
	 * @param confReqTime 설정하려는 confReqTime.
	 */
	public void setConfReqTime(String confReqTime) {
		this.confReqTime = confReqTime;
	}
	/**
	 * @param confService 설정하려는 confService.
	 */
	public void setConfService(String confService) {
		this.confService = confService;
	}
	/**
	 * @param confStartTime 설정하려는 confStartTime.
	 */
	public void setConfStartTime(String confStartTime) {
		this.confStartTime = confStartTime;
	}
	/**
	 * @param confWeek 설정하려는 confWeek.
	 */
	public void setConfWeek(String confWeek) {
		this.confWeek = confWeek;
	}
	/**
	 * @param contents 설정하려는 contents.
	 */
	public void setContents(String contents) {
		this.contents = contents;
	}
	/**
	 * @param contentsBR 설정하려는 contentsBR.
	 */
	public void setContentsBR(String contentsBR) {
		this.contentsBR = contentsBR;
	}
	/**
	 * @param contentsCR 설정하려는 contentsCR.
	 */
	public void setContentsCR(String contentsCR) {
		this.contentsCR = contentsCR;
	}
	/**
	 * @param creIPv4 설정하려는 creIPv4.
	 */
	public void setCreIPv4(String creIPv4) {
		this.creIPv4 = creIPv4;
	}
	/**
	 * @param creIPv6 설정하려는 creIPv6.
	 */
	public void setCreIPv6(String creIPv6) {
		this.creIPv6 = creIPv6;
	}
	/**
	 * @param creMac 설정하려는 creMac.
	 */
	public void setCreMac(String creMac) {
		this.creMac = creMac;
	}
	/**
	 * @param delIPv4 설정하려는 delIPv4.
	 */
	public void setDelIPv4(String delIPv4) {
		this.delIPv4 = delIPv4;
	}
	/**
	 * @param delIPv6 설정하려는 delIPv6.
	 */
	public void setDelIPv6(String delIPv6) {
		this.delIPv6 = delIPv6;
	}
	/**
	 * @param delMac 설정하려는 delMac.
	 */
	public void setDelMac(String delMac) {
		this.delMac = delMac;
	}
	/**
	 * @param divName 설정하려는 divName.
	 */
	public void setDivName(String divName) {
		this.divName = divName;
	}
	/**
	 * @param downAbs 설정하려는 downAbs.
	 */
	public void setDownAbs(String downAbs) {
		this.downAbs = downAbs;
	}
	/**
	 * @param downExt 설정하려는 downExt.
	 */
	public void setDownExt(String downExt) {
		this.downExt = downExt;
	}
	/**
	 * @param downRel 설정하려는 downRel.
	 */
	public void setDownRel(String downRel) {
		this.downRel = downRel;
	}
	/**
	 * @param downUUID 설정하려는 downUUID.
	 */
	public void setDownUUID(String downUUID) {
		this.downUUID = downUUID;
	}
	/**
	 * @param endClass 설정하려는 endClass.
	 */
	public void setEndClass(String endClass) {
		this.endClass = endClass;
	}
	/**
	 * @param endRound 설정하려는 endRound.
	 */
	public void setEndRound(String endRound) {
		this.endRound = endRound;
	}
	/**
	 * @param eraCloseDate 설정하려는 eraCloseDate.
	 */
	public void setEraCloseDate(String eraCloseDate) {
		this.eraCloseDate = eraCloseDate;
	}
	/**
	 * @param eraCount 설정하려는 eraCount.
	 */
	public void setEraCount(String eraCount) {
		this.eraCount = eraCount;
	}
	/**
	 * @param eraStartDate 설정하려는 eraStartDate.
	 */
	public void setEraStartDate(String eraStartDate) {
		this.eraStartDate = eraStartDate;
	}
	/**
	 * @param eraTitle 설정하려는 eraTitle.
	 */
	public void setEraTitle(String eraTitle) {
		this.eraTitle = eraTitle;
	}
	/**
	 * @param etc 설정하려는 etc.
	 */
	public void setEtc(String etc) {
		this.etc = etc;
	}
	/**
	 * @param fileID 설정하려는 fileID.
	 */
	public void setFileID(String fileID) {
		this.fileID = fileID;
	}
	/**
	 * @param fileLinkD 설정하려는 fileLinkD.
	 */
	public void setFileLinkD(String fileLinkD) {
		this.fileLinkD = fileLinkD;
	}
	/**
	 * @param fileLinkM 설정하려는 fileLinkM.
	 */
	public void setFileLinkM(String fileLinkM) {
		this.fileLinkM = fileLinkM;
	}
	/**
	 * @param fileLinkS 설정하려는 fileLinkS.
	 */
	public void setFileLinkS(String fileLinkS) {
		this.fileLinkS = fileLinkS;
	}
	/**
	 * @param genCon 설정하려는 genCon.
	 */
	public void setGenCon(String genCon) {
		this.genCon = genCon;
	}
	/**
	 * @param genIntro 설정하려는 genIntro.
	 */
	public void setGenIntro(String genIntro) {
		this.genIntro = genIntro;
	}
	/**
	 * @param genRep 설정하려는 genRep.
	 */
	public void setGenRep(String genRep) {
		this.genRep = genRep;
	}
	/**
	 * @param genRes 설정하려는 genRes.
	 */
	public void setGenRes(String genRes) {
		this.genRes = genRes;
	}
	/**
	 * @param genResName 설정하려는 genResName.
	 */
	public void setGenResName(String genResName) {
		this.genResName = genResName;
	}
	/**
	 * @param genVote 설정하려는 genVote.
	 */
	public void setGenVote(String genVote) {
		this.genVote = genVote;
	}
	/**
	 * @param insCode 설정하려는 insCode.
	 */
	public void setInsCode(String insCode) {
		this.insCode = insCode;
	}
	/**
	 * @param insCount 설정하려는 insCount.
	 */
	public void setInsCount(String insCount) {
		this.insCount = insCount;
	}
	/**
	 * @param insName 설정하려는 insName.
	 */
	public void setInsName(String insName) {
		this.insName = insName;
	}
	/**
	 * @param insTitle 설정하려는 insTitle.
	 */
	public void setInsTitle(String insTitle) {
		this.insTitle = insTitle;
	}
	/**
	 * @param insType 설정하려는 insType.
	 */
	public void setInsType(String insType) {
		this.insType = insType;
	}
	/**
	 * @param knd000 설정하려는 knd000.
	 */
	public void setKnd000(String knd000) {
		this.knd000 = knd000;
	}
	/**
	 * @param knd100 설정하려는 knd100.
	 */
	public void setKnd100(String knd100) {
		this.knd100 = knd100;
	}
	/**
	 * @param knd200 설정하려는 knd200.
	 */
	public void setKnd200(String knd200) {
		this.knd200 = knd200;
	}
	/**
	 * @param knd300 설정하려는 knd300.
	 */
	public void setKnd300(String knd300) {
		this.knd300 = knd300;
	}
	/**
	 * @param knd400 설정하려는 knd400.
	 */
	public void setKnd400(String knd400) {
		this.knd400 = knd400;
	}
	/**
	 * @param knd500 설정하려는 knd500.
	 */
	public void setKnd500(String knd500) {
		this.knd500 = knd500;
	}
	/**
	 * @param knd600 설정하려는 knd600.
	 */
	public void setKnd600(String knd600) {
		this.knd600 = knd600;
	}
	/**
	 * @param knd700 설정하려는 knd700.
	 */
	public void setKnd700(String knd700) {
		this.knd700 = knd700;
	}
	/**
	 * @param knd800 설정하려는 knd800.
	 */
	public void setKnd800(String knd800) {
		this.knd800 = knd800;
	}
	/**
	 * @param knd900 설정하려는 knd900.
	 */
	public void setKnd900(String knd900) {
		this.knd900 = knd900;
	}
	/**
	 * @param kndCode 설정하려는 kndCode.
	 */
	public void setKndCode(String kndCode) {
		this.kndCode = kndCode;
	}
	/**
	 * @param kndCount 설정하려는 kndCount.
	 */
	public void setKndCount(String kndCount) {
		this.kndCount = kndCount;
	}
	/**
	 * @param kndName 설정하려는 kndName.
	 */
	public void setKndName(String kndName) {
		this.kndName = kndName;
	}
	/**
	 * @param kndTitle 설정하려는 kndTitle.
	 */
	public void setKndTitle(String kndTitle) {
		this.kndTitle = kndTitle;
	}
	/**
	 * @param kndType 설정하려는 kndType.
	 */
	public void setKndType(String kndType) {
		this.kndType = kndType;
	}
	/**
	 * @param memAddr 설정하려는 memAddr.
	 */
	public void setMemAddr(String memAddr) {
		this.memAddr = memAddr;
	}
	/**
	 * @param memBirth 설정하려는 memBirth.
	 */
	public void setMemBirth(String memBirth) {
		this.memBirth = memBirth;
	}
	/**
	 * @param memChi 설정하려는 memChi.
	 */
	public void setMemChi(String memChi) {
		this.memChi = memChi;
	}
	/**
	 * @param memEmail 설정하려는 memEmail.
	 */
	public void setMemEmail(String memEmail) {
		this.memEmail = memEmail;
	}
	/**
	 * @param memEng 설정하려는 memEng.
	 */
	public void setMemEng(String memEng) {
		this.memEng = memEng;
	}
	/**
	 * @param memHome 설정하려는 memHome.
	 */
	public void setMemHome(String memHome) {
		this.memHome = memHome;
	}
	/**
	 * @param memIntro 설정하려는 memIntro.
	 */
	public void setMemIntro(String memIntro) {
		this.memIntro = memIntro;
	}
	/**
	 * @param memIntroBR 설정하려는 memIntroBR.
	 */
	public void setMemIntroBR(String memIntroBR) {
		this.memIntroBR = memIntroBR;
	}
	/**
	 * @param memIntroCR 설정하려는 memIntroCR.
	 */
	public void setMemIntroCR(String memIntroCR) {
		this.memIntroCR = memIntroCR;
	}
	/**
	 * @param memJumin 설정하려는 memJumin.
	 */
	public void setMemJumin(String memJumin) {
		this.memJumin = memJumin;
	}
	/**
	 * @param memMobile 설정하려는 memMobile.
	 */
	public void setMemMobile(String memMobile) {
		this.memMobile = memMobile;
	}
	/**
	 * @param memOffice 설정하려는 memOffice.
	 */
	public void setMemOffice(String memOffice) {
		this.memOffice = memOffice;
	}
	/**
	 * @param memOrder 설정하려는 memOrder.
	 */
	public void setMemOrder(String memOrder) {
		this.memOrder = memOrder;
	}
	/**
	 * @param memParty 설정하려는 memParty.
	 */
	public void setMemParty(String memParty) {
		this.memParty = memParty;
	}
	/**
	 * @param memPic 설정하려는 memPic.
	 */
	public void setMemPic(String memPic) {
		this.memPic = memPic;
	}
	/**
	 * @param memProfile 설정하려는 memProfile.
	 */
	public void setMemProfile(String memProfile) {
		this.memProfile = memProfile;
	}
	/**
	 * @param memProfileBR 설정하려는 memProfileBR.
	 */
	public void setMemProfileBR(String memProfileBR) {
		this.memProfileBR = memProfileBR;
	}
	/**
	 * @param memProfileCR 설정하려는 memProfileCR.
	 */
	public void setMemProfileCR(String memProfileCR) {
		this.memProfileCR = memProfileCR;
	}
	/**
	 * @param memPromise 설정하려는 memPromise.
	 */
	public void setMemPromise(String memPromise) {
		this.memPromise = memPromise;
	}
	/**
	 * @param memPromiseBR 설정하려는 memPromiseBR.
	 */
	public void setMemPromiseBR(String memPromiseBR) {
		this.memPromiseBR = memPromiseBR;
	}
	/**
	 * @param memPromiseCR 설정하려는 memPromiseCR.
	 */
	public void setMemPromiseCR(String memPromiseCR) {
		this.memPromiseCR = memPromiseCR;
	}
	/**
	 * @param memSeq 설정하려는 memSeq.
	 */
	public void setMemSeq(String memSeq) {
		this.memSeq = memSeq;
	}
	/**
	 * @param memUri 설정하려는 memUri.
	 */
	public void setMemUri(String memUri) {
		this.memUri = memUri;
	}
	/**
	 * @param memVod 설정하려는 memVod.
	 */
	public void setMemVod(String memVod) {
		this.memVod = memVod;
	}
	/**
	 * @param modIPv4 설정하려는 modIPv4.
	 */
	public void setModIPv4(String modIPv4) {
		this.modIPv4 = modIPv4;
	}
	/**
	 * @param modIPv6 설정하려는 modIPv6.
	 */
	public void setModIPv6(String modIPv6) {
		this.modIPv6 = modIPv6;
	}
	/**
	 * @param modMac 설정하려는 modMac.
	 */
	public void setModMac(String modMac) {
		this.modMac = modMac;
	}
	/**
	 * @param pntCode 설정하려는 pntCode.
	 */
	public void setPntCode(String pntCode) {
		this.pntCode = pntCode;
	}
	/**
	 * @param pntCount 설정하려는 pntCount.
	 */
	public void setPntCount(String pntCount) {
		this.pntCount = pntCount;
	}
	/**
	 * @param pntName 설정하려는 pntName.
	 */
	public void setPntName(String pntName) {
		this.pntName = pntName;
	}
	/**
	 * @param pntTitle 설정하려는 pntTitle.
	 */
	public void setPntTitle(String pntTitle) {
		this.pntTitle = pntTitle;
	}
	/**
	 * @param pntType 설정하려는 pntType.
	 */
	public void setPntType(String pntType) {
		this.pntType = pntType;
	}
	/**
	 * @param publDate 설정하려는 publDate.
	 */
	public void setPublDate(String publDate) {
		this.publDate = publDate;
	}
	/**
	 * @param publNum 설정하려는 publNum.
	 */
	public void setPublNum(String publNum) {
		this.publNum = publNum;
	}
	/**
	 * @param rndCode 설정하려는 rndCode.
	 */
	public void setRndCode(String rndCode) {
		this.rndCode = rndCode;
	}
	/**
	 * @param rndCount 설정하려는 rndCount.
	 */
	public void setRndCount(String rndCount) {
		this.rndCount = rndCount;
	}
	/**
	 * @param rndDiffDate 설정하려는 rndDiffDate.
	 */
	public void setRndDiffDate(String rndDiffDate) {
		this.rndDiffDate = rndDiffDate;
	}
	/**
	 * @param rndName 설정하려는 rndName.
	 */
	public void setRndName(String rndName) {
		this.rndName = rndName;
	}
	/**
	 * @param rndTitle 설정하려는 rndTitle.
	 */
	public void setRndTitle(String rndTitle) {
		this.rndTitle = rndTitle;
	}
	/**
	 * @param rndType 설정하려는 rndType.
	 */
	public void setRndType(String rndType) {
		this.rndType = rndType;
	}
	/**
	 * @param roundCloseDate 설정하려는 roundCloseDate.
	 */
	public void setRoundCloseDate(String roundCloseDate) {
		this.roundCloseDate = roundCloseDate;
	}
	/**
	 * @param roundStartDate 설정하려는 roundStartDate.
	 */
	public void setRoundStartDate(String roundStartDate) {
		this.roundStartDate = roundStartDate;
	}
	/**
	 * @param startDate 설정하려는 startDate.
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	/**
	 * @param sugDate 설정하려는 sugDate.
	 */
	public void setSugDate(String sugDate) {
		this.sugDate = sugDate;
	}
	/**
	 * @param sugMan 설정하려는 sugMan.
	 */
	public void setSugMan(String sugMan) {
		this.sugMan = sugMan;
	}
	/**
	 * @param sugPosi 설정하려는 sugPosi.
	 */
	public void setSugPosi(String sugPosi) {
		this.sugPosi = sugPosi;
	}
	/**
	 * @param sugTitle 설정하려는 sugTitle.
	 */
	public void setSugTitle(String sugTitle) {
		this.sugTitle = sugTitle;
	}
	/**
	 * @param sugWork 설정하려는 sugWork.
	 */
	public void setSugWork(String sugWork) {
		this.sugWork = sugWork;
	}
	/**
	 * @param tempFlag 설정하려는 tempFlag.
	 */
	public void setTempFlag(String tempFlag) {
		this.tempFlag = tempFlag;
	}
	/**
	 * @param timeMemb 설정하려는 timeMemb.
	 */
	public void setTimeMemb(String timeMemb) {
		this.timeMemb = timeMemb;
	}
	/**
	 * @param timePos 설정하려는 timePos.
	 */
	public void setTimePos(String timePos) {
		this.timePos = timePos;
	}
	/**
	 * @param timeStamp 설정하려는 timeStamp.
	 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	/**
	 * @param tranDate 설정하려는 tranDate.
	 */
	public void setTranDate(String tranDate) {
		this.tranDate = tranDate;
	}
	/**
	 * @param userId 설정하려는 userId.
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @param yosCode 설정하려는 yosCode.
	 */
	public void setYosCode(String yosCode) {
		this.yosCode = yosCode;
	}
	/**
	 * @param yosCount 설정하려는 yosCount.
	 */
	public void setYosCount(String yosCount) {
		this.yosCount = yosCount;
	}
	/**
	 * @param yosName 설정하려는 yosName.
	 */
	public void setYosName(String yosName) {
		this.yosName = yosName;
	}
	/**
	 * @param yosTitle 설정하려는 yosTitle.
	 */
	public void setYosTitle(String yosTitle) {
		this.yosTitle = yosTitle;
	}
	/**
	 * @param yosType 설정하려는 yosType.
	 */
	public void setYosType(String yosType) {
		this.yosType = yosType;
	}
	/**
	 * @param zipCode 설정하려는 zipCode.
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}


	public String getSugSeq() { return BaseUtility.null2Blank( sugSeq ); }
	public void setSugSeq(String sugSeq) { this.sugSeq = sugSeq; }

}	// end of class