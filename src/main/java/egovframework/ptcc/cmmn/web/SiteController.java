/*******************************************************************************
  Program ID  : SiteController
  Description : 클라이언트의 요청(공통)에 관한 controller 클래스를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.cmmn.web;

import java.io.*;
import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.*;

import net.sf.json.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.agenda.service.*;
import egovframework.ptcc.mngr.member.service.*;

@Controller
public class SiteController { 

	@Autowired
	private WebApplicationContext			webContext;

	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="agendaMngrService")
	protected AgendaMngrService			agendaMngrService;
	@Resource(name="membMngrService")
	protected MembMngrService				membMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));


	@RequestMapping(value="/cmmn/forward.do")
	public String scrCommonOfMessage () { 
		return "/cmmn/exe_script";
	}	// end of scrCommonOfMessage

	@RequestMapping(value="/cmmn/print.do")
	public String scrCommonOfPrint () { 
		return "/cmmn/exe_print";
	}	// end of scrCommonOfPrint

	@RequestMapping(value="/cmmn/program.do")
	public String scrCommonOfProgram () { 
		return "/cmmn/pop_programView";
	}	// end of scrCommonOfProgram

	@RequestMapping(value="/certi/checkplus_success.do")
	public String scrInitOfCBSuccess () { 
		return "/certi/checkplus_success";
	}	// end of scrInitOfCBSuccess

	@RequestMapping(value="/certi/checkplus_fail.do")
	public String scrInitOfCBFailure () { 
		return "/certi/checkplus_fail";
	}	// end of scrInitOfCBFailure

	@RequestMapping(value={"/ptnhexa","/ptnhexa/"})
	public String scrInitOfAdmHome () { 
//		return "/mngr/index";
		return "redirect:/ptnhexa/index.do";
	}	// end of scrInitOfAdmHome

	@RequestMapping(value={"","/"})
	public String scrInitOfGenHome () { 
//		return "/coun/index";
		return "redirect:/index.do";
	}	// end of scrInitOfGenHome

	@RequestMapping(value="/coun/{uriPath1}.do")
	public String scrInitOfCounView1 ( 
			@PathVariable("uriPath1") String uriPath1, 
			ModelMap model 
		) { 
		return "/coun/"+ uriPath1;
	}	// end of scrInitOfCounView1

	@RequestMapping(value="/coun/{uriPath1}/{uriPath2}.do")
	public String scrInitOfCounView2 ( 
			@PathVariable("uriPath1") String uriPath1, 
			@PathVariable("uriPath2") String uriPath2, 
			ModelMap model 
		) { 
		return "/coun/"+ uriPath1 +"/"+ uriPath2;
	}	// end of scrInitOfCounView2

	@RequestMapping(value="/coun/{uriPath1}/{uriPath2}/{uriPath3}.do")
	public String scrInitOfCounView3 ( 
			@PathVariable("uriPath1") String uriPath1, 
			@PathVariable("uriPath2") String uriPath2, 
			@PathVariable("uriPath3") String uriPath3, 
			ModelMap model 
		) { 
		return "/coun/"+ uriPath1 +"/"+ uriPath2 +"/"+ uriPath3;
	}	// end of scrInitOfCounView3
//
//	@RequestMapping(value="/stand/{uriPath1}.do")
//	public String scrInitOfStandView1 ( 
//			@PathVariable("uriPath1") String uriPath1, 
//			ModelMap model 
//		) { 
//
//		model.addAttribute("sndType"       , "stand");
//		model.addAttribute("sndName"       , "상임위원회");
//
//		return "/stand/"+ uriPath1;
//	}	// end of scrInitOfStandView1
//
//	@RequestMapping(value="/stand/{uriPath1}/{uriPath2}.do")
//	public String scrInitOfStandView2 ( 
//			@PathVariable("uriPath1") String uriPath1, 
//			@PathVariable("uriPath2") String uriPath2, 
//			ModelMap model 
//		) { 
//
//		model.addAttribute("sndType"       , "stand");
//		model.addAttribute("sndName"       , "상임위원회");
//
//		return "/stand/"+ uriPath1 +"/"+ uriPath2;
//	}	// end of scrInitOfStandView2
//
//	@RequestMapping(value="/stand/{uriPath1}/{uriPath2}/{uriPath3}.do")
//	public String scrInitOfStandView3 ( 
//			@PathVariable("uriPath1") String uriPath1, 
//			@PathVariable("uriPath2") String uriPath2, 
//			@PathVariable("uriPath3") String uriPath3, 
//			ModelMap model 
//		) { 
//
//		model.addAttribute("sndType"       , "stand");
//		model.addAttribute("sndName"       , "상임위원회");
//
//		return "/stand/"+ uriPath1 +"/"+ uriPath2+"/"+ uriPath3;
//	}	// end of scrInitOfStandView3
//
//	@RequestMapping(value="/oper/{uriPath1}.do")
//	public String scrInitOfOperView1 ( 
//			@PathVariable("uriPath1") String uriPath1, 
//			ModelMap model 
//		) { 
//
//		model.addAttribute("sndType"       , "oper");
//		model.addAttribute("sndName"       , "의회운영위원회");
//
//		return "/stand/"+ uriPath1;
//	}	// end of scrInitOfOperView1

	@RequestMapping(value="/oper/{uriPath1}/{uriPath2}.do")
	public String scrInitOfOperView2 ( 
			@PathVariable("uriPath1") String uriPath1, 
			@PathVariable("uriPath2") String uriPath2, 
			ModelMap model 
		) { 
		model.addAttribute("sndType"       , "oper");
		model.addAttribute("sndName"       , "의회운영위원회");

		return "/stand/"+ uriPath1 +"/"+ uriPath2;
	}	// end of scrInitOfOperView2

	@RequestMapping(value="/oper/{uriPath1}/{uriPath2}/{uriPath3}.do")
	public String scrInitOfOperView3 ( 
			@PathVariable("uriPath1") String uriPath1, 
			@PathVariable("uriPath2") String uriPath2, 
			@PathVariable("uriPath3") String uriPath3, 
			ModelMap model 
		) { 
		model.addAttribute("sndType"       , "oper");
		model.addAttribute("sndName"       , "의회운영위원회");

		return "/stand/"+ uriPath1 +"/"+ uriPath2+"/"+ uriPath3;
	}	// end of scrInitOfOperView3
//
//	@RequestMapping(value="/gvadm/{uriPath1}.do")
//	public String scrInitOfGvadmView1 ( 
//			@PathVariable("uriPath1") String uriPath1, 
//			ModelMap model 
//		) { 
//
//		model.addAttribute("sndType"       , "gvadm");
//		model.addAttribute("sndName"       , "기획행정위원회");
//
//		return "/stand/"+ uriPath1;
//	}	// end of scrInitOfGvadmView1

	@RequestMapping(value="/gvadm/{uriPath1}/{uriPath2}.do")
	public String scrInitOfGvadmView2 ( 
			@PathVariable("uriPath1") String uriPath1, 
			@PathVariable("uriPath2") String uriPath2, 
			ModelMap model 
		) { 

		model.addAttribute("sndType"       , "gvadm");
		model.addAttribute("sndName"       , "기획행정위원회");

		return "/stand/"+ uriPath1 +"/"+ uriPath2;
	}	// end of scrInitOfGvadmView2

	@RequestMapping(value="/gvadm/{uriPath1}/{uriPath2}/{uriPath3}.do")
	public String scrInitOfGvadmView3 ( 
			@PathVariable("uriPath1") String uriPath1, 
			@PathVariable("uriPath2") String uriPath2, 
			@PathVariable("uriPath3") String uriPath3, 
			ModelMap model 
		) { 

		model.addAttribute("sndType"       , "gvadm");
		model.addAttribute("sndName"       , "기획행정위원회");

		return "/stand/"+ uriPath1 +"/"+ uriPath2+"/"+ uriPath3;
	}	// end of scrInitOfGvadmView3

	@RequestMapping(value="/welfare/{uriPath1}/{uriPath2}.do")
	public String scrInitOfWelfareView2 (
			@PathVariable("uriPath1") String uriPath1,
			@PathVariable("uriPath2") String uriPath2,
			ModelMap model
	) {

		model.addAttribute("sndType"       , "welfare");
		model.addAttribute("sndName"       , "복지환경위원회");

		return "/stand/"+ uriPath1 +"/"+ uriPath2;
	}	// end of scrInitOfWelfareView2

	@RequestMapping(value="/welfare/{uriPath1}/{uriPath2}/{uriPath3}.do")
	public String scrInitOfWelfareView3 (
			@PathVariable("uriPath1") String uriPath1,
			@PathVariable("uriPath2") String uriPath2,
			@PathVariable("uriPath3") String uriPath3,
			ModelMap model
	) {

		model.addAttribute("sndType"       , "welfare");
		model.addAttribute("sndName"       , "복지환경위원회");

		return "/stand/"+ uriPath1 +"/"+ uriPath2+"/"+ uriPath3;
	}	// end of scrInitOfWelfareView3

//
//	@RequestMapping(value="/build/{uriPath1}.do")
//	public String scrInitOfBuildView1 ( 
//			@PathVariable("uriPath1") String uriPath1, 
//			ModelMap model 
//		) { 
//
//		model.addAttribute("sndType"       , "build");
//		model.addAttribute("sndName"       , "산업건설위원회");
//
//		return "/stand/"+ uriPath1;
//	}	// end of scrInitOfBuildView1

	@RequestMapping(value="/build/{uriPath1}/{uriPath2}.do")
	public String scrInitOfBuildView2 ( 
			@PathVariable("uriPath1") String uriPath1, 
			@PathVariable("uriPath2") String uriPath2, 
			ModelMap model 
		) { 

		model.addAttribute("sndType"       , "build");
		model.addAttribute("sndName"       , "산업건설위원회");

		return "/stand/"+ uriPath1 +"/"+ uriPath2;
	}	// end of scrInitOfBuildView2

	@RequestMapping(value="/build/{uriPath1}/{uriPath2}/{uriPath3}.do")
	public String scrInitOfBuildView3 ( 
			@PathVariable("uriPath1") String uriPath1, 
			@PathVariable("uriPath2") String uriPath2, 
			@PathVariable("uriPath3") String uriPath3, 
			ModelMap model 
		) { 

		model.addAttribute("sndType"       , "build");
		model.addAttribute("sndName"       , "산업건설위원회");

		return "/stand/"+ uriPath1 +"/"+ uriPath2+"/"+ uriPath3;
	}	// end of scrInitOfBuildView3

	@RequestMapping(value="/youth/{uriPath1}.do")
	public String scrInitOfYouthView1 ( 
			@PathVariable("uriPath1") String uriPath1  
		) { 
		return "/youth/"+ uriPath1;
	}	// end of scrInitOfYouthView1

	@RequestMapping(value="/youth/{uriPath1}/{uriPath2}.do")
	public String scrInitOfYouthView2 ( 
			@PathVariable("uriPath1") String uriPath1, 
			@PathVariable("uriPath2") String uriPath2  
		) { 
		return "/youth/"+ uriPath1 +"/"+ uriPath2;
	}	// end of scrInitOfYouthView2

	@RequestMapping(value="/youth/{uriPath1}/{uriPath2}/{uriPath3}.do")
	public String scrInitOfYouthView3 ( 
			@PathVariable("uriPath1") String uriPath1, 
			@PathVariable("uriPath2") String uriPath2, 
			@PathVariable("uriPath3") String uriPath3  
		) { 
		return "/youth/"+ uriPath1 +"/"+ uriPath2 +"/"+ uriPath3;
	}	// end of scrInitOfYouthView3

	/**
	 * <pre>
	 * 로그아웃 처리을(를) 해주는 메소드
	 * </pre>
	 * 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/coun/iuout_proc.do")
	public String doLogoutOfCerti ( 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		String					returnMSG    = "최초 본인인증 후 10분이 경과되어 본인인증이 만료되었습니다.";

		request.getSession().invalidate();

//		model.addAttribute("message"       , returnMSG);
		model.addAttribute("returnUrl"     , webBase+"/");

		return "/cmmn/exe_script";
	}	// end of doLogoutOfCerti

	/**
	 * <pre>
	 * 로그아웃 처리을(를) 해주는 메소드
	 * </pre>
	 * 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/coun/huout_proc.do")
	public String doLogoutOfBases ( 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		String					returnMSG    = "로그아웃 되었습니다.";

		request.getSession().invalidate();

		model.addAttribute("message"       , returnMSG);
		model.addAttribute("returnUrl"     , webBase+"/");

		return "/cmmn/exe_script";
	}	// end of doLogoutOfBases

	/**
	 * <pre>
	 * 방문자 내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/cmmn/visitor.do")
	public String getStstOfVisit ( 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();

		BaseVisitsVO			dataInfo     = new BaseVisitsVO();
		StringBuffer			dataRult     = new StringBuffer();

		try { 
			dataInfo = (BaseVisitsVO)baseCmmnService.getStatOfVisit( null );
			dataRult.setLength(0);

			if( dataInfo == null ) { 
				//dataRult.append("오늘:0명 &nbsp; 전체:0명");
				dataRult.append("<span>오늘: 0명</span> 전체: 0");
			} else { 
				//dataRult.append("오늘:");
				dataRult.append("<span>오늘:");
				dataRult.append(BaseUtility.toNumberFormat( dataInfo.getDaysCnt() ));
				//dataRult.append("명 &nbsp; 전체:");
				dataRult.append("명</span> 전체: ");
				dataRult.append(BaseUtility.toNumberFormat( dataInfo.getTotsCnt() ));
				dataRult.append("명");
			}

//log.debug(cateRult.toString());
			response.setContentType("text/html;charset=utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(dataRult.toString());
			response.getWriter().flush();
			response.getWriter().close();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return null;
	}	// end of getStstOfVisit

	/**
	 * <pre>
	 * 대수별 회의기간을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/cmmn/seleDate.do")
	public ModelAndView getSeleOfDateInfo ( 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		ModelAndView			mav          = new ModelAndView("jsonView");
		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();

		BaseCommonVO			searchVO     = new BaseCommonVO();
		BaseCommonVO			cateInfo     = new BaseCommonVO();

		searchVO.setReEraCode(BaseUtility.null2Char( request.getParameter("era"), "%" ));

log.debug("eraCode: "+ searchVO.getReEraCode());
		try { 
			// 대수별 회의기간 취득
			cateInfo = (BaseCommonVO)baseCmmnService.getInfoOfConfDate( searchVO.getReEraCode() );

			if( !(cateInfo == null) ) { 
				mav.addObject("min"           , BaseUtility.toDateFormat( cateInfo.getMinDate(), dtSep ));
				mav.addObject("max"           , BaseUtility.toDateFormat( cateInfo.getMaxDate(), dtSep ));
			}
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return mav;
	}	// end of getSeleOfDateInfo

	/**
	 * <pre>
	 * 대수별 선거구을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/cmmn/seleParty.do")
	public String getSeleOfPartyList ( 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();

		BaseCommonVO			searchVO     = new BaseCommonVO();
		int						listCnt      = 0;
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		StringBuffer			cateRult     = new StringBuffer();

		searchVO.setReEraCode(BaseUtility.null2Blank( request.getParameter("era") ));
		searchVO.setReRegCode(BaseUtility.null2Blank( request.getParameter("reg") ));

log.debug("eraCode: "+ searchVO.getReEraCode() +" |regCode: "+ searchVO.getReRegCode());
		try { 
			// 분류구분 취득 - 선거구 구분
			cateList = (List<BaseCommonVO>)baseCmmnService.getListOfParty( searchVO.getReEraCode() );
			cateRult.setLength(0);
			cateRult.append("<option value=''>전체</option>");

			if( !(cateList == null || cateList.size() <= 0) ) { 
				for( int c = 0; c < cateList.size(); c++ ) { 
					cateRult.append("<option value='"+ cateList.get(c).getRegCode() +"'");
					cateRult.append((searchVO.getReRegCode().equals(cateList.get(c).getRegCode()) ? " selected='selected'": "") +">");
					cateRult.append(cateList.get(c).getRegName() +"</option>");
				}
			}

//log.debug(cateRult.toString());
			response.setContentType("text/html;charset=utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(cateRult.toString());
			response.getWriter().flush();
			response.getWriter().close();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return null;
	}	// end of getSeleOfPartyList

	/**
	 * <pre>
	 * 대수별 회의회기을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/cmmn/seleRound.do")
	public String getSeleOfRoundList ( 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();

		BaseCommonVO			searchVO     = new BaseCommonVO();
		int						listCnt      = 0;
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		StringBuffer			cateRult     = new StringBuffer();

		searchVO.setReEraCode(BaseUtility.null2Blank( request.getParameter("era") ));
		searchVO.setReRndCode(BaseUtility.null2Blank( request.getParameter("rnd") ));

log.debug("eraCode: "+ searchVO.getReEraCode() +" |rndCode: "+ searchVO.getReRndCode());
		try { 
			// 분류구분 취득 - 회의회수 구분
			cateList = (List<BaseCommonVO>)baseCmmnService.getListOfRoundALL( searchVO.getReEraCode() );
			cateRult.setLength(0);
			cateRult.append("<option value=''>전체</option>");

			if( !(cateList == null || cateList.size() <= 0) ) { 
				for( int c = 0; c < cateList.size(); c++ ) { 
					cateRult.append("<option value='"+ cateList.get(c).getRoundCode() +"'");
					cateRult.append((searchVO.getReRndCode().equals(cateList.get(c).getRoundCode()) ? " selected='selected'": "") +">");
					cateRult.append(cateList.get(c).getRoundName() +"</option>");
				}
			}

//log.debug(cateRult.toString());
			response.setContentType("text/html;charset=utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(cateRult.toString());
			response.getWriter().flush();
			response.getWriter().close();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return null;
	}	// end of getSeleOfRoundList

	/**
	 * <pre>
	 * 대수별 의원을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/cmmn/seleMemb.do")
	public String getSeleOfMembList ( 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();

		BaseCommonVO			searchVO     = new BaseCommonVO();
		int						listCnt      = 0;
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		StringBuffer			cateRult     = new StringBuffer();

		searchVO.setReEraCode(BaseUtility.null2Blank( request.getParameter("era") ));
		searchVO.setReMemID(BaseUtility.null2Blank( request.getParameter("mid") ));

log.debug("eraCode: "+ searchVO.getReEraCode() +" |memID: "+ searchVO.getReMemID());
		try { 
			// 분류구분 취득 - 회의회수 구분
			cateList = (List<BaseCommonVO>)baseCmmnService.getListOfMembALL( searchVO.getReEraCode() );
			cateRult.setLength(0);
			cateRult.append("<option value=''>전체</option>");

			if( !(cateList == null || cateList.size() <= 0) ) { 
				for( int c = 0; c < cateList.size(); c++ ) { 
					cateRult.append("<option value='"+ cateList.get(c).getMemID() +"'");
					cateRult.append((searchVO.getReMemID().equals(cateList.get(c).getMemID()) ? " selected='selected'": "") +">");
					cateRult.append(cateList.get(c).getMemName() +"</option>");
				}
			}

//log.debug(cateRult.toString());
			response.setContentType("text/html;charset=utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(cateRult.toString());
			response.getWriter().flush();
			response.getWriter().close();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return null;
	}	// end of getSeleOfMembList

	/**
	 * <pre>
	 * 의원 내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/cmmn/loadMemb.do")
	public String getLoadOfMember ( 
			@RequestParam("viewNo") String viewNo, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();

		BaseCommonVO			searchVO     = new BaseCommonVO();
		int						listCnt      = 0;
		BaseAgendaVO			cateView     = new BaseAgendaVO();
		StringBuffer			cateRult     = new StringBuffer();

		searchVO.setMemID(BaseUtility.null2Blank( viewNo ));
		searchVO.setActMode(BaseUtility.null2Char(searchVO.getActMode(), "N"));

log.debug("eraCode: "+ searchVO.getReEraCode() +" |memID: "+ searchVO.getMemID());
		try { 
			if( BaseUtility.isValidNull( viewNo ) ) { 
				cateRult.setLength(0);
				cateRult.append("\n<h2>프로필</h2><p>- 발언자 정보가 없습니다.</p>");
			} else { 
				cateView = (BaseAgendaVO)membMngrService.getDetailOfMemb( searchVO );

				if( cateView != null ) { 
					// Data Adjustment(Members)
					cateView.setActMode("Update");
					cateView.setViewNo(cateView.getMemID());

					if( "A".equals(searchVO.getActMode()) ) 
						cateView.setComName("위원장");
					else if( "B".equals(searchVO.getActMode()) ) 
						cateView.setComName("부위원장");
					else if( "C".equals(searchVO.getActMode()) ) 
						cateView.setComName("위원");
					else 
						cateView.setComName(BaseUtility.null2Char( cateView.getComName(), "의원" ));

					cateView.setMemIntroBR(BaseUtility.tag2Sym( cateView.getMemIntro() ));
					cateView.setMemIntroCR(BaseUtility.sym2Tag( cateView.getMemIntro() ));
					cateView.setMemProfileBR(BaseUtility.tag2Sym( cateView.getMemProfile() ));
					cateView.setMemProfileCR(BaseUtility.sym2Tag( cateView.getMemProfile() ));
					cateView.setMemPromiseBR(BaseUtility.tag2Sym( cateView.getMemPromise() ));
					cateView.setMemPromiseCR(BaseUtility.sym2Tag( cateView.getMemPromise() ));
				}
				cateRult.setLength(0);
				cateRult.append("\n<dl><dt>");
				cateRult.append("\n<p><img src='"+ webBase +"/images/former/"+ cateView.getMemPic() +"' alt='"+ cateView.getMemName() +" "+ cateView.getComName() +"'/></p>");
				cateRult.append("\n<span><a href='javascript:void(0);' onclick='fnActActive(\""+ cateView.getMemID() +"\")'>자세히</a></span>");
				cateRult.append("\n</dt><dd>");
				cateRult.append("\n<p>"+ cateView.getMemName() +"</p>");
				cateRult.append("\n<div>직&nbsp; &nbsp;위 : "+ cateView.getComName() +"</div>");
				cateRult.append("\n<div>선거구 : "+ cateView.getRegName() +"</div>");
				cateRult.append("\n</dd></dl>");
			}

//log.debug(cateRult.toString());
			response.setContentType("text/html;charset=utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(cateRult.toString());
			response.getWriter().flush();
			response.getWriter().close();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return null;
	}	// end of getLoadOfMember

	/**
	 * <pre>
	 * 회의록 회기별 검색 - 회수별 회의구분(상위)을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  @RequestParam					// 
	 * @param  @RequestParam					// 
	 * @param  @RequestParam					// 
	 * @param  @RequestParam					// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/sess/loadSession.do")
	public String  getLoadOfSessionSess ( 
			@RequestParam("act") String act, 
			@RequestParam("rid") String rid, 
			@RequestParam("era") String era, 
			@RequestParam("rnd") String rnd, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();

		BaseCommonVO			searchVO     = new BaseCommonVO();
		int						listCnt      = 0;
		List<BaseAgendaVO>		cateList     = new ArrayList<BaseAgendaVO>();
		JSONObject				jsonObjs     = new JSONObject();
		JSONArray				cateRult     = new JSONArray();

		searchVO.setReEraCode(BaseUtility.null2Blank( era ));
		searchVO.setReRndCode(BaseUtility.null2Blank( rnd ));

log.debug("actType: "+ act +" |rootID: "+ rid +" |eraCode: "+ era +" |rndCode: "+ rnd);
		try { 

			if( "PTCC".equals(rid) && "class".equals(act) ) { 
				cateList = (List<BaseAgendaVO>)agendaMngrService.getLoadOfConfHKind( searchVO );

				if( !(cateList == null || cateList.size() <= 0) ) { 
					cateRult.clear();
					for( int c = 0; c < cateList.size(); c++ ) { 
						jsonObjs.clear();
						jsonObjs.put("root"          , "PTCC");
						jsonObjs.put("folder"        , "0".equals(cateList.get(c).getKndCount()) ? "false" : "true");
						jsonObjs.put("lazy"          , "true");
						jsonObjs.put("mode"          , "0".equals(cateList.get(c).getKndType()) ? "order" : "kind");
						jsonObjs.put("era"           , cateList.get(c).getEraCode());
						jsonObjs.put("rnd"           , cateList.get(c).getRndCode());
						jsonObjs.put("knd"           , cateList.get(c).getKndCode());
						jsonObjs.put("title"         , cateList.get(c).getKndTitle());
						cateRult.add(jsonObjs);
					}
				}
			}

//log.debug(cateRult.toString());
			response.setContentType("application/x-json;charset=utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(cateRult);
			response.getWriter().flush();
			response.getWriter().close();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return null;
	}	// end of getLoadOfSessionSess

	/**
	 * <pre>
	 * 회의록 회기별 검색 - 회수별 회의구분(상위)을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  @RequestParam					// 
	 * @param  @RequestParam					// 
	 * @param  @RequestParam					// 
	 * @param  @RequestParam					// 
	 * @param  @RequestParam					// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/sess/loadKind.do")
	public String getLoadOfKindSess ( 
			@RequestParam("act") String act, 
			@RequestParam("rid") String rid, 
			@RequestParam("era") String era, 
			@RequestParam("rnd") String rnd, 
			@RequestParam("knd") String knd, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();

		BaseCommonVO			searchVO     = new BaseCommonVO();
		int						listCnt      = 0;
		List<BaseAgendaVO>		cateList     = new ArrayList<BaseAgendaVO>();
		JSONObject				jsonObjs     = new JSONObject();
		JSONArray				cateRult     = new JSONArray();

		searchVO.setReEraCode(BaseUtility.null2Blank( era ));
		searchVO.setReRndCode(BaseUtility.null2Blank( rnd ));
		searchVO.setReKndCode(BaseUtility.null2Blank( knd ));

log.debug("actType: "+ act +" |rootID: "+ rid +" |eraCode: "+ era +" |rndCode: "+ rnd +" |kndCode: "+ knd);
		try { 
			if( "PTCC".equals(rid) && "kind".equals(act) ) { 
				cateList = (List<BaseAgendaVO>)agendaMngrService.getLoadOfConfLKind( searchVO );

				if( !(cateList == null || cateList.size() <= 0) ) { 
					cateRult.clear();
					for( int c = 0; c < cateList.size(); c++ ) { 
						jsonObjs.clear();
						jsonObjs.put("root"          , "PTCC");
						jsonObjs.put("folder"        , "0".equals(cateList.get(c).getKndCount()) ? "false" : "true");
						jsonObjs.put("lazy"          , "true");
						jsonObjs.put("mode"          , "order");
						jsonObjs.put("era"           , cateList.get(c).getEraCode());
						jsonObjs.put("rnd"           , cateList.get(c).getRndCode());
						jsonObjs.put("knd"           , cateList.get(c).getKndCode());
						jsonObjs.put("title"         , cateList.get(c).getKndTitle());
						cateRult.add(jsonObjs);
					}
				}
			}

//log.debug(cateRult.toString());
			response.setContentType("application/x-json;charset=utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(cateRult);
			response.getWriter().flush();
			response.getWriter().close();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return null;
	}	// end of getLoadOfKindSess

	/**
	 * <pre>
	 * 회의록 회의별 검색 - 회의별 회의대수을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  @RequestParam					// 
	 * @param  @RequestParam					// 
	 * @param  @RequestParam					// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return void
	 * @throws Exception
	 */
	@RequestMapping(value="/meet/loadPeriod.do")
	public String getLoadOfPeriodMeet ( 
			@RequestParam("act") String act, 
			@RequestParam("rid") String rid, 
			@RequestParam("knd") String knd, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();

		BaseCommonVO			searchVO     = new BaseCommonVO();
		int						listCnt      = 0;
		List<BaseAgendaVO>		cateList     = new ArrayList<BaseAgendaVO>();
		JSONObject				jsonObjs     = new JSONObject();
		JSONArray				cateRult     = new JSONArray();

		searchVO.setReKndCode(BaseUtility.null2Blank( knd ));

log.debug("actType: "+ act +" |rootID: "+ rid +" |kndCode: "+ knd);
		try { 
			if( "PTCC".equals(rid) && "period".equals(act) ) { 
				cateList = (List<BaseAgendaVO>)agendaMngrService.getLoadOfConfPeriod( searchVO );

				if( !(cateList == null || cateList.size() <= 0) ) { 
					cateRult.clear();
					for( int c = 0; c < cateList.size(); c++ ) { 
						jsonObjs.clear();
						jsonObjs.put("root"          , "PTCC");
						jsonObjs.put("folder"        , "0".equals(cateList.get(c).getEraCount()) ? "false" : "true");
						jsonObjs.put("lazy"          , "true");
						jsonObjs.put("mode"          , "session");
						jsonObjs.put("era"           , cateList.get(c).getEraCode());
						jsonObjs.put("knd"           , knd);
						jsonObjs.put("title"         , cateList.get(c).getEraTitle());
						cateRult.add(jsonObjs);
					}
				}
			}

//log.debug(cateRult.toString());
			response.setContentType("application/x-json;charset=utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(cateRult);
			response.getWriter().flush();
			response.getWriter().close();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return null;
	}	// end of getLoadOfPeriodMeet

	/**
	 * <pre>
	 * 회의록 회기별 검색 - 대수별 회의일정을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  @RequestParam					// 
	 * @param  @RequestParam					// 
	 * @param  @RequestParam					// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/sess/loadClass.do")
	public String getLoadOfClassSess ( 
			@RequestParam("act") String act, 
			@RequestParam("rid") String rid, 
			@RequestParam("era") String era, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();

		BaseCommonVO			searchVO     = new BaseCommonVO();
		int						listCnt      = 0;
		List<BaseAgendaVO>		cateList     = new ArrayList<BaseAgendaVO>();
		JSONObject				jsonObjs     = new JSONObject();
		JSONArray				cateRult     = new JSONArray();

		searchVO.setReEraCode(BaseUtility.null2Blank( era ));

log.debug("actType: "+ act +" |rootID: "+ rid +" |eraCode: "+ era);
		try { 
			if( "PTCC".equals(rid) && "session".equals(act) ) { 
				cateList = (List<BaseAgendaVO>)agendaMngrService.getLoadOfSessRound( searchVO );

				if( !(cateList == null || cateList.size() <= 0) ) { 
					cateRult.clear();
					for( int c = 0; c < cateList.size(); c++ ) { 
						jsonObjs.clear();
						jsonObjs.put("root"          , "PTCC");
						jsonObjs.put("folder"        , "0".equals(cateList.get(c).getKndCount()) ? "false" : "true");
						jsonObjs.put("lazy"          , "true");
						jsonObjs.put("mode"          , "class");
						jsonObjs.put("era"           , cateList.get(c).getEraCode());
						jsonObjs.put("rnd"           , cateList.get(c).getRndCode());
						jsonObjs.put("title"         , cateList.get(c).getRndTitle());
						cateRult.add(jsonObjs);
					}
				}
			}

log.debug(cateRult.toString());
			response.setContentType("application/x-json;charset=utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(cateRult);
			response.getWriter().flush();
			response.getWriter().close();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return null;
	}	// end of getLoadOfClassSess

	/**
	 * <pre>
	 * 회의록 회의별 검색 - 대수별 회의일정을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  @RequestParam					// 
	 * @param  @RequestParam					// 
	 * @param  @RequestParam					// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/meet/loadClass.do")
	public String getLoadOfClassMeet ( 
			@RequestParam("act") String act, 
			@RequestParam("rid") String rid, 
			@RequestParam("era") String era, 
			@RequestParam("knd") String knd, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();

		int						listCnt      = 0;
		BaseCommonVO			searchVO     = new BaseCommonVO();
		List<BaseAgendaVO>		cateList     = new ArrayList<BaseAgendaVO>();
		JSONObject				jsonObjs     = new JSONObject();
		JSONArray				cateRult     = new JSONArray();

log.debug("actType: "+ act +" |rootID: "+ rid +" |eraCode: "+ era);
		try { 
			searchVO.setReEraCode(era);
			searchVO.setReKndCode(knd);

			if( "PTCC".equals(rid) && "session".equals(act) ) { 
				cateList = (List<BaseAgendaVO>)agendaMngrService.getLoadOfMeetRound( searchVO );

				if( !(cateList == null || cateList.size() <= 0) ) { 
					cateRult.clear();
					for( int c = 0; c < cateList.size(); c++ ) { 
						jsonObjs.clear();
						jsonObjs.put("root"          , "PTCC");
						jsonObjs.put("folder"        , "0".equals(cateList.get(c).getKndCount()) ? "false" : "true");
						jsonObjs.put("lazy"          , "true");
						jsonObjs.put("mode"          , "0".equals(cateList.get(c).getKndType()) ? "order" : "kind");
						jsonObjs.put("era"           , cateList.get(c).getEraCode());
						jsonObjs.put("rnd"           , cateList.get(c).getRndCode());
						jsonObjs.put("knd"           , BaseUtility.null2Char( cateList.get(c).getKndCode(), knd ));
						jsonObjs.put("title"         , cateList.get(c).getRndTitle());
						cateRult.add(jsonObjs);
					}
				}
			}

log.debug(cateRult.toString());
			response.setContentType("application/x-json;charset=utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(cateRult);
			response.getWriter().flush();
			response.getWriter().close();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return null;
	}	// end of getLoadOfClassMeet

	/**
	 * <pre>
	 * 회의록 연도별 검색 - 대수별 회의일정을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  @RequestParam					// 
	 * @param  @RequestParam					// 
	 * @param  @RequestParam					// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/year/loadClass.do")
	public String getLoadOfClassYear ( 
			@RequestParam("act") String act, 
			@RequestParam("rid") String rid, 
			@RequestParam("yos") String yos, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();

		int						listCnt      = 0;
		BaseCommonVO			searchVO     = new BaseCommonVO();
		List<BaseAgendaVO>		cateList     = new ArrayList<BaseAgendaVO>();
		JSONObject				jsonObjs     = new JSONObject();
		JSONArray				cateRult     = new JSONArray();

log.debug("actType: "+ act +" |rootID: "+ rid +" |yosCode: "+ yos);
		try { 
			searchVO.setReStdYear(yos);

			if( "PTCC".equals(rid) && "session".equals(act) ) { 
				cateList = (List<BaseAgendaVO>)agendaMngrService.getLoadOfYearRound( searchVO );

				if( !(cateList == null || cateList.size() <= 0) ) { 
					cateRult.clear();
					for( int c = 0; c < cateList.size(); c++ ) { 
						jsonObjs.clear();
						jsonObjs.put("root"          , "PTCC");
						jsonObjs.put("folder"        , "0".equals(cateList.get(c).getKndCount()) ? "false" : "true");
						jsonObjs.put("lazy"          , "true");
						jsonObjs.put("mode"          , "class");
						jsonObjs.put("era"           , cateList.get(c).getEraCode());
						jsonObjs.put("rnd"           , cateList.get(c).getRndCode());
						jsonObjs.put("knd"           , cateList.get(c).getKndCode());
						jsonObjs.put("title"         , cateList.get(c).getRndTitle());
						cateRult.add(jsonObjs);
					}
				}
			}

log.debug(cateRult.toString());
			response.setContentType("application/x-json;charset=utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(cateRult);
			response.getWriter().flush();
			response.getWriter().close();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return null;
	}	// end of getLoadOfClassYear

	/**
	 * <pre>
	 * 회의록 회의차수을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  @RequestParam					// 
	 * @param  @RequestParam					// 
	 * @param  @RequestParam					// 
	 * @param  @RequestParam					// 
	 * @param  @RequestParam					// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/cmmn/loadOrder.do")
	public String getLoadOfOrderList ( 
			@RequestParam("act") String act, 
			@RequestParam("rid") String rid, 
			@RequestParam("era") String era, 
			@RequestParam("rnd") String rnd, 
			@RequestParam("knd") String knd, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();

		BaseCommonVO			searchVO     = new BaseCommonVO();
		int						listCnt      = 0;
		List<BaseAgendaVO>		cateList     = new ArrayList<BaseAgendaVO>();
		JSONObject				jsonObjs     = new JSONObject();
		JSONArray				cateRult     = new JSONArray();

		searchVO.setReEraCode(BaseUtility.null2Blank( era ));
		searchVO.setReRndCode(BaseUtility.null2Blank( rnd ));
		searchVO.setReKndCode(BaseUtility.null2Blank( knd ));

log.debug("actType: "+ act +" |rootID: "+ rid +" |eraCode: "+ era +" |rndCode: "+ rnd +" |kndCode: "+ knd);
		try { 
			if( "PTCC".equals(rid) && "order".equals(act) ) { 
				cateList = (List<BaseAgendaVO>)agendaMngrService.getLoadOfConfPoint( searchVO );

				if( !(cateList == null || cateList.size() <= 0) ) { 
					cateRult.clear();
					for( int c = 0; c < cateList.size(); c++ ) { 
						jsonObjs.clear();
//						jsonObjs.put("title"         , "<a href='javascript:void(0);' onclick='open_viewer(\""+ webBase +"/viewer/viewer_frame.jsp?conf_code="+ cateList.get(c).getConfCode() +"&conf_name="+ cateList.get(c).getConfName() +"\")'>"+ cateList.get(c).getPntTitle() +"</a>");
						jsonObjs.put("title"         , "<a href='javascript:void(0);' onclick='fnDocuViewer(\""+ cateList.get(c).getConfUUID() +"\")'>"+ ("T".equals(cateList.get(c).getTempFlag()) ? "<span class='td_blu'>[임시회의록] </span> " : "") + cateList.get(c).getPntTitle() +"</a>");
						cateRult.add(jsonObjs);
					}
				}
			}

//log.debug(cateRult.toString());
			response.setContentType("application/x-json;charset=utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(cateRult);
			response.getWriter().flush();
			response.getWriter().close();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return null;
	}	// end of getLoadOfOrderList

	/**
	 * <pre>
	 * 영상회의록 내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  @RequestParam					// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/cmmn/loadCastVod.do")
	public String getLoadOfCastList ( 
			@RequestParam("viewNo") String viewNo, 
			@RequestParam("eraCode") String reEraCode, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();

		BaseCommonVO			searchVO     = new BaseCommonVO();
		int						listCnt      = 0;
		BaseAgendaVO			dataView     = new BaseAgendaVO();
		List<BaseAgendaVO>		cateList     = new ArrayList<BaseAgendaVO>();
		List<BaseAgendaVO>		distList     = new ArrayList<BaseAgendaVO>();
		StringBuffer			cateRult     = new StringBuffer();

		File					fileObjs     = null;

		searchVO.setReEraCode(reEraCode);
		searchVO.setNextUUID(BaseUtility.null2Blank( viewNo ));

log.debug("eraCode: "+ searchVO.getReEraCode());
log.debug("nextUUID: "+ viewNo);
		try {
			if( !BaseUtility.isValidNull( viewNo ) ) { 
				cateList = (List<BaseAgendaVO>)agendaMngrService.getListOfAgenda( searchVO );
				cateRult.setLength(0);

				if( cateList == null || cateList.size() <= 0 ) { 
					cateRult.append("<p>영상회의록이 존재하지 않습니다.</p>");
				} else { 
					fileObjs = new File(BaseUtility.getFileBase().replaceAll("/datas", "") + BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DATAS_VOD" ) +"/"+ cateList.get(0).getConfCode() +".mp4");
					if( fileObjs == null || !fileObjs.isFile() || fileObjs.length() == 0 ) { 
//						cateRult.append("<p>영상회의록 준비중입니다.</p>");
						cateRult.append("<p>영상회의록이 없습니다.<br/>(영상회의록은 제244회 임시회부터 제공됩니다.)</p>");
					} else { 
						searchVO.setConfCode(cateList.get(0).getConfCode());
						dataView = (BaseAgendaVO)agendaMngrService.getDetailOfConfBase( searchVO );
						if( dataView == null ) { 
//							cateRult.append("<p>영상회의록 준비중입니다.</p>");
							cateRult.append("<p>영상회의록이 없습니다.<br/>(영상회의록은 제244회 임시회부터 제공됩니다.)</p>");
						} else { 
							cateRult.append("<p>"+ dataView.getRoundName() +"["+ dataView.getRndivName() +"]"+ dataView.getPointName() +"</p>\n<ul>");
							// 동일시간 먼저 확인
							for( int c = 0; c < cateList.size(); c++ ) { 
								if( c == 0 ) { 
									distList.add(cateList.get(c));
								} else { 
									if(distList.get(distList.size()-1).getTimeStamp().equals(cateList.get(c).getTimeStamp()) ) { 
										distList.get(distList.size()-1).setSugTitle(distList.get(distList.size()-1).getSugTitle()+"<br/>"+cateList.get(c).getSugTitle());
									} else { 
										distList.add(cateList.get(c));
									}
								}
							}
							for( int c = 0; c < distList.size(); c++ ) { 
								cateRult.append("<li><a href='javascript:void(0);' onclick='fnCastViewer(\""+ dataView.getConfUUID() +"\",\""+ distList.get(c).getSugPosi() +"\",\""+ distList.get(c).getTimePos() +"\")'>"+ distList.get(c).getSugTitle() +"</a></li>");
							}
							cateRult.append("</ul>\n");
						}
					}
				} 
			}

//log.debug(cateRult.toString());
			response.setContentType("application/x-json;charset=utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(cateRult.toString());
			response.getWriter().flush();
			response.getWriter().close();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return null;
	}	// end of getLoadOfCastList

	/**
	 * <pre>
	 * 영상회의록 내역을(를) 취득해주는 메소드
	 * </pre>
	 * 
	 * @param  @RequestParam					// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/cmmn/loadCastMem.do")
	public String getLoadOfMembList ( 
			@RequestParam("viewNo") String viewNo, 
			@RequestParam("eraCode") String reEraCode, 
			@RequestParam("lstType") String reLstType, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();

		BaseCommonVO			searchVO     = new BaseCommonVO();
		int						listCnt      = 0;
		BaseAgendaVO			dataView     = new BaseAgendaVO();
		List<BaseAgendaVO>		cateList     = new ArrayList<BaseAgendaVO>();
		List<BaseAgendaVO>		distList     = new ArrayList<BaseAgendaVO>();
		StringBuffer			cateRult     = new StringBuffer();

		File					fileObjs     = null;

		searchVO.setReEraCode(reEraCode);
		searchVO.setCondType("T");
		searchVO.setCondValue(BaseUtility.null2Char( viewNo, viewNo, "%"+viewNo+"%" ));
		searchVO.setReLstType("G".equals(reLstType) ? "GOVER" : searchVO.getReLstType());
		searchVO.setReLstType("S".equals(reLstType) ? "SPCH"  : searchVO.getReLstType());

log.debug("condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("eraCode: "+ searchVO.getReEraCode() +" |lstType: "+ searchVO.getReLstType());
		try { 
			if( !BaseUtility.isValidNull( viewNo ) ) { 
				cateList = (List<BaseAgendaVO>)agendaMngrService.getListOfAgenda( searchVO );
				cateRult.setLength(0);

				if( cateList == null || cateList.size() <= 0 ) { 
					cateRult.append("<p>영상회의록이 존재하지 않습니다.</p>");
				} else { 
					fileObjs = new File(BaseUtility.getFileBase().replaceAll("/datas", "") + BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DATAS_VOD" ) +"/"+ cateList.get(0).getConfCode() +".mp4");
					if( fileObjs == null || !fileObjs.isFile() || fileObjs.length() == 0 ) { 
						cateRult.append("<p>영상회의록 준비중입니다.</p>");
					} else { 
						searchVO.setConfCode(cateList.get(0).getConfCode());
						dataView = (BaseAgendaVO)agendaMngrService.getDetailOfConfBase( searchVO );
						if( dataView == null ) { 
							cateRult.append("<p>영상회의록 준비중입니다.</p>");
						} else { 
							cateRult.append("<p>"+ dataView.getRoundName() +"["+ dataView.getRndivName() +"]"+ dataView.getPointName() +"</p>\n<ul>");
							// 동일시간 먼저 확인
							for( int c = 0; c < cateList.size(); c++ ) { 
								if( c == 0 ) { 
									distList.add(cateList.get(c));
								} else { 
									if(distList.get(distList.size()-1).getTimeStamp().equals(cateList.get(c).getTimeStamp()) ) { 
										distList.get(distList.size()-1).setSugTitle(distList.get(distList.size()-1).getSugTitle()+"<br/>"+cateList.get(c).getSugTitle());
									} else { 
										distList.add(cateList.get(c));
									}
								}
							}
							for( int c = 0; c < distList.size(); c++ ) { 
								cateRult.append("<li><a href='javascript:void(0);' onclick='fnCastViewer(\""+ dataView.getConfUUID() +"\",\""+ distList.get(c).getSugPosi() +"\",\""+ distList.get(c).getTimePos() +"\")'>"+ distList.get(c).getSugTitle() +"</a></li>");
							}
							cateRult.append("</ul>\n");
						}
					}
				} 
			}

//log.debug(cateRult.toString());
			response.setContentType("application/x-json;charset=utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(cateRult.toString());
			response.getWriter().flush();
			response.getWriter().close();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return null;
	}	// end of getLoadOfMembList

/** ---------------------------------------------------------------------------------------------------------------- **/
//	/**
//	 * <pre>
//	 * 회의대수 및 회의기수을(를) 조회해주는 메소드
//	 * </pre>
//	 * 
//	 * @param  HttpServletRequest				// 
//	 * @param  HttpServletResponse				// 
//	 * @param  @ModelAttribute				// 
//	 * @param  ModelMap							// 
//	 * @throws Exception
//	 */
//	@SuppressWarnings("rawtypes")
//	@RequestMapping(value="/common/selectClassList.do")
//	public void getListOfClassBySelect ( 
//			HttpServletRequest request, 
//			HttpServletResponse response, 
//			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
//			ModelMap model 
//		) throws Exception { 
//
//		String					webBase      = webContext.getServletContext().getContextPath();
//		HttpSession				session      = request == null ? null : request.getSession();
//
//		int						listCnt      = 0;
//		List					dataList     = null;
//		String					dataRult     = "";
//
//		try { 
//			dataList = searchGenrService.listSearchEraCodeHier(searchVO);
//			dataRult = "<option value='all'>전체</option>";
//
//			if( !(dataList == null || dataList.size() <= 0) ) { 
//				for( int d = 0; d < dataList.size(); d++ ) { 
//					dataRult += "\n<option value='"+ ((HashMap)dataList.get(d)).get("eraCode") +"'>";
//					dataRult += ((HashMap)dataList.get(d)).get("eraName");
//					dataRult += "</option>";
//				}
//			}
//
//			response.setContentType("text/html");
//			response.setCharacterEncoding("utf-8");
//			response.getWriter().print(dataRult);
//			response.getWriter().flush();
//			response.getWriter().close();
//		} catch( Exception E ) { 
//			log.debug(E.getMessage());
//		} finally { 
//		}	// end of try~catch~finally
//	}	// end of getListOfClassBySelect
//
//	/**
//	 * <pre>
//	 * 회의회수을(를) 조회해주는 메소드
//	 * </pre>
//	 * 
//	 * @param  HttpServletRequest				// 
//	 * @param  HttpServletResponse				// 
//	 * @param  @ModelAttribute				// 
//	 * @param  String							// 
//	 * @param  String							// 
//	 * @param  String							// 
//	 * @param  String							// 
//	 * @param  ModelMap							// 
//	 * @throws Exception
//	 */
//	@SuppressWarnings("rawtypes")
//	@RequestMapping(value="/common/selectMeetingList.do")
//	public void getListOfMettingBySelect ( 
//			HttpServletRequest request, 
//			HttpServletResponse response, 
//			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
//			@RequestParam("councilId") String councilId, 
//			@RequestParam("th") String th, 
//			@RequestParam("classType") String classType, 
//			@RequestParam("selectAll") String selectAll, 
//			ModelMap model 
//		) throws Exception { 
//
//		String					webBase      = webContext.getServletContext().getContextPath();
//		HttpSession				session      = request == null ? null : request.getSession();
//
//		int						listCnt      = 0;
//		List					dataList     = null;
//		String					dataRult     = "";
//
//		try { 
//			dataList = searchGenrService.listSearchEraCodeHier(searchVO);
//			dataRult = "<option value='all'>전체</option>";
//
//			if( !(dataList == null || dataList.size() <= 0) ) { 
//				for( int d = 0; d < dataList.size(); d++ ) { 
//					dataRult += "\n<option value='"+ ((HashMap)dataList.get(d)).get("eraCode") +"'>";
//					dataRult += ((HashMap)dataList.get(d)).get("eraName");
//					dataRult += "</option>";
//				}
//			}
//
//			response.setContentType("text/html");
//			response.setCharacterEncoding("utf-8");
//			response.getWriter().print(dataRult);
//			response.getWriter().flush();
//			response.getWriter().close();
//		} catch( Exception E ) { 
//			log.debug(E.getMessage());
//		} finally { 
//		}	// end of try~catch~finally
//	}	// end of getListOfMettingBySelect

}	// end of class