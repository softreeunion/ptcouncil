/*******************************************************************************
  Program ID  : FileController
  Description : 클라이언트의 요청(파일 및 이미지)에 관한 controller 클래스를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.cmmn.web;

import java.io.*;
import java.net.*;
import java.util.zip.*;

import javax.annotation.Resource;
import javax.servlet.http.*;

import org.apache.commons.io.IOUtils;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.*;

import egovframework.rte.fdl.property.*;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.util.*;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.agenda.service.*;

@Controller
public class FileController { 

	@Autowired
	private WebApplicationContext			webContext;

	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="agendaMngrService")
	protected AgendaMngrService			agendaMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));
	protected int							fileDnMax    = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "file.dnMax" ));


	@RequestMapping(value="/GetImage.do", method=RequestMethod.GET)
	public void getFileImage ( 
			@RequestParam(value="key",required=false) String fileID, 
			HttpServletRequest request, 
			HttpServletResponse response 
		) throws Exception { 

		File					fileObjs     = null;
		String					fileName     = "";
		String					fileExts     = "";
		String					fileCode     = "UTF-8";
		String[]				viewNo       = new String[2];
		BaseFilesVO				fileView     = new BaseFilesVO();

		if( !BaseUtility.isValidNull( fileID ) ) { 
			viewNo[0] = BaseUtility.null2Char( fileID.substring(0, 1), "0" );
			viewNo[1] = fileID.substring(1);
			fileView = (BaseFilesVO)baseCmmnService.getDetailOfDataFile( viewNo[1] );

			// NoImage Setting
			switch( Integer.parseInt(viewNo[0]) ) { 
				case 0  : viewNo[0] = BaseUtility.getTempBase() +"/images/common/NoImage800_Max.png"; break;
				case 1  : viewNo[0] = BaseUtility.getTempBase() +"/images/common/NoImage120_090.png"; break;
				case 2  : viewNo[0] = BaseUtility.getTempBase() +"/images/common/NoImage240_180.png"; break;
				case 3  : viewNo[0] = BaseUtility.getTempBase() +"/images/common/NoImage360_270.png"; break;
				case 4  : viewNo[0] = BaseUtility.getTempBase() +"/images/common/NoImage480_360.png"; break;
				case 5  : viewNo[0] = BaseUtility.getTempBase() +"/images/common/NoImage520_390.png"; break;
				case 6  : viewNo[0] = BaseUtility.getTempBase() +"/images/common/NoImage640_480.png"; break;
				case 9  : viewNo[0] = BaseUtility.getTempBase() +"/images/common/NoPhoto150_200.png"; break;
			}

			if( fileView == null ) { 
				fileObjs = new File(viewNo[0]);
				fileName = viewNo[0].substring(viewNo[0].lastIndexOf("/")+1);
				fileExts = FileProcess.getFileLowerExt( fileName );
			} else { 
				fileObjs = new File(BaseUtility.getFileBase() +"/"+ fileView.getFileAbs());
				fileName = fileView.getFileRel().substring(fileView.getFileRel().lastIndexOf("/")+1);
				if( fileObjs == null || !fileObjs.isFile() || fileObjs.length() <= 0 ) { 
					fileObjs = new File(viewNo[0]);
					fileName = viewNo[0].substring(viewNo[0].lastIndexOf("/")+1);
				}
				fileExts = FileProcess.getFileLowerExt( fileName );
			}
log.debug("File Name: "+ fileExts +" | "+ fileObjs.getAbsoluteFile() +" | "+ fileName);

			response.setContentType("image/"+ fileExts);
			response.setContentLength((int)fileObjs.length());
			response.setCharacterEncoding(fileCode);

			String browser		= FileProcess.getBrowser( request, response );
			String disposition	= FileProcess.getDisposition( "inline", fileName, browser );
//log.debug("browser: "+ browser  +" |disposition: "+ disposition);

			response.setHeader("Content-Disposition", disposition);
			response.setHeader("Accept-Ranges", "bytes");
			response.setHeader("Content-Transfer-Encoding", "binary;");
			response.setHeader("Pragma", "no-cache;");
			response.setHeader("Expires", "-1;");
			response.setHeader("Cache-control", "public");

//			int bytesRead				= 0;
//			byte[] buffer				= new byte[fileDnMax];
//			BufferedInputStream bis		= new BufferedInputStream(new FileInputStream(fileObjs));
//			BufferedOutputStream bos	= new BufferedOutputStream(response.getOutputStream());
			InputStream is				= new FileInputStream(fileObjs);

			try { 
//				while( (bytesRead = bis.read(buffer, 0, buffer.length)) != -1 ) 
//					bos.write(buffer, 0, bytesRead);
//
//				bos.close();
//				bis.close();
				IOUtils.copy(is, response.getOutputStream());
			} catch( IOException IOE ) { 
				log.debug(IOE.getMessage());
				return;
			} catch( Exception E ) { 
				log.debug(E.getMessage());
				log.warn("exception occurs during download "+ fileObjs.getAbsoluteFile());
				return;
			} finally { 
//				if( bos != null ) bos.flush();
//				if( bos != null ) bos.close();
//				if( bis != null ) bis.close();
				if( is  != null ) is.close();
				response.flushBuffer();
			}	// end of try~catch~finally
		}

		return;
	}	// end of getFileImage

	@RequestMapping(value="/GetMovie.do", method=RequestMethod.GET, produces=MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public void getFileMovie ( 
			@RequestParam(value="key",required=false) String fileID, 
			HttpServletRequest request, 
			HttpServletResponse response 
		) throws Exception { 

		File					fileObjs     = null;
		String					fileName     = "";
		String					fileExts     = "";
		String					fileCode     = "UTF-8";
		String[]				viewNo       = new String[2];
		BaseFilesVO				fileView     = new BaseFilesVO();

		if( !BaseUtility.isValidNull( fileID ) ) { 
			fileView = (BaseFilesVO)baseCmmnService.getDetailOfDataFile( fileID );

			if( fileView == null ) { 
				fileObjs = new File(viewNo[0]);
				fileName = viewNo[0].substring(viewNo[0].lastIndexOf("/")+1);
				fileExts = FileProcess.getFileLowerExt( fileName );
			} else { 
				fileObjs = new File(BaseUtility.getFileBase() +"/"+ fileView.getFileAbs());
				fileName = fileView.getFileRel().substring(fileView.getFileRel().lastIndexOf("/")+1);
				if( fileObjs == null || !fileObjs.isFile() || fileObjs.length() == 0 ) { 
					fileObjs = new File(viewNo[0]);
					fileName = viewNo[0].substring(viewNo[0].lastIndexOf("/")+1);
				}
				fileExts = FileProcess.getFileLowerExt( fileName );
			}
log.debug("File Name: "+ fileExts +" | "+ fileObjs.getAbsoluteFile() +" | "+ fileName);

			response.setContentType("video/"+ FileProcess.getFileLowerExt( fileObjs.getAbsoluteFile().getName() ));
			response.setContentLength((int)fileObjs.length());

			String browser		= FileProcess.getBrowser( request, response );
			String disposition	= FileProcess.getDisposition( "inline", fileName, browser );
//log.debug("browser: "+ browser  +" |disposition: "+ disposition);

			response.setHeader("Content-Disposition", disposition);
			response.setHeader("Accept-Ranges", "bytes");
			response.setHeader("Content-Transfer-Encoding", "binary;");
			response.setHeader("Pragma", "no-cache;");
			response.setHeader("Expires", "-1;");
			response.setHeader("Cache-control", "public");

			int bytesRead				= 0;
			byte[] buffer				= new byte[fileDnMax];
			BufferedInputStream bis		= new BufferedInputStream(new FileInputStream(fileObjs));
			BufferedOutputStream bos	= new BufferedOutputStream(response.getOutputStream());

			try { 
				while( (bytesRead = bis.read(buffer, 0, buffer.length)) != -1 ) 
					bos.write(buffer, 0, bytesRead);

				bos.close();
				bis.close();
			} catch( IOException IOE ) { 
				log.debug(IOE.getMessage());
				return;
			} catch( Exception E ) { 
				log.debug(E.getMessage());
				log.warn("exception occurs during download "+ fileObjs.getAbsoluteFile());
				return;
			} finally { 
				if( bos != null ) bos.flush();
				if( bos != null ) bos.close();
				if( bis != null ) bis.close();
				response.flushBuffer();
			}	// end of try~catch~finally
		}

		return;
	}	// end of getFileMovie

	@RequestMapping(value="/VodMovie.do", method=RequestMethod.GET, produces=MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public void getConfMovie ( 
			@RequestParam(value="key",required=false) String fileID, 
			HttpServletRequest request, 
			HttpServletResponse response 
		) throws Exception { 

		File					fileObjs     = null;
		String					fileName     = "";
		String					fileExts     = "";
		String					fileCode     = "UTF-8";
		String[]				viewNo       = new String[2];
		BaseAgendaVO			fileView     = new BaseAgendaVO();

		if( !BaseUtility.isValidNull( fileID ) ) { 
			fileView = (BaseAgendaVO)agendaMngrService.getDetailOfConfBase(fileID);

			if( fileView == null ) { 
				fileObjs = new File(viewNo[0]);
				fileName = viewNo[0].substring(viewNo[0].lastIndexOf("/")+1);
				fileExts = FileProcess.getFileLowerExt( fileName );
			} else { 
				fileObjs = new File(BaseUtility.getFileBase().replaceAll("/datas", "") + BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DATAS_VOD" ) +"/"+ fileView.getConfCode() +".mp4");
				fileName = fileView.getConfCode() +".mp4";
				if( fileObjs == null || !fileObjs.isFile() || fileObjs.length() == 0 ) { 
					fileObjs = new File(viewNo[0]);
					fileName = viewNo[0].substring(viewNo[0].lastIndexOf("/")+1);
				}
				fileExts = FileProcess.getFileLowerExt( fileName );
			}
log.debug("File Name: "+ fileExts +" | "+ fileObjs.getAbsoluteFile() +" | "+ fileName);

			response.setContentType("video/"+ FileProcess.getFileLowerExt( fileObjs.getAbsoluteFile().getName() ));
			response.setContentLength((int)fileObjs.length());

			String browser		= FileProcess.getBrowser( request, response );
			String disposition	= FileProcess.getDisposition( "inline", fileName, browser );
//log.debug("browser: "+ browser  +" |disposition: "+ disposition);

			response.setHeader("Content-Disposition", disposition);
			response.setHeader("Accept-Ranges", "bytes");
			response.setHeader("Content-Transfer-Encoding", "binary;");
			response.setHeader("Pragma", "no-cache;");
			response.setHeader("Expires", "-1;");
			response.setHeader("Cache-control", "public");

			try { 
				FileCopyUtils.copy(new FileInputStream(fileObjs), response.getOutputStream());
			} catch( IOException IOE ) { 
				log.debug(IOE.getMessage());
				return;
			} catch( Exception E ) { 
				log.debug(E.getMessage());
				log.warn("exception occurs during download "+ fileObjs.getAbsoluteFile());
				return;
			} finally {
				response.flushBuffer();
			}
		}

		return;
	}	// end of getConfMovie

	@RequestMapping(value="/cmmn/FileDown.do")
	public void FileDownBase ( 
			@ModelAttribute("searchVO")BaseFilesVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response 
		) throws Exception { 

		String					filePath     = "";
		String					fileName     = "";
		String					browser      = null;
		String					disposition  = null;
		File					fileObjs     = null;
//		String[]				viewNo       = new String[3];
//		BaseCommonVO			fileCond     = new BaseCommonVO();
		BaseFilesVO				fileView     = new BaseFilesVO();

		searchVO.setFileID(BaseUtility.null2Blank( searchVO.getFileID() ));

		if( !BaseUtility.isValidNull( searchVO.getFileID() ) ) { 
			searchVO.setFileID(searchVO.getFileID().length() <= keySize ? searchVO.getFileID() : searchVO.getFileID().substring(1)); 
//			viewNo = BaseUtility.null2Char( (String)baseCmmnService.getInfoOfFilesUUID( searchVO.getFileID(), "``" ).split("[`]", 3);
//log.debug("fileUUID: "+ searchVO.getFileID() +" |viewNo: "+ viewNo[0] +","+ viewNo[1] +","+ viewNo[2]);
//
//			fileCond = new BaseCommonVO();
//			fileCond.setFirDate(BaseUtility.null2Char( fileCond.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
//			fileCond.setLnkName(viewNo[0]);
//			fileCond.setLnkKey(viewNo[1]);
//			fileCond.setFileSeq(viewNo[2]);
//			fileView = (BaseFilesVO)baseCmmnService.getDetailOfDataFile( fileCond );

			fileView = (BaseFilesVO)baseCmmnService.getDetailOfDataFile( searchVO.getFileID() );
			if( fileView == null ) { 
				response.setContentType("text/html;charset=utf-8");
				PrintWriter out = response.getWriter();
				out.println("<script>");
				out.println("alert(\"다운로드할 파일이 존재하지 않습니다.\\r관리자에게 문의바랍니다.\");");
				out.println("if( window.opener ) window.close(); else history.go(-1);");
				out.println("</script>");
				out.close();
				out.flush();

				return;
			} else { 
				fileName = fileView.getFileRel().substring(fileView.getFileRel().lastIndexOf("/")+1);
				filePath = BaseUtility.getFileBase() +"/"+ fileView.getFileAbs();
//log.debug("File Name: "+ filePath +" | "+ fileName);

				fileObjs = new File(filePath);
				if( fileObjs == null || !fileObjs.isFile() || fileObjs.length() == 0 ) { 
					response.setContentType("text/html;charset=utf-8");
					PrintWriter out = response.getWriter();
					out.println("<script>");
					out.println("alert(\"다운로드할 파일이 존재하지 않습니다.\\r관리자에게 문의바랍니다.\");");
					out.println("if( window.opener ) window.close(); else history.go(-1);");
					out.println("</script>");
					out.close();
					out.flush();

					return;
				}
//log.debug("File Name: "+ fileObjs.getAbsoluteFile() +" | "+ fileName);

				response.setContentType("application/octet-stream;charset=utf-8");
				response.setContentLength((int)fileObjs.length());

				browser		= FileProcess.getBrowser( request, response );
				disposition	= FileProcess.getDisposition( "attachment", fileName, browser );
log.debug("browser: "+ browser  +" |disposition: "+ disposition);

				response.setHeader("Content-Disposition", disposition);
				response.setHeader("Accept-Ranges", "bytes");
				response.setHeader("Content-Transfer-Encoding", "binary;");
				response.setHeader("Pragma", "no-cache;");
				response.setHeader("Expires", "-1;");
				response.setHeader("Cache-control", "public");

				int bytesRead				= 0;
				byte[] buffer				= new byte[fileDnMax];
				BufferedInputStream bis		= new BufferedInputStream(new FileInputStream(fileObjs));
				BufferedOutputStream bos	= new BufferedOutputStream(response.getOutputStream());

				try { 
					while( (bytesRead = bis.read(buffer, 0, buffer.length)) != -1 ) 
						bos.write(buffer, 0, bytesRead);

					bos.close();
					bis.close();
				} catch( IOException IOE ) { 
					log.debug(IOE.getMessage());
					return;
				} catch( Exception E ) { 
					log.debug(E.getMessage());
					log.warn("exception occurs during download "+ filePath);
					return;
				} finally { 
					if( bos != null ) bos.flush();
					if( bos != null ) bos.close();
					if( bis != null ) bis.close();
					response.flushBuffer();
				}	// end of try~catch~finally
			}
		}

		return;
	}	// end of FileDownBase

	@RequestMapping(value="/cmmn/TempDown.do")
	public void FileDownTemp ( 
			@ModelAttribute("searchVO")BaseFilesVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response 
		) throws Exception { 

		String[]				inSave       = BaseUtility.isValidNull( searchVO.getFileRel() ) ? null : searchVO.getFileRel().split("[|]");
		String[]				inReal       = BaseUtility.isValidNull( searchVO.getFileAbs() ) ? null : searchVO.getFileAbs().split("[|]");
		String					filePath     = null;
		String					fileName     = null;
		String					browser      = null;
		String					disposition  = null;
		File					fileObjs     = null;

		if( !(inReal == null || inReal.length <= 0) ) { 
			for( int i = 0; i < inReal.length; i++ ) { 
				fileName = inSave[i].substring(inSave[i].lastIndexOf("/")+1);
				filePath = BaseUtility.getFileBase() +"/template/"+ inReal[i];
log.debug("File Name: "+ filePath +" | "+ fileName);

				fileObjs = new File(filePath);
				if( fileObjs == null || !fileObjs.isFile() || fileObjs.length() == 0 ) { 
					response.setContentType("text/html;charset=utf-8");
					PrintWriter out = response.getWriter();
					out.println("<script>");
					out.println("alert(\"다운로드할 파일이 존재하지 않습니다.\\r관리자에게 문의바랍니다.\");");
					out.println("if( window.opener ) window.close(); else history.go(-1);");
					out.println("</script>");
					out.close();
					out.flush();

					return;
				}
//log.debug("File Name: "+ fileObjs.getAbsoluteFile() +" | "+ fileName);

				response.setContentType("application/octet-stream;charset=utf-8");
				response.setContentLength((int)fileObjs.length());

				browser		= FileProcess.getBrowser( request, response );
				disposition	= FileProcess.getDisposition( "attachment", fileName, browser );
log.debug("browser: "+ browser  +" |disposition: "+ disposition);

				response.setHeader("Content-Disposition", disposition);
				response.setHeader("Accept-Ranges", "bytes");
				response.setHeader("Content-Transfer-Encoding", "binary;");
				response.setHeader("Pragma", "no-cache;");
				response.setHeader("Expires", "-1;");
				response.setHeader("Cache-control", "public");

				int bytesRead				= 0;
				byte[] buffer				= new byte[fileDnMax];
				BufferedInputStream bis		= new BufferedInputStream(new FileInputStream(fileObjs));
				BufferedOutputStream bos	= new BufferedOutputStream(response.getOutputStream());

				try { 
					while( (bytesRead = bis.read(buffer, 0, buffer.length)) != -1 ) 
						bos.write(buffer, 0, bytesRead);

					bos.close();
					bis.close();
				} catch( IOException IOE ) { 
					log.debug(IOE.getMessage());
					return;
				} catch( Exception E ) { 
					log.debug(E.getMessage());
					log.warn("exception occurs during download "+ filePath);
					return;
				} finally { 
					if( bos != null ) bos.flush();
					if( bos != null ) bos.close();
					if( bis != null ) bis.close();
					response.flushBuffer();
				}	// end of try~catch~finally
			}
		}

		return;
	}	// end of FileDownTemp

	@RequestMapping(value="/cmmn/LinkDown.do")
	public void FileDownLink ( 
			@ModelAttribute("searchVO")BaseFilesVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response 
		) throws Exception { 

		String[]				inReal       = BaseUtility.isValidNull( searchVO.getFileRel() ) ? null : searchVO.getFileRel().split("[|]");
		String[]				inSave       = BaseUtility.isValidNull( searchVO.getFileAbs() ) ? null : searchVO.getFileAbs().split("[|]");
		String					fileName     = null;
		String					browser      = null;
		String					disposition  = null;
		URL						fileUris     = null;
		HttpURLConnection		httpConn     = null;
		int						resCode      = 0;

		if( !(inReal == null || inReal.length <= 0) ) { 
			for( int i = 0; i < inReal.length; i++ ) { 
				fileName = inReal[i] +"."+ inSave[i].substring(inSave[i].lastIndexOf(".")+1);
log.debug("File Name: "+ inSave[i] +" | "+ fileName);

				if( !inSave[i].toLowerCase().startsWith("http://") ) inSave[i] = request.getRequestURL().toString().replaceAll(request.getRequestURI(), "") + inSave[i]; 

				fileUris = new URL(inSave[i]);
				httpConn = (HttpURLConnection) fileUris.openConnection();
				resCode  = httpConn.getResponseCode();
				if( resCode != HttpURLConnection.HTTP_OK ) { 
					response.setContentType("text/html;charset=utf-8");
					PrintWriter out = response.getWriter();
					out.println("<script>");
					out.println("alert(\"다운로드할 파일이 존재하지 않습니다.\\r관리자에게 문의바랍니다.\");");
					out.println("if(window.opener){window.close();}else{history.go(-1);}");
					out.println("</script>");
					out.close();
					out.flush();

					return;
				}
//log.debug("File Name: "+ fileObjs.getAbsoluteFile() +" | "+ fileName);

				response.setContentType(httpConn.getContentType());
				response.setContentLength(httpConn.getContentLength());

				browser		= FileProcess.getBrowser( request, response );
				disposition	= FileProcess.getDisposition( "attachment", fileName, browser );
log.debug("browser: "+ browser  +" |disposition: "+ disposition);

				response.setHeader("Content-Disposition", disposition);
				response.setHeader("Accept-Ranges", "bytes");
				response.setHeader("Content-Transfer-Encoding", "binary;");
				response.setHeader("Pragma", "no-cache;");
				response.setHeader("Expires", "-1;");
				response.setHeader("Cache-control", "public");

				int bytesRead				= 0;
				byte[] buffer				= new byte[fileDnMax];
				BufferedInputStream bis		= new BufferedInputStream(httpConn.getInputStream());
				BufferedOutputStream bos	= new BufferedOutputStream(response.getOutputStream());

				try { 
					while( (bytesRead = bis.read(buffer, 0, buffer.length)) != -1 ) 
						bos.write(buffer, 0, bytesRead);

					bos.close();
					bis.close();
					response.flushBuffer();
				} catch( IOException IOE ) { 
					log.debug(IOE.getMessage());
					return;
				} catch( Exception E ) { 
					log.debug(E.getMessage());
					log.warn("No file to download. Server replied HTTP code: " + resCode);
					return;
				} finally { 
					if( bos != null ) bos.flush();
					if( bos != null ) bos.close();
					if( bis != null ) bis.close();
					if( httpConn != null ) httpConn.disconnect();
				}	// end of try~catch~finally
			}
		}

		return;
	}	// end of FileDownLink

//	@RequestMapping(value="/cmmn/FormDown.do")
	public void FileDownForm ( 
			HttpServletRequest request, 
			HttpServletResponse response 
		) throws Exception { 

		String					saveFile     = BaseUtility.null2Char( request.getParameter("relFile"), (String)request.getAttribute("relFile") );
		String					filePath     = null;
		String					fileName     = null;
		String					browser      = null;
		String					disposition  = null;
		File					fileObjs     = null;

		fileName = saveFile.substring(saveFile.lastIndexOf("/")+1);
		filePath = BaseUtility.getFileBase() +"/"+ saveFile;
//log.debug("File Name: "+ filePath +" | "+ fileName);

		fileObjs = new File(filePath);
		if( fileObjs == null || !fileObjs.isFile() || fileObjs.length() == 0 ) { 
			response.setContentType("text/html;charset=utf-8");
			PrintWriter out = response.getWriter();
			out.println("<script>");
			out.println("alert(\"다운로드할 파일이 존재하지 않습니다.\\r관리자에게 문의바랍니다.\");");
			out.println("if(window.opener){window.close();}else{history.go(-1);}");
			out.println("</script>");
			out.close();
			out.flush();

			return;
		}
//log.debug("File Name: "+ fileObjs.getAbsoluteFile() +" | "+ fileName);

		response.setContentType("application/octet-stream;charset=utf-8");
		response.setContentLength((int)fileObjs.length());

		browser		= FileProcess.getBrowser( request, response );
		disposition	= FileProcess.getDisposition( "attachment", fileName, browser );
log.debug("browser: "+ browser  +" |disposition: "+ disposition);

		response.setHeader("Content-Disposition", disposition);
		response.setHeader("Accept-Ranges", "bytes");
		response.setHeader("Content-Transfer-Encoding", "binary;");
		response.setHeader("Pragma", "no-cache;");
		response.setHeader("Expires", "-1;");
		response.setHeader("Cache-control", "public");

		int bytesRead				= 0;
		byte[] buffer				= new byte[fileDnMax];
		BufferedInputStream bis		= new BufferedInputStream(new FileInputStream(fileObjs));
		BufferedOutputStream bos	= new BufferedOutputStream(response.getOutputStream());

		try { 
			while( (bytesRead = bis.read(buffer, 0, buffer.length)) != -1 ) 
				bos.write(buffer, 0, bytesRead);

			bos.close();
			bis.close();
		} catch( IOException IOE ) { 
			log.debug(IOE.getMessage());
			return;
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			log.warn("exception occurs during download "+ filePath);
			return;
		} finally { 
			if( bos != null ) bos.flush();
			if( bos != null ) bos.close();
			if( bis != null ) bis.close();
			response.flushBuffer();
		}	// end of try~catch~finally

		return;
	}	// end of FileDownForm

//	@RequestMapping(value="/cmmn/CompDown.do")
	public void FileDownComp ( 
			HttpServletRequest request, 
			HttpServletResponse response 
		) throws Exception { 

		File					fileObjs     = null;
		String					filePath     = null;
//		String					fileName     = null;
		String					saveName     = BaseUtility.null2Blank( request.getParameter("nmZip") );
		String					saveFile     = "";
		String[]				inReal       = request.getParameter("relFile") == null ? (request.getParameter("relFile") == null ? null : ((String)request.getParameter("relFile")).split("[|]")) : request.getParameter("relFile").split("[|]");
		String[]				inSave       = request.getParameter("absFile") == null ? (request.getParameter("absFile") == null ? null : ((String)request.getParameter("absFile")).split("[|]")) : request.getParameter("absFile").split("[|]");
		String					tempPath     = BaseUtility.getFileBase() +"/temp_zip";

//log.debug(String.valueOf(inReal.length));
		//임시 저장 대상 디렉토리가 존재하지 않으면 생성한다.
		if( !FileProcess.existDirectory( tempPath ) ) FileProcess.createDirectory( tempPath );

		if( !(inReal == null || inReal.length <= 0) ) { // 선택 다운처리
			ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(tempPath +"/"+ saveName +"_"+ BaseUtility.getDateOfNow( "yyyyMMdd ") +".zip")));

			try { 
				for( int i = 0; i < inReal.length; i++ ) { 
					saveFile = inSave[i];
//log.debug("Download File Name: "+ saveFile);

					zos.putNextEntry(new ZipEntry(saveFile));

					int bytesRead				= 0;
					byte[] buffer				= new byte[fileDnMax];
					FileInputStream fis			= new FileInputStream(BaseUtility.getFileBase() +"/"+ inReal[i]);
					BufferedInputStream bis		= new BufferedInputStream(fis, fileDnMax);
					try { 
						while( (bytesRead = bis.read(buffer, 0, buffer.length)) != -1 ) 
							zos.write(buffer, 0, bytesRead);

						zos.closeEntry();
						bis.close();
						fis.close();
					} catch( IOException IOE ) { 
						log.debug(IOE.getMessage());
						return;
					} catch( Exception E ) { 
						log.debug(E.getMessage());
						log.warn("exception occurs during download "+ filePath);
						return;
					} finally { 
						if( bis != null ) bis.close();
						if( fis != null ) fis.close();
					}	// end of try~catch~finally
				}
				saveFile = saveName +"_"+ BaseUtility.getDateOfNow( "yyyyMMdd" ) +".zip";
//log.debug("Download SaveFile : "+ tempPath +"/"+ saveFile);
				filePath = tempPath +"/"+ saveFile;
			} catch( IOException IOE ) { 
				log.debug(IOE.getMessage());
				return;
			} catch( Exception E ) { 
				log.debug(E.getMessage());
				log.warn("exception occurs during download "+ filePath);
				return;
			} finally { 
				if( zos != null ) zos.close();
			}	// end of try~catch~finally
		} else { 
			saveFile = inSave[0];
			filePath = tempPath +"/"+ inReal[0];
//log.debug("File Name: "+ inReal[0] +" | "+ saveFile);
		}

		File file = new File(filePath);
		if( file == null || !file.isFile() || file.length() == 0 ) { 
			response.setContentType("text/html;charset=utf-8");
			PrintWriter out = response.getWriter();
			out.println("<script>");
			out.println("alert(\"다운로드할 파일이 존재하지 않습니다.\\r관리자에게 문의바랍니다.\");");
			out.println("if(window.opener){window.close();}else{history.go(-1);}");
			out.println("</script>");
			out.close();
			out.flush();

			return;
		}

//		fileName = file.getName();
//log.debug("File Name: "+ file.getAbsoluteFile() +" | "+ saveFile);

		response.setContentType("application/octet-stream;charset=utf-8");
		response.setContentLength((int)file.length());

		String browser		= FileProcess.getBrowser( request, response );
		String disposition	= FileProcess.getDisposition( "attachment", saveFile, browser );
log.debug("browser: "+ browser  +"\ndisposition: "+ disposition);

		response.setHeader("Content-Disposition", disposition);
		response.setHeader("Accept-Ranges", "bytes");
		response.setHeader("Content-Transfer-Encoding", "binary;");
		response.setHeader("Pragma", "no-cache;");
		response.setHeader("Expires", "-1;");
		response.setHeader("Cache-control", "public");

		int bytesRead = 0;
		byte[] buffer = new byte[fileDnMax];
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
		BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());

		try { 
			while( (bytesRead = bis.read(buffer, 0, buffer.length)) != -1 ) 
				bos.write(buffer, 0, bytesRead);

			bos.close();
			bis.close();
		} catch( IOException IOE ) { 
			log.debug(IOE.getMessage());
			return;
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			log.warn("exception occurs during download "+ filePath);
			return;
		} finally { 
			if( bos != null ) bos.flush();
			if( bos != null ) bos.close();
			if( bis != null ) bis.close();
			response.flushBuffer();
		}	// end of try~catch~finally

		// 선택다운은 압축형태로의 작업으로 생성된 파일을 삭제한다.
		if( FileProcess.existFile( tempPath, saveFile ) ) FileProcess.removeFile( tempPath, saveFile );

		return;
	}	// end of FileDownComp

	@RequestMapping(value="/cmmn/AppDown.do")
	public void FileDownApp ( 
			@ModelAttribute("searchVO")BaseAgendaVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response 
		) throws Exception { 

		String					filePath     = "";
		String					fileName     = "";
		String					browser      = null;
		String					disposition  = null;
		File					fileObjs     = null;
		String[]				viewNo       = new String[2];
		BaseAgendaVO			fileView     = new BaseAgendaVO();

		searchVO.setFileID(BaseUtility.null2Blank( searchVO.getFileID() ));

		if( !BaseUtility.isValidNull( searchVO.getFileID() ) ) { 
			viewNo[0] = searchVO.getFileID().length() <= keySize ? searchVO.getFileID() : searchVO.getFileID().substring(0,keySize);
			viewNo[1] = searchVO.getFileID().length() <= keySize ? ""                   : searchVO.getFileID().substring(keySize);
//log.debug("viewNo: "+ viewNo[0]);

			fileView = (BaseAgendaVO)baseCmmnService.getDetailOfSuppFile( viewNo[0] );
			if( fileView == null ) { 
				response.setContentType("text/html;charset=utf-8");
				PrintWriter out = response.getWriter();
				out.println("<script>");
				out.println("alert(\"다운로드할 파일이 존재하지 않습니다.\\r관리자에게 문의바랍니다.\");");
				out.println("if(window.opener){window.close();}else{history.go(-1);}");
				out.println("</script>");
				out.close();
				out.flush();

				return;
			} else { 
				fileName = BaseUtility.null2Char( fileView.getAppTitle(), "", fileView.getAppTitle() +"."+ fileView.getAppExt() );
				filePath = BaseUtility.getFileBase().replaceAll("/datas", "") + BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DATAS_APP" ) +"/"+ BaseUtility.null2Char( fileView.getAppName(), "", fileView.getAppName() +"."+ fileView.getAppExt() );
log.debug("File Name: "+ filePath +" | "+ fileName);

				fileObjs = new File(filePath);
				if( fileObjs == null || !fileObjs.isFile() || fileObjs.length() == 0 ) { 
					response.setContentType("text/html;charset=utf-8");
					PrintWriter out = response.getWriter();
					out.println("<script>");
					out.println("alert(\"다운로드할 파일이 존재하지 않습니다.\\r관리자에게 문의바랍니다.\");");
					out.println("if(window.opener){window.close();}else{history.go(-1);}");
					out.println("</script>");
					out.close();
					out.flush();

					return;
				}
log.debug("File Name: "+ fileObjs.getAbsoluteFile() +" | "+ fileName);

				response.setContentType("application/octet-stream;charset=utf-8");
				response.setContentLength((int)fileObjs.length());

				browser		= FileProcess.getBrowser( request, response );
				disposition	= FileProcess.getDisposition( "attachment", fileName, browser );
log.debug("browser: "+ browser  +" |disposition: "+ disposition);

				response.setHeader("Content-Disposition", disposition);
				response.setHeader("Accept-Ranges", "bytes");
				response.setHeader("Content-Transfer-Encoding", "binary;");
				response.setHeader("Pragma", "no-cache;");
				response.setHeader("Expires", "-1;");
				response.setHeader("Cache-control", "public");

				int bytesRead				= 0;
				byte[] buffer				= new byte[fileDnMax];
				BufferedInputStream bis		= new BufferedInputStream(new FileInputStream(fileObjs));
				BufferedOutputStream bos	= new BufferedOutputStream(response.getOutputStream());

				try { 
					while( (bytesRead = bis.read(buffer, 0, buffer.length)) != -1 ) 
						bos.write(buffer, 0, bytesRead);

					bos.close();
					bis.close();
				} catch( IOException IOE ) { 
					log.debug(IOE.getMessage());
					return;
				} catch( Exception E ) { 
					log.debug(E.getMessage());
					log.warn("exception occurs during download "+ filePath);
					return;
				} finally { 
					if( bos != null ) bos.flush();
					if( bos != null ) bos.close();
					if( bis != null ) bis.close();
					response.flushBuffer();
				}	// end of try~catch~finally
			}
		}

		return;
	}	// end of FileDownApp

	@RequestMapping(value="/cmmn/SrcDown.do")
	public void FileDownSrc ( 
			@ModelAttribute("searchVO")BaseAgendaVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response 
		) throws Exception { 

		String					filePath     = "";
		String					fileName     = "";
		String					browser      = null;
		String					disposition  = null;
		File					fileObjs     = null;
		String[]				viewNo       = new String[2];
		BaseAgendaVO			dataView     = new BaseAgendaVO();

		searchVO.setFileID(BaseUtility.null2Blank( searchVO.getFileID() ));

		if( !BaseUtility.isValidNull( searchVO.getFileID() ) ) { 
			viewNo[0] = searchVO.getFileID().length() <= keySize ? searchVO.getFileID() : searchVO.getFileID().substring(0,keySize);
			viewNo[1] = searchVO.getFileID().length() <= keySize ? ""                   : searchVO.getFileID().substring(keySize);
//log.debug("viewNo: "+ viewNo[0]);

			dataView = (BaseAgendaVO)agendaMngrService.getDetailOfConfBase( viewNo[0] );
			if( dataView == null ) { 
				response.setContentType("text/html;charset=utf-8");
				PrintWriter out = response.getWriter();
				out.println("<script>");
				out.println("alert(\"다운로드할 파일이 존재하지 않습니다.\\r관리자에게 문의바랍니다.\");");
				out.println("if(window.opener){window.close();}else{history.go(-1);}");
				out.println("</script>");
				out.close();
				out.flush();

				return;
			} else { 
				fileName = BaseUtility.null2Char( dataView.getConfCode(), "", dataView.getConfCode() +".HWP" );
				filePath = BaseUtility.getFileBase().replaceAll("/datas", "") + BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DATAS_HWP" ) +"/"+ BaseUtility.null2Char( dataView.getConfCode(), "", dataView.getConfCode() +".HWP" );
//log.debug("File Name: "+ filePath +" | "+ fileName);

				fileObjs = new File(filePath);
				if( fileObjs == null || !fileObjs.isFile() || fileObjs.length() == 0 ) { 
					fileName = BaseUtility.null2Char( dataView.getConfCode(), "", dataView.getConfCode() +".hwp" );
					filePath = BaseUtility.getFileBase().replaceAll("/datas", "") + BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DATAS_HWP" ) +"/"+ BaseUtility.null2Char( dataView.getConfCode(), "", dataView.getConfCode() +".hwp" );
//log.debug("File Name: "+ filePath +" | "+ fileName);

					fileObjs = new File(filePath);
					if( fileObjs == null || !fileObjs.isFile() || fileObjs.length() == 0 ) { 
						response.setContentType("text/html;charset=utf-8");
						PrintWriter out = response.getWriter();
						out.println("<script>");
						out.println("alert(\"다운로드할 파일이 존재하지 않습니다.\\r관리자에게 문의바랍니다.\");");
						out.println("if(window.opener){window.close();}else{history.go(-1);}");
						out.println("</script>");
						out.close();
						out.flush();

						return;
					}
				}
log.debug("File Name: "+ fileObjs.getAbsoluteFile() +" | "+ fileName);

				response.setContentType("application/octet-stream;charset=utf-8");
				response.setContentLength((int)fileObjs.length());

				browser		= FileProcess.getBrowser( request, response );
				disposition	= FileProcess.getDisposition( "attachment", fileName, browser );
log.debug("browser: "+ browser  +" |disposition: "+ disposition);

				response.setHeader("Content-Disposition", disposition);
				response.setHeader("Accept-Ranges", "bytes");
				response.setHeader("Content-Transfer-Encoding", "binary;");
				response.setHeader("Pragma", "no-cache;");
				response.setHeader("Expires", "-1;");
				response.setHeader("Cache-control", "public");

				int bytesRead				= 0;
				byte[] buffer				= new byte[fileDnMax];
				BufferedInputStream bis		= new BufferedInputStream(new FileInputStream(fileObjs));
				BufferedOutputStream bos	= new BufferedOutputStream(response.getOutputStream());

				try { 
					while( (bytesRead = bis.read(buffer, 0, buffer.length)) != -1 ) 
						bos.write(buffer, 0, bytesRead);

					bos.close();
					bis.close();
				} catch( IOException IOE ) { 
					log.debug(IOE.getMessage());
					return;
				} catch( Exception E ) { 
					log.debug(E.getMessage());
					log.warn("exception occurs during download "+ filePath);
					return;
				} finally { 
					if( bos != null ) bos.flush();
					if( bos != null ) bos.close();
					if( bis != null ) bis.close();
					response.flushBuffer();
				}	// end of try~catch~finally
			}
		}

		return;
	}	// end of FileDownSrc

	@RequestMapping(value={"/copilot/{paramPath}","/copilot/{paramPath}.py","/copilot/{paramPath}.do"})
	public void FileDownBody ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseAgendaVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response 
		) throws Exception { 

		String					filePath     = "";
		String					fileName     = "";
		String					browser      = null;
		String					disposition  = null;
		File					fileObjs     = null;
		String					uuidNo       = BaseUtility.null2Blank( paramPath );
		String[]				viewNo       = new String[2];
		BaseFilesVO				fileView     = new BaseFilesVO();

		if( !BaseUtility.isValidNull( paramPath ) ) { 
			viewNo[0] = uuidNo.length() <= keySize ? uuidNo : uuidNo.substring(0,keySize);
			viewNo[1] = uuidNo.length() <= keySize ? ""     : uuidNo.substring(keySize);
//log.debug("viewNo: "+ viewNo[0]);

			fileView = (BaseFilesVO)baseCmmnService.getDetailOfBodyFile( viewNo[0] );
			if( fileView == null ) { 
				response.setContentType("text/html;charset=utf-8");
				PrintWriter out = response.getWriter();
				out.println("<script>");
				out.println("alert(\"다운로드할 파일이 존재하지 않습니다.\\r관리자에게 문의바랍니다.\");");
				out.println("if(window.opener){window.close();}else{history.go(-1);}");
				out.println("</script>");
				out.close();
				out.flush();

				return;
			} else { 
				fileName = fileView.getFileRel().substring(fileView.getFileRel().lastIndexOf("/")+1);
				filePath = BaseUtility.getFileBase() +"/"+ fileView.getFileAbs();
//log.debug("File Name: "+ filePath +" | "+ fileName);

				fileObjs = new File(filePath);
				if( fileObjs == null || !fileObjs.isFile() || fileObjs.length() == 0 ) { 
					response.setContentType("text/html;charset=utf-8");
					PrintWriter out = response.getWriter();
					out.println("<script>");
					out.println("alert(\"다운로드할 파일이 존재하지 않습니다.\\r관리자에게 문의바랍니다.\");");
					out.println("if(window.opener){window.close();}else{history.go(-1);}");
					out.println("</script>");
					out.close();
					out.flush();

					return;
				}
log.debug("File Name: "+ fileObjs.getAbsoluteFile() +" | "+ fileName);

				response.setContentType("application/octet-stream;charset=utf-8");
				response.setContentLength((int)fileObjs.length());

				browser		= FileProcess.getBrowser( request, response );
				disposition	= FileProcess.getDisposition( "attachment", fileName, browser );
log.debug("browser: "+ browser  +" |disposition: "+ disposition);

				response.setHeader("Content-Disposition", disposition);
				response.setHeader("Accept-Ranges", "bytes");
				response.setHeader("Content-Transfer-Encoding", "binary;");
				response.setHeader("Pragma", "no-cache;");
				response.setHeader("Expires", "-1;");
				response.setHeader("Cache-control", "public");

				int bytesRead				= 0;
				byte[] buffer				= new byte[fileDnMax];
				BufferedInputStream bis		= new BufferedInputStream(new FileInputStream(fileObjs));
				BufferedOutputStream bos	= new BufferedOutputStream(response.getOutputStream());

				try { 
					while( (bytesRead = bis.read(buffer, 0, buffer.length)) != -1 ) 
						bos.write(buffer, 0, bytesRead);

					bos.close();
					bis.close();
				} catch( IOException IOE ) { 
					log.debug(IOE.getMessage());
					return;
				} catch( Exception E ) { 
					log.debug(E.getMessage());
					log.warn("exception occurs during download "+ filePath);
					return;
				} finally { 
					if( bos != null ) bos.flush();
					if( bos != null ) bos.close();
					if( bis != null ) bis.close();
					response.flushBuffer();
				}	// end of try~catch~finally
			}
		}

		return;
	}	// end of FileDownBody

}	// end of class