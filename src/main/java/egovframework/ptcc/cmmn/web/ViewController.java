/*******************************************************************************
  Program ID  : ViewController
  Description : 클라이언트의 요청(파일 및 이미지)에 관한 controller 클래스를 정의한다.
  Author      : HexaMedia new0man
  Write Date  : 2019.07.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.07.01  최초 생성
  v1.0    HexaMedia new0man      2019.07.01  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.cmmn.web;

import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;

import egovframework.rte.fdl.property.*;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.util.FileProcess;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.agenda.service.*;
import egovframework.ptcc.mngr.board.service.*;

@Controller
public class ViewController {

	@Autowired
	private WebApplicationContext			webContext;

	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="boardMngrService")
	protected BoardMngrService				boardMngrService;
	@Resource(name="agendaMngrService")
	protected AgendaMngrService			agendaMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));
	protected String						DATAS_HTML   = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DATAS_HTML" );


	@RequestMapping(value="/cmmn/DocuViewer.do", method=RequestMethod.GET)
	public String getViewerOfDocus ( 
			@RequestParam(value="key",required=false) String confID, 
			@RequestParam(value="kag",required=false) String confAG, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();

		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		BaseAgendaVO			dataView     = new BaseAgendaVO();
		BaseCommonVO			searchVO     = new BaseCommonVO();
		String					fileHtml     = BaseUtility.getFileBase().replaceAll("/datas", "") + DATAS_HTML +"/";
		String					fontSize     = "11";
		String					fontKind     = "굴림";
		String					fontLine     = "160";
		String					fontLink     = "blue";
		StringBuffer			confStyle    = new StringBuffer();
		StringBuffer			confView     = new StringBuffer();

		try { 
			if( !BaseUtility.isValidNull( confID ) ) { 
				searchVO.setRowUUID(confID.length() < keySize ? confID : confID.substring(0,keySize));
				searchVO.setRowSeq( confID.length() < keySize ? ""     : confID.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
				dataView = (BaseAgendaVO)agendaMngrService.getDetailOfConfBase( searchVO );

				if( dataView != null ) { 
					// Data Adjustment(Agendas)
					dataView.setActMode("Update");
					dataView.setViewNo(dataView.getConfUUID());

					dataView.setConfDate(BaseUtility.toDateFormat( dataView.getConfDate(), dtSep, 8 ));
					dataView.setConfStartTime(BaseUtility.toTimeFormat( dataView.getConfStartTime() ));
					dataView.setConfCloseTime(BaseUtility.toTimeFormat( dataView.getConfCloseTime() ));
					dataView.setConfReqTime(BaseUtility.toTimeFormat( dataView.getConfReqTime() ));

					// 회의록 본문
					//	fSize:11, fLine:160,fKind:굴림
					//	span{font-size:11pt;line-height:160%;font-family:굴림;}
					//	.Title{font-size:26pt;line-height:160%;font-family:굴림;font-weight:bold;text-align:center;}
					//	.Sub{font-size:16pt;line-height:160%;font-family:굴림;font-weight:bold;text-align:right;}
					//	.Sug{font-size:13pt;line-height:160%;font-family:굴림;font-weight:bold;text-align:right;}
					//	.HStyle0{text-indent:0.000pt;margin-right:0.000pt;margin-top:0.000pt;margin-bottom:0.000pt;}
					confStyle= new StringBuffer();
					confStyle.append("\n\tspan{font-size:"+ fontSize +"pt;line-height:"+ fontLine +"%;font-family:"+ fontKind +";}");
					confStyle.append("\n\t.Title{font-size:"+ String.valueOf(Integer.parseInt(fontSize)+15) +"pt;line-height:"+ fontLine +"%;font-family:"+ fontKind +";font-weight:bold;text-align:center;}");
					confStyle.append("\n\t.Sub{font-size:"+ String.valueOf(Integer.parseInt(fontSize)+5) +"pt;line-height:"+ fontLine +"%;font-family:"+ fontKind +";font-weight:bold;text-align:right;}");
					confStyle.append("\n\t.Sug{font-size:"+ String.valueOf(Integer.parseInt(fontSize)+2) +"pt;line-height:"+ fontLine +"%;font-family:"+ fontKind +";font-weight:bold;text-align:right;}");
					confStyle.append("\n\tA:link{color:"+ fontLink +";text-decoration:none;}");
					confStyle.append("\n\tA:active{color:blue;text-decoration:none;}");
					confStyle.append("\n\tA:visited{color:"+ fontLink +";text-decoration:none;}");
					confStyle.append("\n\tA:hover{color:red;text-decoration:underline;}");
					confStyle.append("\n\tHR{color:#aee0b3;display:block !important;}");
					confStyle.append("\n\t.HStyle0{text-indent:0.000pt;margin-right:0.000pt;margin-top:0.000pt;margin-bottom:0.000pt;}");
					confStyle.append("\n\t");
					confView = new StringBuffer();
					confView.append(FileProcess.getReadFile("euc-kr", fileHtml + dataView.getConfCode() +".html"));
				}
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("confStyle"     , confStyle);
			model.addAttribute("confView"      , confView.toString());
			model.addAttribute("confAG"        , confAG);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/cmmn/pop_minutesView";
	}	// end of getViewerOfDocus

	@SuppressWarnings("unchecked")
	@RequestMapping(value="/cmmn/CastViewer.do", method=RequestMethod.GET)
	public String getViewerOfCasts ( 
			@RequestParam(value="key",required=false) String confID, 
			@RequestParam(value="kag",required=false) String confAG, 
			@RequestParam(value="kps",required=false) String confPS, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();

		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		BaseAgendaVO			dataView     = new BaseAgendaVO();
		List<BaseAgendaVO>		cateList     = new ArrayList<BaseAgendaVO>();
		List<BaseAgendaVO>		vidsView     = new ArrayList<BaseAgendaVO>();
		BaseCommonVO			searchVO     = new BaseCommonVO();
		String					fileHtml     = BaseUtility.getFileBase().replaceAll("/datas", "") + DATAS_HTML +"/";
		String					fontSize     = "11";
		String					fontKind     = "굴림";
		String					fontLine     = "160";
		String					fontLink     = "blue";
		StringBuffer			confStyle    = new StringBuffer();
		StringBuffer			confView     = new StringBuffer();
		StringBuffer			itemView     = new StringBuffer();

		try { 
			if( !BaseUtility.isValidNull( confID ) ) { 
				searchVO.setRowUUID(confID.length() < keySize ? confID : confID.substring(0,keySize));
				searchVO.setRowSeq( confID.length() < keySize ? ""     : confID.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
				dataView = (BaseAgendaVO)agendaMngrService.getDetailOfConfBase( searchVO );

				if( dataView != null ) { 
					// Data Adjustment(Agendas)
					dataView.setActMode("Update");
					dataView.setViewNo(dataView.getConfUUID());

					dataView.setConfDate(BaseUtility.toDateFormat( dataView.getConfDate(), dtSep, 8 ));
					dataView.setConfStartTime(BaseUtility.toTimeFormat( dataView.getConfStartTime() ));
					dataView.setConfCloseTime(BaseUtility.toTimeFormat( dataView.getConfCloseTime() ));
					dataView.setConfReqTime(BaseUtility.toTimeFormat( dataView.getConfReqTime() ));

					// 회의록 본문
					//	fSize:11, fLine:160,fKind:굴림
					//	span{font-size:11pt;line-height:160%;font-family:굴림;}
					//	.Title{font-size:26pt;line-height:160%;font-family:굴림;font-weight:bold;text-align:center;}
					//	.Sub{font-size:16pt;line-height:160%;font-family:굴림;font-weight:bold;text-align:right;}
					//	.Sug{font-size:13pt;line-height:160%;font-family:굴림;font-weight:bold;text-align:right;}
					//	.HStyle0{text-indent:0.000pt;margin-right:0.000pt;margin-top:0.000pt;margin-bottom:0.000pt;}
					confStyle= new StringBuffer();
					confStyle.append("\n\tspan{font-size:"+ fontSize +"pt;line-height:"+ fontLine +"%;font-family:"+ fontKind +";}");
					confStyle.append("\n\t.Title{font-size:"+ String.valueOf(Integer.parseInt(fontSize)+15) +"pt;line-height:"+ fontLine +"%;font-family:"+ fontKind +";font-weight:bold;text-align:center;}");
					confStyle.append("\n\t.Sub{font-size:"+ String.valueOf(Integer.parseInt(fontSize)+5) +"pt;line-height:"+ fontLine +"%;font-family:"+ fontKind +";font-weight:bold;text-align:right;}");
					confStyle.append("\n\t.Sug{font-size:"+ String.valueOf(Integer.parseInt(fontSize)+2) +"pt;line-height:"+ fontLine +"%;font-family:"+ fontKind +";font-weight:bold;text-align:right;}");
					confStyle.append("\n\tA:link{color:"+ fontLink +";text-decoration:none;}");
					confStyle.append("\n\tA:active{color:blue;text-decoration:none;}");
					confStyle.append("\n\tA:visited{color:"+ fontLink +";text-decoration:none;}");
					confStyle.append("\n\tA:hover{color:red;text-decoration:underline;}");
					confStyle.append("\n\tHR{color:#aee0b3;display:block !important;}");
					confStyle.append("\n\t.HStyle0{text-indent:0.000pt;margin-right:0.000pt;margin-top:0.000pt;margin-bottom:0.000pt;}");
					confStyle.append("\n\t");
					confView = new StringBuffer();
					confView.append(FileProcess.getReadFile("euc-kr", fileHtml + dataView.getConfCode() +".html"));

					if( !BaseUtility.isValidNull( dataView.getChildTime() ) ) { 
						cateList = (List<BaseAgendaVO>)dataView.getChildTime();
						// 동일시간 먼저 확인
						if( !(cateList == null || cateList.size() <= 0) ) { 
							for( int c = 0; c < cateList.size(); c++ ) { 
								if( c == 0 ) { 
									vidsView.add(cateList.get(c));
								} else { 
									if(vidsView.get(vidsView.size()-1).getTimeStamp().equals(cateList.get(c).getTimeStamp()) ) { 
										vidsView.get(vidsView.size()-1).setSugTitle(vidsView.get(vidsView.size()-1).getSugTitle()+"<br/>"+cateList.get(c).getSugTitle());
									} else { 
										vidsView.add(cateList.get(c));
									}
								}
							}
						}
						if( !(vidsView == null || vidsView.size() <= 0) ) { 
							itemView.append("if(currTime<"+ BaseUtility.null2Zero( vidsView.get(0).getTimePos() ) +"){currMemb=\""+ vidsView.get(0).getTimeMemb() +"\";currItem=0;currRecd=\""+ BaseUtility.null2Blank( vidsView.get(0).getSugPosi() ) +"\";}");
							for( int t = 0; t < vidsView.size(); t++ )  
								itemView.append("\n\t\t\t\tif(currTime>="+ BaseUtility.null2Zero( vidsView.get(t).getTimePos() ) +"){currMemb=\""+ vidsView.get(t).getTimeMemb() +"\";currItem="+ t +";currRecd=\""+ BaseUtility.null2Blank( vidsView.get(t).getSugPosi() ) +"\";}");
						}
					}
				}
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("vidsView"      , vidsView);
			model.addAttribute("confStyle"     , confStyle);
			model.addAttribute("confView"      , confView.toString());
			model.addAttribute("itemView"      , itemView.toString());
			model.addAttribute("confAG"        , confAG);
			model.addAttribute("confPS"        , confPS);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/cmmn/pop_mediasView";
	}	// end of getViewerOfCasts

	@RequestMapping(value="/cmmn/ImgsDown.do")
	public String getViewerOfImage ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseBoardsVO			dataView     = new BaseBoardsVO();

		if( BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

log.debug("viewNo: "+ searchVO.getViewNo());
		try { 
			dataView.setBbsTitle(BaseUtility.null2Blank( searchVO.getCondName() ));

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("viewNo"        , uuidNo);
			model.addAttribute("optsNo"        , "print".equals(BaseUtility.null2Blank( searchVO.getCondCate() ))?"T":"F");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/cmmn/pop_imagesView";
	}	// end of getViewerOfImage

	/**
	 * 1-1.열린의장실	포토 의정활동	/cmmn/intro/GetPhoto.do	pop_imagesView.jsp	i	1
	 * 1-2.의정활동		포토 의정활동	/cmmn/count/GetPhoto.do	pop_imagesView.jsp	c	47
	 * 2-1.상임의원회	포토 의정활동	/cmmn/stand/GetPhoto.do	pop_imagesView.jsp	s	2
	 * 2-1.운영위		포토 의정활동	/cmmn/opert/GetPhoto.do	pop_imagesView.jsp	o	3
	 * 2-1.자치위		포토 의정활동	/cmmn/gvadm/GetPhoto.do	pop_imagesView.jsp	g	4
	 * 2-1.자치위		포토 의정활동	/cmmn/welfare/GetPhoto.do	pop_imagesView.jsp	w	6
	 * 2-1.산건위		포토 의정활동	/cmmn/build/GetPhoto.do	pop_imagesView.jsp	b	5
	 * 3-1.모의의회		포토갤러리		/cmmn/youth/GetPhoto.do	pop_imagesView.jsp	y	55
	 */
	@RequestMapping(value={"/player/{paramPath}","/player/{paramPath}.py","/player/{paramPath}.do"})
	public String getViewerOfPhoto ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();

		String					uuidNo       = BaseUtility.null2Blank( paramPath );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		BaseBoardsVO			dataView     = new BaseBoardsVO();
		List<BaseBoardsVO>		chldFile     = new ArrayList<BaseBoardsVO>();

		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			if( !BaseUtility.isValidNull( paramPath ) ) { 
				viewNo = BaseUtility.null2Char( (String)boardMngrService.getInfoOfPhotoUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(게시판)
				searchVO.setBbsType(viewNo[0]);
				searchVO.setBbsSeq(viewNo[1]);
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());

				dataView = (BaseBoardsVO)boardMngrService.getDetailOfPhoto( searchVO );
				if( dataView != null ) { 
					boardMngrService.doRtnOfModifyPhotoHit( dataView );

					// Data Adjustment(Boards)
					dataView.setActMode("Update");
					dataView.setViewNo(dataView.getBbsUUID());

					dataView.setHitCount(BaseUtility.toNumberFormat( Integer.parseInt(BaseUtility.null2Zero( dataView.getHitCount() )) + 1 ));
					dataView.setCreDate(BaseUtility.toDateFormat( dataView.getCreDate(), dtSep, 8 ));
					dataView.setModDate(BaseUtility.toDateFormat( dataView.getModDate(), dtSep, 8 ));
					dataView.setDelDate(BaseUtility.toDateFormat( dataView.getDelDate(), dtSep, 8 ));
					dataView.setTakeDate(BaseUtility.toDateFormat( dataView.getTakeDate(), dtSep, 8 ));

					dataView.setContDescBR(BaseUtility.tag2Sym( dataView.getContDesc() ));
					dataView.setContDescCR(BaseUtility.sym2Tag( dataView.getContDesc() ));
					dataView.setContLinkC(BaseUtility.getUriCheck( dataView.getBbsSiteUri() ) ? "T" : "F");
					dataView.setCreLock(BaseUtility.null2Char( dataView.getCrePasswd(), "", "Y" ));
					dataView.setDownExt(FileProcess.getFileLowerExt( dataView.getDownRel() ));
					dataView.setFileUUID(BaseUtility.null2Char(dataView.getFileUUID(), "", "6"+ dataView.getFileUUID() ));
					dataView.setRegiDate(BaseUtility.toDateFormat( BaseUtility.null2Char( dataView.getRegiDate(), dataView.getCreDate() ), dtSep, 8 ));
					if( !dataView.getCreUser().equals(dataView.getCreName()) ) dataView.setCreName(propertiesService.getString("creName"));

					// 추가게시판 내역
					chldFile = (List<BaseBoardsVO>)boardMngrService.getChldOfPhoto( searchVO );
				}
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("chldFile"      , chldFile);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/cmmn/pop_imagesView";
	}	// end of getViewerOfPhoto

}	// end of class