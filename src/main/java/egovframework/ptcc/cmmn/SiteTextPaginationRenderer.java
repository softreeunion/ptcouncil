/*******************************************************************************
  Program ID  : SiteTextPaginationRenderer
  Description : Pagination Renderer UX 구성
  Author      : HexaMedia new0man
  Write Date  : 2019.06.04
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v2.0    HexaMedia new0man      2019.06.04  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.cmmn;

import javax.servlet.ServletContext;

public class SiteTextPaginationRenderer extends egovframework.rte.ptl.mvc.tags.ui.pagination.AbstractPaginationRenderer implements org.springframework.web.context.ServletContextAware { 

	private ServletContext					servletContext;

	public SiteTextPaginationRenderer() {}

	private void initVariables() { 
		firstPageLabel    = " <a href=\"javascript:void(0);\" onclick=\"{0}({1})\">처음</a> ";
		previousPageLabel = " <a href=\"javascript:void(0);\" onclick=\"{0}({1})\">이전</a> ";
		currentPageLabel  = " <a href=\"javascript:void(0);\" class=\"on\">{0}</a> ";
		otherPageLabel    = " <a href=\"javascript:void(0);\" onclick=\"{0}({1})\">{2}</a> ";
		nextPageLabel     = " <a href=\"javascript:void(0);\" onclick=\"{0}({1})\">다음</a> ";
		lastPageLabel     = " <a href=\"javascript:void(0);\" onclick=\"{0}({1})\">마지막</a> ";
	}

	@Override
	public void setServletContext(ServletContext servletContext) { 
		this.servletContext = servletContext;
		initVariables();
	}

}	// end of class