/*******************************************************************************
  Program ID  : BaseMobilePaginationRenderer
  Description : Pagination Renderer UX 구성
  Author      : HexaMedia new0man
  Write Date  : 2019.06.04
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v2.0    HexaMedia new0man      2019.06.04  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.cmmn;

import javax.servlet.ServletContext;

public class BaseMobilePaginationRenderer extends egovframework.rte.ptl.mvc.tags.ui.pagination.AbstractPaginationRenderer implements org.springframework.web.context.ServletContextAware { 

	private ServletContext					servletContext;

	public BaseMobilePaginationRenderer() {}

	private void initVariables() { 
		firstPageLabel    = "  ";
		previousPageLabel = " <a href=\"javascript:void(0);\" onclick=\"{0}({1})\" class=\"w3-button\">&#10094; Previous</a> ";
		currentPageLabel  = "  ";
		otherPageLabel    = "  ";
		nextPageLabel     = " <a href=\"javascript:void(0);\" onclick=\"{0}({1})\" class=\"w3-button w3-right\">Next &#10095;</a> ";
		lastPageLabel     = "  ";
	}

	@Override
	public void setServletContext(ServletContext servletContext) { 
		this.servletContext = servletContext;
		initVariables();
	}

}	// end of class