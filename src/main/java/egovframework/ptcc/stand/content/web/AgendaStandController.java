/*******************************************************************************
  Program ID  : AgendaStandController
  Description : 클라이언트의 요청(공통)을 처리할 메서드를 구현
  Author      : HexaMedia new0man
  Write Date  : 2019.06.04
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v1.0    (주)헥사미디어 개발자  2014.00.00  최초 생성
  v2.0    HexaMedia new0man      2019.06.04  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.stand.content.web;

import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.util.*;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.agenda.service.*;

@Controller
public class AgendaStandController { 

	@Autowired
	private WebApplicationContext			webContext;
	@Autowired
	private DataSourceTransactionManager	txManager;

	@Resource(name="beanValidator")
	protected DefaultBeanValidator			beanValidator;
	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="agendaMngrService")
	protected AgendaMngrService			agendaMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));
	protected boolean						notiItems    = false;
	protected boolean						moveItems    = true;
	protected boolean						fileUP       = false;
	protected boolean						tempUP       = false;


	/**
	 * <pre>
	 * 의안정보을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath1}/{paramPath2}/agendaList.do")
	public String scrListOfAgenda ( 
			@PathVariable String paramPath1, 
			@PathVariable String paramPath2, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					sndType      = paramPath1.toLowerCase();
		String					retPath      = "";

		int						listCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseAgendaVO>		dataList     = new ArrayList<BaseAgendaVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		eraList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		kndList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		rndList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		sugList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		resList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		comList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		tempList     = new ArrayList<BaseCommonVO>();
		BaseCommonVO			tempView     = new BaseCommonVO();
log.debug(" act: "+ BaseUtility.spaceFill( "Agenda"+ BaseUtility.toCapitalize( paramPath2 ) +"List", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "base".equalsIgnoreCase(paramPath1) ) { 
			searchVO.setReComCode("100");
		} else if( "oper".equalsIgnoreCase(paramPath1) ) { 
			searchVO.setReComCode("600");
		} else if( "gvadm".equalsIgnoreCase(paramPath1) ) { 
			searchVO.setReComCode("200");
		} else if( "welfare".equalsIgnoreCase(paramPath1) ) {
			searchVO.setReComCode("900"); // 코드확인필요
		} else if( "build".equalsIgnoreCase(paramPath1) ) { 
			searchVO.setReComCode("400");
		}
		if( "base".equalsIgnoreCase(paramPath2) && !"".equals(paramPath1) ) { 
			retPath  = "conf/agenda"+ BaseUtility.toCapitalize( paramPath2 ) +"_list";
		} else if( "recv".equalsIgnoreCase(paramPath2) && !"".equals(paramPath1) ) { 
			retPath  = "conf/agenda"+ BaseUtility.toCapitalize( paramPath2 ) +"_list";
		} else if( "judge".equalsIgnoreCase(paramPath2) && !"".equals(paramPath1) ) { 
			retPath  = "conf/agenda"+ BaseUtility.toCapitalize( paramPath2 ) +"_list";
		} else if( "review".equalsIgnoreCase(paramPath2) && !"".equals(paramPath1) ) { 
			retPath  = "conf/agenda"+ BaseUtility.toCapitalize( paramPath2 ) +"_list";
		} else if( "pend".equalsIgnoreCase(paramPath2) && !"".equals(paramPath1) ) { 
			retPath  = "conf/agenda"+ BaseUtility.toCapitalize( paramPath2 ) +"_list";
		}
		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		pageInfo.setTotalRecordCount(1);
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(10);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "N" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reEraCode"     , searchVO.getReEraCode());
		model.addAttribute("reKndCode"     , searchVO.getReKndCode());
		model.addAttribute("reRndCode"     , searchVO.getReRndCode());
		model.addAttribute("reRndDiv"      , searchVO.getReRndDiv());
		model.addAttribute("reSugCode"     , searchVO.getReSugCode());
		model.addAttribute("reResCode"     , searchVO.getReResCode());
		model.addAttribute("reComCode"     , searchVO.getReComCode());
		model.addAttribute("reMemID"       , searchVO.getReMemID());
		model.addAttribute("searchRegi"    , searchVO.getSearchRegi());
		model.addAttribute("searchText"    , searchVO.getSearchText());
		model.addAttribute("sndType"       , sndType);
		model.addAttribute("sndName"       , "gvadm".equals(sndType) ? "기획행정위원회" : ("build".equals(sndType) ? "산업건설위원회" : ("welfare".equals(sndType) ? "복지환경위원회" : "의회운영위원회")));

		// 추가 조회조건 취득
//		searchVO.setCondCate(BaseUtility.null2Char( searchVO.getCondCate(), "%" ));
//		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReBgnDate(BaseUtility.null2Blank( searchVO.getReBgnDate() ).replaceAll("[^0-9]", ""));
		searchVO.setReEndDate(BaseUtility.null2Blank( searchVO.getReEndDate() ).replaceAll("[^0-9]", ""));
		searchVO.setReLstType(paramPath2.toUpperCase());

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("bgnDate: "+ searchVO.getReBgnDate() +" |endDate: "+ searchVO.getReEndDate());
log.debug("eraCode: "+ searchVO.getReEraCode() +" |kndCode: "+ searchVO.getReKndCode() +" |rndCode: "+ searchVO.getReRndCode() +" |rndDiv: "+ searchVO.getReRndDiv());
log.debug("sugCode: "+ searchVO.getReSugCode() +" |resCode: "+ searchVO.getReResCode() +" |comCode: "+ searchVO.getReComCode());
log.debug("memID: "+ searchVO.getReMemID() +" |searchRegi: "+ searchVO.getSearchRegi() +" |searchText: "+ searchVO.getSearchText());
		try { 
			if( BaseUtility.isValidNull( searchVO.getReEraCode() ) ) { 
				// 분류구분 취득 - 현재 회의대수 구분
				tempView = (BaseCommonVO)baseCmmnService.getInfoOfClassNOW( "" );
				searchVO.setReEraCode(tempView == null ? BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.DefsCate" ) : tempView.getEraCode());
				model.addAttribute("reEraCode"     , searchVO.getReEraCode());
log.debug("new eraCode: "+ searchVO.getReEraCode());
			}
			listCnt = (int)agendaMngrService.getCountOfAgendaFind( searchVO );

			// 조회 건수를 전체선택인 경우
			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
			}
			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
			searchVO.setPageFirNo(Integer.toString(idxFir));
			searchVO.setPageLstNo(Integer.toString(idxLst));
			searchVO.setListCnt(Integer.toString(listCnt));

			if( listCnt > 0 ) { 
				dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfAgendaFind( searchVO );
//				dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfAgendaFind( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				// Data Adjustment(Agendas)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					for( int d = 0; d < dataList.size(); d++ ) { 
						dataList.get(d).setViewNo(dataList.get(d).getSugUUID() + dataList.get(d).getRowSeq());

						dataList.get(d).setSugDate(BaseUtility.toDateFormat( dataList.get(d).getSugDate(), dtSep ));
						dataList.get(d).setGenVote(BaseUtility.toDateFormat( dataList.get(d).getGenVote(), dtSep ));
						// 발의자 처리
						if( dataList.get(d).getSugMan().split("[,]").length <= 1 ) { 
							dataList.get(d).setSugWork(null);
						} else { 
							dataList.get(d).setSugWork(dataList.get(d).getSugMan());
							dataList.get(d).setSugMan(dataList.get(d).getSugWork().split("[,]")[0] +" 등 "+ (dataList.get(d).getSugWork().split("[,]").length) +"명");
						}
					}
				}

				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
				pageInfo.setTotalRecordCount(listCnt);
			}

			// 분류구분 취득 - 전체 회의대수 구분
			eraList = (List<BaseCommonVO>)baseCmmnService.getListOfClassALL( "" );
			// 분류구분 취득 - 회의회수 구분
			rndList = (List<BaseCommonVO>)baseCmmnService.getListOfRoundALL( searchVO.getReEraCode() );
			// 분류구분 취득 - 희의분류 구분
			kndList = (List<BaseCommonVO>)baseCmmnService.getListOfGroup( "" );
			// 분류구분 취득 - 의안종류 구분
			sugList = (List<BaseCommonVO>)baseCmmnService.getListOfSugKind( "" );
			// 분류구분 취득 - 의안결과 구분
			resList = (List<BaseCommonVO>)baseCmmnService.getListOfResKind( "" );
			// 분류구분 취득 - 소관위원회 구분
			comList = (List<BaseCommonVO>)baseCmmnService.getListOfComKind( "" );

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			model.addAttribute("dataList"      , dataList);
			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (dataList == null ? 0 : dataList.size()) : listCnt ));
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("eraList"       , eraList);
			model.addAttribute("kndList"       , kndList);
			model.addAttribute("rndList"       , rndList);
			model.addAttribute("sugList"       , sugList);
			model.addAttribute("resList"       , resList);
			model.addAttribute("comList"       , comList);
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/stand/"+retPath;
	}	// end of scrListOfAgenda

	/**
	 * <pre>
	 * 의안정보을(를) 상세조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath1}/{paramPath2}/agendaView.do")
	public String scrViewOfAgenda ( 
			@PathVariable String paramPath1, 
			@PathVariable String paramPath2, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					sndType      = paramPath1.toLowerCase();
		String					retPath      = "";

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		BaseAgendaVO			dataView     = new BaseAgendaVO();
		BaseAgendaVO			prevView     = new BaseAgendaVO();
		BaseAgendaVO			nextView     = new BaseAgendaVO();
		BaseCommonVO			findFile     = new BaseCommonVO();
		List<BaseAgendaVO>		dataFile     = new ArrayList<BaseAgendaVO>();
		List<BaseAgendaVO>		thmbFile     = new ArrayList<BaseAgendaVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
log.debug(" act: "+ BaseUtility.spaceFill( "Agenda"+ BaseUtility.toCapitalize( paramPath2 ) +"View", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "base".equalsIgnoreCase(paramPath1) ) { 
			searchVO.setReComCode("100");
		} else if( "oper".equalsIgnoreCase(paramPath1) ) { 
			searchVO.setReComCode("600");
		} else if( "gvadm".equalsIgnoreCase(paramPath1) ) { 
			searchVO.setReComCode("200");
		} else if( "welfare".equalsIgnoreCase(paramPath1) ) {
			searchVO.setReComCode("900"); // 코드확인필요
		} else if( "build".equalsIgnoreCase(paramPath1) ) { 
			searchVO.setReComCode("400");
		}
		if( "base".equalsIgnoreCase(paramPath2) && !"".equals(paramPath1) ) { 
			retPath  = "conf/agenda"+ BaseUtility.toCapitalize( paramPath2 ) +"_view";
		} else if( "recv".equalsIgnoreCase(paramPath2) && !"".equals(paramPath1) ) { 
			retPath  = "conf/agenda"+ BaseUtility.toCapitalize( paramPath2 ) +"_view";
		} else if( "judge".equalsIgnoreCase(paramPath2) && !"".equals(paramPath1) ) { 
			retPath  = "conf/agenda"+ BaseUtility.toCapitalize( paramPath2 ) +"_view";
		} else if( "review".equalsIgnoreCase(paramPath2) && !"".equals(paramPath1) ) { 
			retPath  = "conf/agenda"+ BaseUtility.toCapitalize( paramPath2 ) +"_view";
		} else if( "pend".equalsIgnoreCase(paramPath2) && !"".equals(paramPath1) ) { 
			retPath  = "conf/agenda"+ BaseUtility.toCapitalize( paramPath2 ) +"_view";
		}
		if( "".equals(retPath) || BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reEraCode"     , searchVO.getReEraCode());
		model.addAttribute("reKndCode"     , searchVO.getReKndCode());
		model.addAttribute("reRndCode"     , searchVO.getReRndCode());
		model.addAttribute("reRndDiv"      , searchVO.getReRndDiv());
		model.addAttribute("reSugCode"     , searchVO.getReSugCode());
		model.addAttribute("reResCode"     , searchVO.getReResCode());
		model.addAttribute("reComCode"     , searchVO.getReComCode());
		model.addAttribute("reMemID"       , searchVO.getReMemID());
		model.addAttribute("searchRegi"    , searchVO.getSearchRegi());
		model.addAttribute("searchText"    , searchVO.getSearchText());
		model.addAttribute("sndType"       , sndType);
		model.addAttribute("sndName"       , "gvadm".equals(sndType) ? "기획행정위원회" : ("build".equals(sndType) ? "산업건설위원회" : ("welfare".equals(sndType) ? "복지환경위원회" : "의회운영위원회")));

		// 추가 조회조건 취득
//		searchVO.setCondCate(BaseUtility.null2Char( searchVO.getCondCate(), "%" ));
//		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReBgnDate(BaseUtility.null2Blank( searchVO.getReBgnDate() ).replaceAll("[^0-9]", ""));
		searchVO.setReEndDate(BaseUtility.null2Blank( searchVO.getReEndDate() ).replaceAll("[^0-9]", ""));
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			if( !"".equals(searchVO.getViewNo()) ) { 
				dataView = (BaseAgendaVO)agendaMngrService.getDetailOfAgendaFind( searchVO );
				if( dataView != null ) { 
					// Data Adjustment(Data Files)
					dataFile = (List<BaseAgendaVO>)agendaMngrService.getListOfAgendaFile( searchVO );

					// Data Adjustment(Agendas)
					dataView.setActMode("Update");
					dataView.setViewNo(dataView.getSugUUID() + dataView.getRowSeq());

					dataView.setSugDate(BaseUtility.toDateFormat( dataView.getSugDate(), dtSep ));
					dataView.setComCon(BaseUtility.toDateFormat( dataView.getComCon(), dtSep ));
					dataView.setComRep(BaseUtility.toDateFormat( dataView.getComRep(), dtSep ));
					dataView.setComIntro(BaseUtility.toDateFormat( dataView.getComIntro(), dtSep ));
					dataView.setComVote(BaseUtility.toDateFormat( dataView.getComVote(), dtSep ));
					dataView.setGenCon(BaseUtility.toDateFormat( dataView.getGenCon(), dtSep ));
					dataView.setGenRep(BaseUtility.toDateFormat( dataView.getGenRep(), dtSep ));
					dataView.setGenIntro(BaseUtility.toDateFormat( dataView.getGenIntro(), dtSep ));
					dataView.setGenVote(BaseUtility.toDateFormat( dataView.getGenVote(), dtSep ));
					dataView.setTranDate(BaseUtility.toDateFormat( dataView.getTranDate(), dtSep ));
					dataView.setPublDate(BaseUtility.toDateFormat( dataView.getPublDate(), dtSep ));

					dataView.setContentsBR(BaseUtility.tag2Sym( dataView.getContents() ));
					dataView.setContentsCR(BaseUtility.sym2Tag( dataView.getContents() ));
					dataView.setFileLinkS(FileProcess.getInfoOfFileAgenda( request, response, dataFile, "S" ));
					dataView.setFileLinkM(FileProcess.getInfoOfFileAgenda( request, response, dataFile, "M" ));
					// 발의자 처리
					if( dataView.getSugMan().split("[,]").length <= 1 ) { 
						dataView.setSugWork(null);
					} else { 
						dataView.setSugWork(dataView.getSugMan());
						dataView.setSugMan(dataView.getSugWork().split("[,]")[0] +" 등 "+ (dataView.getSugWork().split("[,]").length) +"명");
					}
				}

				// Data Adjustment(Move Items)
				if( moveItems && !BaseUtility.isValidNull( searchVO.getRowSeq() ) ) { 
					searchVO.setRowSeq(String.valueOf(Integer.parseInt(searchVO.getRowSeq()) + 1));
					prevView = (BaseAgendaVO)agendaMngrService.getMovingOfAgendaFind( searchVO );
					if( prevView != null ) { 
						prevView.setViewNo(prevView.getSugUUID() + prevView.getRowSeq());
						prevView.setSugDate(BaseUtility.toDateFormat( prevView.getSugDate(), dtSep ));
						prevView.setGenVote(BaseUtility.toDateFormat( prevView.getGenVote(), dtSep ));
					}
					searchVO.setRowSeq(String.valueOf(Integer.parseInt(searchVO.getRowSeq()) - 2));
					nextView = (BaseAgendaVO)agendaMngrService.getMovingOfAgendaFind( searchVO );
					if( nextView != null ) { 
						nextView.setViewNo(nextView.getSugUUID() + nextView.getRowSeq());
						nextView.setSugDate(BaseUtility.toDateFormat( nextView.getSugDate(), dtSep ));
						nextView.setGenVote(BaseUtility.toDateFormat( nextView.getGenVote(), dtSep ));
					}
				} else { 
					prevView = null;
					nextView = null;
				}
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("dataFile"      , dataFile);
			model.addAttribute("thmbFile"      , thmbFile);
			model.addAttribute("moveItem"      , moveItems?"T":"F");
			model.addAttribute("prevView"      , prevView);
			model.addAttribute("nextView"      , nextView);
			model.addAttribute("cateList"      , cateList);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/stand/"+retPath;
	}	// end of scrViewOfAgenda

}	// end of class