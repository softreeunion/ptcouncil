/*******************************************************************************
  Program ID  : MinutesStandController
  Description : 클라이언트의 요청(공통)을 처리할 메서드를 구현
  Author      : HexaMedia new0man
  Write Date  : 2019.06.04
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v1.0    (주)헥사미디어 개발자  2014.00.00  최초 생성
  v2.0    HexaMedia new0man      2019.06.04  평택시의회 Site Renewal
 *******************************************************************************/
package egovframework.ptcc.stand.content.web;

import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.util.UserDetailsHelper;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.agenda.service.*;

@Controller
public class MinutesStandController { 

	@Autowired
	private WebApplicationContext			webContext;
	@Autowired
	private DataSourceTransactionManager	txManager;

	@Resource(name="beanValidator")
	protected DefaultBeanValidator			beanValidator;
	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="agendaMngrService")
	protected AgendaMngrService			agendaMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));


	/**
	 * <pre>
	 * 회의록정보을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/{paramPath1}/{paramPath2}/minutesList.do")
	public String scrListOfMinutes ( 
			@PathVariable String paramPath1, 
			@PathVariable String paramPath2, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					sndType      = paramPath1.toLowerCase();
		String					retPath      = "";

		int						listCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseAgendaVO>		dataList     = new ArrayList<BaseAgendaVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		eraList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		kndList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		rndList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		pntList      = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		memList      = new ArrayList<BaseCommonVO>();
log.debug(" act: "+ BaseUtility.spaceFill( "Minutes"+ BaseUtility.toCapitalize( paramPath2 ) +"List", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "base".equalsIgnoreCase(paramPath1) ) { 
		} else if( "oper".equalsIgnoreCase(paramPath1) ) { 
		} else if( "gvadm".equalsIgnoreCase(paramPath1) ) {
		} else if( "welfare".equalsIgnoreCase(paramPath1) ) {
		} else if( "build".equalsIgnoreCase(paramPath1) ) { 
		}
		if( "base".equalsIgnoreCase(paramPath2) && !"".equals(paramPath1) ) { 
			retPath  = "conf/minutes"+ BaseUtility.toCapitalize( paramPath2 ) +"_list";
		} else if( "board".equalsIgnoreCase(paramPath2) && !"".equals(paramPath1) ) { 
			retPath  = "conf/minutes"+ BaseUtility.toCapitalize( paramPath2 ) +"_list";
		} else if( "movie".equalsIgnoreCase(paramPath2) && !"".equals(paramPath1) ) { 
			retPath  = "conf/minutes"+ BaseUtility.toCapitalize( paramPath2 ) +"_list";
		}
		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		pageInfo.setTotalRecordCount(1);
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(10);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "N" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("reBgnDate"     , searchVO.getReBgnDate());
		model.addAttribute("reEndDate"     , searchVO.getReEndDate());
		model.addAttribute("reConfType"    , searchVO.getReConfType());
		model.addAttribute("reEraCode"     , searchVO.getReEraCode());
		model.addAttribute("reKndCode"     , searchVO.getReKndCode());
		model.addAttribute("reRndCode"     , searchVO.getReRndCode());
		model.addAttribute("reRndDiv"      , searchVO.getReRndDiv());
		model.addAttribute("reMemID"       , searchVO.getReMemID());
		model.addAttribute("searchText"    , searchVO.getSearchText());
		model.addAttribute("sndType"       , sndType);
		model.addAttribute("sndName"       , "gvadm".equals(sndType) ? "기획행정위원회" : ("build".equals(sndType) ? "산업건설위원회" : ("welfare".equals(sndType) ? "복지환경위원회" : "의회운영위원회")));

		// 추가 조회조건 취득
//		searchVO.setCondCate(BaseUtility.null2Char( searchVO.getCondCate(), "%" ));
//		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReBgnDate(BaseUtility.null2Blank( searchVO.getReBgnDate() ).replaceAll("[^0-9]", ""));
		searchVO.setReEndDate(BaseUtility.null2Blank( searchVO.getReEndDate() ).replaceAll("[^0-9]", ""));
		searchVO.setReKndType(sndType.toUpperCase());

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("bgnDate: "+ searchVO.getReBgnDate() +" |endDate: "+ searchVO.getReEndDate());
log.debug("eraCode: "+ searchVO.getReEraCode() +" |kndCode: "+ searchVO.getReKndCode() +" |rndCode: "+ searchVO.getReRndCode() +" |rndDiv: "+ searchVO.getReRndDiv());
log.debug("confType: "+ searchVO.getReConfType() +" |memID: "+ searchVO.getReMemID() +" |searchText: "+ searchVO.getSearchText());
log.debug("kndType: "+ searchVO.getReKndType());
		try { 
			listCnt = (int)agendaMngrService.getCountOfConfBase( searchVO );

			// 조회 건수를 전체선택인 경우
			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
			}
			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
			searchVO.setPageFirNo(Integer.toString(idxFir));
			searchVO.setPageLstNo(Integer.toString(idxLst));
			searchVO.setListCnt(Integer.toString(listCnt));

			if( listCnt > 0 ) { 
				dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfBase( searchVO );
//				dataList = (List<BaseAgendaVO>)agendaMngrService.getListOfConfBase( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				// Data Adjustment(Minutes)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					for( int d = 0; d < dataList.size(); d++ ) { 
						dataList.get(d).setViewNo(dataList.get(d).getConfCode());

						dataList.get(d).setConfDate(BaseUtility.toDateFormat( dataList.get(d).getConfDate(), dtSep ));
						dataList.get(d).setConfStartTime(BaseUtility.toTimeFormat( dataList.get(d).getConfStartTime() ));
						dataList.get(d).setConfCloseTime(BaseUtility.toTimeFormat( dataList.get(d).getConfCloseTime() ));
						dataList.get(d).setConfReqTime(BaseUtility.toTimeFormat( dataList.get(d).getConfReqTime() ));
						dataList.get(d).setConfWeek(BaseUtility.date2Week( dataList.get(d).getConfDate() ));
					}
				}

				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
				pageInfo.setTotalRecordCount(listCnt);
			}

			// 분류구분 취득 - 전체 회의대수 구분
			eraList = (List<BaseCommonVO>)baseCmmnService.getListOfClassALL( "" );

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			model.addAttribute("dataList"      , dataList);
			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (dataList == null ? 0 : dataList.size()) : listCnt ));
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("eraList"       , eraList);
			model.addAttribute("kndList"       , kndList);
			model.addAttribute("rndList"       , rndList);
			model.addAttribute("pntList"       , pntList);
			model.addAttribute("memList"       , memList);
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/stand/"+retPath;
	}	// end of scrListOfMinutes

}	// end of class