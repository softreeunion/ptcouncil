/*******************************************************************************
  Program ID  : ReschdStandController
  Description : 클라이언트의 요청(공통)을 처리할 메서드를 구현
  Author      : HexaMedia new0man
  Write Date  : 2019.10.01
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v0.1    HexaMedia new0man      2019.10.01  최초 생성
  v1.0    HexaMedia new0man      2019.10.01  평택시의회 Site Renewal
  v1.1    HexaMedia new0man      2020.04.01  일정관리에서 일요일 및 토요일 포함여부
 *******************************************************************************/
package egovframework.ptcc.stand.content.web;

import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.util.*;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.board.service.*;

@Controller
public class ReschdStandController { 

	@Autowired
	private WebApplicationContext			webContext;
	@Autowired
	private DataSourceTransactionManager	txManager;

	@Resource(name="beanValidator")
	protected DefaultBeanValidator			beanValidator;
	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="reschdMngrService")
	protected ReschdMngrService			reschdMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));
	protected boolean						notiItems    = false;
	protected boolean						moveItems    = false;
	protected boolean						fileUP       = false;
	protected boolean						tempUP       = false;


	private String getSchdTypeName ( 
			String argType, 
			String argValue 
		) throws Exception { 

		String					retS         = "";

		try { 
			if( BaseUtility.isValidNull( argValue ) ) { 
				retS = "";
			} else if( "CL".equals(argValue) ) { 
				retS = "S".equals(argType) ? "생방(본)"   : ("N".equals(argType) ? "본회의"   : "생 방 송 (본회의)");
			} else if( "OL".equals(argValue) ) { 
				retS = "S".equals(argType) ? "생방(운영)" : ("N".equals(argType) ? "운영위"   : "생 방 송 (의회운영위원회)");
			} else if( "GL".equals(argValue) ) { 
//				retS = "S".equals(argType) ? "생방(자치)" : ("N".equals(argType) ? "자치위"   : "생 방 송 (자치행정위원회)");
				retS = "S".equals(argType) ? "생방(기획)" : ("N".equals(argType) ? "기획위"   : "생 방 송 (기획행정위원회)");
			} else if( "WL".equals(argValue) ) {
				retS = "S".equals(argType) ? "생방(복지)" : ("N".equals(argType) ? "복지위"   : "생 방 송 (복지환경위원회)");
			} else if( "BL".equals(argValue) ) { 
				retS = "S".equals(argType) ? "생방(산건)" : ("N".equals(argType) ? "산건위"   : "생 방 송 (산업건설위원회)");
			} else if( "CS".equals(argValue) ) { 
				retS = "S".equals(argType) ? "의사(시)"   : ("N".equals(argType) ? "시의회"   : "의사일정 (시의회)");
			} else if( "OS".equals(argValue) ) { 
				retS = "S".equals(argType) ? "의사(운영)" : ("N".equals(argType) ? "운영위"   : "의사일정 (의회운영위원회)");
			} else if( "GS".equals(argValue) ) { 
//				retS = "S".equals(argType) ? "의사(자치)" : ("N".equals(argType) ? "자치위"   : "의사일정 (자치행정위원회)");
				retS = "S".equals(argType) ? "의사(기획)" : ("N".equals(argType) ? "기획위"   : "의사일정 (기획행정위원회)");
			} else if( "BS".equals(argValue) ) { 
				retS = "S".equals(argType) ? "의사(산건)" : ("N".equals(argType) ? "산건위"   : "의사일정 (산업건설위원회)");
			} else if( "CW".equals(argValue) ) { 
				retS = "S".equals(argType) ? "방청"       : ("N".equals(argType) ? "방청견학" : "방청견학");
			}
		} catch( Exception E ) { 
			retS = "";
		}	// end of getSchdTypeName

		return retS;
	}

	/**
	 * <pre>
	 * 의사일정을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/{paramPath1}/schd/monthList.do")
	public String scrListOfSchdMonth ( 
			@PathVariable String paramPath1, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					sndType      = paramPath1.toLowerCase();
		String					retPath      = "acts/monthSchd_list";

		int						listCnt      = 0;
		int						chldCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseReschdVO>		schdList     = new ArrayList<BaseReschdVO>();
		List<BaseReschdVO>		schdChld     = new ArrayList<BaseReschdVO>();
		String[]				schdType     = new String[12];
		StringBuffer			schdDocu     = new StringBuffer();
log.debug(" act: "+ BaseUtility.spaceFill( "MonthSchdListList", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		pageInfo.setTotalRecordCount(1);
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(10);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
//		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), propertiesService.getString("pageUnit") ));
		searchVO.setPagePerNo("all");
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "N" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());
		model.addAttribute("sndType"       , sndType);
		model.addAttribute("sndName"       , "gvadm".equals(sndType) ? "기획행정위원회" : ("build".equals(sndType) ? "산업건설위원회" : ("welfare".equals(sndType) ? "복지환경위원회" : "의회운영위원회")));

		// 추가 조회조건 취득
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setReStdStat(BaseUtility.null2Char( searchVO.getReStdStat(), "C" ));
		searchVO.setReStdDate(BaseUtility.null2Char( searchVO.getReStdDate(), systemVO.getToYear() + systemVO.getToMonth() ).replaceAll("[^0-9]", ""));
		searchVO.setReStdDate("PY".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(), -12 ) : searchVO.getReStdDate());
		searchVO.setReStdDate("PM".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(),  -1 ) : searchVO.getReStdDate());
		searchVO.setReStdDate("NM".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(),   1 ) : searchVO.getReStdDate());
		searchVO.setReStdDate("NY".equals(searchVO.getReStdStat()) ? BaseUtility.getDateOfAdder( searchVO.getReStdDate(),  12 ) : searchVO.getReStdDate());
		searchVO.setReBgnDate(BaseUtility.getWeekInMonths( BaseUtility.getDateOfLast( searchVO.getReStdDate(), "F" ), "MF" ));
		searchVO.setReEndDate(BaseUtility.getWeekInMonths( BaseUtility.getDateOfLast( searchVO.getReStdDate(), "L" ), "ML" ));
		searchVO.setReLstType("gvadm".equals(sndType) ? "GS" : ("build".equals(sndType) ? "BS" : ("welfare".equals(sndType) ? "WS" : "OS"))); // 확인필요

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("stdDate: "+ searchVO.getReStdDate() +" |stdStat: "+ searchVO.getReStdStat());
log.debug("bgnDate: "+ searchVO.getReBgnDate() +" |endDate: "+ searchVO.getReEndDate() +" |lstType: "+ searchVO.getReLstType());
		try { 
			listCnt = (int)reschdMngrService.getCountOfReschdCal( searchVO );

			// 조회 건수를 전체선택인 경우
			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
			}
			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
			searchVO.setPageFirNo(Integer.toString(idxFir));
			searchVO.setPageLstNo(Integer.toString(idxLst));
			searchVO.setListCnt(Integer.toString(listCnt));

			if( listCnt > 0 ) { 
				schdList = (List<BaseReschdVO>)reschdMngrService.getListOfReschdCal( searchVO );
//				schdList = (List<BaseReschdVO>)reschdMngrService.getListOfReschdCal( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				// Data Adjustment(Reschds)
				if( !(schdList == null || schdList.size() <= 0) ) { 
					schdDocu.setLength(0);
					for( int d = 0; d < schdList.size(); d++ ) { 
						schdList.get(d).setViewNo(schdList.get(d).getStdUUID() + schdList.get(d).getRowSeq());

//						schdList.get(d).setStdDate(BaseUtility.toDateFormat( schdList.get(d).getStdDate(), dtSep, 8 ));

						if( d % 7 == 0 ) schdDocu.append("<tr>");
						schdDocu.append("\n\t\t\t\t\t\t\t").append("<td class='day'"+ (schdList.get(d).getStdDate().equals(systemVO.getToDate())?" style='background-color:#83d3ca;'":"") +">");
						schdDocu.append("\n\t\t\t\t\t\t\t\t").append("<div class='date"+ (d%7==0?" sun":(d%7==6?" sat":"")) +"'><span class='d_number'");
						schdDocu.append(schdList.get(d).getStdDate().substring(0,6).equals(searchVO.getReStdDate())?"":" style='opacity:0.3;'");
						schdDocu.append(">"+ Integer.parseInt(schdList.get(d).getStdDay()) +"</span></div>");
						schdDocu.append("\n\t\t\t\t\t\t\t\t").append("<div class='day-content'>");
						schdChld = (List<BaseReschdVO>)schdList.get(d).getChildItem();
						chldCnt  = schdChld == null || schdChld.size() <= 0 ? 0 : schdChld.size();
						if( chldCnt <= 0 || ("F".equals(propertiesService.getString("viewSun")) && BaseUtility.dayOfWeek( schdList.get(d).getStdDate() ) == 1) || ("F".equals(propertiesService.getString("viewSat")) && BaseUtility.dayOfWeek( schdList.get(d).getStdDate() ) == 7) ) { 
						} else {
							schdDocu.append("<ul class='p_icon'><li>");
							for( int c = 0; c < chldCnt; c++ ) { 
								schdType = BaseUtility.null2Blank( schdChld.get(c).getSchdType() ).split("[`]", schdType.length);

//								log.debug(">>>>>>>>> here0 => " + schdChld.get(c).getSchdType());
//								log.debug(">>>>>>>>> here1 => " + schdType.length);
//								log.debug(">>>>>>>>> here2 => " + searchVO.getReLstType());

								for( int s = 0; s < schdType.length-1; s ++ ) if( searchVO.getReLstType().equals(schdType[s]) ) {
									schdDocu.append("\n\t\t\t\t\t\t\t\t\t").append("<p title='["+ getSchdTypeName( "N", schdType[s] ) +"] "+ schdChld.get(c).getSchdSTitle() +"'><a href='javascript:void(0);' onclick='fnActMonth(\""+ schdChld.get(c).getSchdUUID() +"\",\""+ schdList.get(d).getStdDate() +"\")'><span>");
									schdDocu.append(BaseUtility.toTimeFormat( schdChld.get(c).getSchdBTime() ) +"&nbsp; ");
									schdDocu.append(schdChld.get(c).getSchdSTitle()).append("</span></a></p>");
								}

							}
							schdDocu.append("\n\t\t\t\t\t\t\t\t").append("</li></ul>");
						}
						schdDocu.append("</div>").append("\n\t\t\t\t\t\t\t").append("</td>");
						if( d % 7 == 6 ) schdDocu.append("\n\t\t\t\t\t\t").append("</tr>");
					}
				}

				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
				pageInfo.setTotalRecordCount(listCnt);
			}

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
//			model.addAttribute("dataList"      , dataList);
//			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (dataList == null ? 0 : dataList.size()) : listCnt ));
//			model.addAttribute("pageInfo"      , pageInfo);
//			model.addAttribute("cateList"      , cateList);
			model.addAttribute("dispDocu"      , schdDocu.toString());
			model.addAttribute("dispDate"      , BaseUtility.toDateFormat( searchVO.getReStdDate(), dtSep, 6 ));
			model.addAttribute("reStdStat"     , searchVO.getReStdStat());
			model.addAttribute("reStdDate"     , BaseUtility.toDateFormat( searchVO.getReStdDate(), dtSep ));
			model.addAttribute("reCurrDate"    , BaseUtility.toDateFormat( systemVO.getToYear() + systemVO.getToMonth(), dtSep ));
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/stand/"+retPath;
	}	// end of scrListOfSchdMonth

}	// end of class