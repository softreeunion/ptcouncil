/*******************************************************************************
  Program ID  : MainYouthController
  Description : 클라이언트의 요청(공통)을 처리할 메서드를 구현
  Author      : HexaMedia new0man
  Write Date  : 2019.06.04
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v1.0    (주)헥사미디어 개발자  2014.00.00  최초 생성
  v2.0    HexaMedia new0man      2019.06.04  평택시의회 Site Renewal
  v1.1    HexaMedia new0man      2020.03.17  비공개 목록 표시 및 검색조건 추가
 *******************************************************************************/
package egovframework.ptcc.youth.main.web;

import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.util.FileProcess;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.board.service.*;

@Controller
@RequestMapping(value="/youth")
public class MainYouthController { 

	@Autowired
	private WebApplicationContext			webContext;

	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="boardMngrService")
	protected BoardMngrService				boardMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected String						hideMark     = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.HideMark" );
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));
	protected boolean						isGallery    = "Gallery.do".equalsIgnoreCase(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.ViewPage" ));
	protected String						replyImage   = "<img src='@@/images/icon/ico_reply.png' alt=''/>";


	/**
	 * <pre>
	 * 상임위 메인화면을(를) 조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/index.do")
	public String scrInitOfMain ( 
//			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();

		List<BaseBoardsVO>		mainImage    = new ArrayList<BaseBoardsVO>();
		List<BaseBoardsVO>		mainPhoto    = new ArrayList<BaseBoardsVO>();

		try { 
			// mainImage : 메인이미지 내역 취득
			searchVO = new BaseCommonVO();
			searchVO.setBbsType("10210030");
			searchVO.setFirDate(firDate);
			searchVO.setReCreHide("N");
			mainImage = (List<BaseBoardsVO>)boardMngrService.getListOfBoardImgs( searchVO );
			// Data Adjustment(Boards)
			if( !(mainImage == null || mainImage.size() <= 0) ) { 
				for( int d = 0; d < mainImage.size(); d++ ) { 
					mainImage.get(d).setViewNo(mainImage.get(d).getBbsUUID() + BaseUtility.null2Char( mainImage.get(d).getRowSeq(), String.valueOf(d+1) ));
					mainImage.get(d).setContLinkC(BaseUtility.getUriCheck( mainImage.get(d).getBbsSiteUri() ) ? "T" : "F");
					mainImage.get(d).setFileUUID(BaseUtility.null2Char(mainImage.get(d).getFileUUID(), "", "0"+ mainImage.get(d).getFileUUID() ));
					if( !mainImage.get(d).getCreUser().equals(mainImage.get(d).getCreName()) ) mainImage.get(d).setCreName(propertiesService.getString("creName"));
				}
			}

			// mainPhoto : 포토의정활동 내역 취득
			searchVO = new BaseCommonVO();
			if( isGallery ) { 
				searchVO.setBbsType("55");
				searchVO.setRowCnt("3");
				mainPhoto = (List<BaseBoardsVO>)boardMngrService.getListOfPhotoMain( searchVO );
			} else { 
				searchVO.setBbsType("10110030");
				searchVO.setFirDate(firDate);
				searchVO.setReCreHide("N");
				searchVO.setRowCnt("3");
				mainPhoto = (List<BaseBoardsVO>)boardMngrService.getListOfBoardMain( searchVO );
			}
			// Data Adjustment(Boards)
			if( !(mainPhoto == null || mainPhoto.size() <= 0) ) { 
				for( int d = 0; d < mainPhoto.size(); d++ ) { 
					mainPhoto.get(d).setViewNo(mainPhoto.get(d).getBbsUUID() + BaseUtility.null2Char( mainPhoto.get(d).getRowSeq(), String.valueOf(d+1) ));
					mainPhoto.get(d).setHitCount(BaseUtility.toNumberFormat( mainPhoto.get(d).getHitCount() ));
					mainPhoto.get(d).setCreDate(BaseUtility.toDateFormat( mainPhoto.get(d).getCreDate(), dtSep, 8 ));
					mainPhoto.get(d).setModDate(BaseUtility.toDateFormat( mainPhoto.get(d).getModDate(), dtSep, 8 ));
					mainPhoto.get(d).setDelDate(BaseUtility.toDateFormat( mainPhoto.get(d).getDelDate(), dtSep, 8 ));
					mainPhoto.get(d).setTakeDate(BaseUtility.toDateFormat( mainPhoto.get(d).getTakeDate(), dtSep, 8 ));

					mainPhoto.get(d).setBbsTitle(("Y".equals(mainPhoto.get(d).getCreHide())?hideMark:"") + mainPhoto.get(d).getBbsTitle());
					mainPhoto.get(d).setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( mainPhoto.get(d).getAnsPos() )) <= 0 ? mainPhoto.get(d).getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ mainPhoto.get(d).getBbsTitle());
					mainPhoto.get(d).setContLinkC(BaseUtility.getUriCheck( mainPhoto.get(d).getBbsSiteUri() ) ? "T" : "F");
					mainPhoto.get(d).setDownExt(FileProcess.getFileLowerExt( mainPhoto.get(d).getDownRel() ));
					mainPhoto.get(d).setFileUUID(BaseUtility.null2Char(mainPhoto.get(d).getFileUUID(), "", "2"+ mainPhoto.get(d).getFileUUID() ));
					mainPhoto.get(d).setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( mainPhoto.get(d).getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
					mainPhoto.get(d).setActMode("coun/photoView");
					if( !mainPhoto.get(d).getCreUser().equals(mainPhoto.get(d).getCreName()) ) mainPhoto.get(d).setCreName(propertiesService.getString("creName"));
				}
			}

			model.addAttribute("mainImage"     , mainImage);
			model.addAttribute("mainPhoto"     , mainPhoto);
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/youth/main";
	}	// end of scrInitOfMain

}	// end of class