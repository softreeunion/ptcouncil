/*******************************************************************************
  Program ID  : ReportYouthController
  Description : 클라이언트의 요청(공통)을 처리할 메서드를 구현
  Author      : HexaMedia new0man
  Write Date  : 2019.06.04
 -------------------------------------------------------------------------------
  Program History
 -------------------------------------------------------------------------------
  버전No  개발(운영)담당         수정일자    수정근거
  v1.0    (주)헥사미디어 개발자  2014.00.00  최초 생성
  v2.0    HexaMedia new0man      2019.06.04  평택시의회 Site Renewal
  v1.1    HexaMedia new0man      2020.03.17  비공개 목록 표시 및 검색조건 추가
 *******************************************************************************/
package egovframework.ptcc.youth.content.web;

import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cmm.util.*;
import egovframework.ptcc.cmmn.service.*;
import egovframework.ptcc.cmmn.util.*;
import egovframework.ptcc.cmmn.vo.*;
import egovframework.ptcc.mngr.board.service.*;

@Controller
@RequestMapping(value="/youth")
public class ReportYouthController { 

	@Autowired
	private WebApplicationContext			webContext;
	@Autowired
	private DataSourceTransactionManager	txManager;

	@Resource(name="beanValidator")
	protected DefaultBeanValidator			beanValidator;
	@Resource(name="propertiesService")
	protected EgovPropertyService			propertiesService;
	@Resource(name="baseCmmnService")
	protected BaseCmmnService				baseCmmnService;
	@Resource(name="boardMngrService")
	protected BoardMngrService				boardMngrService;

	protected Logger						log          = LoggerFactory.getLogger(this.getClass());
	protected String						dtSep        = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "DATE.SEPA" );
	protected String						firDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "OPEN.DATE" ).replaceAll("[^0-9]", "");
	protected String						pwdDate      = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "PASS.DATE" ).replaceAll("[^0-9]", "");
	protected String						hideMark     = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.HideMark" );
	protected int							keySize      = Integer.parseInt(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.KeySizes" ));
	protected boolean						isGallery    = "Gallery.do".equalsIgnoreCase(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.ViewPage" ));
	protected String						replyImage   = "<img src='@@/images/icon/ico_reply.png' alt=''/>";
	protected String						lnkName      = "PT_BOARD";
	protected String						bbsType      = "10150010";
	protected boolean						notiItems    = false;
	protected boolean						moveItems    = true;
	protected boolean						fileUP       = false;
	protected boolean						tempUP       = false;


	/**
	 * <pre>
	 * 의정활동을(를) 목록조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/coun/{paramPath}List.do")
	public String scrListOfActivity ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					retPath      = "";

		int						listCnt      = 0;
		int						idxFir       = 0;
		int						idxLst       = 0;
		PaginationInfo			pageInfo     = new PaginationInfo();
		List<BaseBoardsVO>		notiList     = new ArrayList<BaseBoardsVO>();
		List<BaseBoardsVO>		dataList     = new ArrayList<BaseBoardsVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		BaseBoardsVO			dataView     = new BaseBoardsVO();
log.debug(" act: "+ BaseUtility.spaceFill( BaseUtility.toCapitalize( paramPath ) +"CounList", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			retPath  = "acts/"+ paramPath.toLowerCase() +"Coun_list";
		} else if( "photo".equalsIgnoreCase(paramPath) ) { 
			if( isGallery ) { 
				bbsType  = "55";
			} else { 
				bbsType  = "10110030";
			}
			retPath  = "acts/"+ paramPath.toLowerCase() +"Coun_list";
		} else if( "movie".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110060";
			retPath  = "acts/"+ paramPath.toLowerCase() +"Coun_list";
		}
		if( "".equals(retPath) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		pageInfo.setCurrentPageNo(1);
		pageInfo.setRecordCountPerPage(1);
		pageInfo.setPageSize(propertiesService.getInt("pageSize"));
		pageInfo.setTotalRecordCount(1);
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), "photo".equalsIgnoreCase(paramPath) ? propertiesService.getString("imgsUnit") : propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		if( isGallery && "photo".equalsIgnoreCase(paramPath) ) searchVO.setSearchText(BaseUtility.null2Blank( searchVO.getCondValue() ));
		searchVO.setCondName("BASE");
		searchVO.setBbsType(BaseUtility.null2Char( searchVO.getBbsType(), bbsType ));
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));

log.debug("pageCurNo: "+ searchVO.getPageCurNo() +" |pagePerNo: "+ searchVO. getPagePerNo() +" |firDate: "+ searchVO.getFirDate() +" |creHide: "+ searchVO.getReCreHide());
log.debug("condName: "+ searchVO.getCondName() +" |condCate: "+ searchVO.getCondCate() +" |condType: "+ searchVO.getCondType() +" |condValue: "+ searchVO.getCondValue());
log.debug("bbsType: "+ searchVO.getBbsType());
		try { 
			if( isGallery && "photo".equalsIgnoreCase(paramPath) ) { 
				listCnt = (int)boardMngrService.getCountOfPhoto( searchVO );
			} else { 
				listCnt = (int)boardMngrService.getCountOfBoard( searchVO );
			}

			// 조회 건수를 전체선택인 경우
			if( "all".equalsIgnoreCase(searchVO.getPagePerNo()) ) { 
				searchVO.setPageCurNo("1");
				searchVO.setPagePerNo(listCnt <= 0 ? "1" : Integer.toString(listCnt));
			}
			idxFir  = ((Integer.parseInt(searchVO.getPageCurNo()) - 1) * Integer.parseInt(searchVO.getPagePerNo())) + 1;
			idxLst  = idxFir + Integer.parseInt(searchVO.getPagePerNo()) - 1;
			searchVO.setPageFirNo(Integer.toString(idxFir));
			searchVO.setPageLstNo(Integer.toString(idxLst));
			searchVO.setListCnt(Integer.toString(listCnt));

			if( listCnt > 0 ) { 
				if( isGallery && "photo".equalsIgnoreCase(paramPath) ) { 
					dataList = (List<BaseBoardsVO>)boardMngrService.getListOfPhoto( searchVO );
//					dataList = (List<BaseBoardsVO>)boardMngrService.getListOfPhoto( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				} else { 
					dataList = (List<BaseBoardsVO>)boardMngrService.getListOfBoard( searchVO );
//					dataList = (List<BaseBoardsVO>)boardMngrService.getListOfBoard( searchVO, searchVO.getPageCurNo(), searchVO.getPagePerNo() );
				}
				// Data Adjustment(Boards)
				if( !(dataList == null || dataList.size() <= 0) ) { 
					for( int d = 0; d < dataList.size(); d++ ) { 
						dataList.get(d).setViewNo(dataList.get(d).getBbsUUID() + dataList.get(d).getRowSeq());

						dataList.get(d).setBbsFiles(BaseUtility.toNumberFormat( dataList.get(d).getBbsFiles() ));
						dataList.get(d).setBgnDate(BaseUtility.toDateFormat( dataList.get(d).getBgnDate(), dtSep, 8 ));
						dataList.get(d).setEndDate(BaseUtility.toDateFormat( dataList.get(d).getEndDate(), dtSep, 8 ));
						dataList.get(d).setHitCount(BaseUtility.toNumberFormat( dataList.get(d).getHitCount() ));
						dataList.get(d).setCreDate(BaseUtility.toDateFormat( dataList.get(d).getCreDate(), dtSep, 8 ));
						dataList.get(d).setModDate(BaseUtility.toDateFormat( dataList.get(d).getModDate(), dtSep, 8 ));
						dataList.get(d).setDelDate(BaseUtility.toDateFormat( dataList.get(d).getDelDate(), dtSep, 8 ));
						dataList.get(d).setTakeDate(BaseUtility.toDateFormat( dataList.get(d).getTakeDate(), dtSep, 8 ));

						dataList.get(d).setBbsTitle(("Y".equals(dataList.get(d).getCreHide())?hideMark:"") + dataList.get(d).getBbsTitle());
						dataList.get(d).setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( dataList.get(d).getAnsPos() )) <= 0 ? dataList.get(d).getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ dataList.get(d).getBbsTitle());
						dataList.get(d).setContDescBR(BaseUtility.tag2Sym( dataList.get(d).getContDesc() ));
						dataList.get(d).setContDescCR(BaseUtility.sym2Tag( dataList.get(d).getContDesc() ));
						dataList.get(d).setContLinkC(BaseUtility.getUriCheck( dataList.get(d).getBbsSiteUri() ) ? "T" : "F");
						dataList.get(d).setCreLock(BaseUtility.null2Char( dataList.get(d).getCrePasswd(), "", "Y" ));
						dataList.get(d).setDownAbs(BaseUtility.null2Blank( dataList.get(d).getDownAbs() ).replace("/movie_","/movie/"));
						dataList.get(d).setDownExt(FileProcess.getFileLowerExt( dataList.get(d).getDownRel() ));
						dataList.get(d).setFileUUID(BaseUtility.null2Char(dataList.get(d).getFileUUID(), "", "2"+ dataList.get(d).getFileUUID() ));
						dataList.get(d).setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( dataList.get(d).getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
						dataList.get(d).setRegiDate(BaseUtility.toDateFormat( BaseUtility.null2Char( dataList.get(d).getRegiDate(), dataList.get(d).getCreDate() ), dtSep, 8 ));
						if( !dataList.get(d).getCreUser().equals(dataList.get(d).getCreName()) ) dataList.get(d).setCreName(propertiesService.getString("creName"));

						// 영상인경우만 처리
						if( "movie".equalsIgnoreCase(paramPath) ) { 
							if( BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
								if( d == 0 ) { 
									dataView = dataList.get(d);
									boardMngrService.doRtnOfModifyBoardHit( dataView );
									dataList.get(d).setHitCount(BaseUtility.toNumberFormat( Integer.parseInt(BaseUtility.null2Zero( dataList.get(d).getHitCount() )) + 1 ));
								}
							} else { 
								if( dataList.get(d).getViewNo().equals(searchVO.getViewNo()) ) { 
									dataView = dataList.get(d);
									boardMngrService.doRtnOfModifyBoardHit( dataView );
									dataList.get(d).setHitCount(BaseUtility.toNumberFormat( Integer.parseInt(BaseUtility.null2Zero( dataList.get(d).getHitCount() )) + 1 ));
								}
							}
						}
					}
				}

				pageInfo.setCurrentPageNo(Integer.parseInt(searchVO.getPageCurNo()));
				pageInfo.setRecordCountPerPage(Integer.parseInt(searchVO.getPagePerNo()));
				pageInfo.setTotalRecordCount(listCnt);
			}

log.debug("idxF: "+ BaseUtility.spaceFill( idxFir, 4, "R" ) +" |idxL: "+ BaseUtility.spaceFill( idxLst, 4, "R" ));
			model.addAttribute("dataList"      , dataList);
			model.addAttribute("listCnt"       , BaseUtility.toNumberFormat( listCnt <= 0 ? (dataList == null ? 0 : dataList.size()) : listCnt ));
			model.addAttribute("pageInfo"      , pageInfo);
			model.addAttribute("notiList"      , notiList);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("dataView"      , dataView);
			model.remove("searchVO");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/youth/"+retPath;
	}	// end of scrListOfActivity

	/**
	 * <pre>
	 * 의정활동을(를) 상세조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/coun/{paramPath}View.do")
	public String scrViewOfActivity ( 
			@PathVariable String paramPath, 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		String					retPath      = "";

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		int						chldIndx     = 0;
		BaseBoardsVO			dataView     = new BaseBoardsVO();
		BaseBoardsVO			prevView     = new BaseBoardsVO();
		BaseBoardsVO			nextView     = new BaseBoardsVO();
		BaseCommonVO			findFile     = new BaseCommonVO();
		List<BaseFilesVO>		dataFile     = new ArrayList<BaseFilesVO>();
		List<BaseFilesVO>		thmbFile     = new ArrayList<BaseFilesVO>();
		List<BaseCommonVO>		cateList     = new ArrayList<BaseCommonVO>();
		List<BaseCommonVO>		dataSupp     = new ArrayList<BaseCommonVO>();
		List<BaseBoardsVO>		chldFile     = new ArrayList<BaseBoardsVO>();
		BaseBoardsVO			tempSupp     = new BaseBoardsVO();
		BaseBoardsVO			tempView     = new BaseBoardsVO();
		String[][]				tempList     = new String[4][10];
log.debug(" act: "+ BaseUtility.spaceFill( BaseUtility.toCapitalize( paramPath ) +"CounView", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( "base".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10150010";
			retPath  = "acts/"+ paramPath.toLowerCase() +"Coun_view";
		} else if( "photo".equalsIgnoreCase(paramPath) ) { 
			if( isGallery ) { 
				bbsType  = "55";
			} else { 
				bbsType  = "10110030";
			}
			retPath  = "acts/"+ paramPath.toLowerCase() +"Coun_view";
		} else if( "movie".equalsIgnoreCase(paramPath) ) { 
			bbsType  = "10110060";
			retPath  = "acts/"+ paramPath.toLowerCase() +"Coun_view";
		}
		if( "".equals(retPath) || BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 기본 조회조건 취득
		searchVO.setFirDate(BaseUtility.null2Char( searchVO.getFirDate(), firDate ).replaceAll("[^0-9]", ""));
		searchVO.setPageCurNo(BaseUtility.null2Char( searchVO.getPageCurNo(), "1" ));
		searchVO.setPagePerNo(BaseUtility.null2Char( searchVO.getPagePerNo(), "photo".equalsIgnoreCase(paramPath) ? propertiesService.getString("imgsUnit") : propertiesService.getString("pageUnit") ));
		searchVO.setCondType(BaseUtility.null2Char( searchVO.getCondType(), "A" ));

		// 기본 조회조건 설정
		model.addAttribute("viewNo"        , searchVO.getViewNo());
		model.addAttribute("pageCurNo"     , searchVO.getPageCurNo());
		model.addAttribute("pagePerNo"     , searchVO.getPagePerNo());
		model.addAttribute("condCate"      , searchVO.getCondCate());
		model.addAttribute("condType"      , searchVO.getCondType());
		model.addAttribute("condValue"     , searchVO.getCondValue());

		// 추가 조회조건 취득
		if( isGallery && "photo".equalsIgnoreCase(paramPath) ) searchVO.setSearchText(BaseUtility.null2Blank( searchVO.getCondValue() ));
		searchVO.setCondName("BASE");
		searchVO.setBbsType(BaseUtility.null2Char( searchVO.getBbsType(), bbsType ));
		searchVO.setCondValue(BaseUtility.null2Char( searchVO.getCondValue(), "%", "%"+ searchVO.getCondValue() +"%" ));
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			if( !"".equals(searchVO.getViewNo()) ) { 
				if( isGallery && "photo".equalsIgnoreCase(paramPath) ) { 
					viewNo = BaseUtility.null2Char( (String)boardMngrService.getInfoOfPhotoUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);
				} else { 
					viewNo = BaseUtility.null2Char( (String)boardMngrService.getInfoOfBoardUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);
				}

				// 입력내역 취득(게시판)
				searchVO.setBbsType(viewNo[0]);
				searchVO.setBbsSeq(viewNo[1]);
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());

				if( isGallery && "photo".equalsIgnoreCase(paramPath) ) { 
					dataView = (BaseBoardsVO)boardMngrService.getDetailOfPhoto( searchVO );
				} else { 
					dataView = (BaseBoardsVO)boardMngrService.getDetailOfBoard( searchVO );
				}
				if( dataView != null ) { 
					if( isGallery && "photo".equalsIgnoreCase(paramPath) ) { 
						boardMngrService.doRtnOfModifyPhotoHit( dataView );
					} else { 
						boardMngrService.doRtnOfModifyBoardHit( dataView );

						findFile.setLnkName(lnkName);
						findFile.setLnkKey(viewNo[0] + viewNo[1]);
						// Data Adjustment(Data Files)
						dataFile = (List<BaseFilesVO>)baseCmmnService.getListOfDataFile( findFile );
						// Data Adjustment(Thumb Files)
						thmbFile = (List<BaseFilesVO>)baseCmmnService.getListOfThmbFile( findFile );
						if( !(thmbFile == null || thmbFile.size() <= 0) ) { 
							for( int f = 0; f < thmbFile.size(); f++ ) { 
								thmbFile.get(f).setFileImage(BaseUtility.getFileAllowOfImage( thmbFile.get(f).getFileAbs() ) ? "T" : null);
							}
						}
					}

					// Data Adjustment(Boards)
					dataView.setActMode("Update");
					dataView.setViewNo(dataView.getBbsUUID());

					dataView.setBbsFiles(BaseUtility.toNumberFormat( dataView.getBbsFiles() ));
					dataView.setBgnDate(BaseUtility.toDateFormat( dataView.getBgnDate(), dtSep, 8 ));
					dataView.setEndDate(BaseUtility.toDateFormat( dataView.getEndDate(), dtSep, 8 ));
					dataView.setHitCount(BaseUtility.toNumberFormat( Integer.parseInt(BaseUtility.null2Zero( dataView.getHitCount() )) + 1 ));
					dataView.setCreDate(BaseUtility.toDateFormat( dataView.getCreDate(), dtSep, 8 ));
					dataView.setModDate(BaseUtility.toDateFormat( dataView.getModDate(), dtSep, 8 ));
					dataView.setDelDate(BaseUtility.toDateFormat( dataView.getDelDate(), dtSep, 8 ));
					dataView.setTakeDate(BaseUtility.toDateFormat( dataView.getTakeDate(), dtSep, 8 ));

					dataView.setBbsTitle(("Y".equals(dataView.getCreHide())?hideMark:"") + dataView.getBbsTitle());
					dataView.setContDescBR(BaseUtility.tag2Sym( dataView.getContDesc() ));
					dataView.setContDescCR(BaseUtility.sym2Tag( dataView.getContDesc() ));
					dataView.setContLinkC(BaseUtility.getUriCheck( dataView.getBbsSiteUri() ) ? "T" : "F");
					dataView.setCreLock(BaseUtility.null2Char( dataView.getCrePasswd(), "", "Y" ));
					dataView.setDownAbs(BaseUtility.null2Blank( dataView.getDownAbs() ).replace("/movie_","/movie/"));
					dataView.setDownExt(FileProcess.getFileLowerExt( dataView.getDownRel() ));
					dataView.setFileUUID(BaseUtility.null2Char(dataView.getFileUUID(), "", "6"+ dataView.getFileUUID() ));
					dataView.setFileLinkS(FileProcess.getInfoOfFileBBS( request, response, dataFile, "S" ));
					dataView.setFileLinkM(FileProcess.getInfoOfFileBBS( request, response, dataFile, "M" ));
					dataView.setRegiDate(BaseUtility.toDateFormat( BaseUtility.null2Char( dataView.getRegiDate(), dataView.getCreDate() ), dtSep, 8 ));
					if( !dataView.getCreUser().equals(dataView.getCreName()) ) dataView.setCreName(propertiesService.getString("creName"));

					// 추가게시판 내역
					if( isGallery && "photo".equalsIgnoreCase(paramPath) ) { 
						chldFile = (List<BaseBoardsVO>)boardMngrService.getChldOfPhoto( searchVO );
						if( !(chldFile == null || chldFile.size() <= 0) ) for( int f = 0; f < chldFile.size(); f++ ) if( dataView.getBbsUUID().equals(chldFile.get(f).getBbsUUID()) ) chldIndx = Integer.parseInt(BaseUtility.null2Zero( chldFile.get(f).getRowSeq() ))-1;
					} else { 
						tempSupp = (BaseBoardsVO)boardMngrService.getDetailOfAdder( searchVO );
						if( tempSupp != null ) { 
							dataView.setConfCode1(tempSupp.getConfCode1());
							dataView.setConfName1((String)baseCmmnService.getInfoOfConfName( tempSupp.getConfCode1() ));
							dataView.setRegiDate1(BaseUtility.toDateFormat( tempSupp.getRegDate1(), dtSep, 8 ));

							tempList[0] = BaseUtility.isValidNull( tempSupp.getConfCode2() ) ? null : BaseUtility.null2Blank( tempSupp.getConfCode2() ).split("[◆]");
							tempList[1] = BaseUtility.isValidNull( tempSupp.getRegDate2()  ) ? null : BaseUtility.null2Blank( tempSupp.getRegDate2() ).split("[◆]");
							tempList[2] = BaseUtility.isValidNull( tempSupp.getRegName2()  ) ? null : BaseUtility.null2Blank( tempSupp.getRegName2() ).split("[◆]");
							tempList[3] = BaseUtility.isValidNull( tempSupp.getContent2()  ) ? null : BaseUtility.null2Blank( tempSupp.getContent2() ).split("[◆]");

							if( !(tempList[2] == null || tempList[2].length <= 0) ) { 
								tempList[0] = tempList[0] == null ? new String[tempList[2].length] : tempList[0];
								tempList[1] = tempList[1] == null ? new String[tempList[2].length] : tempList[1];
								tempList[3] = tempList[3] == null ? new String[tempList[2].length] : tempList[3];
								for( int i = 0; i < tempList[2].length; i++ ) { 
									tempView = new BaseBoardsVO();
									tempView.setConfCode2(BaseUtility.null2Blank( tempList[0][i] ));
									tempView.setConfName2((String)baseCmmnService.getInfoOfConfName( tempList[0][i] ));
									tempView.setRegiDate2(BaseUtility.toDateFormat( tempList[1][i], dtSep, 8 ));
									tempView.setRegiName2(BaseUtility.null2Blank( tempList[2][i] ));
									tempView.setContents2(BaseUtility.null2Blank( tempList[3][i] ));
									tempView.setContents2BR(BaseUtility.tag2Sym( tempList[3][i] ));
									tempView.setContents2CR(BaseUtility.sym2Tag( tempList[3][i] ));
									dataSupp.add(tempView);
								}
							}
						}
					}

					// Data Adjustment(Move Items)
					if( moveItems && !BaseUtility.isValidNull( searchVO.getRowSeq() ) ) { 
						searchVO.setRowSeq(String.valueOf(Integer.parseInt(searchVO.getRowSeq()) + 1));
						if( isGallery && "photo".equalsIgnoreCase(paramPath) ) { 
							searchVO.setBbsType(bbsType);
							prevView = (BaseBoardsVO)boardMngrService.getMovingOfPhoto( searchVO );
						} else { 
							prevView = (BaseBoardsVO)boardMngrService.getMovingOfBoard( searchVO );
						}
						if( prevView != null ) { 
							prevView.setViewNo(prevView.getBbsUUID() + prevView.getRowSeq());
							prevView.setBbsTitle(("Y".equals(prevView.getCreHide())?hideMark:"") + prevView.getBbsTitle());
							prevView.setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( prevView.getAnsPos() )) <= 0 ? prevView.getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ prevView.getBbsTitle());
							prevView.setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( prevView.getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
							if( !prevView.getCreUser().equals(prevView.getCreName()) ) prevView.setCreName(propertiesService.getString("creName"));
						}
						searchVO.setRowSeq(String.valueOf(Integer.parseInt(searchVO.getRowSeq()) - 2));
						if( isGallery && "photo".equalsIgnoreCase(paramPath) ) { 
							searchVO.setBbsType(bbsType);
							nextView = (BaseBoardsVO)boardMngrService.getMovingOfPhoto( searchVO );
						} else { 
							nextView = (BaseBoardsVO)boardMngrService.getMovingOfBoard( searchVO );
						}
						if( nextView != null ) { 
							nextView.setViewNo(nextView.getBbsUUID() + nextView.getRowSeq());
							nextView.setBbsTitle(("Y".equals(nextView.getCreHide())?hideMark:"") + nextView.getBbsTitle());
							nextView.setBbsTitle(Integer.parseInt(BaseUtility.null2Zero( nextView.getAnsPos() )) <= 0 ? nextView.getBbsTitle() : replyImage.replaceAll("@@", webBase) +"&nbsp;"+ nextView.getBbsTitle());
							nextView.setGabSpace(BaseUtility.zeroFill( "", Integer.parseInt(BaseUtility.null2Zero( nextView.getAnsPos() )) * 2 ).replaceAll("0", "&nbsp;"));
							if( !nextView.getCreUser().equals(nextView.getCreName()) ) nextView.setCreName(propertiesService.getString("creName"));
						}
					}
				}
			}

			model.addAttribute("dataView"      , dataView);
			model.addAttribute("dataFile"      , dataFile);
			model.addAttribute("thmbFile"      , thmbFile);
			model.addAttribute("moveItem"      , moveItems?"T":"F");
			model.addAttribute("prevView"      , prevView);
			model.addAttribute("nextView"      , nextView);
			model.addAttribute("cateList"      , cateList);
			model.addAttribute("dataSupp"      , dataSupp);
			model.addAttribute("chldFile"      , chldFile);
			model.addAttribute("chldShow"      , chldIndx<0?0:chldIndx);
			model.addAttribute("viewLink"      , webBase +"/player/"+ searchVO.getRowUUID() + bbsType +".do");
//			model.addAttribute("viewLink"      , webBase +"/player/"+ searchVO.getRowUUID() +"y.do");
		} catch( Exception E ) { 
			log.debug(E.getMessage());
			model.addAttribute("error"         , "데이터 조회중 오류가 발생했습니다.");
			return "/cmmn/exe_script";
		} finally { 
		}	// end of try~catch~finally

		return "/youth/"+retPath;
	}	// end of scrViewOfActivity

	/**
	 * <pre>
	 * 의정활동을(를) 상세조회해주는 메소드
	 * </pre>
	 * 
	 * @param  @PathVariable					// 
	 * @param  @ModelAttribute				// 
	 * @param  HttpServletRequest				// 
	 * @param  HttpServletResponse				// 
	 * @param  ModelMap							// 
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value="/mediaView.do")
	public String scrAjaxOfActivity ( 
			@ModelAttribute("searchVO")BaseCommonVO searchVO, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			ModelMap model 
		) throws Exception { 

		String					webBase      = webContext.getServletContext().getContextPath();
		HttpSession				session      = request == null ? null : request.getSession();
		BaseCommonVO			systemVO     = (BaseCommonVO)baseCmmnService.getSystemOfDate();
		BaseMemberVO			memberVO     = UserDetailsHelper.isAuthenticatedUser() ? (BaseMemberVO)UserDetailsHelper.getAuthenticatedUser() : new BaseMemberVO();
		boolean				vodMode      = "Direct.do".equalsIgnoreCase(BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.ViewMode" ));
		String					viewPort     = BaseUtility.getKeyProperty( "egovframework.egovProps.globals", "Globals.ViewPort" );

		String					uuidNo       = BaseUtility.null2Blank( searchVO.getViewNo() );
		String[]				viewNo       = new String[2]; //BaseUtility.isValidNull( searchVO.getViewNo() ) ? new String[2] : searchVO.getViewNo().split("[`]");
//		String					viewNo       = "";
		int						rowSeq       = 0;
		BaseBoardsVO			dataView     = new BaseBoardsVO();
		StringBuffer			dataRult     = new StringBuffer();
log.debug(" act: "+ BaseUtility.spaceFill( "MediaCounView", 24, "R" ) +" | net: "+ BaseUtility.spaceFill( BaseUtility.reqIPv4Address( request ), 16, "R" ) +" |user: " + memberVO.getUserPID());

		if( BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
			model.addAttribute("message"       , "잘못된 경로로 접속하였습니다.\\n\\n메인화면으로 전환합니다.");
			model.addAttribute("returnUrl"     , webBase);
			return "/cmmn/exe_script";
		}

		// 추가 조회조건 취득
		searchVO.setRowUUID(uuidNo.length() < keySize ? uuidNo : uuidNo.substring(0,keySize));
		searchVO.setRowSeq( uuidNo.length() < keySize ? ""     : uuidNo.substring(keySize));

log.debug("rowUUID: "+ searchVO.getRowUUID() +" |rowSeq: "+ searchVO.getRowSeq());
		try { 
			if( !BaseUtility.isValidNull( searchVO.getViewNo() ) ) { 
				viewNo = BaseUtility.null2Char( (String)boardMngrService.getInfoOfBoardUUID( searchVO.getRowUUID() ), "`" ).split("[`]", viewNo.length);

				// 입력내역 취득(게시판)
				searchVO.setBbsType(viewNo[0]);
				searchVO.setBbsSeq(viewNo[1]);
log.debug("viewNo: "+ searchVO.getBbsType() +" | "+ searchVO.getBbsSeq());

				dataView = (BaseBoardsVO)boardMngrService.getDetailOfBoard( searchVO );
				if( dataView != null ) { 
					boardMngrService.doRtnOfModifyBoardHit( dataView );

					// Data Adjustment(Boards)
					dataView.setActMode("Update");
					dataView.setViewNo(dataView.getBbsUUID());

					dataView.setCreDate(BaseUtility.toDateFormat( dataView.getCreDate(), dtSep, 8 ));
					dataView.setModDate(BaseUtility.toDateFormat( dataView.getModDate(), dtSep, 8 ));
					dataView.setDelDate(BaseUtility.toDateFormat( dataView.getDelDate(), dtSep, 8 ));
					dataView.setTakeDate(BaseUtility.toDateFormat( dataView.getTakeDate(), dtSep, 8 ));

					dataView.setDownAbs(BaseUtility.null2Blank( dataView.getDownAbs() ).replace("/movie_","/movie/"));
					dataView.setDownExt(FileProcess.getFileLowerExt( dataView.getDownRel() ));
					dataView.setFileUUID(BaseUtility.null2Char(dataView.getFileUUID(), "", "6"+ dataView.getFileUUID() ));
					dataView.setRegiDate(BaseUtility.toDateFormat( BaseUtility.null2Char( dataView.getRegiDate(), dataView.getCreDate() ), dtSep, 8 ));
					if( !dataView.getCreUser().equals(dataView.getCreName()) ) dataView.setCreName(propertiesService.getString("creName"));

					dataRult.append("<h1>"+ dataView.getBbsTitle() +"</h1>");
					dataRult.append("<div class='layer_media'>");
					dataRult.append("<video id='vod' oncontextmenu='return false;' width='100%' controls autoplay preload='auto'>");
					if( vodMode ) 
						dataRult.append("<source src='"+ webBase +"/GetMovie.do?key="+ dataView.getDownUUID() +"' type='video/"+ dataView.getDownExt() +"'/>");
					else 
						dataRult.append("<source src='"+ viewPort + webBase + dataView.getDownAbs() +"' type='video/"+ dataView.getDownExt() +"'/>");
					dataRult.append("</video>");
					dataRult.append("</div>");
					dataRult.append("<div class='close_btn'><a href='javascript:void(0);' class='btn_c'><img src='"+ webBase +"/images/coun/popup_close.png' alt='닫기'/></a></div>");
//				} else { 
//					dataRult.append("<h1></h1>");
//					dataRult.append("<div class='layer_media'>");
//					dataRult.append("<video id='vod' oncontextmenu='return false;' width='100%' controls autoplay preload='auto'>");
//					dataRult.append("<source src='' type=''/>");
//					dataRult.append("</video>");
//					dataRult.append("</div>");
//					dataRult.append("<div class='close_btn'><a href='javascript:void(0);' class='btn_c'><img src='"+ webBase +"/images/coun/popup_close.png' alt='닫기'/></a></div>");
				}
			}

//log.debug(dataRult.toString());
			response.setContentType("text/html;charset=utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(dataRult.toString());
			response.getWriter().flush();
			response.getWriter().close();
		} catch( Exception E ) { 
			log.debug(E.getMessage());
		} finally { 
		}	// end of try~catch~finally

		return null;
	}	// end of scrAjaxOfActivity

}	// end of class