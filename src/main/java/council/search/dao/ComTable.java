package council.search.dao;

public class ComTable
{
	private String com_code;
    private String com_name;
	private String com_create;
	private String com_purpose;
	private String com_exe;

	public void setCom_code(String com_code){this.com_code=com_code;}
    public void setCom_name(String com_name){this.com_name=com_name;}
	public void setCom_create(String com_create){this.com_create=com_create;}
	public void setCom_purpose(String com_purpose){this.com_purpose=com_purpose;}
	public void setCom_exe(String com_exe){this.com_exe=com_exe;}

	public String getCom_code(){return com_code;}
    public String getCom_name(){return com_name;}
	public String getCom_create(){return com_create;}
	public String getCom_purpose(){return com_purpose;}
	public String getCom_exe(){return com_exe;}
}