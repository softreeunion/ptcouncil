package council.search.dao;

public class BillTable
{
	private String sug_seq;
    private String bill_num;
	private String bill_title;
	private String com_name;
	private String com_con;
	private String gen_vote;
	private String gen_res;


	public void setSug_seq(String sug_seq){this.sug_seq=sug_seq;}
    public void setBill_num(String bill_num){this.bill_num=bill_num;}
	public void setBill_title(String bill_title){this.bill_title=bill_title;}
	public void setCom_name(String com_name){this.com_name=com_name;}
	public void setCom_con(String com_con){this.com_con=com_con;}
	public void setGen_vote(String gen_vote){this.gen_vote=gen_vote;}
	public void setGen_res(String gen_res){this.gen_res=gen_res;}

	public String getSug_seq(){return sug_seq;}
    public String getBill_num(){return bill_num;}
	public String getBill_title(){return bill_title;}
	public String getCom_name(){return com_name;}
	public String getCom_con(){return com_con;}
	public String getGen_vote(){return gen_vote;}
	public String getGen_res(){return gen_res;}
}