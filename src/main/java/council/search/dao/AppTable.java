package council.search.dao;

public class AppTable
{
	private String conf_code;
    private String app_title;
	private String app_name;
	private String app_ext;

	public void setConf_code(String conf_code){this.conf_code=conf_code;}
    public void setApp_title(String app_title){this.app_title=app_title;}
	public void setApp_name(String app_name){this.app_name=app_name;}
	public void setApp_ext(String app_ext){this.app_ext=app_ext;}

	public String getConf_code(){return conf_code;}
    public String getApp_title(){return app_title;}
	public String getApp_name(){return app_name;}
	public String getApp_ext(){return app_ext;}
}