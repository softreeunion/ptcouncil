package council.search.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import council.search.jdbc.DBManager;

/**
 * DB가 mssql을 사용할때 처리
 */
public class MsBoardDAOImpl implements CSearchDAO
{
	/**
	 * 대수 데이터 가져오기
	 * @return List 결과 값을 EraTable 빈에 담아 List로 반환한다.
	 */
	@SuppressWarnings({"unchecked","rawtypes"})
	public List getEraList() throws Exception
	{
		StringBuffer query = new StringBuffer(); 
		query.append("select era_code,era_name,era_startdate,era_closedate from era_code order by era_code desc;");

		List listData = new ArrayList();
		EraTable eTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				eTable = new EraTable();
				eTable.setEra_code(rs.getString("era_code"));
				eTable.setEra_name(rs.getString("era_name"));
				eTable.setEra_startdate(rs.getString("era_startdate"));
				eTable.setEra_closedate(rs.getString("era_closedate"));
				listData.add(eTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	@SuppressWarnings({"unchecked","rawtypes"})
	public List getEraList(String kind_code) throws Exception
	{
		StringBuffer query = new StringBuffer(); 
		query.append("select distinct B.era_code, B.era_name from conf_ms A, era_code B ");
		query.append("where A.kind_code='"+kind_code+"' and A.era_code=B.era_code order by B.era_code desc;");

		List listData = new ArrayList();
		EraTable eTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				eTable = new EraTable();
				eTable.setEra_code(rs.getString("era_code"));
				eTable.setEra_name(rs.getString("era_name"));
				listData.add(eTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * 년도 데이터 가져오기
	 * @return List 결과 값을 EraTable 빈에 담아 List로 반환한다.
	 */
	@SuppressWarnings({"unchecked","rawtypes"})
	public List getYearList() throws Exception
	{
		StringBuffer query = new StringBuffer(); 
		query.append("select conf_year from conf_ms group by conf_year order by conf_year desc;");

		ArrayList listData = new ArrayList();
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				listData.add(rs.getString("conf_year"));
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * 부모 회의종류 가져오기
	 * @return List 결과 값을 PkindTable 빈에 담아 List로 반환한다.
	 */
	@SuppressWarnings({"unchecked","rawtypes"})
	public List getPkindList(String main_code, int mode) throws Exception
	{
		StringBuffer query = new StringBuffer();	
		if (mode == 0)
		{
			query.append("select distinct D.kind_parent, D.kind_name from ");
			query.append("(select distinct B.kind_parent from conf_ms A, conf_kind B ");
			query.append("where A.era_code='"+main_code+"' and A.kind_code=B.kind_code and B.kind_code<>'000') C,");
			query.append("conf_kind D where D.kind_code=C.kind_parent");
		}else if (mode == 1)
		{
			query.append("select distinct D.kind_parent, D.kind_name from ");
			query.append("(select distinct B.kind_parent from conf_ms A, conf_kind B ");
			query.append("where A.conf_year='"+main_code+"' and A.kind_code=B.kind_code and B.kind_code<>'000') C,");
			query.append("conf_kind D where D.kind_code=C.kind_parent");
		}

		List listData = new ArrayList();
		PkindTable pTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				pTable = new PkindTable();			
				pTable.setKind_parent(rs.getString("kind_parent"));
				pTable.setKind_name(rs.getString("kind_name"));
				listData.add(pTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	@SuppressWarnings({"unchecked","rawtypes"})
	public List getPkindList() throws Exception
	{
		StringBuffer query = new StringBuffer();		
		query.append("select kind_parent, kind_name from conf_kind where kind_parent=kind_code ");
		query.append("and kind_code<>'000' order by kind_code");

		List listData = new ArrayList();
		PkindTable pTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				pTable = new PkindTable();			
				pTable.setKind_parent(rs.getString("kind_parent"));
				pTable.setKind_name(rs.getString("kind_name"));
				listData.add(pTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	@SuppressWarnings({"unchecked","rawtypes"})
	public List getPkindSumList(String round_code) throws Exception
	{
		StringBuffer query = new StringBuffer();		
		query.append("SELECT B.kind_parent AS Pk, COUNT(SUBSTRING(A.kind_code, 0, 2)) AS cnt FROM conf_ms A ");
		query.append("INNER JOIN conf_kind B ON A.kind_code = B.kind_code WHERE (A.conf_close <> '1') AND ");
		query.append("(A.point_code <> '00') AND (A.round_code = '"+round_code+"') GROUP BY B.kind_parent");
		List listData = new ArrayList();
		PkindTable pTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				pTable = new PkindTable();			
				pTable.setPk(rs.getString("Pk"));
				pTable.setCnt(rs.getString("cnt"));
				listData.add(pTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * 자식 회의종류 가져오기
	 * @return List 결과 값을 PkindTable 빈에 담아 List로 반환한다.
	 */
	@SuppressWarnings({"unchecked","rawtypes"})
	public List getCkindList(String main_code, String kind_parent, int mode) throws Exception
	{
		StringBuffer query = new StringBuffer();
		if (mode == 0)
		{
			query.append("select distinct B.kind_code, B.kind_name from conf_ms A, conf_kind B ");
			query.append("where A.era_code='"+main_code+"' and B.kind_parent='"+kind_parent+"' ");
			query.append("and A.kind_code=B.kind_code");
		}else if (mode == 1)
		{
			query.append("select distinct B.kind_code, B.kind_name from conf_ms A, conf_kind B ");
			query.append("where A.conf_year='"+main_code+"' and B.kind_parent='"+kind_parent+"' ");
			query.append("and A.kind_code=B.kind_code");
		}else if (mode == 2)
		{
			query.append("select distinct B.kind_code, B.kind_name from conf_ms A, conf_kind B ");
			query.append("where B.kind_parent='"+kind_parent+"' ");
			query.append("and A.kind_code=B.kind_code");
		}    	
		List listData = new ArrayList();
		CkindTable cTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				cTable = new CkindTable();			
				cTable.setKind_Code(rs.getString("kind_code"));
				cTable.setKind_name(rs.getString("kind_name"));
				listData.add(cTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * 회수 가져오기
	 * @return List 결과 값을 RoundTable 빈에 담아 List로 반환한다.
	 */
	@SuppressWarnings({"unchecked","rawtypes"})
	public List getRoundList(String main_code, String kind_code, int mode) throws Exception
	{
		StringBuffer query = new StringBuffer();
		if (mode == 0)
		{
			query.append("select distinct B.round_code, B.round_name, B.round_div, B.round_startdate, ");
			query.append("B.round_closedate from conf_ms A, round_code B where A.era_code='"+main_code+"' ");
			query.append("and A.kind_code='"+kind_code+"' and A.round_code=B.round_code order by B.round_code desc");
		}else if (mode == 1)
		{
			query.append("select distinct B.round_code, B.round_name, B.round_div, B.round_startdate, ");
			query.append("B.round_closedate from conf_ms A, round_code B where A.conf_year='"+main_code+"' ");
			query.append("and A.kind_code='"+kind_code+"' and A.round_code=B.round_code order by B.round_code desc");
		}else if (mode == 2)
		{
			query.append("select distinct B.round_code, B.round_name, B.round_div, B.round_startdate, ");
			query.append("B.round_closedate from conf_ms A, round_code B where A.era_code='"+main_code+"' ");
			query.append("and A.kind_code='"+kind_code+"' and A.round_code=B.round_code order by B.round_code desc");
		}else if (mode == 3)
		{
			query.append("select distinct B.round_code, B.round_name, B.round_div, B.round_startdate, ");
			query.append("B.round_closedate from conf_ms A, round_code B where A.era_code='"+main_code+"' ");
			query.append("and A.round_code=B.round_code and A.conf_seq in (select C.conf_seq from conf_sug C, conf_sug D where C.conf_seq=D.conf_seq and D.bill_title<>'') order by B.round_code desc");
		}      	
		List listData = new ArrayList();
		RoundTable rTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				rTable = new RoundTable();
				rTable.setRound_code(rs.getString("round_code"));
				rTable.setRound_name(rs.getString("round_name"));
				rTable.setRound_div(rs.getString("round_div"));
				rTable.setRound_startdate(rs.getString("round_startdate"));
				rTable.setRound_closedate(rs.getString("round_closedate"));
				listData.add(rTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * 회수 가져오기
	 * @return List 결과 값을 RoundTable 빈에 담아 List로 반환한다.
	 */
	@SuppressWarnings({"unchecked","rawtypes"})
	public List getRoundList(String era_code) throws Exception
	{
		StringBuffer query = new StringBuffer();		
		query.append("select round_code, round_name, round_div, round_startdate, round_closedate from round_code ");
		query.append("where era_code='"+era_code+"' order by round_code desc");

		List listData = new ArrayList();
		RoundTable rTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				rTable = new RoundTable();
				rTable.setRound_code(rs.getString("round_code"));
				rTable.setRound_name(rs.getString("round_name")); 
				rTable.setRound_div(rs.getString("round_div"));
				rTable.setRound_startdate(rs.getString("round_startdate"));
				rTable.setRound_closedate(rs.getString("round_closedate"));
				listData.add(rTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	@SuppressWarnings({"unchecked","rawtypes"})
	public List getRoundList1(String start_round, String close_round) throws Exception
	{
		StringBuffer query = new StringBuffer();
		String where = "";
		if (!start_round.equals(""))
		{
			where = "and cast(B.round_code as int)>="+Integer.parseInt(start_round);
		}
		if (!close_round.equals(""))
		{
			where = where + "and cast(B.round_code as int)<="+Integer.parseInt(close_round);
		}
		query.append("SELECT B.round_code, B.round_name, COUNT(DISTINCT A.conf_date) AS cnt, B.round_startDate, B.round_closeDate ");
		query.append("FROM conf_ms A INNER JOIN round_code B ON A.round_code = B.round_code WHERE B.round_code <> ''"+where);
		query.append("GROUP BY B.round_code, B.round_name, B.round_startDate, B.round_closeDate order by B.round_code desc");

		List listData = new ArrayList();
		RoundTable rTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				rTable = new RoundTable();			
				rTable.setRound_code(rs.getString("round_code"));
				rTable.setRound_name(rs.getString("round_name")); 
				rTable.setRound_cnt(rs.getString("cnt"));
				rTable.setRound_startdate(rs.getString("round_startdate"));
				rTable.setRound_closedate(rs.getString("round_closedate"));
				listData.add(rTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * 차수 가져오기
	 * @return List 결과 값을 PointTable 빈에 담아 List로 반환한다.
	 */
	@SuppressWarnings({"unchecked","rawtypes"})
	public List getPointList(String main_code, String kind_code, String round_code, int mode) throws Exception
	{
		StringBuffer query = new StringBuffer();
		if (mode == 0)
		{
			query.append("select distinct B.point_code, A.conf_code, B.point_name, A.conf_date, A.conf_starttime, ");
			query.append("A.conf_close from conf_ms A, conf_point B where A.era_code='"+main_code+"' ");
			query.append("and A.kind_code='"+kind_code+"' and A.round_code='"+round_code+"' ");
			query.append("and A.point_code=B.point_code order by A.conf_date, A.conf_starttime");
		}else if (mode == 1)
		{
			query.append("select distinct B.point_code, A.conf_code, B.point_name, A.conf_date, A.conf_starttime, ");
			query.append("A.conf_close from conf_ms A, conf_point B where A.conf_year='"+main_code+"' ");
			query.append("and A.kind_code='"+kind_code+"' and A.round_code='"+round_code+"' ");
			query.append("and A.point_code=B.point_code order by A.conf_date, A.conf_starttime");
		}else if (mode == 2)
		{
			query.append("select distinct B.point_code, A.conf_code, B.point_name, A.conf_date, A.conf_starttime, ");
			query.append("A.conf_close from conf_ms A, conf_point B where ");
			query.append("A.kind_code='"+kind_code+"' and A.round_code='"+round_code+"' ");
			query.append("and A.point_code=B.point_code order by A.conf_date, A.conf_starttime");
		}

		List listData = new ArrayList();
		PointTable tTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				tTable = new PointTable();
				tTable.setPoint_code(rs.getString("point_code"));
				tTable.setConf_code(rs.getString("conf_code")); 
				tTable.setPoint_name(rs.getString("point_name"));
				tTable.setConf_date(rs.getString("conf_date"));
				tTable.setConf_starttime(rs.getString("conf_starttime"));
				tTable.setConf_close(rs.getString("conf_close"));
				listData.add(tTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * 안건 갯수 가져오기
	 */
	public int getSugCount(String conf_code) throws Exception
	{
		int Scnt = 0;
		String query = "select count(A.sug_seq) as Scnt from conf_sug A, conf_ms B where A.conf_seq = B.conf_seq ";
		query = query + "and B.conf_code='"+conf_code+"'";

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			if(rs.next()) Scnt = rs.getInt("Scnt");
		} finally {
			DBManager.close(rs,pstmt,conn);
		}
		return Scnt;
	}

	/**
	 * 안건 가져오기
	 * @return List 결과 값을 SugTable 빈에 담아 List로 반환한다.
	 */
	@SuppressWarnings({"unchecked","rawtypes"})
	public List getSugList(String conf_code) throws Exception
	{
		StringBuffer query = new StringBuffer();			
		query.append("select A.sug_seq, A.sug_num, A.sug_title from conf_sug A, conf_ms B where A.conf_seq = B.conf_seq and B.conf_code='"+conf_code+"' order by A.sug_num");

		List listData = new ArrayList();
		SugTable sTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				sTable = new SugTable();
				sTable.setSug_seq(rs.getString("sug_seq"));
				sTable.setSug_num(rs.getString("sug_num")); 
				sTable.setSug_title(rs.getString("sug_title"));
				listData.add(sTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * 의안 가져오기
	 * @return List 결과 값을 BillTable 빈에 담아 List로 반환한다.
	 */
	@SuppressWarnings({"unchecked","rawtypes"})
	public List getBillList(String round_code) throws Exception
	{
		StringBuffer query = new StringBuffer();
		query.append("select A.sug_seq, A.bill_title from conf_sug A where (A.conf_seq in (select conf_seq from conf_ms where round_code='"+round_code+"') or A.round_code='"+round_code+"') and A.bill_title<>'' order by A.sug_num desc");		

		List listData = new ArrayList();
		BillTable bTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			//System.out.println(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				bTable = new BillTable();
				bTable.setSug_seq(rs.getString("sug_seq"));
				bTable.setBill_title(rs.getString("bill_title"));
				listData.add(bTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * 첫 대수 시작 기간 가져오기
	 */
	public String getEraStart() throws Exception
	{
		String era_startdate = null;
		String query = "select min(era_startdate) as era_startdate from era_code";

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			if(rs.next()) era_startdate = rs.getString("era_startdate");
		} finally {
			DBManager.close(rs,pstmt,conn);
		}
		return era_startdate;
	}

	/**
	 * 마지막 대수 끝 기간 가져오기
	 */
	public String getEraClose() throws Exception
	{
		String era_closedate = null;
		String query = "select max(era_closedate) as era_closedate from era_code";

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			if(rs.next()) era_closedate = rs.getString("era_closedate");
		} finally {
			DBManager.close(rs,pstmt,conn);
		}
		return era_closedate;
	}

	/**
	 * 항목검색 결과갯수
	 */
	public int getItemCount(String where) throws Exception
	{
		int Scnt = 0;
		String query = "select count(conf_code) as Scnt from conf_ms where conf_code <>''"+where;

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			if(rs.next()) Scnt = rs.getInt("Scnt");
		} finally {
			DBManager.close(rs,pstmt,conn);
		}
		return Scnt;
	} 

	/**
	 * 항목검색결과 가져오기
	 */
	@SuppressWarnings({"unchecked","rawtypes"})
	public List getItemList(int page_art, int now_page, String where) throws Exception
	{
		StringBuffer query = new StringBuffer();		
		query.append("select top "+page_art+" conf_code from conf_ms where conf_code <>''"+where+" and conf_code not in ");
		query.append("(select top "+(now_page-1)*page_art+" conf_code from conf_ms where conf_code <>''"+where+" order by era_code desc, round_code desc, kind_code asc, point_code asc)");
		query.append("order by era_code desc, round_code desc, kind_code asc, point_code asc");

		List listData = new ArrayList();
		ItemTable iTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				iTable = new ItemTable();
				iTable.setConf_code(rs.getString("conf_code"));
				listData.add(iTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * 대수코드에서 대수이름가져오기
	 */
	public String getEraName(String era_code) throws Exception
	{
		String era_name = "코드없음";
		String query = "select era_name from era_code where era_code='"+era_code+"'";

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			if(rs.next()) era_name = rs.getString("era_name");
		} finally {
			DBManager.close(rs,pstmt,conn);
		}
		return era_name;
	}

	/**
	 * 회수코드에서 회수이름가져오기
	 */
	public String getRoundName(String round_code) throws Exception
	{
		String round_name = "코드없음";
		String query = "select round_name from round_code where round_code='"+round_code+"'";

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			if(rs.next()) round_name = rs.getString("round_name");
		} finally {
			DBManager.close(rs,pstmt,conn);
		}
		return round_name;
	}

	/**
	 * 회의코드에서 회의이름가져오기
	 */
	public String getKindName(String kind_code) throws Exception
	{
		String kind_name = "코드없음";
		String query = "select kind_name from conf_kind where kind_code='"+kind_code+"'";

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			if(rs.next()) kind_name = rs.getString("kind_name");
		} finally {
			DBManager.close(rs,pstmt,conn);
		}
		return kind_name;
	}

	/**
	 * 차수코드에서 차수이름가져오기
	 */
	public String getPointName(String point_code) throws Exception
	{
		String point_name = "코드없음";
		String query = "select point_name from conf_point where point_code='"+point_code+"'";

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			if(rs.next()) point_name = rs.getString("point_name");
		} finally {
			DBManager.close(rs,pstmt,conn);
		}
		return point_name;
	}

	/**
	 * 의회용어 검색결과 가져오기
	 * @return List 결과 값을 DicTable 빈에 담아 List로 반환한다.
	 */
    @SuppressWarnings({"unchecked","rawtypes"})
	public List getDicList(String dic_index, String keyword) throws Exception
	{
		String query = "";
		String query1 = "Select dic_word from dic_ms where dic_index = '" + dic_index + "' order by dic_word";
		String query2 = "Select dic_word, dic_explain from dic_ms where dic_word like '%" + keyword + "%' order by dic_word";
		String dic_word = "";
		String dic_explain = "";
		String dic_word1 = "";

		if (keyword.equals(""))
		{
			query = query1;
		}else{
			query = query2;
		}

		List listData = new ArrayList();
		DicTable dTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			while(rs.next()){
				dTable = new DicTable();	
				dic_word = rs.getString("dic_word");
				dTable.setDic_word(dic_word);

				if (!keyword.equals("")) {
					dic_explain = rs.getString("dic_explain");
					String query3 = "Select dic_word from dic_ms";

					ResultSet rs1 = null;
					PreparedStatement pstmt1 = null;
					Connection conn1 = null;
					try{
						conn1 = DBManager.getConnection();
						pstmt1 = conn1.prepareStatement(query3);
						rs1 = pstmt1.executeQuery();
						while(rs1.next()){
							dic_word1 = rs1.getString("dic_word").trim();
							dic_explain = dic_explain.replaceAll(dic_word1,"<a href='CSearch?cmd=dic&keyword="+dic_word1+"' class='D'>"+dic_word1+"</a>"); 							
						}
					}finally{
						DBManager.close(pstmt1,conn1);
					}
					dTable.setDic_explain(dic_explain); 
				}

				listData.add(dTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * 안건검색 결과갯수
	 */
	public int getRsugCount(String where) throws Exception
	{
		int Scnt = 0;
		String query = "select count(A.sug_seq) as Scnt from conf_sug A, conf_ms B where A.conf_seq=B.conf_seq"+where;

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			if(rs.next()) Scnt = rs.getInt("Scnt");
		} finally {
			DBManager.close(rs,pstmt,conn);
		}
		return Scnt;
	} 

	/**
	 * 안건검색결과 가져오기
	 */
    @SuppressWarnings({"unchecked","rawtypes"})
	public List getRsugList(int page_art, int now_page, String where) throws Exception
	{
		StringBuffer query = new StringBuffer();		
		query.append("select top "+page_art+" B.conf_code, A.sug_num, A.sug_title, A.sug_seq from ");
		query.append("conf_sug A, conf_ms B where A.conf_seq = B.conf_seq and A.sug_seq <>''"+where);
		query.append(" and A.sug_seq not in (select top "+(now_page-1)*page_art+" A.sug_seq from conf_sug A, ");
		query.append("conf_ms B where A.conf_seq = B.conf_seq and A.sug_seq <>''"+where);
		query.append(" order by B.conf_code desc, A.sug_seq asc) order by B.conf_code desc, A.sug_seq asc");

		List listData = new ArrayList();
		SugTable sTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				sTable = new SugTable();			
				sTable.setConf_code(rs.getString("conf_code"));
				sTable.setSug_num(rs.getString("sug_num")); 
				sTable.setSug_title(rs.getString("sug_title"));
				sTable.setSug_seq(rs.getString("sug_seq"));
				listData.add(sTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * 회의시간검색결과 가져오기
	 */
    @SuppressWarnings({"unchecked","rawtypes"})
	public List getTimesList(String where) throws Exception
	{
		StringBuffer query = new StringBuffer();		
		query.append("select A.conf_code, B.round_name, C.kind_name, D.point_name, B.round_div, A.conf_starttime, ");
		query.append("A.conf_closetime, A.conf_reqtime, A.conf_close from conf_ms A, round_code B, conf_kind C, ");
		query.append("conf_point D where A.round_code=B.round_code and A.kind_code=C.kind_code and A.kind_code<>'000' ");
		query.append("and A.point_code=D.point_code "+where+" order by A.round_code desc, A.kind_code, A.point_code");

		List listData = new ArrayList();
		TimeTable tTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				tTable = new TimeTable();			
				tTable.setConf_code(rs.getString("conf_code"));
				tTable.setRound_name(rs.getString("round_name"));
				tTable.setKind_name(rs.getString("kind_name"));
				tTable.setPoint_name(rs.getString("point_name"));
				tTable.setRound_div(rs.getString("round_div"));
				tTable.setConf_starttime(rs.getString("conf_starttime"));
				tTable.setConf_closetime(rs.getString("conf_closetime"));
				tTable.setConf_reqtime(rs.getString("conf_reqtime"));
				tTable.setConf_close(rs.getString("conf_close"));
				listData.add(tTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * 부록검색결과 가져오기
	 */
    @SuppressWarnings({"unchecked","rawtypes"})
	public List getAppList(int page_art, int now_page, String where) throws Exception
	{
		StringBuffer query = new StringBuffer();		
		query.append("select top "+page_art+" C.conf_code, A.app_title, A.app_name, A.app_ext from sug_app A, conf_sug B, conf_ms C where A.app_seq <>''"+where+" and A.app_seq not in ");
		query.append("(select top "+(now_page-1)*page_art+" A.app_seq from sug_app A, conf_sug B, conf_ms C where A.app_seq <>''"+where+" order by C.conf_code desc, A.app_name asc)");
		query.append("order by C.conf_code desc, A.app_name asc");

		List listData = new ArrayList();
		AppTable aTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			//System.out.println(query.toString());
			while(rs.next()){
				aTable = new AppTable();			
				aTable.setConf_code(rs.getString("conf_code"));
				aTable.setApp_title(rs.getString("app_title"));
				aTable.setApp_name(rs.getString("app_name"));
				aTable.setApp_ext(rs.getString("app_ext"));
				listData.add(aTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * 부록검색결과 갯수
	 */
	public int getAppCount(String where) throws Exception
	{
		int Scnt = 0;
		String query = "select count(A.app_seq) as Scnt from sug_app A, conf_sug B, conf_ms C where A.app_seq <>''"+where;

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			if(rs.next()) Scnt = rs.getInt("Scnt");
		} finally {
			DBManager.close(rs,pstmt,conn);
		}
		return Scnt;      
	}

	/**
	 * 질문답변 색인 검색결과 가져오기
	 */
    @SuppressWarnings({"unchecked","rawtypes"})
	public List getAskList(int page_art, int now_page, String where) throws Exception
	{
		StringBuffer query = new StringBuffer();		
		query.append("select top "+page_art+" B.conf_code, A.qna_qman, C.mem_name, A.qna_qsum, A.qna_aman, A.qna_asum from ");
		query.append("conf_qna A, conf_ms B, mem_ms C where A.qna_seq <>''"+where+" and A.qna_qman=C.mem_id and A.qna_seq not in ");
		query.append("(select top "+(now_page-1)*page_art+" A.qna_seq from conf_qna A, conf_ms B, mem_ms C where ");
		query.append("A.qna_seq <>''"+where+" and A.qna_qman=C.mem_id order by B.conf_code desc, A.qna_seq asc)");
		query.append("order by B.conf_code desc, A.qna_seq asc");

		List listData = new ArrayList();
		AskTable aTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		//System.out.println(query.toString());
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();			
			while(rs.next()){
				aTable = new AskTable();			
				aTable.setConf_code(rs.getString("conf_code"));
				aTable.setQna_qman(rs.getString("qna_qman"));
				aTable.setQna_qman(rs.getString("mem_name"));
				aTable.setQna_qsum(rs.getString("qna_qsum"));
				aTable.setQna_aman(rs.getString("qna_aman"));
				aTable.setQna_asum(rs.getString("qna_asum"));
				listData.add(aTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * 질문답변 색인 검색결과 갯수
	 */
	public int getAskCount(String where) throws Exception
	{
		int Scnt = 0;
		String query = "select count(A.qna_seq) as Scnt from conf_qna A, conf_ms B, mem_ms C where A.qna_seq <>'' and A.qna_qman=C.mem_id"+where;

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			if(rs.next()) Scnt = rs.getInt("Scnt");
		} finally {
			DBManager.close(rs,pstmt,conn);
		}
		return Scnt;
	}

	/**
	 * 안건종류 가져오기
	 */
    @SuppressWarnings({"unchecked","rawtypes"})
	public List getSug_kindList() throws Exception
	{
		StringBuffer query = new StringBuffer();	
		query.append("select sug_code, sug_name from sug_kind where sug_code<>'000' order by sug_code");

		List listData = new ArrayList();
		BillKindTable bkTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				bkTable = new BillKindTable();			
				bkTable.setKind_code(rs.getString("sug_code"));
				bkTable.setKind_name(rs.getString("sug_name"));
				listData.add(bkTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * 안건처리 가져오기
	 */
    @SuppressWarnings({"unchecked","rawtypes"})
	public List getSug_resList() throws Exception
	{
		StringBuffer query = new StringBuffer();	
		query.append("select res_code, res_name from sug_res where res_code<>'00' order by res_code");

		List listData = new ArrayList();
		BillResTable brTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				brTable = new BillResTable();			
				brTable.setRes_code(rs.getString("res_code"));
				brTable.setRes_name(rs.getString("res_name"));
				listData.add(brTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * 안건통계수 가져오기
	 */
    @SuppressWarnings("rawtypes")
	public int getBillSum(String era_code, String code, String res_code, String mode) throws Exception
	{
		StringBuffer query = new StringBuffer();	
		int Bcnt = 0;
		query.append("select count(sug_seq) as Bcnt from conf_sug A where (A.conf_seq in (select conf_seq from conf_ms where era_code='"+era_code+"') or A.era_code='"+era_code+"')");
		if (mode.equals("0"))
		{
			query.append(" and A.sug_code='"+code+"' and A.gen_res='"+res_code+"'");
		}else{
			query.append(" and A.com_code='"+code+"' and A.gen_res='"+res_code+"'");
		}		

		//System.out.println (query.toString());
		List listData = new ArrayList();
		BillResTable brTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			if(rs.next()) Bcnt = rs.getInt("Bcnt");
		} finally {
			DBManager.close(rs,pstmt,conn);
		}
		return Bcnt;     
	}

	/**
	 * 의안검색 결과 가져오기
	 */
    @SuppressWarnings({"unchecked","rawtypes"})
	public List getBillList(int page_art, int now_page, String where) throws Exception
	{
		StringBuffer query = new StringBuffer();		
		query.append("select top "+page_art+" A.sug_seq, A.bill_num, A.bill_title, B.com_name, A.com_con, A.gen_vote, C.res_name from ");
		query.append("conf_sug A, com_code B, sug_res C "+where+" and A.sug_seq not in ");
		query.append("(select top "+(now_page-1)*page_art+" A.sug_seq from conf_sug A, com_code B, sug_res C ");
		query.append(where+" order by A.com_con desc)");
		query.append(" order by A.com_con desc");
		//System.out.println(query.toString());		
		//query.append("select A.sug_seq, A.bill_num, A.bill_title, B.com_name, A.com_con, A.gen_vote, C.res_name from ");
		//query.append("conf_sug A, com_code B, sug_res C, conf_ms D "+where+" order by A.bill_num desc");

		List listData = new ArrayList();
		BillTable bTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				bTable = new BillTable();			
				bTable.setSug_seq(rs.getString("sug_seq"));
				bTable.setBill_num(rs.getString("bill_num"));
				bTable.setBill_title(rs.getString("bill_title"));
				bTable.setCom_name(rs.getString("com_name"));  
				bTable.setCom_con(rs.getString("com_con"));  
				bTable.setGen_vote(rs.getString("gen_vote"));  
				bTable.setGen_res(rs.getString("res_name"));  
				listData.add(bTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * ㅡ의안검색 갯수
	 */
	public int getBillCount(String where) throws Exception
	{
		int Bcnt = 0;
		String query = "select count(A.sug_seq) as Bcnt from conf_sug A, com_code B, sug_res C "+where;
		//System.out.println(query);
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			if(rs.next()) Bcnt = rs.getInt("Bcnt");
		} finally {
			DBManager.close(rs,pstmt,conn);
		}
		return Bcnt;      
	}

	/**
	 * 검색어 통계수 입력
	 */
	@SuppressWarnings("resource")
	public void updateKwrdStat(String kwrd) throws Exception  
	{
		PreparedStatement pstmt = null;
		Connection conn = null;

		int Scnt = 0;
		String query = "select count(stat_seq) as Scnt from kwrd_stat where kwrd='"+kwrd+"'";
		ResultSet rs = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query);
			rs = pstmt.executeQuery();
			if(rs.next()) Scnt = rs.getInt("Scnt");
		} finally {
			DBManager.close(rs,pstmt,conn);
		}

		if (Scnt == 0)
		{
			query = "insert into kwrd_stat (kwrd,cnt) values(?,?)";
			try{
				conn = DBManager.getConnection();
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1,kwrd);
				pstmt.setInt(2,1);
				pstmt.executeUpdate();
			}finally{
				DBManager.close(pstmt,conn);
			}
		}else{
			query = "update kwrd_stat set cnt=cnt+1 where kwrd='"+kwrd+"'";
			try{
				conn = DBManager.getConnection();
				pstmt = conn.prepareStatement(query);
				pstmt.executeUpdate();
			}finally{
				DBManager.close(pstmt,conn);
			}
		}        
	}

	/**
	 * 위원회정보 가져오기
	 */
    @SuppressWarnings({"unchecked","rawtypes"})
	public List getComCode() throws Exception
	{
		StringBuffer query = new StringBuffer();		
		query.append("select com_code, com_name, com_create, com_purpose, com_exe from com_code ");
		query.append("where com_code <> '000' order by com_code");

		//System.out.println(query.toString());			

		List listData = new ArrayList();
		ComTable cTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				cTable = new ComTable();			
				cTable.setCom_code(rs.getString("com_code"));
				cTable.setCom_name(rs.getString("com_name")); 
				cTable.setCom_create(rs.getString("com_create")); 
				cTable.setCom_purpose(rs.getString("com_purpose")); 
				cTable.setCom_exe(rs.getString("com_exe")); 
				listData.add(cTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * 피감사기관정보 가져오기
	 */
    @SuppressWarnings({"unchecked","rawtypes"})
	public List getInsCode() throws Exception
	{
		StringBuffer query = new StringBuffer();		
		query.append("select ins_code, ins_name from conf_ins ");
		query.append("where ins_code <> '00' order by ins_code");

		//System.out.println(query.toString());			

		List listData = new ArrayList();
		InsTable iTable = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				iTable = new InsTable();			
				iTable.setIns_code(rs.getString("ins_code"));
				iTable.setIns_name(rs.getString("ins_name")); 
				listData.add(iTable);
			}
		} finally{
			DBManager.close(pstmt,conn);
		}
		return listData;
	}

	/**
	 * 피감사기관 이름
	 */
	public String getInsName(String where) throws Exception
	{		
		String ins_name = "";
		String query = "select ins_name from conf_ins "+where;
		//System.out.println(query);
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query);            
			rs = pstmt.executeQuery();
			if(rs.next()) ins_name = rs.getString("ins_name");
		} finally {
			DBManager.close(rs,pstmt,conn);
		}
		return ins_name;
	}

	/**
	 * 회의 동영상파일
	 */
	public String getConfMedia(String conf_code) throws Exception
	{		
		String query = "select conf_media from conf_ms where conf_code='"+conf_code+"'";
		//System.out.println(query);
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		String conf_media = "";
		try{
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(query);            
			rs = pstmt.executeQuery();
			if(rs.next()) conf_media = rs.getString("conf_media");
		} finally {
			DBManager.close(rs,pstmt,conn);
		}
		return conf_media;
	}
}