package council.search.dao;

public class PkindTable
{
    private String kind_parent;
    private String kind_name;
	private String Pk;
	private String cnt;

    public void setKind_parent(String kind_parent){this.kind_parent=kind_parent;}
    public void setKind_name(String kind_name){this.kind_name=kind_name;}
	public void setPk(String Pk){this.Pk=Pk;}
	public void setCnt(String cnt){this.cnt=cnt;}

    public String getKind_parent(){return kind_parent;}
    public String getKind_name(){return kind_name;}
	public String getPk(){return Pk;}
	public String getCnt(){return cnt;}
}