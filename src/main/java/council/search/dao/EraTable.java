package council.search.dao;

public class EraTable
{
    private String era_code;
    private String era_name;
    private String era_startdate;
    private String era_closedate;

    public void setEra_code(String era_code){this.era_code=era_code;}
    public void setEra_name(String era_name){this.era_name=era_name;}
    public void setEra_startdate(String era_startdate){this.era_startdate=era_startdate;}
    public void setEra_closedate(String era_closedate){this.era_closedate=era_closedate;}

    public String getEra_code(){return era_code;}
    public String getEra_name(){return era_name;}
    public String getEra_startdate(){return era_startdate;}
    public String getEra_closedate(){return era_closedate;}
}