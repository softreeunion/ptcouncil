package council.search.dao;

//의회용어 검색결과
public class DicTable
{
  private String dic_word;
  private String dic_explain;

  public void setDic_word(String dic_word){this.dic_word=dic_word;}
  public void setDic_explain(String dic_explain){this.dic_explain=dic_explain;}

  public String getDic_word(){return dic_word;}
  public String getDic_explain(){return dic_explain;}
}