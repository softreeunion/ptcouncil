package council.search.dao;

public class AskTable
{
	private String conf_code;
    private String qna_qman;
	private String mem_name;
	private String qna_qsum;
	private String qna_aman;
	private String qna_asum;

	public void setConf_code(String conf_code){this.conf_code=conf_code;}
    public void setQna_qman(String qna_qman){this.qna_qman=qna_qman;}
	public void setMem_name(String mem_name){this.mem_name=mem_name;}
	public void setQna_qsum(String qna_qsum){this.qna_qsum=qna_qsum;}
	public void setQna_aman(String qna_aman){this.qna_aman=qna_aman;}
	public void setQna_asum(String qna_asum){this.qna_asum=qna_asum;}

	public String getConf_code(){return conf_code;}
    public String getQna_qman(){return qna_qman;}
	public String getMem_name(){return mem_name;}
	public String getQna_qsum(){return qna_qsum;}
	public String getQna_aman(){return qna_aman;}
	public String getQna_asum(){return qna_asum;}
}