package council.search.dao;

public class BillKindTable
{
	private String kind_code;
    private String kind_name;

	public void setKind_code(String kind_code){this.kind_code=kind_code;}
    public void setKind_name(String kind_name){this.kind_name=kind_name;}

	public String getKind_code(){return kind_code;}
    public String getKind_name(){return kind_name;}
}