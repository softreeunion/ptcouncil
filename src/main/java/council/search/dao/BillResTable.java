package council.search.dao;

public class BillResTable
{
	private String res_code;
    private String res_name;

	public void setRes_code(String res_code){this.res_code=res_code;}
    public void setRes_name(String res_name){this.res_name=res_name;}

	public String getRes_code(){return res_code;}
    public String getRes_name(){return res_name;}
}