package council.search.dao;

public class SugTable
{
    private String sug_seq;
	private String conf_code;
    private String sug_num;
	private String sug_title;

    public void setSug_seq(String sug_seq){this.sug_seq=sug_seq;}
	public void setConf_code(String conf_code){this.conf_code=conf_code;}
    public void setSug_num(String sug_num){this.sug_num=sug_num;}
	public void setSug_title(String sug_title){this.sug_title=sug_title;}

    public String getSug_seq(){return sug_seq;}
	public String getConf_code(){return conf_code;}
    public String getSug_num(){return sug_num;}
	public String getSug_title(){return sug_title;}
}