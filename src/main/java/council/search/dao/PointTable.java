package council.search.dao;

public class PointTable
{
    private String point_code;
    private String conf_code;
	private String point_name;
	private String conf_date;
	private String conf_starttime;
	private String conf_close;

    public void setPoint_code(String point_code){this.point_code=point_code;}
    public void setConf_code(String conf_code){this.conf_code=conf_code;}
	public void setPoint_name(String point_name){this.point_name=point_name;}
	public void setConf_date(String conf_date){this.conf_date=conf_date;}
	public void setConf_starttime(String conf_starttime){this.conf_starttime=conf_starttime;}
	public void setConf_close(String conf_close){this.conf_close=conf_close;}

    public String getPoint_code(){return point_code;}
    public String getConf_code(){return conf_code;}
	public String getPoint_name(){return point_name;}
	public String getConf_date(){return conf_date;}
	public String getConf_starttime(){return conf_starttime;}
	public String getConf_close(){return conf_close;}
}