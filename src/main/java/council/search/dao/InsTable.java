package council.search.dao;

public class InsTable
{
	private String ins_code;
    private String ins_name;

	public void setIns_code(String ins_code){this.ins_code=ins_code;}
    public void setIns_name(String ins_name){this.ins_name=ins_name;}

	public String getIns_code(){return ins_code;}
    public String getIns_name(){return ins_name;}
}