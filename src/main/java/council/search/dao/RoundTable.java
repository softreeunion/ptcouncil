package council.search.dao;

public class RoundTable
{
    private String round_code;
    private String round_name;
	private String round_div;
	private String round_startdate;
	private String round_closedate;
	private String cnt;

    public void setRound_code(String round_code){this.round_code=round_code;}
    public void setRound_name(String round_name){this.round_name=round_name;}
	public void setRound_div(String round_div){this.round_div=round_div;}
	public void setRound_startdate(String round_startdate){this.round_startdate=round_startdate;}
	public void setRound_closedate(String round_closedate){this.round_closedate=round_closedate;}
	public void setRound_cnt(String cnt){this.cnt=cnt;}

    public String getRound_code(){return round_code;}
    public String getRound_name(){return round_name;}
	public String getRound_div(){return round_div;}
	public String getRound_startdate(){return round_startdate;}
	public String getRound_closedate(){return round_closedate;}
	public String getRound_cnt(){return cnt;}
}