package council.search.dao;

//회의시간통계 결과
public class TimeTable {
	private String conf_code;
	private String round_name;
	private String kind_name;
	private String point_name;
	private String round_div;
	private String conf_starttime;
	private String conf_closetime;
	private String conf_reqtime;
	private String conf_close;

	public void setConf_code(String conf_code) {
		this.conf_code = conf_code;
	}

	public void setRound_name(String round_name) {
		this.round_name = round_name;
	}

	public void setKind_name(String kind_name) {
		this.kind_name = kind_name;
	}

	public void setPoint_name(String point_name) {
		this.point_name = point_name;
	}

	public void setRound_div(String round_div) {
		this.round_div = round_div;
	}

	public void setConf_starttime(String conf_starttime) {
		this.conf_starttime = conf_starttime;
	}

	public void setConf_closetime(String conf_closetime) {
		this.conf_closetime = conf_closetime;
	}

	public void setConf_reqtime(String conf_reqtime) {
		this.conf_reqtime = conf_reqtime;
	}

	public void setConf_close(String conf_close) {
		this.conf_close = conf_close;
	}

	public String getConf_code() {
		return conf_code;
	}

	public String getRound_name() {
		return round_name;
	}

	public String getKind_name() {
		return kind_name;
	}

	public String getPoint_name() {
		return point_name;
	}

	public String getRound_div() {
		return round_div;
	}

	public String getConf_starttime() {
		return conf_starttime;
	}

	public String getConf_closetime() {
		return conf_closetime;
	}

	public String getConf_reqtime() {
		return conf_reqtime;
	}

	public String getConf_close() {
		return conf_close;
	}
}