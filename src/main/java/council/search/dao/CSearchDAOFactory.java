package council.search.dao;


public class CSearchDAOFactory 
{
	public static CSearchDAO getDAO() throws Exception 
	{
        //bundle = ResourceBundle.getBundle("Res_Infomation");
        String dbType = "mssql";
		/*
		if (dbType.equals("oracle")) {
			return new OraBoardDAOImpl();
		} else if (dbType.equals("mysql")) {
			return new MyBoardDAOImpl();
		} else */if (dbType.equals("mssql")) {
			return new MsBoardDAOImpl();
		} else {
			throw new Exception("dbType: " + dbType + " NOT IMPLEMENTED");
		} 
    }
}