package council.search.dao;

import java.util.List;

public interface CSearchDAO
{
	@SuppressWarnings("rawtypes")
    public List getEraList(String kind_code) throws Exception;
	@SuppressWarnings("rawtypes")
	public List getEraList() throws Exception;
	@SuppressWarnings("rawtypes")
	public List getYearList() throws Exception;
	@SuppressWarnings("rawtypes")
	public List getPkindList(String era_code, int mode) throws Exception;
	@SuppressWarnings("rawtypes")
	public List getPkindList() throws Exception;
	@SuppressWarnings("rawtypes")
	public List getPkindSumList(String round_code) throws Exception;
	@SuppressWarnings("rawtypes")
	public List getCkindList(String era_code, String kind_parent, int mode) throws Exception;
	@SuppressWarnings("rawtypes")
	public List getRoundList(String era_code, String kind_code, int mode) throws Exception;
	@SuppressWarnings("rawtypes")
	public List getRoundList(String era_code) throws Exception;
	@SuppressWarnings("rawtypes")
	public List getRoundList1(String start_round, String close_round) throws Exception;
	@SuppressWarnings("rawtypes")
	public List getPointList(String era_code, String kind_code, String round_code, int mode) throws Exception;
	public int getSugCount(String conf_code) throws Exception;
	@SuppressWarnings("rawtypes")
	public List getSugList(String conf_code) throws Exception; 
	@SuppressWarnings("rawtypes")
	public List getBillList(String round_code) throws Exception; 
	public String getEraStart() throws Exception;
	public String getEraClose() throws Exception;
	public int getItemCount(String where) throws Exception;
	@SuppressWarnings("rawtypes")
	public List getItemList(int page_art, int now_page, String where) throws Exception;
	public String getEraName(String era_code) throws Exception;
	public String getRoundName(String round_code) throws Exception;
	public String getKindName(String kind_code) throws Exception;
	public String getPointName(String point_code) throws Exception;
	@SuppressWarnings("rawtypes")
	public List getDicList(String dic_index, String keyword) throws Exception;
	public int getRsugCount(String where) throws Exception;
	@SuppressWarnings("rawtypes")
	public List getRsugList(int page_art, int now_page, String where) throws Exception;
	@SuppressWarnings("rawtypes")
	public List getTimesList(String where) throws Exception;
	@SuppressWarnings("rawtypes")
	public List getAppList(int page_art, int now_page, String where) throws Exception;
	public int getAppCount(String where) throws Exception;
	@SuppressWarnings("rawtypes")
	public List getAskList(int page_art, int now_page, String where) throws Exception;
	public int getAskCount(String where) throws Exception;
	@SuppressWarnings("rawtypes")
	public List getSug_kindList() throws Exception;
	@SuppressWarnings("rawtypes")
	public List getSug_resList() throws Exception;
	public int getBillSum(String era_code, String code, String res_code, String mode) throws Exception;
	@SuppressWarnings("rawtypes")
	public List getBillList(int page_art, int now_page, String where) throws Exception;
	public int getBillCount(String where) throws Exception;
	public void updateKwrdStat(String kwrd) throws Exception;
	@SuppressWarnings("rawtypes")
	public List getComCode() throws Exception;
	@SuppressWarnings("rawtypes")
	public List getInsCode() throws Exception;
	public String getInsName(String where) throws Exception;
	public String getConfMedia(String conf_code) throws Exception;
}