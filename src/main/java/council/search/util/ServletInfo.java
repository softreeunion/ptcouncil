package council.search.util;

import java.util.ResourceBundle;

public class ServletInfo {

	protected static String DATAS_HTML;
	protected static String DATAS_HWP;
	protected static String DATAS_APP;
	protected static String DATAS_MEM;
    
    public ServletInfo() throws Exception 
    {	
		ResourceBundle bundle = null;
        try {
            //bundle = ResourceBundle.getBundle("Res_Infomation");
            DATAS_HTML = "/datas/html";
			DATAS_HWP = "/datas/hwp";
			DATAS_APP = "/datas/app";
			DATAS_MEM = "/datas/mem";
        } catch (Exception e) {
            System.out.println("Err: "+e.toString());
        }
    }

	public static String getHtmlPath()
    {	
		String sRetrun = DATAS_HTML;
		return sRetrun;
    }

	public static String getHwpPath()
    {	
		String sRetrun = DATAS_HWP;
		return sRetrun;
    }

	public static String getAppPath()
    {	
		String sRetrun = DATAS_APP;
		return sRetrun;
    }

	public static String getMemPath()
    {	
		String sRetrun = DATAS_MEM;
		return sRetrun;
    }
}