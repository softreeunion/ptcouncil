package council.search.util;

public class ParamUtil {
    public static String getReqParameter(String Req,String ifNulltoReplace ) {
        if ( Req == null || Req.equals("")) return ifNulltoReplace;
        else return Req.trim();
    }
    public static String getReqParameter(String Req ) {
        if (Req == null || Req.equals("")) return "";
        else               return Req.trim();
    }
    public static int getIntParameter(String Req,int ifNulltoReplace ) {
        try {
            if ( Req == null || Req.equals("")) return ifNulltoReplace;
            else               return Integer.parseInt(Req.toString());
        }catch(NumberFormatException e){
            return ifNulltoReplace;
        }
    }
    public static int getIntParameter(String Req ) {
        try {
            if (Req == null || Req.equals("")) return 0;
            else               return Integer.parseInt(Req.toString());
        }catch(NumberFormatException e){
            return 0;
        }
    }
}