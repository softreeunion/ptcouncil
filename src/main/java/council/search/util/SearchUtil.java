package council.search.util;

import java.io.*;
import java.text.*;

import org.apache.commons.io.FileUtils;

import council.search.dao.CSearchDAO;
import council.search.dao.CSearchDAOFactory;

public class SearchUtil {
	/*
	public static String getReadFile(String sFileName){		
		String sReturn = null;
		try{
			String fileString = "";
			StringBuffer textbuffer = new StringBuffer(); 
			BufferedReader in = new BufferedReader(new FileReader(sFileName));
			String s;			
			while((s = in.readLine()) != null) {
				textbuffer.append(s); 
			}
			sReturn=textbuffer.toString();
			in.close();
			
		}catch(IOException e){
			System.out.println("IO Exception");
		}		
        return sReturn;
	}
	*/
	
	public static String getReadFile(String sFileName){
		File file = new File(sFileName);
		String fileContent = "";
		try {
//			fileContent = FileUtils.readFileToString(file);
fileContent = FileUtils.readFileToString(file,"euc-kr");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("SearchUtil.class : getReadFile =>" + e.getMessage());
		} 
		return fileContent;
	}

	public static String getReadFile(String sFileName, int ipage_line, int ipage){		
		String sReturn = null;
		try{
			String fileString = "";
			StringBuffer textbuffer = new StringBuffer(); 
//			BufferedReader in = new BufferedReader(new FileReader(sFileName));
File file = new File(sFileName);
BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file),"euc-kr"));
			String s;
			int i = 0;
			while((s = in.readLine()) != null && i < ipage_line * ipage) {				
				if (i < ipage_line * (ipage-1))
				{
				}else{
					textbuffer.append(s); 
				}		
				i++;				
			}
			sReturn=textbuffer.toString();
			in.close();
			
		}catch(IOException e){
			System.out.println("IO Exception");
		}
        return sReturn;
	}
	
    public static String getNumToDate(String sNumber) throws Exception {		
		String sReturn = "";
		String yeqr = "";
		String month = "";
		String day = "";

		if (sNumber.length() == 8) // 20030705 -> 2003년 07월 05일 토요일
		{
			yeqr = sNumber.substring(0,4);
			month = sNumber.substring(4,6);
			day = sNumber.substring(6,8);			
		}else{ // 2003-07-05 -> 2003년 07월 05일 토요일
			yeqr = sNumber.substring(0,4);
			month = sNumber.substring(5,7);
			day = sNumber.substring(8,10);				
		}		
		
		DateFormat df = new SimpleDateFormat("yyyyMMdd"); 
		DateFormat ddf = new SimpleDateFormat("EEE"); 
		
		sReturn=yeqr+"년 "+month+"월 "+day+"일 "+ddf.format(df.parse(yeqr+month+day))+"요일";

        return sReturn;
	}
    
	public static String getCodeToName(String sConf_code) throws Exception {		
		CSearchDAO dao;
		dao = CSearchDAOFactory.getDAO();
		String sReturn = "";
		String era_name = dao.getEraName(sConf_code.substring(0,2));
		String round_name = dao.getRoundName(sConf_code.substring(2,6));
		String kind_name = dao.getKindName(sConf_code.substring(6,9));
		String point_name = dao.getPointName(sConf_code.substring(17,19));
		String getDate = getNumToDate(sConf_code.substring(9,17));

		sReturn = era_name + " " + round_name + " " + kind_name + " " + point_name + " (" + getDate + ")";
        return sReturn;
	}	
}
